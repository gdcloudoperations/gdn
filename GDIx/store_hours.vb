﻿'AUTHOR: Alex Dumitrascu
'DATE: 21-12-2017
'UPDATE: 31-01-2018
'
'FORM: store_hours.vb (access from MAIN)

Imports System.Data.OleDb
Imports System.IO
Imports System.Text.RegularExpressions

Public Class form_store_hours

    'PUBLIC VAR
    Public STORE As String = ""     'to store the store number

    'ON LOAD: store_hours.vb
    Private Sub form_store_hours_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'get store number from main GDIx
        lbl_store_hours_store_load.Text = gdx_main.lbl_strno_load.Text
        STORE = gdx_main.txtbox_storeno.Text.ToString

        'load store hours
        getinfo(STORE)

    End Sub

    'GET INFO: read info from CSV file
    Private Sub getinfo(store_no As String)


        'read store_open_hours.csv
        Try

            Dim lines() As String = IO.File.ReadAllLines(gdx_main.store_hours_file)

            'read all lines inside file and store them to an array
            Dim lineArray As New ArrayList()
            For x As Integer = 0 To lines.GetUpperBound(0)

                lineArray.Add(lines(x))
            Next

            'get info based on store number
            Dim regex As Regex = New Regex("^" & store_no & ",")
            For x = 0 To lineArray.Count - 1
                Dim match As Match = regex.Match(lineArray.Item(x))     'store info into match based on regex (store_no)

                'display info based on search
                If match.Success Then
                    'MsgBox(lineArray.Item(x))
                    Dim s As String = lineArray.Item(x)
                    Dim words() As String = s.Split(New Char() {","})

                    lbl_mon_load.Text = words(5) & Chr(9) & " - " & words(6)             'Monday hours
                    lbl_tue_load.Text = words(7) & Chr(9) & " - " & words(8)             'Tuesday hours
                    lbl_wed_load.Text = words(9) & Chr(9) & " - " & words(10)             'Wednesday hours
                    lbl_thu_load.Text = words(11) & Chr(9) & " - " & words(12)           'Thursday hours
                    lbl_fri_load.Text = words(13) & Chr(9) & " - " & words(14)           'Friday hours
                    lbl_sat_load.Text = words(15) & Chr(9) & " - " & words(16)           'Saturday hours
                    lbl_sun_load.Text = words(3) & Chr(9) & " - " & words(4)           'Sunday hours


                End If
            Next

        Catch ex As Exception
            MsgBox("Unable to read store hours at this time!" & vbNewLine & "Please contact support!", MsgBoxStyle.Exclamation, Title:="Error: Store Hours!")
            Me.Dispose()
        End Try

    End Sub

End Class