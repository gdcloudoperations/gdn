﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EditUser))
        Me.grpbox_edituser_details = New System.Windows.Forms.GroupBox()
        Me.lbl_edituser_details = New System.Windows.Forms.Label()
        Me.grpbox_edituser_account = New System.Windows.Forms.GroupBox()
        Me.lbl_edituser_status = New System.Windows.Forms.Label()
        Me.radio_edituser_disabled = New System.Windows.Forms.RadioButton()
        Me.radio_edituser_enabled = New System.Windows.Forms.RadioButton()
        Me.grpbox_edituser_info = New System.Windows.Forms.GroupBox()
        Me.combo_edituser_role = New System.Windows.Forms.ComboBox()
        Me.lbl_edituser_role = New System.Windows.Forms.Label()
        Me.txtbox_edituser_usr_load = New System.Windows.Forms.TextBox()
        Me.lbl_edituser_user = New System.Windows.Forms.Label()
        Me.btn_edituser_cancel = New System.Windows.Forms.Button()
        Me.btn_edituser_save = New System.Windows.Forms.Button()
        Me.grpbox_edituser_details.SuspendLayout()
        Me.grpbox_edituser_account.SuspendLayout()
        Me.grpbox_edituser_info.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpbox_edituser_details
        '
        Me.grpbox_edituser_details.Controls.Add(Me.lbl_edituser_details)
        Me.grpbox_edituser_details.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_edituser_details.Location = New System.Drawing.Point(243, 12)
        Me.grpbox_edituser_details.Name = "grpbox_edituser_details"
        Me.grpbox_edituser_details.Size = New System.Drawing.Size(357, 168)
        Me.grpbox_edituser_details.TabIndex = 11
        Me.grpbox_edituser_details.TabStop = False
        Me.grpbox_edituser_details.Text = "Details"
        '
        'lbl_edituser_details
        '
        Me.lbl_edituser_details.AutoSize = True
        Me.lbl_edituser_details.Location = New System.Drawing.Point(6, 20)
        Me.lbl_edituser_details.Name = "lbl_edituser_details"
        Me.lbl_edituser_details.Size = New System.Drawing.Size(341, 150)
        Me.lbl_edituser_details.TabIndex = 0
        Me.lbl_edituser_details.Text = resources.GetString("lbl_edituser_details.Text")
        '
        'grpbox_edituser_account
        '
        Me.grpbox_edituser_account.Controls.Add(Me.lbl_edituser_status)
        Me.grpbox_edituser_account.Controls.Add(Me.radio_edituser_disabled)
        Me.grpbox_edituser_account.Controls.Add(Me.radio_edituser_enabled)
        Me.grpbox_edituser_account.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_edituser_account.Location = New System.Drawing.Point(12, 101)
        Me.grpbox_edituser_account.Name = "grpbox_edituser_account"
        Me.grpbox_edituser_account.Size = New System.Drawing.Size(225, 49)
        Me.grpbox_edituser_account.TabIndex = 10
        Me.grpbox_edituser_account.TabStop = False
        Me.grpbox_edituser_account.Text = "Account"
        '
        'lbl_edituser_status
        '
        Me.lbl_edituser_status.AutoSize = True
        Me.lbl_edituser_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_edituser_status.Location = New System.Drawing.Point(17, 26)
        Me.lbl_edituser_status.Name = "lbl_edituser_status"
        Me.lbl_edituser_status.Size = New System.Drawing.Size(47, 13)
        Me.lbl_edituser_status.TabIndex = 5
        Me.lbl_edituser_status.Text = "Status:"
        '
        'radio_edituser_disabled
        '
        Me.radio_edituser_disabled.AutoSize = True
        Me.radio_edituser_disabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radio_edituser_disabled.Location = New System.Drawing.Point(140, 24)
        Me.radio_edituser_disabled.Name = "radio_edituser_disabled"
        Me.radio_edituser_disabled.Size = New System.Drawing.Size(66, 17)
        Me.radio_edituser_disabled.TabIndex = 7
        Me.radio_edituser_disabled.TabStop = True
        Me.radio_edituser_disabled.Text = "Disabled"
        Me.radio_edituser_disabled.UseVisualStyleBackColor = True
        '
        'radio_edituser_enabled
        '
        Me.radio_edituser_enabled.AutoSize = True
        Me.radio_edituser_enabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radio_edituser_enabled.Location = New System.Drawing.Point(70, 24)
        Me.radio_edituser_enabled.Name = "radio_edituser_enabled"
        Me.radio_edituser_enabled.Size = New System.Drawing.Size(64, 17)
        Me.radio_edituser_enabled.TabIndex = 6
        Me.radio_edituser_enabled.TabStop = True
        Me.radio_edituser_enabled.Text = "Enabled"
        Me.radio_edituser_enabled.UseVisualStyleBackColor = True
        '
        'grpbox_edituser_info
        '
        Me.grpbox_edituser_info.Controls.Add(Me.combo_edituser_role)
        Me.grpbox_edituser_info.Controls.Add(Me.lbl_edituser_role)
        Me.grpbox_edituser_info.Controls.Add(Me.txtbox_edituser_usr_load)
        Me.grpbox_edituser_info.Controls.Add(Me.lbl_edituser_user)
        Me.grpbox_edituser_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_edituser_info.Location = New System.Drawing.Point(12, 12)
        Me.grpbox_edituser_info.Name = "grpbox_edituser_info"
        Me.grpbox_edituser_info.Size = New System.Drawing.Size(225, 83)
        Me.grpbox_edituser_info.TabIndex = 9
        Me.grpbox_edituser_info.TabStop = False
        Me.grpbox_edituser_info.Text = "User Information"
        '
        'combo_edituser_role
        '
        Me.combo_edituser_role.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.combo_edituser_role.FormattingEnabled = True
        Me.combo_edituser_role.Items.AddRange(New Object() {"*SYS", "Admin", "DBA", "DEV", "Guest", "HO Support", "ORPOS", "Tech Support", "Tech Support*", "SAM", "SAM*"})
        Me.combo_edituser_role.Location = New System.Drawing.Point(92, 51)
        Me.combo_edituser_role.Name = "combo_edituser_role"
        Me.combo_edituser_role.Size = New System.Drawing.Size(121, 23)
        Me.combo_edituser_role.TabIndex = 4
        '
        'lbl_edituser_role
        '
        Me.lbl_edituser_role.AutoSize = True
        Me.lbl_edituser_role.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_edituser_role.Location = New System.Drawing.Point(22, 56)
        Me.lbl_edituser_role.Name = "lbl_edituser_role"
        Me.lbl_edituser_role.Size = New System.Drawing.Size(37, 13)
        Me.lbl_edituser_role.TabIndex = 3
        Me.lbl_edituser_role.Text = "Role:"
        '
        'txtbox_edituser_usr_load
        '
        Me.txtbox_edituser_usr_load.Location = New System.Drawing.Point(92, 21)
        Me.txtbox_edituser_usr_load.Name = "txtbox_edituser_usr_load"
        Me.txtbox_edituser_usr_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_edituser_usr_load.TabIndex = 2
        '
        'lbl_edituser_user
        '
        Me.lbl_edituser_user.AutoSize = True
        Me.lbl_edituser_user.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_edituser_user.Location = New System.Drawing.Point(22, 26)
        Me.lbl_edituser_user.Name = "lbl_edituser_user"
        Me.lbl_edituser_user.Size = New System.Drawing.Size(67, 13)
        Me.lbl_edituser_user.TabIndex = 1
        Me.lbl_edituser_user.Text = "Username:"
        '
        'btn_edituser_cancel
        '
        Me.btn_edituser_cancel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_edituser_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_edituser_cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_edituser_cancel.Location = New System.Drawing.Point(162, 156)
        Me.btn_edituser_cancel.Name = "btn_edituser_cancel"
        Me.btn_edituser_cancel.Size = New System.Drawing.Size(74, 21)
        Me.btn_edituser_cancel.TabIndex = 18
        Me.btn_edituser_cancel.Text = "Close"
        Me.btn_edituser_cancel.UseVisualStyleBackColor = False
        '
        'btn_edituser_save
        '
        Me.btn_edituser_save.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_edituser_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_edituser_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_edituser_save.Location = New System.Drawing.Point(82, 156)
        Me.btn_edituser_save.Name = "btn_edituser_save"
        Me.btn_edituser_save.Size = New System.Drawing.Size(74, 21)
        Me.btn_edituser_save.TabIndex = 17
        Me.btn_edituser_save.Text = "Save"
        Me.btn_edituser_save.UseVisualStyleBackColor = False
        '
        'EditUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(613, 194)
        Me.Controls.Add(Me.btn_edituser_cancel)
        Me.Controls.Add(Me.btn_edituser_save)
        Me.Controls.Add(Me.grpbox_edituser_details)
        Me.Controls.Add(Me.grpbox_edituser_account)
        Me.Controls.Add(Me.grpbox_edituser_info)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "EditUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Edit User"
        Me.grpbox_edituser_details.ResumeLayout(False)
        Me.grpbox_edituser_details.PerformLayout()
        Me.grpbox_edituser_account.ResumeLayout(False)
        Me.grpbox_edituser_account.PerformLayout()
        Me.grpbox_edituser_info.ResumeLayout(False)
        Me.grpbox_edituser_info.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents grpbox_edituser_details As GroupBox
    Friend WithEvents lbl_edituser_details As Label
    Friend WithEvents grpbox_edituser_account As GroupBox
    Friend WithEvents lbl_edituser_status As Label
    Friend WithEvents radio_edituser_disabled As RadioButton
    Friend WithEvents radio_edituser_enabled As RadioButton
    Friend WithEvents grpbox_edituser_info As GroupBox
    Friend WithEvents combo_edituser_role As ComboBox
    Friend WithEvents lbl_edituser_role As Label
    Friend WithEvents txtbox_edituser_usr_load As TextBox
    Friend WithEvents lbl_edituser_user As Label
    Friend WithEvents btn_edituser_cancel As Button
    Friend WithEvents btn_edituser_save As Button
End Class
