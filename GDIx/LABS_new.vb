﻿'AUTHOR: Alex Dumitrascu
'DATE: 3-05-2018
'UPDATE: 9-05-2018
'
'FORM: LABS_new.vb (access from LABS.vb)

Imports System.Data.SqlClient
Imports System.Text.RegularExpressions

Public Class LABS_new

    'VARS
    Dim flag_getInfo As Boolean = False          'flag to see if values available from stores DB
    Private regEx As Regex                       'for regular exp
    Dim reg1date, reg2date, bodate, pin1date, pin2date, print1date, print2date As New String(String.Empty) 'store install dates= nothing

    'MAIN FORM (ON LOAD)
    Private Sub LABS_new_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'disable stars
        disableStars()

        'datetimepickers format
        datetimepick_nl_reg1installdate_load.CustomFormat = "MM/dd/yyyy hh-mm-ss"
        datetimepick_nl_pin1_installdate_load.CustomFormat = "MM/dd/yyyy hh-mm-ss"
        datetimepick_nl_print1_installdate_load.CustomFormat = "MM/dd/yyyy hh-mm-ss"
        datetimepick_nl_reg2installdate_load.CustomFormat = "MM/dd/yyyy hh-mm-ss"
        datetimepick_nl_pin2_installdate_load.CustomFormat = "MM/dd/yyyy hh-mm-ss"
        datetimepick_nl_print2_installdate_load.CustomFormat = "MM/dd/yyyy hh-mm-ss"
        datetimepick_nl_boinstalldate_load.CustomFormat = "MM/dd/yyyy hh-mm-ss"


        'disable regs textboxes
        'REG1
        txtbox_nl_reg1hostname_load.Enabled = False
        txtbox_nl_reg1ipaddress_load.Enabled = False
        txtbox_nl_reg1location_load.Enabled = False
        datetimepick_nl_reg1installdate_load.Enabled = False
        datetimepick_nl_reg1installdate_load.Value = DateTime.Now
        'PINPAD1
        txtbox_nl_pin1_ip_load.Enabled = False
        txtbox_nl_pin1_location_load.Enabled = False
        datetimepick_nl_pin1_installdate_load.Enabled = False
        datetimepick_nl_pin1_installdate_load.Value = DateTime.Now
        'PRINTER1
        txtbox_nl_print1_ip_load.Enabled = False
        txtbox_nl_print1_location_load.Enabled = False
        datetimepick_nl_print1_installdate_load.Enabled = False
        datetimepick_nl_print1_installdate_load.Value = DateTime.Now

        'REG2
        txtbox_nl_reg2hostname_load.Enabled = False
        txtbox_nl_reg2ipaddress_load.Enabled = False
        txtbox_nl_reg2location_load.Enabled = False
        datetimepick_nl_reg2installdate_load.Enabled = False
        datetimepick_nl_reg2installdate_load.Value = DateTime.Now
        'PINPAD2
        txtbox_nl_pin2_ip_load.Enabled = False
        txtbox_nl_pin2_location_load.Enabled = False
        datetimepick_nl_pin2_installdate_load.Enabled = False
        datetimepick_nl_pin2_installdate_load.Value = DateTime.Now
        'PRINTER2
        txtbox_nl_print2_ip_load.Enabled = False
        txtbox_nl_print2_location_load.Enabled = False
        datetimepick_nl_print2_installdate_load.Enabled = False
        datetimepick_nl_print2_installdate_load.Value = DateTime.Now

        'BO
        datetimepick_nl_boinstalldate_load.Value = DateTime.Now

        'patches text
        txtbox_nl_epatch_load.Text = "use ,(comma) as separator"
        txtbox_nl_epatch_load.ForeColor = Color.Gray


    End Sub

    'disable mandatory stars
    Private Sub disableStars()

        nl_star12.Visible = False
        nl_star13.Visible = False
        nl_star14.Visible = False
        nl_star15.Visible = False
        nl_star16.Visible = False
        nl_star17.Visible = False
        nl_star18.Visible = False
        nl_star19.Visible = False
        nl_star25.Visible = False
        nl_star26.Visible = False
        nl_star28.Visible = False
        nl_star29.Visible = False
        nl_star30.Visible = False
        nl_star31.Visible = False
        nl_star32.Visible = False
        nl_star34.Visible = False
        nl_star35.Visible = False


    End Sub

    'BUTTON: GET INFO
    Private Sub btn_nl_getinfo_Click(sender As Object, e As EventArgs) Handles btn_nl_getinfo.Click

        'set getinfo flag to false
        flag_getInfo = False

        'check store input
        If txtbox_nl_storeno_load.Text = "" Then
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (lab#)")
            txtbox_nl_storeno_load.Clear()
        ElseIf Not IsNumeric(txtbox_nl_storeno_load.Text) Then
            MsgBox("Not a valid store number. Only digits allowed!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (lab#)")
            txtbox_nl_storeno_load.Clear()
        Else
            GetInfo(txtbox_nl_storeno_load.Text)
        End If

    End Sub

    'BUTTON: CLEAR STORE INFO
    Private Sub btn_nl_clear_Click(sender As Object, e As EventArgs) Handles btn_nl_clear.Click

        'txtbox_nl_storeno_load.Text = Nothing
        cmbbox_nl_env_load.SelectedItem = Nothing
        cmbbox_nl_store_system.SelectedItem = Nothing
        cmbbox_nl_banner_load.SelectedItem = Nothing
        cmbbox_nl_iscombo_load.SelectedItem = Nothing
        txtbox_nl_sisstore_load.Text = Nothing
        txtbox_nl_address_load.Text = Nothing
        txtbox_nl_city_load.Text = Nothing
        cmbbox_nl_country_load.SelectedItem = Nothing
        cmbbox_nl_province_load.SelectedItem = Nothing
        cmbbox_nl_nbofreg_load.SelectedItem = "0"

    End Sub


    'clear all data
    Private Sub clearAll()

        'STORE
        txtbox_nl_storeno_load.Text = Nothing
        cmbbox_nl_env_load.SelectedItem = Nothing
        cmbbox_nl_store_system.SelectedItem = Nothing
        cmbbox_nl_banner_load.SelectedItem = Nothing
        cmbbox_nl_iscombo_load.SelectedItem = Nothing
        txtbox_nl_sisstore_load.Text = Nothing
        txtbox_nl_address_load.Text = Nothing
        txtbox_nl_city_load.Text = Nothing
        cmbbox_nl_country_load.SelectedItem = Nothing
        cmbbox_nl_province_load.SelectedItem = Nothing
        cmbbox_nl_nbofreg_load.SelectedItem = Nothing

        'REG1
        txtbox_nl_reg1hostname_load.Text = Nothing
        txtbox_nl_reg1ipaddress_load.Text = Nothing
        txtbox_nl_reg1location_load.Text = Nothing
        datetimepick_nl_reg1installdate_load.Text = Nothing
        'PINPAD1                      
        txtbox_nl_pin1_ip_load.Text = Nothing
        txtbox_nl_pin1_location_load.Text = Nothing
        datetimepick_nl_pin1_installdate_load.Text = Nothing
        'PRINTER1                      
        txtbox_nl_print1_ip_load.Text = Nothing
        txtbox_nl_print1_location_load.Text = Nothing
        datetimepick_nl_print1_installdate_load.Text = Nothing

        'REG2                            
        txtbox_nl_reg2hostname_load.Text = Nothing
        txtbox_nl_reg2ipaddress_load.Text = Nothing
        txtbox_nl_reg2location_load.Text = Nothing
        datetimepick_nl_reg2installdate_load.Text = Nothing
        'PINPAD2                         
        txtbox_nl_pin2_ip_load.Text = Nothing
        txtbox_nl_pin2_location_load.Text = Nothing
        datetimepick_nl_pin2_installdate_load.Text = Nothing
        'PRINTER2                       
        txtbox_nl_print2_ip_load.Text = Nothing
        txtbox_nl_print2_location_load.Text = Nothing
        datetimepick_nl_print2_installdate_load.Text = Nothing

        'BO                              
        txtbox_nl_bohostname_load.Text = Nothing
        txtbox_nl_boipaddress_load.Text = Nothing
        txtbox_nl_bolocation_load.Text = Nothing
        datetimepick_nl_boinstalldate_load.Text = Nothing

        'project
        cmbbox_nl_status_load.SelectedItem = Nothing
        txtbox_nl_project_load.Text = Nothing
        txtbox_nl_pm_load.Text = Nothing
        txtbox_nl_epatch_load.Text = Nothing
        txtbox_nl_epatch_load.Text = "use ,(comma) as separator"
        txtbox_nl_epatch_load.ForeColor = Color.Gray
        txtbox_labs_comments_load.Text = Nothing

    End Sub

    'get lab details from stores DB
    Private Sub GetInfo(STORE As Integer)

        'clear previous info first
        cmbbox_nl_banner_load.SelectedItem = Nothing
        cmbbox_nl_iscombo_load.SelectedItem = Nothing
        txtbox_nl_sisstore_load.Text = Nothing
        txtbox_nl_address_load.Text = Nothing
        txtbox_nl_city_load.Text = Nothing
        cmbbox_nl_country_load.SelectedItem = Nothing
        cmbbox_nl_province_load.SelectedItem = Nothing


        'VARS
        Dim labs_connection As SqlConnection
        Dim cmd_lab_info As SqlCommand
        Dim labsReader As SqlDataReader

        'SQL STATEMENTS
        Dim sql_lab_info As String = "select Banner, COMBO, [Sister Store], Address, City, Province, Country from stores_info where store = " & STORE & ""

        labs_connection = New SqlConnection(gdx_main.connectionString)
        cmd_lab_info = New SqlCommand(sql_lab_info, labs_connection)

        Try
            labs_connection.Open()
            labsReader = cmd_lab_info.ExecuteReader

            'GET LABS from DB
            If labsReader.HasRows Then
                While labsReader.Read

                    'read values
                    cmbbox_nl_banner_load.SelectedItem = labsReader.GetValue(0).ToString()     'banner
                    If labsReader.GetValue(1).ToString() = True Then                           'isCombo
                        cmbbox_nl_iscombo_load.SelectedItem = "YES"
                    Else
                        cmbbox_nl_iscombo_load.SelectedItem = "NO"
                    End If
                    If IsDBNull(labsReader.GetValue(2)) Then                                   'sister store
                        txtbox_nl_sisstore_load.Text = Nothing
                    Else
                        txtbox_nl_sisstore_load.Text = labsReader.GetValue(2).ToString
                    End If
                    txtbox_nl_address_load.Text = labsReader.GetValue(3).ToString              'address
                    txtbox_nl_city_load.Text = labsReader.GetValue(4).ToString                 'city
                    cmbbox_nl_country_load.SelectedItem = labsReader.GetValue(6).ToString      'country
                    cmbbox_nl_province_load.SelectedItem = labsReader.GetValue(5).ToString     'state

                End While
            End If

            flag_getInfo = True

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [edit labs - update store info]")
            flag_getInfo = False
        End Try

        'CLOSE CONNECTIONS
        labs_connection.Close()

    End Sub

    'load labs details into txtboxes
    Private Sub LoadInfo(STORE As String)



    End Sub

    'get provinces and states
    Private Sub cmbbox_nl_country_load_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_nl_country_load.SelectedIndexChanged
        'ADD CANADIAN PROVINCES
        If cmbbox_nl_country_load.SelectedItem = "Canada" Then

            'clear all items
            cmbbox_nl_province_load.Items.Clear()

            'add items
            cmbbox_nl_province_load.Items.Add("Alberta")
            cmbbox_nl_province_load.Items.Add("BC")
            cmbbox_nl_province_load.Items.Add("Manitoba")
            cmbbox_nl_province_load.Items.Add("NB")
            cmbbox_nl_province_load.Items.Add("NS")
            cmbbox_nl_province_load.Items.Add("NFLD")
            cmbbox_nl_province_load.Items.Add("Nunavut")
            cmbbox_nl_province_load.Items.Add("NWT")
            cmbbox_nl_province_load.Items.Add("Ontario")
            cmbbox_nl_province_load.Items.Add("PEI")
            cmbbox_nl_province_load.Items.Add("Quebec")
            cmbbox_nl_province_load.Items.Add("Saskatchewan")
            cmbbox_nl_province_load.Items.Add("Yukon")

            'ADD US STATES
        ElseIf cmbbox_nl_country_load.SelectedItem = "US" Then

            'clear all items
            cmbbox_nl_province_load.Items.Clear()

            'add items
            cmbbox_nl_province_load.Items.Add("Alabama")
            cmbbox_nl_province_load.Items.Add("Alaska")
            cmbbox_nl_province_load.Items.Add("American Samoa")
            cmbbox_nl_province_load.Items.Add("Arizona")
            cmbbox_nl_province_load.Items.Add("Arkansas")
            cmbbox_nl_province_load.Items.Add("California")
            cmbbox_nl_province_load.Items.Add("Colorado")
            cmbbox_nl_province_load.Items.Add("Connecticut")
            cmbbox_nl_province_load.Items.Add("Delaware")
            cmbbox_nl_province_load.Items.Add("District of Columbia")
            cmbbox_nl_province_load.Items.Add("Federated States of Micronesia")
            cmbbox_nl_province_load.Items.Add("Florida")
            cmbbox_nl_province_load.Items.Add("Georgia")
            cmbbox_nl_province_load.Items.Add("Guam")
            cmbbox_nl_province_load.Items.Add("Hawaii")
            cmbbox_nl_province_load.Items.Add("Idaho")
            cmbbox_nl_province_load.Items.Add("Illinois")
            cmbbox_nl_province_load.Items.Add("Indiana")
            cmbbox_nl_province_load.Items.Add("Iowa")
            cmbbox_nl_province_load.Items.Add("Kansas")
            cmbbox_nl_province_load.Items.Add("Kentucky")
            cmbbox_nl_province_load.Items.Add("Louisiana")
            cmbbox_nl_province_load.Items.Add("Maine")
            cmbbox_nl_province_load.Items.Add("Marshall Islands")
            cmbbox_nl_province_load.Items.Add("Maryland")
            cmbbox_nl_province_load.Items.Add("Massachusetts")
            cmbbox_nl_province_load.Items.Add("Michigan")
            cmbbox_nl_province_load.Items.Add("Minnesota")
            cmbbox_nl_province_load.Items.Add("Mississippi")
            cmbbox_nl_province_load.Items.Add("Missouri")
            cmbbox_nl_province_load.Items.Add("Montana")
            cmbbox_nl_province_load.Items.Add("Nebraska")
            cmbbox_nl_province_load.Items.Add("Nevada")
            cmbbox_nl_province_load.Items.Add("New Hampshire")
            cmbbox_nl_province_load.Items.Add("New Jersey")
            cmbbox_nl_province_load.Items.Add("New Mexico")
            cmbbox_nl_province_load.Items.Add("New York")
            cmbbox_nl_province_load.Items.Add("North Carolina")
            cmbbox_nl_province_load.Items.Add("North Dakota")
            cmbbox_nl_province_load.Items.Add("Northern Mariana Islands")
            cmbbox_nl_province_load.Items.Add("Ohio")
            cmbbox_nl_province_load.Items.Add("Oklahoma")
            cmbbox_nl_province_load.Items.Add("Oregon")
            cmbbox_nl_province_load.Items.Add("Palau")
            cmbbox_nl_province_load.Items.Add("Pennsylvania")
            cmbbox_nl_province_load.Items.Add("Puerto Rico")
            cmbbox_nl_province_load.Items.Add("Rhode Island")
            cmbbox_nl_province_load.Items.Add("South Carolina")
            cmbbox_nl_province_load.Items.Add("South Dakota")
            cmbbox_nl_province_load.Items.Add("Tennessee")
            cmbbox_nl_province_load.Items.Add("Texas")
            cmbbox_nl_province_load.Items.Add("Utah")
            cmbbox_nl_province_load.Items.Add("Vermont")
            cmbbox_nl_province_load.Items.Add("Virgin Islands")
            cmbbox_nl_province_load.Items.Add("Virginia")
            cmbbox_nl_province_load.Items.Add("Washington")
            cmbbox_nl_province_load.Items.Add("West Virginia")
            cmbbox_nl_province_load.Items.Add("Wisconsin")
            cmbbox_nl_province_load.Items.Add("Wyoming")

        Else
            'clear all items
            cmbbox_nl_province_load.Items.Clear()
        End If
    End Sub

    Private Sub grpbox_nl_network_Enter(sender As Object, e As EventArgs) Handles grpbox_nl_network.Enter

    End Sub

    'AUTOCOMPLETE VARS
    Dim storeNo As Integer = Nothing
    Dim storeFormat As String = Nothing
    Dim env As String = Nothing

    'AUTO COMPLETE: BO HOSTNAME
    Private Sub txtbox_nl_bohostname_load_Click(sender As Object, e As EventArgs) Handles txtbox_nl_bohostname_load.Click

        storeNo = Convert.ToInt32(txtbox_nl_storeno_load.Text)
        storeFormat = String.Format("{0:000}", storeNo)
        env = cmbbox_nl_env_load.SelectedItem

        If Not txtbox_nl_storeno_load.Text = "" And Not cmbbox_nl_env_load.SelectedItem = "" Then
            txtbox_nl_bohostname_load.Text = storeFormat & "-" & env.ToString & "-000"
        Else
            'do nothing
        End If

    End Sub

    'AUTO COMPLETE: REG1 HOSTNAME
    Private Sub txtbox_nl_reg1hostname_load_Click(sender As Object, e As EventArgs) Handles txtbox_nl_reg1hostname_load.Click

        storeNo = Convert.ToInt32(txtbox_nl_storeno_load.Text)
        storeFormat = String.Format("{0:000}", storeNo)
        env = cmbbox_nl_env_load.SelectedItem

        If Not txtbox_nl_storeno_load.Text = "" And Not cmbbox_nl_env_load.SelectedItem = "" Then
            txtbox_nl_reg1hostname_load.Text = storeFormat & "-" & env.ToString & "-001"
        Else
            'do nothing
        End If

    End Sub

    'AUTO COMPLETE: REG2 HOSTNAME
    Private Sub txtbox_nl_reg2hostname_load_Click(sender As Object, e As EventArgs) Handles txtbox_nl_reg2hostname_load.Click

        storeNo = Convert.ToInt32(txtbox_nl_storeno_load.Text)
        storeFormat = String.Format("{0:000}", storeNo)
        env = cmbbox_nl_env_load.SelectedItem

        If Not txtbox_nl_storeno_load.Text = "" And Not cmbbox_nl_env_load.SelectedItem = "" Then
            txtbox_nl_reg2hostname_load.Text = storeFormat & "-" & env.ToString & "-002"
        Else
            'do nothing
        End If

    End Sub

    'number of regs
    Private Sub cmbbox_nl_nbofreg_load_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_nl_nbofreg_load.SelectedIndexChanged

        If cmbbox_nl_nbofreg_load.SelectedItem = "0" Then

            'REG1
            txtbox_nl_reg1hostname_load.Enabled = False
            txtbox_nl_reg1ipaddress_load.Enabled = False
            txtbox_nl_reg1location_load.Enabled = False
            datetimepick_nl_reg1installdate_load.Enabled = False
            txtbox_nl_reg1hostname_load.Text = Nothing
            txtbox_nl_reg1ipaddress_load.Text = Nothing
            txtbox_nl_reg1location_load.Text = Nothing
            reg1date = Nothing
            'PINPAD1
            txtbox_nl_pin1_ip_load.Enabled = False
            txtbox_nl_pin1_location_load.Enabled = False
            datetimepick_nl_pin1_installdate_load.Enabled = False
            txtbox_nl_pin1_ip_load.Text = Nothing
            txtbox_nl_pin1_location_load.Text = Nothing
            pin1date = Nothing
            'PRINTER1
            txtbox_nl_print1_ip_load.Enabled = False
            txtbox_nl_print1_location_load.Enabled = False
            datetimepick_nl_print1_installdate_load.Enabled = False
            txtbox_nl_print1_ip_load.Text = Nothing
            txtbox_nl_print1_location_load.Text = Nothing
            print1date = Nothing

            'REG2
            txtbox_nl_reg2hostname_load.Enabled = False
            txtbox_nl_reg2ipaddress_load.Enabled = False
            txtbox_nl_reg2location_load.Enabled = False
            datetimepick_nl_reg2installdate_load.Enabled = False
            txtbox_nl_reg2hostname_load.Text = Nothing
            txtbox_nl_reg2ipaddress_load.Text = Nothing
            txtbox_nl_reg2location_load.Text = Nothing
            reg2date = Nothing
            'PINPAD2
            txtbox_nl_pin2_ip_load.Enabled = False
            txtbox_nl_pin2_location_load.Enabled = False
            datetimepick_nl_pin2_installdate_load.Enabled = False
            txtbox_nl_pin2_ip_load.Text = Nothing
            txtbox_nl_pin2_location_load.Text = Nothing
            pin2date = Nothing
            'PRINTER2
            txtbox_nl_print2_ip_load.Enabled = False
            txtbox_nl_print2_location_load.Enabled = False
            datetimepick_nl_print2_installdate_load.Enabled = False
            txtbox_nl_print2_ip_load.Text = Nothing
            txtbox_nl_print2_location_load.Text = Nothing
            print2date = Nothing

            nl_star12.Visible = False
            nl_star13.Visible = False
            nl_star14.Visible = False
            nl_star15.Visible = False
            nl_star16.Visible = False
            nl_star17.Visible = False
            nl_star18.Visible = False
            nl_star19.Visible = False
            nl_star25.Visible = False
            nl_star26.Visible = False
            nl_star28.Visible = False
            nl_star29.Visible = False
            nl_star30.Visible = False
            nl_star31.Visible = False
            nl_star32.Visible = False
            nl_star34.Visible = False
            nl_star35.Visible = False

        ElseIf cmbbox_nl_nbofreg_load.SelectedItem = "1" Then

            'REG1
            txtbox_nl_reg1hostname_load.Enabled = True
            txtbox_nl_reg1ipaddress_load.Enabled = True
            txtbox_nl_reg1location_load.Enabled = True
            datetimepick_nl_reg1installdate_load.Enabled = True
            'PINPAD1
            txtbox_nl_pin1_ip_load.Enabled = True
            txtbox_nl_pin1_location_load.Enabled = True
            txtbox_nl_pin1_location_load.Text = "N/A"
            datetimepick_nl_pin1_installdate_load.Enabled = True
            'PRINTER1
            txtbox_nl_print1_ip_load.Enabled = True
            txtbox_nl_print1_location_load.Enabled = True
            txtbox_nl_print1_location_load.Text = "N/A"
            datetimepick_nl_print1_installdate_load.Enabled = True

            'REG2
            txtbox_nl_reg2hostname_load.Enabled = False
            txtbox_nl_reg2ipaddress_load.Enabled = False
            txtbox_nl_reg2location_load.Enabled = False
            datetimepick_nl_reg2installdate_load.Enabled = False
            txtbox_nl_reg2hostname_load.Text = Nothing
            txtbox_nl_reg2ipaddress_load.Text = Nothing
            txtbox_nl_reg2location_load.Text = Nothing
            reg2date = Nothing
            'PINPAD2
            txtbox_nl_pin2_ip_load.Enabled = False
            txtbox_nl_pin2_location_load.Enabled = False
            datetimepick_nl_pin2_installdate_load.Enabled = False
            txtbox_nl_pin2_ip_load.Text = Nothing
            txtbox_nl_pin2_location_load.Text = Nothing
            pin2date = Nothing
            'PRINTER2
            txtbox_nl_print2_ip_load.Enabled = False
            txtbox_nl_print2_location_load.Enabled = False
            datetimepick_nl_print2_installdate_load.Enabled = False
            txtbox_nl_print2_ip_load.Text = Nothing
            txtbox_nl_print2_location_load.Text = Nothing
            print2date = Nothing

            nl_star12.Visible = True
            nl_star13.Visible = True
            nl_star14.Visible = True
            nl_star15.Visible = True
            nl_star16.Visible = False
            nl_star17.Visible = False
            nl_star18.Visible = False
            nl_star19.Visible = False
            nl_star25.Visible = True
            nl_star26.Visible = True
            nl_star28.Visible = True
            nl_star29.Visible = True
            nl_star30.Visible = False
            nl_star31.Visible = False
            nl_star32.Visible = False
            nl_star34.Visible = False
            nl_star35.Visible = False

        ElseIf cmbbox_nl_nbofreg_load.SelectedItem = "2" Then

            'REG1
            txtbox_nl_reg1hostname_load.Enabled = True
            txtbox_nl_reg1ipaddress_load.Enabled = True
            txtbox_nl_reg1location_load.Enabled = True
            datetimepick_nl_reg1installdate_load.Enabled = True
            'PINPAD1
            txtbox_nl_pin1_ip_load.Enabled = True
            txtbox_nl_pin1_location_load.Enabled = True
            txtbox_nl_pin1_location_load.Text = "N/A"
            datetimepick_nl_pin1_installdate_load.Enabled = True
            'PRINTER1
            txtbox_nl_print1_ip_load.Enabled = True
            txtbox_nl_print1_location_load.Enabled = True
            txtbox_nl_print1_location_load.Text = "N/A"
            datetimepick_nl_print1_installdate_load.Enabled = True

            'REG2
            txtbox_nl_reg2hostname_load.Enabled = True
            txtbox_nl_reg2ipaddress_load.Enabled = True
            txtbox_nl_reg2location_load.Enabled = True
            datetimepick_nl_reg2installdate_load.Enabled = True
            'PINPAD2
            txtbox_nl_pin2_ip_load.Enabled = True
            txtbox_nl_pin2_location_load.Enabled = True
            txtbox_nl_pin2_location_load.Text = "N/A"
            datetimepick_nl_pin2_installdate_load.Enabled = True
            'PRINTER2
            txtbox_nl_print2_ip_load.Enabled = True
            txtbox_nl_print2_location_load.Enabled = True
            txtbox_nl_print2_location_load.Text = "N/A"
            datetimepick_nl_print2_installdate_load.Enabled = True

            nl_star12.Visible = True
            nl_star13.Visible = True
            nl_star14.Visible = True
            nl_star15.Visible = True
            nl_star16.Visible = True
            nl_star17.Visible = True
            nl_star18.Visible = True
            nl_star19.Visible = True
            nl_star25.Visible = True
            nl_star26.Visible = True
            nl_star28.Visible = True
            nl_star29.Visible = True
            nl_star30.Visible = True
            nl_star31.Visible = True
            nl_star32.Visible = True
            nl_star34.Visible = True
            nl_star35.Visible = True

        Else

            'REG1
            txtbox_nl_reg1hostname_load.Enabled = False
            txtbox_nl_reg1ipaddress_load.Enabled = False
            txtbox_nl_reg1location_load.Enabled = False
            datetimepick_nl_reg1installdate_load.Enabled = False
            'PINPAD1
            txtbox_nl_pin1_ip_load.Enabled = False
            txtbox_nl_pin1_location_load.Enabled = False
            datetimepick_nl_pin1_installdate_load.Enabled = False
            'PRINTER1
            txtbox_nl_print1_ip_load.Enabled = False
            txtbox_nl_print1_location_load.Enabled = False
            datetimepick_nl_print1_installdate_load.Enabled = False

            'REG2
            txtbox_nl_reg2hostname_load.Enabled = False
            txtbox_nl_reg2ipaddress_load.Enabled = False
            txtbox_nl_reg2location_load.Enabled = False
            datetimepick_nl_reg2installdate_load.Enabled = False
            'PINPAD2
            txtbox_nl_pin2_ip_load.Enabled = False
            txtbox_nl_pin2_location_load.Enabled = False
            datetimepick_nl_pin2_installdate_load.Enabled = False
            'PRINTER2
            txtbox_nl_print2_ip_load.Enabled = False
            txtbox_nl_print2_location_load.Enabled = False
            datetimepick_nl_print2_installdate_load.Enabled = False

            nl_star12.Visible = False
            nl_star13.Visible = False
            nl_star14.Visible = False
            nl_star15.Visible = False
            nl_star16.Visible = False
            nl_star17.Visible = False
            nl_star18.Visible = False
            nl_star19.Visible = False
            nl_star25.Visible = False
            nl_star26.Visible = False
            nl_star28.Visible = False
            nl_star29.Visible = False
            nl_star30.Visible = False
            nl_star31.Visible = False
            nl_star32.Visible = False
            nl_star34.Visible = False
            nl_star35.Visible = False

        End If

    End Sub

    'BUTTON: CLOSE
    Private Sub btn_nl_close_Click(sender As Object, e As EventArgs) Handles btn_nl_close.Click
        Me.Dispose()
    End Sub

    'BUTTON: CLEAR ALL
    Private Sub btn_nl_clear_form_Click(sender As Object, e As EventArgs) Handles btn_nl_clear_form.Click
        clearAll()
    End Sub

    'BUTTON: ADD LAB
    Private Sub btn_nl_add_Click(sender As Object, e As EventArgs) Handles btn_nl_add.Click

        Dim error_fields As String = Nothing    'field errors
        Dim storeno_int As Integer = Nothing    'storeNo as integer
        Dim combo_int As Integer = Nothing      'store combo value (0 or 1)
        Dim sstore_str As String = Nothing      'sister store as string
        Dim ipFormat As String = Nothing        'regEx ip format
        ipFormat = "^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$"
        Dim extrapatch As String = Nothing


        'CHECK FIELDS
        'STORE NO
        If txtbox_nl_storeno_load.Text = "" Then
            error_fields = "Missing store number!" & vbNewLine
            txtbox_nl_storeno_load.Clear()
            txtbox_nl_storeno_load.BackColor = Color.DarkSalmon
        ElseIf Not IsNumeric(txtbox_nl_storeno_load.Text) Then
            error_fields = "Invalid store #. Digits only!" & vbNewLine
            txtbox_nl_storeno_load.Clear()
            txtbox_nl_storeno_load.BackColor = Color.DarkSalmon
        Else
            txtbox_nl_storeno_load.BackColor = Color.White
            error_fields = ""
            storeno_int = Integer.Parse(txtbox_nl_storeno_load.Text)
        End If

        'ENVIRONMENT
        If cmbbox_nl_env_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing environment!" & vbNewLine
            cmbbox_nl_env_load.ResetText()
            cmbbox_nl_env_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_nl_env_load.BackColor = Color.White
        End If

        'STORE SYSTEM
        If cmbbox_nl_store_system.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing SYSTEM!" & vbNewLine
            cmbbox_nl_store_system.ResetText()
            cmbbox_nl_store_system.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_nl_store_system.BackColor = Color.White
        End If

        'BANNER
        If cmbbox_nl_banner_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing store banner!" & vbNewLine
            cmbbox_nl_banner_load.ResetText()
            cmbbox_nl_banner_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_nl_banner_load.BackColor = Color.White
        End If

        'isCOMBO
        If cmbbox_nl_iscombo_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing COMBO status!" & vbNewLine
            cmbbox_nl_iscombo_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_nl_iscombo_load.BackColor = Color.White
        End If

        'SISTER STORE
        'SISTER STORE (if combo sis store mandatory)
        If cmbbox_nl_iscombo_load.SelectedItem = "YES" Then
            If txtbox_nl_sisstore_load.Text = Nothing Then
                error_fields = error_fields & "Combo store selected. Must have a sister store!" & vbNewLine
                txtbox_nl_sisstore_load.BackColor = Color.DarkSalmon
                txtbox_nl_sisstore_load.Clear()
            ElseIf Not IsNumeric(txtbox_nl_sisstore_load.Text) Then
                error_fields = error_fields & "Invalid sister store #. Digits only!" & vbNewLine
                txtbox_nl_sisstore_load.BackColor = Color.DarkSalmon
                txtbox_nl_sisstore_load.Clear()
            ElseIf txtbox_nl_storeno_load.Text = txtbox_nl_sisstore_load.Text Then
                error_fields = error_fields & "Invalid sister store #. Cannot match store number!" & vbNewLine
                txtbox_nl_sisstore_load.Clear()
                txtbox_nl_sisstore_load.BackColor = Color.DarkSalmon
            Else
                txtbox_nl_sisstore_load.BackColor = Color.White
                sstore_str = txtbox_nl_sisstore_load.Text
            End If

        ElseIf cmbbox_nl_iscombo_load.SelectedItem = "NO" Then
            If txtbox_nl_sisstore_load.Text = Nothing Then
                error_fields = error_fields & ""
                txtbox_nl_sisstore_load.BackColor = Color.White
            ElseIf Not IsNumeric(txtbox_nl_sisstore_load.Text) Then
                error_fields = error_fields & "Invalid sister store #. Digits only!" & vbNewLine
                txtbox_nl_sisstore_load.BackColor = Color.DarkSalmon
                txtbox_nl_sisstore_load.Clear()
            ElseIf txtbox_nl_storeno_load.Text = txtbox_nl_sisstore_load.Text Then
                error_fields = error_fields & "Invalid sister store #. Cannot match store number!" & vbNewLine
                txtbox_nl_sisstore_load.Clear()
                txtbox_nl_sisstore_load.BackColor = Color.DarkSalmon
            Else
                'sstore_str = Integer.Parse(txtbox_es_sisstore_load.Text)
                sstore_str = txtbox_nl_sisstore_load.Text
                txtbox_nl_sisstore_load.BackColor = Color.White
            End If

        End If

        'ADDRESS
        If txtbox_nl_address_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: ADDRESS" & vbNewLine
            txtbox_nl_address_load.Clear()
            txtbox_nl_address_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_nl_address_load.BackColor = Color.White
        End If

        'CITY
        If txtbox_nl_city_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: CITY" & vbNewLine
            txtbox_nl_city_load.Clear()
            txtbox_nl_city_load.BackColor = Color.DarkSalmon
        ElseIf Not IsNumeric(txtbox_nl_city_load.Text) Then
            error_fields = error_fields & ""
            txtbox_nl_city_load.BackColor = Color.White
        Else
            error_fields = error_fields & "Invalid city. Characters only!" & vbNewLine
            txtbox_nl_city_load.Clear()
            txtbox_nl_city_load.BackColor = Color.DarkSalmon
        End If

        'COUNTRY
        If cmbbox_nl_country_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Incomplete address: COUNTRY" & vbNewLine
            cmbbox_nl_country_load.ResetText()
            cmbbox_nl_country_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_nl_country_load.BackColor = Color.White
        End If

        'STATE
        If cmbbox_nl_province_load.SelectedItem = "" Then
            error_fields = error_fields & "Incomplete address: PROVINCE/STATE" & vbNewLine
            cmbbox_nl_province_load.ResetText()
            cmbbox_nl_province_load.BackColor = Color.DarkSalmon
        ElseIf IsNumeric(cmbbox_nl_province_load.SelectedItem) Then
            error_fields = error_fields & "Invalid Province/State. Digits only!" & vbNewLine
            cmbbox_nl_province_load.ResetText()
            cmbbox_nl_province_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_nl_province_load.BackColor = Color.White
        End If

        'NB OF REGS
        If cmbbox_nl_nbofreg_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing # of regs!" & vbNewLine
            cmbbox_nl_nbofreg_load.ResetText()
            cmbbox_nl_nbofreg_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_nl_nbofreg_load.BackColor = Color.White
        End If


        'REG1
        If txtbox_nl_reg1hostname_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_reg1hostname_load.Text = "" Then
                error_fields = error_fields & "Missing Reg1 Hostname!" & vbNewLine
                txtbox_nl_reg1hostname_load.Clear()
                txtbox_nl_reg1hostname_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_reg1hostname_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_reg1ipaddress_load.Enabled = False Then
            'do nothing
        Else
            If Regex.IsMatch(txtbox_nl_reg1ipaddress_load.Text, ipFormat) = False Then
                error_fields = error_fields & "Invalid Reg1 IP Address. Follow format *.*.*.*" & vbNewLine
                txtbox_nl_reg1ipaddress_load.Clear()
                txtbox_nl_reg1ipaddress_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_reg1ipaddress_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_reg1location_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_reg1location_load.Text = "" Then
                error_fields = error_fields & "Missing Reg1 Location!" & vbNewLine
                txtbox_nl_reg1location_load.Clear()
                txtbox_nl_reg1location_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_reg1location_load.BackColor = Color.White
            End If
        End If

        If datetimepick_nl_reg1installdate_load.Enabled = False Then
            reg1date = Nothing
        Else
            reg1date = datetimepick_nl_reg1installdate_load.Value
        End If

        'PINPAD1
        If txtbox_nl_pin1_ip_load.Enabled = False Then
            'do nothing 
        Else
            If Not txtbox_nl_pin1_ip_load.Text = "" Then
                If Regex.IsMatch(txtbox_nl_pin1_ip_load.Text, ipFormat) = False Then
                    error_fields = error_fields & "Invalid Pinpad1 IP Address. Follow format *.*.*.*" & vbNewLine
                    txtbox_nl_pin1_ip_load.Clear()
                    txtbox_nl_pin1_ip_load.BackColor = Color.DarkSalmon
                Else
                    error_fields = error_fields & ""
                    txtbox_nl_pin1_ip_load.BackColor = Color.White
                End If
            Else
                error_fields = error_fields & ""
                txtbox_nl_pin1_ip_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_reg1location_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_pin1_location_load.Text = "" Then
                error_fields = error_fields & "Missing Pinpad1 Location!" & vbNewLine
                txtbox_nl_pin1_location_load.Clear()
                txtbox_nl_pin1_location_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_pin1_location_load.BackColor = Color.White
            End If
        End If

        If datetimepick_nl_pin1_installdate_load.Enabled = False Then
            pin1date = Nothing
        Else
            pin1date = datetimepick_nl_pin1_installdate_load.Value
        End If

        'PRINTER1  
        If txtbox_nl_print1_ip_load.Enabled = False Then
            'do nothing 
        Else
            If Not txtbox_nl_print1_ip_load.Text = "" Then
                If Regex.IsMatch(txtbox_nl_print1_ip_load.Text, ipFormat) = False Then
                    error_fields = error_fields & "Invalid Printer1 IP Address. Follow format *.*.*.*" & vbNewLine
                    txtbox_nl_print1_ip_load.Clear()
                    txtbox_nl_print1_ip_load.BackColor = Color.DarkSalmon
                Else
                    error_fields = error_fields & ""
                    txtbox_nl_print1_ip_load.BackColor = Color.White
                End If
            Else
                error_fields = error_fields & ""
                txtbox_nl_print1_ip_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_print1_location_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_print1_location_load.Text = "" Then
                error_fields = error_fields & "Missing Printer1 Location!" & vbNewLine
                txtbox_nl_print1_location_load.Clear()
                txtbox_nl_print1_location_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_print1_location_load.BackColor = Color.White
            End If
        End If

        If datetimepick_nl_print1_installdate_load.Enabled = False Then
            print1date = Nothing
        Else
            print1date = datetimepick_nl_print1_installdate_load.Value
        End If


        'REG2
        If txtbox_nl_reg2hostname_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_reg2hostname_load.Text = "" Then
                error_fields = error_fields & "Missing Reg2 Hostname!" & vbNewLine
                txtbox_nl_reg2hostname_load.Clear()
                txtbox_nl_reg2hostname_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_reg2hostname_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_reg2ipaddress_load.Enabled = False Then
            'do nothing
        Else
            If Regex.IsMatch(txtbox_nl_reg2ipaddress_load.Text, ipFormat) = False Then
                error_fields = error_fields & "Invalid Reg2 IP Address. Follow format *.*.*.*" & vbNewLine
                txtbox_nl_reg2ipaddress_load.Clear()
                txtbox_nl_reg2ipaddress_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_reg2ipaddress_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_reg2location_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_reg2location_load.Text = "" Then
                error_fields = error_fields & "Missing Reg2 Location!" & vbNewLine
                txtbox_nl_reg2location_load.Clear()
                txtbox_nl_reg2location_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_reg2location_load.BackColor = Color.White
            End If
        End If

        If datetimepick_nl_reg2installdate_load.Enabled = False Then
            reg2date = Nothing
        Else
            reg2date = datetimepick_nl_reg2installdate_load.Value
        End If

        'PINPAD2
        If txtbox_nl_pin2_ip_load.Enabled = False Then
            'do nothing 
        Else
            If Not txtbox_nl_pin2_ip_load.Text = "" Then
                If Regex.IsMatch(txtbox_nl_pin2_ip_load.Text, ipFormat) = False Then
                    error_fields = error_fields & "Invalid Pinpad2 IP Address. Follow format *.*.*.*" & vbNewLine
                    txtbox_nl_pin2_ip_load.Clear()
                    txtbox_nl_pin2_ip_load.BackColor = Color.DarkSalmon
                Else
                    error_fields = error_fields & ""
                    txtbox_nl_pin2_ip_load.BackColor = Color.White
                End If
            Else
                error_fields = error_fields & ""
                txtbox_nl_pin2_ip_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_pin2_location_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_pin2_location_load.Text = "" Then
                error_fields = error_fields & "Missing Pinpad2 Location!" & vbNewLine
                txtbox_nl_pin2_location_load.Clear()
                txtbox_nl_pin2_location_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_pin2_location_load.BackColor = Color.White
            End If
        End If

        If datetimepick_nl_pin2_installdate_load.Enabled = False Then
            pin2date = Nothing
        Else
            pin2date = datetimepick_nl_pin2_installdate_load.Value
        End If

        'PRINTER2  
        If txtbox_nl_print2_ip_load.Enabled = False Then
            'do nothing 
        Else
            If Not txtbox_nl_print2_ip_load.Text = "" Then
                If Regex.IsMatch(txtbox_nl_print2_ip_load.Text, ipFormat) = False Then
                    error_fields = error_fields & "Invalid Printer2 IP Address. Follow format *.*.*.*" & vbNewLine
                    txtbox_nl_print2_ip_load.Clear()
                    txtbox_nl_print2_ip_load.BackColor = Color.DarkSalmon
                Else
                    error_fields = error_fields & ""
                    txtbox_nl_print2_ip_load.BackColor = Color.White
                End If
            Else
                error_fields = error_fields & ""
                txtbox_nl_print2_ip_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_print2_location_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_print2_location_load.Text = "" Then
                error_fields = error_fields & "Missing Printer2 Location!" & vbNewLine
                txtbox_nl_print2_location_load.Clear()
                txtbox_nl_print2_location_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_print2_location_load.BackColor = Color.White
            End If
        End If

        If datetimepick_nl_print2_installdate_load.Enabled = False Then
            print2date = Nothing
        Else
            print2date = datetimepick_nl_print2_installdate_load.Value
        End If

        'BO   
        If txtbox_nl_bohostname_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_bohostname_load.Text = "" Then
                error_fields = error_fields & "Missing BO Hostname!" & vbNewLine
                txtbox_nl_bohostname_load.Clear()
                txtbox_nl_bohostname_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_bohostname_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_boipaddress_load.Enabled = False Then
            'do nothing
        Else
            If Regex.IsMatch(txtbox_nl_boipaddress_load.Text, ipFormat) = False Then
                error_fields = error_fields & "Invalid BO IP Address. Follow format *.*.*.*" & vbNewLine
                txtbox_nl_boipaddress_load.Clear()
                txtbox_nl_boipaddress_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_boipaddress_load.BackColor = Color.White
            End If
        End If

        If txtbox_nl_bolocation_load.Enabled = False Then
            'do nothing
        Else
            If txtbox_nl_bolocation_load.Text = "" Then
                error_fields = error_fields & "Missing BO Location!" & vbNewLine
                txtbox_nl_bolocation_load.Clear()
                txtbox_nl_bolocation_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_nl_bolocation_load.BackColor = Color.White
            End If
        End If

        If datetimepick_nl_boinstalldate_load.Enabled = False Then
            bodate = Nothing
        Else
            bodate = datetimepick_nl_boinstalldate_load.Value
        End If

        'project
        If cmbbox_nl_status_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing LAB status!" & vbNewLine
            cmbbox_nl_status_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_nl_status_load.BackColor = Color.White
        End If

        If txtbox_nl_project_load.Text = Nothing Then
            error_fields = error_fields & "Missing Project name!" & vbNewLine
            txtbox_nl_project_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_nl_project_load.BackColor = Color.White
        End If

        If txtbox_nl_pm_load.Text = Nothing Then
            error_fields = error_fields & "Missing PM!" & vbNewLine
            txtbox_nl_pm_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_nl_pm_load.BackColor = Color.White
        End If

        If txtbox_nl_epatch_load.Text = "use ,(comma) as separator" Then
            extrapatch = Nothing
        Else
            extrapatch = txtbox_nl_epatch_load.Text
        End If

        If cmbbox_nl_iscombo_load.SelectedItem = "YES" Then
            combo_int = 1
        Else
            combo_int = 0
        End If

        'FINAL CHECK & ADD LAB
        If error_fields = "" Then

            'set pinpad1 & printer1 location & IP to null if N/A
            If txtbox_nl_pin1_location_load.Text = "N/A" Or txtbox_nl_pin1_location_load.Text = "n/a" Then
                txtbox_nl_pin1_location_load.Text = Nothing
                txtbox_nl_pin1_ip_load.Text = Nothing
            End If
            If txtbox_nl_print1_location_load.Text = "N/A" Or txtbox_nl_print1_location_load.Text = "n/a" Then
                txtbox_nl_print1_location_load.Text = Nothing
                txtbox_nl_print1_ip_load.Text = Nothing
            End If

            'set pinpad2 & printer2 location & IP to null if N/A
            If txtbox_nl_pin2_location_load.Text = "N/A" Or txtbox_nl_pin2_location_load.Text = "n/a" Then
                txtbox_nl_pin2_location_load.Text = Nothing
                txtbox_nl_pin2_ip_load.Text = Nothing
            End If
            If txtbox_nl_print2_location_load.Text = "N/A" Or txtbox_nl_print2_location_load.Text = "n/a" Then
                txtbox_nl_print2_location_load.Text = Nothing
                txtbox_nl_print2_ip_load.Text = Nothing
            End If

            addLab(storeno_int, cmbbox_nl_env_load.SelectedItem, cmbbox_nl_store_system.SelectedItem, cmbbox_nl_banner_load.SelectedItem, combo_int, txtbox_nl_sisstore_load.Text,
                   txtbox_nl_address_load.Text, txtbox_nl_city_load.Text, cmbbox_nl_country_load.SelectedItem, cmbbox_nl_province_load.SelectedItem, txtbox_nl_bohostname_load.Text,
                   txtbox_nl_bolocation_load.Text, txtbox_nl_boipaddress_load.Text, bodate, txtbox_nl_reg1hostname_load.Text, txtbox_nl_reg1location_load.Text,
                   txtbox_nl_reg1ipaddress_load.Text, reg1date, txtbox_nl_pin1_ip_load.Text, txtbox_nl_pin1_location_load.Text, pin1date, txtbox_nl_print1_ip_load.Text,
                   txtbox_nl_print1_location_load.Text, print1date, txtbox_nl_reg2hostname_load.Text, txtbox_nl_reg2location_load.Text, txtbox_nl_reg2ipaddress_load.Text,
                   reg2date, txtbox_nl_pin2_ip_load.Text, txtbox_nl_pin2_location_load.Text, pin2date, txtbox_nl_print2_ip_load.Text, txtbox_nl_print2_location_load.Text, print2date,
                   cmbbox_nl_status_load.SelectedItem, txtbox_nl_project_load.Text, txtbox_nl_pm_load.Text, extrapatch, txtbox_labs_comments_load.Text)

            Else
                MsgBox("Please correct the following errors before adding the new store:" & vbNewLine & vbNewLine & error_fields)
        End If

    End Sub

    'add new lab
    Private Sub addLab(STORE As Integer, ENV As String, SYSTEM As String, BANNER As String, COMBO As Integer, SIS_STORE As String, ADDRESS As String, CITY As String,
                       COUNTRY As String, PROVINCE As String, BO_HOST As String, BO_LOCATION As String, BO_IP As String, BO_DATE As String, R1_HOST As String,
                       R1_LOCATION As String, R1_IP As String, R1_DATE As String, PIN1_IP As String, PIN1_LOCATION As String, PIN1_DATE As String, PRINT1_IP As String,
                       PRINT1_LOCATION As String, PRINT1_DATE As String, R2_HOST As String, R2_LOCATION As String, R2_IP As String, R2_DATE As String, PIN2_IP As String,
                       PIN2_LOCATION As String, PIN2_DATE As String, PRINT2_IP As String, PRINT2_LOCATION As String, PRINT2_DATE As String, STATUS As String,
                       PROJECT As String, PM As String, PATCH As String, COMMENTS As String)

        'FOR TESTING
        '     MsgBox(STORE & " | " & ENV & " | " & SYSTEM & " | " & BANNER & " | " & COMBO & " | " & SIS_STORE & " | " & ADDRESS & " | " & CITY & " | " & vbNewLine &
        '     COUNTRY & " | " & PROVINCE & " | " & BO_HOST & " | " & BO_LOCATION & " | " & BO_IP & " | " & BO_DATE & " | " & R1_HOST & " | " & vbNewLine &
        '     R1_LOCATION & " | " & R1_IP & " | " & R1_DATE & " | " & PIN1_IP & " | " & PIN1_LOCATION & " | " & PIN1_DATE & " | " & PRINT1_IP & " | " & vbNewLine &
        '     PRINT1_LOCATION & " | " & PRINT1_DATE & " | " & R2_HOST & " | " & R2_LOCATION & " | " & R2_IP & " | " & R2_DATE & " | " & PIN2_IP & " | " & vbNewLine &
        '     PIN2_LOCATION & " | " & PIN2_DATE & " | " & PRINT2_IP & " | " & PRINT2_LOCATION & " | " & PRINT2_DATE & " | " & STATUS & " | " & vbNewLine &
        '     PROJECT & " | " & PM & " | " & PATCH & " | " & COMMENTS)

        'VARS
        Dim lab_connection As SqlConnection
        Dim lab_connection_check As SqlConnection
        Dim cmd_lab As SqlCommand
        Dim cmd_select As SqlCommand
        Dim labReader As SqlDataReader
        Dim lab_exists As Boolean = False

        Dim sql_lab_add As String = "INSERT INTO labs_info (Store, Environment, System, Banner, COMBO, [Sister Store], Address, City, Province, Country, [BO Hostname], 
        			   [BO Location], [BO IPaddress], [BO Install Date], [Reg1 Hostname], [Reg1 Location], [Reg1 IPaddress], [Reg1 Install Date], [Pinpad1 IPaddress], 
                       [Pinpad1 Location], [Pinpad1 Install Date], [Printer1 IPaddress], [Printer1 Location], [Printer1 Install Date], [Reg2 Hostname], [Reg2 Location], 
                       [Reg2 IPaddress], [Reg2 Install Date], [Pinpad2 IPaddress], [Pinpad2 Location], [Pinpad2 Install Date], [Printer2 IPaddress], [Printer2 Location], 
                       [Printer2 Install Date], Status, Project, PM, [Extra patches], Comments) 
                       VALUES(" & STORE & ", '" & ENV & "', '" & SYSTEM & "', '" & BANNER & "', " & COMBO & ", 
                       " & IIf(String.IsNullOrEmpty(SIS_STORE), "null", SIS_STORE) & ",
                       '" & ADDRESS & "', '" & CITY & "', '" & PROVINCE & "', '" & COUNTRY & "', '" & BO_HOST & "', '" & BO_LOCATION & "', '" & BO_IP & "', '" & BO_DATE & "',
                       " & IIf(String.IsNullOrEmpty(R1_HOST), "null", "'" & R1_HOST & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(R1_LOCATION), "null", "'" & R1_LOCATION & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(R1_IP), "null", "'" & R1_IP & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(R1_DATE), "null", "'" & R1_DATE & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(PIN1_IP), "null", "'" & PIN1_IP & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(PIN1_LOCATION), "null", "'" & PIN1_LOCATION & "'") & ",
                       " & IIf(String.IsNullOrEmpty(PIN1_DATE), "null", "'" & PIN1_DATE & "'") & ",
                       " & IIf(String.IsNullOrEmpty(PRINT1_IP), "null", "'" & PRINT1_IP & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(PRINT1_LOCATION), "null", "'" & PRINT1_LOCATION & "'") & ",
                       " & IIf(String.IsNullOrEmpty(PRINT1_DATE), "null", "'" & PRINT1_DATE & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(R2_HOST), "null", "'" & R2_HOST & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(R2_LOCATION), "null", "'" & R2_LOCATION & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(R2_IP), "null", "'" & R2_IP & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(R2_DATE), "null", "'" & R2_DATE & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(PIN2_IP), "null", "'" & PIN2_IP & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(PIN2_LOCATION), "null", "'" & PIN2_LOCATION & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(PIN2_DATE), "null", "'" & PIN2_DATE & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(PRINT2_IP), "null", "'" & PRINT2_IP & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(PRINT2_LOCATION), "null", "'" & PRINT2_LOCATION & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(PRINT2_DATE), "null", "'" & PRINT2_DATE & "'") & ", 
                       '" & STATUS & "', '" & PROJECT & "', '" & PM & "', 
                       " & IIf(String.IsNullOrEmpty(PATCH), "null", "'" & PATCH & "'") & ", 
                       " & IIf(String.IsNullOrEmpty(COMMENTS), "null", "'" & COMMENTS & "'") & ")"

        Dim sql_check_lab As String = "Select * from labs_info where (Store = " & STORE & ")"

        'CONNECTION PARA
        lab_connection_check = New SqlConnection(gdx_main.connectionString)
        cmd_select = New SqlCommand(sql_check_lab, lab_connection_check)
        lab_connection = New SqlConnection(gdx_main.connectionString)
        cmd_lab = New SqlCommand(sql_lab_add, lab_connection)


        Try
            lab_connection_check.Open()
            'OPEN ADD CONNECTION

            labReader = cmd_select.ExecuteReader()
            'If lab already exists
            If labReader.Read() = True Then
                lab_exists = True
            Else
                lab_connection.Open()
                cmd_lab.ExecuteNonQuery()
                lab_exists = False
            End If

            lab_connection_check.Close()

            'return result
            If lab_exists = True Then
                MsgBox("LAB already exists! Please check Store No.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (lab exists)")
                'clear store no
                txtbox_nl_storeno_load.Text = Nothing
                txtbox_nl_storeno_load.BackColor = Color.DarkSalmon
            Else
                MsgBox("LAB added. Please refresh LAB list.", MsgBoxStyle.Information, Title:="GDnetworks - Info! [add labs]")
                Labs.btn_labs_reload.PerformClick()
                clearAll()
            End If

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [add labs]")
        End Try

        'CLOSE CONNECTIONS
        lab_connection.Close()

    End Sub


    Private Sub TextBox1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtbox_nl_epatch_load.Click
        txtbox_nl_epatch_load.Text = ""
        txtbox_nl_epatch_load.ForeColor = Color.Black
    End Sub


End Class

