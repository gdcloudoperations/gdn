﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class admin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(admin))
        Me.listview_users = New System.Windows.Forms.ListView()
        Me.col_id = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_user = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_role = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_status = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_ip = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_connection = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_version = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_platform = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_created_on = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_last_login = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_browser = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_remote = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_ref_rate = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_form_loc = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_auto_update = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btn_admin_adduser = New System.Windows.Forms.Button()
        Me.btn_admin_edituser = New System.Windows.Forms.Button()
        Me.btn_admin_deleteuser = New System.Windows.Forms.Button()
        Me.btn_admin_refresh = New System.Windows.Forms.Button()
        Me.lbl_admin_version = New System.Windows.Forms.Label()
        Me.lbl_master_version_load = New System.Windows.Forms.Label()
        Me.lbl_admin_date = New System.Windows.Forms.Label()
        Me.lbl_admin_user = New System.Windows.Forms.Label()
        Me.lbl_admin_usramount = New System.Windows.Forms.Label()
        Me.lbl_admin_enabled = New System.Windows.Forms.Label()
        Me.lbl_admin_disabled = New System.Windows.Forms.Label()
        Me.lbl_admin_online = New System.Windows.Forms.Label()
        Me.lbl_admin_offline = New System.Windows.Forms.Label()
        Me.lbl_admin_autoupdate = New System.Windows.Forms.Label()
        Me.grpbox_admin_user = New System.Windows.Forms.GroupBox()
        Me.lbl_admin_auto_load = New System.Windows.Forms.Label()
        Me.lbl_admin_offline_load = New System.Windows.Forms.Label()
        Me.lbl_admin_online_load = New System.Windows.Forms.Label()
        Me.lbl_admin_disabled_load = New System.Windows.Forms.Label()
        Me.lbl_admin_enabled_load = New System.Windows.Forms.Label()
        Me.lbl_admin_user_nb_load = New System.Windows.Forms.Label()
        Me.lbl_admin_user_load = New System.Windows.Forms.Label()
        Me.lbl_admin_date_load = New System.Windows.Forms.Label()
        Me.grpbox_admin_usrlist = New System.Windows.Forms.GroupBox()
        Me.grpbox_admin_usrcount = New System.Windows.Forms.GroupBox()
        Me.lbl_admin_sm_load = New System.Windows.Forms.Label()
        Me.lbl_admin_sm = New System.Windows.Forms.Label()
        Me.lbl_admin_sam_load = New System.Windows.Forms.Label()
        Me.lbl_admin_techsup_load = New System.Windows.Forms.Label()
        Me.lbl_admin_techsupport_load = New System.Windows.Forms.Label()
        Me.lbl_admin_orpos_load = New System.Windows.Forms.Label()
        Me.lbl_admin_hosupport_load = New System.Windows.Forms.Label()
        Me.lbl_admin_dev_load = New System.Windows.Forms.Label()
        Me.lbl_admin_dba_load = New System.Windows.Forms.Label()
        Me.lbl_admin_admin_load = New System.Windows.Forms.Label()
        Me.lbl_admin_admin = New System.Windows.Forms.Label()
        Me.lbl_admin_dba = New System.Windows.Forms.Label()
        Me.lbl_admin_sam = New System.Windows.Forms.Label()
        Me.lbl_admin_dev = New System.Windows.Forms.Label()
        Me.lbl_admin_techsup = New System.Windows.Forms.Label()
        Me.lbl_admin_hosupport = New System.Windows.Forms.Label()
        Me.lbl_admin_techsupport = New System.Windows.Forms.Label()
        Me.lbl_admin_orpos = New System.Windows.Forms.Label()
        Me.lbl_admin_guest_load = New System.Windows.Forms.Label()
        Me.lbl_admin_guest = New System.Windows.Forms.Label()
        Me.grpbox_admin_user.SuspendLayout()
        Me.grpbox_admin_usrlist.SuspendLayout()
        Me.grpbox_admin_usrcount.SuspendLayout()
        Me.SuspendLayout()
        '
        'listview_users
        '
        Me.listview_users.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.col_id, Me.col_user, Me.col_role, Me.col_status, Me.col_ip, Me.col_connection, Me.col_version, Me.col_platform, Me.col_created_on, Me.col_last_login, Me.col_browser, Me.col_remote, Me.col_ref_rate, Me.col_form_loc, Me.col_auto_update})
        Me.listview_users.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listview_users.FullRowSelect = True
        Me.listview_users.HideSelection = False
        Me.listview_users.Location = New System.Drawing.Point(16, 26)
        Me.listview_users.MultiSelect = False
        Me.listview_users.Name = "listview_users"
        Me.listview_users.Size = New System.Drawing.Size(641, 262)
        Me.listview_users.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.listview_users.TabIndex = 2
        Me.listview_users.UseCompatibleStateImageBehavior = False
        Me.listview_users.View = System.Windows.Forms.View.Details
        '
        'col_id
        '
        Me.col_id.Text = "ID"
        Me.col_id.Width = 44
        '
        'col_user
        '
        Me.col_user.Text = "Username"
        Me.col_user.Width = 70
        '
        'col_role
        '
        Me.col_role.Text = "Role"
        Me.col_role.Width = 70
        '
        'col_status
        '
        Me.col_status.Text = "Status"
        '
        'col_ip
        '
        Me.col_ip.Text = "IP Address"
        Me.col_ip.Width = 78
        '
        'col_connection
        '
        Me.col_connection.Text = "GDn connection"
        Me.col_connection.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.col_connection.Width = 99
        '
        'col_version
        '
        Me.col_version.Text = "GD Version"
        '
        'col_platform
        '
        Me.col_platform.Text = "OS Platform"
        '
        'col_created_on
        '
        Me.col_created_on.Text = "Created On"
        Me.col_created_on.Width = 74
        '
        'col_last_login
        '
        Me.col_last_login.Text = "Last Login"
        Me.col_last_login.Width = 69
        '
        'col_browser
        '
        Me.col_browser.Text = "Browser"
        Me.col_browser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'col_remote
        '
        Me.col_remote.Text = "Remote APP"
        Me.col_remote.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'col_ref_rate
        '
        Me.col_ref_rate.Text = "Refresh Rate"
        Me.col_ref_rate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'col_form_loc
        '
        Me.col_form_loc.Text = "OnScreen Location"
        Me.col_form_loc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'col_auto_update
        '
        Me.col_auto_update.Text = "AutoUpdate"
        '
        'btn_admin_adduser
        '
        Me.btn_admin_adduser.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_admin_adduser.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_admin_adduser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_admin_adduser.Location = New System.Drawing.Point(819, 292)
        Me.btn_admin_adduser.Name = "btn_admin_adduser"
        Me.btn_admin_adduser.Size = New System.Drawing.Size(95, 21)
        Me.btn_admin_adduser.TabIndex = 14
        Me.btn_admin_adduser.Text = "Add User"
        Me.btn_admin_adduser.UseVisualStyleBackColor = False
        '
        'btn_admin_edituser
        '
        Me.btn_admin_edituser.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_admin_edituser.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_admin_edituser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_admin_edituser.Location = New System.Drawing.Point(920, 292)
        Me.btn_admin_edituser.Name = "btn_admin_edituser"
        Me.btn_admin_edituser.Size = New System.Drawing.Size(95, 21)
        Me.btn_admin_edituser.TabIndex = 15
        Me.btn_admin_edituser.Text = "Edit User"
        Me.btn_admin_edituser.UseVisualStyleBackColor = False
        '
        'btn_admin_deleteuser
        '
        Me.btn_admin_deleteuser.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_admin_deleteuser.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_admin_deleteuser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_admin_deleteuser.Location = New System.Drawing.Point(1021, 292)
        Me.btn_admin_deleteuser.Name = "btn_admin_deleteuser"
        Me.btn_admin_deleteuser.Size = New System.Drawing.Size(95, 21)
        Me.btn_admin_deleteuser.TabIndex = 16
        Me.btn_admin_deleteuser.Text = "Delete User"
        Me.btn_admin_deleteuser.UseVisualStyleBackColor = False
        '
        'btn_admin_refresh
        '
        Me.btn_admin_refresh.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_admin_refresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_admin_refresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_admin_refresh.Location = New System.Drawing.Point(718, 292)
        Me.btn_admin_refresh.Name = "btn_admin_refresh"
        Me.btn_admin_refresh.Size = New System.Drawing.Size(95, 21)
        Me.btn_admin_refresh.TabIndex = 17
        Me.btn_admin_refresh.Text = "Refresh"
        Me.btn_admin_refresh.UseVisualStyleBackColor = False
        '
        'lbl_admin_version
        '
        Me.lbl_admin_version.AutoSize = True
        Me.lbl_admin_version.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_version.Location = New System.Drawing.Point(15, 51)
        Me.lbl_admin_version.Name = "lbl_admin_version"
        Me.lbl_admin_version.Size = New System.Drawing.Size(80, 15)
        Me.lbl_admin_version.TabIndex = 18
        Me.lbl_admin_version.Text = "Master Ver:"
        '
        'lbl_master_version_load
        '
        Me.lbl_master_version_load.AutoSize = True
        Me.lbl_master_version_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_master_version_load.Location = New System.Drawing.Point(100, 53)
        Me.lbl_master_version_load.Name = "lbl_master_version_load"
        Me.lbl_master_version_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_master_version_load.Size = New System.Drawing.Size(113, 13)
        Me.lbl_master_version_load.TabIndex = 19
        Me.lbl_master_version_load.Text = "gd_master_version"
        '
        'lbl_admin_date
        '
        Me.lbl_admin_date.AutoSize = True
        Me.lbl_admin_date.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_date.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_date.Location = New System.Drawing.Point(15, 26)
        Me.lbl_admin_date.Name = "lbl_admin_date"
        Me.lbl_admin_date.Size = New System.Drawing.Size(41, 15)
        Me.lbl_admin_date.TabIndex = 111
        Me.lbl_admin_date.Text = "Date:"
        '
        'lbl_admin_user
        '
        Me.lbl_admin_user.AutoSize = True
        Me.lbl_admin_user.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_user.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_user.Location = New System.Drawing.Point(15, 99)
        Me.lbl_admin_user.Name = "lbl_admin_user"
        Me.lbl_admin_user.Size = New System.Drawing.Size(41, 15)
        Me.lbl_admin_user.TabIndex = 113
        Me.lbl_admin_user.Text = "User:"
        '
        'lbl_admin_usramount
        '
        Me.lbl_admin_usramount.AutoSize = True
        Me.lbl_admin_usramount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_usramount.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_usramount.Location = New System.Drawing.Point(15, 123)
        Me.lbl_admin_usramount.Name = "lbl_admin_usramount"
        Me.lbl_admin_usramount.Size = New System.Drawing.Size(48, 15)
        Me.lbl_admin_usramount.TabIndex = 114
        Me.lbl_admin_usramount.Text = "Users:"
        '
        'lbl_admin_enabled
        '
        Me.lbl_admin_enabled.AutoSize = True
        Me.lbl_admin_enabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_enabled.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_enabled.Location = New System.Drawing.Point(15, 148)
        Me.lbl_admin_enabled.Name = "lbl_admin_enabled"
        Me.lbl_admin_enabled.Size = New System.Drawing.Size(64, 15)
        Me.lbl_admin_enabled.TabIndex = 115
        Me.lbl_admin_enabled.Text = "Enabled:"
        '
        'lbl_admin_disabled
        '
        Me.lbl_admin_disabled.AutoSize = True
        Me.lbl_admin_disabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_disabled.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lbl_admin_disabled.Location = New System.Drawing.Point(15, 173)
        Me.lbl_admin_disabled.Name = "lbl_admin_disabled"
        Me.lbl_admin_disabled.Size = New System.Drawing.Size(68, 15)
        Me.lbl_admin_disabled.TabIndex = 116
        Me.lbl_admin_disabled.Text = "Disabled:"
        '
        'lbl_admin_online
        '
        Me.lbl_admin_online.AutoSize = True
        Me.lbl_admin_online.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_online.ForeColor = System.Drawing.Color.Green
        Me.lbl_admin_online.Location = New System.Drawing.Point(15, 198)
        Me.lbl_admin_online.Name = "lbl_admin_online"
        Me.lbl_admin_online.Size = New System.Drawing.Size(53, 15)
        Me.lbl_admin_online.TabIndex = 117
        Me.lbl_admin_online.Text = "Online:"
        '
        'lbl_admin_offline
        '
        Me.lbl_admin_offline.AutoSize = True
        Me.lbl_admin_offline.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_offline.ForeColor = System.Drawing.Color.Red
        Me.lbl_admin_offline.Location = New System.Drawing.Point(15, 223)
        Me.lbl_admin_offline.Name = "lbl_admin_offline"
        Me.lbl_admin_offline.Size = New System.Drawing.Size(53, 15)
        Me.lbl_admin_offline.TabIndex = 118
        Me.lbl_admin_offline.Text = "Offline:"
        '
        'lbl_admin_autoupdate
        '
        Me.lbl_admin_autoupdate.AutoSize = True
        Me.lbl_admin_autoupdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_autoupdate.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_autoupdate.Location = New System.Drawing.Point(15, 248)
        Me.lbl_admin_autoupdate.Name = "lbl_admin_autoupdate"
        Me.lbl_admin_autoupdate.Size = New System.Drawing.Size(88, 15)
        Me.lbl_admin_autoupdate.TabIndex = 119
        Me.lbl_admin_autoupdate.Text = "Auto-update:"
        '
        'grpbox_admin_user
        '
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_auto_load)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_offline_load)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_online_load)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_master_version_load)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_disabled_load)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_version)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_enabled_load)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_user_nb_load)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_user_load)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_date_load)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_date)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_user)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_autoupdate)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_usramount)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_offline)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_enabled)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_online)
        Me.grpbox_admin_user.Controls.Add(Me.lbl_admin_disabled)
        Me.grpbox_admin_user.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_admin_user.ForeColor = System.Drawing.Color.RoyalBlue
        Me.grpbox_admin_user.Location = New System.Drawing.Point(693, 12)
        Me.grpbox_admin_user.Name = "grpbox_admin_user"
        Me.grpbox_admin_user.Size = New System.Drawing.Size(243, 270)
        Me.grpbox_admin_user.TabIndex = 120
        Me.grpbox_admin_user.TabStop = False
        Me.grpbox_admin_user.Text = "USER INFO"
        '
        'lbl_admin_auto_load
        '
        Me.lbl_admin_auto_load.AutoSize = True
        Me.lbl_admin_auto_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_auto_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_auto_load.Location = New System.Drawing.Point(100, 250)
        Me.lbl_admin_auto_load.Name = "lbl_admin_auto_load"
        Me.lbl_admin_auto_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_auto_load.Size = New System.Drawing.Size(63, 13)
        Me.lbl_admin_auto_load.TabIndex = 129
        Me.lbl_admin_auto_load.Text = "auto_load"
        '
        'lbl_admin_offline_load
        '
        Me.lbl_admin_offline_load.AutoSize = True
        Me.lbl_admin_offline_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_offline_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_offline_load.Location = New System.Drawing.Point(100, 225)
        Me.lbl_admin_offline_load.Name = "lbl_admin_offline_load"
        Me.lbl_admin_offline_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_offline_load.Size = New System.Drawing.Size(73, 13)
        Me.lbl_admin_offline_load.TabIndex = 128
        Me.lbl_admin_offline_load.Text = "offline_load"
        '
        'lbl_admin_online_load
        '
        Me.lbl_admin_online_load.AutoSize = True
        Me.lbl_admin_online_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_online_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_online_load.Location = New System.Drawing.Point(100, 200)
        Me.lbl_admin_online_load.Name = "lbl_admin_online_load"
        Me.lbl_admin_online_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_online_load.Size = New System.Drawing.Size(72, 13)
        Me.lbl_admin_online_load.TabIndex = 127
        Me.lbl_admin_online_load.Text = "online_load"
        '
        'lbl_admin_disabled_load
        '
        Me.lbl_admin_disabled_load.AutoSize = True
        Me.lbl_admin_disabled_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_disabled_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_disabled_load.Location = New System.Drawing.Point(100, 175)
        Me.lbl_admin_disabled_load.Name = "lbl_admin_disabled_load"
        Me.lbl_admin_disabled_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_disabled_load.Size = New System.Drawing.Size(85, 13)
        Me.lbl_admin_disabled_load.TabIndex = 126
        Me.lbl_admin_disabled_load.Text = "disabled_load"
        '
        'lbl_admin_enabled_load
        '
        Me.lbl_admin_enabled_load.AutoSize = True
        Me.lbl_admin_enabled_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_enabled_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_enabled_load.Location = New System.Drawing.Point(100, 150)
        Me.lbl_admin_enabled_load.Name = "lbl_admin_enabled_load"
        Me.lbl_admin_enabled_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_enabled_load.Size = New System.Drawing.Size(83, 13)
        Me.lbl_admin_enabled_load.TabIndex = 125
        Me.lbl_admin_enabled_load.Text = "enabled_load"
        '
        'lbl_admin_user_nb_load
        '
        Me.lbl_admin_user_nb_load.AutoSize = True
        Me.lbl_admin_user_nb_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_user_nb_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_user_nb_load.Location = New System.Drawing.Point(100, 125)
        Me.lbl_admin_user_nb_load.Name = "lbl_admin_user_nb_load"
        Me.lbl_admin_user_nb_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_user_nb_load.Size = New System.Drawing.Size(83, 13)
        Me.lbl_admin_user_nb_load.TabIndex = 124
        Me.lbl_admin_user_nb_load.Text = "user_nb_load"
        '
        'lbl_admin_user_load
        '
        Me.lbl_admin_user_load.AutoSize = True
        Me.lbl_admin_user_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_user_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_user_load.Location = New System.Drawing.Point(100, 101)
        Me.lbl_admin_user_load.Name = "lbl_admin_user_load"
        Me.lbl_admin_user_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_user_load.Size = New System.Drawing.Size(62, 13)
        Me.lbl_admin_user_load.TabIndex = 123
        Me.lbl_admin_user_load.Text = "user_load"
        '
        'lbl_admin_date_load
        '
        Me.lbl_admin_date_load.AutoSize = True
        Me.lbl_admin_date_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_date_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_date_load.Location = New System.Drawing.Point(100, 28)
        Me.lbl_admin_date_load.Name = "lbl_admin_date_load"
        Me.lbl_admin_date_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_date_load.Size = New System.Drawing.Size(63, 13)
        Me.lbl_admin_date_load.TabIndex = 122
        Me.lbl_admin_date_load.Text = "date_load"
        '
        'grpbox_admin_usrlist
        '
        Me.grpbox_admin_usrlist.Controls.Add(Me.listview_users)
        Me.grpbox_admin_usrlist.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_admin_usrlist.ForeColor = System.Drawing.Color.RoyalBlue
        Me.grpbox_admin_usrlist.Location = New System.Drawing.Point(15, 12)
        Me.grpbox_admin_usrlist.Name = "grpbox_admin_usrlist"
        Me.grpbox_admin_usrlist.Size = New System.Drawing.Size(672, 301)
        Me.grpbox_admin_usrlist.TabIndex = 121
        Me.grpbox_admin_usrlist.TabStop = False
        Me.grpbox_admin_usrlist.Text = "USER LIST"
        '
        'grpbox_admin_usrcount
        '
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_guest_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_guest)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_sm_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_sm)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_sam_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_techsup_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_techsupport_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_orpos_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_hosupport_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_dev_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_dba_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_admin_load)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_admin)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_dba)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_sam)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_dev)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_techsup)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_hosupport)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_techsupport)
        Me.grpbox_admin_usrcount.Controls.Add(Me.lbl_admin_orpos)
        Me.grpbox_admin_usrcount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_admin_usrcount.ForeColor = System.Drawing.Color.RoyalBlue
        Me.grpbox_admin_usrcount.Location = New System.Drawing.Point(942, 12)
        Me.grpbox_admin_usrcount.Name = "grpbox_admin_usrcount"
        Me.grpbox_admin_usrcount.Size = New System.Drawing.Size(174, 270)
        Me.grpbox_admin_usrcount.TabIndex = 130
        Me.grpbox_admin_usrcount.TabStop = False
        Me.grpbox_admin_usrcount.Text = "ROLE COUNT"
        '
        'lbl_admin_sm_load
        '
        Me.lbl_admin_sm_load.AutoSize = True
        Me.lbl_admin_sm_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_sm_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_sm_load.Location = New System.Drawing.Point(143, 251)
        Me.lbl_admin_sm_load.Name = "lbl_admin_sm_load"
        Me.lbl_admin_sm_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_sm_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_sm_load.TabIndex = 131
        Me.lbl_admin_sm_load.Text = "cnt"
        '
        'lbl_admin_sm
        '
        Me.lbl_admin_sm.AutoSize = True
        Me.lbl_admin_sm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_sm.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_sm.Location = New System.Drawing.Point(15, 249)
        Me.lbl_admin_sm.Name = "lbl_admin_sm"
        Me.lbl_admin_sm.Size = New System.Drawing.Size(46, 15)
        Me.lbl_admin_sm.TabIndex = 130
        Me.lbl_admin_sm.Text = "SAM*:"
        '
        'lbl_admin_sam_load
        '
        Me.lbl_admin_sam_load.AutoSize = True
        Me.lbl_admin_sam_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_sam_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_sam_load.Location = New System.Drawing.Point(143, 226)
        Me.lbl_admin_sam_load.Name = "lbl_admin_sam_load"
        Me.lbl_admin_sam_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_sam_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_sam_load.TabIndex = 129
        Me.lbl_admin_sam_load.Text = "cnt"
        '
        'lbl_admin_techsup_load
        '
        Me.lbl_admin_techsup_load.AutoSize = True
        Me.lbl_admin_techsup_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_techsup_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_techsup_load.Location = New System.Drawing.Point(143, 201)
        Me.lbl_admin_techsup_load.Name = "lbl_admin_techsup_load"
        Me.lbl_admin_techsup_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_techsup_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_techsup_load.TabIndex = 128
        Me.lbl_admin_techsup_load.Text = "cnt"
        '
        'lbl_admin_techsupport_load
        '
        Me.lbl_admin_techsupport_load.AutoSize = True
        Me.lbl_admin_techsupport_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_techsupport_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_techsupport_load.Location = New System.Drawing.Point(143, 176)
        Me.lbl_admin_techsupport_load.Name = "lbl_admin_techsupport_load"
        Me.lbl_admin_techsupport_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_techsupport_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_techsupport_load.TabIndex = 127
        Me.lbl_admin_techsupport_load.Text = "cnt"
        '
        'lbl_admin_orpos_load
        '
        Me.lbl_admin_orpos_load.AutoSize = True
        Me.lbl_admin_orpos_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_orpos_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_orpos_load.Location = New System.Drawing.Point(143, 151)
        Me.lbl_admin_orpos_load.Name = "lbl_admin_orpos_load"
        Me.lbl_admin_orpos_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_orpos_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_orpos_load.TabIndex = 126
        Me.lbl_admin_orpos_load.Text = "cnt"
        '
        'lbl_admin_hosupport_load
        '
        Me.lbl_admin_hosupport_load.AutoSize = True
        Me.lbl_admin_hosupport_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_hosupport_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_hosupport_load.Location = New System.Drawing.Point(143, 126)
        Me.lbl_admin_hosupport_load.Name = "lbl_admin_hosupport_load"
        Me.lbl_admin_hosupport_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_hosupport_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_hosupport_load.TabIndex = 125
        Me.lbl_admin_hosupport_load.Text = "cnt"
        '
        'lbl_admin_dev_load
        '
        Me.lbl_admin_dev_load.AutoSize = True
        Me.lbl_admin_dev_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_dev_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_dev_load.Location = New System.Drawing.Point(143, 77)
        Me.lbl_admin_dev_load.Name = "lbl_admin_dev_load"
        Me.lbl_admin_dev_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_dev_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_dev_load.TabIndex = 124
        Me.lbl_admin_dev_load.Text = "cnt"
        '
        'lbl_admin_dba_load
        '
        Me.lbl_admin_dba_load.AutoSize = True
        Me.lbl_admin_dba_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_dba_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_dba_load.Location = New System.Drawing.Point(143, 53)
        Me.lbl_admin_dba_load.Name = "lbl_admin_dba_load"
        Me.lbl_admin_dba_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_dba_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_dba_load.TabIndex = 123
        Me.lbl_admin_dba_load.Text = "cnt"
        '
        'lbl_admin_admin_load
        '
        Me.lbl_admin_admin_load.AutoSize = True
        Me.lbl_admin_admin_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_admin_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_admin_load.Location = New System.Drawing.Point(143, 28)
        Me.lbl_admin_admin_load.Name = "lbl_admin_admin_load"
        Me.lbl_admin_admin_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_admin_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_admin_load.TabIndex = 122
        Me.lbl_admin_admin_load.Text = "cnt"
        '
        'lbl_admin_admin
        '
        Me.lbl_admin_admin.AutoSize = True
        Me.lbl_admin_admin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_admin.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_admin.Location = New System.Drawing.Point(15, 26)
        Me.lbl_admin_admin.Name = "lbl_admin_admin"
        Me.lbl_admin_admin.Size = New System.Drawing.Size(51, 15)
        Me.lbl_admin_admin.TabIndex = 111
        Me.lbl_admin_admin.Text = "Admin:"
        '
        'lbl_admin_dba
        '
        Me.lbl_admin_dba.AutoSize = True
        Me.lbl_admin_dba.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_dba.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_dba.Location = New System.Drawing.Point(15, 51)
        Me.lbl_admin_dba.Name = "lbl_admin_dba"
        Me.lbl_admin_dba.Size = New System.Drawing.Size(38, 15)
        Me.lbl_admin_dba.TabIndex = 113
        Me.lbl_admin_dba.Text = "DBA:"
        '
        'lbl_admin_sam
        '
        Me.lbl_admin_sam.AutoSize = True
        Me.lbl_admin_sam.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_sam.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_sam.Location = New System.Drawing.Point(15, 224)
        Me.lbl_admin_sam.Name = "lbl_admin_sam"
        Me.lbl_admin_sam.Size = New System.Drawing.Size(40, 15)
        Me.lbl_admin_sam.TabIndex = 119
        Me.lbl_admin_sam.Text = "SAM:"
        '
        'lbl_admin_dev
        '
        Me.lbl_admin_dev.AutoSize = True
        Me.lbl_admin_dev.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_dev.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_dev.Location = New System.Drawing.Point(15, 75)
        Me.lbl_admin_dev.Name = "lbl_admin_dev"
        Me.lbl_admin_dev.Size = New System.Drawing.Size(38, 15)
        Me.lbl_admin_dev.TabIndex = 114
        Me.lbl_admin_dev.Text = "DEV:"
        '
        'lbl_admin_techsup
        '
        Me.lbl_admin_techsup.AutoSize = True
        Me.lbl_admin_techsup.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_techsup.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_techsup.Location = New System.Drawing.Point(15, 199)
        Me.lbl_admin_techsup.Name = "lbl_admin_techsup"
        Me.lbl_admin_techsup.Size = New System.Drawing.Size(102, 15)
        Me.lbl_admin_techsup.TabIndex = 118
        Me.lbl_admin_techsup.Text = "Tech Support*:"
        '
        'lbl_admin_hosupport
        '
        Me.lbl_admin_hosupport.AutoSize = True
        Me.lbl_admin_hosupport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_hosupport.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_hosupport.Location = New System.Drawing.Point(15, 124)
        Me.lbl_admin_hosupport.Name = "lbl_admin_hosupport"
        Me.lbl_admin_hosupport.Size = New System.Drawing.Size(85, 15)
        Me.lbl_admin_hosupport.TabIndex = 115
        Me.lbl_admin_hosupport.Text = "HO Support:"
        '
        'lbl_admin_techsupport
        '
        Me.lbl_admin_techsupport.AutoSize = True
        Me.lbl_admin_techsupport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_techsupport.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_techsupport.Location = New System.Drawing.Point(15, 174)
        Me.lbl_admin_techsupport.Name = "lbl_admin_techsupport"
        Me.lbl_admin_techsupport.Size = New System.Drawing.Size(96, 15)
        Me.lbl_admin_techsupport.TabIndex = 117
        Me.lbl_admin_techsupport.Text = "Tech Support:"
        '
        'lbl_admin_orpos
        '
        Me.lbl_admin_orpos.AutoSize = True
        Me.lbl_admin_orpos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_orpos.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_orpos.Location = New System.Drawing.Point(15, 149)
        Me.lbl_admin_orpos.Name = "lbl_admin_orpos"
        Me.lbl_admin_orpos.Size = New System.Drawing.Size(59, 15)
        Me.lbl_admin_orpos.TabIndex = 116
        Me.lbl_admin_orpos.Text = "ORPOS:"
        '
        'lbl_admin_guest_load
        '
        Me.lbl_admin_guest_load.AutoSize = True
        Me.lbl_admin_guest_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_guest_load.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_guest_load.Location = New System.Drawing.Point(143, 101)
        Me.lbl_admin_guest_load.Name = "lbl_admin_guest_load"
        Me.lbl_admin_guest_load.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lbl_admin_guest_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_admin_guest_load.TabIndex = 133
        Me.lbl_admin_guest_load.Text = "cnt"
        '
        'lbl_admin_guest
        '
        Me.lbl_admin_guest.AutoSize = True
        Me.lbl_admin_guest.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_guest.ForeColor = System.Drawing.Color.Black
        Me.lbl_admin_guest.Location = New System.Drawing.Point(15, 99)
        Me.lbl_admin_guest.Name = "lbl_admin_guest"
        Me.lbl_admin_guest.Size = New System.Drawing.Size(48, 15)
        Me.lbl_admin_guest.TabIndex = 132
        Me.lbl_admin_guest.Text = "Guest:"
        '
        'admin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1128, 327)
        Me.Controls.Add(Me.grpbox_admin_usrcount)
        Me.Controls.Add(Me.grpbox_admin_usrlist)
        Me.Controls.Add(Me.grpbox_admin_user)
        Me.Controls.Add(Me.btn_admin_refresh)
        Me.Controls.Add(Me.btn_admin_deleteuser)
        Me.Controls.Add(Me.btn_admin_edituser)
        Me.Controls.Add(Me.btn_admin_adduser)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "admin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Administrator"
        Me.grpbox_admin_user.ResumeLayout(False)
        Me.grpbox_admin_user.PerformLayout()
        Me.grpbox_admin_usrlist.ResumeLayout(False)
        Me.grpbox_admin_usrcount.ResumeLayout(False)
        Me.grpbox_admin_usrcount.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents listview_users As ListView
    Friend WithEvents col_id As ColumnHeader
    Friend WithEvents col_user As ColumnHeader
    Friend WithEvents col_role As ColumnHeader
    Friend WithEvents btn_admin_adduser As Button
    Friend WithEvents btn_admin_edituser As Button
    Friend WithEvents btn_admin_deleteuser As Button
    Friend WithEvents col_status As ColumnHeader
    Friend WithEvents col_connection As ColumnHeader
    Friend WithEvents col_ip As ColumnHeader
    Friend WithEvents col_version As ColumnHeader
    Friend WithEvents col_platform As ColumnHeader
    Friend WithEvents col_created_on As ColumnHeader
    Friend WithEvents col_last_login As ColumnHeader
    Friend WithEvents btn_admin_refresh As Button
    Friend WithEvents col_browser As ColumnHeader
    Friend WithEvents col_remote As ColumnHeader
    Friend WithEvents col_ref_rate As ColumnHeader
    Friend WithEvents col_form_loc As ColumnHeader
    Friend WithEvents lbl_admin_version As Label
    Friend WithEvents lbl_master_version_load As Label
    Friend WithEvents col_auto_update As ColumnHeader
    Friend WithEvents lbl_admin_date As Label
    Friend WithEvents lbl_admin_user As Label
    Friend WithEvents lbl_admin_usramount As Label
    Friend WithEvents lbl_admin_enabled As Label
    Friend WithEvents lbl_admin_disabled As Label
    Friend WithEvents lbl_admin_online As Label
    Friend WithEvents lbl_admin_offline As Label
    Friend WithEvents lbl_admin_autoupdate As Label
    Friend WithEvents grpbox_admin_user As GroupBox
    Friend WithEvents grpbox_admin_usrlist As GroupBox
    Friend WithEvents lbl_admin_date_load As Label
    Friend WithEvents lbl_admin_user_load As Label
    Friend WithEvents lbl_admin_user_nb_load As Label
    Friend WithEvents lbl_admin_enabled_load As Label
    Friend WithEvents lbl_admin_disabled_load As Label
    Friend WithEvents lbl_admin_auto_load As Label
    Friend WithEvents lbl_admin_offline_load As Label
    Friend WithEvents lbl_admin_online_load As Label
    Friend WithEvents grpbox_admin_usrcount As GroupBox
    Friend WithEvents lbl_admin_sam_load As Label
    Friend WithEvents lbl_admin_techsup_load As Label
    Friend WithEvents lbl_admin_techsupport_load As Label
    Friend WithEvents lbl_admin_orpos_load As Label
    Friend WithEvents lbl_admin_hosupport_load As Label
    Friend WithEvents lbl_admin_dev_load As Label
    Friend WithEvents lbl_admin_dba_load As Label
    Friend WithEvents lbl_admin_admin_load As Label
    Friend WithEvents lbl_admin_admin As Label
    Friend WithEvents lbl_admin_dba As Label
    Friend WithEvents lbl_admin_sam As Label
    Friend WithEvents lbl_admin_dev As Label
    Friend WithEvents lbl_admin_techsup As Label
    Friend WithEvents lbl_admin_hosupport As Label
    Friend WithEvents lbl_admin_techsupport As Label
    Friend WithEvents lbl_admin_orpos As Label
    Friend WithEvents lbl_admin_sm As Label
    Friend WithEvents lbl_admin_sm_load As Label
    Friend WithEvents lbl_admin_guest_load As Label
    Friend WithEvents lbl_admin_guest As Label
End Class
