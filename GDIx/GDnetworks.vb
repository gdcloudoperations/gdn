﻿'AUTHOR: Alex Dumitrascu
'DATE: 07-12-2017
'UPDATE: 28-08-2018
'VERSION: 3.6.0 (change version var)
'
'FORM: GDIx.vb (MAIN)


Imports System
Imports System.Data
Imports System.Diagnostics
Imports System.Windows.Forms
Imports Microsoft.Office.Interop.Excel
Imports Microsoft.VisualBasic
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO
Imports System.Text
Imports System.Environment
Imports System.Net.NetworkInformation
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Threading
Imports System.Net
Imports System.Security.AccessControl
Imports System.ComponentModel
Imports System.Text.RegularExpressions
'Imports Oracle.DataAccess.Client
'Imports Oracle.DataAccess.Types

Public Class gdx_main

    ''''APP VARIABLES
    'MAIN
    Public version As String = "3.6.0"                                                                     'GDnetworks VERSION
    Public putty As Boolean = False                                                                        'putty exists
    Public Property ActiveSheet As Object                                                                  'for EXCEL reading
    Public listcount As Integer = 0                                                                        'ping list counter
    Private listOfPing As New List(Of String)                                                              'LIST OF DEVICES USED FOR PING
    Private dataPing As Ping = Nothing                                                                     'PING VARIABLE
    Protected Friend edit_flag = False                                                                     'EDIT STORE FLAG (used for sister store)
    Protected Friend add_flag = False                                                                      'ADD STORE FLAG (used for sister store)
    Protected Friend click_flag = False                                                                    'second flag action
    Protected Friend search_value As String = Nothing                                                      'search valuie (searchby other than store#)
    Private tooltip = New ToolTip()                                                                        'used for loading tooltips

    'USER
    Public set_browser As String = "c:\Program Files (x86)\Internet Explorer\iexplore.exe"                 'set default browser as IE if not set
    Public set_remote As String = "teamviewer"                                                             'set default Remote APP
    Public set_position As String = Nothing                                                                'store app desktop position
    Public ping_rate As Integer = 0                                                                        'PING STORE RATE
    Public accountRole As String = Nothing                                                                 'account role
    Protected Friend user_level As String                                                                  'user level
    Public osVersion As String = My.Computer.Info.OSFullName                                               'user's PC OS version
    Private LocalIP As String = Nothing                                                                    'user's PC IPaddress
    Private host As String = System.Net.Dns.GetHostName()                                                  'user's PC host
    Private putty_user As String = Nothing                                                                 'check putty
    Protected Friend auto_update As Boolean = False                                                        'app auto-update
    Protected Friend docs_gdrive, docs_jira, docs_local As String                                          'store location of support docs

    ''''STORE VARIABLES - LOCAL
    Public storeStatus As Boolean = False   'store status (True - Open , False - Closed)
    Public store_loaded As Boolean = False  'store loaded (user input on search)
    Public device_name As String            'store network device name (stores)
    Public cmd_router, cmd_sw1, cmd_sw2, cmd_ap1, cmd_ap2, cmd_tc, cmd_music, cmd_ipad, cmd_samsung As String   'network devices IP
    Public cmd_c1, cmd_c2, cmd_c3, cmd_c4, cmd_c5, cmd_c6, cmd_bo As String                                     'cashes IP
    Public pp_c1, pp_c2, pp_c3, pp_c4, pp_c5, pp_c6 As String                                                   'pinpad IPS


    ''''STORE VARIABLES - FROM DB
    'STORE
    Public boLine, faxLine, dslLine As String                                                               'additional phone lines
    Public dssName, dssLine, dssEmail, directorName, directorLine As String                                 'district info
    Public isCombo As Boolean = False                                                                       'isCOMBO
    Public isTemp As Boolean = False                                                                        'isTEMP
    Public cashes As String = Nothing                                                                       '# of cashes
    Public pinpadCon As String = Nothing                                                                    'pinpad connection

    'NETWORK
    Public IPAddress As String                                                                              'store IP address (3 octets only)
    Public ispName, ispAccount, ispStatus, ispAddress, ispSubnet, ispGateway, ispEmail, ispPwd As String    'ISP INFO
    Public routerModel, sw1Model, sw2Model, sw3Model, itCabinetLock, routerLocation                         'network equip info
    Public hasSwitch2 As Boolean = False                                                                    'switch 2 exists
    Public hasSwitch3 As Boolean = False                                                                    'switch 3 exists
    Public hasAP2 As Boolean = False                                                                        'AP2 exists

    'SPARE EQUIP
    Public spModem, spRouter, spWC, spHC, spPinpad As String                                                'spare equip


    ''''DB PARAMETERS
    'MAIN
    Private provider As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source ="                                                              'DB PROVIDER
    Protected Friend connectionString As String = "Data Source=SDNETH02;Initial Catalog=gdxDB2;User ID=gdXdbu;Password=" & creds("stores")      'DB LOCATION
    Protected Friend oracleConnectionString As String = "Driver = {Microsoft ODBC For Oracle};Server=rmstst-scan.corp.gdglobal.ca;uid=SIMADMIN;pwd=s1St0r3Admin"
    Private excel_provider As String = ";Extended Properties = ""Excel 12.0 Xml;HDR=YES;IMEX=1"""                                               'EXCEL ADDITIONAL PROVIDER INFO

    'STORE HOURS (.CSV)
    Protected Friend store_hours_file = "C:\users\" & getUsername() & "\appdata\local\GDnetworks\DB\store_open_hours.csv"   'CSV: STORE HOURS (PROD LOCALPC TEMP)
    'Protected Friend store_hours_file = "D:\Scripting\_HO\$VB projects\GDIx\GDIx\store_open_hours.csv"                     'CSV: STORE HOURS (DEV)
    'Protected Friend store_hours_file = "\\sagdxh01\C$\GDnetworks\DB\store_open_hours.csv"                                 'CSV: STORE HOURS (PROD NETWORK)

    'DISTRICT INFO (.XLSX)
    Private districtFile As String = "C:\users\" & getUsername() & "\appdata\local\GDnetworks\DB\District_List.xlsx"        'DISTRICT FILE LOCATION (PROD LOCALPC TEMP)
    'Private districtFile As String = "D:\Scripting\_HO\$VB projects\GDIx\GDIx\District_List.xlsx"                          'DISTRICT FILE LOCATION (DEV)
    'Private districtFile As String = "\\sagdxh01\C$\GDnetworks\DB\District_List.xlsx"                                      'DISTRICT FILE LOCATION (PROD NETWORK)




    'GDX MAIN ( ON LOAD )
    Private Sub gdx_main_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'select search by always to (by store)
        cmbbox_searchby.SelectedItem = "Store"

        bgwork_ping.WorkerReportsProgress = True
        bgwork_ping.WorkerSupportsCancellation = True

        'ENABLE KEYBOARD SHORTCUTS
        Me.KeyPreview = True

        'GET LOCAL IP
        For Each hostAddress In System.Net.Dns.GetHostEntry(host).AddressList()
            If hostAddress.ToString.StartsWith("172.") Then
                LocalIP = hostAddress.ToString
                Exit For
            ElseIf hostAddress.ToString.StartsWith("192.") Then
                LocalIP = hostAddress.ToString
                Exit For
            Else
                LocalIP = "0.0.0.0"
            End If

        Next

        'SET STATUS ONLINE
        update_user(getUsername, "Connection Status", "Online")

        'Check USERNAME
        checkUsername()

        'ENABLE DISABLE options based on user role
        accountPrivileges()

        'check if putty exists
        If File.Exists("c:\program files\putty\putty.exe") Then
            putty = True
        Else
            putty = False
        End If

        'GET TEMP FILES (TEMP - WILL BE PART OF FULL PACKAGE)
        get_temp_files()

        'start fresh
        clearall()

        'get local username
        lbl_user_load.Text = getUsername()
        'get user role
        lbl_role_load.Text = accountRole
        'get latest refresh time
        lbl_refresh_load.Text = DateTime.Now

        'get vpn connection
        checkVPN()

        'START VPN TIMER
        vpn_timer.Interval = 300000  '5 mins
        vpn_timer.Enabled = True
        vpn_timer.Start()

        'CHECK FOR AUTO-UPDATES
        If auto_update = True Then
            OnToolStripMenuItem.Text = "On " & ChrW(&H2713)
            OffToolStripMenuItem.Text = "Off"
            UpdatesToolStripMenuItem.PerformClick()
        Else
            OnToolStripMenuItem.Text = "On"
            OffToolStripMenuItem.Text = "Off " & ChrW(&H2713)
        End If

        'disable buttons
        statusMainButtons("disable")

        'PROD SELECTED
        PRODToolStripMenuItem.PerformClick()

        'clear pinpad pings
        pic_pinpad_c1.Image = Nothing
        pic_pinpad_c2.Image = Nothing
        pic_pinpad_c3.Image = Nothing
        pic_pinpad_c4.Image = Nothing
        pic_pinpad_c5.Image = Nothing
        pic_pinpad_c6.Image = Nothing

        'load gdnet.info tooltip
        loadPicToolTips(pic_gdx_main, "gdnet")

        'START APP TIMER ( WILL FORCE CLOSE AFTER 8H )
        app_running_timer.Interval = 28800000   '8 H
        app_running_timer.Enabled = True
        app_running_timer.Start()

        'FUTURE OPTIONS
        UnlockStoreAccountToolStripMenuItem.Enabled = False
        UnlockPOSToolStripMenuItem.Enabled = False

    End Sub

    'VPN timer
    Private Sub vpn_timer_Tick(sender As Object, e As EventArgs) Handles vpn_timer.Tick

        'check VPN connection every 5mins
        checkVPN()
    End Sub

    'CHECK VPN connection
    Private Sub checkVPN()
        Dim vpnConnection As Boolean = False
        Dim hostName = System.Net.Dns.GetHostName
        For Each hostAddress In System.Net.Dns.GetHostEntry(hostName).AddressList()

            If hostAddress.ToString().StartsWith("10.5.") Then
                vpnConnection = True
                Exit For
            End If
        Next

        If vpnConnection = True Then
            lbl_vpn_con_load.Text = "Connected"
            lbl_vpn_con_load.ForeColor = Color.Green
        Else
            lbl_vpn_con_load.Text = "Disconnected"
            lbl_vpn_con_load.ForeColor = Color.Red
        End If
    End Sub

    'GET TEMP FILES (TEMP - WILL BE PART OF FULL PACKAGE)
    Private Sub get_temp_files()

        'GET STORE-HOURS & DISTRICT LIST DB (TEMP)
        Dim db_location As String = "C:\users\" & getUsername() & "\appdata\local\GDnetworks\DB\"
        'GET HELP FILES
        'Dim hf_location As String = "C:\users\" & getUsername() & "\appdata\local\GDnetworks\HELP\"

        'Create APP FOLDER DB
        If (Not System.IO.Directory.Exists(db_location)) Then
            System.IO.Directory.CreateDirectory(db_location)
        End If
        'Create APP FOLDER HELP
        'If (Not System.IO.Directory.Exists(hf_location)) Then
        ' System.IO.Directory.CreateDirectory(hf_location)
        'End If

        'COPY DBs
        Try
            File.Copy("\\sagdxh01\GDnetworks\DB\store_open_hours.csv", db_location & "store_open_hours.csv", True)    'store hours
            File.Copy("\\sagdxh01\GDnetworks\DB\District_List.xlsx", db_location & "District_List.xlsx", True)        'district list

            'COPY HELP
            'File.Copy("\\sagdxh01\GDnetworks\Program\Help\shortcuts.html", hf_location & "shortcuts.html", True)
            'File.Copy("\\sagdxh01\GDnetworks\Program\Help\help.html", hf_location & "help.html", True)
        Catch ex As Exception
            MsgBox("Latest offlineDB not available.")
        End Try


    End Sub

    'KEYBOARD SHORTCUTS
    Private Sub gdx_main_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        'NAVIGATION DONE ON INDIVIDUAL SUBS & FUNCTIONS

        'OPERATION       
        If (e.Control AndAlso (e.KeyCode = Keys.G)) Then        'CTRL+G = Store GMAIL info
            StorePWDToolStripMenuItem.PerformClick()
        ElseIf (e.Control AndAlso (e.KeyCode = Keys.R)) Then    'CTRL+R = RSG login portal
            RSGToolStripMenuItem.PerformClick()
        ElseIf (e.Control AndAlso (e.KeyCode = Keys.C)) Then    'CTRL+C = CMR page
            CMRToolStripMenuItem.PerformClick()
        ElseIf (e.Control AndAlso (e.KeyCode = Keys.O)) Then    'CTRL+O = Orion with store (if loaded)
            OrionToolStripMenuItem.PerformClick()
        ElseIf (e.Shift AndAlso (e.KeyCode = Keys.A)) Then      'CTRL+A = Track Canada POst
            CanadaPostToolStripMenuItem.PerformClick()
        ElseIf (e.Shift AndAlso (e.KeyCode = Keys.C)) Then      'CTRL+C = Track Canpar
            CanparToolStripMenuItem.PerformClick()
        ElseIf (e.Shift AndAlso (e.KeyCode = Keys.F)) Then      'CTRL+F = Track Fedex
            FedExToolStripMenuItem.PerformClick()
        ElseIf (e.Shift AndAlso (e.KeyCode = Keys.P)) Then      'CTRL+P = Track Purolator
            PurolatorToolStripMenuItem.PerformClick()
        ElseIf (e.Shift AndAlso (e.KeyCode = Keys.U)) Then      'CTRL+U = Track UPS
            UPSToolStripMenuItem.PerformClick()
        End If

        'Tools
        If (e.KeyCode = Keys.F1) Then                           'F1 = Help
            HelpF1ToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F2) Then                       'F2 = Keyboard Shortcuts
            KeyboardShortcutsToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F5) Then                       'F5 = Refresh
            RefreshToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F7) Then                       'F7 = LABS
            LABSToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F8) Then                       'F8 = PROD
            PRODToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F10) Then                      'F10 = Save Position
            SavePositionToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F11) Then                      'F11 = Check for updates
            UpdatesToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F12) Then                      'F12 = Exit
            ExitToolStripMenuItem.PerformClick()
        End If

    End Sub

    'CHECK: ACCOUNT PRIVILEGES
    Private Sub accountPrivileges()

        'PUTTY USER
        If accountRole = "Admin" Or accountRole = "*SYS" Then
            putty_user = "admin"
        ElseIf accountRole = "Tech Support" Or accountRole = "Tech Support*" Then
            putty_user = "helpdesk"
        Else
            putty_user = Nothing
        End If

        'ACCOUNT ROLES
        If accountRole = "Guest" Then
            user_level = "noaccess"
        ElseIf accountRole = "DBA" Or accountRole = "DEV" Or accountRole = "HO Support" Or accountRole = "ORPOS" Or accountRole = "SAM" Then
            user_level = "limited"
        ElseIf accountRole = "SAM*" Then
            user_level = "limited+"
        ElseIf accountRole = "Tech Support" Then
            user_level = "general"
        ElseIf accountRole = "Tech Support*" Then
            user_level = "general+"
        ElseIf accountRole = "Admin" Then
            user_level = "elevated"
        ElseIf accountRole = "*SYS" Then
            user_level = "full"
        End If

        ''''ACCESS TO TOPMENU & BUTTONS

        ''FILE
        'ADMIN CONSOLE
        If user_level = "full" Or user_level = "elevated" Or user_level = "general+" Or user_level = "limited+" Then
            AdminToolStripMenuItem.Enabled = True
        Else
            AdminToolStripMenuItem.Enabled = False
        End If

        'PROD & LAB MODES
        If accountRole = "SAM" Or accountRole = "SAM*" Or accountRole = "Guest" Then
            LABSToolStripMenuItem.Enabled = False
        Else
            LABSToolStripMenuItem.Enabled = True
        End If

        'STORE CHANGES
        If user_level = "full" Or user_level = "elevated" Then 'DELETE, ADD, EDIT STORE
            DeleteStoreToolStripMenuItem.Enabled = True
            AddToolStripMenuItem.Enabled = True
            EditToolStripMenuItem.Enabled = True
        ElseIf user_level = "general" Then                     'EDIT STORE
            DeleteStoreToolStripMenuItem.Enabled = False
            AddToolStripMenuItem.Enabled = False
            EditToolStripMenuItem.Enabled = True
        ElseIf user_level = "general+" Then                    'ADD, EDIT STORE
            DeleteStoreToolStripMenuItem.Enabled = False
            AddToolStripMenuItem.Enabled = True
            EditToolStripMenuItem.Enabled = True
        Else
            DeleteStoreToolStripMenuItem.Enabled = False       'DISABLED
            AddToolStripMenuItem.Enabled = False
            EditToolStripMenuItem.Enabled = False
        End If

        ''VIEW
        'CALENDARS
        If user_level = "noaccess" Then
            CalendarToolStripMenuItem.Enabled = False
        Else
            CalendarToolStripMenuItem.Enabled = True
        End If

        'CMR
        If user_level = "noaccess" Then
            CMRToolStripMenuItem.Enabled = False
        Else
            CMRToolStripMenuItem.Enabled = True
        End If

        'HW pics
        If user_level = "noaccess" Then
            HWPicsToolStripMenuItem.Enabled = False
        Else
            HWPicsToolStripMenuItem.Enabled = True
        End If

        'Inventory
        If user_level = "noaccess" Or user_level = "limited" Or user_level = "limited+" Then
            InventoryToolStripMenuItem.Enabled = False
        Else
            InventoryToolStripMenuItem.Enabled = True
        End If

        'PRODCO
        If user_level = "noaccess" Then
            TrafficCounterToolStripMenuItem.Enabled = False
        Else
            TrafficCounterToolStripMenuItem.Enabled = True
        End If

        'RSG
        If user_level = "noaccess" Then
            RSGToolStripMenuItem.Enabled = False
        Else
            RSGToolStripMenuItem.Enabled = True
        End If

        'GMAIl
        If user_level = "noaccess" Then
            StorePWDToolStripMenuItem.Enabled = False
        Else
            StorePWDToolStripMenuItem.Enabled = True
        End If

        'Support Docs
        If user_level = "noaccess" Then
            SupportDocsToolStripMenuItem.Enabled = False
        Else
            SupportDocsToolStripMenuItem.Enabled = True
        End If

        ''TOOLS
        'Bell
        If user_level = "noaccess" Then
            BellDialToolStripMenuItem.Enabled = False
        Else
            BellDialToolStripMenuItem.Enabled = True
        End If

        'Beehivr
        If user_level = "noaccess" Or user_level = "limited" Then
            BeehivrToolStripMenuItem.Enabled = False
        Else
            BeehivrToolStripMenuItem.Enabled = True
        End If

        'IPad Signup
        If user_level = "noaccess" Or user_level = "limited" Or user_level = "limited+" Then
            IPadSignUPToolStripMenuItem.Enabled = False
        Else
            IPadSignUPToolStripMenuItem.Enabled = True
        End If

        'Meraki
        If user_level = "noaccess" Or user_level = "limited" Then
            MerakiToolStripMenuItem.Enabled = False
        Else
            MerakiToolStripMenuItem.Enabled = True
        End If

        'MobiControl
        If user_level = "noaccess" Or user_level = "limited" Then
            MobiControlToolStripMenuItem.Enabled = False
        Else
            MobiControlToolStripMenuItem.Enabled = True
        End If

        'Orion
        If user_level = "noaccess" Then
            OrionToolStripMenuItem.Enabled = False
        Else
            OrionToolStripMenuItem.Enabled = True
        End If

        'PrintersDB
        If user_level = "noaccess" Or user_level = "limited" Or user_level = "limited+" Then
            PrintersToolStripMenuItem.Enabled = False
        Else
            PrintersToolStripMenuItem.Enabled = True
        End If

        'Speedtest
        If user_level = "noaccess" Or user_level = "limited" Or user_level = "limited+" Then
            SpeedtestToolStripMenuItem.Enabled = False
        Else
            SpeedtestToolStripMenuItem.Enabled = True
        End If


        ''OPTIONS
        'Auto-update
        If user_level = "noaccess" Then
            AutoUpdateToolStripMenuItem.Enabled = False
        Else
            AutoUpdateToolStripMenuItem.Enabled = True
        End If

        'Remote
        If user_level = "noaccess" Then
            RemoteToolStripMenuItem.Enabled = False
        Else
            RemoteToolStripMenuItem.Enabled = True
        End If

        'Remote (PSEXEC)
        If accountRole = "DBA" Or accountRole = "HO Support" Or accountRole = "SAM" Or accountRole = "SAM*" Then
            PSEXECToolStripMenuItem.Enabled = False
        Else
            PSEXECToolStripMenuItem.Enabled = True
        End If

        'Support Locations
        If user_level = "noaccess" Then
            SupportLocationsToolStripMenuItem.Enabled = False
        Else
            SupportLocationsToolStripMenuItem.Enabled = True
        End If

        ''''DISABLE SEARCH ( only by store)
        If user_level = "noaccess" Then
            cmbbox_searchby.Enabled = False
        Else
            cmbbox_searchby.Enabled = True
        End If

        ''''DISABLE BUTTONS
        'Store Hours
        If user_level = "noaccess" Then
            btn_store_hours.Enabled = False
        Else
            btn_store_hours.Enabled = True
        End If

        'Ping LIST
        If user_level = "full" Or user_level = "elevated" Or user_level = "general+" Or user_level = "general" Or user_level = "noaccess" Then
            btn_list_status.Enabled = True
        Else
            btn_list_status.Enabled = False
        End If

        'More Info
        If user_level = "noaccess" Then
            btn_moreinfo.Enabled = False
        Else
            btn_moreinfo.Enabled = True
        End If

        'NETWORK BUTTONS:
        'search for: ENABLE BUTTONS (KEEP DISABLE FOR noaccess)

        'disable shortcuts
        'search for: BLOCK ACCESS TO C DRIVE FOR noaccess

        'disable EXPORT inside searchby
        'search inside searchby: DISABLE EXPORT for noaccess

    End Sub

    'CHECK: USERNAME
    Private Sub checkUsername()

        'GET USER'S IP ADDRESS into DB      
        update_user(getUsername(), "IP Address", LocalIP)
        'GET USER'S APP VERSION
        update_user(getUsername(), "Version", version)
        'GET USERS'S OS PLATFORM
        update_user(getUsername(), "Platform", osVersion)
        'GET USER'S LAST LOGIN
        update_user(getUsername(), "Last Login", DateTime.Now)


        'GET USER INFO FROM DB
        Dim get_userinfo() = Split(read_user(getUsername()), "|")

        'CHECK: IF USER EXISTS (HAS ACCESS TO APP)
        If get_userinfo(0).ToString = getUsername() Then
            'CHECK: USER ACCOUNT STATUS
            If get_userinfo(2).ToString = "Enabled" Then

                'get account role
                accountRole = get_userinfo(1).ToString

                'get default BROWSER
                If get_userinfo(9).ToString = "Chrome" Then
                    ChromeToolStripMenuItem.PerformClick()      'select chrome
                Else
                    IEToolStripMenuItem.PerformClick()          'select iexplore
                End If
                'get default REMOTE
                If get_userinfo(10).ToString = "RDP" Then
                    RDPToolStripMenuItem.PerformClick()         'RDP
                Else
                    TeamViewerToolStripMenuItem1.PerformClick() 'TeamViewer
                End If
                'get refresh rate
                If get_userinfo(11) = 10 Then
                    topmenu_refresh_10sec.PerformClick()        '10 SEC REFRESH RATE
                ElseIf get_userinfo(11) = 30 Then
                    topmenu_refresh_30sec.PerformClick()        '30 SEC REFRESH RATE
                ElseIf get_userinfo(11) = 60 Then
                    topmenu_refresh_60sec.PerformClick()        '60 SEC REFRESH RATE
                Else
                    topmenu_refresh_0sec.PerformClick()         'NO REFRESH RATE
                End If
                'get FORM position
                set_position = get_userinfo(12).ToString
                Dim position_x As String = set_position.Split("/"c)(0)
                Dim position_y As String = set_position.Split("/"c)(1)
                Me.Location = New System.Drawing.Point(position_x, position_y)

                'get auto_update info
                If get_userinfo(13) = True Then
                    auto_update = True
                Else
                    auto_update = False
                End If

                'get google support docs
                If IsDBNull(get_userinfo(14).ToString) Then
                    docs_gdrive = "N/A"
                Else
                    docs_gdrive = get_userinfo(14).ToString
                End If

                'get jira support docs
                If IsDBNull(get_userinfo(15).ToString) Then
                    docs_jira = "N/A"
                Else
                    docs_jira = get_userinfo(15).ToString
                End If

                'get local support docs
                If IsDBNull(get_userinfo(16).ToString) Then
                    docs_local = "N/A"
                Else
                    docs_local = get_userinfo(16).ToString
                End If

            Else
                    'CHECK: IF USER ACCOUNT IS DISABLED
                    MsgBox("Your account is DISABLED!", MsgBoxStyle.Critical, Title:="GDnetworks - Error! (account)")
                Close()
            End If
        Else
            'CHECK: IF USER DOES NOT EXIST CLOSE APP (NO ACCESS TO APP)
            MsgBox("You don't have enough rights to use this application!", MsgBoxStyle.Critical, Title:="GDnetworks - Error! (account)")
            Close()
        End If

    End Sub

    'ADD USER SETTINGS
    Protected Friend Sub add_user(Username As String, Role As String, Status As String)

        'DB CONNECTION VARS
        Dim userReader As SqlDataReader
        Dim usr_connection As SqlConnection
        Dim select_connection As SqlConnection
        Dim cmd_usr As SqlCommand
        Dim cmd_select As SqlCommand
        Dim sql_select_user = "Select * from users_settings where Username = '" & Username & "'"
        Dim sql_add_user = "insert into users_settings([Username], [Role], [Status], [IP Address], [Connection Status], [Version], [Platform],
                               [Created On], [Last Login], [Browser], [Remote], [Refresh Rate], [Form Location], [Auto Update]) 
                               VALUES('" & Username & "', '" & Role & "', '" & Status & "', 'N/A', 'N/A', 'N/A', 'N/A', '" & DateTime.Now & "', 
                               'N/A', 'IE', 'TV', '0', '560/320', 0)"

        'CONNECTION PARAM
        select_connection = New SqlConnection(connectionString)
        cmd_select = New SqlCommand(sql_select_user, select_connection)
        usr_connection = New SqlConnection(connectionString)
        cmd_usr = New SqlCommand(sql_add_user, usr_connection)

        Try
            select_connection.Open()                    'OPEN SELECT CONNECTION
            usr_connection.Open()                       'OPEN ADD CONNECTION
            userReader = cmd_select.ExecuteReader()
            If userReader.Read() = True Then
                MsgBox("User: " & Username & " already exists!" & vbNewLine & "Please check the username and try again.",
                       MsgBoxStyle.Information, Title:="GDnetworks - Info! (add user)")
                AddUser.clearAddUserForm()
            Else
                cmd_usr.ExecuteNonQuery()
                MsgBox("User: " & Username & " added successfully!", MsgBoxStyle.Information, Title:="GDnetworks - Success! (add user)")
            End If
        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: users_settings - add user]")
        End Try

        'CLOSE CONNECTIONS
        select_connection.Close()
        usr_connection.Close()

        'clear form
        AddUser.clearAddUserForm()

    End Sub

    'DELETE USER SETTINGS
    Protected Friend Sub delete_user(Username As String)

        'DB CONNECTION VARS
        Dim usr_connection As SqlConnection
        Dim cmd_usr As SqlCommand
        Dim sql_delete_user = "delete from users_settings where Username = '" & Username & "'"
        usr_connection = New SqlConnection(connectionString)
        cmd_usr = New SqlCommand(sql_delete_user, usr_connection)

        Try
            usr_connection.Open()
            cmd_usr.ExecuteNonQuery()
            MsgBox("User: " & Username & " deleted successfully!", MsgBoxStyle.Information, Title:="GDnetworks - Success! (delete user)")
            usr_connection.Close()

            'exception if cannot connect to DB
        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: users_settings - delete user]")
            Close()
        End Try

    End Sub

    'UPDATE USER SETTINGS
    Protected Friend Sub update_user(selected_user As String, field As String, new_value As String)

        'DB CONNECTION VARS
        Dim usr_connection As SqlConnection
        Dim cmd_usr As SqlCommand
        Dim sql_update_user = "update [users_settings] set [" & field & "] = '" & new_value & "' where [Username] = '" & selected_user & "'"
        usr_connection = New SqlConnection(connectionString)
        cmd_usr = New SqlCommand(sql_update_user, usr_connection)

        Try
            usr_connection.Open()
            cmd_usr.ExecuteNonQuery()
            usr_connection.Close()

            'exception if cannot connect to DB
        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: users_settings - update user]")
            Close()
        End Try

    End Sub

    'READ USER SETTINGS
    Protected Friend Function read_user(selected_user As String)

        'FUNCTION VARS
        Dim read_username As String
        Dim read_role As String
        Dim read_status As String
        Dim read_ip_address As String
        Dim read_conn_status As String
        Dim read_version As String
        Dim read_platform As String
        Dim read_creation_date As String
        Dim read_last_login As String
        Dim read_browser As String
        Dim read_remote As String
        Dim read_refresh_rate As String
        Dim read_form_location As String
        Dim read_auto_update As Boolean
        Dim read_docs_google As String
        Dim read_docs_jira As String
        Dim read_docs_local As String

        'DB CONNECTION VARS
        Dim usr_connection As SqlConnection
        Dim cmd_usr As SqlCommand
        Dim userReader As SqlDataReader
        Dim sql_user_settings = "Select * from users_settings where Username = '" & selected_user & "'"
        usr_connection = New SqlConnection(connectionString)
        cmd_usr = New SqlCommand(sql_user_settings, usr_connection)


        'GET USER INFO FROM users_settings DB
        Try
            'OPEN CONNECTIOn
            usr_connection.Open()
            userReader = cmd_usr.ExecuteReader()
            'CHECK IF USER EXISTS
            If Not userReader.HasRows Then
                'return NOTHING if user does not exist
                Return Nothing
            Else
                While userReader.Read()
                    read_username = userReader("Username")                  'Username
                    read_role = userReader("Role")                          'Role
                    read_status = userReader("Status")                      'User Status
                    If IsDBNull(userReader("IP Address")) Then
                        read_ip_address = "ipNULL"
                    Else
                        read_ip_address = userReader("IP Address")          'IP Address
                    End If
                    If IsDBNull(userReader("Connection Status")) Then
                        read_conn_status = "connstatusNULL"
                    Else
                        read_conn_status = userReader("Connection Status")  'Connection Status
                    End If
                    If IsDBNull(userReader("Version")) Then
                        read_version = "versionNULL"
                    Else
                        read_version = userReader("Version")                'GD Version
                    End If
                    If IsDBNull(userReader("Platform")) Then
                        read_platform = "platformNULL"
                    Else
                        read_platform = userReader("Platform")              'User OS Platform
                    End If
                    If IsDBNull(userReader("Created On")) Then
                        read_creation_date = "creationNULL"
                    Else
                        read_creation_date = userReader("Created On")       'User Creation Date
                    End If
                    If IsDBNull(userReader("Last Login")) Then
                        read_last_login = "loginNULL"
                    Else
                        read_last_login = userReader("Last Login")          'Last User Login
                    End If
                    read_browser = userReader("Browser")                    'Default Browser
                    read_remote = userReader("Remote")                      'Default Remote
                    read_refresh_rate = userReader("Refresh Rate")          'App Refresh Rate
                    read_form_location = userReader("Form Location")        'Default Form Position
                    read_auto_update = userReader("Auto Update")            'App Auto Update
                    If IsDBNull(userReader("DocsGDrive")) Then              'GDrive support docs
                        read_docs_google = "N/A"
                    Else
                        read_docs_google = userReader("DocsGDrive")
                    End If
                    If IsDBNull(userReader("DocsJira")) Then                'Jira support docs
                        read_docs_jira = "N/A"
                    Else
                        read_docs_jira = userReader("DocsJira")
                    End If
                    If IsDBNull(userReader("DocsLocal")) Then               'Local support docs
                        read_docs_local = "N/A"
                    Else
                        read_docs_local = userReader("DocsLocal")
                    End If


                End While
            End If
        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: users_settings - READ]")
        End Try

        'CLOSE CONNECTION
        usr_connection.Close()

        'Return VALUES using | as separator
        Return read_username & "|" & read_role & "|" & read_status & "|" & read_ip_address & "|" & read_conn_status & "|" & read_version & "|" & read_platform _
         & "|" & read_creation_date & "|" & read_last_login & "|" & read_browser & "|" & read_remote & "|" & read_refresh_rate & "|" & read_form_location & "|" & read_auto_update _
         & "|" & read_docs_google & "|" & read_docs_jira & "|" & read_docs_local

    End Function

    'TOP MENU
    Private Sub top_menu_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs) Handles top_menu.ItemClicked
    End Sub

    'TOP TOOL BAR (Admin)
    Private Sub AdminToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AdminToolStripMenuItem.Click
        'get admin pwd dialog
        admin_pwd.ShowDialog()
    End Sub

    'TOP TOOL BAR (PROD)
    Private Sub PRODToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PRODToolStripMenuItem.Click

        LABSToolStripMenuItem.Text = "LABS   F7"
        PRODToolStripMenuItem.Text = "PROD   F8 " & ChrW(&H2713)

    End Sub

    'TOP TOOL BAR (LABS)
    Private Sub LABSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LABSToolStripMenuItem.Click

        LABSToolStripMenuItem.Text = "LABS   F7 " & ChrW(&H2713)
        PRODToolStripMenuItem.Text = "PROD   F8"

        Me.Hide()
        ping_timer.Stop()
        Labs.Show()

    End Sub


    'TOP TOOL BAR (exit)
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        gdx_main.ActiveForm.Close()
    End Sub

    'TOP TOOL BAR (Remote Desktop)
    Private Sub RemoteDesktopToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoteDesktopToolStripMenuItem.Click
        Process.Start("C:\Windows\System32\mstsc.exe")
    End Sub

    'TOP TOOL BAR (RMS - Unlock Store2Store user)
    Private Sub UnlockStoreAccountToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UnlockStoreAccountToolStripMenuItem.Click

        If store_loaded = Nothing Then
            MsgBox("You must search for a store first!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (RMS - unlock str2str)")
        Else
            unlockRMSstore2store()
        End If

    End Sub

    'TOP TOOL BAR (RMS - Unlock WL POS user)
    Private Sub UnlockPOSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UnlockPOSToolStripMenuItem.Click
        If store_loaded = Nothing Then
            MsgBox("You must search for a store first!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (WL - unlock POS account)")
        Else
            unlockWLpos()
        End If
    End Sub

    'TOP TOOL BAR (list all Speedtests by store)
    Private Sub ListSpeedtestByStoreToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListSpeedtestByStoreToolStripMenuItem.Click

        speedtest_list.ShowDialog()

    End Sub

    'TOP TOOL BAR (Run Speedtest)
    Private Sub RunSpeedtestToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RunSpeedtestToolStripMenuItem.Click

        If store_loaded = True Then
            If Speedtest.Visible = True Then
                Speedtest.TopMost = True
            Else
                Speedtest.Show()
            End If

        Else
                MsgBox("You must search for a store first!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (Speedtest)")
       End If

    End Sub


    'TOP TOOL BAR (Team Viewer)
    Private Sub TeamViewerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TeamViewerToolStripMenuItem.Click
        Process.Start("C:\Program Files (x86)\TeamViewer\TeamViewer.exe")
    End Sub

    'TOP TOOL BAR (Bell Dial System)
    Private Sub BellDialToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BellDialToolStripMenuItem.Click
        Process.Start(set_browser, "https://odcc1.bell.ca/AGUI/login.php")
    End Sub

    'TOP TOOL BAR (Beehivr)
    Private Sub BeehivrToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BeehivrToolStripMenuItem.Click
        Process.Start(set_browser, "https://dashboard.beehivr.com/authentication/login")
    End Sub

    'TOP TOOL BAR (Ipad signup)
    Private Sub IPadSignUPToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IPadSignUPToolStripMenuItem.Click
        Process.Start(set_browser, "http://addstoretablet.dynamite.ca/AddStoreForm.aspx")
    End Sub

    'TOP TOOL BAR (Meraki)
    Private Sub MerakiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MerakiToolStripMenuItem.Click
        Process.Start(set_browser, "https://n97.meraki.com/login/dashboard_login")
    End Sub

    'TOP TOOL BAR (MobiControl)
    Private Sub MobiControlToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MobiControlToolStripMenuItem.Click
        Process.Start(set_browser, "https://samobh01.corp.gdglobal.ca/MobiControl/WebConsole/Home")
    End Sub

    'TOP TOOL BAR (Orion)
    Private Sub OrionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OrionToolStripMenuItem.Click

        If IPAddress = "" Then
            Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/")
        Else
            Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & IPAddress & "%2A&ResourceID=7")
        End If

    End Sub

    'TOP TOOL BAR (CUPS)
    Private Sub CUPSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CUPSToolStripMenuItem.Click
        Process.Start(set_browser, "https://sacuph01.corp.gdglobal.ca:631/printers/")
    End Sub

    'TOP TOOL BAR (BIpublisher)
    Private Sub BIpublisherToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BIpublisherToolStripMenuItem.Click
        Process.Start(set_browser, "http://sastoh01.corp.gdglobal.ca:9706/xmlpserver/login.jsp")
    End Sub

    'TOP TOOL BAR (Set CHROME as default browser)
    Private Sub ChromeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ChromeToolStripMenuItem.Click
        set_browser = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"
        ChromeToolStripMenuItem.Text = "Chrome " & ChrW(&H2713)
        IEToolStripMenuItem.Text = "IExplorer"

        'UPDATE USER DB
        update_user(getUsername(), "Browser", "Chrome")

    End Sub

    'TOP TOOL BAR (Set IE as default browser)
    Private Sub IEToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IEToolStripMenuItem.Click
        set_browser = "C:\Program Files (x86)\Internet Explorer\iexplore.exe"
        IEToolStripMenuItem.Text = "IExplorer " & ChrW(&H2713)
        ChromeToolStripMenuItem.Text = "Chrome"

        'UPDATE USER DB
        update_user(getUsername(), "Browser", "IE")

    End Sub

    'TOP TOOL BAR (Save Form Position)
    Private Sub SavePositionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SavePositionToolStripMenuItem.Click
        set_position = Me.Location.X.ToString
        set_position = set_position & "/" & Me.Location.Y.ToString
        update_user(getUsername(), "Form Location", set_position)
        SavePositionToolStripMenuItem.Text = "Save Position   F10 " & ChrW(&H2713)
    End Sub

    'TOP TOOL(Support locations)
    Private Sub SupportLocationsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SupportLocationsToolStripMenuItem.Click
        support_docs.ShowDialog()
    End Sub

    Private Sub ViewToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ViewToolStripMenuItem.Click

    End Sub

    Private Sub FileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FileToolStripMenuItem.Click

    End Sub

    'TRAFFIC COUNTER - DASHBOARD
    Private Sub TrafficCounterToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TrafficCounterToolStripMenuItem.Click
        If store_loaded = True Then
            Process.Start(set_browser, "http://prodco.gdnet.info/PRODCO/store_" & lbl_strno_load.Text & ".htm")
        Else
            MsgBox("You must search for a store first!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (Prodco - Dashboard)")
        End If

    End Sub

    'TOP TOOL BAR (Store GMAIL pwd list)
    Private Sub StorePWDToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles StorePWDToolStripMenuItem.Click
        Process.Start(set_browser, "https://docs.google.com/spreadsheets/d/1mWXgDVV6G2ZaEBAflZGm5b-3j4B7-EUDQoHv2WtPxoQ/edit#gid=144755263")
    End Sub

    'TOP TOOL BAR (Support DOCS GOOGLE)
    Private Sub GDriveToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GDriveToolStripMenuItem.Click

        If docs_gdrive = "N/A" Then
            MsgBox("G!Drive support docs location not set!" & vbNewLine & "Go to Options-Support locations", MsgBoxStyle.Information, Title:="GDnetworks - Info! (supportDocs)")
        Else
            Process.Start(set_browser, docs_gdrive)
        End If

    End Sub

    'TOP TOOL BAR (Support DOCS JIRA)
    Private Sub JiraToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JiraToolStripMenuItem.Click

        If docs_jira = "N/A" Then
            MsgBox("Jira support docs location not set!" & vbNewLine & "Go to Options-Support locations", MsgBoxStyle.Information, Title:="GDnetworks - Info! (supportDocs)")
        Else
            Process.Start(set_browser, docs_jira)
        End If

    End Sub

    'TOP TOOL BAR (Support DOCS LOCAL)
    Private Sub LocalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LocalToolStripMenuItem.Click

        If docs_local = "N/A" Then
            MsgBox("Local support docs location not set!" & vbNewLine & "Go to Options-Support locations", MsgBoxStyle.Information, Title:="GDnetworks - Info! (supportDocs)")
        Else
            Process.Start(docs_local)
        End If

    End Sub

    'TOP TOOL BAR (WEB Garage CA)
    Private Sub GarageToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GarageToolStripMenuItem.Click
        Process.Start(set_browser, "https://www.garageclothing.com/ca/")
    End Sub

    'TOP TOOL BAR (WEB Garage US)
    Private Sub DynamiteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DynamiteToolStripMenuItem.Click
        Process.Start(set_browser, "https://www.garageclothing.com/us/")
    End Sub

    'TOP TOOL BAR (WEB Dynamite CA)
    Private Sub DynamiteCAToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DynamiteCAToolStripMenuItem.Click
        Process.Start(set_browser, "https://www.dynamiteclothing.com/ca/")
    End Sub

    'TOP TOOL BAR (WEB Dynamite US)
    Private Sub DynamiteUSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DynamiteUSToolStripMenuItem.Click
        Process.Start(set_browser, "https://www.dynamiteclothing.com/us/")
    End Sub

    'TOP TOOL BAR (WEB Dynamite MAIN)
    Private Sub GRPDYNToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GRPDYNToolStripMenuItem.Click
        Process.Start(set_browser, "http://groupedynamite.com/")
    End Sub


    'TOP TOOL BAR (Support form)
    Private Sub SupportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SupportToolStripMenuItem.Click
        'form_support.ShowDialog()
        MsgBox("Please report any issues to adumitrascu@dynamite.ca", MsgBoxStyle.Information, Title:="GDnetworks - info! (Support)")

    End Sub

    'TOP TOOL BAR (Updates form)
    Private Sub UpdatesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UpdatesToolStripMenuItem.Click

        'UPDATES FILE ( VERSION UPDATE - GET VERSION )
        Dim files = Directory.GetFiles("\\sagdxh01\GDnetworks\Program", "gdV_*.txt")
        If files.Length > 0 Then
            'GET VERSION
            For Each file In files
                Dim regex_version As Regex = New Regex("(?<=_).*?(?=.txt)", RegexOptions.IgnoreCase)
                Dim new_version = regex_version.Match(file)

                If new_version.ToString() = version Then
                    MsgBox("GDnetworks is up to date!", MsgBoxStyle.Information, Title:="GDnetworks - info! (updates)")
                Else

                    Dim update_result As Integer = MsgBox("Update found. Do you wish to continue?", MsgBoxStyle.YesNo, Title:="GDnetworks - info! (updates)")
                    If update_result = DialogResult.No Then
                        'NOTHING
                    Else
                        System.IO.File.Copy("\\sagdxh01\GDnetworks\Program\uninstall.cmd", "C:\users\" & getUsername() _
                                            & "\appdata\local\GDnetworks\uninstall.cmd", True)
                        'Process.Start("C:\users\" & getUsername() _
                        '                   & "\appdata\local\GDnetworks\uninstall.cmd")
                        Process.Start("C:\Users\" & getUsername() & "\AppData\Local\GDnetworks\uninstall.cmd")
                    End If
                End If
            Next
        Else
            MsgBox("Missing update config.", MsgBoxStyle.Critical, Title:="GDnetworks - Error! (updates_file)")
        End If

    End Sub

    'TOP TOOL BAR (CMR)
    Private Sub CMRToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CMRToolStripMenuItem.Click
        Process.Start(set_browser, "https://docs.google.com/spreadsheets/d/14-gC5LLy1o6EbBSPO6z6ZihCly1UXXzvnjdnN7jB5IA/edit#gid=0")
    End Sub

    'TOP TOOL BAR (CALENDAR: Tech Support)
    Private Sub TechSupportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TechSupportToolStripMenuItem.Click
        Process.Start(set_browser, "https://calendar.google.com/calendar/embed?src=dynamite.ca_8lv4c57o207mii1l5i3io2oct0%40group.calendar.google.com")
    End Sub

    'TOP TOOL BAR (CALENDAR: DBA Support)
    Private Sub DBAToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DBAToolStripMenuItem.Click
        Process.Start(set_browser, "https://calendar.google.com/calendar/embed?src=7mldhtc9hf1cbeqvlo1haiogh73l4rre%40import.calendar.google.com")
    End Sub

    'TOP TOOL BAR (CALENDAR: Server Support)
    Private Sub ServerSupportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ServerSupportToolStripMenuItem.Click
        Process.Start(set_browser, "https://calendar.google.com/calendar/embed?src=scun9qb7sq6oj9ic2tl6icluoovuqavq%40import.calendar.google.com")
    End Sub

    'TOP TOOL BAR (CALENDAR: ORPOS Support)
    Private Sub ORPOSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ORPOSToolStripMenuItem.Click
        Process.Start(set_browser, "https://calendar.google.com/calendar/embed?src=vbe3h3cfiehlme3e0u92lps10ck0coq3%40import.calendar.google.com")
    End Sub

    'TOP TOOL BAR (RSG)
    Private Sub RSGToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RSGToolStripMenuItem.Click
        Process.Start(set_browser, "https://trackit.retailsgi.com/TrackItWeb/SelfService/Account/LogIn?ReturnUrl=%2fTrackItWeb%2fSelfService%2fApplication%2fMain")
    End Sub

    'TOP TOOL BAR (SHIPPING: Canada Post)
    Private Sub CanadaPostToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CanadaPostToolStripMenuItem.Click
        Process.Start(set_browser, "https://www.canadapost.ca/cpotools/apps/track/personal/findByTrackNumber?execution=e1s1")
    End Sub

    'TOP TOOL BAR (SHIPPING: Canpar)
    Private Sub CanparToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CanparToolStripMenuItem.Click
        Process.Start(set_browser, "https://www.canpar.ca/en/track/tracking.jsp")
    End Sub

    'TOP TOOL BAR (SHIPPING: FexEX)
    Private Sub FedExToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FedExToolStripMenuItem.Click
        Process.Start(set_browser, "https://www.fedex.com/en-us/tracking.html")
    End Sub

    'TOP TOOL BAR (SHIPPING: Puro)
    Private Sub PurolatorToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PurolatorToolStripMenuItem.Click
        Process.Start(set_browser, "https://www.purolator.com/en/ship-track/tracking-summary.page?")
    End Sub

    'TOP TOOL BAR (SHIPPING: UPS)
    Private Sub UPSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UPSToolStripMenuItem.Click
        Process.Start(set_browser, "https://www.ups.com/WebTracking/track?loc=en_CA")
    End Sub

    'TOP TOOL BAR (ABOUT form)
    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        form_about.ShowDialog()
    End Sub

    'TOP TOOL BAR (Keyboard Shortcuts)
    Private Sub KeyboardShortcutsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeyboardShortcutsToolStripMenuItem.Click
        Process.Start("C:\Program Files\GDnetworks\help\shortcuts.html")
    End Sub

    'TOP TOOL BAR (Help)
    Private Sub HelpF1ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HelpF1ToolStripMenuItem.Click
        Process.Start("C:/Program Files/GDnetworks/help/help.html")
    End Sub

    'TOP TOOL BAR (Reload)
    Private Sub ReloadToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReloadToolStripMenuItem.Click
        gdx_main_Load(Me, New System.EventArgs)
    End Sub

    'TOP TOOL BAR (Store - EDIT)
    Private Sub EditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditToolStripMenuItem.Click

        'check if store is loaded
        If store_loaded = True Then

            Dim edit_result As Integer = MsgBox("Do you want to edit STORE: " & lbl_strno_load.Text, MsgBoxStyle.YesNo, Title:="GDnetworks - store EDIT")
            If edit_result = DialogResult.No Then
                'NOTHING
            Else
                Store_edit.ShowDialog()
            End If

        Else
            MsgBox("Please search for a store to EDIT!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (selection)")
        End If

    End Sub

    'TOP TOOL BAR (Store - DELETE)
    Private Sub DeleteStoreToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteStoreToolStripMenuItem.Click

        'check if store is loaded
        If store_loaded = True Then

            Dim delete_result As Integer = MsgBox("Do you want to delete STORE: " & txtbox_storeno.Text, MsgBoxStyle.YesNo, Title:="GDnetworks - store DELETE")
            If delete_result = DialogResult.No Then
                'NOTHING
            Else
                deleteStore(txtbox_storeno.Text)
            End If

        Else
            MsgBox("Please search for a store to DELETE!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (selection)")
        End If

    End Sub

    'TOP TOOL BAR (Store - NEW)
    Private Sub AddToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddToolStripMenuItem.Click
        Store_new.ShowDialog()
    End Sub


    'BUTTON: CLEAR
    Private Sub btn_clear_store_Click(sender As Object, e As EventArgs) Handles btn_clear_store.Click
        txtbox_storeno.Clear()              'clear searchbox
        txtbox_storeno.Focus()              'get cursor in searchbox
        clearall()                          'clear all info
        statusMainButtons("disable")
    End Sub


    'TXTBOX: SEARCH (SHORTCUT KEYS: enter and del)
    Private Sub txtbox_storeno_KeyDown(sender As Object, e As KeyEventArgs) Handles txtbox_storeno.KeyDown

        'on ENTER search store
        If e.KeyCode = Keys.Enter Then
            e.SuppressKeyPress = True
            btn_search_store.PerformClick()
            'on DEL clear form
        ElseIf e.KeyCode = Keys.Delete Then
            e.SuppressKeyPress = True
            btn_clear_store.PerformClick()
        End If

    End Sub

    'Inventory selectedStore flag
    Protected Friend strInvFlag As Boolean = False

    'ON CLICK: ping Router
    Private Sub lbl_router_load_Click(sender As Object, e As EventArgs) Handles lbl_router_load.Click
        If Not cmd_router = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_router.ToString + " -t")
            Dim rtr_replace As Integer = MsgBox("Was the ROUTER replaced?", MsgBoxStyle.YesNo, Title:="GDnetworks - Replacement: ROUTER")
            If rtr_replace = DialogResult.No Then
                'NOTHING
            Else
                'select TAB based on store device
                Dim tabselect As New Inventory
                tabselect.tab_inventory.SelectedIndex = 0
                tabselect.selectedDevice = "RTR"
                strInvFlag = True           'flag ON (load store)
                tabselect.ShowDialog()
                strInvFlag = False          'flag OFF (reset)
            End If
        End If
    End Sub

    'ON CLICK: ping SW1
    Private Sub lbl_switch1_load_Click(sender As Object, e As EventArgs) Handles lbl_switch1_load.Click
        If Not cmd_sw1 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_sw1.ToString + " -t")
            Dim sw_replace As Integer = MsgBox("Was the SWITCH1 replaced?", MsgBoxStyle.YesNo, Title:="GDnetworks - Replacement: SWITCH1")
            If sw_replace = DialogResult.No Then
                'NOTHING
            Else
                'open inventory on Switch tab
                Dim tabselect As New Inventory
                tabselect.tab_inventory.SelectedIndex = 1
                tabselect.selectedDevice = "SW"
                strInvFlag = True           'flag ON (load store)
                tabselect.ShowDialog()
                strInvFlag = False          'flag OFF (reset)
            End If
        End If
    End Sub

    'ON CLICK: ping SW2
    Private Sub lbl_switch2_load_Click(sender As Object, e As EventArgs) Handles lbl_switch2_load.Click
        If Not cmd_sw2 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_sw2.ToString + " -t")
            Dim sw_replace As Integer = MsgBox("Was the SWITCH2 replaced?", MsgBoxStyle.YesNo, Title:="GDnetworks - Replacement: SWITCH2")
            If sw_replace = DialogResult.No Then
                'NOTHING
            Else
                'open inventory on Switch tab
                Dim tabselect As New Inventory
                tabselect.tab_inventory.SelectedIndex = 1
                tabselect.selectedDevice = "SW"
                strInvFlag = True           'flag ON (load store)
                tabselect.ShowDialog()
                strInvFlag = False          'flag OFF (reset)
            End If
        End If
    End Sub

    'ON CLICK: ping AP1
    Private Sub lbl_ap1_load_Click(sender As Object, e As EventArgs) Handles lbl_ap1_load.Click
        If Not cmd_ap1 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_ap1.ToString + " -t")
            Dim ap_replace As Integer = MsgBox("Was the AP1 replaced?", MsgBoxStyle.YesNo, Title:="GDnetworks - Replacement: AP1")
            If ap_replace = DialogResult.No Then
                'NOTHING
            Else
                'open inventory on Switch tab
                Dim tabselect As New Inventory
                tabselect.tab_inventory.SelectedIndex = 2
                tabselect.selectedDevice = "AP"
                strInvFlag = True           'flag ON (load store)
                tabselect.ShowDialog()
                strInvFlag = False          'flag OFF (reset)
            End If
        End If
    End Sub

    'ON CLICK: ping AP2
    Private Sub lbl_ap2_load_Click(sender As Object, e As EventArgs) Handles lbl_ap2_load.Click
        If Not cmd_ap2 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_ap2.ToString + " -t")
            Dim ap_replace As Integer = MsgBox("Was the AP2 replaced?", MsgBoxStyle.YesNo, Title:="GDnetworks - Replacement: AP2")
            If ap_replace = DialogResult.No Then
                'NOTHING
            Else
                'open inventory on Switch tab
                Dim tabselect As New Inventory
                tabselect.tab_inventory.SelectedIndex = 2
                tabselect.selectedDevice = "AP"
                strInvFlag = True           'flag ON (load store)
                tabselect.ShowDialog()
                strInvFlag = False          'flag OFF (reset)
            End If
        End If
    End Sub

    'ON CLICK: ping TC
    Private Sub lbl_tc_load_Click(sender As Object, e As EventArgs) Handles lbl_tc_load.Click
        If Not cmd_tc = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_tc.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping Music
    Private Sub lbl_music_load_Click(sender As Object, e As EventArgs) Handles lbl_music_load.Click
        If Not cmd_music = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_music.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping IPAD
    Private Sub lbl_ipad_load_Click(sender As Object, e As EventArgs) Handles lbl_ipad_load.Click
        If Not cmd_ipad = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_ipad.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping Samsung
    Private Sub lbl_samsung_load_Click(sender As Object, e As EventArgs) Handles lbl_samsung_load.Click
        If Not cmd_samsung = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_samsung.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping CASH1
    Private Sub lbl_cash1_load_Click(sender As Object, e As EventArgs) Handles lbl_cash1_load.Click
        If Not cmd_c1 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_c1.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping CASH2
    Private Sub lbl_cash2_load_Click(sender As Object, e As EventArgs) Handles lbl_cash2_load.Click
        If Not cmd_c2 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_c2.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping CASH3
    Private Sub lbl_cash3_load_Click(sender As Object, e As EventArgs) Handles lbl_cash3_load.Click
        If Not cmd_c3 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_c3.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping CASH4
    Private Sub lbl_cash4_load_Click(sender As Object, e As EventArgs) Handles lbl_cash4_load.Click
        If Not cmd_c4 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_c4.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping CASH5
    Private Sub lbl_cash5_load_Click(sender As Object, e As EventArgs) Handles lbl_cash5_load.Click
        If Not cmd_c5 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_c5.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping CASH6
    Private Sub lbl_cash6_load_Click(sender As Object, e As EventArgs) Handles lbl_cash6_load.Click
        If Not cmd_c6 = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_c6.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping BO
    Private Sub lbl_bo_load_Click(sender As Object, e As EventArgs) Handles lbl_bo_load.Click
        If Not cmd_bo = "" Then
            Process.Start("cmd.exe", "/K ping " + cmd_bo.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping PINPAD1
    Private Sub pic_pinpad_c1_Click(sender As Object, e As EventArgs) Handles pic_pinpad_c1.Click
        If Not pic_pinpad_c1.Image Is Nothing Then
            Process.Start("cmd.exe", "/K ping " + pp_c1.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping PINPAD2
    Private Sub pic_pinpad_c2_Click(sender As Object, e As EventArgs) Handles pic_pinpad_c2.Click
        If Not pic_pinpad_c2.Image Is Nothing Then
            Process.Start("cmd.exe", "/K ping " + pp_c2.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping PINPAD3
    Private Sub pic_pinpad_c3_Click(sender As Object, e As EventArgs) Handles pic_pinpad_c3.Click
        If Not pic_pinpad_c3.Image Is Nothing Then
            Process.Start("cmd.exe", "/K ping " + pp_c3.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping PINPAD4
    Private Sub pic_pinpad_c4_Click(sender As Object, e As EventArgs) Handles pic_pinpad_c4.Click
        If Not pic_pinpad_c4.Image Is Nothing Then
            Process.Start("cmd.exe", "/K ping " + pp_c4.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping PINPAD5
    Private Sub pic_pinpad_c5_Click(sender As Object, e As EventArgs) Handles pic_pinpad_c5.Click
        If Not pic_pinpad_c5.Image Is Nothing Then
            Process.Start("cmd.exe", "/K ping " + pp_c5.ToString + " -t")
        End If
    End Sub

    'ON CLICK: ping PINPAD6
    Private Sub pic_pinpad_c6_Click(sender As Object, e As EventArgs) Handles pic_pinpad_c6.Click
        If Not pic_pinpad_c6.Image Is Nothing Then
            Process.Start("cmd.exe", "/K ping " + pp_c6.ToString + " -t")
        End If
    End Sub

    'BUTTON: OPEN DEVICE STATUS LIST
    Private Sub btn_list_status_Click(sender As Object, e As EventArgs) Handles btn_list_status.Click
        form_devices_list.ShowDialog()
    End Sub

    Private Sub lbl_sis_store_load_Click(sender As Object, e As EventArgs) Handles lbl_sis_store_load.Click
        txtbox_storeno.Text = lbl_sis_store_load.Text
        btn_search_store.PerformClick()
    End Sub

    'BUTTON: Router CLI
    Private Sub btn_cli_router_Click(sender As Object, e As EventArgs) Handles btn_cli_router.Click

        'check if store is loaded
        If store_loaded = False Then
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        Else
            'Check if putty is installed
            If putty = False Then
                MsgBox("You cannot use this function. Putty not found.", MsgBoxStyle.Information, Title:="GDnetworks - Error! (putty missing)")
            Else
                lunchPutty(cmd_router)
            End If
        End If

    End Sub

    'BUTTON: Switch1 CLI
    Private Sub btn_cli_sw1_Click(sender As Object, e As EventArgs) Handles btn_cli_sw1.Click

        'check if store is loaded
        If store_loaded = False Then
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        Else
            'Check if putty is installed
            If putty = False Then
                MsgBox("You cannot use this function. Putty not found.", MsgBoxStyle.Information, Title:="GDnetworks - Error! (putty missing)")
            Else
                lunchPutty(cmd_sw1)
            End If
        End If

    End Sub

    'ON CLICK: CASH1 - COPY IP to Clipboard | ON CTRL CLICK: CASH1 - Open Orion with IP | ON SHIFT CLICK: CASH1 - connect to C drive
    Private Sub lbl_cash1_Click(sender As Object, e As EventArgs) Handles lbl_cash1.Click

        'IF IP String is not empty
        If Not cmd_c1 = "" Then
            'When using SHIFT+(click)Cash1.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    If lbl_cash1_load.Text = "Online" Then
                        openDrive(cmd_c1)
                    Else
                        MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                    End If
                End If
                'When using CTRL+(click)cash1.lbl open Orion with IP
            ElseIf My.Computer.Keyboard.CtrlKeyDown = True Then
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_c1 & "&ResourceID=7")
                End If
                'when using (click)Cash1.lbl copy IP to clipboard 
            Else
                My.Computer.Clipboard.SetText(cmd_c1.ToString)
            End If
        Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'ON CLICK: CASH2 - COPY IP to Clipboard | ON CTRL CLICK: CASH2 - Open Orion with IP | ON SHIFT CLICK: CASH2 - connect to C drive
    Private Sub lbl_cash2_Click(sender As Object, e As EventArgs) Handles lbl_cash2.Click

        'IF IP String is not empty
        If Not cmd_c2 = "" Then
            'When using SHIFT+(click)Cash2.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive

                If lbl_cash2_load.Text = "Online" Then
                    'BLOCK ACCESS TO C DRIVE FOR noaccess
                    If user_level = "noaccess" Then
                        'DO NOTHING
                    Else
                        openDrive(cmd_c2)
                    End If
                Else
                    MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                End If
                'When using CTRL+(click)cash2.lbl open Orion with IP
            ElseIf My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_c2 & "&ResourceID=7")
                End If
                'when using (click)Cash2.lbl copy IP to clipboard             
            Else
                My.Computer.Clipboard.SetText(cmd_c2.ToString)
            End If
        Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'ON CLICK: CASH3 - COPY IP to Clipboard or connect to C drive
    Private Sub lbl_cash3_Click(sender As Object, e As EventArgs) Handles lbl_cash3.Click

        'IF IP String is not empty
        If Not cmd_c3 = "" Then
            'When using SHIFT+(click)Cash3.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    If lbl_cash3_load.Text = "Online" Then
                        openDrive(cmd_c3)

                    Else
                        MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                    End If
                End If
                'When using CTRL+(click)cash3.lbl open Orion with IP
            ElseIf My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_c3 & "&ResourceID=7")
                End If
                'when using (click)Cash3.lbl copy IP to clipboard             
            Else
                My.Computer.Clipboard.SetText(cmd_c3.ToString)
            End If
        Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'ON CLICK: CASH4 - COPY IP to Clipboard or connect to C drive
    Private Sub lbl_cash4_Click(sender As Object, e As EventArgs) Handles lbl_cash4.Click

        'IF IP String is not empty
        If Not cmd_c4 = "" Then
            'When using SHIFT+(click)Cash4.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    If lbl_cash4_load.Text = "Online" Then
                        openDrive(cmd_c4)
                    Else
                        MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                    End If
                End If
                'When using CTRL+(click)cash4.lbl open Orion with IP
            ElseIf My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_c4 & "&ResourceID=7")
                End If
                'when using (click)Cash4.lbl copy IP to clipboard             
            Else
                My.Computer.Clipboard.SetText(cmd_c4.ToString)
            End If
        Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'ON CLICK: CASH5 - COPY IP to Clipboard or connect to C drive
    Private Sub lbl_cash5_Click(sender As Object, e As EventArgs) Handles lbl_cash5.Click

        'IF IP String is not empty
        If Not cmd_c5 = "" Then
            'When using SHIFT+(click)Cash5.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    If lbl_cash5_load.Text = "Online" Then
                        openDrive(cmd_c5)
                    Else
                        MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                    End If
                End If
                'When using CTRL+(click)cash5.lbl open Orion with IP
            ElseIf My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_c5 & "&ResourceID=7")
                End If
                'when using (click)Cash5.lbl copy IP to clipboard             
            Else
                My.Computer.Clipboard.SetText(cmd_c5.ToString)
            End If
        Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'ON CLICK: CASH6 - COPY IP to Clipboard or connect to C drive
    Private Sub lbl_cash6_Click(sender As Object, e As EventArgs) Handles lbl_cash6.Click

        'IF IP String is not empty
        If Not cmd_c6 = "" Then
            'When using SHIFT+(click)Cash6.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    If lbl_cash6_load.Text = "Online" Then
                        openDrive(cmd_c6)
                    Else
                        MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                    End If
                End If
                'When using CTRL+(click)cash6.lbl open Orion with IP
            ElseIf My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_c6 & "&ResourceID=7")
                End If
                'when using (click)Cash6.lbl copy IP to clipboard             
            Else
                My.Computer.Clipboard.SetText(cmd_c6.ToString)
            End If
        Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'ON CLICK: BO - COPY IP to Clipboard or connect to C drive
    Private Sub lbl_bo_Click(sender As Object, e As EventArgs) Handles lbl_bo.Click

        'IF IP String is not empty
        If Not cmd_bo = "" Then
            'When using SHIFT+(click)BO.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    If lbl_bo_load.Text = "Online" Then
                        openDrive(cmd_bo)
                    Else
                        MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                    End If
                End If
                'When using CTRL+(click)bo.lbl open Orion with IP
            ElseIf My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_bo & "&ResourceID=7")
                End If
                'when using (click)BO.lbl copy IP to clipboard
            ElseIf My.Computer.Keyboard.AltKeyDown Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "https://" + cmd_bo + ":7002/backoffice")
                End If
            Else
                My.Computer.Clipboard.SetText(cmd_bo.ToString)
            End If
        Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'BUTTON: Store Hours
    Private Sub btn_store_hours_Click(sender As Object, e As EventArgs) Handles btn_store_hours.Click
        Try
            form_store_hours.ShowDialog()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub lbl_str_no_Click(sender As Object, e As EventArgs) Handles lbl_str_no.Click

    End Sub

    'BUTTON: Switch2 CLI
    Private Sub btn_cli_sw2_Click(sender As Object, e As EventArgs) Handles btn_cli_sw2.Click

        'check if store is loaded
        If store_loaded = False Then
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        Else
            'Check if putty is installed
            If putty = False Then
                MsgBox("You cannot use this function. Putty not found.", MsgBoxStyle.Information, Title:="GDnetworks - Error! (putty missing)")
            ElseIf cmd_sw2 = "" Then
                MsgBox("Switch 2 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (SW2 n/a)")
            Else
                lunchPutty(cmd_sw2)
            End If
        End If

    End Sub

    'BUTTON: Router GUI
    Private Sub btn_gui_router_Click(sender As Object, e As EventArgs) Handles btn_gui_router.Click
        If store_loaded = False Then
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        Else
            Process.Start(set_browser, "https://" + cmd_router + "/login")
        End If
    End Sub

    'ON CLICK: Router - COPY IP to Clipboard |ON CTRL CLICK: ROUTER - Open Orion with IP
    Private Sub lbl_net_rtr_Click(sender As Object, e As EventArgs) Handles lbl_net_rtr.Click

        'IF IP String is not empty
        If Not cmd_router = "" Then
            'When using CTRL+(click)router.lbl open Orion with IP
            If My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_router & "&ResourceID=7")
                End If
            Else
                My.Computer.Clipboard.SetText(cmd_router.ToString)
            End If
        Else
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        End If

    End Sub

    'ON CLICK: SW1 - COPY IP to Clipboard | ON CTRL CLICK: SWITCH1 - Open Orion with IP
    Private Sub lbl_net_sw1_Click(sender As Object, e As EventArgs) Handles lbl_net_sw1.Click

        'IF IP String is not empty
        If Not cmd_sw1 = "" Then
            'When using CTRL+(click)switch1.lbl open Orion with IP
            If My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_sw1 & "&ResourceID=7")
                End If
            Else
                    My.Computer.Clipboard.SetText(cmd_sw1.ToString)
            End If
        Else
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        End If

    End Sub

    'ON CLICK: SW2 - COPY IP to Clipboard | ON CTRL CLICK: SWITCH2 - Open Orion with IP
    Private Sub lbl_net_sw2_Click(sender As Object, e As EventArgs) Handles lbl_net_sw2.Click

        'IF IP String is not empty
        If Not cmd_sw2 = "" Then
            'When using CTRL+(click)switch2.lbl open Orion with IP
            If My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_sw2 & "&ResourceID=7")
                End If
            Else
                    My.Computer.Clipboard.SetText(cmd_sw2.ToString)
            End If
        ElseIf lbl_switch2_load.Text = "N/A" Then
            MsgBox("Switch 2 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (SW2 n/a)")
        Else
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        End If

    End Sub

    'ON CLICK: AP1 - COPY IP to Clipboard | ON CTRL CLICK: AP1 - Open Orion with IP
    Private Sub lbl_ap1_Click(sender As Object, e As EventArgs) Handles lbl_ap1.Click

        'IF IP String is not empty
        If Not cmd_ap1 = "" Then
            'When using CTRL+(click)ap1.lbl open Orion with IP
            If My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_ap1 & "&ResourceID=7")
                End If
            Else
                    My.Computer.Clipboard.SetText(cmd_ap1.ToString)
            End If
        Else
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        End If

    End Sub

    'ON CLICK: AP2 - COPY IP to Clipboard | ON CTRL CLICK: AP2 - Open Orion with IP
    Private Sub lbl_ap2_Click(sender As Object, e As EventArgs) Handles lbl_ap2.Click

        'IF IP String is not empty
        If Not cmd_ap2 = "" Then
            'When using CTRL+(click)ap2.lbl open Orion with IP
            If My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_ap2 & "&ResourceID=7")
                End If
            Else
                    My.Computer.Clipboard.SetText(cmd_ap2.ToString)
            End If
        ElseIf lbl_ap2_load.Text = "N/A" Then
            MsgBox("AP2 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (AP2 n/a)")
        Else
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        End If

    End Sub

    'ON CLICK: TC - COPY IP to Clipboard | ON CTRL CLICK: TC - Open Orion with IP | ON SHIFT OPEN PRODCO
    Private Sub lbl_tc_Click(sender As Object, e As EventArgs) Handles lbl_tc.Click

        'IF IP String is not empty
        If Not cmd_tc = "" Then
            'When using CTRL+(click)tc.lbl open Orion with IP
            If My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_tc & "&ResourceID=7")
                End If
            ElseIf My.Computer.Keyboard.AltKeyDown = True Then
                    TrafficCounterToolStripMenuItem.PerformClick()
            Else
                My.Computer.Clipboard.SetText(cmd_tc.ToString)
            End If
        Else
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        End If

    End Sub


    'ON CLICK: Music - COPY IP to Clipboard | ON CTRL CLICK: MUSIC  - Open Orion with IP
    Private Sub lbl_music_Click(sender As Object, e As EventArgs) Handles lbl_music.Click

        'IF IP String is not empty
        If Not cmd_music = "" Then
            'When using CTRL+(click)music.lbl open Orion with IP
            If My.Computer.Keyboard.CtrlKeyDown = True Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                If user_level = "noaccess" Then
                    'DO NOTHING
                Else
                    Process.Start(set_browser, "http://samont01.corp.gdglobal.ca/Orion/NetPerfMon/Resources/NodeSearchResults.aspx?Property=IPAddress&SearchText=" & cmd_music & "&ResourceID=7")
                End If
            Else
                    My.Computer.Clipboard.SetText(cmd_music.ToString)
            End If
        Else
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        End If

    End Sub

    'ON CTRL CLICK: IPAD  - Open Orion with IP ( NOT IN ORION )
    Private Sub lbl_ipad_Click(sender As Object, e As EventArgs) Handles lbl_ipad.Click
        If Not cmd_ipad = "" Then
            My.Computer.Clipboard.SetText(cmd_ipad.ToString)
        End If
    End Sub

    'ON CTRL CLICK: Samsung  - Open Orion with IP ( NOT IN ORION )
    Private Sub lbl_stbl_Click(sender As Object, e As EventArgs) Handles lbl_stbl.Click
        If Not cmd_samsung = "" Then
            My.Computer.Clipboard.SetText(cmd_samsung.ToString)
        End If
    End Sub

    'ON CLICK: STORE INFO REFRESH RATE 0 SECS - NO UPDATES
    Private Sub SecNoUpdatesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles topmenu_refresh_0sec.Click

        ping_timer.Enabled = False
        ping_timer.Stop()

        'Load Menu text
        topmenu_refresh_0sec.Text = "0sec - no updates " & ChrW(&H2713)
        topmenu_refresh_10sec.Text = "10sec"
        topmenu_refresh_30sec.Text = "30sec"
        topmenu_refresh_60sec.Text = "60sec"

        'UPDATE USER DB
        update_user(getUsername(), "Refresh Rate", "0")

    End Sub

    'ON CLICK: STORE INFO REFRESH RATE 10 SECS
    Private Sub SecToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles topmenu_refresh_10sec.Click

        ping_timer.Interval = 10000
        ping_timer.Enabled = True
        ping_timer.Start()

        'load menu text
        topmenu_refresh_0sec.Text = "0sec - no updates"
        topmenu_refresh_10sec.Text = "10sec " & ChrW(&H2713)
        topmenu_refresh_30sec.Text = "30sec"
        topmenu_refresh_60sec.Text = "60sec"

        'UPDATE USER DB
        update_user(getUsername(), "Refresh Rate", "10")

    End Sub


    'ON CLICK: STORE INFO REFRESH RATE 30 SECS
    Private Sub SecToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles topmenu_refresh_30sec.Click

        ping_timer.Interval = 30000
        ping_timer.Enabled = True
        ping_timer.Start()

        'load menu text
        topmenu_refresh_0sec.Text = "0sec - no updates"
        topmenu_refresh_10sec.Text = "10sec"
        topmenu_refresh_30sec.Text = "30sec " & ChrW(&H2713)
        topmenu_refresh_60sec.Text = "60sec"

        'UPDATE USER DB
        update_user(getUsername(), "Refresh Rate", "30")

    End Sub

    'ON CLICK: STORE INFO REFRESH RATE 60 SEC
    Private Sub SecToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles topmenu_refresh_60sec.Click

        ping_timer.Interval = 60000
        ping_timer.Enabled = True
        ping_timer.Start()

        'load menu text
        topmenu_refresh_0sec.Text = "0sec - no updates"
        topmenu_refresh_10sec.Text = "10sec"
        topmenu_refresh_30sec.Text = "30sec"
        topmenu_refresh_60sec.Text = "60sec " & ChrW(&H2713)

        'UPDATE USER DB
        update_user(getUsername(), "Refresh Rate", "60")

    End Sub

    'SET PING REFRESH TIMER
    Private Sub ping_timer_Tick(sender As Object, e As EventArgs) Handles ping_timer.Tick

        If store_loaded = False Then
            'if store not loaded do nothing
        Else
            'btn_search_store.PerformClick()  
            form_devices_list.listview_devices.Items.Clear()
            PingConnection(IPAddress, isCombo, hasSwitch2, hasAP2)
        End If

    End Sub

    'SET APP RUNNING TIMER ( 8H MAX )
    Private Sub app_running_timer_Tick(sender As Object, e As EventArgs) Handles app_running_timer.Tick

        MyBase.Close()

    End Sub


    'BUTTON: Switch1 GUI
    Private Sub btn_gui_sw1_Click(sender As Object, e As EventArgs) Handles btn_gui_sw1.Click
        If store_loaded = False Then
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        Else
            Process.Start(set_browser, "https://" + cmd_sw1 + "/login")
        End If
    End Sub

    'REFRESH TIME: get latest info from DB
    Private Sub lbl_refresh_load_Click(sender As Object, e As EventArgs) Handles lbl_refresh_load.Click

        ReloadToolStripMenuItem.PerformClick()  'Perform Reload page

    End Sub


    'BUTTON: Switch2 GUI
    Private Sub btn_gui_sw2_Click(sender As Object, e As EventArgs) Handles btn_gui_sw2.Click
        If store_loaded = False Then
            MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
        Else
            'check if SW2 is available
            If Not cmd_sw2 = "" Then
                Process.Start(set_browser, "https://" + cmd_sw2 + "/login")
            Else
                MsgBox("SW22 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (SW2 n/a)")
            End If
        End If
    End Sub


    'ON CLICK: LOAD GDNET.INFO
    Private Sub pic_gdx_main_Click(sender As Object, e As EventArgs) Handles pic_gdx_main.Click
        Process.Start("www.gdnet.info")
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click
        btn_search_store.PerformClick()
    End Sub


    'BUTTON: Cash1 TeamViewer
    Private Sub btn_tv_c1_Click(sender As Object, e As EventArgs) Handles btn_tv_c1.Click
        'check if cash1 exists
        If Not cmd_c2 = "" Then
            'lunch with Teamviewer
            If set_remote = "teamviewer" Then
                lunchTeamViewer(cmd_c1)
                'lunch with RDP
            ElseIf set_remote = "rdp" Then
                lunchRemoteDesktop(cmd_c1)
            ElseIf set_remote = "psexec" Then
                lunchPsExec(cmd_c1)
            End If
        Else
            MsgBox("Cash1 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (C1 n/a)")
        End If
    End Sub

    'BUTTON: Cash2 TeamViewer
    Private Sub btn_tv_c2_Click(sender As Object, e As EventArgs) Handles btn_tv_c2.Click
        'check if cash2 exists
        If Not cmd_c2 = "" Then
            'lunch with Teamviewer
            If set_remote = "teamviewer" Then
                lunchTeamViewer(cmd_c2)
                'lunch with RDP
            ElseIf set_remote = "rdp" Then
                lunchRemoteDesktop(cmd_c2)
            ElseIf set_remote = "psexec" Then
                lunchPsExec(cmd_c2)
            End If
        Else
            MsgBox("Cash2 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (C2 n/a)")
        End If
    End Sub

    'BUTTON: Cash3 TeamViewer
    Private Sub btn_rv_c3_Click(sender As Object, e As EventArgs) Handles btn_tv_c3.Click
        'check if cash3 exists
        If Not cmd_c3 = "" Then
            'lunch with Teamviewer
            If set_remote = "teamviewer" Then
                lunchTeamViewer(cmd_c3)
                'lunch with RDP
            ElseIf set_remote = "rdp" Then
                lunchRemoteDesktop(cmd_c3)
            ElseIf set_remote = "psexec" Then
                lunchPsExec(cmd_c3)
            End If
        Else
            MsgBox("Cash3 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (C3 n/a)")
        End If
    End Sub

    'BUTTON: Cash4 TeamViewer
    Private Sub btn_tv_c4_Click(sender As Object, e As EventArgs) Handles btn_tv_c4.Click
        'check if cash4 exists
        If Not cmd_c4 = "" Then
            'lunch with Teamviewer
            If set_remote = "teamviewer" Then
                lunchTeamViewer(cmd_c4)
                'lunch with RDP
            ElseIf set_remote = "rdp" Then
                lunchRemoteDesktop(cmd_c4)
            ElseIf set_remote = "psexec" Then
                lunchPsExec(cmd_c4)
            End If
        Else
            MsgBox("Cash4 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (C4 n/a)")
        End If
    End Sub

    'BUTTON: Cash5 TeamViewer
    Private Sub btn_tv_c5_Click(sender As Object, e As EventArgs) Handles btn_tv_c5.Click
        'check if cash5 exists
        If Not cmd_c5 = "" Then
            'lunch with Teamviewer
            If set_remote = "teamviewer" Then
                lunchTeamViewer(cmd_c5)
                'lunch with RDP
            ElseIf set_remote = "rdp" Then
                lunchRemoteDesktop(cmd_c5)
            ElseIf set_remote = "psexec" Then
                lunchPsExec(cmd_c5)
            End If
        Else
            MsgBox("Cash5 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (C5 n/a)")
        End If
    End Sub

    'BUTTON: Cash6 TeamViewer
    Private Sub btn_tv_c6_Click(sender As Object, e As EventArgs) Handles btn_tv_c6.Click
        'check if cash6 exists
        If Not cmd_c6 = "" Then
            'lunch with Teamviewer
            If set_remote = "teamviewer" Then
                lunchTeamViewer(cmd_c6)
                'lunch with RDP
            ElseIf set_remote = "rdp" Then
                lunchRemoteDesktop(cmd_c6)
            ElseIf set_remote = "psexec" Then
                lunchPsExec(cmd_c6)
            End If
        Else
            MsgBox("Cash6 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (C6 n/a)")
        End If
    End Sub

    'BUTTON: BO TeamViewer
    Private Sub btn_tv_bo_Click(sender As Object, e As EventArgs) Handles btn_tv_bo.Click
        'check if bo exists
        If Not cmd_bo = "" Then
            'lunch with Teamviewer
            If set_remote = "teamviewer" Then
                lunchTeamViewer(cmd_bo)
                'lunch with RDP
            ElseIf set_remote = "rdp" Then
                lunchRemoteDesktop(cmd_bo)
            ElseIf set_remote = "psexec" Then
                lunchPsExec(cmd_bo)
            End If
        Else
            MsgBox("BO is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (BO n/a)")
        End If
    End Sub


    'BUTTON: COPY STORE ADDRESS TO CLIPBOARD
    Private Sub btn_cb_address_Click(sender As Object, e As EventArgs) Handles btn_cb_address.Click

        If store_loaded = False Then
            MsgBox("You must search for a store first!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (copy address)")
        Else
            Dim store_address As String = lbl_address_load.Text & ", " & lbl_city_load.Text & ", " & lbl_state_load.Text & ", " & lbl_country_load.Text & ", " & lbl_postal_load.Text
            My.Computer.Clipboard.SetText(store_address)
            MsgBox("Address in clipboard. Use CTRL+V to paste it.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (copy address)")
        End If

    End Sub

    'BUTTON: OPEN STORE HW PICS
    Private Sub HWPicsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HWPicsToolStripMenuItem.Click

        Dim hw_location As String = "\\sagdxh01\GDnetworks\HW pics\" & lbl_strno_load.Text

        'Create store folder if it does not exist
        If (Not System.IO.Directory.Exists(hw_location)) Then
            System.IO.Directory.CreateDirectory(hw_location)
        End If

        'open folder
        Process.Start(hw_location)

    End Sub


    'AUTO UPDATES ON
    Private Sub OnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OnToolStripMenuItem.Click

        OnToolStripMenuItem.Text = "On " & ChrW(&H2713)
        OffToolStripMenuItem.Text = "Off"
        update_user(getUsername(), "Auto Update", 1)

    End Sub

    'AUTO UPDATES OFF
    Private Sub OffToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OffToolStripMenuItem.Click

        OnToolStripMenuItem.Text = "On"
        OffToolStripMenuItem.Text = "Off " & ChrW(&H2713)
        update_user(getUsername(), "Auto Update", 0)

    End Sub



    'COMBO BOX: SEARCH BY
    Private Sub cmbbox_searchby_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_searchby.SelectedIndexChanged

        If cmbbox_searchby.SelectedItem = "Address" Then
            tooltip.SetToolTip(cmbbox_searchby, "Input Address")
        ElseIf cmbbox_searchby.SelectedItem = "Banner" Then
            tooltip.SetToolTip(cmbbox_searchby, "Input Banner")
        ElseIf cmbbox_searchby.SelectedItem = "Group" Then
            tooltip.SetToolTip(cmbbox_searchby, "Input Group" & vbNewLine & "MT - Maritimes" _
                                                             & vbNewLine & "GQ - Garage Quebec" _
                                                             & vbNewLine & "DQ - Dynamite Quebec" _
                                                             & vbNewLine & "GO - Garage Ontario" _
                                                             & vbNewLine & "DO - Dynamite Ontario" _
                                                             & vbNewLine & "WC - West Canada" _
                                                             & vbNewLine & "CA - Canada" _
                                                             & vbNewLine & "US - US")
        ElseIf cmbbox_searchby.SelectedItem = "Province" Then
            tooltip.SetToolTip(cmbbox_searchby, "Input Province")
        ElseIf cmbbox_searchby.SelectedItem = "Store" Then
            tooltip.SetToolTip(cmbbox_searchby, "Input Store#")
        End If


    End Sub

    Private Sub MyCalendarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MyCalendarToolStripMenuItem.Click
        Process.Start(set_browser, "https://calendar.google.com/calendar/r/month")
    End Sub

    'OPEN FORTI TOKEN IF AVAILABLE
    Private Sub lbl_vpn_con_load_Click(sender As Object, e As EventArgs) Handles lbl_vpn_con_load.Click

        Dim fortitoken_location As String = "C:\Program Files (x86)\Fortinet\FortiClient"

        If (Not System.IO.Directory.Exists(fortitoken_location)) Then
            MsgBox("Cannot locate your fortiClient. Please open it manually or install it if missing!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (fortiClient)")
        Else
            Process.Start(fortitoken_location & "\FortiClient.exe")
        End If

    End Sub

    Private Sub BrowserToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BrowserToolStripMenuItem.Click

    End Sub

    'INVENTORY
    Private Sub InventoryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InventoryToolStripMenuItem.Click

        Inventory.selectedDevice = "RTR" 'preload Routers inventory
        Inventory.ShowDialog()

    End Sub

    'LOCATE STORE ON MAP
    Private Sub OnMapToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OnMapToolStripMenuItem.Click

        If store_loaded = False Then
            MsgBox("You must search for a store first!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (view onMap)")
        Else
            Dim store_address As String = lbl_address_load.Text & " " & lbl_city_load.Text & " " & lbl_state_load.Text & " " & lbl_country_load.Text & " " & lbl_postal_load.Text
            Dim google_address As String = store_address.Replace(" ", "+")
            Process.Start(set_browser, "https://www.google.com/maps/search/?api=1&query=" & google_address)
        End If

    End Sub

    'ON CLICK - MAIN LINE COPY TO CLIPBOARD
    Private Sub lbl_mainl_load_Click(sender As Object, e As EventArgs) Handles lbl_mainl_load.Click

        If Not lbl_mainl_load.Text = "" Then
            My.Computer.Clipboard.SetText(lbl_mainl_load.Text)
        End If

    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles pic_pinpad_c3.Click

    End Sub


    'TOP TOOL BAR (Set RDP as default)
    Private Sub RDPToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RDPToolStripMenuItem.Click
        set_remote = "rdp"
        RDPToolStripMenuItem.Text = "RDP " & ChrW(&H2713)
        TeamViewerToolStripMenuItem1.Text = "Team Viewer"
        PSEXECToolStripMenuItem.Text = "PSEXEC"

        'UPDATE USER DB
        update_user(getUsername(), "Remote", "RDP")

    End Sub

    'TOP TOOL BAR (Set TeamViewer as default)
    Private Sub TeamViewerToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles TeamViewerToolStripMenuItem1.Click, TeamViewerToolStripMenuItem.Click
        set_remote = "teamviewer"
        TeamViewerToolStripMenuItem1.Text = "Team Viewer " & ChrW(&H2713)
        RDPToolStripMenuItem.Text = "RDP"
        PSEXECToolStripMenuItem.Text = "PSEXEC"

        'UPDATE USER DB
        update_user(getUsername(), "Remote", "TV")

    End Sub

    'TOP TOOL BAR (Set PSExec as default)
    Private Sub PSEXECToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PSEXECToolStripMenuItem.Click

        set_remote = "psexec"
        TeamViewerToolStripMenuItem1.Text = "Team Viewer"
        RDPToolStripMenuItem.Text = "RDP"
        PSEXECToolStripMenuItem.Text = "PSEXEC *"

    End Sub

    Private Sub lbl_strno_load_Click(sender As Object, e As EventArgs) Handles lbl_strno_load.Click

    End Sub

    Private Sub grp_devices_Enter(sender As Object, e As EventArgs) Handles grp_devices.Enter

    End Sub

    'BUTTON: SEARCH
    Private Sub btn_search_store_Click(sender As Object, e As EventArgs) Handles btn_search_store.Click


        'clear labels before getting new info
        ClearStoreInfo()                        'clear store info
        ClearNetwork()                          'clear network info
        ClearMoreInfo()                         'clear spare equip info
        form_devices_list.listview_devices.Clear()     'clear ip's list
        btn_moreinfo.Enabled = True             'enable More Info button
        btn_store_hours.Enabled = True          'enable Store Hours button

        'Check SEARCH BY BOX
        'search value
        search_value = txtbox_storeno.Text

        'SEARCH BY ADDRESS
        If cmbbox_searchby.SelectedItem = "Address" Then
            clearall()
            searchby.ShowDialog()
            search_value = Nothing
            'SEARCH BY BANNER
        ElseIf cmbbox_searchby.SelectedItem = "Banner" Then
            clearall()
            searchby.ShowDialog()
            search_value = Nothing
            'SEARCH BY GROUP (MT, DQ, GQ, DO, GO, WC, US)
        ElseIf cmbbox_searchby.SelectedItem = "Group" Then
            clearall()
            searchby.ShowDialog()
            search_value = Nothing
            'SEARCH BY PROVINCE
        ElseIf cmbbox_searchby.SelectedItem = "Province" Then
            clearall()
            searchby.ShowDialog()
            search_value = Nothing
            'SEARCH BY STORE
        ElseIf cmbbox_searchby.SelectedItem = "Store" Then

            'check for invalid input such as characters, letters or no blanks
            If txtbox_storeno.Text = "" Then
                MsgBox("You must enter a store number.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
                clearall()
            ElseIf Not IsNumeric(txtbox_storeno.Text) Then
                MsgBox("Not a valid store number. Only digits allowed!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store#)")
                clearall()
            Else
                LoadInfo(txtbox_storeno.Text)
            End If

        End If

    End Sub

    'BLOCK LETTERS ON TXTBOX STORE NUMBER
    Private Sub txtbox_storeno_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtbox_storeno.KeyPress

        '97 - 122 = Ascii codes for simple letters
        '65 - 90  = Ascii codes for capital letters
        '48 - 57  = Ascii codes for numbers

        'ONLY IF SHIFT OR ALT used as shortcuts ( in combination with letters)
        If My.Computer.Keyboard.ShiftKeyDown Or My.Computer.Keyboard.AltKeyDown Then

            If Asc(e.KeyChar) <> 8 Then
                If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                    e.Handled = True
                End If
            End If

        End If


    End Sub

    'CLEAR ALL INFO
    Private Sub clearall()

        'Clear search box
        txtbox_storeno.Clear()
        txtbox_storeno.Focus()

        ClearStoreInfo()        'clear store info
        ClearNetwork()          'clear network info
        ClearMoreInfo()         'clear more info
        store_loaded = False    'set loaded var to false

        btn_moreinfo.Enabled = False        'disable More Info button
        btn_list_status.Enabled = False     'disable List status button
        btn_store_hours.Enabled = False     'disable Store Hours button


        OrionToolStripMenuItem.Text = "Orion"   'Set Orion to default
        IPAddress = ""                          'Clear IP address from buffer
        listcount = 0

    End Sub

    'CLEAR STORE INFO
    Private Sub ClearStoreInfo()

        'set store status color to default
        lbl_str_no.ForeColor = Color.Black

        'Clear store details
        lbl_strno_load.Text = ""
        lbl_ban_load.Text = ""
        lbl_str_system_load.Text = ""
        lbl_store_iscombo_load.Text = ""
        lbl_sis_store_load.Text = ""
        lbl_mall_load.Text = ""
        lbl_address_load.Text = ""
        lbl_city_load.Text = ""
        lbl_state_load.Text = ""
        lbl_postal_load.Text = ""
        lbl_country_load.Text = ""
        lbl_mainl_load.Text = ""
        lbl_district_load.Text = ""


    End Sub

    'CLEAR NETWORK INFO
    Private Sub ClearNetwork()

        'Clear network 
        lbl_router_load.Text = ""
        lbl_switch1_load.Text = ""
        lbl_switch2_load.Text = ""

        'Clear store devices
        lbl_ap1_load.Text = ""
        lbl_ap2_load.Text = ""
        lbl_tc_load.Text = ""
        lbl_music_load.Text = ""
        lbl_ipad_load.Text = ""
        lbl_samsung_load.Text = ""

        'Clear store cashes
        lbl_cash1_load.Text = ""
        lbl_cash2_load.Text = ""
        lbl_cash3_load.Text = ""
        lbl_cash4_load.Text = ""
        lbl_cash5_load.Text = ""
        lbl_cash6_load.Text = ""
        lbl_bo_load.Text = ""

        IPAddress = Nothing
        cmd_router = ""
        cmd_sw1 = ""
        cmd_sw2 = ""
        cmd_ap1 = ""
        cmd_ap2 = ""
        cmd_tc = ""
        cmd_music = ""
        cmd_ipad = ""
        cmd_samsung = ""
        cmd_c1 = ""
        cmd_c2 = ""
        cmd_c3 = ""
        cmd_c4 = ""
        cmd_c5 = ""
        cmd_c6 = ""
        cmd_bo = ""

        'clear pinpad pings
        pic_pinpad_c1.Image = Nothing
        pic_pinpad_c2.Image = Nothing
        pic_pinpad_c3.Image = Nothing
        pic_pinpad_c4.Image = Nothing
        pic_pinpad_c5.Image = Nothing
        pic_pinpad_c6.Image = Nothing

    End Sub

    'CLEAR MORE INFO
    Private Sub ClearMoreInfo()

        'Clear more info
        lbl_remote_load.Text = ""
        lbl_spare_equip_load.Text = ""
        lbl_store_speed_load.Text = ""
        lbl_wifi_load.Text = ""

    End Sub

    'Load store info from database
    Private Sub LoadInfo(ByVal STORE As Integer)

        Dim store_exists As Boolean = False

        'DB CONNECTION VARS
        Dim str_connection As SqlConnection
        Dim cmd_str As SqlCommand
        Dim sql_store_info As String = "Select * from stores_info where (Store = " & STORE & ")"
        Dim sql_store_network As String = "Select * from stores_network where (Store = " & STORE & ")"
        Dim sql_store_spare As String = "Select * from stores_spare_equip where (Store = " & STORE & ")"
        str_connection = New SqlConnection(connectionString)

        'GET STORE INFO FROM stores_info DB
        Try
            'set connection vars
            str_connection.Open()
            cmd_str = New SqlCommand(sql_store_info, str_connection)
            Dim storeReader As SqlDataReader = cmd_str.ExecuteReader()


            'CHECK IF STORE EXISTS
            If Not storeReader.HasRows Then
                MsgBox("Store does not exist!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store n/a)")
                clearall()
                statusMainButtons("disable")
                store_exists = False
            Else

                store_exists = True
                'READ STORE INFO AND POPULATE FIELDS
                While storeReader.Read()

                    If storeReader("TEMP").Equals(False) Then
                        isTemp = False
                    Else
                        isTemp = True
                    End If

                    If storeReader("Status").Equals(False) Then
                        MsgBox("Store is CLOSED!" & vbNewLine & "Buttons are disabled!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store closed)")
                        lbl_str_no.ForeColor = Color.Red
                        ClearNetwork()      'clear network info
                        ClearMoreInfo()     'clear more info
                        storeStatus = False 'store closed variable

                        store_loaded = False

                        'DISABLE BUTTONS
                        statusMainButtons("disable")

                    Else

                        'set STORELOADED to true
                        store_loaded = True

                        If isTemp = True Then
                            lbl_str_no.ForeColor = Color.DarkOrange
                        Else
                            lbl_str_no.ForeColor = Color.Green
                        End If

                        storeStatus = True  'store open variable

                        'ENABLE BUTTONS (KEEP DISABLE FOR noaccess)
                        If user_level = "noaccess" Then
                            statusMainButtons("disable")
                        Else
                            statusMainButtons("enable")
                        End If
                    End If

                    lbl_strno_load.Text = String.Format("{0:000}", storeReader("Store"))                'store number   
                        loadToolTips(lbl_str_no, "", "store_no")
                        lbl_ban_load.Text = storeReader("Banner").ToString                                  'store banner
                        lbl_str_system_load.Text = storeReader("storeSystem").ToString                      'store system
                        'sister store
                        If IsDBNull(storeReader("Sister Store")) Then
                            lbl_sis_store_load.Text = "N/A"
                        Else
                            lbl_sis_store_load.Text = String.Format("{0:000}", storeReader("Sister Store"))
                            loadToolTips(lbl_sis_store_load, "", "sister_store")                            'sister store tooltip
                        End If
                        lbl_address_load.Text = storeReader("Address").ToString                             'store address
                        lbl_city_load.Text = storeReader("City").ToString                                   'city
                        lbl_state_load.Text = storeReader("Province").ToString                              'Province/State    
                        lbl_postal_load.Text = storeReader("Postal Code").ToString                          'Postal Code
                        lbl_country_load.Text = storeReader("Country").ToString                             'Country
                        lbl_mall_load.Text = storeReader("Mall").ToString                                   'mall 
                        cashes = storeReader("Cashes").ToString                                             'number of cashes

                        'CHECK: if store is COMBO
                        If storeReader("COMBO").Equals(True) Then
                            isCombo = True
                        Else
                            isCombo = False
                        End If

                        If isCombo = True Then
                            lbl_store_iscombo_load.Text = "YES"
                        Else
                            lbl_store_iscombo_load.Text = "NO"
                        End If

                        ' 'CHECK: MAIN LINE
                        If IsDBNull(storeReader("Main Line")) Then
                            lbl_mainl_load.Text = "N/A"
                        Else
                            Dim format_main_line As String = "(" & storeReader("Main Line").ToString.Substring(0, 3) & ") " _
                                  & storeReader("Main Line").ToString.Substring(3, 3) & "-" & storeReader("Main Line").ToString.Substring(6)
                            lbl_mainl_load.Text = format_main_line
                            loadToolTips(lbl_mainl_load, "", "main_line")
                            ' lbl_mainl_load.Text = storeReader("Main Line")
                        End If

                        'CHECK: BO LINE
                        If IsDBNull(storeReader("BO Line")) Then
                            boLine = "N/A"
                        Else
                            Dim format_bo_line As String = "(" & storeReader("BO Line").ToString.Substring(0, 3) & ") " _
                                  & storeReader("BO Line").ToString.Substring(3, 3) & "-" & storeReader("BO Line").ToString.Substring(6)
                            boLine = format_bo_line
                            'boLine = storeReader("BO Line")
                        End If

                        'CHECK: FAX Line
                        If IsDBNull(storeReader("Fax Line")) Then
                            faxLine = "N/A"
                        Else
                            Dim format_fax_line As String = "(" & storeReader("Fax Line").ToString.Substring(0, 3) & ") " _
                              & storeReader("Fax Line").ToString.Substring(3, 3) & "-" & storeReader("Fax Line").ToString.Substring(6)
                            faxLine = format_fax_line
                            'faxLine = storeReader("Fax Line")
                        End If

                        'CHECK: DSL LINE
                        If IsDBNull(storeReader("DSL Line")) Then
                            dslLine = "N/A"
                        Else
                            Dim format_dsl_line As String = "(" & storeReader("DSL Line").ToString.Substring(0, 3) & ") " _
                              & storeReader("DSL Line").ToString.Substring(3, 3) & "-" & storeReader("DSL Line").ToString.Substring(6)
                            dslLine = format_dsl_line
                            'dslLine = storeReader("DSL Line")
                        End If

                        'CHECK: if store is remote
                        If storeReader("Remote").Equals(True) Then
                            lbl_remote_load.Text = "YES"
                        Else
                            lbl_remote_load.Text = "NO"
                        End If

                        pinpadCon = storeReader("pinpadConnection").ToString         'pinpad connection                        


                End While

            End If

            'clear connection vars and close connection
            storeReader.Close()
            str_connection.Close()
            cmd_str.Dispose()

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [stores_info]")
        End Try

        'GET STORE NETWORK INFO FROM stores_network DB
        If storeStatus = False Then

        Else
            Try
                'set connection vars
                str_connection.Open()
                cmd_str = New SqlCommand(sql_store_network, str_connection)
                Dim storeReader As SqlDataReader = cmd_str.ExecuteReader()

                'CHECK IF NETWORK INFO EXISTS
                If Not storeReader.HasRows Then
                    ClearNetwork()
                Else

                    'READ STORE INFO AND POPULATE FIELDS
                    While storeReader.Read()

                        'CHECK: rouyter model
                        If IsDBNull(storeReader("Router")) Then
                            routerModel = "N/A"
                        Else
                            routerModel = storeReader("Router").ToString
                        End If

                        'CHECK: switch1 model
                        If IsDBNull(storeReader("Switch1")) Then
                            sw1Model = "N/A"
                        Else
                            sw1Model = storeReader("Switch1").ToString
                        End If
                        'CHECK: if store has 2nd SWITCH and add model
                        If IsDBNull(storeReader("Switch2")) Then
                            hasSwitch2 = False
                            sw2Model = "N/A"
                        Else
                            hasSwitch2 = True
                            sw2Model = storeReader("Switch2").ToString
                        End If
                        'CHECK: if store has 3rd SWITCH and add model
                        If IsDBNull(storeReader("Switch3")) Then
                            hasSwitch3 = False
                            sw3Model = "N/A"
                        Else
                            hasSwitch3 = True
                            sw3Model = storeReader("Switch3").ToString
                        End If
                        'CHECK: if store has 2nd AP
                        If storeReader("AP2").ToString = "1" Then
                            hasAP2 = True
                        Else
                            hasAP2 = False
                        End If
                        'ISP NAME
                        If IsDBNull(storeReader("ISP")) Then
                            ispName = "N/A"
                        Else
                            ispName = storeReader("ISP").ToString
                        End If

                        'ISP ACCOUNT
                        If IsDBNull(storeReader("Account")) Then
                            ispAccount = "N/A"
                        Else
                            ispAccount = storeReader("Account").ToString
                        End If

                        'ISP IP ASSIGNMENT
                        If IsDBNull(storeReader("ISPstatus")) Then
                            ispStatus = "N/A"
                        Else
                            ispStatus = storeReader("ISPstatus").ToString
                        End If

                        'ISP IP ADDRESS
                        If IsDBNull(storeReader("ISPaddress")) Then
                            ispAddress = "N/A"
                        Else
                            ispAddress = storeReader("ISPaddress").ToString
                        End If

                        'ISP IP SUBNET
                        If IsDBNull(storeReader("ISPsubnet")) Then
                            ispSubnet = "N/A"
                        Else
                            ispSubnet = storeReader("ISPsubnet").ToString
                        End If

                        'ISP IP GATEWAY
                        If IsDBNull(storeReader("ISPgateway")) Then
                            ispGateway = "N/A"
                        Else
                            ispGateway = storeReader("ISPgateway").ToString
                        End If

                        'ISP Username
                        If IsDBNull(storeReader("Username")) Then
                            ispEmail = "N/A"
                        Else
                            ispEmail = storeReader("Username").ToString
                        End If
                        'ISP Password
                        If IsDBNull(storeReader("Password")) Then
                            ispPwd = "N/A"
                        Else
                            ispPwd = storeReader("Password").ToString
                        End If
                        'Router Location
                        If IsDBNull(storeReader("Router Location")) Then
                            routerLocation = "N/A"
                        Else
                            routerLocation = storeReader("Router Location").ToString
                        End If
                        'IT Cabinet Lock
                        If IsDBNull(storeReader("IT Cabinet Lock")) Then
                            itCabinetLock = "N/A"
                        Else
                            itCabinetLock = storeReader("IT Cabinet Lock").ToString
                        End If
                        'ISP Speed
                        If IsDBNull(storeReader("Speed")) Then
                            lbl_store_speed_load.Text = "N/A"
                        Else
                            lbl_store_speed_load.Text = storeReader("Speed").ToString
                        End If
                        'WiFi Speed
                        If IsDBNull(storeReader("WiFi Speed")) Then
                            lbl_wifi_load.Text = "N/A"
                        Else
                            lbl_wifi_load.Text = storeReader("WiFi Speed").ToString
                        End If

                        'ping ips
                        If IsDBNull(storeReader("IP")) Then
                            MsgBox("Unable to PING store! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [stores_network IP]")
                        Else
                            IPAddress = storeReader("IP").ToString  'make the IP available in main
                        End If

                    End While

                    'PING STORE IP
                    PingConnection(IPAddress, isCombo, hasSwitch2, hasAP2)

                End If

                'clear connection vars and close connection
                storeReader.Close()
                str_connection.Close()
                cmd_str.Dispose()

            Catch ex As Exception
                MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [stores_network]")
            End Try
        End If

        'GET STORE SPARE EQUIP INFO FROM stores_spare_equip DB
        If storeStatus = False Then

        Else
            Try
                'set connection vars
                str_connection.Open()
                cmd_str = New SqlCommand(sql_store_spare, str_connection)
                Dim storeReader As SqlDataReader = cmd_str.ExecuteReader()

                'CHECK IF SPARE EQUIP INFO EXISTS
                If Not storeReader.HasRows Then
                    ClearMoreInfo()
                Else



                    'READ STORE INFO AND POPULATE FIELDS
                    While storeReader.Read()

                        'spare modem
                        If storeReader("Modem").Equals(True) Then
                            spModem = "YES"
                        Else
                            spModem = "NO"
                        End If
                        'spare router
                        If storeReader("Router").Equals(True) Then
                            spRouter = "YES"
                        Else
                            spRouter = "NO"
                        End If
                        'spare wireless card
                        If storeReader("Wireless Card").Equals(True) Then
                            spWC = "YES"
                        Else
                            spWC = "NO"
                        End If
                        'spare holiday pc
                        If storeReader("Holiday PC").Equals(True) Then
                            spHC = "YES"
                        Else
                            spHC = "NO"
                        End If
                        'spare pinpad
                        If storeReader("Pinpad").Equals(True) Then
                            spPinpad = "YES"
                        Else
                            spPinpad = "NO"
                        End If
                    End While

                End If

                'clear connection vars and close connection
                storeReader.Close()
                str_connection.Close()
                cmd_str.Dispose()

                'SPARE EQUIP MAIN FORM
                If lbl_strno_load.Text = "" Then
                    lbl_spare_equip_load.Text = ""
                Else

                    If spModem = "YES" Or spRouter = "YES" Or spWC = "YES" Or spHC = "YES" Or spPinpad = "YES" Then
                        lbl_spare_equip_load.Text = "YES"
                    Else
                        lbl_spare_equip_load.Text = "NO"
                    End If

                End If

            Catch ex As Exception
                MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [stores_spare_equip]")
            End Try
        End If

        'GET STORE DISTRICT INFO

        Dim distrct_conn As String                                                                              'EXCEL DB conn string
        Dim excel_conn As OleDb.OleDbConnection = New OleDb.OleDbConnection                                     'Excel new connection
        Dim districtReader As OleDbDataReader                                                                   'EXCEL: Revised Data Base sheet

        distrct_conn = provider & districtFile & excel_provider
        excel_conn.ConnectionString = distrct_conn
        Dim districtSQL As String = "SELECT * FROM [Data Base$] WHERE (Store = " & STORE & ")"

        If store_exists = False Then
            'if store does not exist stop if store exists load DISTRICT INFO
        Else
            Try
                excel_conn.Open()
                Dim cmd As New OleDbCommand(districtSQL, excel_conn)
                Dim dsExcel As New DataSet
                Dim daExcel As New OleDbDataAdapter(cmd)
                daExcel.Fill(dsExcel)
                districtReader = cmd.ExecuteReader

                'Gather DISTRICT LIST info
                While districtReader.Read()
                    'get district
                    If String.IsNullOrEmpty(districtReader("District").ToString) Then
                        lbl_district_load.Text = "N/A"
                    Else
                        lbl_district_load.Text = districtReader("District").ToString
                    End If

                    'get DSS Name
                    If String.IsNullOrEmpty(districtReader("Supervisor").ToString) Then
                        dssName = "N/A"
                    Else
                        dssName = districtReader("Supervisor").ToString
                    End If

                    'get DSS Email
                    If String.IsNullOrEmpty(districtReader("DSS Email").ToString) Then
                        dssEmail = "N/A"
                    Else
                        dssEmail = districtReader("DSS Email")
                    End If

                    'get DSS phone
                    If String.IsNullOrEmpty(districtReader("DSS Telephone").ToString) Then
                        dssLine = "N/A"
                    Else
                        dssLine = districtReader("DSS Telephone").ToString
                    End If

                    'get Director name
                    If String.IsNullOrEmpty(districtReader("Director").ToString) Then
                        directorName = "N/A"
                    Else
                        directorName = districtReader("Director").ToString
                    End If

                    If String.IsNullOrEmpty(districtReader("Director Telephone").ToString) Then
                        directorLine = "N/A"
                    Else
                        directorLine = districtReader("Director Telephone").ToString
                    End If

                End While
            Catch ex As Exception
                MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [district_list]")
            End Try
            'CLOSE CONNECTION TO DB
            excel_conn.Close()

            'load Orion with store IP
            OrionToolStripMenuItem.Text = "Orion (" & IPAddress & " )"

        End If

    End Sub

    'ADD NEW STORE INTO DB
    Protected Friend Function addStore(ByVal STORE As Integer, Banner As String, storeSystem As String, COMBO As Integer, pinpad As String, Remote As Integer, Status As Integer,
                                       Location As Integer, sis_store As String, Mall As String, Address As String, City As String, Province As String,
                                       p_code As String, Country As String, m_line As String, bo_line As String, fax_line As String,
                                       dsl_line As String, ip As String, isp As String, isp_account As String, isp_status As String, isp_ipaddress As String,
                                       isp_subnet As String, isp_gateway As String, speed As String, wifi As String, UserName As String,
                                       password As String, r_location As String, it_cabinet As String, smodem As Integer,
                                       srouter As Integer, swcard As Integer, shcash As Integer, spinpad As Integer)

        'VARS
        Dim store_exists As Boolean = False
        Dim storeReader As SqlDataReader
        Dim select_connection As SqlConnection
        Dim str_connection As SqlConnection
        Dim cmd_str As SqlCommand
        Dim cmd_net As SqlCommand
        Dim cmd_spr As SqlCommand
        Dim cmd_select As SqlCommand
        Dim sql_check_store As String = "Select * from stores_info where (Store = " & STORE & ")"

        'STORE TABLE
        Dim sql_store_info As String = "Insert into stores_info(Store, Banner, storeSystem, COMBO, pinpadConnection, Remote, Status, TEMP, [Sister Store], Mall, Address, City, Province, 
                                        [Postal Code], Country, [Main Line], [BO Line], [Fax Line], [DSL Line]) VALUES(" & STORE & ", '" & Banner & "', '" & storeSystem & "', " &
                                        COMBO & ", '" & pinpad & "', " & Remote & ", " & Status & ", " & Location & ", " & IIf(String.IsNullOrEmpty(sis_store), "null", "'" & sis_store & "'") &
                                        ", '" & Mall & "', '" & Address & "', '" & City & "', '" & Province & "', '" & p_code & "', '" & Country & "', " &
                                        IIf(String.IsNullOrEmpty(m_line), "null", "'" & m_line & "'") & ", " &
                                        IIf(String.IsNullOrEmpty(bo_line), "null", "'" & bo_line & "'") & ", " &
                                        IIf(String.IsNullOrEmpty(fax_line), "null", "'" & fax_line & "'") & ", " &
                                        IIf(String.IsNullOrEmpty(dsl_line), "null", "'" & dsl_line & "'") & ")"

        'STORE NETWORK
        Dim sql_store_network As String = "Insert into stores_network(Store, IP, ISP, Account, ISPstatus, ISPaddress, ISPsubnet, ISPgateway, Speed, [WiFi Speed], Username, 
                                            Password, [Router Location], [IT Cabinet Lock]) VALUES(" & STORE & ", '" & ip & "', " &
                                            IIf(String.IsNullOrEmpty(isp), "null", "'" & isp & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(isp_account), "null", "'" & isp_account & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(isp_status), "null", "'" & isp_status & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(isp_ipaddress), "null", "'" & isp_ipaddress & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(isp_subnet), "null", "'" & isp_subnet & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(isp_gateway), "null", "'" & isp_gateway & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(speed), "null", "'" & speed & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(wifi), "null", "'" & wifi & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(UserName), "null", "'" & UserName & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(password), "null", "'" & password & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(r_location), "null", "'" & r_location & "'") & ", " &
                                            IIf(String.IsNullOrEmpty(it_cabinet), "null", it_cabinet) & ")"

        'STORE SPARE EQUIP
        Dim sql_store_spare As String = "Insert into stores_spare_equip(Store, Modem, Router, [Wireless Card], [Holiday PC], Pinpad) VALUES(" & STORE & ", " &
                                          smodem & ", " & srouter & ", " & swcard & ", " & shcash & ", " & spinpad & ")"

        'CONNECTION PARA
        select_connection = New SqlConnection(connectionString)
        cmd_select = New SqlCommand(sql_check_store, select_connection)
        str_connection = New SqlConnection(connectionString)
        cmd_str = New SqlCommand(sql_store_info, str_connection)
        cmd_net = New SqlCommand(sql_store_network, str_connection)
        cmd_spr = New SqlCommand(sql_store_spare, str_connection)

        Try
            select_connection.Open()                    'OPEN SELECT CONNECTION
            str_connection.Open()                       'OPEN ADD CONNECTION
            storeReader = cmd_select.ExecuteReader()
            'If store already exists
            If storeReader.Read() = True Then
                store_exists = True
                'if store does not exist
            Else
                cmd_str.ExecuteNonQuery()
                cmd_net.ExecuteNonQuery()
                cmd_spr.ExecuteNonQuery()
                store_exists = False
            End If

            'return result
            If store_exists = True Then
                Return MsgBox("Store already exists! Please search for the store and EDIT.", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (store exists)")
                'clear form
                Store_new.clear_addstore("no", False)
            Else
                Return MsgBox("Store added to the DB.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (store added)")
            End If

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [add store]")
        End Try

        'CLOSE CONNECTIONS
        select_connection.Close()
        str_connection.Close()

    End Function

    'EDIT STORE IN DB
    Protected Friend Function editStore(ByVal STORE As Integer, Banner As String, storeSystem As String, COMBO As Integer, pinpad As String, Remote As Integer, Status As Integer,
                                        Location As Integer, sis_store As String, Mall As String, Address As String, City As String, Province As String, p_code As String,
                                        Country As String, m_line As String, bo_line As String, fax_line As String, dsl_line As String, ip As String, isp As String,
                                        isp_account As String, isp_status As String, isp_ip_address As String, isp_subnet As String, isp_gateway As String, speed As String,
                                        wifi As String, UserName As String, password As String, r_location As String, it_cabinet As String, smodem As Integer, srouter As Integer,
                                        swcard As Integer, shcash As Integer, spinpad As Integer)

        'STORE TABLE
        Dim sql_store_info As String = "Update stores_info set Banner = '" & Banner & "', storeSystem = '" & storeSystem & "', COMBO = " & COMBO & ", pinpadConnection = '" & pinpad & "', 
                                                                        Remote = " & Remote & ", Status = " & Status & ", TEMP = " & Location & ", 
                                                                        [Sister Store] = " & IIf(String.IsNullOrEmpty(sis_store), "null", "'" & sis_store & "'") & ", Mall = '" & Mall & "', 
                                                                        Address = '" & Address & "', City = '" & City & "', Province = '" & Province & "', [Postal Code] = '" & p_code & "', 
                                                                        Country = '" & Country & "', [Main Line] = " & IIf(String.IsNullOrEmpty(m_line), "null", "'" & m_line & "'") & ",
                                                                        [BO Line] = " & IIf(String.IsNullOrEmpty(bo_line), "null", "'" & bo_line & "'") & ",
                                                                        [Fax Line] = " & IIf(String.IsNullOrEmpty(fax_line), "null", "'" & fax_line & "'") & ",
                                                                        [DSL Line] = " & IIf(String.IsNullOrEmpty(dsl_line), "null", "'" & dsl_line & "'") & " where Store = " & STORE & ""

        'STORE NETWORK
        Dim sql_store_network As String = "Update stores_network set IP = '" & ip & "', ISP = " & IIf(String.IsNullOrEmpty(isp), "null", "'" & isp & "'") & ",
                                                                     Account = " & IIf(String.IsNullOrEmpty(isp_account), "null", "'" & isp_account & "'") & ",
                                                                     ISPstatus = " & IIf(String.IsNullOrEmpty(isp_status), "null", "'" & isp_status & "'") & ",
                                                                     ISPaddress = " & IIf(String.IsNullOrEmpty(isp_ip_address), "null", "'" & isp_ip_address & "'") & ",
                                                                     ISPsubnet = " & IIf(String.IsNullOrEmpty(isp_subnet), "null", "'" & isp_subnet & "'") & ",
                                                                     ISPgateway = " & IIf(String.IsNullOrEmpty(isp_gateway), "null", "'" & isp_gateway & "'") & ",
                                                                     Speed = " & IIf(String.IsNullOrEmpty(speed), "null", "'" & speed & "'") & ",
                                                                     [WiFi Speed] = " & IIf(String.IsNullOrEmpty(wifi), "null", "'" & wifi & "'") & ",
                                                                     Username = " & IIf(String.IsNullOrEmpty(UserName), "null", "'" & UserName & "'") & ",
                                                                     Password = " & IIf(String.IsNullOrEmpty(password), "null", "'" & password & "'") & ",
                                                                     [Router Location] = " & IIf(String.IsNullOrEmpty(r_location), "null", "'" & r_location & "'") & ",
                                                                     [IT Cabinet Lock] = " & IIf(String.IsNullOrEmpty(it_cabinet), "null", it_cabinet) & " where Store = " & STORE & ""

        'STORE SPARE EQUIP
        Dim sql_store_spare As String = "Update stores_spare_equip set Modem = " & smodem & ", Router = " & srouter & ", [Wireless Card] = " & swcard & ", [Holiday PC] = " & shcash & ",
                                                                       Pinpad =  " & spinpad & " where Store = " & STORE & ""


        'VARS
        Dim str_connection As SqlConnection
        Dim cmd_str As SqlCommand
        Dim cmd_net As SqlCommand
        Dim cmd_spr As SqlCommand

        'CONNECTION PARA
        str_connection = New SqlConnection(connectionString)
        cmd_str = New SqlCommand(sql_store_info, str_connection)
        cmd_net = New SqlCommand(sql_store_network, str_connection)
        cmd_spr = New SqlCommand(sql_store_spare, str_connection)

        Try
            str_connection.Open()                       'OPEN ADD CONNECTION

            'OPDATE DB
            cmd_str.ExecuteNonQuery()
            cmd_net.ExecuteNonQuery()
            cmd_spr.ExecuteNonQuery()


            'return result
            Return MsgBox("Store modified in DB.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (store edit)")

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [edit store]")
        End Try

        'CLOSE CONNECTIONS
        str_connection.Close()

    End Function



    'DELETE STORE
    Private Sub deleteStore(STORE As Integer)

        Dim delete_flag = False
        Dim sister_store As String = lbl_sis_store_load.Text

        Dim str_connection As SqlConnection
        Dim cmd_str As SqlCommand
        Dim cmd_net As SqlCommand
        Dim cmd_spr As SqlCommand
        Dim delStore As String = "delete from stores_info where store = " & STORE & ""
        Dim delNetwork As String = "delete from stores_network where store = " & STORE & ""
        Dim delSpare As String = "delete from stores_spare_equip where store = " & STORE & ""

        str_connection = New SqlConnection(connectionString)
        cmd_str = New SqlCommand(delStore, str_connection)
        cmd_net = New SqlCommand(delNetwork, str_connection)
        cmd_spr = New SqlCommand(delSpare, str_connection)

        Try
            str_connection.Open()                    'OPEN DELETE CONNECTION

            cmd_spr.ExecuteNonQuery()                'RUN SQL COMMAND
            cmd_net.ExecuteNonQuery()                'RUN SQL COMMAND
            cmd_str.ExecuteNonQuery()                'RUN SQL COMMAND


            MsgBox("Store " & STORE & " deleted from DB.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (store deleted)")
            btn_clear_store.PerformClick()

            'CLOSE CONNECTIONS
            str_connection.Close()

            'Check if store has sister store
            If sister_store = "N/A" Then
                'IF NO SISTER STORE FOUND - DO NOTHING
            Else
                'IF SISTER STORE FOUND
                Dim delete_sis_store As Integer = MsgBox("Do you want to delete sister store " & sister_store & " as well ?", MsgBoxStyle.YesNo, Title:="GDnetworks - info! (delete store)")
                If delete_sis_store = DialogResult.No Then
                    'EDIT STORE
                    txtbox_storeno.Text = sister_store
                    btn_search_store.PerformClick()
                    EditToolStripMenuItem.PerformClick()
                    Store_edit.txtbox_es_sisstore_load.Clear()
                    Store_edit.cmbbox_es_iscombo_load.SelectedItem = "NO"
                Else
                    'DELETE SISTER STORE

                    If delete_flag = False Then

                        delStore = "delete from stores_info where store = " & sister_store & ""
                        delNetwork = "delete from stores_network where store = " & sister_store & ""
                        delSpare = "delete from stores_spare_equip where store = " & sister_store & ""

                        str_connection = New SqlConnection(connectionString)
                        cmd_str = New SqlCommand(delStore, str_connection)
                        cmd_net = New SqlCommand(delNetwork, str_connection)
                        cmd_spr = New SqlCommand(delSpare, str_connection)

                        Try
                            str_connection.Open()                    'OPEN DELETE CONNECTION

                            cmd_spr.ExecuteNonQuery()                'RUN SQL COMMAND
                            cmd_net.ExecuteNonQuery()                'RUN SQL COMMAND
                            cmd_str.ExecuteNonQuery()                'RUN SQL COMMAND

                            MsgBox("Store " & lbl_sis_store_load.Text & " deleted from DB. ", MsgBoxStyle.Information, Title:="GDnetworks - Info! (store deleted)")

                            delete_flag = True

                            'CLOSE CONNECTIONS
                            str_connection.Close()

                            btn_clear_store.PerformClick()

                        Catch ex As Exception
                            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [delete store]")
                        End Try

                    End If
                End If

            End If


        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [delete store]")
        End Try



    End Sub

    'DISABLE ENABLE MAIN INTERFACE BUTTONS
    Private Sub statusMainButtons(status As String)
        'DISABLE BUTTONS
        If status = "disable" Then
            disableButtons(btn_store_hours)
            disableButtons(btn_list_status)
            disableButtons(btn_moreinfo)
            disableButtons(btn_cb_address)
            disableButtons(btn_cli_router)
            disableButtons(btn_cli_sw1)
            disableButtons(btn_cli_sw2)
            disableButtons(btn_gui_router)
            disableButtons(btn_gui_sw1)
            disableButtons(btn_gui_sw2)
            disableButtons(btn_tv_c1)
            disableButtons(btn_tv_c2)
            disableButtons(btn_tv_c3)
            disableButtons(btn_tv_c4)
            disableButtons(btn_tv_c5)
            disableButtons(btn_tv_c6)
            disableButtons(btn_tv_bo)
            'ENABLE BUTTONS
        ElseIf status = "enable" Then
            enableButtons(btn_store_hours)
            enableButtons(btn_moreinfo)
            If user_level = "limited" Or user_level = "limited+" Then
                'dont enable buttons
            Else
                enableButtons(btn_list_status)
                enableButtons(btn_cli_router)
                enableButtons(btn_cli_sw1)
                enableButtons(btn_cli_sw2)
                enableButtons(btn_gui_router)
                enableButtons(btn_gui_sw1)
                enableButtons(btn_gui_sw2)
            End If
            enableButtons(btn_cb_address)
            enableButtons(btn_tv_c1)
            enableButtons(btn_tv_c2)
            enableButtons(btn_tv_c3)
            enableButtons(btn_tv_c4)
            enableButtons(btn_tv_c5)
            enableButtons(btn_tv_c6)
            enableButtons(btn_tv_bo)
        End If

    End Sub


    Private Sub txtbox_remote_TextChanged(sender As Object, e As EventArgs)

    End Sub

    'Get network info
    Private Sub PingConnection(IP As String, isCombo As Boolean, hasSwitch2 As Boolean, hasAP2 As Boolean)

        Dim ping As New System.Net.NetworkInformation.Ping  'ping variable

        'Create list used to ping devices
        'Dim dataPing As Ping = Nothing
        'Dim listOfPing As New List(Of String)

        With listOfPing

            'Router
            .Add(IP & "129")
            cmd_router = IP & "129"

            'Switch1
            .Add(IP & "130")
            cmd_sw1 = IP & "130"

            'Switch2
            'check availability
            If hasSwitch2 = True Then
                .Add(IP & "131")
                cmd_sw2 = IP & "131"
            Else
                lbl_switch2_load.Text = "N/A"
                lbl_switch2_load.ForeColor = Color.Blue
            End If

            'AP1
            .Add(IP & "135")
            cmd_ap1 = IP & "135"

            'AP2
            'check availability
            If hasAP2 = True Then
                .Add(IP & "136")
                cmd_ap2 = IP & "136"
            Else
                lbl_ap2_load.Text = "N/A"
                lbl_ap2_load.ForeColor = Color.Blue
            End If

            'TC
            .Add(IP & "114")
            cmd_tc = IP & "114"

            'Music
            .Add(IP & "98")
            cmd_music = IP & "98"

            'IPAD
            .Add(IP & "66")
            cmd_ipad = IP & "66"

            'Samsung
            .Add(IP & "68")
            cmd_samsung = IP & "68"

            'CASHES & BO

            'Check for number of cashes in DB
            'if cashes is null ping 6 cashes
            If cashes = "" Then
                cashes = 6
            Else
                cashes = cashes
            End If
            'PING ONLY NUMBER OF CASHES
            For i As Integer = 1 To cashes
                .Add(IP & "1" & i)
            Next i

            'Cash1
            '.Add(IP & "11")
            cmd_c1 = IP & "11"

            'Cash2
            '.Add(IP & "12")
            cmd_c2 = IP & "12"

            'Cash3
            '.Add(IP & "13")
            cmd_c3 = IP & "13"

            'Cash4
            '.Add(IP & "14")
            cmd_c4 = IP & "14"

            'Cash5
            '.Add(IP & "15")
            cmd_c5 = IP & "15"

            'Cash6
            '.Add(IP & "16")
            cmd_c6 = IP & "16"

            'BO
            .Add(IP & "25")
            cmd_bo = IP & "25"

            'PINPADS
            If pinpadCon.Contains("IP") Then

                For a As Integer = 5 To cashes + 4
                    .Add(IP & a)

                    'exit loop if pinpad ips go over storeIP.10
                    If a > 11 Then
                        Exit For
                    End If
                Next

                'set pinpad ips
                pp_c1 = IP & "5"
                pp_c2 = IP & "6"
                pp_c3 = IP & "7"
                pp_c4 = IP & "8"
                pp_c5 = IP & "9"
                pp_c6 = IP & "10"

            End If

        End With

        Try
            For Each ItemToPing As String In listOfPing
                dataPing = New Ping
                AddHandler dataPing.PingCompleted, AddressOf PingResult
                dataPing.SendAsync(ItemToPing, 2000, ItemToPing)
            Next

            'clear ping list       
            listcount = listOfPing.Count
            listOfPing.Clear()

        Catch ex As Exception
            MsgBox("Unable to PING!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (PING)")
        End Try

    End Sub

    'Create ping list
    Private Sub PingResult(ByVal sender As Object, ByVal e As System.Net.NetworkInformation.PingCompletedEventArgs)

        'VARS
        Dim devicefound As Boolean              'check if device exists in list
        Dim devices As String = e.UserState     'DEVICE to ping
        Dim device As New ListViewItem          'DEVICE's IP and TTL


        'Check if device exists in list
        For Each item As ListViewItem In form_devices_list.listview_devices.Items
            If item.Text = devices Then
                devicefound = True
                Exit For
            End If
        Next


        'Populate device ping list with ping info
        If Not devicefound = True Then

            'Add device and info to list
            device = form_devices_list.listview_devices.Items.Add(devices)
            device.SubItems.Add(e.Reply.Status.ToString)
            device.SubItems.Add(e.Reply.RoundtripTime.ToString)
            device.SubItems.Add(device_name)

            'Set row color for Online/Offline mode
            If e.Reply.Status = IPStatus.Success Then
                device.BackColor = Color.Green
            Else
                device.BackColor = Color.Red

            End If
        End If

        PrintToLabels()

    End Sub

    Private Sub PrintToLabels()

        'get device list
        Dim device As ListView = form_devices_list.listview_devices

        With device
            Dim item As ListViewItem 'get each device

            'ROUTER CONNECTION STATUS
            item = .FindItemWithText(IPAddress & "129", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Router"

                'TOOLTIP router
                loadToolTips(lbl_router_load, "", "router")
                loadToolTips(lbl_net_rtr, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    'Check dialup connection
                    If item.SubItems(2).Text > 400 Then
                        lbl_router_load.Text = "Dialup | " & item.SubItems(2).Text & "ms" & item.Text
                        lbl_router_load.ForeColor = Color.DarkOrange
                    Else
                        lbl_router_load.Text = "Online | " & item.SubItems(2).Text & "ms" & " | " & item.Text
                        lbl_router_load.ForeColor = Color.Green
                    End If

                Else
                    lbl_router_load.Text = "Offline | " & item.SubItems(2).Text & "ms" & " | " & item.Text
                    lbl_router_load.ForeColor = Color.Red
                End If

            End If

            'SWITCH1 connection status
            item = .FindItemWithText(IPAddress & "130", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "SW1"

                'TOOLTIP switch1
                loadToolTips(lbl_switch1_load, "", "sw1")
                loadToolTips(lbl_net_sw1, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    'Check dialup connection
                    If item.SubItems(2).Text > 400 Then
                        lbl_switch1_load.Text = "Dialup | " & item.SubItems(2).Text & "ms" & item.Text
                        lbl_switch1_load.ForeColor = Color.DarkOrange
                    Else
                        lbl_switch1_load.Text = "Online | " & item.SubItems(2).Text & "ms" & " | " & item.Text
                        lbl_switch1_load.ForeColor = Color.Green
                    End If

                Else
                    lbl_switch1_load.Text = "Offline | " & item.SubItems(2).Text & "ms" & " | " & item.Text
                    lbl_switch1_load.ForeColor = Color.Red
                End If

            End If

            'SWITCH2 connection status
            item = .FindItemWithText(IPAddress & "131", True, 0, False)

            'TOOLTIP switch2
            loadToolTips(lbl_switch2_load, "", "sw2")
            loadToolTips(lbl_net_sw2, "", "copyIP")

            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "SW2"

                If item.SubItems(1).Text = "Success" Then
                    'Check dialup connection
                    If item.SubItems(2).Text > 400 Then
                        lbl_switch2_load.Text = "Dialup | " & item.SubItems(2).Text & "ms" & item.Text
                        lbl_switch2_load.ForeColor = Color.DarkOrange
                    Else
                        lbl_switch2_load.Text = "Online | " & item.SubItems(2).Text & "ms" & " | " & item.Text
                        lbl_switch2_load.ForeColor = Color.Green
                    End If

                Else
                    lbl_switch2_load.Text = "Offline | " & item.SubItems(2).Text & "ms" & " | " & item.Text
                    lbl_switch2_load.ForeColor = Color.Red
                End If
            End If

            'AP1 connection status
            item = .FindItemWithText(IPAddress & "135", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "AP1"

                'TOOLTIP AP1
                loadToolTips(lbl_ap1_load, "", "ap1")
                loadToolTips(lbl_ap1, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_ap1_load.Text = "Online"
                    lbl_ap1_load.ForeColor = Color.Green
                Else
                    lbl_ap1_load.ForeColor = Color.Red
                    lbl_ap1_load.Text = "Offline"
                End If
            End If

            'AP2 connection status
            item = .FindItemWithText(IPAddress & "136", True, 0, False)

            'TOOLTIP AP2
            loadToolTips(lbl_ap2_load, "", "ap2")
            loadToolTips(lbl_ap2, "", "copyIP")

            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "AP2"

                If item.SubItems(1).Text = "Success" Then
                    lbl_ap2_load.Text = "Online"
                    lbl_ap2_load.ForeColor = Color.Green
                Else
                    lbl_ap2_load.ForeColor = Color.Red
                    lbl_ap2_load.Text = "Offline"
                End If
            End If

            'TC connection status
            item = .FindItemWithText(IPAddress & "114", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Traffic Counter"

                'TOOLTIP TC
                loadToolTips(lbl_tc_load, "", "tc")
                loadToolTips(lbl_tc, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_tc_load.Text = "Online"
                    lbl_tc_load.ForeColor = Color.Green
                Else
                    lbl_tc_load.ForeColor = Color.Red
                    lbl_tc_load.Text = "Offline"
                End If
            End If

            'Music connection status
            item = .FindItemWithText(IPAddress & "98", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Music"

                'TOOLTIP music
                loadToolTips(lbl_music_load, "", "music")
                loadToolTips(lbl_music, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_music_load.Text = "Online"
                    lbl_music_load.ForeColor = Color.Green
                Else
                    lbl_music_load.ForeColor = Color.Red
                    lbl_music_load.Text = "Offline"
                End If
            End If

            'IPAD connection status
            item = .FindItemWithText(IPAddress & "66", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "IPAD"

                'TOOLTIP IPAD
                loadToolTips(lbl_ipad_load, "", "ipad")
                loadToolTips(lbl_ipad, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_ipad_load.Text = "Online"
                    lbl_ipad_load.ForeColor = Color.Green
                Else
                    lbl_ipad_load.ForeColor = Color.Red
                    lbl_ipad_load.Text = "Offline"
                End If
            End If

            'Samsung connection status
            item = .FindItemWithText(IPAddress & "68", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Samsung"

                'TOOLTIP samsung
                loadToolTips(lbl_samsung_load, "", "samsung")
                loadToolTips(lbl_stbl, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_samsung_load.Text = "Online"
                    lbl_samsung_load.ForeColor = Color.Green
                Else
                    lbl_samsung_load.ForeColor = Color.Red
                    lbl_samsung_load.Text = "Offline"
                End If
            End If

            'Cash1
            item = .FindItemWithText(IPAddress & "11", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Cash1"

                'TOOLTIP Cash1
                loadToolTips(lbl_cash1_load, "", "cash1")
                loadToolTips(lbl_cash1, "", "copyIP")


                If item.SubItems(1).Text = "Success" Then
                    lbl_cash1_load.Text = "Online"
                    lbl_cash1_load.ForeColor = Color.Green
                Else
                    lbl_cash1_load.ForeColor = Color.Red
                    lbl_cash1_load.Text = "Offline"
                End If
            Else                                                                          'apply N/A if cash does not exist
                lbl_cash1_load.ForeColor = Color.Blue
                lbl_cash1_load.Text = "N/A"

                'hide tooltips
                tooltip.SetToolTip(lbl_cash1_load, Nothing)
                tooltip.SetToolTip(lbl_cash1, Nothing)
            End If

            'Cash2
            item = .FindItemWithText(IPAddress & "12", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Cash2"

                'TOOLTIP Cash2
                loadToolTips(lbl_cash2_load, "", "cash2")
                loadToolTips(lbl_cash2, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_cash2_load.Text = "Online"
                    lbl_cash2_load.ForeColor = Color.Green
                Else
                    lbl_cash2_load.ForeColor = Color.Red
                    lbl_cash2_load.Text = "Offline"
                End If
            Else                                                                          'apply N/A if cash does not exist     
                lbl_cash2_load.ForeColor = Color.Blue
                lbl_cash2_load.Text = "N/A"

                'hide tooltips
                tooltip.SetToolTip(lbl_cash2_load, Nothing)
                tooltip.SetToolTip(lbl_cash2, Nothing)
            End If

            'Cash3
            item = .FindItemWithText(IPAddress & "13", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Cash3"

                'TOOLTIP Cash3
                loadToolTips(lbl_cash3_load, "", "cash3")
                loadToolTips(lbl_cash3, "", "copyIP")


                If item.SubItems(1).Text = "Success" Then
                    lbl_cash3_load.Text = "Online"
                    lbl_cash3_load.ForeColor = Color.Green
                Else
                    lbl_cash3_load.ForeColor = Color.Red
                    lbl_cash3_load.Text = "Offline"
                End If
            Else                                                                          'apply N/A if cash does not exist
                lbl_cash3_load.ForeColor = Color.Blue
                lbl_cash3_load.Text = "N/A"

                'hide tooltips
                tooltip.SetToolTip(lbl_cash3_load, Nothing)
                tooltip.SetToolTip(lbl_cash3, Nothing)
            End If

            'Cash4
            item = .FindItemWithText(IPAddress & "14", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Cash4"

                'TOOLTIP Cash4
                loadToolTips(lbl_cash4_load, "", "cash4")
                loadToolTips(lbl_cash4, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_cash4_load.Text = "Online"
                    lbl_cash4_load.ForeColor = Color.Green
                Else
                    lbl_cash4_load.ForeColor = Color.Red
                    lbl_cash4_load.Text = "Offline"
                End If
            Else                                                                          'apply N/A if cash does not exist
                lbl_cash4_load.ForeColor = Color.Blue
                lbl_cash4_load.Text = "N/A"

                'hide tooltips
                tooltip.SetToolTip(lbl_cash4_load, Nothing)
                tooltip.SetToolTip(lbl_cash4, Nothing)
            End If

            'Cash5
            item = .FindItemWithText(IPAddress & "15", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Cash5"

                'TOOLTIP Cash5
                loadToolTips(lbl_cash5_load, "", "cash5")
                loadToolTips(lbl_cash5, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_cash5_load.Text = "Online"
                    lbl_cash5_load.ForeColor = Color.Green
                Else
                    lbl_cash5_load.ForeColor = Color.Red
                    lbl_cash5_load.Text = "Offline"
                End If
            Else                                                                          'apply N/A if cash does not exist
                lbl_cash5_load.ForeColor = Color.Blue
                lbl_cash5_load.Text = "N/A"

                'hide tooltips
                tooltip.SetToolTip(lbl_cash5_load, Nothing)
                tooltip.SetToolTip(lbl_cash5, Nothing)

            End If

            'Cash6
            item = .FindItemWithText(IPAddress & "16", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Cash6"

                'TOOLTIP Cash6
                loadToolTips(lbl_cash6_load, "", "cash6")
                loadToolTips(lbl_cash6, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_cash6_load.Text = "Online"
                    lbl_cash6_load.ForeColor = Color.Green
                Else
                    lbl_cash6_load.ForeColor = Color.Red
                    lbl_cash6_load.Text = "Offline"
                End If
            Else                                                                          'apply N/A if cash does not exist
                lbl_cash6_load.ForeColor = Color.Blue
                lbl_cash6_load.Text = "N/A"

                'hide tooltips
                tooltip.SetToolTip(lbl_cash6_load, Nothing)
                tooltip.SetToolTip(lbl_cash6, Nothing)

            End If

            'BO
            item = .FindItemWithText(IPAddress & "25", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Back Office"

                'TOOLTIP BP
                loadToolTips(lbl_bo_load, "", "bo")
                loadToolTips(lbl_bo, "", "copyIP")

                If item.SubItems(1).Text = "Success" Then
                    lbl_bo_load.Text = "Online"
                    lbl_bo_load.ForeColor = Color.Green
                Else
                    lbl_bo_load.ForeColor = Color.Red
                    lbl_bo_load.Text = "Offline"
                End If
            Else                                                                          'apply N/A if cash does not exist
                lbl_bo_load.ForeColor = Color.Blue
                lbl_bo_load.Text = "N/A"
            End If

            'PINPAD1
            item = .FindItemWithText(IPAddress & "5", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Pinpad 1"

                'TOOLTIP BP
                loadPicToolTips(pic_pinpad_c1, "pp1")

                If item.SubItems(1).Text = "Success" Then
                    pic_pinpad_c1.Image = My.Resources.ResourceManager.GetObject("pinpad_online")
                Else
                    pic_pinpad_c1.Image = My.Resources.ResourceManager.GetObject("pinpad_offline")
                End If
            Else
                'set tooltip to nothing
                tooltip.SetToolTip(pic_pinpad_c1, Nothing)
            End If

                'PINPAD2
                item = .FindItemWithText(IPAddress & "6", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Pinpad 2"

                'TOOLTIP BP
                loadPicToolTips(pic_pinpad_c2, "pp2")

                If item.SubItems(1).Text = "Success" Then
                    pic_pinpad_c2.Image = My.Resources.ResourceManager.GetObject("pinpad_online")
                Else
                    pic_pinpad_c2.Image = My.Resources.ResourceManager.GetObject("pinpad_offline")
                End If
            Else
                'set tooltip to nothing
                tooltip.SetToolTip(pic_pinpad_c2, Nothing)
            End If

            'PINPAD3
            item = .FindItemWithText(IPAddress & "7", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Pinpad 3"

                'TOOLTIP BP
                loadPicToolTips(pic_pinpad_c3, "pp3")

                If item.SubItems(1).Text = "Success" Then
                    pic_pinpad_c3.Image = My.Resources.ResourceManager.GetObject("pinpad_online")
                Else
                    pic_pinpad_c3.Image = My.Resources.ResourceManager.GetObject("pinpad_offline")
                End If
            Else
                'set tooltip to nothing
                tooltip.SetToolTip(pic_pinpad_c3, Nothing)
            End If

            'PINPAD4
            item = .FindItemWithText(IPAddress & "8", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Pinpad 4"

                'TOOLTIP BP
                loadPicToolTips(pic_pinpad_c4, "pp4")

                If item.SubItems(1).Text = "Success" Then
                    pic_pinpad_c4.Image = My.Resources.ResourceManager.GetObject("pinpad_online")
                Else
                    pic_pinpad_c4.Image = My.Resources.ResourceManager.GetObject("pinpad_offline")
                End If
            Else
                'set tooltip to nothing
                tooltip.SetToolTip(pic_pinpad_c4, Nothing)
            End If

            'PINPAD5
            item = .FindItemWithText(IPAddress & "9", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Pinpad 5"

                'TOOLTIP BP
                loadPicToolTips(pic_pinpad_c5, "pp5")

                If item.SubItems(1).Text = "Success" Then
                    pic_pinpad_c5.Image = My.Resources.ResourceManager.GetObject("pinpad_online")
                Else
                    pic_pinpad_c5.Image = My.Resources.ResourceManager.GetObject("pinpad_offline")
                End If
            Else
                'set tooltip to nothing
                tooltip.SetToolTip(pic_pinpad_c5, Nothing)
            End If

            'PINPAD6
            item = .FindItemWithText(IPAddress & "10", True, 0, False)
            If Not item Is Nothing Then

                'Set device name in ping list
                item.SubItems(3).Text = "Pinpad 6"

                'TOOLTIP BP
                loadPicToolTips(pic_pinpad_c6, "pp6")

                If item.SubItems(1).Text = "Success" Then
                    pic_pinpad_c6.Image = My.Resources.ResourceManager.GetObject("pinpad_online")
                Else
                    pic_pinpad_c6.Image = My.Resources.ResourceManager.GetObject("pinpad_offline")
                End If
            Else
                'set tooltip to nothing
                tooltip.SetToolTip(pic_pinpad_c6, Nothing)
            End If

        End With
    End Sub

    Private Sub btn_moreinfo_Click(sender As Object, e As EventArgs) Handles btn_moreinfo.Click
        form_more_info.Owner = Me
        form_more_info.ShowDialog()
    End Sub

    'LOAD: pic tooltips
    Protected Friend Sub loadPicToolTips(pic_location As System.Windows.Forms.PictureBox, location As String)

        If location = "pp1" Then
                tooltip.SetToolTip(pic_location, "Ping pinpad (.5)")
            ElseIf location = "pp2" Then
                tooltip.SetToolTip(pic_location, "Ping pinpad (.6)")
            ElseIf location = "pp3" Then
                tooltip.SetToolTip(pic_location, "Ping pinpad (.7)")
            ElseIf location = "pp4" Then
                tooltip.SetToolTip(pic_location, "Ping pinpad (.8)")
            ElseIf location = "pp5" Then
                tooltip.SetToolTip(pic_location, "Ping pinpad (.9)")
            ElseIf location = "pp6" Then
                tooltip.SetToolTip(pic_location, "Ping pinpad (.10)")
            ElseIf location = "gdnet" Then
                tooltip.SetToolTip(pic_location, "Open GDnet.info")
        End If

    End Sub

    'LOAD: labels & buttons tooltips
    Protected Friend Sub loadToolTips(label_location As System.Windows.Forms.Label, address As String, form_location As String)

        'tooltip.IsBalloon = False
        'tooltip.UseAnimation = False
        'tooltip.ToolTipTitle = "For help press F1."
        'tooltip.Dispose()


        'router
        If form_location = "main_line" Then
            tooltip.SetToolTip(label_location, "Copy to clipboard")
        ElseIf form_location = "router" Then
            tooltip.SetToolTip(label_location, "Ping Router (.129)")
            'switch1
        ElseIf form_location = "sw1" Then
            tooltip.SetToolTip(label_location, "Ping Switch1 (.130)")
            'switch2
        ElseIf form_location = "sw2" Then
            tooltip.SetToolTip(label_location, "Ping Switch2 (.131)")
            'ap1
        ElseIf form_location = "ap1" Then
            tooltip.SetToolTip(label_location, "Ping AP1 (.135)")
            'ap2
        ElseIf form_location = "ap2" Then
            tooltip.SetToolTip(label_location, "Ping AP2 (.136)")
            'TC
        ElseIf form_location = "tc" Then
            tooltip.SetToolTip(label_location, "Ping Traffic Counter (.114)")
            'Music
        ElseIf form_location = "music" Then
            tooltip.SetToolTip(label_location, "Ping Music System (.98)")
            'IPAD
        ElseIf form_location = "ipad" Then
            tooltip.SetToolTip(label_location, "Ping IPAD (.66)")
            'Samsung
        ElseIf form_location = "samsung" Then
            tooltip.SetToolTip(label_location, "Ping Samsung Tablet (.68)")
            'Cash1
        ElseIf form_location = "cash1" Then
            tooltip.SetToolTip(label_location, "Ping Cash1 (.11)")
            'Cash2
        ElseIf form_location = "cash2" Then
            tooltip.SetToolTip(label_location, "Ping Cash2 (.12)")
            'Cash3
        ElseIf form_location = "cash3" Then
            tooltip.SetToolTip(label_location, "Ping Cash3 (.13)")
            'Cash4
        ElseIf form_location = "cash4" Then
            tooltip.SetToolTip(label_location, "Ping Cash4 (.14)")
            'Cash5
        ElseIf form_location = "cash5" Then
            tooltip.SetToolTip(label_location, "Ping Cash5 (.15)")
            'Cash6
        ElseIf form_location = "cash6" Then
            tooltip.SetToolTip(label_location, "Ping Cash6 (.16)")
            'BO
        ElseIf form_location = "bo" Then
            tooltip.SetToolTip(label_location, "Ping BackOffice (.25)")

        ElseIf form_location = "store_no" Then
            tooltip.SetToolTip(label_location, "Green: OPEN / Red: CLOSED")
            'Sister store
        ElseIf form_location = "sister_store" Then
            tooltip.SetToolTip(label_location, "Load sister store")
            'Copy IP
        ElseIf form_location = "copyIP" Then
            tooltip.SetToolTip(label_location, "Copy IP")
            'open C drive
        ElseIf form_location = "openC" Then
            tooltip.SetToolTip(label_location, "Open C drive")

            'ADD & EDIT STORE
            'store no
        ElseIf form_location = "ns_store" Then
            tooltip.SetToolTip(label_location, "Must be a number only." & vbNewLine & "EG: 6, 55, 232")
            'BANNER
        ElseIf form_location = "ns_banner" Then
            tooltip.SetToolTip(label_location, "Choose from the available options." & vbNewLine & "Note: HO only for HO-test")
            'store System
        ElseIf form_location = "ns_ssystem" Then
            tooltip.SetToolTip(label_location, "Choose from the available options." & vbNewLine & "Note: use ORPOS for now.")
            'Sister store
        ElseIf form_location = "ns_sstore" Then
            tooltip.SetToolTip(label_location, "Must be a number only. Leave blank if N/A." & vbNewLine & "Mandatory for COMBO" & vbNewLine & "EG: 6, 55, 232")
            'Mall
        ElseIf form_location = "ns_mall" Then
            tooltip.SetToolTip(label_location, "Can be both letters & digits" & vbNewLine & "EG: Quartier 10/30")
            'Address
        ElseIf form_location = "ns_address" Then
            tooltip.SetToolTip(label_location, "Can be both letters & digits" & vbNewLine & "EG: 9405 Boulevard Leduc")
            'City
        ElseIf form_location = "ns_city" Then
            tooltip.SetToolTip(label_location, "Must be letters only." & vbNewLine & "EG: Brossard")
            'State/province
        ElseIf form_location = "ns_state" Then
            tooltip.SetToolTip(label_location, "Must be letters only." & vbNewLine & "EG: Quebec")
            'Postal code
        ElseIf form_location = "ns_pcode" Then
            tooltip.SetToolTip(label_location, "Must be of format: A1A 1A1 (CA) 12345 (US)." & vbNewLine & "EG: J4Y 0A5 / 12345")
            'Country
        ElseIf form_location = "ns_countrye" Then
            tooltip.SetToolTip(label_location, "Choose from the available options.")
            'Main line
        ElseIf form_location = "ns_mline" Then
            tooltip.SetToolTip(label_location, "Must be of format: 10digits." & vbNewLine & "EG: 4504458451")
            'BO line
        ElseIf form_location = "ns_boline" Then
            tooltip.SetToolTip(label_location, "Must be of format: 10digits." & vbNewLine & "EG: 4504458451")
            'Fax line
        ElseIf form_location = "ns_faxline" Then
            tooltip.SetToolTip(label_location, "Must be of format: 10digits." & vbNewLine & "EG: 4504458451")
            'DSL line
        ElseIf form_location = "ns_dsl" Then
            tooltip.SetToolTip(label_location, "Must be of format: 10digits." & vbNewLine & "EG: 4504458451")
            'IP 3 octets
        ElseIf form_location = "ns_ip" Then
            tooltip.SetToolTip(label_location, "Must be of format: 0.0.0." & vbNewLine & "EG: 10.20.38.")
            'ISP
        ElseIf form_location = "ns_isp" Then
            tooltip.SetToolTip(label_location, "Can be both letters & digits." & vbNewLine & "EG: BELLNET")
            'Username
        ElseIf form_location = "ns_user" Then
            tooltip.SetToolTip(label_location, "Can be both letters & digits. Only if PPPOE selected." & vbNewLine & "EG: belleg@bellnet.ca")
            'Username
        ElseIf form_location = "ns_pwd" Then
            tooltip.SetToolTip(label_location, "Can be both letters & digits. Only if PPPOE selected." & vbNewLine & "Only if 'Username' exists" & vbNewLine & "EG: myexample10")
            'Store speed
        ElseIf form_location = "ns_speed" Then
            tooltip.SetToolTip(label_location, "Must be of format: DL / UP" & vbNewLine & "EG: 23.78 / 6.40")
            'wifi speed
        ElseIf form_location = "ns_wifi" Then
            tooltip.SetToolTip(label_location, "Must be of format: DL" & vbNewLine & "EG: 0.6, 20")
            'Router location
        ElseIf form_location = "ns_rlocation" Then
            tooltip.SetToolTip(label_location, "Choose from the available options.")
            'it cabinet lock
        ElseIf form_location = "ns_cabinet" Then
            tooltip.SetToolTip(label_location, "Must be of format ####." & vbNewLine & "EG: 1234")
            'IS COMBO
        ElseIf form_location = "ns_combo" Then
            tooltip.SetToolTip(label_location, "Choose from the available options." & vbNewLine & "If YES, Sister store # is mandatory!")
            'pinpad connection
        ElseIf form_location = "ns_pinpads" Then
            tooltip.SetToolTip(label_location, "Choose from the available options." & vbNewLine & "Note: overIP are the new pinpads.")
            'IS remote
        ElseIf form_location = "ns_remote" Then
            tooltip.SetToolTip(label_location, "Choose from the available options. Is this a remote location?")
        ElseIf form_location = "ns_location" Then
            tooltip.SetToolTip(label_location, "Choose from the available options. Is this a temp location?")
        ElseIf form_location = "ns_ispEmail" Then
            tooltip.SetToolTip(label_location, "If available enter ISP account number. Can be any format.")
        ElseIf form_location = "ns_ispstatus" Then
            tooltip.SetToolTip(label_location, "Is the store on DHCP, PPPOE or Static?")
        ElseIf form_location = "ns_ispaddress" Then
            tooltip.SetToolTip(label_location, "ISP IP Address. Only if Static IP selected. EG: 172.85.226.54")
        ElseIf form_location = "ns_ispsubnet" Then
            tooltip.SetToolTip(label_location, "ISP Subnet. Only if Static IP selected. EG: 255.255.255.252")
        ElseIf form_location = "ns_ispgateway" Then
            tooltip.SetToolTip(label_location, "ISP Default Gateway. Only if Static IP selected. EG: 172.85.226.53")
        End If

    End Sub

    'Lunch putty
    Private Sub lunchPutty(ip As String)
        Dim processInfo As New ProcessStartInfo
        processInfo.FileName = "C:\Program Files\putty\putty.exe"
        processInfo.Arguments = "-ssh " + putty_user + "@" + ip
        processInfo.UseShellExecute = True
        processInfo.WindowStyle = ProcessWindowStyle.Normal
        Dim process_start As Process = Process.Start(processInfo)
    End Sub

    'Lunch teamviewer
    Protected Friend Sub lunchTeamViewer(ip As String)
        Dim processInfo As New ProcessStartInfo
        processInfo.FileName = "C:\Program Files (x86)\TeamViewer\TeamViewer.exe"
        processInfo.Arguments = "-i " + ip + " --Password T3am_4t3ch"
        processInfo.UseShellExecute = True
        processInfo.WindowStyle = ProcessWindowStyle.Normal
        Dim process_start As Process = Process.Start(processInfo)
    End Sub

    'Lunch RDP
    Protected Friend Sub lunchRemoteDesktop(ip As String)
        Dim processInfo As New ProcessStartInfo
        processInfo.FileName = "C:\Windows\System32\mstsc.exe"
        processInfo.Arguments = "/v: " + ip
        processInfo.UseShellExecute = True
        processInfo.WindowStyle = ProcessWindowStyle.Normal
        Dim process_start As Process = Process.Start(processInfo)
    End Sub

    'Lunch PsExec
    Protected Friend Sub lunchPsExec(ip As String)

        Dim pc_user, pc_pwd, Result As String

        'set old image username
        If lbl_state_load.Text.Contains("QC") Or lbl_state_load.Text.Contains("Quebec") Then
            pc_user = "Administrateur"     'get username for french stores
        Else
            pc_user = "Administrator"      'get username for enghish stores
        End If

        'set pwd
        pc_pwd = "S3cure!!733"


        'test username for old image
        Result = Run_Process("cmd.exe", " /c net use \\" & ip & "\c$ /USER:" & ip & "\" & pc_user & " " & pc_pwd, , True)

        'test username for new image
        If Result = "" Then
            pc_user = "pos0"
        End If

        Dim processInfo As New ProcessStartInfo
        processInfo.FileName = "C:\Program Files\PSTOOLS\PsExec.exe"
        processInfo.Arguments = "\\" & ip & " -u " & pc_user & " -p " & pc_pwd & " cmd.exe"
        processInfo.UseShellExecute = True
        processInfo.WindowStyle = ProcessWindowStyle.Normal
        Dim process_start As Process = Process.Start(processInfo)

    End Sub

    'Open C: drive
    Private Sub openDrive(ipAddress As String)

        Dim username, username_newimage, password As String
        Dim result As Boolean = False

        If lbl_state_load.Text.Contains("QC") Or lbl_state_load.Text.Contains("Quebec") Then
            username = "Administrateur"     'get username for french stores
        Else
            username = "Administrator"      'get username for enghish stores
        End If

        username_newimage = "pos0"          'username for new image
        password = "S3cure!!733"            'get password

        result = Run_Process("cmd.exe", " /c net use \\" & ipAddress.ToString & "\c$ /USER:" & ipAddress.ToString & "\" & username.ToString & " " & password.ToString, , True)

        'test connection to drive is TRUE
        'IF CONNECTION TRUE connect to drive
        If result = True Then
            Run_Process("explorer.exe", "\\" & ipAddress & "\c$")
            'ELSE try connecting with POS0 account for new images
        Else
            result = Run_Process("cmd.exe", " /c net use \\" & ipAddress.ToString & "\c$ /USER:" & ipAddress.ToString & "\" & username_newimage.ToString & " " & password.ToString, , True)
            'IF CONNECTION TRUE connect to drive
            If result = True Then
                Run_Process("explorer.exe", "\\" & ipAddress & "\c$")
                'ELSE show error msg
            Else
                MsgBox("Unable to connect to " & ipAddress & " !", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (mapping)")
            End If

        End If
        ipAddress = ""

    End Sub

    'Disable buttons
    Private Sub disableButtons(Button As System.Windows.Forms.Button)

        Button.Enabled = False

    End Sub

    'Enable buttons
    Private Sub enableButtons(Button As System.Windows.Forms.Button)

        Button.Enabled = True

    End Sub

    'RUN PROCESS TO OPEN C$ DRIVE
    Protected Friend Function Run_Process(ByVal Process_Name As String, ByVal Process_Arguments As String,
                                Optional Read_Output As Boolean = False, Optional Process_Hide As Boolean = False,
                                Optional Process_TimeOut As Integer = 10000)

        Try

            Dim My_Process As New Process()
            Dim My_Process_info As New ProcessStartInfo()

            My_Process_info.FileName = Process_Name
            My_Process_info.Arguments = Process_Arguments
            My_Process_info.CreateNoWindow = Process_Hide
            My_Process_info.UseShellExecute = False
            My_Process_info.RedirectStandardOutput = Read_Output
            My_Process_info.RedirectStandardError = Read_Output
            My_Process.StartInfo = My_Process_info
            My_Process.Start()

            My_Process.WaitForExit(Process_TimeOut)

            Dim ERRORLEVEL = My_Process.ExitCode
            If Not ERRORLEVEL = 0 Then Return False

            If Read_Output = True Then
                Dim Process_ErrorOutput As String = My_Process.StandardOutput.ReadToEnd()
                Dim Process_StandardOutput As String = My_Process.StandardOutput.ReadToEnd()

                If Process_ErrorOutput IsNot Nothing Then Return Process_ErrorOutput
                If Process_StandardOutput IsNot Nothing Then Return Process_StandardOutput
            End If

        Catch ex As Exception

            'MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB)")
            Return Nothing

        End Try

        Return True

    End Function

    'unlock WL POS Account
    Private Sub unlockWLpos()

    End Sub

    'unlock RMS store2store
    Private Sub unlockRMSstore2store()


        '    'Dim sConnectionString As String = "Provider=OraOLEDB.Oracle;Data Source=rmstst-scan.corp.gdglobal;Database=RMSTST.corp.gdglobal.ca;User ID=SIMADMIN;password=s1St0r3Admin;"
        '    Dim sConnectionString As String = "Data Source=rmstst-scan.corp.gdglobal;User Id=SIMADMIN;Password=s1St0r3Admin;Provider=OraOLEDB.Oracle"
        '    Dim mySelectQuery As String = "elect username,account_status from dba_users;"
        '    Dim myConnection As New OleDb.OleDbConnection(sConnectionString)
        '    Dim myCommand As New OleDbCommand(mySelectQuery, myConnection)
        '
        '    'Open connection to Oracle database.
        '    myConnection.Open()
        '
        '    'Populate the DataReader.
        '    Dim myReader As OleDbDataReader = myCommand.ExecuteReader()
        '    Dim RecordCount As Integer
        '    Try
        '        While myReader.Read()
        '            RecordCount = RecordCount + 1
        '            MessageBox.Show(myReader.GetString(0).ToString())
        '        End While
        '        If RecordCount = 0 Then
        '            MessageBox.Show("No data returned")
        '        Else
        '            MessageBox.Show("Number of records returned: " & RecordCount)
        '        End If
        '    Catch ex As Exception
        '        MessageBox.Show(ex.ToString())
        '    Finally
        '
        '        'Close all objects.
        '        myReader.Close()
        '        myConnection.Close()
        '    End Try

    End Sub


    'DB PASSWORDS
    Protected Friend Function creds(account As String)
        Dim pwd As String = Nothing
        If account = "users" Then
            pwd = "Dyngdix8102"
        ElseIf account = "stores" Then
            pwd = "GDxUs3rpw_09102015"
        End If
        Return pwd
    End Function

    'GET LOCAL USERNAME
    Protected Friend Function getUsername() As String

        'Get local username and remove CORP
        If TypeOf My.User.CurrentPrincipal Is Security.Principal.WindowsPrincipal Then

            'GET entire username including domain
            Dim corp_user() As String = Split(My.User.Name, "\")
            'GET only username *without CORP
            Dim username As String = corp_user(1)
            'return username
            Return username
        Else
            Return My.User.Name     'if CORP cannot be removed return entire user
        End If

    End Function

    'ON CLOSING FORM
    Private Sub gdx_main_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        update_user(getUsername(), "Connection Status", "Offline")
    End Sub

End Class