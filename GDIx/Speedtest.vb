﻿'AUTHOR: Alex Dumitrascu
'DATE: 20-04-2019
'UPDATE: 24-04-2018
'
'FORM: Speedtest.vb (access from MAIN)

Imports System.Data.SqlClient

Public Class Speedtest


    'VARS
    Private store_no = gdx_main.lbl_strno_load.Text         'store no
    Private bo_ip = gdx_main.cmd_bo                         'bo pc ip
    Private process As New Process()                        'process
    Private process_info As New ProcessStartInfo()          'processStartInfo
    Private pstools_errors As String                        'for ps tools error return codes
    Private pstools_results As String                       'for ps tools results
    Private parse_results()                                 'parse result (get speedtest info)
    Private pc_user, pc_pwd As String                       'user & pwd for logging in to pc                                 
    Private result As String = Nothing                      'for testing image (new/old)
    Private start_timer As DateTime                         'get process start time
    Private stop_timer As DateTime                          'get process stop time
    Private elapsed_counter As TimeSpan                     'get elapsed process time



    'Speedtest MAIN ( ON LOAD )
    Private Sub Speedtest_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'center to parent
        Me.CenterToParent()

        'GET STORE #
        lbl_speedtest_store_load.Text = store_no.ToString()

        'set form text to nothing
        lbl_speedtest_testing.Text = "Progress"
        lbl_speedtest_con_load.Text = "n/a"
        lbl_speedtest_con_load.ForeColor = Color.Gray
        lbl_speedtest_file_load.Text = "n/a"
        lbl_speedtest_file_load.ForeColor = Color.Gray
        lbl_speedtest_server_load.Text = "n/a"
        lbl_speedtest_server_load.ForeColor = Color.Gray
        lbl_speedtest_ping_load.Text = "n/a"
        lbl_speedtest_ping_load.ForeColor = Color.Gray
        lbl_speedtest_dl_load.Text = "n/a"
        lbl_speedtest_dl_load.ForeColor = Color.Gray
        lbl_speedtest_up_load.Text = "n/a"
        lbl_speedtest_up_load.ForeColor = Color.Gray
        lbl_speedtest_time_load.Text = "n/a"
        lbl_speedtest_time_load.ForeColor = Color.Gray


        'show progressbar
        progbar_speedtest.Visible = True

    End Sub

    'BUTTON: CLOSE
    Private Sub btn_speedtest_close_Click(sender As Object, e As EventArgs) Handles btn_speedtest_close.Click
        Me.Dispose()
    End Sub

    'BUTTON: RUN SPEED TEST
    Private Sub btn_speedtest_run_Click(sender As Object, e As EventArgs) Handles btn_speedtest_run.Click


        'start progressbar       
        progbar_speedtest.Style = ProgressBarStyle.Marquee
        progbar_speedtest.MarqueeAnimationSpeed = 60
        progbar_speedtest.Refresh()

        'run speedtest
        speedtest()

    End Sub


    'RUN PSTOOLS SPEEDTEST
    Private Async Sub speedtest()

        'start timer
        start_timer = Now

        'set old image username
        If gdx_main.lbl_state_load.Text.Contains("QC") Or gdx_main.lbl_state_load.Text.Contains("Quebec") Then
            pc_user = "Administrateur"     'get username for french stores
        Else
            pc_user = "Administrator"      'get username for enghish stores
        End If

        'set pwd
        pc_pwd = "S3cure!!733"

        'display check creds
        lbl_speedtest_testing.Text = "Checking credentials..."

        'test username for old image
        result = gdx_main.Run_Process("cmd.exe", " /c net use \\" & gdx_main.cmd_bo.ToString & "\c$ /USER:" & gdx_main.cmd_bo.ToString & "\" & pc_user & " " & pc_pwd, , True)

        'test username for new image
        If result = "" Then
            pc_user = "pos0"
        End If

        'display testing internet speed
        lbl_speedtest_testing.Text = "Testing internet speed..."

        'Start PSTOOLS PROCESS & TEST INTERNET SPEED
        Try
            process.StartInfo.FileName = "C:\Program Files\PSTOOLS\PsExec.exe"
            process.StartInfo.Arguments = "\\" & bo_ip & " -u " & pc_user & " -p " & pc_pwd & " -w C:\FromHQ\speedtest\ cmd.exe /c speedtestnew.exe -r -rc='_$_'"
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            process.StartInfo.CreateNoWindow = True
            process.StartInfo.RedirectStandardError = True
            process.StartInfo.RedirectStandardOutput = True
            process.StartInfo.UseShellExecute = False

            process.Start()

            'change labels text
            lbl_speedtest_con_load.Text = "checking..."
            lbl_speedtest_con_load.ForeColor = Color.Gray
            lbl_speedtest_file_load.Text = "checking..."
            lbl_speedtest_file_load.ForeColor = Color.Gray
            lbl_speedtest_server_load.Text = "checking..."
            lbl_speedtest_server_load.ForeColor = Color.Gray
            lbl_speedtest_ping_load.Text = "checking..."
            lbl_speedtest_ping_load.ForeColor = Color.Gray
            lbl_speedtest_dl_load.Text = "checking..."
            lbl_speedtest_dl_load.ForeColor = Color.Gray
            lbl_speedtest_up_load.Text = "checking..."
            lbl_speedtest_up_load.ForeColor = Color.Gray
            lbl_speedtest_time_load.Text = "checking..."
            lbl_speedtest_time_load.ForeColor = Color.Gray

            Await Task.Run(Sub() process.WaitForExit())

        Catch ex As Exception
            MsgBox("Something went wrong...try again!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (Speedtest)")
        End Try

        'redirect output from PSTOOLS
        pstools_errors = process.StandardError.ReadToEnd
        pstools_results = process.StandardOutput.ReadToEnd

        'DISPLAY TEST STATUS
        'Check connection to remote host
        If pstools_errors.ToUpper.Contains("COULDN'T ACCESS") Then
            lbl_speedtest_con_load.Text = "Error"
            lbl_speedtest_con_load.ForeColor = Color.Red
            lbl_speedtest_file_load.Text = "Error"
            lbl_speedtest_file_load.ForeColor = Color.Red

        ElseIf pstools_errors.ToUpper.Contains("COULD NOT START CMD.EXE") Then
            lbl_speedtest_con_load.Text = "Error"
            lbl_speedtest_con_load.ForeColor = Color.Red
            lbl_speedtest_file_load.Text = "Error"
            lbl_speedtest_file_load.ForeColor = Color.Red
        Else
            lbl_speedtest_con_load.Text = "OK"
            lbl_speedtest_con_load.ForeColor = Color.Green

            'check if speedtest tool is available
            If pstools_errors.ToUpper.Contains("THE DIRECTORY NAME IS INVALID") Then
                lbl_speedtest_file_load.Text = "Error"
                lbl_speedtest_file_load.ForeColor = Color.Red
            Else
                lbl_speedtest_file_load.Text = "OK"
                lbl_speedtest_file_load.ForeColor = Color.Green
            End If

        End If

        If lbl_speedtest_con_load.Text = "OK" And lbl_speedtest_file_load.Text = "OK" Then

            'IF SERVER NOT FOUND
            If pstools_results.Contains("No such host is known") Then
                lbl_speedtest_server_load.Text = "Unknown"
                lbl_speedtest_server_load.ForeColor = Color.Red
                lbl_speedtest_dl_load.Text = "n/a"
                lbl_speedtest_up_load.Text = "n/a"
                lbl_speedtest_ping_load.Text = "n/a"
                MsgBox("Server is unknown! Try to run a manual speedtest", MsgBoxStyle.Information, Title:="GDnetworks - Info! (Speedtest)")
            Else

                'parse results
                parse_results = Split(pstools_results, "_$_")

                lbl_speedtest_server_load.Text = parse_results(2).replace("""", "")
                lbl_speedtest_ping_load.Text = parse_results(3).replace("'", "")

                'convert to int and get value in Mbps
                Dim dl_int = Convert.ToInt32(parse_results(4).replace("'", ""))
                Dim up_int = Convert.ToInt32(parse_results(5).replace("'", ""))
                lbl_speedtest_dl_load.Text = (dl_int / 1000).ToString("N2")
                lbl_speedtest_up_load.Text = (up_int / 1000).ToString("N2")

            End If

        Else
            MsgBox("Cannot connect to host...")
            lbl_speedtest_server_load.Text = "n/a"
            lbl_speedtest_server_load.ForeColor = Color.Gray
            lbl_speedtest_ping_load.Text = "n/a"
            lbl_speedtest_ping_load.ForeColor = Color.Gray
            lbl_speedtest_dl_load.Text = "n/a"
            lbl_speedtest_dl_load.ForeColor = Color.Gray
            lbl_speedtest_up_load.Text = "n/a"
            lbl_speedtest_up_load.ForeColor = Color.Gray
        End If

        'show progressbar as full (done)
        progbar_speedtest.Style = ProgressBarStyle.Blocks
        progbar_speedtest.Value = 100
        lbl_speedtest_testing.Text = "Speedtest completed"

        'stop timer
        stop_timer = Now

        'get process time
        elapsed_counter = stop_timer.Subtract(start_timer)
        lbl_speedtest_time_load.Text = elapsed_counter.TotalSeconds.ToString("00.00")

        'bring form to front
        Me.TopMost = True


    End Sub


    'BUTTON: SAVE INFO
    Private Sub btn_speedtest_save_Click(sender As Object, e As EventArgs) Handles btn_speedtest_save.Click


        If lbl_speedtest_dl_load.Text = "n/a" Or lbl_speedtest_up_load.Text = "n/a" Then

            MsgBox("Internet speeds are not available. Please perform speed test.", MsgBoxStyle.Information, Title:="GDnetworks - Info! [internet speed]")

        Else

            Dim storeno_int As Integer = Integer.Parse(lbl_speedtest_store_load.Text)
            Dim str_connection As SqlConnection
            Dim cmd_network As SqlCommand
            Dim sql_storespeed As String = "update stores_network set Speed = '" & lbl_speedtest_dl_load.Text & " / " & lbl_speedtest_up_load.Text & "',[Speedtest Date] = '" &
                                            DateTime.Now & "' where store = " & storeno_int

            str_connection = New SqlConnection(gdx_main.connectionString)
            cmd_network = New SqlCommand(sql_storespeed, str_connection)

            Try
                str_connection.Open()                       'OPEN ADD CONNECTION

                'UPDATE DB
                cmd_network.ExecuteNonQuery()
                MsgBox("Internet speed updated!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [internet speed]")

            Catch ex As Exception
                MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [edit storespeed]")
            End Try

            'CLOSE CONNECTIONS
            str_connection.Close()


        End If

    End Sub

    'ON CLOSING FORM
    Private Sub speedtest_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Me.Dispose()
    End Sub

End Class
