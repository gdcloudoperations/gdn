﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_more_info
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_more_info))
        Me.grp_addinfo = New System.Windows.Forms.GroupBox()
        Me.grpbox_additional_lines = New System.Windows.Forms.GroupBox()
        Me.lbl_fax_line_load = New System.Windows.Forms.Label()
        Me.lbl_bo_line_load = New System.Windows.Forms.Label()
        Me.lbl_bo_line = New System.Windows.Forms.Label()
        Me.lbl_fax_line = New System.Windows.Forms.Label()
        Me.grpbox_moreinfo_tel = New System.Windows.Forms.GroupBox()
        Me.lbl_director_line_load = New System.Windows.Forms.Label()
        Me.lbl_director_load = New System.Windows.Forms.Label()
        Me.lbl_dss_email_load = New System.Windows.Forms.Label()
        Me.lbl_dss_line_load = New System.Windows.Forms.Label()
        Me.lbl_dss_name_load = New System.Windows.Forms.Label()
        Me.lbl_director_line = New System.Windows.Forms.Label()
        Me.lbl_director = New System.Windows.Forms.Label()
        Me.lbl_dss_name = New System.Windows.Forms.Label()
        Me.lbl_dss_phone = New System.Windows.Forms.Label()
        Me.lbl_dss_email = New System.Windows.Forms.Label()
        Me.lbl_dsl_line_load = New System.Windows.Forms.Label()
        Me.lbl_dsl_line = New System.Windows.Forms.Label()
        Me.lbl_combo = New System.Windows.Forms.Label()
        Me.grp_equip = New System.Windows.Forms.GroupBox()
        Me.lbl_spare_hoc_load = New System.Windows.Forms.Label()
        Me.lbl_spare_wc_load = New System.Windows.Forms.Label()
        Me.lbl_spare_pinpad_load = New System.Windows.Forms.Label()
        Me.lbl_spare_router_load = New System.Windows.Forms.Label()
        Me.lbl_spare_modem_load = New System.Windows.Forms.Label()
        Me.lbl_spare_pinpad = New System.Windows.Forms.Label()
        Me.lbl_holoday_cash = New System.Windows.Forms.Label()
        Me.lbl_spare_router = New System.Windows.Forms.Label()
        Me.lbl_spare_modem = New System.Windows.Forms.Label()
        Me.lbl_spare_wc = New System.Windows.Forms.Label()
        Me.grp_network = New System.Windows.Forms.GroupBox()
        Me.lbl_isp_gateway_load = New System.Windows.Forms.Label()
        Me.lbl_isp_gateway = New System.Windows.Forms.Label()
        Me.lbl_isp_subnet_load = New System.Windows.Forms.Label()
        Me.lbl_isp_subnet = New System.Windows.Forms.Label()
        Me.lbl_isp_address_load = New System.Windows.Forms.Label()
        Me.lbl_isp_address = New System.Windows.Forms.Label()
        Me.lbl_isp_status_load = New System.Windows.Forms.Label()
        Me.lbl_isp_status = New System.Windows.Forms.Label()
        Me.lbl_isp_account_load = New System.Windows.Forms.Label()
        Me.lbl_isp_account = New System.Windows.Forms.Label()
        Me.lbl_router_location_load = New System.Windows.Forms.Label()
        Me.lbl_itcabinet_lock_load = New System.Windows.Forms.Label()
        Me.lbl_sw3_model_load = New System.Windows.Forms.Label()
        Me.lbl_sw2_model_load = New System.Windows.Forms.Label()
        Me.lbl_sw1_model_load = New System.Windows.Forms.Label()
        Me.lbl_isp_pwd_load = New System.Windows.Forms.Label()
        Me.lbl_isp_email_load = New System.Windows.Forms.Label()
        Me.lbl_isp_load = New System.Windows.Forms.Label()
        Me.lbl_sw3_model = New System.Windows.Forms.Label()
        Me.lbl_itcabinet_lock = New System.Windows.Forms.Label()
        Me.lbl_router_loc = New System.Windows.Forms.Label()
        Me.lbl_isp_pwd = New System.Windows.Forms.Label()
        Me.lbl_isp_email = New System.Windows.Forms.Label()
        Me.lbl_isp = New System.Windows.Forms.Label()
        Me.lbl_sw2_model = New System.Windows.Forms.Label()
        Me.lbl_sw1_model = New System.Windows.Forms.Label()
        Me.lbl_moreinfo_store = New System.Windows.Forms.Label()
        Me.lbl_moreinfo_store_load = New System.Windows.Forms.Label()
        Me.lbl_combo_load = New System.Windows.Forms.Label()
        Me.btn_moreInfo_close = New System.Windows.Forms.Button()
        Me.lbl_moreinfo_pinpadcon = New System.Windows.Forms.Label()
        Me.lbl_moreinfo_pinpadcon_load = New System.Windows.Forms.Label()
        Me.lbl_moreinfo_storesys = New System.Windows.Forms.Label()
        Me.lbl_moreinfo_storesys_load = New System.Windows.Forms.Label()
        Me.lbl_router_model = New System.Windows.Forms.Label()
        Me.lbl_router_model_load = New System.Windows.Forms.Label()
        Me.grp_addinfo.SuspendLayout()
        Me.grpbox_additional_lines.SuspendLayout()
        Me.grpbox_moreinfo_tel.SuspendLayout()
        Me.grp_equip.SuspendLayout()
        Me.grp_network.SuspendLayout()
        Me.SuspendLayout()
        '
        'grp_addinfo
        '
        Me.grp_addinfo.Controls.Add(Me.grpbox_additional_lines)
        Me.grp_addinfo.Controls.Add(Me.grpbox_moreinfo_tel)
        Me.grp_addinfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_addinfo.Location = New System.Drawing.Point(342, 40)
        Me.grp_addinfo.Name = "grp_addinfo"
        Me.grp_addinfo.Size = New System.Drawing.Size(324, 270)
        Me.grp_addinfo.TabIndex = 0
        Me.grp_addinfo.TabStop = False
        Me.grp_addinfo.Text = "Additional Store Info"
        '
        'grpbox_additional_lines
        '
        Me.grpbox_additional_lines.Controls.Add(Me.lbl_fax_line_load)
        Me.grpbox_additional_lines.Controls.Add(Me.lbl_bo_line_load)
        Me.grpbox_additional_lines.Controls.Add(Me.lbl_bo_line)
        Me.grpbox_additional_lines.Controls.Add(Me.lbl_fax_line)
        Me.grpbox_additional_lines.Location = New System.Drawing.Point(7, 25)
        Me.grpbox_additional_lines.Name = "grpbox_additional_lines"
        Me.grpbox_additional_lines.Size = New System.Drawing.Size(304, 77)
        Me.grpbox_additional_lines.TabIndex = 22
        Me.grpbox_additional_lines.TabStop = False
        Me.grpbox_additional_lines.Text = "Other Phone Lines"
        '
        'lbl_fax_line_load
        '
        Me.lbl_fax_line_load.AutoSize = True
        Me.lbl_fax_line_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fax_line_load.Location = New System.Drawing.Point(100, 54)
        Me.lbl_fax_line_load.Name = "lbl_fax_line_load"
        Me.lbl_fax_line_load.Size = New System.Drawing.Size(98, 13)
        Me.lbl_fax_line_load.TabIndex = 39
        Me.lbl_fax_line_load.Text = "store_fax_line_load"
        '
        'lbl_bo_line_load
        '
        Me.lbl_bo_line_load.AutoSize = True
        Me.lbl_bo_line_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_bo_line_load.Location = New System.Drawing.Point(100, 30)
        Me.lbl_bo_line_load.Name = "lbl_bo_line_load"
        Me.lbl_bo_line_load.Size = New System.Drawing.Size(96, 13)
        Me.lbl_bo_line_load.TabIndex = 38
        Me.lbl_bo_line_load.Text = "store_bo_line_load"
        '
        'lbl_bo_line
        '
        Me.lbl_bo_line.AutoSize = True
        Me.lbl_bo_line.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_bo_line.Location = New System.Drawing.Point(13, 30)
        Me.lbl_bo_line.Name = "lbl_bo_line"
        Me.lbl_bo_line.Size = New System.Drawing.Size(56, 13)
        Me.lbl_bo_line.TabIndex = 1
        Me.lbl_bo_line.Text = "BO Line:"
        '
        'lbl_fax_line
        '
        Me.lbl_fax_line.AutoSize = True
        Me.lbl_fax_line.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fax_line.Location = New System.Drawing.Point(13, 54)
        Me.lbl_fax_line.Name = "lbl_fax_line"
        Me.lbl_fax_line.Size = New System.Drawing.Size(59, 13)
        Me.lbl_fax_line.TabIndex = 2
        Me.lbl_fax_line.Text = "Fax Line:"
        '
        'grpbox_moreinfo_tel
        '
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_director_line_load)
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_director_load)
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_dss_email_load)
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_dss_line_load)
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_dss_name_load)
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_director_line)
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_director)
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_dss_name)
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_dss_phone)
        Me.grpbox_moreinfo_tel.Controls.Add(Me.lbl_dss_email)
        Me.grpbox_moreinfo_tel.Location = New System.Drawing.Point(7, 111)
        Me.grpbox_moreinfo_tel.Name = "grpbox_moreinfo_tel"
        Me.grpbox_moreinfo_tel.Size = New System.Drawing.Size(304, 148)
        Me.grpbox_moreinfo_tel.TabIndex = 21
        Me.grpbox_moreinfo_tel.TabStop = False
        Me.grpbox_moreinfo_tel.Text = "Escalation Contact Info"
        '
        'lbl_director_line_load
        '
        Me.lbl_director_line_load.AutoSize = True
        Me.lbl_director_line_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_director_line_load.Location = New System.Drawing.Point(93, 125)
        Me.lbl_director_line_load.Name = "lbl_director_line_load"
        Me.lbl_director_line_load.Size = New System.Drawing.Size(119, 13)
        Me.lbl_director_line_load.TabIndex = 45
        Me.lbl_director_line_load.Text = "store_director_line_load"
        '
        'lbl_director_load
        '
        Me.lbl_director_load.AutoSize = True
        Me.lbl_director_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_director_load.Location = New System.Drawing.Point(93, 101)
        Me.lbl_director_load.Name = "lbl_director_load"
        Me.lbl_director_load.Size = New System.Drawing.Size(116, 13)
        Me.lbl_director_load.TabIndex = 44
        Me.lbl_director_load.Text = "store_director_load"
        '
        'lbl_dss_email_load
        '
        Me.lbl_dss_email_load.AutoSize = True
        Me.lbl_dss_email_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dss_email_load.Location = New System.Drawing.Point(93, 77)
        Me.lbl_dss_email_load.Name = "lbl_dss_email_load"
        Me.lbl_dss_email_load.Size = New System.Drawing.Size(108, 13)
        Me.lbl_dss_email_load.TabIndex = 43
        Me.lbl_dss_email_load.Text = "store_dss_email_load"
        '
        'lbl_dss_line_load
        '
        Me.lbl_dss_line_load.AutoSize = True
        Me.lbl_dss_line_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dss_line_load.Location = New System.Drawing.Point(93, 53)
        Me.lbl_dss_line_load.Name = "lbl_dss_line_load"
        Me.lbl_dss_line_load.Size = New System.Drawing.Size(100, 13)
        Me.lbl_dss_line_load.TabIndex = 42
        Me.lbl_dss_line_load.Text = "store_dss_line_load"
        '
        'lbl_dss_name_load
        '
        Me.lbl_dss_name_load.AutoSize = True
        Me.lbl_dss_name_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dss_name_load.Location = New System.Drawing.Point(93, 29)
        Me.lbl_dss_name_load.Name = "lbl_dss_name_load"
        Me.lbl_dss_name_load.Size = New System.Drawing.Size(116, 13)
        Me.lbl_dss_name_load.TabIndex = 41
        Me.lbl_dss_name_load.Text = "store_dsl_line_load"
        '
        'lbl_director_line
        '
        Me.lbl_director_line.AutoSize = True
        Me.lbl_director_line.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_director_line.Location = New System.Drawing.Point(6, 125)
        Me.lbl_director_line.Name = "lbl_director_line"
        Me.lbl_director_line.Size = New System.Drawing.Size(84, 13)
        Me.lbl_director_line.TabIndex = 22
        Me.lbl_director_line.Text = "Director Line:"
        '
        'lbl_director
        '
        Me.lbl_director.AutoSize = True
        Me.lbl_director.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_director.Location = New System.Drawing.Point(6, 101)
        Me.lbl_director.Name = "lbl_director"
        Me.lbl_director.Size = New System.Drawing.Size(56, 13)
        Me.lbl_director.TabIndex = 20
        Me.lbl_director.Text = "Director:"
        '
        'lbl_dss_name
        '
        Me.lbl_dss_name.AutoSize = True
        Me.lbl_dss_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dss_name.Location = New System.Drawing.Point(6, 29)
        Me.lbl_dss_name.Name = "lbl_dss_name"
        Me.lbl_dss_name.Size = New System.Drawing.Size(36, 13)
        Me.lbl_dss_name.TabIndex = 3
        Me.lbl_dss_name.Text = "DSS:"
        '
        'lbl_dss_phone
        '
        Me.lbl_dss_phone.AutoSize = True
        Me.lbl_dss_phone.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dss_phone.Location = New System.Drawing.Point(6, 53)
        Me.lbl_dss_phone.Name = "lbl_dss_phone"
        Me.lbl_dss_phone.Size = New System.Drawing.Size(64, 13)
        Me.lbl_dss_phone.TabIndex = 4
        Me.lbl_dss_phone.Text = "DSS Line:"
        '
        'lbl_dss_email
        '
        Me.lbl_dss_email.AutoSize = True
        Me.lbl_dss_email.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dss_email.Location = New System.Drawing.Point(6, 77)
        Me.lbl_dss_email.Name = "lbl_dss_email"
        Me.lbl_dss_email.Size = New System.Drawing.Size(70, 13)
        Me.lbl_dss_email.TabIndex = 6
        Me.lbl_dss_email.Text = "DSS Email:"
        '
        'lbl_dsl_line_load
        '
        Me.lbl_dsl_line_load.AutoSize = True
        Me.lbl_dsl_line_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dsl_line_load.Location = New System.Drawing.Point(147, 78)
        Me.lbl_dsl_line_load.Name = "lbl_dsl_line_load"
        Me.lbl_dsl_line_load.Size = New System.Drawing.Size(97, 13)
        Me.lbl_dsl_line_load.TabIndex = 40
        Me.lbl_dsl_line_load.Text = "store_dsl_line_load"
        '
        'lbl_dsl_line
        '
        Me.lbl_dsl_line.AutoSize = True
        Me.lbl_dsl_line.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dsl_line.Location = New System.Drawing.Point(17, 78)
        Me.lbl_dsl_line.Name = "lbl_dsl_line"
        Me.lbl_dsl_line.Size = New System.Drawing.Size(83, 13)
        Me.lbl_dsl_line.TabIndex = 3
        Me.lbl_dsl_line.Text = "Internet Line:"
        '
        'lbl_combo
        '
        Me.lbl_combo.AutoSize = True
        Me.lbl_combo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_combo.Location = New System.Drawing.Point(521, 15)
        Me.lbl_combo.Name = "lbl_combo"
        Me.lbl_combo.Size = New System.Drawing.Size(83, 13)
        Me.lbl_combo.TabIndex = 0
        Me.lbl_combo.Text = "Combo Store:"
        '
        'grp_equip
        '
        Me.grp_equip.Controls.Add(Me.lbl_spare_hoc_load)
        Me.grp_equip.Controls.Add(Me.lbl_spare_wc_load)
        Me.grp_equip.Controls.Add(Me.lbl_spare_pinpad_load)
        Me.grp_equip.Controls.Add(Me.lbl_spare_router_load)
        Me.grp_equip.Controls.Add(Me.lbl_spare_modem_load)
        Me.grp_equip.Controls.Add(Me.lbl_spare_pinpad)
        Me.grp_equip.Controls.Add(Me.lbl_holoday_cash)
        Me.grp_equip.Controls.Add(Me.lbl_spare_router)
        Me.grp_equip.Controls.Add(Me.lbl_spare_modem)
        Me.grp_equip.Controls.Add(Me.lbl_spare_wc)
        Me.grp_equip.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_equip.Location = New System.Drawing.Point(342, 316)
        Me.grp_equip.Name = "grp_equip"
        Me.grp_equip.Size = New System.Drawing.Size(324, 92)
        Me.grp_equip.TabIndex = 4
        Me.grp_equip.TabStop = False
        Me.grp_equip.Text = "Spare Equip"
        '
        'lbl_spare_hoc_load
        '
        Me.lbl_spare_hoc_load.AutoSize = True
        Me.lbl_spare_hoc_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_hoc_load.Location = New System.Drawing.Point(269, 49)
        Me.lbl_spare_hoc_load.Name = "lbl_spare_hoc_load"
        Me.lbl_spare_hoc_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_spare_hoc_load.TabIndex = 45
        Me.lbl_spare_hoc_load.Text = "spC"
        '
        'lbl_spare_wc_load
        '
        Me.lbl_spare_wc_load.AutoSize = True
        Me.lbl_spare_wc_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_wc_load.Location = New System.Drawing.Point(269, 28)
        Me.lbl_spare_wc_load.Name = "lbl_spare_wc_load"
        Me.lbl_spare_wc_load.Size = New System.Drawing.Size(29, 13)
        Me.lbl_spare_wc_load.TabIndex = 44
        Me.lbl_spare_wc_load.Text = "spW"
        '
        'lbl_spare_pinpad_load
        '
        Me.lbl_spare_pinpad_load.AutoSize = True
        Me.lbl_spare_pinpad_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_pinpad_load.Location = New System.Drawing.Point(75, 71)
        Me.lbl_spare_pinpad_load.Name = "lbl_spare_pinpad_load"
        Me.lbl_spare_pinpad_load.Size = New System.Drawing.Size(25, 13)
        Me.lbl_spare_pinpad_load.TabIndex = 43
        Me.lbl_spare_pinpad_load.Text = "spP"
        '
        'lbl_spare_router_load
        '
        Me.lbl_spare_router_load.AutoSize = True
        Me.lbl_spare_router_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_router_load.Location = New System.Drawing.Point(75, 49)
        Me.lbl_spare_router_load.Name = "lbl_spare_router_load"
        Me.lbl_spare_router_load.Size = New System.Drawing.Size(26, 13)
        Me.lbl_spare_router_load.TabIndex = 42
        Me.lbl_spare_router_load.Text = "spR"
        '
        'lbl_spare_modem_load
        '
        Me.lbl_spare_modem_load.AutoSize = True
        Me.lbl_spare_modem_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_modem_load.Location = New System.Drawing.Point(75, 28)
        Me.lbl_spare_modem_load.Name = "lbl_spare_modem_load"
        Me.lbl_spare_modem_load.Size = New System.Drawing.Size(27, 13)
        Me.lbl_spare_modem_load.TabIndex = 41
        Me.lbl_spare_modem_load.Text = "spM"
        '
        'lbl_spare_pinpad
        '
        Me.lbl_spare_pinpad.AutoSize = True
        Me.lbl_spare_pinpad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_pinpad.Location = New System.Drawing.Point(18, 71)
        Me.lbl_spare_pinpad.Name = "lbl_spare_pinpad"
        Me.lbl_spare_pinpad.Size = New System.Drawing.Size(50, 13)
        Me.lbl_spare_pinpad.TabIndex = 15
        Me.lbl_spare_pinpad.Text = "Pinpad:"
        '
        'lbl_holoday_cash
        '
        Me.lbl_holoday_cash.AutoSize = True
        Me.lbl_holoday_cash.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_holoday_cash.Location = New System.Drawing.Point(175, 49)
        Me.lbl_holoday_cash.Name = "lbl_holoday_cash"
        Me.lbl_holoday_cash.Size = New System.Drawing.Size(85, 13)
        Me.lbl_holoday_cash.TabIndex = 14
        Me.lbl_holoday_cash.Text = "Holiday Cash:"
        '
        'lbl_spare_router
        '
        Me.lbl_spare_router.AutoSize = True
        Me.lbl_spare_router.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_router.Location = New System.Drawing.Point(18, 49)
        Me.lbl_spare_router.Name = "lbl_spare_router"
        Me.lbl_spare_router.Size = New System.Drawing.Size(49, 13)
        Me.lbl_spare_router.TabIndex = 12
        Me.lbl_spare_router.Text = "Router:"
        '
        'lbl_spare_modem
        '
        Me.lbl_spare_modem.AutoSize = True
        Me.lbl_spare_modem.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_modem.Location = New System.Drawing.Point(18, 28)
        Me.lbl_spare_modem.Name = "lbl_spare_modem"
        Me.lbl_spare_modem.Size = New System.Drawing.Size(51, 13)
        Me.lbl_spare_modem.TabIndex = 11
        Me.lbl_spare_modem.Text = "Modem:"
        '
        'lbl_spare_wc
        '
        Me.lbl_spare_wc.AutoSize = True
        Me.lbl_spare_wc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_wc.Location = New System.Drawing.Point(174, 28)
        Me.lbl_spare_wc.Name = "lbl_spare_wc"
        Me.lbl_spare_wc.Size = New System.Drawing.Size(89, 13)
        Me.lbl_spare_wc.TabIndex = 13
        Me.lbl_spare_wc.Text = "Wireless Card:"
        '
        'grp_network
        '
        Me.grp_network.Controls.Add(Me.lbl_router_model_load)
        Me.grp_network.Controls.Add(Me.lbl_router_model)
        Me.grp_network.Controls.Add(Me.lbl_isp_gateway_load)
        Me.grp_network.Controls.Add(Me.lbl_isp_gateway)
        Me.grp_network.Controls.Add(Me.lbl_isp_subnet_load)
        Me.grp_network.Controls.Add(Me.lbl_isp_subnet)
        Me.grp_network.Controls.Add(Me.lbl_isp_address_load)
        Me.grp_network.Controls.Add(Me.lbl_isp_address)
        Me.grp_network.Controls.Add(Me.lbl_isp_status_load)
        Me.grp_network.Controls.Add(Me.lbl_isp_status)
        Me.grp_network.Controls.Add(Me.lbl_dsl_line_load)
        Me.grp_network.Controls.Add(Me.lbl_isp_account_load)
        Me.grp_network.Controls.Add(Me.lbl_isp_account)
        Me.grp_network.Controls.Add(Me.lbl_router_location_load)
        Me.grp_network.Controls.Add(Me.lbl_itcabinet_lock_load)
        Me.grp_network.Controls.Add(Me.lbl_sw3_model_load)
        Me.grp_network.Controls.Add(Me.lbl_dsl_line)
        Me.grp_network.Controls.Add(Me.lbl_sw2_model_load)
        Me.grp_network.Controls.Add(Me.lbl_sw1_model_load)
        Me.grp_network.Controls.Add(Me.lbl_isp_pwd_load)
        Me.grp_network.Controls.Add(Me.lbl_isp_email_load)
        Me.grp_network.Controls.Add(Me.lbl_isp_load)
        Me.grp_network.Controls.Add(Me.lbl_sw3_model)
        Me.grp_network.Controls.Add(Me.lbl_itcabinet_lock)
        Me.grp_network.Controls.Add(Me.lbl_router_loc)
        Me.grp_network.Controls.Add(Me.lbl_isp_pwd)
        Me.grp_network.Controls.Add(Me.lbl_isp_email)
        Me.grp_network.Controls.Add(Me.lbl_isp)
        Me.grp_network.Controls.Add(Me.lbl_sw2_model)
        Me.grp_network.Controls.Add(Me.lbl_sw1_model)
        Me.grp_network.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_network.Location = New System.Drawing.Point(12, 40)
        Me.grp_network.Name = "grp_network"
        Me.grp_network.Size = New System.Drawing.Size(324, 391)
        Me.grp_network.TabIndex = 11
        Me.grp_network.TabStop = False
        Me.grp_network.Text = "Network"
        '
        'lbl_isp_gateway_load
        '
        Me.lbl_isp_gateway_load.AutoSize = True
        Me.lbl_isp_gateway_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_gateway_load.Location = New System.Drawing.Point(147, 174)
        Me.lbl_isp_gateway_load.Name = "lbl_isp_gateway_load"
        Me.lbl_isp_gateway_load.Size = New System.Drawing.Size(108, 13)
        Me.lbl_isp_gateway_load.TabIndex = 48
        Me.lbl_isp_gateway_load.Text = "lbl_isp_gateway_load"
        '
        'lbl_isp_gateway
        '
        Me.lbl_isp_gateway.AutoSize = True
        Me.lbl_isp_gateway.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_gateway.Location = New System.Drawing.Point(17, 174)
        Me.lbl_isp_gateway.Name = "lbl_isp_gateway"
        Me.lbl_isp_gateway.Size = New System.Drawing.Size(84, 13)
        Me.lbl_isp_gateway.TabIndex = 47
        Me.lbl_isp_gateway.Text = "ISP Gateway:"
        '
        'lbl_isp_subnet_load
        '
        Me.lbl_isp_subnet_load.AutoSize = True
        Me.lbl_isp_subnet_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_subnet_load.Location = New System.Drawing.Point(147, 150)
        Me.lbl_isp_subnet_load.Name = "lbl_isp_subnet_load"
        Me.lbl_isp_subnet_load.Size = New System.Drawing.Size(100, 13)
        Me.lbl_isp_subnet_load.TabIndex = 46
        Me.lbl_isp_subnet_load.Text = "lbl_isp_subnet_load"
        '
        'lbl_isp_subnet
        '
        Me.lbl_isp_subnet.AutoSize = True
        Me.lbl_isp_subnet.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_subnet.Location = New System.Drawing.Point(17, 150)
        Me.lbl_isp_subnet.Name = "lbl_isp_subnet"
        Me.lbl_isp_subnet.Size = New System.Drawing.Size(75, 13)
        Me.lbl_isp_subnet.TabIndex = 45
        Me.lbl_isp_subnet.Text = "ISP Subnet:"
        '
        'lbl_isp_address_load
        '
        Me.lbl_isp_address_load.AutoSize = True
        Me.lbl_isp_address_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_address_load.Location = New System.Drawing.Point(147, 126)
        Me.lbl_isp_address_load.Name = "lbl_isp_address_load"
        Me.lbl_isp_address_load.Size = New System.Drawing.Size(105, 13)
        Me.lbl_isp_address_load.TabIndex = 44
        Me.lbl_isp_address_load.Text = "lbl_isp_address_load"
        '
        'lbl_isp_address
        '
        Me.lbl_isp_address.AutoSize = True
        Me.lbl_isp_address.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_address.Location = New System.Drawing.Point(17, 126)
        Me.lbl_isp_address.Name = "lbl_isp_address"
        Me.lbl_isp_address.Size = New System.Drawing.Size(96, 13)
        Me.lbl_isp_address.TabIndex = 43
        Me.lbl_isp_address.Text = "ISP IP Address:"
        '
        'lbl_isp_status_load
        '
        Me.lbl_isp_status_load.AutoSize = True
        Me.lbl_isp_status_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_status_load.Location = New System.Drawing.Point(147, 102)
        Me.lbl_isp_status_load.Name = "lbl_isp_status_load"
        Me.lbl_isp_status_load.Size = New System.Drawing.Size(115, 13)
        Me.lbl_isp_status_load.TabIndex = 42
        Me.lbl_isp_status_load.Text = "lbl_isp_status_load"
        '
        'lbl_isp_status
        '
        Me.lbl_isp_status.AutoSize = True
        Me.lbl_isp_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_status.Location = New System.Drawing.Point(17, 102)
        Me.lbl_isp_status.Name = "lbl_isp_status"
        Me.lbl_isp_status.Size = New System.Drawing.Size(115, 13)
        Me.lbl_isp_status.TabIndex = 41
        Me.lbl_isp_status.Text = "ISP IP Assignment:"
        '
        'lbl_isp_account_load
        '
        Me.lbl_isp_account_load.AutoSize = True
        Me.lbl_isp_account_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_account_load.Location = New System.Drawing.Point(147, 54)
        Me.lbl_isp_account_load.Name = "lbl_isp_account_load"
        Me.lbl_isp_account_load.Size = New System.Drawing.Size(142, 13)
        Me.lbl_isp_account_load.TabIndex = 39
        Me.lbl_isp_account_load.Text = "store_isp_account_load"
        '
        'lbl_isp_account
        '
        Me.lbl_isp_account.AutoSize = True
        Me.lbl_isp_account.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_account.Location = New System.Drawing.Point(17, 54)
        Me.lbl_isp_account.Name = "lbl_isp_account"
        Me.lbl_isp_account.Size = New System.Drawing.Size(58, 13)
        Me.lbl_isp_account.TabIndex = 38
        Me.lbl_isp_account.Text = "Account:"
        '
        'lbl_router_location_load
        '
        Me.lbl_router_location_load.AutoSize = True
        Me.lbl_router_location_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_router_location_load.Location = New System.Drawing.Point(147, 366)
        Me.lbl_router_location_load.Name = "lbl_router_location_load"
        Me.lbl_router_location_load.Size = New System.Drawing.Size(132, 13)
        Me.lbl_router_location_load.TabIndex = 37
        Me.lbl_router_location_load.Text = "store_router_location_load"
        '
        'lbl_itcabinet_lock_load
        '
        Me.lbl_itcabinet_lock_load.AutoSize = True
        Me.lbl_itcabinet_lock_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_itcabinet_lock_load.Location = New System.Drawing.Point(147, 342)
        Me.lbl_itcabinet_lock_load.Name = "lbl_itcabinet_lock_load"
        Me.lbl_itcabinet_lock_load.Size = New System.Drawing.Size(153, 13)
        Me.lbl_itcabinet_lock_load.TabIndex = 36
        Me.lbl_itcabinet_lock_load.Text = "store_itcabinet_lock_load"
        '
        'lbl_sw3_model_load
        '
        Me.lbl_sw3_model_load.AutoSize = True
        Me.lbl_sw3_model_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sw3_model_load.Location = New System.Drawing.Point(147, 318)
        Me.lbl_sw3_model_load.Name = "lbl_sw3_model_load"
        Me.lbl_sw3_model_load.Size = New System.Drawing.Size(115, 13)
        Me.lbl_sw3_model_load.TabIndex = 35
        Me.lbl_sw3_model_load.Text = "store_sw3_model_load"
        '
        'lbl_sw2_model_load
        '
        Me.lbl_sw2_model_load.AutoSize = True
        Me.lbl_sw2_model_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sw2_model_load.Location = New System.Drawing.Point(147, 294)
        Me.lbl_sw2_model_load.Name = "lbl_sw2_model_load"
        Me.lbl_sw2_model_load.Size = New System.Drawing.Size(115, 13)
        Me.lbl_sw2_model_load.TabIndex = 34
        Me.lbl_sw2_model_load.Text = "store_sw2_model_load"
        '
        'lbl_sw1_model_load
        '
        Me.lbl_sw1_model_load.AutoSize = True
        Me.lbl_sw1_model_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sw1_model_load.Location = New System.Drawing.Point(147, 270)
        Me.lbl_sw1_model_load.Name = "lbl_sw1_model_load"
        Me.lbl_sw1_model_load.Size = New System.Drawing.Size(115, 13)
        Me.lbl_sw1_model_load.TabIndex = 33
        Me.lbl_sw1_model_load.Text = "store_sw1_model_load"
        '
        'lbl_isp_pwd_load
        '
        Me.lbl_isp_pwd_load.AutoSize = True
        Me.lbl_isp_pwd_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_pwd_load.Location = New System.Drawing.Point(147, 222)
        Me.lbl_isp_pwd_load.Name = "lbl_isp_pwd_load"
        Me.lbl_isp_pwd_load.Size = New System.Drawing.Size(119, 13)
        Me.lbl_isp_pwd_load.TabIndex = 32
        Me.lbl_isp_pwd_load.Text = "store_isp_pwd_load"
        '
        'lbl_isp_email_load
        '
        Me.lbl_isp_email_load.AutoSize = True
        Me.lbl_isp_email_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_email_load.Location = New System.Drawing.Point(147, 198)
        Me.lbl_isp_email_load.Name = "lbl_isp_email_load"
        Me.lbl_isp_email_load.Size = New System.Drawing.Size(125, 13)
        Me.lbl_isp_email_load.TabIndex = 31
        Me.lbl_isp_email_load.Text = "store_isp_email_load"
        '
        'lbl_isp_load
        '
        Me.lbl_isp_load.AutoSize = True
        Me.lbl_isp_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_load.Location = New System.Drawing.Point(147, 30)
        Me.lbl_isp_load.Name = "lbl_isp_load"
        Me.lbl_isp_load.Size = New System.Drawing.Size(89, 13)
        Me.lbl_isp_load.TabIndex = 30
        Me.lbl_isp_load.Text = "store_isp_load"
        '
        'lbl_sw3_model
        '
        Me.lbl_sw3_model.AutoSize = True
        Me.lbl_sw3_model.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sw3_model.Location = New System.Drawing.Point(17, 318)
        Me.lbl_sw3_model.Name = "lbl_sw3_model"
        Me.lbl_sw3_model.Size = New System.Drawing.Size(75, 13)
        Me.lbl_sw3_model.TabIndex = 28
        Me.lbl_sw3_model.Text = "SW3 model:"
        '
        'lbl_itcabinet_lock
        '
        Me.lbl_itcabinet_lock.AutoSize = True
        Me.lbl_itcabinet_lock.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_itcabinet_lock.Location = New System.Drawing.Point(17, 342)
        Me.lbl_itcabinet_lock.Name = "lbl_itcabinet_lock"
        Me.lbl_itcabinet_lock.Size = New System.Drawing.Size(102, 13)
        Me.lbl_itcabinet_lock.TabIndex = 10
        Me.lbl_itcabinet_lock.Text = "IT Cabinet Lock:"
        '
        'lbl_router_loc
        '
        Me.lbl_router_loc.AutoSize = True
        Me.lbl_router_loc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_router_loc.Location = New System.Drawing.Point(17, 366)
        Me.lbl_router_loc.Name = "lbl_router_loc"
        Me.lbl_router_loc.Size = New System.Drawing.Size(102, 13)
        Me.lbl_router_loc.TabIndex = 9
        Me.lbl_router_loc.Text = "Router Location:"
        '
        'lbl_isp_pwd
        '
        Me.lbl_isp_pwd.AutoSize = True
        Me.lbl_isp_pwd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_pwd.Location = New System.Drawing.Point(17, 222)
        Me.lbl_isp_pwd.Name = "lbl_isp_pwd"
        Me.lbl_isp_pwd.Size = New System.Drawing.Size(65, 13)
        Me.lbl_isp_pwd.TabIndex = 8
        Me.lbl_isp_pwd.Text = "Password:"
        '
        'lbl_isp_email
        '
        Me.lbl_isp_email.AutoSize = True
        Me.lbl_isp_email.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp_email.Location = New System.Drawing.Point(17, 198)
        Me.lbl_isp_email.Name = "lbl_isp_email"
        Me.lbl_isp_email.Size = New System.Drawing.Size(67, 13)
        Me.lbl_isp_email.TabIndex = 7
        Me.lbl_isp_email.Text = "Username:"
        '
        'lbl_isp
        '
        Me.lbl_isp.AutoSize = True
        Me.lbl_isp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_isp.Location = New System.Drawing.Point(17, 30)
        Me.lbl_isp.Name = "lbl_isp"
        Me.lbl_isp.Size = New System.Drawing.Size(31, 13)
        Me.lbl_isp.TabIndex = 6
        Me.lbl_isp.Text = "ISP:"
        '
        'lbl_sw2_model
        '
        Me.lbl_sw2_model.AutoSize = True
        Me.lbl_sw2_model.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sw2_model.Location = New System.Drawing.Point(17, 294)
        Me.lbl_sw2_model.Name = "lbl_sw2_model"
        Me.lbl_sw2_model.Size = New System.Drawing.Size(75, 13)
        Me.lbl_sw2_model.TabIndex = 5
        Me.lbl_sw2_model.Text = "SW2 model:"
        '
        'lbl_sw1_model
        '
        Me.lbl_sw1_model.AutoSize = True
        Me.lbl_sw1_model.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sw1_model.Location = New System.Drawing.Point(17, 270)
        Me.lbl_sw1_model.Name = "lbl_sw1_model"
        Me.lbl_sw1_model.Size = New System.Drawing.Size(75, 13)
        Me.lbl_sw1_model.TabIndex = 4
        Me.lbl_sw1_model.Text = "SW1 model:"
        '
        'lbl_moreinfo_store
        '
        Me.lbl_moreinfo_store.AutoSize = True
        Me.lbl_moreinfo_store.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_moreinfo_store.Location = New System.Drawing.Point(38, 13)
        Me.lbl_moreinfo_store.Name = "lbl_moreinfo_store"
        Me.lbl_moreinfo_store.Size = New System.Drawing.Size(67, 15)
        Me.lbl_moreinfo_store.TabIndex = 12
        Me.lbl_moreinfo_store.Text = "Store No:"
        '
        'lbl_moreinfo_store_load
        '
        Me.lbl_moreinfo_store_load.AutoSize = True
        Me.lbl_moreinfo_store_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_moreinfo_store_load.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_moreinfo_store_load.Location = New System.Drawing.Point(102, 13)
        Me.lbl_moreinfo_store_load.Name = "lbl_moreinfo_store_load"
        Me.lbl_moreinfo_store_load.Size = New System.Drawing.Size(68, 16)
        Me.lbl_moreinfo_store_load.TabIndex = 13
        Me.lbl_moreinfo_store_load.Text = "store_no"
        '
        'lbl_combo_load
        '
        Me.lbl_combo_load.AutoSize = True
        Me.lbl_combo_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_combo_load.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_combo_load.Location = New System.Drawing.Point(601, 15)
        Me.lbl_combo_load.Name = "lbl_combo_load"
        Me.lbl_combo_load.Size = New System.Drawing.Size(54, 13)
        Me.lbl_combo_load.TabIndex = 38
        Me.lbl_combo_load.Text = "isCombo"
        '
        'btn_moreInfo_close
        '
        Me.btn_moreInfo_close.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_moreInfo_close.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_moreInfo_close.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_moreInfo_close.Location = New System.Drawing.Point(500, 414)
        Me.btn_moreInfo_close.Name = "btn_moreInfo_close"
        Me.btn_moreInfo_close.Size = New System.Drawing.Size(95, 21)
        Me.btn_moreInfo_close.TabIndex = 39
        Me.btn_moreInfo_close.Text = "Close"
        Me.btn_moreInfo_close.UseVisualStyleBackColor = False
        '
        'lbl_moreinfo_pinpadcon
        '
        Me.lbl_moreinfo_pinpadcon.AutoSize = True
        Me.lbl_moreinfo_pinpadcon.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_moreinfo_pinpadcon.Location = New System.Drawing.Point(181, 13)
        Me.lbl_moreinfo_pinpadcon.Name = "lbl_moreinfo_pinpadcon"
        Me.lbl_moreinfo_pinpadcon.Size = New System.Drawing.Size(93, 15)
        Me.lbl_moreinfo_pinpadcon.TabIndex = 40
        Me.lbl_moreinfo_pinpadcon.Text = "Pinpad Conn:"
        '
        'lbl_moreinfo_pinpadcon_load
        '
        Me.lbl_moreinfo_pinpadcon_load.AutoSize = True
        Me.lbl_moreinfo_pinpadcon_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_moreinfo_pinpadcon_load.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_moreinfo_pinpadcon_load.Location = New System.Drawing.Point(271, 13)
        Me.lbl_moreinfo_pinpadcon_load.Name = "lbl_moreinfo_pinpadcon_load"
        Me.lbl_moreinfo_pinpadcon_load.Size = New System.Drawing.Size(90, 16)
        Me.lbl_moreinfo_pinpadcon_load.TabIndex = 41
        Me.lbl_moreinfo_pinpadcon_load.Text = "ppcon_load"
        '
        'lbl_moreinfo_storesys
        '
        Me.lbl_moreinfo_storesys.AutoSize = True
        Me.lbl_moreinfo_storesys.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_moreinfo_storesys.Location = New System.Drawing.Point(374, 13)
        Me.lbl_moreinfo_storesys.Name = "lbl_moreinfo_storesys"
        Me.lbl_moreinfo_storesys.Size = New System.Drawing.Size(67, 15)
        Me.lbl_moreinfo_storesys.TabIndex = 42
        Me.lbl_moreinfo_storesys.Text = "StoreSys:"
        '
        'lbl_moreinfo_storesys_load
        '
        Me.lbl_moreinfo_storesys_load.AutoSize = True
        Me.lbl_moreinfo_storesys_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_moreinfo_storesys_load.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_moreinfo_storesys_load.Location = New System.Drawing.Point(438, 13)
        Me.lbl_moreinfo_storesys_load.Name = "lbl_moreinfo_storesys_load"
        Me.lbl_moreinfo_storesys_load.Size = New System.Drawing.Size(88, 16)
        Me.lbl_moreinfo_storesys_load.TabIndex = 43
        Me.lbl_moreinfo_storesys_load.Text = "strsys_load"
        '
        'lbl_router_model
        '
        Me.lbl_router_model.AutoSize = True
        Me.lbl_router_model.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_router_model.Location = New System.Drawing.Point(17, 246)
        Me.lbl_router_model.Name = "lbl_router_model"
        Me.lbl_router_model.Size = New System.Drawing.Size(86, 13)
        Me.lbl_router_model.TabIndex = 49
        Me.lbl_router_model.Text = "Router model:"
        '
        'lbl_router_model_load
        '
        Me.lbl_router_model_load.AutoSize = True
        Me.lbl_router_model_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_router_model_load.Location = New System.Drawing.Point(147, 246)
        Me.lbl_router_model_load.Name = "lbl_router_model_load"
        Me.lbl_router_model_load.Size = New System.Drawing.Size(123, 13)
        Me.lbl_router_model_load.TabIndex = 50
        Me.lbl_router_model_load.Text = "store_router_model_load"
        '
        'form_more_info
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(678, 443)
        Me.Controls.Add(Me.lbl_moreinfo_storesys_load)
        Me.Controls.Add(Me.lbl_moreinfo_storesys)
        Me.Controls.Add(Me.lbl_moreinfo_pinpadcon_load)
        Me.Controls.Add(Me.lbl_moreinfo_pinpadcon)
        Me.Controls.Add(Me.btn_moreInfo_close)
        Me.Controls.Add(Me.lbl_combo_load)
        Me.Controls.Add(Me.grp_network)
        Me.Controls.Add(Me.lbl_combo)
        Me.Controls.Add(Me.lbl_moreinfo_store_load)
        Me.Controls.Add(Me.lbl_moreinfo_store)
        Me.Controls.Add(Me.grp_equip)
        Me.Controls.Add(Me.grp_addinfo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "form_more_info"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - More Info"
        Me.grp_addinfo.ResumeLayout(False)
        Me.grpbox_additional_lines.ResumeLayout(False)
        Me.grpbox_additional_lines.PerformLayout()
        Me.grpbox_moreinfo_tel.ResumeLayout(False)
        Me.grpbox_moreinfo_tel.PerformLayout()
        Me.grp_equip.ResumeLayout(False)
        Me.grp_equip.PerformLayout()
        Me.grp_network.ResumeLayout(False)
        Me.grp_network.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents grp_addinfo As GroupBox
    Friend WithEvents lbl_combo As Label
    Friend WithEvents lbl_fax_line As Label
    Friend WithEvents lbl_bo_line As Label
    Friend WithEvents grp_equip As GroupBox
    Friend WithEvents lbl_spare_modem As Label
    Friend WithEvents lbl_spare_router As Label
    Friend WithEvents lbl_spare_wc As Label
    Friend WithEvents lbl_holoday_cash As Label
    Friend WithEvents grp_network As GroupBox
    Friend WithEvents lbl_itcabinet_lock As Label
    Friend WithEvents lbl_router_loc As Label
    Friend WithEvents lbl_isp_pwd As Label
    Friend WithEvents lbl_isp_email As Label
    Friend WithEvents lbl_isp As Label
    Friend WithEvents lbl_sw2_model As Label
    Friend WithEvents lbl_sw1_model As Label
    Friend WithEvents lbl_dsl_line As Label
    Friend WithEvents lbl_dss_name As Label
    Friend WithEvents lbl_dss_email As Label
    Friend WithEvents lbl_dss_phone As Label
    Friend WithEvents lbl_moreinfo_store As Label
    Friend WithEvents lbl_moreinfo_store_load As Label
    Friend WithEvents grpbox_moreinfo_tel As GroupBox
    Friend WithEvents lbl_director As Label
    Friend WithEvents lbl_director_line As Label
    Friend WithEvents grpbox_additional_lines As GroupBox
    Friend WithEvents lbl_sw3_model As Label
    Friend WithEvents lbl_spare_pinpad As Label
    Friend WithEvents lbl_isp_load As Label
    Friend WithEvents lbl_isp_email_load As Label
    Friend WithEvents lbl_isp_pwd_load As Label
    Friend WithEvents lbl_sw1_model_load As Label
    Friend WithEvents lbl_sw2_model_load As Label
    Friend WithEvents lbl_sw3_model_load As Label
    Friend WithEvents lbl_itcabinet_lock_load As Label
    Friend WithEvents lbl_router_location_load As Label
    Friend WithEvents lbl_combo_load As Label
    Friend WithEvents lbl_bo_line_load As Label
    Friend WithEvents lbl_fax_line_load As Label
    Friend WithEvents lbl_dsl_line_load As Label
    Friend WithEvents lbl_dss_name_load As Label
    Friend WithEvents lbl_dss_line_load As Label
    Friend WithEvents lbl_dss_email_load As Label
    Friend WithEvents lbl_director_load As Label
    Friend WithEvents lbl_director_line_load As Label
    Friend WithEvents lbl_spare_modem_load As Label
    Friend WithEvents lbl_spare_router_load As Label
    Friend WithEvents lbl_spare_pinpad_load As Label
    Friend WithEvents lbl_spare_hoc_load As Label
    Friend WithEvents lbl_spare_wc_load As Label
    Friend WithEvents btn_moreInfo_close As Button
    Friend WithEvents lbl_isp_account As Label
    Friend WithEvents lbl_isp_account_load As Label
    Friend WithEvents lbl_isp_status As Label
    Friend WithEvents lbl_isp_status_load As Label
    Friend WithEvents lbl_isp_address As Label
    Friend WithEvents lbl_isp_address_load As Label
    Friend WithEvents lbl_isp_subnet As Label
    Friend WithEvents lbl_isp_subnet_load As Label
    Friend WithEvents lbl_isp_gateway As Label
    Friend WithEvents lbl_isp_gateway_load As Label
    Friend WithEvents lbl_moreinfo_pinpadcon As Label
    Friend WithEvents lbl_moreinfo_pinpadcon_load As Label
    Friend WithEvents lbl_moreinfo_storesys As Label
    Friend WithEvents lbl_moreinfo_storesys_load As Label
    Friend WithEvents lbl_router_model As Label
    Friend WithEvents lbl_router_model_load As Label
End Class
