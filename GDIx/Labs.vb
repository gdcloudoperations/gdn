﻿'AUTHOR: Alex Dumitrascu
'DATE: 27-04-2019
'UPDATE: 11-05-2018
'
'FORM: Labs.vb (access from MAIN)


Imports System.Data.SqlClient
Imports System.IO
Imports System.Net.NetworkInformation

Public Class Labs

    Private labs_tooltip = New ToolTip()                    'used for loading tooltips
    Private lab_loaded As Boolean = False                   'lab is loaded
    Private listOfPing As New List(Of String)               'LIST OF IPs USED FOR PING
    Private listOfDevices As New List(Of String)
    Private dataPing As Ping = Nothing                      'PING VARIABLE
    Protected Friend listcount As Integer = 0                         'ping list counter
    Protected Friend device_name As String            'store network device name (stores)

    'DB VARS (store info from labs DB)
    Protected Friend store_sys, env As New String(String.Empty)
    Protected Friend lab_status, project, pm As New String(String.Empty)
    Protected Friend bo_hostname, reg1_hostname, reg2_hostname As String
    Protected Friend bo_location, reg1_location, reg2_location, pinpad1_location, pinpad2_location, printer1_location, printer2_location As String
    Protected Friend bo_ipaddress, reg1_ipaddress, reg2_ipaddress, pinpad1_ipaddress, pinpad2_ipaddress, printer1_ipaddress, printer2_ipaddress As String
    Protected Friend bo_install_date, reg1_install_date, reg2_install_date, pinpad1_install_date, pinpad2_install_date, printer1_install_date, printer2_install_date As String
    Protected Friend extra_patches, comments As String


    'MAIN (ON LOAD)
    Private Sub Labs_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.KeyPreview = True

        'load topmenu selections
        LABSToolStripMenuItem.Text = "LABS   F7 " & ChrW(&H2713)                                'load labs mode
        PRODToolStripMenuItem.Text = "PROD   F8"                                                'unload prod mode
        OnToolStripMenuItem.Text = gdx_main.OnToolStripMenuItem.Text                            'set autoupdate on text
        OffToolStripMenuItem.Text = gdx_main.OffToolStripMenuItem.Text                          'set autoupdate off text
        ChromeToolStripMenuItem.Text = gdx_main.ChromeToolStripMenuItem.Text                    'set chrome text             
        IEToolStripMenuItem.Text = gdx_main.IEToolStripMenuItem.Text                            'set iexplorer text   
        RDPToolStripMenuItem.Text = gdx_main.RDPToolStripMenuItem.Text                          'set rdp text
        TeamViewerToolStripMenuItem1.Text = gdx_main.TeamViewerToolStripMenuItem1.Text          'set teamviewer text
        topmenu_refresh_0sec.Text = gdx_main.topmenu_refresh_0sec.Text                          'set auto-refresh 0sec text
        topmenu_refresh_10sec.Text = gdx_main.topmenu_refresh_10sec.Text                        'set auto-refresh 10sec text
        topmenu_refresh_30sec.Text = gdx_main.topmenu_refresh_30sec.Text                        'set auto-refresh 30sec text
        topmenu_refresh_60sec.Text = gdx_main.topmenu_refresh_60sec.Text                        'set auto-refresh 60sec text
        SavePositionToolStripMenuItem.Text = gdx_main.SavePositionToolStripMenuItem.Text        'set form position

        'load user info
        lbl_labs_refresh_load.Text = gdx_main.lbl_refresh_load.Text
        lbl_labs_vpn_con_load.Text = gdx_main.lbl_vpn_con_load.Text
        If lbl_labs_vpn_con_load.Text = "Disconnected" Then
            lbl_labs_vpn_con_load.ForeColor = Color.Red
        Else
            lbl_labs_vpn_con_load.ForeColor = Color.Green
        End If
        lbl_labs_user_load.Text = gdx_main.lbl_user_load.Text
        lbl_labs_role_load.Text = gdx_main.lbl_role_load.Text

        'set form location
        Me.Location = New System.Drawing.Point(gdx_main.Location.X, gdx_main.Location.Y)

        'FORM ACCESS RIGHTS

        'ADMIN CONSOLE
        If gdx_main.user_level = "full" Or gdx_main.user_level = "elevated" Or gdx_main.user_level = "general+" Or gdx_main.user_level = "limited+" Then
            AdminToolStripMenuItem.Enabled = True
        Else
            AdminToolStripMenuItem.Enabled = False
        End If

        'ADD, EDIT, DELETE LABS ACCESS
        If gdx_main.accountRole = "DBA" Or gdx_main.accountRole = "HO Support" Or gdx_main.accountRole = "Tech Support" Or gdx_main.accountRole = "Tech Support*" Then
            EditToolStripMenuItem.Enabled = False
            DeleteStoreToolStripMenuItem.Enabled = False
            AddToolStripMenuItem.Enabled = False
            btn_labs_save.Enabled = False
        ElseIf gdx_main.accountRole = "ORPOS" Then
            EditToolStripMenuItem.Enabled = True
            DeleteStoreToolStripMenuItem.Enabled = False
            AddToolStripMenuItem.Enabled = False
        ElseIf gdx_main.accountRole = "DEV" Then
            EditToolStripMenuItem.Enabled = True
            DeleteStoreToolStripMenuItem.Enabled = True
            AddToolStripMenuItem.Enabled = True
        ElseIf gdx_main.accountRole = "*SYS" Or gdx_main.accountRole = "Admin" Then
            EditToolStripMenuItem.Enabled = True
            DeleteStoreToolStripMenuItem.Enabled = True
            AddToolStripMenuItem.Enabled = True
        Else
            EditToolStripMenuItem.Enabled = False
            DeleteStoreToolStripMenuItem.Enabled = False
            AddToolStripMenuItem.Enabled = False
            btn_labs_save.Enabled = False
        End If

        'clear labs list
        listview_labs_stores.Items.Clear()
        lbl_labs_abailable_load.Text = Nothing

        'GET LABS from DB
        getLabsList()

        'clear all info (of loaded store)
        clearAllInfo()

        'show form tooltips
        loadFormTooltips()

        'start ping timer for labs
        If topmenu_refresh_0sec.Text = "0sec - no updates " & ChrW(&H2713) Then
            topmenu_refresh_0sec.PerformClick()
        ElseIf topmenu_refresh_10sec.Text = "10sec " & ChrW(&H2713) Then
            topmenu_refresh_10sec.PerformClick()
        ElseIf topmenu_refresh_30sec.Text = "30sec " & ChrW(&H2713) Then
            topmenu_refresh_30sec.PerformClick()
        ElseIf topmenu_refresh_60sec.Text = "60sec " & ChrW(&H2713) Then
            topmenu_refresh_60sec.PerformClick()
        End If

    End Sub

    'KEYBOARD SHORTCUTS
    Private Sub Labs_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown

        'NAVIGATION DONE ON INDIVIDUAL SUBS & FUNCTIONS

        'Tools
        If (e.KeyCode = Keys.F1) Then                           'F1 = Help
            HelpF1ToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F2) Then                       'F2 = Keyboard Shortcuts
            KeyboardShortcutsToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F5) Then                       'F5 = Refresh
            RefreshToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F7) Then                       'F7 = LABS
            LABSToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F8) Then                       'F8 = PROD
            PRODToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F10) Then                      'F10 = Save Position
            SavePositionToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F11) Then                      'F11 = Check for updates
            UpdatesToolStripMenuItem.PerformClick()
        ElseIf (e.KeyCode = Keys.F12) Then                      'F12 = Exit
            ExitToolStripMenuItem.PerformClick()
        End If

    End Sub

    'TOP TOOL BAR (Mode PROD)
    Private Sub PRODToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PRODToolStripMenuItem.Click

        'close form and set prod mode
        Me.Dispose()
        gdx_main.Show()
        gdx_main.ping_timer.Start()
        gdx_main.ReloadToolStripMenuItem.PerformClick()
        gdx_main.PRODToolStripMenuItem.PerformClick()

    End Sub

    'TOP TOOL BAR (Mode Admin)
    Private Sub AdminToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AdminToolStripMenuItem.Click

        gdx_main.AdminToolStripMenuItem.PerformClick()

    End Sub

    'TOP TOOL BAR (Refresh)
    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click

        'clear labs list
        listview_labs_stores.Items.Clear()

        'GET LABS from DB
        getLabsList()

        'clear all labels
        clearAllInfo()

    End Sub

    'TOP TOOL BAR (Reload)
    Private Sub ReloadToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReloadToolStripMenuItem.Click
        Labs_Load(Me, New System.EventArgs)
    End Sub

    'TOP TOOL BAR (Labs: EDIT)
    Private Sub EditToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EditToolStripMenuItem.Click
        If lab_loaded = True Then
            LABS_edit.ShowDialog()
        Else
            MsgBox("Please select a LAB to EDIT!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (selection - edit)")
        End If
    End Sub

    'TOP TOOL BAR (Labs: DELETE)
    Private Sub DeleteStoreToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteStoreToolStripMenuItem.Click

        'check if store is loaded
        If lab_loaded = True Then

            Dim delete_result As Integer = MsgBox("Do you want to delete STORE: " & lbl_labs_strno_load.Text, MsgBoxStyle.YesNo, Title:="GDnetworks - store DELETE")
            If delete_result = DialogResult.No Then
                'NOTHING
            Else

                Dim lab_connection As SqlConnection
                Dim cmd_lab As SqlCommand
                Dim delStore As String = "delete from labs_info where store = " & lbl_labs_strno_load.Text & ""

                lab_connection = New SqlConnection(gdx_main.connectionString)
                cmd_lab = New SqlCommand(delStore, lab_connection)

                Try
                    lab_connection.Open()                    'OPEN DELETE CONNECTION
                    cmd_lab.ExecuteNonQuery()                'RUN SQL COMMAND

                    MsgBox("LAB " & lbl_labs_strno_load.Text & " deleted from DB.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (LAB deleted)")
                    btn_labs_reload.PerformClick()

                    'CLOSE CONNECTIONS
                    lab_connection.Close()

                Catch ex As Exception
                    MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [delete LAB]")
                End Try
            End If
        Else
            MsgBox("Please select a LAB to DELETE!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (selection - delete)")
        End If

    End Sub

    'TOP TOOL BAR (Labs: ADD)
    Private Sub AddToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AddToolStripMenuItem.Click
        LABS_new.ShowDialog()
    End Sub

    'TOP TOOL BAR (EXIT)
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        gdx_main.ActiveForm.Close()
    End Sub

    'TOP TOOL BAR (Support docs: Gdrive)
    Private Sub GDriveToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GDriveToolStripMenuItem.Click
        gdx_main.GDriveToolStripMenuItem.PerformClick()
    End Sub

    'TOP TOOL BAR (Support docs: Jira)
    Private Sub JiraToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles JiraToolStripMenuItem.Click
        gdx_main.JiraToolStripMenuItem.PerformClick()
    End Sub

    'TOP TOOL BAR (Support docs: Local)
    Private Sub LocalToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LocalToolStripMenuItem.Click
        gdx_main.LocalToolStripMenuItem.PerformClick()
    End Sub

    'TOP TOOL BAR (WEB Garage CA)
    Private Sub GarageToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GarageToolStripMenuItem.Click
        Process.Start(gdx_main.set_browser, "https://www.garageclothing.com/ca/")
    End Sub

    'TOP TOOL BAR (WEB Garage US)
    Private Sub DynamiteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DynamiteToolStripMenuItem.Click
        Process.Start(gdx_main.set_browser, "https://www.garageclothing.com/us/")
    End Sub

    'TOP TOOL BAR (WEB Dynamite CA)
    Private Sub DynamiteCAToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DynamiteCAToolStripMenuItem.Click
        Process.Start(gdx_main.set_browser, "https://www.dynamiteclothing.com/ca/")
    End Sub

    'TOP TOOL BAR (WEB Dynamite US)
    Private Sub DynamiteUSToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DynamiteUSToolStripMenuItem.Click
        Process.Start(gdx_main.set_browser, "https://www.dynamiteclothing.com/us/")
    End Sub

    'TOP TOOL BAR (WEB DYNAMITE MAIN)
    Private Sub GRPDYNToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GRPDYNToolStripMenuItem.Click
        Process.Start(gdx_main.set_browser, "http://groupedynamite.com/")
    End Sub

    'TOP TOOL BAR (Remote Desktop)
    Private Sub RemoteDesktopToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoteDesktopToolStripMenuItem.Click
        gdx_main.RemoteDesktopToolStripMenuItem.PerformClick()
    End Sub

    'TOP TOOL BAR (TeamViewer)
    Private Sub TeamViewerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TeamViewerToolStripMenuItem.Click
        gdx_main.TeamViewerToolStripMenuItem.PerformClick()
    End Sub

    'TOP TOOL BAR (RDP TOOL)
    Private Sub RDPToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RDPToolStripMenuItem.Click

        gdx_main.RDPToolStripMenuItem.PerformClick()

        RDPToolStripMenuItem.Text = "RDP " & ChrW(&H2713)
        TeamViewerToolStripMenuItem1.Text = "Team Viewer"
    End Sub

    'TOP TOOL BAR (TV TOOL)
    Private Sub TeamViewerToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles TeamViewerToolStripMenuItem1.Click

        gdx_main.TeamViewerToolStripMenuItem1.PerformClick()

        TeamViewerToolStripMenuItem1.Text = "Team Viewer " & ChrW(&H2713)
        RDPToolStripMenuItem.Text = "RDP"
    End Sub

    'TOP TOOL BAR (AUTOUPDATES ON)
    Private Sub OnToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OnToolStripMenuItem.Click

        gdx_main.OnToolStripMenuItem.PerformClick()

        OnToolStripMenuItem.Text = "On " & ChrW(&H2713)
        OffToolStripMenuItem.Text = "Off"
    End Sub

    'TOP TOOL BAR (AUTOUPDATES OFF)
    Private Sub OffToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OffToolStripMenuItem.Click

        gdx_main.OffToolStripMenuItem.PerformClick()

        OnToolStripMenuItem.Text = "On"
        OffToolStripMenuItem.Text = "Off " & ChrW(&H2713)
    End Sub

    'TOP TOOL BAR (Setbrowser Chrome)
    Private Sub ChromeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ChromeToolStripMenuItem.Click

        gdx_main.ChromeToolStripMenuItem.PerformClick()

        ChromeToolStripMenuItem.Text = "Chrome " & ChrW(&H2713)
        IEToolStripMenuItem.Text = "IExplorer"
    End Sub

    'TOP TOOL BAR (Setbrowser IExplorer)
    Private Sub IEToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles IEToolStripMenuItem.Click

        gdx_main.IEToolStripMenuItem.PerformClick()

        IEToolStripMenuItem.Text = "IExplorer " & ChrW(&H2713)
        ChromeToolStripMenuItem.Text = "Chrome"

    End Sub

    'TOP TOOL BAR (Refresh rate 0sec)
    Private Sub topmenu_refresh_0sec_Click(sender As Object, e As EventArgs) Handles topmenu_refresh_0sec.Click

        ping_timer_labs.Enabled = False
        ping_timer_labs.Stop()

        'Load Menu text
        topmenu_refresh_0sec.Text = "0sec - no updates " & ChrW(&H2713)
        topmenu_refresh_10sec.Text = "10sec"
        topmenu_refresh_30sec.Text = "30sec"
        topmenu_refresh_60sec.Text = "60sec"
    End Sub

    'TOP TOOL BAR (Refresh rate 10sec)
    Private Sub topmenu_refresh_10sec_Click(sender As Object, e As EventArgs) Handles topmenu_refresh_10sec.Click

        ping_timer_labs.Interval = 10000
        ping_timer_labs.Enabled = True
        ping_timer_labs.Start()

        'load menu text
        topmenu_refresh_0sec.Text = "0sec - no updates"
        topmenu_refresh_10sec.Text = "10sec " & ChrW(&H2713)
        topmenu_refresh_30sec.Text = "30sec"
        topmenu_refresh_60sec.Text = "60sec"
    End Sub

    'TOP TOOL BAR (Refresh rate 30sec)
    Private Sub topmenu_refresh_30sec_Click(sender As Object, e As EventArgs) Handles topmenu_refresh_30sec.Click

        ping_timer_labs.Interval = 30000
        ping_timer_labs.Enabled = True
        ping_timer_labs.Start()

        'load menu text
        topmenu_refresh_0sec.Text = "0sec - no updates"
        topmenu_refresh_10sec.Text = "10sec"
        topmenu_refresh_30sec.Text = "30sec " & ChrW(&H2713)
        topmenu_refresh_60sec.Text = "60sec"
    End Sub

    'TOP TOOL BAR (Refresh rate 60sec)
    Private Sub topmenu_refresh_60sec_Click(sender As Object, e As EventArgs) Handles topmenu_refresh_60sec.Click

        ping_timer_labs.Interval = 60000
        ping_timer_labs.Enabled = True
        ping_timer_labs.Start()

        'load menu text
        topmenu_refresh_0sec.Text = "0sec - no updates"
        topmenu_refresh_10sec.Text = "10sec"
        topmenu_refresh_30sec.Text = "30sec"
        topmenu_refresh_60sec.Text = "60sec " & ChrW(&H2713)
    End Sub


    'TOP TOOL BAR (Save Form Position)
    Private Sub SavePositionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SavePositionToolStripMenuItem.Click

        gdx_main.set_position = Me.Location.X.ToString
        gdx_main.set_position = gdx_main.set_position & "/" & Me.Location.Y.ToString
        gdx_main.SavePositionToolStripMenuItem.PerformClick()
        SavePositionToolStripMenuItem.Text = "Save Position   F10 " & ChrW(&H2713)
    End Sub

    'TOP TOOL BAR (ON MAP)
    Private Sub OnMapToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles OnMapToolStripMenuItem.Click
        If lab_loaded = False Then
            MsgBox("You must select a store first!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (view onMap)")
        Else
            Dim store_address As String = lbl_labs_address_load.Text & " " & lbl_labs_city_load.Text & " " & lbl_labs_state_load.Text & " " & lbl_labs_country_load.Text
            Dim google_address As String = store_address.Replace(" ", "+")
            Process.Start(gdx_main.set_browser, "https://www.google.com/maps/search/?api=1&query=" & google_address)
        End If
    End Sub

    'BUTTON: DEVICES LIST
    Private Sub btn_list_status_Click(sender As Object, e As EventArgs)
        form_devices_list.Show()
    End Sub

    'BUTTON: RELOAD
    Private Sub btn_labs_reload_Click(sender As Object, e As EventArgs) Handles btn_labs_reload.Click
        RefreshToolStripMenuItem.PerformClick()
    End Sub

    'BUTTON: export labs list
    Private Sub btn_labs_export_Click(sender As Object, e As EventArgs) Handles btn_labs_export.Click

        Dim sfd As New SaveFileDialog With {
        .Title = "Select file location",
        .FileName = "GDnReports_LabsList.csv",
        .Filter = "CSV (*.csv)|*.csv",
        .FilterIndex = 0,
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}

        'save CSV fIle 

        If sfd.ShowDialog = DialogResult.OK Then

            Dim headers = (From ch In listview_labs_stores.Columns Let header = DirectCast(ch, ColumnHeader) Select header.Text).ToArray()

            Dim items() = (From item In listview_labs_stores.Items Let lvi = DirectCast(item, ListViewItem) Select (From subitem In lvi.SubItems
                                                                                                                    Let si = DirectCast(subitem, ListViewItem.ListViewSubItem)
                                                                                                                    Select si.Text).ToArray()).ToArray()
            Dim table As String = String.Join(",", headers) & Environment.NewLine

            For Each a In items
                table &= String.Join(",", a) & Environment.NewLine
            Next

            table = table.TrimEnd(CChar(vbCr), CChar(vbLf))
            IO.File.WriteAllText(sfd.FileName, table)

        End If
    End Sub

    'BUTTON: refresh store
    Private Sub btn_labs_refresh_store_Click(sender As Object, e As EventArgs) Handles btn_labs_refresh_store.Click

        'lookup store
        If lab_loaded = False Then
            'do nothing
        Else
            Dim selected_lab As String = lbl_labs_strno_load.Text
            clearAllInfo()
            form_devices_list.listview_devices.Items.Clear()
            'reload
            selectLab(selected_lab.ToString())
        End If

    End Sub

    'BUTTON: CLEAR
    Private Sub btn_labs_clear_Click_1(sender As Object, e As EventArgs) Handles btn_labs_clear.Click
        clearAllInfo()
    End Sub

    'BUTTON: SAVE COMMENTS
    Private Sub btn_labs_save_Click(sender As Object, e As EventArgs) Handles btn_labs_save.Click

        If lab_loaded = False Then
            MsgBox("You must select a store first!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (save comments)")
        Else
            editComments(lbl_labs_strno_load.Text, txtbox_labs_comments.Text)
        End If

    End Sub


    'EDIT LAB

    'EDIT LAB - COMMENTS ONLY
    Private Sub editComments(STORE As String, comments As String)

        'VARS
        Dim lab_connection As SqlConnection
        Dim cmd_lab As SqlCommand

        Dim sql_lab_comments As String = "update labs_info set Comments = " & IIf(String.IsNullOrEmpty(comments), "null", "'" & comments & "'") & " where Store = " & STORE & ""

        'CONNECTION PARA
        lab_connection = New SqlConnection(gdx_main.connectionString)
        cmd_lab = New SqlCommand(sql_lab_comments, lab_connection)


        Try
            lab_connection.Open()                       'OPEN ADD CONNECTION

            'OPDATE DB
            cmd_lab.ExecuteNonQuery()

            MsgBox("Comments added. Please refresh store.", MsgBoxStyle.Information, Title:="GDnetworks - Info! [edit labs - save comments]")

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! (DB) [edit labs - comments]")
        End Try

        'CLOSE CONNECTIONS
        lab_connection.Close()

    End Sub

    'PING CASH1
    Private Sub lbl_labs_cash1_load_Click(sender As Object, e As EventArgs) Handles lbl_labs_cash1_load.Click
        If Not reg1_ipaddress = "" Or Not reg1_ipaddress = Nothing Then
            Process.Start("cmd.exe", "/K ping " + reg1_ipaddress.ToString + " -t")
        End If
    End Sub

    'PING CASH2
    Private Sub lbl_labs_cash2_load_Click(sender As Object, e As EventArgs) Handles lbl_labs_cash2_load.Click
        If Not reg2_ipaddress = "" Or Not reg2_ipaddress = Nothing Then
            Process.Start("cmd.exe", "/K ping " + reg2_ipaddress.ToString + " -t")
        End If
    End Sub

    'PING BO
    Private Sub lbl_labs_bo_load_Click(sender As Object, e As EventArgs) Handles lbl_labs_bo_load.Click
        If Not bo_ipaddress = "" Or Not bo_ipaddress = Nothing Then
            Process.Start("cmd.exe", "/K ping " + bo_ipaddress.ToString + " -t")
        End If
    End Sub

    'PING PRINTER CASH1
    Private Sub pic_labs_printer_c1_Click(sender As Object, e As EventArgs) Handles pic_labs_printer_c1.Click
        If Not printer1_ipaddress = "" Or Not printer1_ipaddress = Nothing Then
            Process.Start("cmd.exe", "/K ping " + printer1_ipaddress.ToString + " -t")
        End If
    End Sub

    'PING PRINTER CASH2
    Private Sub pic_labs_printer_c2_Click(sender As Object, e As EventArgs) Handles pic_labs_printer_c2.Click
        If Not printer2_ipaddress = "" Or Not printer2_ipaddress = Nothing Then
            Process.Start("cmd.exe", "/K ping " + printer2_ipaddress.ToString + " -t")
        End If
    End Sub

    'PING PINPAD CASH1
    Private Sub pic_labs_pinpad_c1_Click(sender As Object, e As EventArgs) Handles pic_labs_pinpad_c1.Click
        If Not pinpad1_ipaddress = "" Or Not pinpad1_ipaddress = Nothing Then
            Process.Start("cmd.exe", "/K ping " + pinpad1_ipaddress.ToString + " -t")
        End If
    End Sub

    'PING PINPAD CASH2
    Private Sub pic_labs_pinpad_c2_Click(sender As Object, e As EventArgs) Handles pic_labs_pinpad_c2.Click
        If Not pinpad2_ipaddress = "" Or Not pinpad2_ipaddress = Nothing Then
            Process.Start("cmd.exe", "/K ping " + pinpad2_ipaddress.ToString + " -t")
        End If
    End Sub

    'BUTTON: Remote Cash1
    Private Sub btn_labs_tv_c1_Click(sender As Object, e As EventArgs) Handles btn_labs_tv_c1.Click
        'check if cash1 exists
        If Not reg1_ipaddress = "" Or Not reg1_ipaddress = Nothing Then
            'lunch with Teamviewer
            If gdx_main.set_remote = "teamviewer" Then
                gdx_main.lunchTeamViewer(reg1_ipaddress)
                'lunch with RDP
            ElseIf gdx_main.set_remote = "rdp" Then
                gdx_main.lunchRemoteDesktop(reg1_ipaddress)
            End If
        Else
            MsgBox("Cash1 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (C1 n/a)")
        End If
    End Sub

    'BUTTON: Remote Cash2
    Private Sub btn_labs_tv_c2_Click(sender As Object, e As EventArgs) Handles btn_labs_tv_c2.Click
        'check if cash2 exists
        If Not reg2_ipaddress = "" Or Not reg2_ipaddress = Nothing Then
            'lunch with Teamviewer
            If gdx_main.set_remote = "teamviewer" Then
                gdx_main.lunchTeamViewer(reg2_ipaddress)
                'lunch with RDP
            ElseIf gdx_main.set_remote = "rdp" Then
                gdx_main.lunchRemoteDesktop(reg2_ipaddress)
            End If
        Else
            MsgBox("Cash2 is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (C2 n/a)")
        End If
    End Sub

    'BUTTON: Remote BO
    Private Sub btn_labs_tv_bo_Click(sender As Object, e As EventArgs) Handles btn_labs_tv_bo.Click
        'check if BO exists
        If Not bo_ipaddress = "" Or Not bo_ipaddress = Nothing Then
            'lunch with Teamviewer
            If gdx_main.set_remote = "teamviewer" Then
                gdx_main.lunchTeamViewer(bo_ipaddress)
                'lunch with RDP
            ElseIf gdx_main.set_remote = "rdp" Then
                gdx_main.lunchRemoteDesktop(bo_ipaddress)
            End If
        Else
            MsgBox("BO is not available.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (BO n/a)")
        End If
    End Sub

    'display ping list
    Private Sub btn_labs_ping_Click_1(sender As Object, e As EventArgs) Handles btn_labs_ping.Click
        form_devices_list.ShowDialog()
    End Sub

    'TOP TOOL BAR (Support Locations)
    Private Sub SupportLocationsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SupportLocationsToolStripMenuItem.Click
        gdx_main.SupportLocationsToolStripMenuItem.PerformClick()
    End Sub

    'CASH1 SHORTCUTS
    Private Sub lbl_labs_cash1_Click(sender As Object, e As EventArgs) Handles lbl_labs_cash1.Click

        'IF IP String is not empty
        If Not lbl_labs_cash1_load.Text = "" Then
            'When using SHIFT+(click)BO.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive
                'BLOCK ACCESS TO C DRIVE FOR noaccess

                If lbl_labs_cash1_load.Text = "Online" Then
                    openDrive(reg1_ipaddress)
                Else
                    MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                End If
            Else
                If lbl_labs_cash1_load.Text = "N/A" Then
                    'do nothing
                Else
                    My.Computer.Clipboard.SetText(reg1_ipaddress.ToString)
                End If
            End If
        Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'CASH2 SHORTCUTS
    Private Sub lbl_labs_cash2_Click(sender As Object, e As EventArgs) Handles lbl_labs_cash2.Click

        'IF IP String is not empty
        If Not lbl_labs_cash2_load.Text = "" Then
            'When using SHIFT+(click)BO.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive
                'BLOCK ACCESS TO C DRIVE FOR noaccess

                If lbl_labs_cash2_load.Text = "Online" Then
                    openDrive(reg2_ipaddress)
                Else
                    MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                End If

            Else
                If lbl_labs_cash2_load.Text = "N/A" Then
                    'do nothing
                Else
                    My.Computer.Clipboard.SetText(reg2_ipaddress.ToString)
                End If
            End If
                Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'BO SHORTCUTS
    Private Sub lbl_labs_bo_Click(sender As Object, e As EventArgs) Handles lbl_labs_bo.Click

        'IF IP String is not empty
        If Not lbl_labs_bo_load.Text = "" Then
            'When using SHIFT+(click)BO.lbl
            If My.Computer.Keyboard.ShiftKeyDown = True Then

                'check if PC is online and connect to C drive
                'BLOCK ACCESS TO C DRIVE FOR noaccess

                If lbl_labs_bo_load.Text = "Online" Then
                    openDrive(bo_ipaddress)
                Else
                    MsgBox("Cannot connect to an offline PC!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (pc offline)")
                End If

                'when using (click)BO.lbl copy IP to clipboard
            ElseIf My.Computer.Keyboard.AltKeyDown Then
                'BLOCK ACCESS TO C DRIVE FOR noaccess
                Process.Start(gdx_main.set_browser, "https://" + bo_ipaddress + ":7002/backoffice")
            Else
                If lbl_labs_bo_load.Text = "N/A" Then
                    'do nothing
                Else
                    My.Computer.Clipboard.SetText(bo_ipaddress.ToString)
                End If
            End If
        Else
            MsgBox("Search for a store first!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (invalid ip)")
        End If

    End Sub

    'TOP TOOL BAR (Help)
    Private Sub HelpF1ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HelpF1ToolStripMenuItem.Click
        gdx_main.HelpF1ToolStripMenuItem.PerformClick()
    End Sub

    'TOP TOOL BAR (Updates)
    Private Sub UpdatesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UpdatesToolStripMenuItem.Click
        gdx_main.UpdatesToolStripMenuItem.PerformClick()
    End Sub

    'TOP TOOL BAR (Keyboard Shortcuts)
    Private Sub KeyboardShortcutsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles KeyboardShortcutsToolStripMenuItem.Click
        gdx_main.KeyboardShortcutsToolStripMenuItem.PerformClick()
    End Sub

    'TOP TOOL BAR (Support Docs)
    Private Sub SupportToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SupportToolStripMenuItem.Click
        gdx_main.SupportToolStripMenuItem.PerformClick()
    End Sub

    'TOP TOOL BAR (About)
    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        gdx_main.AboutToolStripMenuItem.PerformClick()
    End Sub

    'ON CLICK: LOAD GDNET.INFO
    Private Sub pic_labs_Click(sender As Object, e As EventArgs) Handles pic_labs.Click
        Process.Start("www.gdnet.info")
    End Sub

    'GET LABS from DB
    Private Sub getLabsList()

        'count available labs
        Dim labs_avaliable As Integer = Nothing
        Dim labs_reserved As Integer = Nothing
        Dim labs_torebuild As Integer = Nothing

        'connection VARS
        Dim labs_connection As SqlConnection
        Dim cmd_labs As SqlCommand
        Dim labsReader As SqlDataReader
        Dim sql_labs = "Select * from labs_info order by Store"
        labs_connection = New SqlConnection(gdx_main.connectionString)
        cmd_labs = New SqlCommand(sql_labs, labs_connection)

        Try
            labs_connection.Open()
            labsReader = cmd_labs.ExecuteReader

            'GET LABS from DB
            If labsReader.HasRows Then
                While labsReader.Read


                    'set items in list
                    Dim newitem As New ListViewItem()
                    newitem.UseItemStyleForSubItems = False
                    newitem.Text = labsReader.GetValue(0).ToString                 'Store                             
                    newitem.SubItems.Add(labsReader.GetValue(1).ToString)          'Environment
                    newitem.SubItems.Add(labsReader.GetValue(2).ToString)          'System  
                    newitem.SubItems.Add(labsReader.GetValue(3).ToString)          'Banner  
                    newitem.SubItems.Add(labsReader.GetValue(9).ToString)          'Country
                    If labsReader.GetValue(8).ToString = "Quebec" Then             'Language
                        newitem.SubItems.Add("FR")
                    Else
                        newitem.SubItems.Add("EN")
                    End If                            'Language
                    newitem.SubItems.Add(labsReader.GetValue(11).ToString)         'BO Location
                    newitem.SubItems.Add(labsReader.GetValue(35).ToString)         'Project
                    newitem.SubItems.Add(labsReader.GetValue(36).ToString)         'PM
                    newitem.SubItems.Add(labsReader.GetValue(34).ToString)         'Status
                    If labsReader.GetValue(34).ToString = "Available" Then
                        labs_avaliable = labs_avaliable + 1
                    ElseIf labsReader.GetValue(34).ToString = "Reserved by Project" Then
                        labs_reserved = labs_reserved + 1
                    ElseIf labsReader.GetValue(34).ToString = "To Rebuild" Then
                        labs_torebuild = labs_torebuild + 1
                    End If

                    newitem.SubItems.Add(labsReader.GetValue(13).ToString)         'BO Build Date

                    'add to list
                    listview_labs_stores.Items.Add(newitem).ToString()
                End While
            End If

            labs_connection.Close()

            'autosize columns & header
            listview_labs_stores.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
            listview_labs_stores.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: labs_info - get LABS]")
        End Try

        'display counted labs
        lbl_labs_abailable_load.Text = labs_avaliable.ToString() & "/" & listview_labs_stores.Items.Count
        lbl_labs_reserved_load.Text = labs_reserved.ToString() & "/" & listview_labs_stores.Items.Count
        lbl_labs_torebuild_load.Text = labs_torebuild.ToString() & "/" & listview_labs_stores.Items.Count

    End Sub

    'Clear all data
    Private Sub clearAllInfo()

        'lab loaded false
        lab_loaded = False

        'clear all items       
        form_devices_list.listview_devices.Items.Clear()

        'clear store info
        clearStoreInfo()

        'clear network info
        clearNetworkInfo()

    End Sub

    'clear store info
    Private Sub clearStoreInfo()

        'store info
        lbl_labs_strno_load.Text = ""
        lbl_labs_status_load.Text = ""
        lbl_labs_ban_load.Text = ""
        lbl_labs_str_system_load.Text = ""
        lbl_labs_store_iscombo_load.Text = ""
        lbl_labs_sis_store_load.Text = ""
        lbl_labs_address_load.Text = ""
        lbl_labs_city_load.Text = ""
        lbl_labs_state_load.Text = ""
        lbl_labs_country_load.Text = ""

        'store details
        ListView_labs_patch.Items.Clear()
        txtbox_labs_comments.Text = Nothing

        'clear vars
        ''network
        bo_hostname = Nothing
        reg1_hostname = Nothing
        reg2_hostname = Nothing
        bo_ipaddress = Nothing
        reg1_ipaddress = Nothing
        reg2_ipaddress = Nothing
        pinpad1_ipaddress = Nothing
        pinpad2_ipaddress = Nothing
        printer1_ipaddress = Nothing
        printer2_ipaddress = Nothing

        ''loations
        bo_location = Nothing
        reg1_location = Nothing
        reg2_location = Nothing
        pinpad1_location = Nothing
        pinpad2_location = Nothing
        printer1_location = Nothing
        printer2_location = Nothing

        ''installation dates
        bo_install_date = Nothing
        reg1_install_date = Nothing
        reg2_install_date = Nothing
        pinpad1_install_date = Nothing
        pinpad2_install_date = Nothing
        printer1_install_date = Nothing
        printer2_install_date = Nothing

        ''additional info
        extra_patches = Nothing
        comments = Nothing

    End Sub

    'clear network info
    Private Sub clearNetworkInfo()

        'cashes
        lbl_labs_cash1_load.Text = Nothing
        lbl_labs_cash2_load.Text = Nothing
        lbl_labs_bo_load.Text = Nothing

        'pinpads & printers
        pic_labs_printer_c1.Image = Nothing
        pic_labs_printer_c2.Image = Nothing
        pic_labs_pinpad_c1.Image = Nothing
        pic_labs_pinpad_c2.Image = Nothing

    End Sub

    'LOAD STORE INFO (ON DOUBLE CLICK)
    Private Sub listview_labs_stores_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles listview_labs_stores.MouseDoubleClick


        'clear all info
        clearAllInfo()

        'clear ping list
        form_devices_list.listview_devices.Items.Clear()

        'lookup store
        selectLab(listview_labs_stores.SelectedItems(0).SubItems(0).Text)


    End Sub

    'load selected lab info
    Private Sub selectLab(STORE As Integer)

        'connection VARS
        Dim labs_connection As SqlConnection
        Dim cmd_labs As SqlCommand
        Dim labsReader As SqlDataReader
        Dim sql_labs = "Select * from labs_info where Store = " & STORE & " order by Store"
        labs_connection = New SqlConnection(gdx_main.connectionString)
        cmd_labs = New SqlCommand(sql_labs, labs_connection)

        Try
            labs_connection.Open()
            labsReader = cmd_labs.ExecuteReader

            'GET LABS from DB
            If labsReader.HasRows Then
                While labsReader.Read

                    'lab loaded
                    lab_loaded = True

                    'store info
                    lbl_labs_strno_load.Text = String.Format("{0:000}", labsReader.GetValue(0))     'store#
                    env = labsReader.GetValue(1).ToString()                                         'environment
                    If IsDBNull(labsReader.GetValue(3)) Then                                        'banner
                        lbl_labs_ban_load.Text = "N/A"
                    Else
                        lbl_labs_ban_load.Text = labsReader.GetValue(3).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(2)) Then                                        'system
                        lbl_labs_str_system_load.Text = "N/A"
                    Else
                        lbl_labs_str_system_load.Text = labsReader.GetValue(2).ToString()
                        store_sys = labsReader.GetValue(2).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(4)) Then                                        'isCombo
                        lbl_labs_store_iscombo_load.Text = "N/A"
                    Else
                        If labsReader.GetValue(4).ToString() = False Then
                            lbl_labs_store_iscombo_load.Text = "NO"
                        Else
                            lbl_labs_store_iscombo_load.Text = "YES"
                        End If
                    End If
                    If IsDBNull(labsReader.GetValue(5)) Then                                        'sister store
                        lbl_labs_sis_store_load.Text = "N/A"
                    Else
                        lbl_labs_sis_store_load.Text = String.Format("{0:000}", labsReader.GetValue(5))
                    End If
                    If IsDBNull(labsReader.GetValue(6)) Then                                        'address
                        lbl_labs_address_load.Text = "N/A"
                    Else
                        lbl_labs_address_load.Text = labsReader.GetValue(6).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(7)) Then                                        'city
                        lbl_labs_city_load.Text = "N/A"
                    Else
                        lbl_labs_city_load.Text = labsReader.GetValue(7).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(8)) Then                                        'state
                        lbl_labs_state_load.Text = "N/A"
                    Else
                        lbl_labs_state_load.Text = labsReader.GetValue(8).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(9)) Then                                        'country
                        lbl_labs_country_load.Text = "N/A"
                    Else
                        lbl_labs_country_load.Text = labsReader.GetValue(9).ToString()
                    End If

                    'network info
                    bo_hostname = labsReader.GetValue(10).ToString()
                    reg1_hostname = labsReader.GetValue(14).ToString()
                    reg2_hostname = labsReader.GetValue(24).ToString()
                    bo_ipaddress = labsReader.GetValue(12).ToString()
                    reg1_ipaddress = labsReader.GetValue(16).ToString()
                    reg2_ipaddress = labsReader.GetValue(26).ToString()
                    pinpad1_ipaddress = labsReader.GetValue(18).ToString()
                    pinpad2_ipaddress = labsReader.GetValue(28).ToString()
                    printer1_ipaddress = labsReader.GetValue(21).ToString()
                    printer2_ipaddress = labsReader.GetValue(31).ToString()

                    'loations & install date
                    If IsDBNull(labsReader.GetValue(11)) Then
                        bo_location = Nothing
                        bo_install_date = Nothing
                    Else
                        bo_location = labsReader.GetValue(11).ToString()
                        bo_install_date = labsReader.GetValue(13).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(15)) Then
                        reg1_location = Nothing
                        reg1_install_date = Nothing
                    Else
                        reg1_location = labsReader.GetValue(15).ToString()
                        reg1_install_date = labsReader.GetValue(17).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(25)) Then
                        reg2_location = Nothing
                        reg2_install_date = Nothing
                    Else
                        reg2_location = labsReader.GetValue(25).ToString()
                        reg2_install_date = labsReader.GetValue(27).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(19)) Then
                        pinpad1_location = Nothing
                        pic_labs_pinpad_c1.Image = Nothing
                    Else
                        pinpad1_location = labsReader.GetValue(19).ToString()
                        pic_labs_pinpad_c1.Image = My.Resources.ResourceManager.GetObject("pinpad_manual")
                        pinpad1_install_date = labsReader.GetValue(20).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(29)) Then
                        pinpad2_location = Nothing
                        pic_labs_pinpad_c2.Image = Nothing
                    Else
                        pinpad2_location = labsReader.GetValue(29).ToString()
                        pic_labs_pinpad_c2.Image = My.Resources.ResourceManager.GetObject("pinpad_manual")
                        pinpad2_install_date = labsReader.GetValue(30).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(22)) Then
                        printer1_location = Nothing
                        pic_labs_printer_c1.Image = Nothing
                    Else
                        printer1_location = labsReader.GetValue(22).ToString()
                        pic_labs_printer_c1.Image = My.Resources.ResourceManager.GetObject("printer_manual")
                        printer1_install_date = labsReader.GetValue(23).ToString()
                    End If
                    If IsDBNull(labsReader.GetValue(32)) Then
                        printer2_location = Nothing
                        pic_labs_printer_c2.Image = Nothing
                    Else
                        printer2_location = labsReader.GetValue(32).ToString()
                        pic_labs_printer_c2.Image = My.Resources.ResourceManager.GetObject("printer_manual")
                        printer2_install_date = labsReader.GetValue(33).ToString()
                    End If

                    'additional info
                    lab_status = labsReader.GetValue(34).ToString       'lab status
                    If lab_status = "Available" Then
                        lbl_labs_status_load.Text = lab_status
                        lbl_labs_status_load.ForeColor = Color.Green
                    ElseIf lab_status = "Decommissioned" Then
                        lbl_labs_status_load.Text = lab_status
                        lbl_labs_status_load.ForeColor = Color.Purple
                    ElseIf lab_status = "Reserved by Project" Then
                        lbl_labs_status_load.Text = lab_status
                        lbl_labs_status_load.ForeColor = Color.Red
                    ElseIf lab_status = "Staging In progress" Then
                        lbl_labs_status_load.Text = lab_status
                        lbl_labs_status_load.ForeColor = Color.Blue
                    ElseIf lab_status = "To Rebuild" Then
                        lbl_labs_status_load.Text = lab_status
                        lbl_labs_status_load.ForeColor = Color.Orange
                    End If

                    project = labsReader.GetValue(35).ToString          'project
                    pm = labsReader.GetValue(36).ToString               'pm
                    'patches
                    If IsDBNull(labsReader.GetValue(37)) Then
                        extra_patches = Nothing
                    Else
                        extra_patches = labsReader.GetValue(37).ToString()
                    End If
                    'comments
                    If IsDBNull(labsReader.GetValue(38)) Then
                        comments = Nothing
                    Else
                        comments = labsReader.GetValue(38).ToString()
                    End If

                End While
            End If

            'close connection
            labs_connection.Close()

            'display network info (ping first)
            pingLAB()

            'display patches and comments
            printPatchesAndComments()

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: labs_info - get LAB details]")
            lab_loaded = False
        End Try

    End Sub

    'PING LAB DEVICES
    Private Sub pingLAB()

        'ping Cash1
        If Not reg1_ipaddress.ToString = "" Then
            PingEachDevice(reg1_ipaddress, "Reg1")
        End If

        'ping Cash2
        If Not reg2_ipaddress.ToString = "" Then
            PingEachDevice(reg2_ipaddress, "Reg2")
        End If

        'ping BO
        If Not bo_ipaddress = "" Then
            PingEachDevice(bo_ipaddress, "BO")
        End If

        'ping pinpad1
        If Not pinpad1_ipaddress = "" Then
            PingEachDevice(pinpad1_ipaddress, "Pinpad1")
        End If
        'ping pinpad2
        If Not pinpad2_ipaddress = "" Then
            PingEachDevice(pinpad2_ipaddress, "Pinpad2")
        End If

        'ping printer1
        If Not printer1_ipaddress = "" Then
            PingEachDevice(printer1_ipaddress, "Printer1")
        End If
        'ping printer2
        If Not printer2_ipaddress = "" Then
            PingEachDevice(printer2_ipaddress, "Printer2")
        End If

    End Sub

    'SET LIST OF DEVICES TO PINGs
    Private Sub PingEachDevice(IP As String, device As String)

        'Set list of device IP and Name
        With listOfPing
            .Add(IP & "|" & device)
        End With

        Try
            'var to split IP and Device name
            Dim dev_ip As String() = Nothing
            'ping each device
            For Each ItemToPing As String In listOfPing
                'get IP (0) and name (1)
                dev_ip = Split(ItemToPing, "|")
                dataPing = New Ping
                AddHandler dataPing.PingCompleted, AddressOf PingResult
                dataPing.SendAsync(dev_ip(0), 2000, dev_ip(1))
            Next ItemToPing

            'clear ping list       
            listcount = listOfPing.Count
            listOfPing.Clear()

        Catch ex As Exception
            MsgBox("Unable to PING!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (PING)")
        End Try

    End Sub


    'Create ping list
    Private Sub PingResult(ByVal sender As Object, ByVal e As System.Net.NetworkInformation.PingCompletedEventArgs)

        'VARS
        Dim devices As String = e.UserState     'DEVICE to ping
        Dim device_list As New ListViewItem     'DEVICE's IP and TTL

        'Add device and info to list
        device_list = form_devices_list.listview_devices.Items.Add(e.Reply.Address.ToString)    'IP
        device_list.SubItems.Add(e.Reply.Status.ToString)                                       'Ping status (online / offline)
        device_list.SubItems.Add(e.Reply.RoundtripTime.ToString)                                'get TTL
        device_list.SubItems.Add(devices)                                                       'get device name

        'Set row color for Online/Offline mode
        If e.Reply.Status = IPStatus.Success Then
            device_list.BackColor = Color.Green
        Else
            device_list.BackColor = Color.Red
        End If

        'print network status
        printNetworkInfo()

    End Sub

    'PRINT PING RESULTS
    Private Sub printNetworkInfo()

        Dim device As ListView = form_devices_list.listview_devices

        With device
            Dim item As ListViewItem 'get each device

            'REG1
            item = .FindItemWithText("Reg1", True, 0, False)
            If Not item Is Nothing Then

                If item.SubItems(1).Text = "Success" Then
                    lbl_labs_cash1_load.Text = "Online"
                    lbl_labs_cash1_load.ForeColor = Color.Green
                Else
                    lbl_labs_cash1_load.Text = "Offline"
                    lbl_labs_cash1_load.ForeColor = Color.Red
                End If
            Else
                If reg1_location = Nothing Then
                    lbl_labs_cash1_load.Text = "N/A"
                    lbl_labs_cash1_load.ForeColor = Color.Blue
                End If
            End If

            'REG2
            item = .FindItemWithText("Reg2", True, 0, False)
            If Not item Is Nothing Then

                If item.SubItems(1).Text = "Success" Then
                    lbl_labs_cash2_load.Text = "Online"
                    lbl_labs_cash2_load.ForeColor = Color.Green
                Else
                    lbl_labs_cash2_load.Text = "Offline"
                    lbl_labs_cash2_load.ForeColor = Color.Red
                End If
            Else
                If reg2_location = Nothing Then
                    lbl_labs_cash2_load.Text = "N/A"
                    lbl_labs_cash2_load.ForeColor = Color.Blue
                End If
            End If

            'BO
            item = .FindItemWithText("BO", True, 0, False)
            If Not item Is Nothing Then

                If item.SubItems(1).Text = "Success" Then
                    lbl_labs_bo_load.Text = "Online"
                    lbl_labs_bo_load.ForeColor = Color.Green
                Else
                    lbl_labs_bo_load.Text = "Offline"
                    lbl_labs_bo_load.ForeColor = Color.Red
                End If
            Else
                If bo_location = Nothing Then
                    lbl_labs_bo_load.Text = "N/A"
                    lbl_labs_bo_load.ForeColor = Color.Blue
                End If
            End If

            'PINPAD1
            item = .FindItemWithText("Pinpad1", True, 0, False)
            If Not item Is Nothing Then

                If item.SubItems(1).Text = "Success" Then
                    pic_labs_pinpad_c1.Image = My.Resources.ResourceManager.GetObject("pinpad_online")
                Else
                    pic_labs_pinpad_c1.Image = My.Resources.ResourceManager.GetObject("pinpad_offline")
                End If
                'Else
                '    If pinpad1_location = Nothing Then
                '        pic_labs_pinpad_c1.Image = Nothing
                '    Else
                '        pic_labs_pinpad_c1.Image = My.Resources.ResourceManager.GetObject("pinpad_manual")
                '    End If
            End If

            'PINPAD2
            item = .FindItemWithText("Pinpad2", True, 0, False)
            If Not item Is Nothing Then

                If item.SubItems(1).Text = "Success" Then
                    pic_labs_pinpad_c2.Image = My.Resources.ResourceManager.GetObject("pinpad_online")
                Else
                    pic_labs_pinpad_c2.Image = My.Resources.ResourceManager.GetObject("pinpad_offline")
                End If
                'Else
                '    If pinpad1_location = Nothing Then
                '        pic_labs_pinpad_c2.Image = Nothing
                '    Else
                '        pic_labs_pinpad_c2.Image = My.Resources.ResourceManager.GetObject("pinpad_manual")
                '    End If
            End If

            'PRINTER1
            item = .FindItemWithText("Printer1", True, 0, False)
            If Not item Is Nothing Then

                If item.SubItems(1).Text = "Success" Then
                    pic_labs_printer_c1.Image = My.Resources.ResourceManager.GetObject("printer_online")
                Else
                    pic_labs_printer_c1.Image = My.Resources.ResourceManager.GetObject("printer_offline")
                End If
                'Else
                '    If printer1_location = Nothing Then
                '        pic_labs_printer_c1.Image = Nothing
                '    Else
                '        pic_labs_printer_c1.Image = My.Resources.ResourceManager.GetObject("printer_manual")
                '    End If
            End If

            'PRINTER2
            item = .FindItemWithText("Printer2", True, 0, False)
            If Not item Is Nothing Then

                If item.SubItems(1).Text = "Success" Then
                    pic_labs_printer_c2.Image = My.Resources.ResourceManager.GetObject("printer_online")
                Else
                    pic_labs_printer_c2.Image = My.Resources.ResourceManager.GetObject("printer_offline")
                End If
                'Else
                '    If printer1_location = Nothing Then
                '        pic_labs_printer_c2.Image = Nothing
                '    Else
                '        pic_labs_printer_c2.Image = My.Resources.ResourceManager.GetObject("printer_manual")
                '    End If
            End If

            'load lab tooltips
            loadStoreTooltips()

        End With

    End Sub

    'print info to form
    Private Sub printPatchesAndComments()

        'EXTRA PATCH LIST
        Dim patches As String() = Nothing
        If extra_patches = Nothing Then
            patches = Nothing
        Else
            patches = Split(extra_patches.ToString(), ",")
            'print patches to listview
            For Each patch As String In patches
                ListView_labs_patch.Items.Add(patch).ToString()
            Next patch

            'autosize columns & header
            ListView_labs_patch.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
            'ListView_labs_patch.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)
        End If

        'ADD COMMENTS TO TEXTBOX
        txtbox_labs_comments.Text = comments

    End Sub

    'LOAD STORE TOOLTIPS
    Private Sub loadStoreTooltips()

        'REG1 INFO
        If reg1_location = Nothing Then
            labs_tooltip.SetToolTip(lbl_labs_cash1, Nothing)
        Else
            labs_tooltip.SetToolTip(lbl_labs_cash1, "Hostname: " & reg1_hostname & vbNewLine & "Location: " & reg1_location & vbNewLine & "IP Address: " & reg1_ipaddress & vbNewLine _
                                        & "Install Date: " & reg1_install_date)
        End If
        If printer1_location = Nothing Then
            labs_tooltip.SetToolTip(pic_labs_printer_c1, Nothing)
        Else
            labs_tooltip.SetToolTip(pic_labs_printer_c1, "Location: " & printer1_location & vbNewLine & "IP Address: " & printer1_ipaddress & vbNewLine _
                                        & "Install Date: " & printer1_install_date)
        End If
        If pinpad1_location = Nothing Then
            labs_tooltip.SetToolTip(pic_labs_pinpad_c1, Nothing)
        Else
            labs_tooltip.SetToolTip(pic_labs_pinpad_c1, "Location: " & pinpad1_location & vbNewLine & "IP Address: " & pinpad1_ipaddress & vbNewLine _
                                        & "Install Date: " & pinpad1_install_date)
        End If

        'REG2 INFO
        If reg2_location = Nothing Then
            labs_tooltip.SetToolTip(lbl_labs_cash2, Nothing)
        Else
            labs_tooltip.SetToolTip(lbl_labs_cash2, "Hostname: " & reg2_hostname & vbNewLine & "Location: " & reg2_location & vbNewLine & "IP Address: " & reg2_ipaddress & vbNewLine _
                                                & "Install Date: " & reg2_install_date)
        End If
        If printer2_location = Nothing Then
            labs_tooltip.SetToolTip(pic_labs_printer_c2, Nothing)
        Else
            labs_tooltip.SetToolTip(pic_labs_printer_c2, "Location: " & printer2_location & vbNewLine & "IP Address: " & printer2_ipaddress & vbNewLine _
                                    & "Install Date: " & printer2_install_date)
        End If
        If pinpad2_location = Nothing Then
            labs_tooltip.SetToolTip(pic_labs_pinpad_c2, Nothing)
        Else
            labs_tooltip.SetToolTip(pic_labs_pinpad_c2, "Location: " & pinpad2_location & vbNewLine & "IP Address: " & pinpad2_ipaddress & vbNewLine _
                                                & "Install Date: " & pinpad2_install_date)
        End If

        'BO INFO
        If bo_location = Nothing Then
            labs_tooltip.SetToolTip(lbl_labs_bo, Nothing)
        Else
            labs_tooltip.SetToolTip(lbl_labs_bo, "Hostname: " & bo_hostname & vbNewLine & "Location: " & bo_location & vbNewLine & "IP Address: " & bo_ipaddress & vbNewLine _
                                            & "Install Date: " & bo_install_date)
        End If

    End Sub

    'LOAD FORM TOOLTIPS
    Private Sub loadFormTooltips()

        'LOAD FORM TOOLTIPS
        If listview_labs_stores.Items.Count = 0 Then
            labs_tooltip.SetToolTip(listview_labs_stores, Nothing)

        Else
            labs_tooltip.SetToolTip(listview_labs_stores, "Double-click on a LAB to see it in details.")
            labs_tooltip.SetToolTip(btn_labs_export, "Export LAB list as .CSV file.")
            labs_tooltip.SetToolTip(btn_labs_reload, "Refresh LAB list")
        End If

        'SET PATCHES TOOLTIP
        labs_tooltip.SetToolTip(ListView_labs_patch, "Double-click on a patch to open it in JIRA.")

    End Sub

    'LOAD PATCH IN JIRA (DOUBLE CLICK)
    Private Sub ListView_labs_patch_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles ListView_labs_patch.MouseDoubleClick

        Process.Start(gdx_main.set_browser, "https://gdynamite.atlassian.net/browse/" & ListView_labs_patch.SelectedItems(0).SubItems(0).Text)

    End Sub

    'ON CLOSING (close app)
    Private Sub Labs_closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        'Me.Dispose()
        gdx_main.Dispose()

    End Sub

    'SORT LAB LIST
    Private Sub listview_labs_stores_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles listview_labs_stores.ColumnClick

        If listview_labs_stores.Sorting = SortOrder.Ascending Then
            listview_labs_stores.Sorting = SortOrder.Descending
        Else
            listview_labs_stores.Sorting = SortOrder.Ascending
        End If
        'End If

        'Set the ListviewItemSorter property to a new ListviewItemComparer object
        Me.listview_labs_stores.ListViewItemSorter = New admin_users_compare(e.Column, listview_labs_stores.Sorting)

        ' Call the sort method to manually sort
        listview_labs_stores.Sort()

    End Sub

    'SET PING REFRESH TIMER
    Private Sub ping_timer_labs_Tick(sender As Object, e As EventArgs) Handles ping_timer_labs.Tick

        If lab_loaded = False Then
            'if store not loaded do nothing
        Else
            'btn_search_store.PerformClick()  
            form_devices_list.listview_devices.Items.Clear()
            pingLAB()
        End If

    End Sub

    'Open C: drive
    Private Sub openDrive(ipAddress As String)

        Dim username, username_newimage, password As String
        Dim result As Boolean = False

        If lbl_labs_state_load.Text.Contains("QC") Or lbl_labs_state_load.Text.Contains("Quebec") Then
            username = "Administrateur"     'get username for french stores
        Else
            username = "Administrator"      'get username for enghish stores
        End If

        username_newimage = "pos0"          'username for new image
        password = "S3cure!!733"            'get password

        result = gdx_main.Run_Process("cmd.exe", " /c net use \\" & ipAddress.ToString & "\c$ /USER:" & ipAddress.ToString & "\" & username.ToString & " " & password.ToString, , True)

        'test connection to drive is TRUE
        'IF CONNECTION TRUE connect to drive
        If result = True Then
            gdx_main.Run_Process("explorer.exe", "\\" & ipAddress & "\c$")
            'ELSE try connecting with POS0 account for new images
        Else
            result = gdx_main.Run_Process("cmd.exe", " /c net use \\" & ipAddress.ToString & "\c$ /USER:" & ipAddress.ToString & "\" & username_newimage.ToString & " " & password.ToString, , True)
            'IF CONNECTION TRUE connect to drive
            If result = True Then
                gdx_main.Run_Process("explorer.exe", "\\" & ipAddress & "\c$")
                'ELSE show error msg
            Else
                MsgBox("Unable to connect to " & ipAddress & " !", MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (mapping)")
            End If

        End If
        ipAddress = ""

    End Sub

End Class