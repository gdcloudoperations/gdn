﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class admin_pwd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(admin_pwd))
        Me.lbl_admin_pwd_txt = New System.Windows.Forms.Label()
        Me.lbl_admin_pwd_txt2 = New System.Windows.Forms.Label()
        Me.lbl_admin_pwd_usr = New System.Windows.Forms.Label()
        Me.lbl_admin_pwd_pwd = New System.Windows.Forms.Label()
        Me.txtbox_admin_usr = New System.Windows.Forms.TextBox()
        Me.txtbox_admin_pwd = New System.Windows.Forms.TextBox()
        Me.btn_admin_pwd_ok = New System.Windows.Forms.Button()
        Me.btn_admin_pwd_exit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lbl_admin_pwd_txt
        '
        Me.lbl_admin_pwd_txt.AutoSize = True
        Me.lbl_admin_pwd_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_pwd_txt.Location = New System.Drawing.Point(12, 9)
        Me.lbl_admin_pwd_txt.Name = "lbl_admin_pwd_txt"
        Me.lbl_admin_pwd_txt.Size = New System.Drawing.Size(211, 15)
        Me.lbl_admin_pwd_txt.TabIndex = 0
        Me.lbl_admin_pwd_txt.Text = "Please enter Admin credentials."
        '
        'lbl_admin_pwd_txt2
        '
        Me.lbl_admin_pwd_txt2.AutoSize = True
        Me.lbl_admin_pwd_txt2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_pwd_txt2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lbl_admin_pwd_txt2.Location = New System.Drawing.Point(12, 120)
        Me.lbl_admin_pwd_txt2.Name = "lbl_admin_pwd_txt2"
        Me.lbl_admin_pwd_txt2.Size = New System.Drawing.Size(277, 15)
        Me.lbl_admin_pwd_txt2.TabIndex = 1
        Me.lbl_admin_pwd_txt2.Text = "If you are not an Administrator please exit."
        '
        'lbl_admin_pwd_usr
        '
        Me.lbl_admin_pwd_usr.AutoSize = True
        Me.lbl_admin_pwd_usr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_pwd_usr.Location = New System.Drawing.Point(27, 42)
        Me.lbl_admin_pwd_usr.Name = "lbl_admin_pwd_usr"
        Me.lbl_admin_pwd_usr.Size = New System.Drawing.Size(67, 13)
        Me.lbl_admin_pwd_usr.TabIndex = 2
        Me.lbl_admin_pwd_usr.Text = "Username:"
        '
        'lbl_admin_pwd_pwd
        '
        Me.lbl_admin_pwd_pwd.AutoSize = True
        Me.lbl_admin_pwd_pwd.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_admin_pwd_pwd.Location = New System.Drawing.Point(27, 70)
        Me.lbl_admin_pwd_pwd.Name = "lbl_admin_pwd_pwd"
        Me.lbl_admin_pwd_pwd.Size = New System.Drawing.Size(65, 13)
        Me.lbl_admin_pwd_pwd.TabIndex = 3
        Me.lbl_admin_pwd_pwd.Text = "Password:"
        '
        'txtbox_admin_usr
        '
        Me.txtbox_admin_usr.Location = New System.Drawing.Point(100, 39)
        Me.txtbox_admin_usr.Name = "txtbox_admin_usr"
        Me.txtbox_admin_usr.Size = New System.Drawing.Size(123, 20)
        Me.txtbox_admin_usr.TabIndex = 4
        '
        'txtbox_admin_pwd
        '
        Me.txtbox_admin_pwd.Location = New System.Drawing.Point(100, 67)
        Me.txtbox_admin_pwd.Name = "txtbox_admin_pwd"
        Me.txtbox_admin_pwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtbox_admin_pwd.Size = New System.Drawing.Size(123, 20)
        Me.txtbox_admin_pwd.TabIndex = 5
        '
        'btn_admin_pwd_ok
        '
        Me.btn_admin_pwd_ok.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_admin_pwd_ok.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_admin_pwd_ok.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_admin_pwd_ok.Location = New System.Drawing.Point(149, 93)
        Me.btn_admin_pwd_ok.Name = "btn_admin_pwd_ok"
        Me.btn_admin_pwd_ok.Size = New System.Drawing.Size(67, 24)
        Me.btn_admin_pwd_ok.TabIndex = 14
        Me.btn_admin_pwd_ok.Text = "Login"
        Me.btn_admin_pwd_ok.UseVisualStyleBackColor = False
        '
        'btn_admin_pwd_exit
        '
        Me.btn_admin_pwd_exit.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_admin_pwd_exit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_admin_pwd_exit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_admin_pwd_exit.Location = New System.Drawing.Point(222, 93)
        Me.btn_admin_pwd_exit.Name = "btn_admin_pwd_exit"
        Me.btn_admin_pwd_exit.Size = New System.Drawing.Size(67, 24)
        Me.btn_admin_pwd_exit.TabIndex = 15
        Me.btn_admin_pwd_exit.Text = "Exit"
        Me.btn_admin_pwd_exit.UseVisualStyleBackColor = False
        '
        'admin_pwd
        '
        Me.AcceptButton = Me.btn_admin_pwd_ok
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(301, 142)
        Me.Controls.Add(Me.btn_admin_pwd_exit)
        Me.Controls.Add(Me.btn_admin_pwd_ok)
        Me.Controls.Add(Me.txtbox_admin_pwd)
        Me.Controls.Add(Me.txtbox_admin_usr)
        Me.Controls.Add(Me.lbl_admin_pwd_pwd)
        Me.Controls.Add(Me.lbl_admin_pwd_usr)
        Me.Controls.Add(Me.lbl_admin_pwd_txt2)
        Me.Controls.Add(Me.lbl_admin_pwd_txt)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "admin_pwd"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Administrator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_admin_pwd_txt As Label
    Friend WithEvents lbl_admin_pwd_txt2 As Label
    Friend WithEvents lbl_admin_pwd_usr As Label
    Friend WithEvents lbl_admin_pwd_pwd As Label
    Friend WithEvents txtbox_admin_usr As TextBox
    Friend WithEvents txtbox_admin_pwd As TextBox
    Friend WithEvents btn_admin_pwd_ok As Button
    Friend WithEvents btn_admin_pwd_exit As Button
End Class
