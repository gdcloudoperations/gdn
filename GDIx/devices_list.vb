﻿'AUTHOR: Alex Dumitrascu
'DATE: 18-12-2017
'UPDATE: 25-02-2018
'
'FORM: devices_list.vb (access from MAIN)


Imports System.IO

Public Class form_devices_list

    'List View
    Private Sub devices_list_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Add list columns
        listview_devices.View = System.Windows.Forms.View.Details
        listview_devices.Columns.Add("IP", 100, HorizontalAlignment.Left)
        listview_devices.Columns.Add("Status", 100, HorizontalAlignment.Left)
        listview_devices.Columns.Add("TTL", 100, HorizontalAlignment.Left)
        listview_devices.Columns.Add("Device", 100, HorizontalAlignment.Left)



    End Sub

    Private Sub ListView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listview_devices.SelectedIndexChanged

    End Sub

    'Export list to file
    Private Sub btn_dev_list_export_Click(sender As Object, e As EventArgs) Handles btn_dev_list_export.Click


        Dim sfd As New SaveFileDialog With {
        .Title = "Select file location",
        .FileName = "GDnReports_PingDevicesList_Store" & gdx_main.lbl_strno_load.Text & ".csv",
        .Filter = "CSV (*.csv)|*.csv",
        .FilterIndex = 0,
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}

        'save CSV fIle 

        If sfd.ShowDialog = DialogResult.OK Then

            Dim headers = (From ch In listview_devices.Columns Let header = DirectCast(ch, ColumnHeader) Select header.Text).ToArray()

            Dim items() = (From item In listview_devices.Items Let lvi = DirectCast(item, ListViewItem) Select (From subitem In lvi.SubItems
                                                                                                                Let si = DirectCast(subitem, ListViewItem.ListViewSubItem)
                                                                                                                Select si.Text).ToArray()).ToArray()

            Dim table As String = String.Join(",", headers) & Environment.NewLine

            For Each a In items
                table &= String.Join(",", a) & Environment.NewLine
            Next

            table = table.TrimEnd(CChar(vbCr), CChar(vbLf))
            IO.File.WriteAllText(sfd.FileName, table)

        End If

    End Sub

End Class