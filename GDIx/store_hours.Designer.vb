﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_store_hours
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_store_hours))
        Me.lbl_store_hours_store = New System.Windows.Forms.Label()
        Me.lbl_store_hours_store_load = New System.Windows.Forms.Label()
        Me.lbl_monday = New System.Windows.Forms.Label()
        Me.grpbox_store_hours = New System.Windows.Forms.GroupBox()
        Me.lbl_sun_load = New System.Windows.Forms.Label()
        Me.lbl_sat_load = New System.Windows.Forms.Label()
        Me.lbl_fri_load = New System.Windows.Forms.Label()
        Me.lbl_thu_load = New System.Windows.Forms.Label()
        Me.lbl_wed_load = New System.Windows.Forms.Label()
        Me.lbl_tue_load = New System.Windows.Forms.Label()
        Me.lbl_mon_load = New System.Windows.Forms.Label()
        Me.lbl_sunday = New System.Windows.Forms.Label()
        Me.lbl_saturday = New System.Windows.Forms.Label()
        Me.lbl_friday = New System.Windows.Forms.Label()
        Me.lbl_thursday = New System.Windows.Forms.Label()
        Me.lbl_wednesday = New System.Windows.Forms.Label()
        Me.lbl_tuesday = New System.Windows.Forms.Label()
        Me.grpbox_store_hours.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_store_hours_store
        '
        Me.lbl_store_hours_store.AutoSize = True
        Me.lbl_store_hours_store.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_store_hours_store.Location = New System.Drawing.Point(47, 13)
        Me.lbl_store_hours_store.Name = "lbl_store_hours_store"
        Me.lbl_store_hours_store.Size = New System.Drawing.Size(67, 15)
        Me.lbl_store_hours_store.TabIndex = 16
        Me.lbl_store_hours_store.Text = "Store No:"
        '
        'lbl_store_hours_store_load
        '
        Me.lbl_store_hours_store_load.AutoSize = True
        Me.lbl_store_hours_store_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_store_hours_store_load.Location = New System.Drawing.Point(120, 13)
        Me.lbl_store_hours_store_load.Name = "lbl_store_hours_store_load"
        Me.lbl_store_hours_store_load.Size = New System.Drawing.Size(60, 16)
        Me.lbl_store_hours_store_load.TabIndex = 17
        Me.lbl_store_hours_store_load.Text = "store_no"
        '
        'lbl_monday
        '
        Me.lbl_monday.AutoSize = True
        Me.lbl_monday.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_monday.Location = New System.Drawing.Point(24, 30)
        Me.lbl_monday.Name = "lbl_monday"
        Me.lbl_monday.Size = New System.Drawing.Size(57, 15)
        Me.lbl_monday.TabIndex = 18
        Me.lbl_monday.Text = "Monday"
        '
        'grpbox_store_hours
        '
        Me.grpbox_store_hours.Controls.Add(Me.lbl_sun_load)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_sat_load)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_fri_load)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_thu_load)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_wed_load)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_tue_load)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_mon_load)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_sunday)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_saturday)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_friday)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_thursday)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_wednesday)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_tuesday)
        Me.grpbox_store_hours.Controls.Add(Me.lbl_monday)
        Me.grpbox_store_hours.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_store_hours.Location = New System.Drawing.Point(12, 32)
        Me.grpbox_store_hours.Name = "grpbox_store_hours"
        Me.grpbox_store_hours.Size = New System.Drawing.Size(305, 209)
        Me.grpbox_store_hours.TabIndex = 19
        Me.grpbox_store_hours.TabStop = False
        Me.grpbox_store_hours.Text = "Store Hours"
        '
        'lbl_sun_load
        '
        Me.lbl_sun_load.AutoSize = True
        Me.lbl_sun_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sun_load.Location = New System.Drawing.Point(145, 176)
        Me.lbl_sun_load.Name = "lbl_sun_load"
        Me.lbl_sun_load.Size = New System.Drawing.Size(48, 13)
        Me.lbl_sun_load.TabIndex = 31
        Me.lbl_sun_load.Text = "AM - PM"
        '
        'lbl_sat_load
        '
        Me.lbl_sat_load.AutoSize = True
        Me.lbl_sat_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sat_load.Location = New System.Drawing.Point(145, 152)
        Me.lbl_sat_load.Name = "lbl_sat_load"
        Me.lbl_sat_load.Size = New System.Drawing.Size(48, 13)
        Me.lbl_sat_load.TabIndex = 30
        Me.lbl_sat_load.Text = "AM - PM"
        '
        'lbl_fri_load
        '
        Me.lbl_fri_load.AutoSize = True
        Me.lbl_fri_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fri_load.Location = New System.Drawing.Point(145, 128)
        Me.lbl_fri_load.Name = "lbl_fri_load"
        Me.lbl_fri_load.Size = New System.Drawing.Size(48, 13)
        Me.lbl_fri_load.TabIndex = 29
        Me.lbl_fri_load.Text = "AM - PM"
        '
        'lbl_thu_load
        '
        Me.lbl_thu_load.AutoSize = True
        Me.lbl_thu_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_thu_load.Location = New System.Drawing.Point(145, 104)
        Me.lbl_thu_load.Name = "lbl_thu_load"
        Me.lbl_thu_load.Size = New System.Drawing.Size(48, 13)
        Me.lbl_thu_load.TabIndex = 28
        Me.lbl_thu_load.Text = "AM - PM"
        '
        'lbl_wed_load
        '
        Me.lbl_wed_load.AutoSize = True
        Me.lbl_wed_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_wed_load.Location = New System.Drawing.Point(145, 80)
        Me.lbl_wed_load.Name = "lbl_wed_load"
        Me.lbl_wed_load.Size = New System.Drawing.Size(48, 13)
        Me.lbl_wed_load.TabIndex = 27
        Me.lbl_wed_load.Text = "AM - PM"
        '
        'lbl_tue_load
        '
        Me.lbl_tue_load.AutoSize = True
        Me.lbl_tue_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_tue_load.Location = New System.Drawing.Point(145, 56)
        Me.lbl_tue_load.Name = "lbl_tue_load"
        Me.lbl_tue_load.Size = New System.Drawing.Size(48, 13)
        Me.lbl_tue_load.TabIndex = 26
        Me.lbl_tue_load.Text = "AM - PM"
        '
        'lbl_mon_load
        '
        Me.lbl_mon_load.AutoSize = True
        Me.lbl_mon_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_mon_load.Location = New System.Drawing.Point(145, 32)
        Me.lbl_mon_load.Name = "lbl_mon_load"
        Me.lbl_mon_load.Size = New System.Drawing.Size(48, 13)
        Me.lbl_mon_load.TabIndex = 25
        Me.lbl_mon_load.Text = "AM - PM"
        '
        'lbl_sunday
        '
        Me.lbl_sunday.AutoSize = True
        Me.lbl_sunday.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sunday.Location = New System.Drawing.Point(24, 174)
        Me.lbl_sunday.Name = "lbl_sunday"
        Me.lbl_sunday.Size = New System.Drawing.Size(54, 15)
        Me.lbl_sunday.TabIndex = 24
        Me.lbl_sunday.Text = "Sunday"
        '
        'lbl_saturday
        '
        Me.lbl_saturday.AutoSize = True
        Me.lbl_saturday.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_saturday.Location = New System.Drawing.Point(24, 150)
        Me.lbl_saturday.Name = "lbl_saturday"
        Me.lbl_saturday.Size = New System.Drawing.Size(63, 15)
        Me.lbl_saturday.TabIndex = 23
        Me.lbl_saturday.Text = "Saturday"
        '
        'lbl_friday
        '
        Me.lbl_friday.AutoSize = True
        Me.lbl_friday.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_friday.Location = New System.Drawing.Point(24, 126)
        Me.lbl_friday.Name = "lbl_friday"
        Me.lbl_friday.Size = New System.Drawing.Size(46, 15)
        Me.lbl_friday.TabIndex = 22
        Me.lbl_friday.Text = "Friday"
        '
        'lbl_thursday
        '
        Me.lbl_thursday.AutoSize = True
        Me.lbl_thursday.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_thursday.Location = New System.Drawing.Point(24, 102)
        Me.lbl_thursday.Name = "lbl_thursday"
        Me.lbl_thursday.Size = New System.Drawing.Size(65, 15)
        Me.lbl_thursday.TabIndex = 21
        Me.lbl_thursday.Text = "Thursday"
        '
        'lbl_wednesday
        '
        Me.lbl_wednesday.AutoSize = True
        Me.lbl_wednesday.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_wednesday.Location = New System.Drawing.Point(24, 78)
        Me.lbl_wednesday.Name = "lbl_wednesday"
        Me.lbl_wednesday.Size = New System.Drawing.Size(80, 15)
        Me.lbl_wednesday.TabIndex = 20
        Me.lbl_wednesday.Text = "Wednesday"
        '
        'lbl_tuesday
        '
        Me.lbl_tuesday.AutoSize = True
        Me.lbl_tuesday.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_tuesday.Location = New System.Drawing.Point(24, 54)
        Me.lbl_tuesday.Name = "lbl_tuesday"
        Me.lbl_tuesday.Size = New System.Drawing.Size(60, 15)
        Me.lbl_tuesday.TabIndex = 19
        Me.lbl_tuesday.Text = "Tuesday"
        '
        'form_store_hours
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(332, 255)
        Me.Controls.Add(Me.grpbox_store_hours)
        Me.Controls.Add(Me.lbl_store_hours_store_load)
        Me.Controls.Add(Me.lbl_store_hours_store)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_store_hours"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Store Hours"
        Me.TopMost = True
        Me.grpbox_store_hours.ResumeLayout(False)
        Me.grpbox_store_hours.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lbl_store_hours_store As Label
    Friend WithEvents lbl_store_hours_store_load As Label
    Friend WithEvents lbl_monday As Label
    Friend WithEvents grpbox_store_hours As GroupBox
    Friend WithEvents lbl_tuesday As Label
    Friend WithEvents lbl_thursday As Label
    Friend WithEvents lbl_wednesday As Label
    Friend WithEvents lbl_friday As Label
    Friend WithEvents lbl_saturday As Label
    Friend WithEvents lbl_sunday As Label
    Friend WithEvents lbl_mon_load As Label
    Friend WithEvents lbl_tue_load As Label
    Friend WithEvents lbl_wed_load As Label
    Friend WithEvents lbl_thu_load As Label
    Friend WithEvents lbl_fri_load As Label
    Friend WithEvents lbl_sat_load As Label
    Friend WithEvents lbl_sun_load As Label
End Class
