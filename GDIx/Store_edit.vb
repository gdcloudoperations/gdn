﻿'AUTHOR: Alex Dumitrascu
'DATE: 05-02-2018
'UPDATE: 02-03-2018
'
'FORM: Store_edit.vb (access from MAIN)

Imports System.Text.RegularExpressions



Public Class Store_edit

    Private regEx As Regex                       'for regular exp
    Private tooltip = New ToolTip()              'used for loading tooltips

    Private Sub Store_edit_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        Dim error_fields As String = Nothing

        'LOAD TOOLTIPS
        loadToolTips(lbl_es_storeno, "", "ns_store")        'store number
        loadToolTips(lbl_es_str_banner, "", "ns_banner")    'banner
        loadToolTips(lbl_es_store_system, "", "ns_ssystem")  'store system
        loadToolTips(lbll_es_str_sisstr, "", "ns_sstore")   'sister store number
        loadToolTips(lbl_es_str_mall, "", "ns_mall")        'mall
        loadToolTips(lbl_es_str_add, "", "ns_address")      'address
        loadToolTips(lbl_es_str_city, "", "ns_city")        'city
        loadToolTips(lbl_es_str_prov, "", "ns_state")       'state/province
        loadToolTips(lbl_es_str_pcode, "", "ns_pcode")      'postal code
        loadToolTips(lbl_es_str_cntry, "", "ns_countrye")   'Country
        loadToolTips(lbl_es_str_ph, "", "ns_mline")         'main line
        loadToolTips(lbl_es_str_boline, "", "ns_boline")    'bo line
        loadToolTips(lbl_es_str_faxline, "", "ns_faxline")  'fax line
        loadToolTips(lbl_es_str_dslline, "", "ns_dsl")      'internet line
        loadToolTips(lbl_es_ip, "", "ns_ip")                'ip 3 octets
        loadToolTips(lbl_es_isp, "", "ns_isp")              'ISP
        loadToolTips(lbl_es_ispaccount, "", "ns_ispaccount") 'ISP Account
        loadToolTips(lbl_es_ispstatus, "", "ns_ispstatus")   'ISP Address Asign
        loadToolTips(lbl_es_ispaddress, "", "ns_ispaddress") 'ISP IP Address
        loadToolTips(lbl_es_ispsubnet, "", "ns_ispsubnet")   'ISP Subnet
        loadToolTips(lbl_es_ispgateway, "", "ns_ispgateway") 'ISP Default Gateway
        loadToolTips(lbl_es_username, "", "ns_user")        'Username
        loadToolTips(lbl_es_password, "", "ns_pwd")         'Password
        loadToolTips(lbl_es_speed, "", "ns_speed")          'store speed
        loadToolTips(lbl_es_wifispeed, "", "ns_wifi")       'WiFi speed
        loadToolTips(lbl_rtr_location, "", "ns_rlocation")  'Router location
        loadToolTips(lbl_es_cabinet_lock, "", "ns_cabinet") 'it cabinet lock
        loadToolTips(lbl_es_iscombo, "", "ns_combo")        'is combo
        loadToolTips(lbl_es_pinpads, "", "ns_pinpads")       'pinpads connection
        loadToolTips(lbl_es_isremote, "", "ns_remote")      'is remote
        loadToolTips(lbl_es_status, "", "ns_status")        'store status
        loadToolTips(lbl_es_location, "", "ns_location")    'store location


        'remove * from mandatory fields (case fields only)
        ns_star18.Text = ""     'password field
        ns_star4.Text = ""      'Sister store (for combo)
        ns_star17.Text = ""     'ISP username
        ns_star18.Text = ""     'ISP password
        ns_star14.Text = ""     'ISP Static IP
        ns_star15.Text = ""     'ISP Static Subnet
        ns_star16.Text = ""     'ISP Static Gateway

        'disable store no edit
        txtbox_es_storeno_load.Enabled = False

        'PRELOAD SELECTED STORE INFO
        txtbox_es_storeno_load.Text = gdx_main.lbl_strno_load.Text
        cmbbox_es_banner_load.SelectedItem = gdx_main.lbl_ban_load.Text
        cmbbox_es_store_system.SelectedItem = gdx_main.lbl_str_system_load.Text
        If gdx_main.pinpadCon.Contains("COM2") Then
            cmbbox_es_pinpad_load.SelectedItem = "COM2"
        ElseIf gdx_main.pinpadCon.Contains("overIP") Then
            cmbbox_es_pinpad_load.SelectedItem = "overIP"
        Else
            cmbbox_es_pinpad_load.SelectedIndex = -1
        End If

        If gdx_main.lbl_sis_store_load.Text = "N/A" Then
            txtbox_es_sisstore_load.Clear()
        Else
            txtbox_es_sisstore_load.Text = gdx_main.lbl_sis_store_load.Text
        End If
        txtbox_es_mall_load.Text = gdx_main.lbl_mall_load.Text
        txtbox_es_address_load.Text = gdx_main.lbl_address_load.Text
        txtbox_es_city_load.Text = gdx_main.lbl_city_load.Text
        txtbox_es_pcode_load.Text = gdx_main.lbl_postal_load.Text
        cmbbox_es_country_load.SelectedItem = gdx_main.lbl_country_load.Text
        cmbbox_es_province_load.SelectedItem = gdx_main.lbl_state_load.Text

        Dim line_patern As String = "[^\d]"

        If gdx_main.lbl_mainl_load.Text = "N/A" Then
            txtbox_es_mainline_load.Clear()
        Else
            txtbox_es_mainline_load.Text = Regex.Replace(gdx_main.lbl_mainl_load.Text, line_patern, "")
        End If
        If gdx_main.boLine = "N/A" Then
            txtbox_es_boline_load.Clear()
        Else
            txtbox_es_boline_load.Text = Regex.Replace(gdx_main.boLine, line_patern, "")
        End If
        If gdx_main.faxLine = "N/A" Then
            txtbox_es_faxline_load.Clear()
        Else
            txtbox_es_faxline_load.Text = Regex.Replace(gdx_main.faxLine, line_patern, "")
        End If
        If gdx_main.dslLine = "N/A" Then
            txtbox_es_dslline_load.Clear()
        Else
            txtbox_es_dslline_load.Text = Regex.Replace(gdx_main.dslLine, line_patern, "")
        End If

        txtbox_es_ip_load.Text = gdx_main.IPAddress

        If gdx_main.ispName = "N/A" Then
            txtbox_es_isp_load.Clear()
        Else
            txtbox_es_isp_load.Text = gdx_main.ispName
        End If
        If gdx_main.ispAccount = "N/A" Then
            txtbox_es_ispaccount_load.Clear()
        Else
            txtbox_es_ispaccount_load.Text = gdx_main.ispAccount
        End If
        If gdx_main.ispStatus = "N/A" Then
            cmbbox_es_ispstatus_load.SelectedIndex = -1
        Else
            cmbbox_es_ispstatus_load.Text = gdx_main.ispStatus
        End If
        If gdx_main.ispAddress = "N/A" Then
            txtbox_es_ispaddress_load.Clear()
        Else
            txtbox_es_ispaddress_load.Text = gdx_main.ispAddress
        End If
        If gdx_main.ispSubnet = "N/A" Then
            txtbox_es_ispsubnet_load.Clear()
        Else
            txtbox_es_ispsubnet_load.Text = gdx_main.ispSubnet
        End If
        If gdx_main.ispGateway = "N/A" Then
            txtbox_es_ispgateway_load.Clear()
        Else
            txtbox_es_ispgateway_load.Text = gdx_main.ispGateway
        End If
        If gdx_main.ispEmail = "N/A" Then
            txtbox_es_user_load.Clear()
        Else
            txtbox_es_user_load.Text = gdx_main.ispEmail
        End If
        If gdx_main.ispPwd = "N/A" Then
            txtbox_es_pwd_load.Clear()
        Else
            txtbox_es_pwd_load.Text = gdx_main.ispPwd
        End If
        If gdx_main.lbl_store_speed_load.Text = "N/A" Then
            txtbox_es_speed_load.Clear()
        Else
            txtbox_es_speed_load.Text = gdx_main.lbl_store_speed_load.Text
        End If
        If gdx_main.lbl_wifi_load.Text = "N/A" Then
            txtbox_es_wifi_load.Clear()
        Else
            txtbox_es_wifi_load.Text = gdx_main.lbl_wifi_load.Text
        End If
        If gdx_main.routerLocation = "N/A" Then
            cmbbox_es_rlocation_load.SelectedText = ""
        Else
            cmbbox_es_rlocation_load.SelectedItem = gdx_main.routerLocation
        End If
        If gdx_main.itCabinetLock = "N/A" Then
            txtbox_es_cablock_load.Clear()
        Else
            txtbox_es_cablock_load.Text = gdx_main.itCabinetLock
        End If


        cmbbox_es_smodem_load.SelectedItem = gdx_main.spModem
        cmbbox_es_srouter_load.SelectedItem = gdx_main.spRouter
        cmbbox_es_swc_load.SelectedItem = gdx_main.spWC
        cmbbox_es_hpc_load.SelectedItem = gdx_main.spHC
        cmbbox_es_sppad_load.SelectedItem = gdx_main.spPinpad
        cmbbox_es_isremote_load.SelectedItem = gdx_main.lbl_remote_load.Text
        If gdx_main.isCombo = True Then
            cmbbox_es_iscombo_load.SelectedItem = "YES"
        Else
            cmbbox_es_iscombo_load.SelectedItem = "NO"
        End If
        If gdx_main.storeStatus = True Then
            cmbbox_es_status_load.SelectedItem = "OPEN"
        Else
            cmbbox_es_status_load.SelectedItem = "CLOSED"
        End If
        If gdx_main.isTemp = True Then
            cmbbox_es_location_load.SelectedItem = "TEMP"
        Else
            cmbbox_es_location_load.SelectedItem = "MAIN"
        End If



    End Sub

    Private Sub btn_es_save_Click(sender As Object, e As EventArgs) Handles btn_es_save.Click

        Dim error_fields As String = Nothing
        Dim sstore_str As String = Nothing
        Dim storeno_int As Integer = Nothing

        'STORE NO
        If txtbox_es_storeno_load.Text = "" Then
            error_fields = "Missing store number!" & vbNewLine
            txtbox_es_storeno_load.Clear()
            txtbox_es_storeno_load.BackColor = Color.DarkSalmon
        ElseIf Not IsNumeric(txtbox_es_storeno_load.Text) Then
            error_fields = "Invalid store #. Digits only!" & vbNewLine
            txtbox_es_storeno_load.Clear()
            txtbox_es_storeno_load.BackColor = Color.DarkSalmon
        Else
            error_fields = ""
            txtbox_es_storeno_load.BackColor = Color.White
            storeno_int = Integer.Parse(txtbox_es_storeno_load.Text)
        End If

        'BANNER
        If cmbbox_es_banner_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing store banner!" & vbNewLine
            cmbbox_es_banner_load.ResetText()
            cmbbox_es_banner_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_banner_load.BackColor = Color.White
        End If

        'storeSystem
        If cmbbox_es_store_system.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing storeSystem!" & vbNewLine
            cmbbox_es_store_system.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_store_system.BackColor = Color.White
        End If

        'SISTER STORE (if combo sis store mandatory)
        If cmbbox_es_iscombo_load.SelectedItem = "YES" Then
            If txtbox_es_sisstore_load.Text = Nothing Then
                error_fields = error_fields & "Combo store selected. Must have a sister store!" & vbNewLine
                txtbox_es_sisstore_load.BackColor = Color.DarkSalmon
                txtbox_es_sisstore_load.Clear()
            ElseIf Not IsNumeric(txtbox_es_sisstore_load.Text) Then
                error_fields = error_fields & "Invalid sister store #. Digits only!" & vbNewLine
                txtbox_es_sisstore_load.BackColor = Color.DarkSalmon
                txtbox_es_sisstore_load.Clear()
            ElseIf txtbox_es_storeno_load.Text = txtbox_es_sisstore_load.Text Then
                error_fields = error_fields & "Invalid sister store #. Cannot match store number!" & vbNewLine
                txtbox_es_sisstore_load.Clear()
                txtbox_es_sisstore_load.BackColor = Color.DarkSalmon
            Else
                txtbox_es_sisstore_load.BackColor = Color.White
                sstore_str = txtbox_es_sisstore_load.Text
            End If

        ElseIf cmbbox_es_iscombo_load.SelectedItem = "NO" Then
            If txtbox_es_sisstore_load.Text = Nothing Then
                error_fields = error_fields & ""
                txtbox_es_sisstore_load.BackColor = Color.White
            ElseIf Not IsNumeric(txtbox_es_sisstore_load.Text) Then
                error_fields = error_fields & "Invalid sister store #. Digits only!" & vbNewLine
                txtbox_es_sisstore_load.BackColor = Color.DarkSalmon
                txtbox_es_sisstore_load.Clear()
            ElseIf txtbox_es_storeno_load.Text = txtbox_es_sisstore_load.Text Then
                error_fields = error_fields & "Invalid sister store #. Cannot match store number!" & vbNewLine
                txtbox_es_sisstore_load.Clear()
                txtbox_es_sisstore_load.BackColor = Color.DarkSalmon
            Else
                'sstore_str = Integer.Parse(txtbox_es_sisstore_load.Text)
                sstore_str = txtbox_es_sisstore_load.Text
                txtbox_es_sisstore_load.BackColor = Color.White
            End If

        End If


        'MALL
        If txtbox_es_mall_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: MALL" & vbNewLine
            txtbox_es_mall_load.Clear()
            txtbox_es_mall_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_es_mall_load.BackColor = Color.White
        End If

        'ADDRESS
        If txtbox_es_address_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: ADDRESS" & vbNewLine
            txtbox_es_address_load.Clear()
            txtbox_es_address_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_es_address_load.BackColor = Color.White
        End If

        'CITY
        If txtbox_es_city_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: CITY" & vbNewLine
            txtbox_es_city_load.Clear()
            txtbox_es_city_load.BackColor = Color.DarkSalmon
        ElseIf Not IsNumeric(txtbox_es_city_load.Text) Then
            error_fields = error_fields & ""
            txtbox_es_city_load.BackColor = Color.White
        Else
            error_fields = error_fields & "Invalid city. Characters only!" & vbNewLine
            txtbox_es_city_load.Clear()
            txtbox_es_city_load.BackColor = Color.DarkSalmon
        End If

        'STATE
        If cmbbox_es_province_load.SelectedItem = "" Then
            error_fields = error_fields & "Incomplete address: PROVINCE/STATE" & vbNewLine
            cmbbox_es_province_load.ResetText()
            cmbbox_es_province_load.BackColor = Color.DarkSalmon
        ElseIf IsNumeric(cmbbox_es_province_load.SelectedItem) Then
            error_fields = error_fields & "Invalid Province/State. Digits only!" & vbNewLine
            cmbbox_es_province_load.ResetText()
            cmbbox_es_province_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_province_load.BackColor = Color.White
        End If

        'POSTAL CODE        
        Dim pcode_patern_ca As String = "[A-Za-z]\d[A-Za-z] \d[A-Za-z]\d"
        Dim pcode_patern_us As String = "^\d{5}$"

        If txtbox_es_pcode_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: POSTAL CODE" & vbNewLine
            txtbox_es_pcode_load.Clear()
            txtbox_es_pcode_load.BackColor = Color.DarkSalmon

            'CHECK CANADIAN POSTAL CODES
        ElseIf cmbbox_es_country_load.SelectedItem = "Canada" Then
            If Regex.IsMatch(txtbox_es_pcode_load.Text, pcode_patern_ca) = False Then
                error_fields = error_fields & "Invalid Postal Code. Canadian postal code only!" & vbNewLine
                txtbox_es_pcode_load.Clear()
                txtbox_es_pcode_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_pcode_load.BackColor = Color.White
            End If

            'CHECK US POSTAL CODES
        ElseIf cmbbox_es_country_load.SelectedItem = "US" Then
            If Regex.IsMatch(txtbox_es_pcode_load.Text, pcode_patern_us) = False Then
                error_fields = error_fields & "Invalid Postal Code. US postal code only!" & vbNewLine
                txtbox_es_pcode_load.Clear()
                txtbox_es_pcode_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_pcode_load.BackColor = Color.White
            End If
        Else
            error_fields = error_fields & ""
            txtbox_es_pcode_load.BackColor = Color.White
        End If

        'COUNTRY
        If cmbbox_es_country_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Incomplete address: COUNTRY" & vbNewLine
            cmbbox_es_country_load.ResetText()
            cmbbox_es_country_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_country_load.BackColor = Color.White
        End If

        'MAIN LINE
        Dim phone_patern = "\d{10}"
        If txtbox_es_mainline_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_es_mainline_load.Text = ""
            txtbox_es_mainline_load.BackColor = Color.White
        Else
            If Not IsNumeric(txtbox_es_mainline_load.Text) Then
                error_fields = error_fields & "Invalid Main Line. Digits only!" & vbNewLine
                txtbox_es_mainline_load.Clear()
                txtbox_es_mainline_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_es_mainline_load.Text, phone_patern) = False Then
                error_fields = error_fields & "Invalid Main Line. Follow format 5145557777" & vbNewLine
                txtbox_es_mainline_load.Clear()
                txtbox_es_mainline_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_mainline_load.BackColor = Color.White
            End If
        End If


        'BO LINE
        If txtbox_es_boline_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_es_boline_load.BackColor = Color.White
        Else
            If Not IsNumeric(txtbox_es_boline_load.Text) Then
                error_fields = error_fields & "Invalid BO Line. Digits only!" & vbNewLine
                txtbox_es_boline_load.Clear()
                txtbox_es_boline_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_es_boline_load.Text, phone_patern) = False Then
                error_fields = error_fields & "Invalid BO Line. Follow format 5145557777" & vbNewLine
                txtbox_es_boline_load.Clear()
                txtbox_es_boline_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_boline_load.BackColor = Color.White
            End If
        End If

        'FAX LINE
        If txtbox_es_faxline_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_es_faxline_load.BackColor = Color.White
        Else
            If Not IsNumeric(txtbox_es_faxline_load.Text) Then
                error_fields = error_fields & "Invalid Fax Line. Digits only!" & vbNewLine
                txtbox_es_faxline_load.Clear()
                txtbox_es_faxline_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_es_faxline_load.Text, phone_patern) = False Then
                error_fields = error_fields & "Invalid Fax Line. Follow format 5145557777" & vbNewLine
                txtbox_es_faxline_load.Clear()
                txtbox_es_faxline_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_faxline_load.BackColor = Color.White
            End If
        End If

        'Internet LINE
        If txtbox_es_dslline_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_es_dslline_load.BackColor = Color.White
        Else

            If Not IsNumeric(txtbox_es_dslline_load.Text) Then
                error_fields = error_fields & "Invalid Internet Line. Digits only!" & vbNewLine
                txtbox_es_dslline_load.Clear()
                txtbox_es_dslline_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_es_dslline_load.Text, phone_patern) = False Then
                error_fields = error_fields & "Invalid Internet Line. Follow format 5145557777" & vbNewLine
                txtbox_es_dslline_load.Clear()
                txtbox_es_dslline_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_dslline_load.BackColor = Color.White
            End If
        End If


        'IP
        Dim ip_patern = "10.([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).$"
        Dim isp_ip_patern = "^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$"

        If Regex.IsMatch(txtbox_es_ip_load.Text, ip_patern) = False Then
            error_fields = error_fields & "Invalid Store IP Address. Follow format 10.99.99." & vbNewLine
            txtbox_es_ip_load.Clear()
            txtbox_es_ip_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_es_ip_load.BackColor = Color.White
        End If

        'ISP check
        If txtbox_es_isp_load.Text = "" Then
            error_fields = error_fields & "Missing ISP name." & vbNewLine
            txtbox_es_isp_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_es_isp_load.BackColor = Color.White
        End If

        'ISP Address assignment, Username, Password, ISP_IP, ISP_Subnet, ISP_Gateway
        If cmbbox_es_ispstatus_load.SelectedItem = "PPPOE" Then
            If txtbox_es_user_load.Text = "" Then
                error_fields = error_fields & "PPPOE selected. Missing ISP username!" & vbNewLine
                txtbox_es_user_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_user_load.BackColor = Color.White
            End If

            If txtbox_es_pwd_load.Text = "" Then
                error_fields = error_fields & "PPPOE selected. Missing ISP Password!" & vbNewLine
                txtbox_es_pwd_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_pwd_load.BackColor = Color.White
            End If

            'clear unnecessary info
            txtbox_es_ispaddress_load.Clear()
            txtbox_es_ispsubnet_load.Clear()
            txtbox_es_ispgateway_load.Clear()

        ElseIf cmbbox_es_ispstatus_load.SelectedItem = "Static" Then
            If txtbox_es_ispaddress_load.Text = "" Then
                error_fields = error_fields & "Static selected. Missing ISP IP Address!" & vbNewLine
                txtbox_es_ispaddress_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_es_ispaddress_load.Text, isp_ip_patern) = False Then
                error_fields = error_fields & "Invalid ISP IP Address. Follow format 192.168.1.1" & vbNewLine
                txtbox_es_ispaddress_load.Clear()
                txtbox_es_ispaddress_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_ispaddress_load.BackColor = Color.White
            End If

            If txtbox_es_ispsubnet_load.Text = "" Then
                error_fields = error_fields & "Static selected. Missing ISP Subnet!" & vbNewLine
                txtbox_es_ispsubnet_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_es_ispsubnet_load.Text, isp_ip_patern) = False Then
                error_fields = error_fields & "Invalid ISP Subnet. Follow format 255.255.255.0" & vbNewLine
                txtbox_es_ispsubnet_load.Clear()
                txtbox_es_ispsubnet_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_ispsubnet_load.BackColor = Color.White
            End If

            If txtbox_es_ispgateway_load.Text = "" Then
                error_fields = error_fields & "Static selected. Missing ISP Gateway!" & vbNewLine
                txtbox_es_ispgateway_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_es_ispgateway_load.Text, isp_ip_patern) = False Then
                error_fields = error_fields & "Invalid ISP Gateway. Follow format 192.168.2.1" & vbNewLine
                txtbox_es_ispgateway_load.Clear()
                txtbox_es_ispgateway_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_ispgateway_load.BackColor = Color.White
            End If

            'clear unnecessary info
            txtbox_es_user_load.Clear()
            txtbox_es_pwd_load.Clear()

        ElseIf cmbbox_es_ispstatus_load.SelectedItem = "DHCP" Then
            error_fields = error_fields & ""
            cmbbox_es_ispstatus_load.BackColor = Color.White

            'clear unnecessary info
            txtbox_es_ispaddress_load.Clear()
            txtbox_es_ispsubnet_load.Clear()
            txtbox_es_ispgateway_load.Clear()
            txtbox_es_user_load.Clear()
            txtbox_es_pwd_load.Clear()

        Else
            error_fields = error_fields & "Missing ISP Address Assignment!" & vbNewLine
            cmbbox_es_ispstatus_load.BackColor = Color.DarkSalmon
        End If


        'Store Speed
        Dim sspeed_patern = "[0-9]*\.?[0-9]+ \/ ?[0-9]*\.?[0-9]+"
        If txtbox_es_speed_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_es_speed_load.BackColor = Color.White
        ElseIf Regex.IsMatch(txtbox_es_speed_load.Text, sspeed_patern) = False Then
            error_fields = error_fields & "Invalid Store Speed. Follow format 10 / 10" & vbNewLine
            txtbox_es_speed_load.Clear()
            txtbox_es_speed_load.BackColor = Color.DarkSalmon
        End If

        'WiFi Speed
        Dim wifi_patern = "^[0-9]*\.?[0-9]+$"
        If txtbox_es_wifi_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_es_wifi_load.BackColor = Color.White
        ElseIf Regex.IsMatch(txtbox_es_wifi_load.Text, wifi_patern) = False Then
            error_fields = error_fields & "Invalid Store WiFi Speed. Follow format 0.6" & vbNewLine
            txtbox_es_wifi_load.Clear()
            txtbox_es_wifi_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_es_wifi_load.BackColor = Color.White
        End If

        'Router Location
        If cmbbox_es_rlocation_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing Router Location!" & vbNewLine
            cmbbox_es_rlocation_load.ResetText()
            cmbbox_es_rlocation_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_rlocation_load.BackColor = Color.White
        End If

        'Cabinet Lock
        Dim itlock = "^\d{4}$"

        If Not txtbox_es_cablock_load.Text = "" Then

            If Regex.IsMatch(txtbox_es_cablock_load.Text, itlock) = False Then
                error_fields = error_fields & "Invalid IT Cabinet Lock. Digits only!" & vbNewLine
                txtbox_es_cablock_load.Clear()
                txtbox_es_cablock_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_es_cablock_load.BackColor = Color.White
            End If
        Else
            error_fields = error_fields & ""
            txtbox_es_cablock_load.BackColor = Color.White
        End If

        'SPARE: modem
        If cmbbox_es_smodem_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Modem info!" & vbNewLine
            cmbbox_es_smodem_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_smodem_load.BackColor = Color.White
        End If

        'SPARE: router
        If cmbbox_es_srouter_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Router info!" & vbNewLine
            cmbbox_es_srouter_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_srouter_load.BackColor = Color.White
        End If

        'SPARE: wireless card
        If cmbbox_es_swc_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Wireless Card info!" & vbNewLine
            cmbbox_es_swc_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_swc_load.BackColor = Color.White
        End If

        'SPARE: holiday cash
        If cmbbox_es_hpc_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Holiday PC info!" & vbNewLine
            cmbbox_es_hpc_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_hpc_load.BackColor = Color.White
        End If

        'SPARE: pinpad
        If cmbbox_es_sppad_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Pinpad info!" & vbNewLine
            cmbbox_es_sppad_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_sppad_load.BackColor = Color.White
        End If

        'isCOMBO
        If cmbbox_es_iscombo_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing COMBO status!" & vbNewLine
            cmbbox_es_iscombo_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_iscombo_load.BackColor = Color.White
        End If

        'pinpads
        If cmbbox_es_pinpad_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing Pinpad connection info!" & vbNewLine
            cmbbox_es_pinpad_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_pinpad_load.BackColor = Color.White
        End If

        'isREMOTE
        If cmbbox_es_isremote_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing REMOTE status!" & vbNewLine
            cmbbox_es_isremote_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_isremote_load.BackColor = Color.White
        End If

        'STATUS (ALWAYS OPEN FOR NOW)
        If cmbbox_es_status_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing store STATUS!" & vbNewLine
            cmbbox_es_status_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_status_load.BackColor = Color.White
        End If

        'STORE LOCATION
        If cmbbox_es_location_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing store Location!" & vbNewLine
            cmbbox_es_location_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_es_location_load.BackColor = Color.White
        End If

        'SET YES/NO to BITS
        'COMBO
        Dim combo_int As Integer = Nothing
        If cmbbox_es_iscombo_load.SelectedItem = "YES" Then
            combo_int = 1
        Else
            combo_int = 0
        End If
        'REMOTE
        Dim remote_int As Integer = Nothing
        If cmbbox_es_isremote_load.SelectedItem = "YES" Then
            remote_int = 1
        Else
            remote_int = 0
        End If
        'STATUS
        Dim status_int As Integer = Nothing
        If cmbbox_es_status_load.SelectedItem = "OPEN" Then
            status_int = 1
        Else
            status_int = 0
        End If

        'STORE LOCATION
        Dim location_int As Integer = Nothing
        If cmbbox_es_location_load.SelectedItem = "TEMP" Then
            location_int = 1
        Else
            location_int = 0
        End If

        'MODEM
        Dim smodem_int As Integer = Nothing
        If cmbbox_es_smodem_load.SelectedItem = "YES" Then
            smodem_int = 1
        Else
            smodem_int = 0
        End If
        'ROUTER
        Dim srouter_int As Integer = Nothing
        If cmbbox_es_srouter_load.SelectedItem = "YES" Then
            srouter_int = 1
        Else
            srouter_int = 0
        End If
        'WIRELESS CARD
        Dim swc_int As Integer = Nothing
        If cmbbox_es_swc_load.SelectedItem = "YES" Then
            swc_int = 1
        Else
            swc_int = 0
        End If
        'HOLIDAY PC
        Dim spc_int As Integer = Nothing
        If cmbbox_es_hpc_load.SelectedItem = "YES" Then
            spc_int = 1
        Else
            spc_int = 0
        End If
        'PINPAD
        Dim spp_int As Integer = Nothing
        If cmbbox_es_sppad_load.SelectedItem = "YES" Then
            spp_int = 1
        Else
            spp_int = 0
        End If


        'Display error
        If error_fields = "" Then

            gdx_main.editStore(storeno_int, cmbbox_es_banner_load.SelectedItem, cmbbox_es_store_system.SelectedItem, combo_int, cmbbox_es_pinpad_load.SelectedItem, remote_int,
                               status_int, location_int, sstore_str, txtbox_es_mall_load.Text, txtbox_es_address_load.Text, txtbox_es_city_load.Text, cmbbox_es_province_load.SelectedItem,
                               UCase(txtbox_es_pcode_load.Text), cmbbox_es_country_load.SelectedItem, txtbox_es_mainline_load.Text, txtbox_es_boline_load.Text, txtbox_es_faxline_load.Text,
                               txtbox_es_dslline_load.Text, txtbox_es_ip_load.Text, txtbox_es_isp_load.Text, txtbox_es_ispaccount_load.Text, cmbbox_es_ispstatus_load.SelectedItem,
                               txtbox_es_ispaddress_load.Text, txtbox_es_ispsubnet_load.Text, txtbox_es_ispgateway_load.Text, txtbox_es_speed_load.Text, txtbox_es_wifi_load.Text,
                               txtbox_es_user_load.Text, txtbox_es_pwd_load.Text, cmbbox_es_rlocation_load.SelectedItem, txtbox_es_cablock_load.Text,
                               smodem_int, srouter_int, swc_int, spc_int, spp_int)

            If gdx_main.edit_flag = True Then
                clear_editstore("no", False)
                gdx_main.edit_flag = False      'set flag to false
            Else

                'CHECK AUTO-EDIT SIS STORE IF NEEDED OR THROW EXEPTION
                If cmbbox_es_iscombo_load.SelectedItem = "NO" Then
                    'IF COMBO NO BUT HAS SISTER STORE
                    If txtbox_es_sisstore_load.Text = "" Then
                        'clear all form
                        clear_editstore("no", False)
                    Else

                        Dim edit_result As Integer = MsgBox("Do you want to edit SISTER STORE: " & gdx_main.lbl_sis_store_load.Text, MsgBoxStyle.YesNo, Title:="GDnetworks - store EDIT")
                        If edit_result = DialogResult.No Then
                            clear_editstore("no", False)
                            gdx_main.edit_flag = False
                        Else
                            gdx_main.edit_flag = True
                            Me.Dispose()
                            gdx_main.txtbox_storeno.Text = gdx_main.lbl_sis_store_load.Text
                            gdx_main.btn_search_store.PerformClick()
                            gdx_main.EditToolStripMenuItem.PerformClick()
                        End If
                    End If

                ElseIf cmbbox_es_iscombo_load.SelectedItem = "YES" Then

                    MsgBox("Please check if the following fields are correct: " & vbNewLine & " - Sister store" &
                           vbNewLine & " - COMBO", MsgBoxStyle.Exclamation, Title:="GDnetworks - Info! (combo - add)")

                    'exchange store number with sis store
                    gdx_main.edit_flag = True
                    Me.Dispose()
                    gdx_main.txtbox_storeno.Text = gdx_main.lbl_sis_store_load.Text
                    gdx_main.btn_search_store.PerformClick()
                    gdx_main.EditToolStripMenuItem.PerformClick()

                End If
            End If
        Else
            MsgBox("Missing mandatory information Or information Is Not correct." & vbNewLine &
                   "Make sure you follow the instructions: mouse-over on labels." & vbNewLine &
                   "Please check the following fields:" & vbNewLine & vbNewLine & error_fields)

        End If

    End Sub

    'clear entire form or clear form without store
    Protected Friend Sub clear_editstore(sstore As String, combo As Boolean)

        'IF NEW STORE HAS SISTER STORE
        If sstore = "yes" Then

            Dim new_main As String = txtbox_es_sisstore_load.Text
            Dim new_sister As String = txtbox_es_storeno_load.Text
            Dim old_banner As String = cmbbox_es_banner_load.SelectedItem

            'load SS store#
            txtbox_es_storeno_load.Text = new_main
            'load SS banner
            If old_banner = "Dynamite" Then
                cmbbox_es_banner_load.SelectedItem = "Garage"
            ElseIf old_banner = "Dynamite HO" Then
                cmbbox_es_banner_load.SelectedItem = "Garage HO"
            ElseIf old_banner = "Dynamite combo" Then
                cmbbox_es_banner_load.SelectedItem = "Garage combo"
            ElseIf old_banner = "Dynamite USA" Then
                cmbbox_es_banner_load.SelectedItem = "Garage USA"
            ElseIf old_banner = "Garage" Then
                cmbbox_es_banner_load.SelectedItem = "Dynamite"
            ElseIf old_banner = "Garage HO" Then
                cmbbox_es_banner_load.SelectedItem = "Dynamite HO"
            ElseIf old_banner = "Garage combo" Then
                cmbbox_es_banner_load.SelectedItem = "Dynamite combo"
            ElseIf old_banner = "Garage USA" Then
                cmbbox_es_banner_load.SelectedItem = "Dynamite USA"
            End If
            'load ss
            txtbox_es_sisstore_load.Text = new_sister

            'IF STORE IS COMBO
            If combo = False Then

                cmbbox_es_store_system.SelectedValue = -1
                cmbbox_es_iscombo_load.SelectedItem = "NO"
                txtbox_es_address_load.Text = ""
                txtbox_es_pcode_load.Text = ""
                txtbox_es_mainline_load.Text = ""
                txtbox_es_boline_load.Text = ""
                txtbox_es_faxline_load.Text = ""
                txtbox_es_dslline_load.Text = ""
                txtbox_es_ip_load.Text = ""
                txtbox_es_isp_load.Text = ""
                txtbox_es_ispaccount_load.Text = ""
                cmbbox_es_ispstatus_load.SelectedValue = -1
                txtbox_es_ispaddress_load.Text = ""
                txtbox_es_ispsubnet_load.Text = ""
                txtbox_es_ispgateway_load.Text = ""
                txtbox_es_user_load.Text = ""
                txtbox_es_pwd_load.Text = ""
                txtbox_es_speed_load.Text = ""
                txtbox_es_wifi_load.Text = ""
                cmbbox_es_rlocation_load.ResetText()
                txtbox_es_cablock_load.Text = ""
                cmbbox_es_smodem_load.ResetText()
                cmbbox_es_srouter_load.ResetText()
                cmbbox_es_swc_load.ResetText()
                cmbbox_es_hpc_load.ResetText()
                cmbbox_es_sppad_load.ResetText()
                cmbbox_es_isremote_load.ResetText()
                cmbbox_es_pinpad_load.SelectedValue = -1

            Else

            End If

        ElseIf sstore = "no" Then

            txtbox_es_storeno_load.Text = ""
            cmbbox_es_banner_load.ResetText()
            cmbbox_es_store_system.ResetText()
            cmbbox_es_iscombo_load.ResetText()
            txtbox_es_sisstore_load.Text = ""
            txtbox_es_mall_load.Text = ""
            txtbox_es_address_load.Text = ""
            txtbox_es_city_load.Text = ""
            cmbbox_es_province_load.ResetText()
            txtbox_es_pcode_load.Text = ""
            cmbbox_es_country_load.ResetText()
            txtbox_es_mainline_load.Text = ""
            txtbox_es_boline_load.Text = ""
            txtbox_es_faxline_load.Text = ""
            txtbox_es_dslline_load.Text = ""
            txtbox_es_ip_load.Text = ""
            txtbox_es_isp_load.Text = ""
            txtbox_es_ispaccount_load.Text = ""
            cmbbox_es_ispstatus_load.ResetText()
            txtbox_es_ispaddress_load.Text = ""
            txtbox_es_ispsubnet_load.Text = ""
            txtbox_es_ispgateway_load.Text = ""
            txtbox_es_user_load.Text = ""
            txtbox_es_pwd_load.Text = ""
            txtbox_es_speed_load.Text = ""
            txtbox_es_wifi_load.Text = ""
            cmbbox_es_rlocation_load.ResetText()
            txtbox_es_cablock_load.Text = ""
            cmbbox_es_smodem_load.ResetText()
            cmbbox_es_srouter_load.ResetText()
            cmbbox_es_swc_load.ResetText()
            cmbbox_es_hpc_load.ResetText()
            cmbbox_es_sppad_load.ResetText()
            cmbbox_es_isremote_load.ResetText()
            cmbbox_es_pinpad_load.ResetText()

        End If

    End Sub

    'BUTTON : CLEAR FORM
    Private Sub btn_es_clear_Click_1(sender As Object, e As EventArgs) Handles btn_es_clear.Click

        clear_editstore("no", False)

    End Sub

    'BUTTON : CLOSE
    Private Sub btn_es_cancel_Click_1(sender As Object, e As EventArgs) Handles btn_es_cancel.Click
        Me.Dispose()
    End Sub

    'ON CLOSING FORM
    Private Sub Store_edit_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        gdx_main.edit_flag = False
        Me.Dispose()
    End Sub

    'ENABLE MANDATORY FIELD USER & PASSWORD
    Private Sub txtbox_es_user_load_TextChanged(sender As Object, e As EventArgs)

        If Not txtbox_es_user_load.Text = "" Then
            ns_star18.Text = "*"
        Else
            ns_star18.Text = ""
        End If

    End Sub

    'ENABLE MANDATORY RouterLocation and IT Cabinet lock
    Private Sub cmbbox_es_rlocation_load_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    'ENABLE MANDATORY COMBO & SIS STORE
    Private Sub cmbbox_es_iscombo_load_SelectedIndexChanged(sender As Object, e As EventArgs)

        If cmbbox_es_iscombo_load.SelectedItem = "YES" Then
            ns_star4.Text = "*"
        Else
            ns_star4.Text = ""
        End If

    End Sub

    'manage IP Status combo box and mandatory fields
    Private Sub cmbbox_es_ispstatus_load_SelectedIndexChanged(sender As Object, e As EventArgs)

        If cmbbox_es_ispstatus_load.SelectedItem = "PPPOE" Then
            ns_star17.Text = "*"
            ns_star18.Text = "*"
            ns_star14.Text = ""
            ns_star15.Text = ""
            ns_star16.Text = ""

            txtbox_es_ispaddress_load.Clear()
            txtbox_es_ispsubnet_load.Clear()
            txtbox_es_ispgateway_load.Clear()

        ElseIf cmbbox_es_ispstatus_load.SelectedItem = "Static" Then

            ns_star17.Text = ""
            ns_star18.Text = ""
            ns_star14.Text = "*"
            ns_star15.Text = "*"
            ns_star16.Text = "*"

            txtbox_es_user_load.Clear()
            txtbox_es_pwd_load.Clear()

        ElseIf cmbbox_es_ispstatus_load.SelectedItem = "DHCP" Then

            ns_star17.Text = ""
            ns_star18.Text = ""
            ns_star14.Text = ""
            ns_star15.Text = ""
            ns_star16.Text = ""

            txtbox_es_ispaddress_load.Clear()
            txtbox_es_ispsubnet_load.Clear()
            txtbox_es_ispgateway_load.Clear()
            txtbox_es_user_load.Clear()
            txtbox_es_pwd_load.Clear()

        End If

    End Sub

    'CHECK PPPOE. Static or DHCP input
    Private Sub cmbbox_es_ispstatus_load_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles cmbbox_es_ispstatus_load.SelectedIndexChanged


        If cmbbox_es_ispstatus_load.SelectedItem = "PPPOE" Then


            ns_star17.Text = "*"
            ns_star18.Text = "*"
            ns_star14.Text = ""
            ns_star15.Text = ""
            ns_star16.Text = ""

            txtbox_es_ispaddress_load.Clear()
            txtbox_es_ispsubnet_load.Clear()
            txtbox_es_ispgateway_load.Clear()

        ElseIf cmbbox_es_ispstatus_load.SelectedItem = "Static" Then

            ns_star17.Text = ""
            ns_star18.Text = ""
            ns_star14.Text = "*"
            ns_star15.Text = "*"
            ns_star16.Text = "*"

            txtbox_es_user_load.Clear()
            txtbox_es_pwd_load.Clear()

        ElseIf cmbbox_es_ispstatus_load.SelectedItem = "DHCP" Then

            ns_star17.Text = ""
            ns_star18.Text = ""
            ns_star14.Text = ""
            ns_star15.Text = ""
            ns_star16.Text = ""

            txtbox_es_ispaddress_load.Clear()
            txtbox_es_ispsubnet_load.Clear()
            txtbox_es_ispgateway_load.Clear()
            txtbox_es_user_load.Clear()
            txtbox_es_pwd_load.Clear()

        End If

    End Sub

    'ADD ITEMS FOR PROVINCE COMBO
    Private Sub cmbbox_es_country_load_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_es_country_load.SelectedIndexChanged

        'ADD CANADIAN PROVINCES
        If cmbbox_es_country_load.SelectedItem = "Canada" Then

            'clear all items
            cmbbox_es_province_load.Items.Clear()

            'add items
            cmbbox_es_province_load.Items.Add("Alberta")
            cmbbox_es_province_load.Items.Add("BC")
            cmbbox_es_province_load.Items.Add("Manitoba")
            cmbbox_es_province_load.Items.Add("NB")
            cmbbox_es_province_load.Items.Add("NS")
            cmbbox_es_province_load.Items.Add("NFLD")
            cmbbox_es_province_load.Items.Add("Nunavut")
            cmbbox_es_province_load.Items.Add("NWT")
            cmbbox_es_province_load.Items.Add("Ontario")
            cmbbox_es_province_load.Items.Add("PEI")
            cmbbox_es_province_load.Items.Add("Quebec")
            cmbbox_es_province_load.Items.Add("Saskatchewan")
            cmbbox_es_province_load.Items.Add("Yukon")

            'ADD US STATES
        ElseIf cmbbox_es_country_load.SelectedItem = "US" Then

            'clear all items
            cmbbox_es_province_load.Items.Clear()

            'add items
            cmbbox_es_province_load.Items.Add("Alabama")
            cmbbox_es_province_load.Items.Add("Alaska")
            cmbbox_es_province_load.Items.Add("American Samoa")
            cmbbox_es_province_load.Items.Add("Arizona")
            cmbbox_es_province_load.Items.Add("Arkansas")
            cmbbox_es_province_load.Items.Add("California")
            cmbbox_es_province_load.Items.Add("Colorado")
            cmbbox_es_province_load.Items.Add("Connecticut")
            cmbbox_es_province_load.Items.Add("Delaware")
            cmbbox_es_province_load.Items.Add("District of Columbia")
            cmbbox_es_province_load.Items.Add("Federated States of Micronesia")
            cmbbox_es_province_load.Items.Add("Florida")
            cmbbox_es_province_load.Items.Add("Georgia")
            cmbbox_es_province_load.Items.Add("Guam")
            cmbbox_es_province_load.Items.Add("Hawaii")
            cmbbox_es_province_load.Items.Add("Idaho")
            cmbbox_es_province_load.Items.Add("Illinois")
            cmbbox_es_province_load.Items.Add("Indiana")
            cmbbox_es_province_load.Items.Add("Iowa")
            cmbbox_es_province_load.Items.Add("Kansas")
            cmbbox_es_province_load.Items.Add("Kentucky")
            cmbbox_es_province_load.Items.Add("Louisiana")
            cmbbox_es_province_load.Items.Add("Maine")
            cmbbox_es_province_load.Items.Add("Marshall Islands")
            cmbbox_es_province_load.Items.Add("Maryland")
            cmbbox_es_province_load.Items.Add("Massachusetts")
            cmbbox_es_province_load.Items.Add("Michigan")
            cmbbox_es_province_load.Items.Add("Minnesota")
            cmbbox_es_province_load.Items.Add("Mississippi")
            cmbbox_es_province_load.Items.Add("Missouri")
            cmbbox_es_province_load.Items.Add("Montana")
            cmbbox_es_province_load.Items.Add("Nebraska")
            cmbbox_es_province_load.Items.Add("Nevada")
            cmbbox_es_province_load.Items.Add("New Hampshire")
            cmbbox_es_province_load.Items.Add("New Jersey")
            cmbbox_es_province_load.Items.Add("New Mexico")
            cmbbox_es_province_load.Items.Add("New York")
            cmbbox_es_province_load.Items.Add("North Carolina")
            cmbbox_es_province_load.Items.Add("North Dakota")
            cmbbox_es_province_load.Items.Add("Northern Mariana Islands")
            cmbbox_es_province_load.Items.Add("Ohio")
            cmbbox_es_province_load.Items.Add("Oklahoma")
            cmbbox_es_province_load.Items.Add("Oregon")
            cmbbox_es_province_load.Items.Add("Palau")
            cmbbox_es_province_load.Items.Add("Pennsylvania")
            cmbbox_es_province_load.Items.Add("Puerto Rico")
            cmbbox_es_province_load.Items.Add("Rhode Island")
            cmbbox_es_province_load.Items.Add("South Carolina")
            cmbbox_es_province_load.Items.Add("South Dakota")
            cmbbox_es_province_load.Items.Add("Tennessee")
            cmbbox_es_province_load.Items.Add("Texas")
            cmbbox_es_province_load.Items.Add("Utah")
            cmbbox_es_province_load.Items.Add("Vermont")
            cmbbox_es_province_load.Items.Add("Virgin Islands")
            cmbbox_es_province_load.Items.Add("Virginia")
            cmbbox_es_province_load.Items.Add("Washington")
            cmbbox_es_province_load.Items.Add("West Virginia")
            cmbbox_es_province_load.Items.Add("Wisconsin")
            cmbbox_es_province_load.Items.Add("Wyoming")

        Else

            'clear all items
            cmbbox_es_province_load.Items.Clear()

        End If

    End Sub


    'TOOLTIPS
    Private Sub loadToolTips(label_location As System.Windows.Forms.Label, address As String, form_location As String)

        If form_location = "ns_store" Then
            ToolTip.SetToolTip(label_location, "Must be a number only." & vbNewLine & "EG: 6, 55, 232")
            'BANNER
        ElseIf form_location = "ns_banner" Then
            ToolTip.SetToolTip(label_location, "Choose from the available options." & vbNewLine & "Note: HO only for HO-test")
            'store System
        ElseIf form_location = "ns_ssystem" Then
            ToolTip.SetToolTip(label_location, "Choose from the available options." & vbNewLine & "Note: use ORPOS for now.")
            'Sister store
        ElseIf form_location = "ns_sstore" Then
            ToolTip.SetToolTip(label_location, "Must be a number only. Leave blank if N/A." & vbNewLine & "Mandatory for COMBO" & vbNewLine & "EG: 6, 55, 232")
            'Mall
        ElseIf form_location = "ns_mall" Then
            ToolTip.SetToolTip(label_location, "Can be both letters & digits" & vbNewLine & "EG: Quartier 10/30")
            'Address
        ElseIf form_location = "ns_address" Then
            ToolTip.SetToolTip(label_location, "Can be both letters & digits" & vbNewLine & "EG: 9405 Boulevard Leduc")
            'City
        ElseIf form_location = "ns_city" Then
            ToolTip.SetToolTip(label_location, "Must be letters only." & vbNewLine & "EG: Brossard")
            'State/province
        ElseIf form_location = "ns_state" Then
            ToolTip.SetToolTip(label_location, "Must be letters only." & vbNewLine & "EG: Quebec")
            'Postal code
        ElseIf form_location = "ns_pcode" Then
            ToolTip.SetToolTip(label_location, "Must be of format: A1A 1A1 (CA) 12345 (US)." & vbNewLine & "EG: J4Y 0A5 / 12345")
            'Country
        ElseIf form_location = "ns_countrye" Then
            ToolTip.SetToolTip(label_location, "Choose from the available options.")
            'Main line
        ElseIf form_location = "ns_mline" Then
            ToolTip.SetToolTip(label_location, "Must be of format: 10digits." & vbNewLine & "EG: 4504458451")
            'BO line
        ElseIf form_location = "ns_boline" Then
            ToolTip.SetToolTip(label_location, "Must be of format: 10digits." & vbNewLine & "EG: 4504458451")
            'Fax line
        ElseIf form_location = "ns_faxline" Then
            ToolTip.SetToolTip(label_location, "Must be of format: 10digits." & vbNewLine & "EG: 4504458451")
            'DSL line
        ElseIf form_location = "ns_dsl" Then
            ToolTip.SetToolTip(label_location, "Must be of format: 10digits." & vbNewLine & "EG: 4504458451")
            'IP 3 octets
        ElseIf form_location = "ns_ip" Then
            ToolTip.SetToolTip(label_location, "Must be of format: 0.0.0." & vbNewLine & "EG: 10.20.38.")
            'ISP
        ElseIf form_location = "ns_isp" Then
            ToolTip.SetToolTip(label_location, "Can be both letters & digits." & vbNewLine & "EG: BELLNET")
            'Username
        ElseIf form_location = "ns_user" Then
            ToolTip.SetToolTip(label_location, "Can be both letters & digits. Only if PPPOE selected." & vbNewLine & "EG: belleg@bellnet.ca")
            'Username
        ElseIf form_location = "ns_pwd" Then
            ToolTip.SetToolTip(label_location, "Can be both letters & digits. Only if PPPOE selected." & vbNewLine & "Only if 'Username' exists" & vbNewLine & "EG: myexample10")
            'Store speed
        ElseIf form_location = "ns_speed" Then
            ToolTip.SetToolTip(label_location, "Must be of format: DL / UP" & vbNewLine & "EG: 23.78 / 6.40")
            'wifi speed
        ElseIf form_location = "ns_wifi" Then
            ToolTip.SetToolTip(label_location, "Must be of format: DL" & vbNewLine & "EG: 0.6, 20")
            'Router location
        ElseIf form_location = "ns_rlocation" Then
            ToolTip.SetToolTip(label_location, "Choose from the available options.")
            'it cabinet lock
        ElseIf form_location = "ns_cabinet" Then
            ToolTip.SetToolTip(label_location, "Must be of format ####." & vbNewLine & "EG: 1234")
            'IS COMBO
        ElseIf form_location = "ns_combo" Then
            ToolTip.SetToolTip(label_location, "Choose from the available options." & vbNewLine & "If YES, Sister store # is mandatory!")
            'pinpad connection
        ElseIf form_location = "ns_pinpads" Then
            ToolTip.SetToolTip(label_location, "Choose from the available options." & vbNewLine & "Note: overIP are the new pinpads.")
            'IS remote
        ElseIf form_location = "ns_remote" Then
            ToolTip.SetToolTip(label_location, "Choose from the available options. Is this a remote location?")
        ElseIf form_location = "ns_location" Then
            ToolTip.SetToolTip(label_location, "Choose from the available options. Is this a temp location?")
        ElseIf form_location = "ns_ispEmail" Then
            ToolTip.SetToolTip(label_location, "If available enter ISP account number. Can be any format.")
        ElseIf form_location = "ns_ispstatus" Then
            ToolTip.SetToolTip(label_location, "Is the store on DHCP, PPPOE or Static?")
        ElseIf form_location = "ns_ispaddress" Then
            ToolTip.SetToolTip(label_location, "ISP IP Address. Only if Static IP selected. EG: 172.85.226.54")
        ElseIf form_location = "ns_ispsubnet" Then
            ToolTip.SetToolTip(label_location, "ISP Subnet. Only if Static IP selected. EG: 255.255.255.252")
        ElseIf form_location = "ns_ispgateway" Then
            ToolTip.SetToolTip(label_location, "ISP Default Gateway. Only if Static IP selected. EG: 172.85.226.53")
        End If

    End Sub

End Class