﻿'AUTHOR: Alex Dumitrascu
'DATE: 02-01-2018
'UPDATE: 31-01-2018
'
'FORM: admin_pwd.vb (access from MAIN)


Public Class admin_pwd

    'BUTTON: EXIT (Close admin pwd dialog)
    Private Sub btn_admin_pwd_exit_Click(sender As Object, e As EventArgs) Handles btn_admin_pwd_exit.Click
        Me.Dispose()
    End Sub

    Private Sub admin_pwd_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'clear user and pwd on load. Put focus on user
        txtbox_admin_usr.Text = ""
        txtbox_admin_pwd.Text = ""
        txtbox_admin_usr.Focus()

    End Sub

    Private Sub btn_admin_pwd_ok_Click(sender As Object, e As EventArgs) Handles btn_admin_pwd_ok.Click

        'check credentials
        If txtbox_admin_usr.Text = "admin" And txtbox_admin_pwd.Text = "Dynamis!!2018" Then

            admin.ShowDialog()
            Me.Dispose()

        Else
            'if creds are wrong clear user and pwd
            MsgBox("Wrong credentials!", MsgBoxStyle.Information, Title:="Login error.")
            txtbox_admin_usr.Text = ""
            txtbox_admin_pwd.Text = ""
            txtbox_admin_usr.Focus()
        End If

    End Sub
End Class