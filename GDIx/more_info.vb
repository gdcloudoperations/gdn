﻿'AUTHOR: Alex Dumitrascu
'DATE: 11-01-2018
'UPDATE: 01-03-2018
'
'FORM: more_info.vb (access from MAIN)


Imports System.Data.OleDb

Public Class form_more_info
    Private Sub grp_network_Enter(sender As Object, e As EventArgs) Handles grp_network.Enter

    End Sub

    Private Sub form_more_info_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'load top info from main form
        lbl_moreinfo_store_load.Text = gdx_main.lbl_strno_load.Text
        getAdditionalInfo(lbl_moreinfo_store_load.Text)
        lbl_moreinfo_pinpadcon_load.Text = gdx_main.pinpadCon
        lbl_moreinfo_storesys_load.Text = gdx_main.lbl_str_system_load.Text

        'load tooltips

        'COPY
        loadToolTips(lbl_isp_load, "copy_to_cb")
        loadToolTips(lbl_isp_account_load, "copy_to_cb")
        loadToolTips(lbl_dsl_line_load, "copy_to_cb")
        loadToolTips(lbl_isp_status_load, "copy_to_cb")
        loadToolTips(lbl_isp_address_load, "copy_to_cb")
        loadToolTips(lbl_isp_subnet_load, "copy_to_cb")
        loadToolTips(lbl_isp_gateway_load, "copy_to_cb")
        loadToolTips(lbl_isp_email_load, "copy_to_cb")
        loadToolTips(lbl_isp_pwd_load, "copy_to_cb")
        loadToolTips(lbl_sw1_model_load, "copy_to_cb")
        loadToolTips(lbl_sw2_model_load, "copy_to_cb")
        loadToolTips(lbl_sw3_model_load, "copy_to_cb")
        loadToolTips(lbl_itcabinet_lock_load, "copy_to_cb")
        loadToolTips(lbl_router_location_load, "copy_to_cb")

        loadToolTips(lbl_bo_line_load, "copy_to_cb")
        loadToolTips(lbl_fax_line_load, "copy_to_cb")
        loadToolTips(lbl_dss_name_load, "copy_to_cb")
        loadToolTips(lbl_dss_line_load, "copy_to_cb")
        loadToolTips(lbl_dss_email_load, "copy_to_cb")
        loadToolTips(lbl_director_load, "copy_to_cb")
        loadToolTips(lbl_director_line_load, "copy_to_cb")

        'ISP DETAILS
        loadToolTips(lbl_isp_status, "ISP_STATUS")
        loadToolTips(lbl_isp_email, "PPPOE")
        loadToolTips(lbl_isp_pwd, "PPPOE")
        loadToolTips(lbl_isp_address, "STATIC")
        loadToolTips(lbl_isp_subnet, "STATIC")
        loadToolTips(lbl_isp_gateway, "STATIC")


    End Sub

    'l0ad local tooltips
    Private Sub loadToolTips(label_location As System.Windows.Forms.Label, form_location As String)

        Dim tooltip = New ToolTip()

        If form_location = "copy_to_cb" Then
            tooltip.SetToolTip(label_location, "Copy to clipboard.")

        ElseIf form_location = "ISP_STATUS" Then
            tooltip.SetToolTip(label_location, "DHCP-N/A" & vbNewLine & "PPPOE-User&Pwd" & vbNewLine & "Static-IP,Subnet&Gateway")

        ElseIf form_location = "PPPOE" Then
            tooltip.SetToolTip(label_location, "Only for ISP on PPPOE.")

        ElseIf form_location = "STATIC" Then
            tooltip.SetToolTip(label_location, "Only for ISP on STATIC IP.")
        End If

    End Sub

    Private Sub getAdditionalInfo(storeno As String)

        'load all info into textboxes
        'isCOMBO
        If gdx_main.isCombo = True Then
            lbl_combo_load.Text = "YES"
        Else
            lbl_combo_load.Text = "NO"
        End If
        lbl_bo_line_load.Text = gdx_main.boLine                     'Backoffice line
        lbl_fax_line_load.Text = gdx_main.faxLine                   'Fax line
        lbl_dss_name_load.Text = gdx_main.dssName                   'DSS name
        lbl_dss_line_load.Text = gdx_main.dssLine                   'DSS phone
        lbl_dss_email_load.Text = gdx_main.dssEmail                 'DSS email
        lbl_dsl_line_load.Text = gdx_main.dslLine                   'Internet line
        lbl_director_load.Text = gdx_main.directorName              'Director name
        lbl_director_line_load.Text = gdx_main.directorLine         'Director line
        lbl_isp_load.Text = gdx_main.ispName                        'ISP name

        'CHECK FOR LIMITED OR LIMITED+ ACCOUNTS AND DISPLAY ****** for (ISP account, ISP IP assignment, ISP IPaddress, ISP IPsubnet, ISPgateway, ISPemail, ISPpassword, ITcabinetLock)

        'ISP account
        If gdx_main.user_level = "limited" Then
            lbl_isp_account_load.Text = "******"
        Else
            lbl_isp_account_load.Text = gdx_main.ispAccount
        End If

        'ISP IP assignment
        If gdx_main.user_level = "limited" Then
            lbl_isp_status_load.Text = "******"
        Else
            lbl_isp_status_load.Text = gdx_main.ispStatus
        End If

        'ISP IP address
        If gdx_main.user_level = "limited" Then
            lbl_isp_address_load.Text = "******"
        Else
            lbl_isp_address_load.Text = gdx_main.ispAddress
        End If

        'ISP IP subnet
        If gdx_main.user_level = "limited" Then
            lbl_isp_subnet_load.Text = "******"
        Else
            lbl_isp_subnet_load.Text = gdx_main.ispSubnet
        End If

        'ISP IP gateway
        If gdx_main.user_level = "limited" Then
            lbl_isp_gateway_load.Text = "******"
        Else
            lbl_isp_gateway_load.Text = gdx_main.ispGateway
        End If

        'ISP email, pwd & IT cabinet Lock
        If gdx_main.user_level = "limited" Then
            lbl_isp_email_load.Text = "******"
            lbl_isp_pwd_load.Text = "******"
            lbl_itcabinet_lock_load.Text = "******"
        ElseIf gdx_main.user_level = "limited+" Then
            lbl_isp_email_load.Text = gdx_main.ispEmail
            lbl_isp_pwd_load.Text = "******"
            lbl_itcabinet_lock_load.Text = gdx_main.itCabinetLock
        Else
            lbl_isp_email_load.Text = gdx_main.ispEmail
            lbl_isp_pwd_load.Text = gdx_main.ispPwd
            lbl_itcabinet_lock_load.Text = gdx_main.itCabinetLock
        End If

        lbl_router_model_load.Text = gdx_main.routerModel           'Router model
        lbl_sw1_model_load.Text = gdx_main.sw1Model                 'Switch1 model
        lbl_sw2_model_load.Text = gdx_main.sw2Model                 'Switch2 model
        lbl_sw3_model_load.Text = gdx_main.sw3Model                 'Switch3 model
        lbl_router_location_load.Text = gdx_main.routerLocation     'Router location
        lbl_spare_modem_load.Text = gdx_main.spModem                'SPARE modem
        lbl_spare_router_load.Text = gdx_main.spRouter              'SPARE router
        lbl_spare_wc_load.Text = gdx_main.spWC                      'SPARE wireless card
        lbl_spare_hoc_load.Text = gdx_main.spHC                     'SPARE holiday cash
        lbl_spare_pinpad_load.Text = gdx_main.spPinpad              'SPARE pinpad

    End Sub

    ''COPY TO CLIPBOARD INFO FROM LABELS

    'ISP
    Private Sub lbl_isp_load_Click(sender As Object, e As EventArgs) Handles lbl_isp_load.Click
        If Not lbl_isp_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_isp_load.Text)
        End If
    End Sub

    'ISP ACCOUNT
    Private Sub lbl_isp_account_load_Click(sender As Object, e As EventArgs) Handles lbl_isp_account_load.Click
        If Not lbl_isp_account_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_isp_account_load.Text)
        End If
    End Sub

    'INTERNET LINE
    Private Sub lbl_dsl_line_load_Click(sender As Object, e As EventArgs) Handles lbl_dsl_line_load.Click
        If Not lbl_dsl_line_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_dsl_line_load.Text)
        End If
    End Sub

    'ISP STATUS
    Private Sub lbl_isp_status_load_Click(sender As Object, e As EventArgs) Handles lbl_isp_status_load.Click
        If Not lbl_isp_status_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_isp_status_load.Text)
        End If
    End Sub

    'ISP IP ADDRESS
    Private Sub lbl_isp_address_load_Click(sender As Object, e As EventArgs) Handles lbl_isp_address_load.Click
        If Not lbl_isp_address_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_isp_address_load.Text)
        End If
    End Sub

    'ISP IP SUBNET
    Private Sub lbl_isp_subnet_load_Click(sender As Object, e As EventArgs) Handles lbl_isp_subnet_load.Click
        If Not lbl_isp_subnet_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_isp_subnet_load.Text)
        End If
    End Sub

    'ISP IP GATEWAY
    Private Sub lbl_isp_gateway_load_Click(sender As Object, e As EventArgs) Handles lbl_isp_gateway_load.Click
        If Not lbl_isp_gateway_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_isp_gateway_load.Text)
        End If
    End Sub

    'ISP EMAIL
    Private Sub lbl_isp_email_load_Click(sender As Object, e As EventArgs) Handles lbl_isp_email_load.Click
        If Not lbl_isp_email_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_isp_email_load.Text)
        End If
    End Sub

    'ISP PASSWORD
    Private Sub lbl_isp_pwd_load_Click(sender As Object, e As EventArgs) Handles lbl_isp_pwd_load.Click
        If Not lbl_isp_pwd_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_isp_pwd_load.Text)
        End If
    End Sub

    'SWITCH1
    Private Sub lbl_sw1_model_load_Click(sender As Object, e As EventArgs) Handles lbl_sw1_model_load.Click
        If Not lbl_sw1_model_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_sw1_model_load.Text)
        End If
    End Sub

    'SWITCH2
    Private Sub lbl_sw2_model_load_Click(sender As Object, e As EventArgs) Handles lbl_sw2_model_load.Click
        If Not lbl_sw2_model_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_sw2_model_load.Text)
        End If
    End Sub

    'SWITCH3
    Private Sub lbl_sw3_model_load_Click(sender As Object, e As EventArgs) Handles lbl_sw3_model_load.Click
        If Not lbl_sw3_model_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_sw3_model_load.Text)
        End If
    End Sub

    'IT CABINET LOCK
    Private Sub lbl_itcabinet_lock_load_Click(sender As Object, e As EventArgs) Handles lbl_itcabinet_lock_load.Click
        If Not lbl_itcabinet_lock_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_itcabinet_lock_load.Text)
        End If
    End Sub

    'Router Location
    Private Sub lbl_router_location_load_Click(sender As Object, e As EventArgs) Handles lbl_router_location_load.Click
        If Not lbl_router_location_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_router_location_load.Text)
        End If
    End Sub

    'BO LINE
    Private Sub lbl_bo_line_load_Click(sender As Object, e As EventArgs) Handles lbl_bo_line_load.Click
        If Not lbl_bo_line_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_bo_line_load.Text)
        End If
    End Sub

    'BO LINE
    Private Sub lbl_fax_line_load_Click(sender As Object, e As EventArgs) Handles lbl_fax_line_load.Click
        If Not lbl_fax_line_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_fax_line_load.Text)
        End If
    End Sub

    'DSS NAME
    Private Sub lbl_dss_name_load_Click(sender As Object, e As EventArgs) Handles lbl_dss_name_load.Click
        If Not lbl_dss_name_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_dss_name_load.Text)
        End If
    End Sub


    'DSS LINE
    Private Sub lbl_dss_line_load_Click(sender As Object, e As EventArgs) Handles lbl_dss_line_load.Click
        If Not lbl_dss_line_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_dss_line_load.Text)
        End If
    End Sub

    'DSS EMAIL
    Private Sub lbl_dss_email_load_Click(sender As Object, e As EventArgs) Handles lbl_dss_email_load.Click
        If Not lbl_dss_email_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_dss_email_load.Text)
        End If
    End Sub


    'DIRECTOR NAME
    Private Sub lbl_director_load_Click(sender As Object, e As EventArgs) Handles lbl_director_load.Click
        If Not lbl_director_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_director_load.Text)
        End If
    End Sub

    'DIRECTOR LINE
    Private Sub lbl_director_line_load_Click(sender As Object, e As EventArgs) Handles lbl_director_line_load.Click
        If Not lbl_director_line_load.Text = "N/A" Then
            My.Computer.Clipboard.SetText(lbl_director_line_load.Text)
        End If
    End Sub


    'BUTTON: EXIT (Close moreinfo dialog)
    Private Sub btn_moreInfo_close_Click(sender As Object, e As EventArgs) Handles btn_moreInfo_close.Click
        Me.Dispose()
    End Sub

End Class