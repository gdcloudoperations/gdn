﻿'AUTHOR: Alex Dumitrascu
'DATE: 09-08-2018
'UPDATE: 18-09-2018
'
'FORM: inventory.vb (access from MAIN)

Imports System.Data.SqlClient

Public Class Inventory

    'VARS
    'Invetory lists
    Dim bkpList_rtr As New List(Of ListViewItem)       'backup entire router inventory list
    Dim bkpList_sw As New List(Of ListViewItem)        'backup entire router inventory list
    Dim bkpList_ap As New List(Of ListViewItem)        'backup entire router inventory list
    Dim searchList As New List(Of ListViewItem)        'search inventory list

    'ADD/EDIT/DELETE VARS
    Dim errorAlert As String = Nothing

    'SELECTED DEVICE
    Dim selectedSN As String = Nothing
    Protected Friend selectedDevice As String = Nothing
    Dim selectedOS As String = Nothing
    Dim selectedStatus As String = Nothing
    Dim selectedLocation As String = Nothing
    Dim selectedNotes As String = Nothing

    'DB
    Dim inventory_con As SqlConnection = New SqlConnection(gdx_main.connectionString)

    'Check if location changed (used as flag for EDIT)
    Dim locationFlag As Boolean = False

    'MAIN ( ON LOAD)
    Private Sub Inventory_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'BLOCK ADD & DELETE DEVICE FOR ALL USERS ( ENABLE FOR ADMIN ONLY)
        If gdx_main.user_level = "full" Or gdx_main.user_level = "elevated" Then
            context_inventory_delete.Enabled = True
            chkbox_inventory_adddev.Enabled = True
        Else
            context_inventory_delete.Enabled = False
            chkbox_inventory_adddev.Enabled = False
        End If

        'disable ADD & SAVE inventory buttons
        btn_inventory_add.Enabled = False
        btn_inventory_save.Enabled = False

        'LOAD INVENTORY (Always pre-load routers)
        If gdx_main.strInvFlag = True Then
            getInventory(selectedDevice)
            cmbbox_inventory_location.Text = gdx_main.lbl_strno_load.Text.TrimStart("0"c)
            btn_inventory_refresh.PerformClick()
        Else
            getInventory(selectedDevice)
        End If

    End Sub

    'Check selected Device
    Private Sub tab_inventory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tab_inventory.SelectedIndexChanged

        'clear list first
        listview_inventory_routers.Items.Clear()
        listview_inventory_switches.Items.Clear()
        listview_inventory_aps.Items.Clear()

        'clear flag
        locationFlag = False

        If tab_inventory.SelectedTab Is tab_inventory_routers Then
            getInventory("RTR")
            selectedDevice = "RTR"
        ElseIf tab_inventory.SelectedTab Is tab_inventory_switches Then
            getInventory("SW")
            selectedDevice = "SW"
        ElseIf tab_inventory.SelectedTab Is tab_inventory_aps Then
            getInventory("AP")
            selectedDevice = "AP"
        End If

    End Sub

    'GET INVENTORY and populate lists
    Private Sub getInventory(selectedDevice As String)

        'Load list based on selected Device       
        If selectedDevice = "RTR" Then
            getRTRinventory("")                   'GET ROUTERS INVENTORY           
        ElseIf selectedDevice = "SW" Then
            getSWinventory("")                    'GET SWITCHES INVENTORY         
        ElseIf selectedDevice = "AP" Then
            getAPinventory("")                    'GET APs INVENTORY
        End If

    End Sub


    'GET ROUTER INVENTORY
    Private Sub getRTRinventory(attributes As String)

        Dim cmd_inv_rtr As SqlCommand
        Dim rtrReader As SqlDataReader
        Dim sql_inventory_RTR = "Select * from stores_inventory where Device like '%RTR%'" & attributes & "order by SN"
        cmd_inv_rtr = New SqlCommand(sql_inventory_RTR, inventory_con)

        Try
            inventory_con.Open()
            rtrReader = cmd_inv_rtr.ExecuteReader
            If rtrReader.HasRows Then
                While rtrReader.Read
                    'set items in list
                    ''Dim newRouter As New ListViewItem()
                    Dim newRouter As New List(Of ListViewItem)
                    ''newRouter.UseItemStyleForSubItems = False
                    ''newRouter.Text = rtrReader.GetValue(0).ToString            'SN                             
                    ''newRouter.SubItems.Add(rtrReader.GetValue(1).ToString)     'Device
                    ''newRouter.SubItems.Add(rtrReader.GetValue(2).ToString)     'OS 
                    ''newRouter.SubItems.Add(rtrReader.GetValue(3).ToString)     'Status 
                    ''newRouter.SubItems.Add(rtrReader.GetValue(4).ToString)     'Location 
                    ''newRouter.SubItems.Add(rtrReader.GetValue(5).ToString)     'Notes
                    newRouter.Add(New ListViewItem(New String() {rtrReader("SN"), rtrReader("Device"), rtrReader("OS"), rtrReader("Status"), rtrReader("Location"), rtrReader("Notes")}))
                    'add to list
                    ''listview_inventory_routers.Items.Add(newRouter).ToString()
                    listview_inventory_routers.Items.AddRange(newRouter.ToArray)
                End While
            End If
            inventory_con.Close()

            'autosize columns & header
            listview_inventory_routers.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
            listview_inventory_routers.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)

            'PREPARE SEARCH OPTION
            If Not attributes = "" Then
                'leave search options selected
            Else
                prepareSearch("RTR", "yes")
            End If

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - get RTR]")
        End Try

        'display toatl number of ROUTERS
        lbl_inventory_total.Text = "TOTAL (Routers): " & listview_inventory_routers.Items.Count

        'backup original list for restore
        'For Each bkpItem As ListViewItem In listview_inventory_routers.Items
        'bkpList_rtr.Add(bkpItem)
        'Next

    End Sub

    'GET SWITCH INVENTORY
    Private Sub getSWinventory(attributes As String)

        Dim cmd_inv_sw As SqlCommand
        Dim swReader As SqlDataReader
        Dim sql_inventory_SW = "Select * from stores_inventory where Device like '%SW%'" & attributes & " order by SN"
        cmd_inv_sw = New SqlCommand(sql_inventory_SW, inventory_con)

        Try
            inventory_con.Open()
            swReader = cmd_inv_sw.ExecuteReader
            If swReader.HasRows Then
                While swReader.Read
                    'set items in list
                    Dim newSwitch As New ListViewItem()
                    '''Dim newSwitch As New List(Of ListViewItem)
                    newSwitch.UseItemStyleForSubItems = False
                    newSwitch.Text = swReader.GetValue(0).ToString            'SN                             
                    newSwitch.SubItems.Add(swReader.GetValue(1).ToString)     'Device
                    newSwitch.SubItems.Add(swReader.GetValue(2).ToString)     'OS 
                    newSwitch.SubItems.Add(swReader.GetValue(3).ToString)     'Status 
                    newSwitch.SubItems.Add(swReader.GetValue(4).ToString)     'Location 
                    newSwitch.SubItems.Add(swReader.GetValue(5).ToString)     'Notes
                    '''newSwitch.Add(New ListViewItem(New String() {swReader("SN"), swReader("Device"), swReader("OS"), swReader("Status"), swReader("Location"), swReader("Notes")}))
                    'add to list
                    listview_inventory_switches.Items.Add(newSwitch).ToString()
                    '''listview_inventory_switches.Items.AddRange(newSwitch.ToArray)
                End While
            End If
            inventory_con.Close()

            'autosize columns & header
            listview_inventory_switches.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
            listview_inventory_switches.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)

            'PREPARE SEARCH OPTION
            If Not attributes = "" Then
                'leave search options selected
            Else
                prepareSearch("SW", "yes")
            End If

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - get SW]")
        End Try

        'display toatl number of SWITCHES
        lbl_inventory_total.Text = "TOTAL (Switches): " & listview_inventory_switches.Items.Count

        'backup original list for restore
        'For Each bkpItem As ListViewItem In listview_inventory_switches.Items
        'bkpList_sw.Add(bkpItem)
        'Next

    End Sub

    Private Sub getAPinventory(attributes As String)

        Dim cmd_inv_ap As SqlCommand
        Dim apReader As SqlDataReader
        Dim sql_inventory_AP = "Select * from stores_inventory where Device like '%AP%'" & attributes & " order by SN"
        cmd_inv_ap = New SqlCommand(sql_inventory_AP, inventory_con)

        Try
            inventory_con.Open()
            apReader = cmd_inv_ap.ExecuteReader
            If apReader.HasRows Then
                While apReader.Read
                    'set items in list
                    ''Dim newAP As New ListViewItem()
                    Dim newAP As New List(Of ListViewItem)
                    ''newAP.UseItemStyleForSubItems = False
                    ''newAP.Text = apReader.GetValue(0).ToString            'SN                             
                    ''newAP.SubItems.Add(apReader.GetValue(1).ToString)     'Device
                    ''newAP.SubItems.Add(apReader.GetValue(2).ToString)     'OS 
                    ''newAP.SubItems.Add(apReader.GetValue(3).ToString)     'Status 
                    ''newAP.SubItems.Add(apReader.GetValue(4).ToString)     'Location 
                    ''newAP.SubItems.Add(apReader.GetValue(5).ToString)     'Notes
                    newAP.Add(New ListViewItem(New String() {apReader("SN"), apReader("Device"), apReader("OS"), apReader("Status"), apReader("Location"), apReader("Notes")}))
                    'add to list
                    ''listview_inventory_aps.Items.Add(newAP).ToString()
                    listview_inventory_aps.Items.AddRange(newAP.ToArray)
                End While
            End If
            inventory_con.Close()


            'autosize columns & header
            listview_inventory_aps.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
            listview_inventory_aps.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)

            'PREPARE SEARCH OPTION
            If Not attributes = "" Then
                'leave search options selected
            Else
                prepareSearch("AP", "yes")
            End If

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - get AP]")
        End Try

        'display toatl number of APs
        lbl_inventory_total.Text = "TOTAL (APs): " & listview_inventory_aps.Items.Count

        'backup original list for restore
        'For Each bkpItem As ListViewItem In listview_inventory_aps.Items
        ' bkpList_ap.Add(bkpItem)
        'Next

    End Sub

    'prepare search choices
    Private Sub prepareSearch(device As String, forSearch As String)

        'clear SN search
        txtbox_inventory_searchSN.Text = Nothing

        'clear OS search
        cmbbox_inventory_os.Items.Clear()
        cmbbox_inventory_os.Items.Add("")
        cmbbox_inventory_os.SelectedIndex = 0

        'clear Status search
        cmbbox_inventory_status.Items.Clear()
        cmbbox_inventory_status.Items.Add("")
        cmbbox_inventory_status.SelectedIndex = 0

        'clear Location search
        cmbbox_inventory_location.Items.Clear()
        cmbbox_inventory_location.Items.Add("")
        cmbbox_inventory_location.SelectedIndex = 0

        'clear Notes search
        cmbbox_inventory_notes.Items.Clear()
        cmbbox_inventory_notes.Items.Add("")
        cmbbox_inventory_notes.SelectedIndex = 0

        'DB vars
        Dim inventory_con As SqlConnection
        inventory_con = New SqlConnection(gdx_main.connectionString)

        'OS SEARCH
        Dim sql_os As String = "Select DISTINCT OS FROM stores_inventory WHERE Device Like '%" & device & "%'"
        Dim osReader As SqlDataReader
        Dim cmd_inv_os As SqlCommand = New SqlCommand(sql_os, inventory_con)
        Try
            inventory_con.Open()
            osReader = cmd_inv_os.ExecuteReader
            If osReader.HasRows Then
                While osReader.Read
                    cmbbox_inventory_os.Items.Add(osReader.GetValue(0))
                End While
            End If
            inventory_con.Close()
        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - search OS]")
        End Try

        'STATUS SEARCH
        Dim sql_status As String = "Select DISTINCT Status FROM stores_inventory WHERE Device Like '%" & device & "%'"
        Dim statusReader As SqlDataReader
        Dim cmd_inv_staus As SqlCommand = New SqlCommand(sql_status, inventory_con)
        Try
            inventory_con.Open()
            statusReader = cmd_inv_staus.ExecuteReader
            If statusReader.HasRows Then
                While statusReader.Read
                    cmbbox_inventory_status.Items.Add(statusReader.GetValue(0))
                End While
            End If
            inventory_con.Close()
        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - search Status]")
        End Try

        'LOCATION SEARCH
        'Dim sql_location As String = "Select DISTINCT Location FROM stores_inventory WHERE Location not like '%[0-9]%' and Device Like '%" & device & "%'"
        Dim sql_location As String = Nothing
        If forSearch = "yes" Then
            sql_location = "Select DISTINCT Location FROM stores_inventory WHERE Device Like '%" & device & "%' and Location not like '%_2%'"
        Else
            sql_location = "Select DISTINCT Location FROM stores_inventory WHERE Device Like '%" & device & "%'"
        End If

        Dim locationReader As SqlDataReader
            Dim cmd_inv_location As SqlCommand = New SqlCommand(sql_location, inventory_con)
            cmbbox_inventory_location.Items.Add("All Stores")
            Try
                inventory_con.Open()
                locationReader = cmd_inv_location.ExecuteReader
                If locationReader.HasRows Then
                    While locationReader.Read
                        cmbbox_inventory_location.Items.Add(locationReader.GetValue(0))
                    End While
                End If
                inventory_con.Close()
            Catch ex As Exception
                MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - search Location]")
            End Try

            'NOTES SEARCH
            Dim sql_notes As String = "Select DISTINCT Notes FROM stores_inventory WHERE Device Like '%" & device & "%'"
            Dim notesReader As SqlDataReader
            Dim cmd_inv_notes As SqlCommand = New SqlCommand(sql_notes, inventory_con)
            Try
                inventory_con.Open()
                notesReader = cmd_inv_notes.ExecuteReader
                If notesReader.HasRows Then
                    While notesReader.Read
                        cmbbox_inventory_notes.Items.Add(notesReader.GetValue(0))
                    End While
                End If
                inventory_con.Close()
            Catch ex As Exception
                MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - search Notes]")
            End Try

    End Sub

    'BTN: Close
    Private Sub btn_inventory_close_Click(sender As Object, e As EventArgs) Handles btn_inventory_close.Click
        MyBase.Dispose()
        listview_inventory_routers.Items.Clear()
        listview_inventory_switches.Items.Clear()
        listview_inventory_aps.Items.Clear()
    End Sub

    'ON CLOSING PRESS CLOSE BUTTON
    Private Sub inventory_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        btn_inventory_close.PerformClick()
    End Sub


    'BTN: REFRESH INVENTORY (SEARCH)
    Private Sub btn_inventory_refresh_Click(sender As Object, e As EventArgs) Handles btn_inventory_refresh.Click

        'ROUTERS INVENTORY
        If tab_inventory.SelectedTab Is tab_inventory_routers Then

            'SEARCH BY SN
            If Not txtbox_inventory_searchSN.Text = Nothing Then

                listview_inventory_routers.Items.Clear()
                getRTRinventory("and SN like '%" & txtbox_inventory_searchSN.Text & "%'")

                'SEARCH BY OS/STATUS/LOCATION/NOTES
            Else

                'Check selected search
                If Not cmbbox_inventory_os.SelectedIndex = 0 Or                 'sort OS
                   Not cmbbox_inventory_status.SelectedIndex = 0 Or             'sort STATUS
                   Not cmbbox_inventory_location.SelectedIndex = 0 Or           'sort LOCATION
                   Not cmbbox_inventory_notes.SelectedIndex = 0 Then            'sort NOTES

                    'clear old list
                    listview_inventory_routers.Items.Clear()
                    'search only stores
                    If cmbbox_inventory_location.SelectedItem = "All Stores" Then

                        getRTRinventory("and OS like '%" & cmbbox_inventory_os.SelectedItem & "%' 
                                     and Status like '%" & cmbbox_inventory_status.SelectedItem & "%' 
                                     and Location like '%[0-9]%'
                                     and Notes like '%" & cmbbox_inventory_notes.SelectedItem & "%'")
                        'search everything
                    Else
                        getRTRinventory("and OS like '%" & cmbbox_inventory_os.SelectedItem & "%' 
                                     and Status like '%" & cmbbox_inventory_status.SelectedItem & "%' 
                                     and Location = '" & cmbbox_inventory_location.SelectedItem & "'
                                     and Notes like '%" & cmbbox_inventory_notes.SelectedItem & "%'")
                    End If

                End If

            End If

            'SWITCHES INVENTORY
        ElseIf tab_inventory.SelectedTab Is tab_inventory_switches Then

            'SEARCH BY SN
            If Not txtbox_inventory_searchSN.Text = Nothing Then

                listview_inventory_switches.Items.Clear()
                getSWinventory("and SN like '%" & txtbox_inventory_searchSN.Text & "%'")

                'SEARCH BY OS/STATUS/LOCATION/NOTES
            Else

                'Check selected search
                If Not cmbbox_inventory_os.SelectedIndex = 0 Or                 'sort OS
                   Not cmbbox_inventory_status.SelectedIndex = 0 Or             'sort STATUS
                   Not cmbbox_inventory_location.SelectedIndex = 0 Or           'sort LOCATION
                   Not cmbbox_inventory_notes.SelectedIndex = 0 Then            'sort NOTES

                    'clear old list
                    listview_inventory_switches.Items.Clear()
                    'search only stores
                    If cmbbox_inventory_location.SelectedItem = "All Stores" Then

                        getSWinventory("and OS like '%" & cmbbox_inventory_os.SelectedItem & "%' 
                                     and Status like '%" & cmbbox_inventory_status.SelectedItem & "%' 
                                     and Location like '%[0-9]%'
                                     and Notes like '%" & cmbbox_inventory_notes.SelectedItem & "%'")
                        'search everything
                    Else
                        getSWinventory("and OS like '%" & cmbbox_inventory_os.SelectedItem & "%' 
                                     and Status like '%" & cmbbox_inventory_status.SelectedItem & "%' 
                                     and Location like '" & cmbbox_inventory_location.SelectedItem & "%'
                                     and Notes like '%" & cmbbox_inventory_notes.SelectedItem & "%'")
                    End If

                End If

            End If


            'APs INVENTORY
        ElseIf tab_inventory.SelectedTab Is tab_inventory_aps Then

            'SEARCH BY SN
            If Not txtbox_inventory_searchSN.Text = Nothing Then

                listview_inventory_aps.Items.Clear()
                getAPinventory("and SN like '%" & txtbox_inventory_searchSN.Text & "%'")

                'SEARCH BY OS/STATUS/LOCATION/NOTES
            Else

                'Check selected search
                If Not cmbbox_inventory_os.SelectedIndex = 0 Or                 'sort OS
                   Not cmbbox_inventory_status.SelectedIndex = 0 Or             'sort STATUS
                   Not cmbbox_inventory_location.SelectedIndex = 0 Or           'sort LOCATION
                   Not cmbbox_inventory_notes.SelectedIndex = 0 Then            'sort NOTES

                    'clear old list
                    listview_inventory_aps.Items.Clear()
                    'search only stores
                    If cmbbox_inventory_location.SelectedItem = "All Stores" Then

                        getAPinventory("and OS like '%" & cmbbox_inventory_os.SelectedItem & "%' 
                                     and Status like '%" & cmbbox_inventory_status.SelectedItem & "%' 
                                     and Location like '%[0-9]%'
                                     and Notes like '%" & cmbbox_inventory_notes.SelectedItem & "%'")
                        'search everything
                    Else
                        getAPinventory("and OS like '%" & cmbbox_inventory_os.SelectedItem & "%' 
                                     and Status like '%" & cmbbox_inventory_status.SelectedItem & "%' 
                                     and Location like '" & cmbbox_inventory_location.SelectedItem & "%'
                                     and Notes like '%" & cmbbox_inventory_notes.SelectedItem & "%'")
                    End If

                End If

            End If

        End If

    End Sub

    'DISABLE/ENABLE SEARCH OPTIONS (SN / OS_Status_Loc_Notes)
    Private Sub txtbox_inventory_searchSN_SelectedIndexChanged(sender As Object, e As EventArgs) Handles txtbox_inventory_searchSN.TextChanged

        If Not txtbox_inventory_searchSN.Text = Nothing And chkbox_inventory_adddev.Checked = False Then
            enableFields("disabled")
            cmbbox_inventory_os.SelectedIndex = 0
            cmbbox_inventory_status.SelectedIndex = 0
            cmbbox_inventory_location.SelectedIndex = 0
            cmbbox_inventory_notes.SelectedIndex = 0
        Else
            enableFields("enabled")
        End If

    End Sub

    'DISABLE/ENABLE ADD BUTTON
    Private Sub chkbox_inventory_adddev_CheckedChanged(sender As Object, e As EventArgs) Handles chkbox_inventory_adddev.CheckedChanged

        If chkbox_inventory_adddev.CheckState = CheckState.Checked Then
            btn_inventory_add.Enabled = True
            btn_inventory_refresh.Enabled = False
        Else
            btn_inventory_add.Enabled = False
            btn_inventory_refresh.Enabled = True
        End If

    End Sub


    'DISABLE/ENABLE SEARCH OPTIONS (SN / OS)
    Private Sub cmbbox_inventory_os_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_inventory_os.SelectedIndexChanged
        If Not cmbbox_inventory_os.SelectedIndex = 0 Or
           Not cmbbox_inventory_status.SelectedIndex = 0 Or
           Not cmbbox_inventory_location.SelectedIndex = 0 Or
           Not cmbbox_inventory_notes.SelectedIndex = 0 Then
            txtbox_inventory_searchSN.Enabled = False
        Else
            txtbox_inventory_searchSN.Enabled = True
        End If

        'selectedOS = cmbbox_inventory_os.SelectedItem

    End Sub

    'DISABLE/ENABLE SEARCH OPTIONS (SN / Status)
    Private Sub cmbbox_inventory_status_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_inventory_status.SelectedIndexChanged
        If Not cmbbox_inventory_os.SelectedIndex = 0 Or
           Not cmbbox_inventory_status.SelectedIndex = 0 Or
           Not cmbbox_inventory_location.SelectedIndex = 0 Or
           Not cmbbox_inventory_notes.SelectedIndex = 0 Then
            txtbox_inventory_searchSN.Enabled = False
        Else
            txtbox_inventory_searchSN.Enabled = True
        End If
    End Sub

    'DISABLE/ENABLE SEARCH OPTIONS (SN / Location)
    Private Sub cmbbox_inventory_Location_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_inventory_location.SelectedIndexChanged
        If Not cmbbox_inventory_os.SelectedIndex = 0 Or
           Not cmbbox_inventory_status.SelectedIndex = 0 Or
           Not cmbbox_inventory_location.SelectedIndex = 0 Or
           Not cmbbox_inventory_notes.SelectedIndex = 0 Then
            txtbox_inventory_searchSN.Enabled = False
        Else
            txtbox_inventory_searchSN.Enabled = True
        End If
        locationFlag = True
    End Sub

    'DISABLE/ENABLE SEARCH OPTIONS (SN / Notes)
    Private Sub cmbbox_inventory_Notes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_inventory_notes.SelectedIndexChanged
        If Not cmbbox_inventory_os.SelectedIndex = 0 Or
           Not cmbbox_inventory_status.SelectedIndex = 0 Or
           Not cmbbox_inventory_location.SelectedIndex = 0 Or
           Not cmbbox_inventory_notes.SelectedIndex = 0 Then
            txtbox_inventory_searchSN.Enabled = False
        Else
            txtbox_inventory_searchSN.Enabled = True
        End If
    End Sub

    'BTN: CLEAR SEARCH CHOICES
    Private Sub btn_inventory_clear_Click(sender As Object, e As EventArgs) Handles btn_inventory_clear.Click

        'enable imput dropdown
        cmbbox_inventory_os.DropDownStyle = ComboBoxStyle.DropDown
        cmbbox_inventory_status.DropDownStyle = ComboBoxStyle.DropDown
        cmbbox_inventory_location.DropDownStyle = ComboBoxStyle.DropDown
        cmbbox_inventory_notes.DropDownStyle = ComboBoxStyle.DropDown

        'set search criteria to nothing
        txtbox_inventory_searchSN.Text = Nothing
        cmbbox_inventory_os.SelectedIndex = 0
        cmbbox_inventory_status.SelectedIndex = 0
        cmbbox_inventory_location.SelectedIndex = 0
        cmbbox_inventory_notes.SelectedIndex = 0

        listview_inventory_routers.Items.Clear()
        listview_inventory_switches.Items.Clear()
        listview_inventory_aps.Items.Clear()

        'set ENABLE ADD TO OFF
        chkbox_inventory_adddev.CheckState = False

        'set save btn to off
        btn_inventory_save.Enabled = False

        'restore original inventory list
        If tab_inventory.SelectedTab Is tab_inventory_routers Then
            getRTRinventory("")
        ElseIf tab_inventory.SelectedTab Is tab_inventory_switches Then
            getSWinventory("")
        ElseIf tab_inventory.SelectedTab Is tab_inventory_aps Then
            getAPinventory("")
        End If

        'clear flag
        locationFlag = False
    End Sub

    'BTN: EDIT INVENTORY
    Private Sub btn_inventory_save_Click(sender As Object, e As EventArgs) Handles btn_inventory_save.Click

        errorAlert = Nothing

        'CHECK OS
        If cmbbox_inventory_os.Text = "" Then
            errorAlert = errorAlert & "Missing OS." & vbNewLine
        Else
            errorAlert = errorAlert & ""
        End If

        'CHECK STATUS
        If cmbbox_inventory_status.Text = "" Then
            errorAlert = errorAlert & "Missing Status." & vbNewLine
        Else
            errorAlert = errorAlert & ""
        End If

        'CHECK Location
        If cmbbox_inventory_location.Text = "" Then
            errorAlert = errorAlert & "Missing Location." & vbNewLine
        ElseIf cmbbox_inventory_location.SelectedItem = "All Stores" Then
            errorAlert = errorAlert & "Unable to add this device to all stores." & vbNewLine
        Else
            errorAlert = errorAlert & ""
        End If

        'CHECK Notes
        If cmbbox_inventory_notes.Text = "" Then
            errorAlert = errorAlert & "Missing Notes." & vbNewLine
        Else
            errorAlert = errorAlert & ""
        End If

        'CHECK IF RTR already installed in store
        For Each searchLocation As ListViewItem In listview_inventory_routers.Items
            If searchLocation.SubItems(4).Text.Equals(cmbbox_inventory_location.Text) AndAlso
                searchLocation.SubItems(5).Text = "INSTALLED" AndAlso
                cmbbox_inventory_notes.SelectedItem = "INSTALLED" Then

                If locationFlag = True Then
                    errorAlert = errorAlert & "Device already INSTALLED at this location." & vbNewLine & "Please retire INSTALLED device first, then INSTALL new one." &
                                                                                         vbNewLine & "Advise network team to make changes in Orion."
                Else
                    errorAlert = errorAlert & ""
                End If
            End If
        Next

        'CHECK IF SW already installed in store
        For Each searchLocation As ListViewItem In listview_inventory_switches.Items
            If searchLocation.SubItems(4).Text.Equals(cmbbox_inventory_location.Text) AndAlso
                searchLocation.SubItems(5).Text = "INSTALLED" AndAlso
                cmbbox_inventory_notes.SelectedItem = "INSTALLED" Then

                If locationFlag = True Then
                    errorAlert = errorAlert & "Device already INSTALLED at this location." & vbNewLine & "Please retire INSTALLED device first, then INSTALL new one." &
                                                                                         vbNewLine & "Advise network team to make changes in Orion."
                Else
                    errorAlert = errorAlert & ""
                End If
            End If
        Next

        'CHECK IF AP already installed in store
        For Each searchLocation As ListViewItem In listview_inventory_aps.Items
            If searchLocation.SubItems(4).Text.Equals(cmbbox_inventory_location.Text) AndAlso
                searchLocation.SubItems(5).Text = "INSTALLED" AndAlso
                cmbbox_inventory_notes.SelectedItem = "INSTALLED" Then

                If locationFlag = True Then
                    errorAlert = errorAlert & "Device already INSTALLED at this location." & vbNewLine & "Please retire INSTALLED device first, then INSTALL new one." &
                                                                                         vbNewLine & "Advise network team to make changes in Orion."
                Else
                    errorAlert = errorAlert & ""
                End If
            End If
        Next

        'ADD Device or show error
        If Not errorAlert = "" Then
            MsgBox("Please enter a valid SN!" & vbNewLine & errorAlert, MsgBoxStyle.Information, Title:="GDnetworks - Info! [Inventory - ADD Device]")
        Else
            editDevice(selectedSN, selectedDevice, cmbbox_inventory_os.SelectedItem, cmbbox_inventory_status.SelectedItem, cmbbox_inventory_location.SelectedItem, cmbbox_inventory_notes.SelectedItem)

            'enable combobox imput
            cmbbox_inventory_os.DropDownStyle = ComboBoxStyle.DropDown
            cmbbox_inventory_status.DropDownStyle = ComboBoxStyle.DropDown
            cmbbox_inventory_location.DropDownStyle = ComboBoxStyle.DropDown
            cmbbox_inventory_notes.DropDownStyle = ComboBoxStyle.DropDown

            btn_inventory_clear.PerformClick()
        End If

    End Sub

    'BTN: ADD TO INVENTORY
    Private Sub btn_inventory_add_Click(sender As Object, e As EventArgs) Handles btn_inventory_add.Click

        errorAlert = Nothing

        'CHECK SN
        If txtbox_inventory_searchSN.Text = "" Then
            errorAlert = "Missing SN." & vbNewLine
        Else
            errorAlert = ""
        End If

        'CHECK IF RTR EXISTS
        For Each searchItem As ListViewItem In listview_inventory_routers.Items
            If searchItem.Text.Equals(txtbox_inventory_searchSN.Text) Then
                errorAlert = errorAlert & "SN already exists." & vbNewLine
                txtbox_inventory_searchSN.Enabled = True
            End If
        Next

        'CHECK IF SW EXISTS
        For Each searchItem As ListViewItem In listview_inventory_switches.Items
            If searchItem.Text.Equals(txtbox_inventory_searchSN.Text) Then
                errorAlert = errorAlert & "SN already exists." & vbNewLine
                txtbox_inventory_searchSN.Enabled = True
            End If
        Next

        'CHECK IF RTR EXISTS
        For Each searchItem As ListViewItem In listview_inventory_aps.Items
            If searchItem.Text.Equals(txtbox_inventory_searchSN.Text) Then
                errorAlert = errorAlert & "SN already exists." & vbNewLine
                txtbox_inventory_searchSN.Enabled = True
            End If
        Next

        'CHECK OS
        If cmbbox_inventory_os.Text = "" Then
            errorAlert = errorAlert & "Missing OS." & vbNewLine
        Else
            errorAlert = errorAlert & ""
        End If

        'CHECK STATUS
        If cmbbox_inventory_status.Text = "" Then
            errorAlert = errorAlert & "Missing Status." & vbNewLine
        Else
            errorAlert = errorAlert & ""
        End If

        'CHECK Location
        If cmbbox_inventory_location.Text = "" Then
            errorAlert = errorAlert & "Missing Location." & vbNewLine
        ElseIf cmbbox_inventory_location.SelectedItem = "All Stores" Then
            errorAlert = errorAlert & "Unable to add this device to all stores." & vbNewLine
        Else
            errorAlert = errorAlert & ""
        End If

        'CHECK Notes
        If cmbbox_inventory_notes.Text = "" Then
            errorAlert = errorAlert & "Missing Notes." & vbNewLine
        Else
            errorAlert = errorAlert & ""
        End If

        'CHECK IF RTR already installed in store
        For Each searchLocation As ListViewItem In listview_inventory_routers.Items
            If searchLocation.SubItems(4).Text.Equals(cmbbox_inventory_location.Text) AndAlso
                searchLocation.SubItems(5).Text = "INSTALLED" AndAlso
                cmbbox_inventory_notes.SelectedItem = "INSTALLED" Then
                errorAlert = errorAlert & "Device already INSTALLED at this location." & vbNewLine & "Please retire INSTALLED device first, then INSTALL new one." &
                                                                                         vbNewLine & "Advise network team to make changes in Orion."
            End If
        Next

        'CHECK IF SW already installed in store
        For Each searchLocation As ListViewItem In listview_inventory_switches.Items
            If searchLocation.SubItems(4).Text.Equals(cmbbox_inventory_location.Text) AndAlso
                searchLocation.SubItems(5).Text = "INSTALLED" AndAlso
                cmbbox_inventory_notes.SelectedItem = "INSTALLED" Then
                errorAlert = errorAlert & "Device already INSTALLED at this location." & vbNewLine & "Please retire INSTALLED device first, then INSTALL new one." &
                                                                                         vbNewLine & "Advise network team to make changes in Orion."
            End If
        Next

        'CHECK IF AP already installed in store
        For Each searchLocation As ListViewItem In listview_inventory_aps.Items
            If searchLocation.SubItems(4).Text.Equals(cmbbox_inventory_location.Text) AndAlso
                searchLocation.SubItems(5).Text = "INSTALLED" AndAlso
                cmbbox_inventory_notes.SelectedItem = "INSTALLED" Then
                errorAlert = errorAlert & "Device already INSTALLED at this location." & vbNewLine & "Please retire INSTALLED device first, then INSTALL new one." &
                                                                                         vbNewLine & "Advise network team to make changes in Orion."
            End If
        Next

        'ADD Device or show error
        If Not errorAlert = "" Then
            MsgBox("Please enter a valid SN!" & vbNewLine & errorAlert, MsgBoxStyle.Information, Title:="GDnetworks - Info! [Inventory - ADD Device]")
        Else
            addDevice(txtbox_inventory_searchSN.Text, selectedDevice, cmbbox_inventory_os.Text,
                      cmbbox_inventory_status.Text, cmbbox_inventory_location.Text, cmbbox_inventory_notes.Text)
            btn_inventory_clear.PerformClick()
        End If

    End Sub

    'BTN: EXPORT
    Private Sub btn_inventory_export_Click(sender As Object, e As EventArgs) Handles btn_inventory_export.Click

        Dim sfd As New SaveFileDialog With {
        .Title = "Select file location",
        .FileName = "GDnReports_Inventory.csv",
        .Filter = "CSV (*.csv)|*.csv",
        .FilterIndex = 0,
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}

        'save CSV fIle 

        If sfd.ShowDialog = DialogResult.OK Then

            'selected list
            Dim deviceList As ListView = Nothing
            If selectedDevice = "RTR" Then
                deviceList = listview_inventory_routers
            ElseIf selectedDevice = "SW" Then
                deviceList = listview_inventory_switches
            ElseIf selectedDevice = "AP" Then
                deviceList = listview_inventory_aps
            End If

            'headers
            Dim headers = (From ch In deviceList.Columns Let header = DirectCast(ch, ColumnHeader) Select header.Text).ToArray()
            'items
            Dim items() = (From item In deviceList.Items Let lvi = DirectCast(item, ListViewItem) Select (From subitem In lvi.SubItems
                                                                                                          Let si = DirectCast(subitem, ListViewItem.ListViewSubItem)
                                                                                                          Select si.Text).ToArray()).ToArray()
            'create table
            Dim table As String = String.Join(",", headers) & Environment.NewLine

            For Each a In items
                table &= String.Join(",", a) & Environment.NewLine
            Next
            'output
            table = table.TrimEnd(CChar(vbCr), CChar(vbLf))
            IO.File.WriteAllText(sfd.FileName, table)

        End If
    End Sub

    'BTN: Get latest updates from Orion
    Private Sub btn_inventory_updateOrion_Click(sender As Object, e As EventArgs) Handles btn_inventory_updateOrion.Click

        Dim result As Integer = MsgBox("Are you sure you want to update the inventory DB?", MsgBoxStyle.YesNo, Title:="GDnetworks - Update Inventory")

        If result = DialogResult.No Then
            'do nothing
        Else

            Dim sql_edit As String = ""
            '"update stores_inventory set Device = '" & Device & "', 
            '                                              OS = '" & OS & "', 
            '                                              Status = '" & Status & "', 
            '                                              Location = '" & Location & "', 
            '                                              Notes = '" & Notes & "' where SN = '" & SN & "'"

            Dim cmd_inv_edit As SqlCommand = New SqlCommand(sql_edit, inventory_con)

            Try
                inventory_con.Open()
                cmd_inv_edit.ExecuteNonQuery()
                inventory_con.Close()

                'MsgBox("Edited device " & SN & " with the following info:" & vbNewLine &
                '              "Device: " & Device & vbNewLine &
                '              "OS: " & OS & vbNewLine &
                '              "Status: " & Status & vbNewLine &
                '              "Location: " & Location & vbNewLine &
                '              "Notes: " & Notes & vbNewLine, MsgBoxStyle.Information, Title:="GDnetworks - Info! [DB: stores_inventory - Orion update]")

            Catch ex As Exception
                MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - Orion update]")
            End Try

        End If

    End Sub

    'ADD NEW DEVICE
    Private Function addDevice(SN As String, Device As String, OS As String, Status As String, Location As String, Notes As String)


        Dim sql_add As String = "Insert into stores_inventory (SN, Device, OS, Status, Location, Notes) 
                                                       VALUES ('" & SN & "', '" & Device & "', '" & OS & "', 
                                                       '" & Status & "', '" & Location & "', '" & Notes & "')"

        Dim cmd_inv As SqlCommand = New SqlCommand(sql_add, inventory_con)

        Try
            inventory_con.Open()
            cmd_inv.ExecuteNonQuery()
            inventory_con.Close()


            Return MsgBox("Added device " & SN & " with the following info:" & vbNewLine &
                          "Device: " & Device & vbNewLine &
                          "OS: " & OS & vbNewLine &
                          "Status: " & Status & vbNewLine &
                          "Location: " & Location & vbNewLine &
                          "Notes: " & Notes & vbNewLine, MsgBoxStyle.Information, Title:="GDnetworks - Info! [DB: stores_inventory - add device]")

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - add device]")
        End Try

    End Function

    'EDIT DEVICE
    Private Function editDevice(SN As String, Device As String, OS As String, Status As String, Location As String, Notes As String)

        Dim sql_edit As String = "update stores_inventory set Device = '" & Device & "', 
                                                              OS = '" & OS & "', 
                                                              Status = '" & Status & "', 
                                                              Location = '" & Location & "', 
                                                              Notes = '" & Notes & "' where SN = '" & SN & "'"

        Dim cmd_inv_edit As SqlCommand = New SqlCommand(sql_edit, inventory_con)

        Try
            inventory_con.Open()
            cmd_inv_edit.ExecuteNonQuery()
            inventory_con.Close()

            Return MsgBox("Edited device " & SN & " with the following info:" & vbNewLine &
                          "Device: " & Device & vbNewLine &
                          "OS: " & OS & vbNewLine &
                          "Status: " & Status & vbNewLine &
                          "Location: " & Location & vbNewLine &
                          "Notes: " & Notes & vbNewLine, MsgBoxStyle.Information, Title:="GDnetworks - Info! [DB: stores_inventory - add device]")

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - edit device]")
        End Try

    End Function

    'DELETE DEVICE
    Private Function deleteDevice(device As String)

        Dim sql_delete As String = "Delete FROM stores_inventory WHERE SN = '" & device & "'"
        Dim cmd_inv As SqlCommand = New SqlCommand(sql_delete, inventory_con)

        Try
            inventory_con.Open()
            cmd_inv.ExecuteNonQuery()
            inventory_con.Close()

            Return MsgBox(device & " deleted from inventory!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [DB: stores_inventory - delete device]")

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stores_inventory - delete device]")
        End Try

    End Function

    'CONTEXTMENU ( RTR LIST RIGHT-CLICK OPTIONS )
    Private Sub listview_inventory_routers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles listview_inventory_routers.SelectedIndexChanged
        Me.listview_inventory_routers.ContextMenuStrip = Nothing
        If Me.listview_inventory_routers.SelectedItems.Count > 0 Then
            Me.listview_inventory_routers.ContextMenuStrip = Me.contextmenu_inventory_list
            selectedSN = listview_inventory_routers.SelectedItems(0).Text
            selectedOS = listview_inventory_routers.SelectedItems(0).SubItems(2).Text
            selectedStatus = listview_inventory_routers.SelectedItems(0).SubItems(3).Text
            selectedLocation = listview_inventory_routers.SelectedItems(0).SubItems(4).Text
            selectedNotes = listview_inventory_routers.SelectedItems(0).SubItems(5).Text
        End If
    End Sub

    'CONTEXTMENU ( SW LIST RIGHT-CLICK OPTIONS )
    Private Sub listview_inventory_switches_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles listview_inventory_switches.SelectedIndexChanged
        Me.listview_inventory_switches.ContextMenuStrip = Nothing
        If Me.listview_inventory_switches.SelectedItems.Count > 0 Then
            Me.listview_inventory_switches.ContextMenuStrip = Me.contextmenu_inventory_list
            selectedSN = listview_inventory_switches.SelectedItems(0).Text
            selectedOS = listview_inventory_switches.SelectedItems(0).SubItems(2).Text
            selectedStatus = listview_inventory_switches.SelectedItems(0).SubItems(3).Text
            selectedLocation = listview_inventory_switches.SelectedItems(0).SubItems(4).Text
            selectedNotes = listview_inventory_switches.SelectedItems(0).SubItems(5).Text
        End If
    End Sub

    'CONTEXTMENU ( AP LIST RIGHT-CLICK OPTIONS )
    Private Sub listview_inventory_aps_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles listview_inventory_aps.SelectedIndexChanged
        Me.listview_inventory_aps.ContextMenuStrip = Nothing
        If Me.listview_inventory_aps.SelectedItems.Count > 0 Then
            Me.listview_inventory_aps.ContextMenuStrip = Me.contextmenu_inventory_list
            selectedSN = listview_inventory_aps.SelectedItems(0).Text
            selectedOS = listview_inventory_aps.SelectedItems(0).SubItems(2).Text
            selectedStatus = listview_inventory_aps.SelectedItems(0).SubItems(3).Text
            selectedLocation = listview_inventory_aps.SelectedItems(0).SubItems(4).Text
            selectedNotes = listview_inventory_aps.SelectedItems(0).SubItems(5).Text
        End If
    End Sub

    'CONTEXTMENU ( COPY )
    Private Sub context_inventory_copy_Click(sender As Object, e As EventArgs) Handles context_inventory_copy.Click

        My.Computer.Clipboard.SetText(selectedSN.ToString())

    End Sub

    'CONTEXTMENU ( EDIT )
    Private Sub context_inventory_edit_Click(sender As Object, e As EventArgs) Handles context_inventory_edit.Click

        prepareSearch(selectedDevice, "no")

        'disable combobox imput
        cmbbox_inventory_os.DropDownStyle = ComboBoxStyle.DropDownList
        cmbbox_inventory_status.DropDownStyle = ComboBoxStyle.DropDownList
        cmbbox_inventory_location.DropDownStyle = ComboBoxStyle.DropDownList
        cmbbox_inventory_notes.DropDownStyle = ComboBoxStyle.DropDownList

        'set vars for edit/imput
        txtbox_inventory_searchSN.Text = selectedSN
        cmbbox_inventory_os.SelectedItem = selectedOS
        cmbbox_inventory_status.SelectedItem = selectedStatus
        cmbbox_inventory_location.SelectedItem = selectedLocation
        cmbbox_inventory_notes.SelectedItem = selectedNotes

        txtbox_inventory_searchSN.Enabled = False
        enableFields("enabled")

        btn_inventory_save.Enabled = True

        locationFlag = False

    End Sub

    'CONTEXTMENU ( DElete )
    Private Sub context_inventory_delete_Click(sender As Object, e As EventArgs) Handles context_inventory_delete.Click

        If tab_inventory.SelectedTab Is tab_inventory_routers Then

            Dim delete_rtr_dev As Integer = MsgBox("Do you want to delete the following device: " & selectedSN.ToString() & " ?", MsgBoxStyle.YesNo, Title:="GDnetworks - info! (delete RTR)")
            If delete_rtr_dev = DialogResult.No Then
                'DO NOT DELETE
            Else
                'DELETE
                deleteDevice(selectedSN)
                listview_inventory_routers.Items.Clear()
                getRTRinventory("")
            End If

        ElseIf tab_inventory.SelectedTab Is tab_inventory_switches Then

            Dim delete_sw_dev As Integer = MsgBox("Do you want to delete the following device: " & selectedSN.ToString() & " ?", MsgBoxStyle.YesNo, Title:="GDnetworks - info! (delete SW)")
            If delete_sw_dev = DialogResult.No Then
                'DO NOT DELETE
            Else
                'DELETE
                deleteDevice(selectedSN)
                listview_inventory_switches.Items.Clear()
                getSWinventory("")
            End If

        ElseIf tab_inventory.SelectedTab Is tab_inventory_aps Then
            Dim delete_ap_dev As Integer = MsgBox("Do you want to delete the following device: " & selectedSN.ToString() & " ?", MsgBoxStyle.YesNo, Title:="GDnetworks - info! (delete AP)")
            If delete_ap_dev = DialogResult.No Then
                'DO NOT DELETE
            Else
                'DELETE
                deleteDevice(selectedSN)
                listview_inventory_aps.Items.Clear()
                getAPinventory("")
            End If
        End If

    End Sub

    'enable/disable search fields 
    Private Sub enableFields(status As String)

        If status = "disabled" Then

            cmbbox_inventory_os.Enabled = False
            cmbbox_inventory_status.Enabled = False
            cmbbox_inventory_location.Enabled = False
            cmbbox_inventory_notes.Enabled = False

        ElseIf status = "enabled" Then

            cmbbox_inventory_os.Enabled = True
            cmbbox_inventory_status.Enabled = True
            cmbbox_inventory_location.Enabled = True
            cmbbox_inventory_notes.Enabled = True

        End If

    End Sub

    'SORT RTR LIST USING admin_users_compare CLASS
    Private Sub listview_inventory_routers_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles listview_inventory_routers.ColumnClick

        Dim sortColumn As Integer = -1

        If e.Column <> sortColumn Then
            ' Set the sort column to the new column.
            sortColumn = e.Column
            ' Set the sort order to ascending by default.
            listview_inventory_routers.Sorting = SortOrder.Ascending
        Else
            ' Determine what the last sort order was and change it.
            If listview_inventory_routers.Sorting = SortOrder.Ascending Then
                listview_inventory_routers.Sorting = SortOrder.Descending
            Else
                listview_inventory_routers.Sorting = SortOrder.Ascending
            End If
        End If

        listview_inventory_routers.ListViewItemSorter = New ListViewItemCompare(e.Column, listview_inventory_routers.Sorting)
        listview_inventory_routers.Sort()
    End Sub

    'SORT SW LIST USING admin_users_compare CLASS
    Private Sub listview_inventory_switches_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles listview_inventory_switches.ColumnClick

        Dim sortColumn As Integer = -1

        If e.Column <> sortColumn Then
            ' Set the sort column to the new column.
            sortColumn = e.Column
            ' Set the sort order to ascending by default.
            listview_inventory_switches.Sorting = SortOrder.Ascending
        Else
            ' Determine what the last sort order was and change it.
            If listview_inventory_switches.Sorting = SortOrder.Ascending Then
                listview_inventory_switches.Sorting = SortOrder.Descending
            Else
                listview_inventory_switches.Sorting = SortOrder.Ascending
            End If
        End If

        listview_inventory_switches.ListViewItemSorter = New ListViewItemCompare(e.Column, listview_inventory_switches.Sorting)
        listview_inventory_switches.Sort()
    End Sub

    'SORT AP LIST USING admin_users_compare CLASS
    Private Sub listview_inventory_aps_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles listview_inventory_aps.ColumnClick

        Dim sortColumn As Integer = -1

        If e.Column <> sortColumn Then
            ' Set the sort column to the new column.
            sortColumn = e.Column
            ' Set the sort order to ascending by default.
            listview_inventory_aps.Sorting = SortOrder.Ascending
        Else
            ' Determine what the last sort order was and change it.
            If listview_inventory_aps.Sorting = SortOrder.Ascending Then
                listview_inventory_aps.Sorting = SortOrder.Descending
            Else
                listview_inventory_aps.Sorting = SortOrder.Ascending
            End If
        End If

        listview_inventory_aps.ListViewItemSorter = New ListViewItemCompare(e.Column, listview_inventory_aps.Sorting)
        listview_inventory_aps.Sort()
    End Sub


End Class