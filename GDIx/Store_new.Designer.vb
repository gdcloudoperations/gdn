﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Store_new
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Store_new))
        Me.lbl_ns_storeno = New System.Windows.Forms.Label()
        Me.grpbox_ns_store = New System.Windows.Forms.GroupBox()
        Me.ns_star29 = New System.Windows.Forms.Label()
        Me.cmbbox_ns_store_system = New System.Windows.Forms.ComboBox()
        Me.lbl_ns_store_system = New System.Windows.Forms.Label()
        Me.ns_star4 = New System.Windows.Forms.Label()
        Me.txtbox_ns_faxline_load = New System.Windows.Forms.TextBox()
        Me.ns_star10 = New System.Windows.Forms.Label()
        Me.txtbox_ns_boline_load = New System.Windows.Forms.TextBox()
        Me.ns_star9 = New System.Windows.Forms.Label()
        Me.ns_star3 = New System.Windows.Forms.Label()
        Me.ns_star8 = New System.Windows.Forms.Label()
        Me.lbl_ns_str_faxline = New System.Windows.Forms.Label()
        Me.ns_star7 = New System.Windows.Forms.Label()
        Me.lbl_ns_str_boline = New System.Windows.Forms.Label()
        Me.ns_star6 = New System.Windows.Forms.Label()
        Me.cmbbox_ns_iscombo_load = New System.Windows.Forms.ComboBox()
        Me.ns_star5 = New System.Windows.Forms.Label()
        Me.lbl_ns_iscombo = New System.Windows.Forms.Label()
        Me.ns_star2 = New System.Windows.Forms.Label()
        Me.ns_star1 = New System.Windows.Forms.Label()
        Me.txtbox_ns_mainline_load = New System.Windows.Forms.TextBox()
        Me.cmbbox_ns_country_load = New System.Windows.Forms.ComboBox()
        Me.txtbox_ns_pcode_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_city_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_address_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_mall_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_sisstore_load = New System.Windows.Forms.TextBox()
        Me.cmbbox_ns_banner_load = New System.Windows.Forms.ComboBox()
        Me.lbl_ns_str_ph = New System.Windows.Forms.Label()
        Me.lbl_ns_str_cntry = New System.Windows.Forms.Label()
        Me.lbl_ns_str_pcode = New System.Windows.Forms.Label()
        Me.lbl_ns_str_prov = New System.Windows.Forms.Label()
        Me.lbl_ns_str_city = New System.Windows.Forms.Label()
        Me.lbl_ns_str_add = New System.Windows.Forms.Label()
        Me.lbl_ns_str_mall = New System.Windows.Forms.Label()
        Me.lbll_ns_str_sisstr = New System.Windows.Forms.Label()
        Me.lbl_ns_str_banner = New System.Windows.Forms.Label()
        Me.txtbox_ns_storeno_load = New System.Windows.Forms.TextBox()
        Me.lbl_new_store_info = New System.Windows.Forms.Label()
        Me.nb_star0 = New System.Windows.Forms.Label()
        Me.grpbox_ns_operation_info = New System.Windows.Forms.GroupBox()
        Me.ns_star23 = New System.Windows.Forms.Label()
        Me.cmbbox_ns_location_load = New System.Windows.Forms.ComboBox()
        Me.lbl_ns_location = New System.Windows.Forms.Label()
        Me.ns_star22 = New System.Windows.Forms.Label()
        Me.ns_star21 = New System.Windows.Forms.Label()
        Me.cmbbox_ns_status_load = New System.Windows.Forms.ComboBox()
        Me.lbl_ns_status = New System.Windows.Forms.Label()
        Me.cmbbox_ns_isremote_load = New System.Windows.Forms.ComboBox()
        Me.lbl_ns_isremote = New System.Windows.Forms.Label()
        Me.txtbox_ns_dslline_load = New System.Windows.Forms.TextBox()
        Me.lbl_ns_str_dslline = New System.Windows.Forms.Label()
        Me.grpbox_ns_network_info = New System.Windows.Forms.GroupBox()
        Me.txtbox_ns_ispaccount_load = New System.Windows.Forms.TextBox()
        Me.lbl_ns_ispaccount = New System.Windows.Forms.Label()
        Me.ns_star17 = New System.Windows.Forms.Label()
        Me.ns_star16 = New System.Windows.Forms.Label()
        Me.ns_star15 = New System.Windows.Forms.Label()
        Me.ns_star14 = New System.Windows.Forms.Label()
        Me.ns_star13 = New System.Windows.Forms.Label()
        Me.ns_star12 = New System.Windows.Forms.Label()
        Me.txtbox_ns_ispgateway_load = New System.Windows.Forms.TextBox()
        Me.lbl_ns_ispgateway = New System.Windows.Forms.Label()
        Me.txtbox_ns_ispsubnet_load = New System.Windows.Forms.TextBox()
        Me.lbl_ns_ispsubnet = New System.Windows.Forms.Label()
        Me.txtbox_ns_ispaddress_load = New System.Windows.Forms.TextBox()
        Me.lbl_ns_ispaddress = New System.Windows.Forms.Label()
        Me.cmbbox_ns_ispstatus_load = New System.Windows.Forms.ComboBox()
        Me.lbl_ns_ispstatus = New System.Windows.Forms.Label()
        Me.ns_star18 = New System.Windows.Forms.Label()
        Me.cmbbox_ns_rlocation_load = New System.Windows.Forms.ComboBox()
        Me.txtbox_ns_cablock_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_wifi_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_speed_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_pwd_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_user_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_isp_load = New System.Windows.Forms.TextBox()
        Me.txtbox_ns_ip_load = New System.Windows.Forms.TextBox()
        Me.lbl_ns_cabinet_lock = New System.Windows.Forms.Label()
        Me.lbl_rtr_location = New System.Windows.Forms.Label()
        Me.lbl_ns_wifispeed = New System.Windows.Forms.Label()
        Me.lbl_ns_speed = New System.Windows.Forms.Label()
        Me.lbl_ns_password = New System.Windows.Forms.Label()
        Me.ns_star19 = New System.Windows.Forms.Label()
        Me.ns_star11 = New System.Windows.Forms.Label()
        Me.lbl_ns_username = New System.Windows.Forms.Label()
        Me.lbl_ns_isp = New System.Windows.Forms.Label()
        Me.lbl_ns_ip = New System.Windows.Forms.Label()
        Me.lbl_ns_main = New System.Windows.Forms.Label()
        Me.btn_ns_cancel = New System.Windows.Forms.Button()
        Me.btn_ns_save = New System.Windows.Forms.Button()
        Me.btn_ns_clear = New System.Windows.Forms.Button()
        Me.lbl_ns_spare_modem = New System.Windows.Forms.Label()
        Me.cmbbox_ns_smodem_load = New System.Windows.Forms.ComboBox()
        Me.lbl_ns_spare_router = New System.Windows.Forms.Label()
        Me.cmbbox_ns_srouter_load = New System.Windows.Forms.ComboBox()
        Me.lbl_ns_spare_wireless_card = New System.Windows.Forms.Label()
        Me.cmbbox_ns_swc_load = New System.Windows.Forms.ComboBox()
        Me.ns_star24 = New System.Windows.Forms.Label()
        Me.ns_star25 = New System.Windows.Forms.Label()
        Me.ns_star26 = New System.Windows.Forms.Label()
        Me.lbl_ns_spare_holiday_cash = New System.Windows.Forms.Label()
        Me.lbl_ns_spare_pinpad = New System.Windows.Forms.Label()
        Me.cmbbox_ns_hpc_load = New System.Windows.Forms.ComboBox()
        Me.cmbbox_ns_sppad_load = New System.Windows.Forms.ComboBox()
        Me.ns_star27 = New System.Windows.Forms.Label()
        Me.ns_star28 = New System.Windows.Forms.Label()
        Me.cmbbox_ns_spare_equip = New System.Windows.Forms.GroupBox()
        Me.grpbox_ns_ip_pinpads = New System.Windows.Forms.GroupBox()
        Me.ns_star30 = New System.Windows.Forms.Label()
        Me.cmbbox_ns_pinpad_load = New System.Windows.Forms.ComboBox()
        Me.lbl_ns_pinpads = New System.Windows.Forms.Label()
        Me.cmbbox_ns_province_load = New System.Windows.Forms.ComboBox()
        Me.grpbox_ns_store.SuspendLayout()
        Me.grpbox_ns_operation_info.SuspendLayout()
        Me.grpbox_ns_network_info.SuspendLayout()
        Me.cmbbox_ns_spare_equip.SuspendLayout()
        Me.grpbox_ns_ip_pinpads.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_ns_storeno
        '
        Me.lbl_ns_storeno.AutoSize = True
        Me.lbl_ns_storeno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_storeno.Location = New System.Drawing.Point(14, 25)
        Me.lbl_ns_storeno.Name = "lbl_ns_storeno"
        Me.lbl_ns_storeno.Size = New System.Drawing.Size(67, 15)
        Me.lbl_ns_storeno.TabIndex = 0
        Me.lbl_ns_storeno.Text = "Store No:"
        '
        'grpbox_ns_store
        '
        Me.grpbox_ns_store.Controls.Add(Me.cmbbox_ns_province_load)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star29)
        Me.grpbox_ns_store.Controls.Add(Me.cmbbox_ns_store_system)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_store_system)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star4)
        Me.grpbox_ns_store.Controls.Add(Me.txtbox_ns_faxline_load)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star10)
        Me.grpbox_ns_store.Controls.Add(Me.txtbox_ns_boline_load)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star9)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star3)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star8)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_faxline)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star7)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_cntry)
        Me.grpbox_ns_store.Controls.Add(Me.cmbbox_ns_country_load)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_boline)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star6)
        Me.grpbox_ns_store.Controls.Add(Me.cmbbox_ns_iscombo_load)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star5)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_iscombo)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star2)
        Me.grpbox_ns_store.Controls.Add(Me.ns_star1)
        Me.grpbox_ns_store.Controls.Add(Me.txtbox_ns_mainline_load)
        Me.grpbox_ns_store.Controls.Add(Me.txtbox_ns_pcode_load)
        Me.grpbox_ns_store.Controls.Add(Me.txtbox_ns_city_load)
        Me.grpbox_ns_store.Controls.Add(Me.txtbox_ns_address_load)
        Me.grpbox_ns_store.Controls.Add(Me.txtbox_ns_mall_load)
        Me.grpbox_ns_store.Controls.Add(Me.txtbox_ns_sisstore_load)
        Me.grpbox_ns_store.Controls.Add(Me.cmbbox_ns_banner_load)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_ph)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_pcode)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_prov)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_city)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_add)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_mall)
        Me.grpbox_ns_store.Controls.Add(Me.lbll_ns_str_sisstr)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_str_banner)
        Me.grpbox_ns_store.Controls.Add(Me.txtbox_ns_storeno_load)
        Me.grpbox_ns_store.Controls.Add(Me.lbl_ns_storeno)
        Me.grpbox_ns_store.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_ns_store.Location = New System.Drawing.Point(12, 59)
        Me.grpbox_ns_store.Name = "grpbox_ns_store"
        Me.grpbox_ns_store.Size = New System.Drawing.Size(271, 376)
        Me.grpbox_ns_store.TabIndex = 1
        Me.grpbox_ns_store.TabStop = False
        Me.grpbox_ns_store.Text = "Store Details"
        '
        'ns_star29
        '
        Me.ns_star29.AutoSize = True
        Me.ns_star29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star29.ForeColor = System.Drawing.Color.Red
        Me.ns_star29.Location = New System.Drawing.Point(248, 77)
        Me.ns_star29.Name = "ns_star29"
        Me.ns_star29.Size = New System.Drawing.Size(13, 15)
        Me.ns_star29.TabIndex = 48
        Me.ns_star29.Text = "*"
        '
        'cmbbox_ns_store_system
        '
        Me.cmbbox_ns_store_system.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_store_system.FormattingEnabled = True
        Me.cmbbox_ns_store_system.Items.AddRange(New Object() {"", "ORPOS", "XSTORE"})
        Me.cmbbox_ns_store_system.Location = New System.Drawing.Point(125, 74)
        Me.cmbbox_ns_store_system.Name = "cmbbox_ns_store_system"
        Me.cmbbox_ns_store_system.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_ns_store_system.TabIndex = 3
        '
        'lbl_ns_store_system
        '
        Me.lbl_ns_store_system.AutoSize = True
        Me.lbl_ns_store_system.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_store_system.Location = New System.Drawing.Point(14, 80)
        Me.lbl_ns_store_system.Name = "lbl_ns_store_system"
        Me.lbl_ns_store_system.Size = New System.Drawing.Size(81, 13)
        Me.lbl_ns_store_system.TabIndex = 46
        Me.lbl_ns_store_system.Text = "StoreSystem:"
        '
        'ns_star4
        '
        Me.ns_star4.AutoSize = True
        Me.ns_star4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star4.ForeColor = System.Drawing.Color.Red
        Me.ns_star4.Location = New System.Drawing.Point(248, 129)
        Me.ns_star4.Name = "ns_star4"
        Me.ns_star4.Size = New System.Drawing.Size(13, 15)
        Me.ns_star4.TabIndex = 45
        Me.ns_star4.Text = "*"
        '
        'txtbox_ns_faxline_load
        '
        Me.txtbox_ns_faxline_load.Location = New System.Drawing.Point(125, 343)
        Me.txtbox_ns_faxline_load.Name = "txtbox_ns_faxline_load"
        Me.txtbox_ns_faxline_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_ns_faxline_load.TabIndex = 14
        '
        'ns_star10
        '
        Me.ns_star10.AutoSize = True
        Me.ns_star10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star10.ForeColor = System.Drawing.Color.Red
        Me.ns_star10.Location = New System.Drawing.Point(248, 225)
        Me.ns_star10.Name = "ns_star10"
        Me.ns_star10.Size = New System.Drawing.Size(13, 15)
        Me.ns_star10.TabIndex = 37
        Me.ns_star10.Text = "*"
        '
        'txtbox_ns_boline_load
        '
        Me.txtbox_ns_boline_load.Location = New System.Drawing.Point(125, 319)
        Me.txtbox_ns_boline_load.Name = "txtbox_ns_boline_load"
        Me.txtbox_ns_boline_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_ns_boline_load.TabIndex = 13
        '
        'ns_star9
        '
        Me.ns_star9.AutoSize = True
        Me.ns_star9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star9.ForeColor = System.Drawing.Color.Red
        Me.ns_star9.Location = New System.Drawing.Point(248, 276)
        Me.ns_star9.Name = "ns_star9"
        Me.ns_star9.Size = New System.Drawing.Size(13, 15)
        Me.ns_star9.TabIndex = 36
        Me.ns_star9.Text = "*"
        '
        'ns_star3
        '
        Me.ns_star3.AutoSize = True
        Me.ns_star3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star3.ForeColor = System.Drawing.Color.Red
        Me.ns_star3.Location = New System.Drawing.Point(248, 103)
        Me.ns_star3.Name = "ns_star3"
        Me.ns_star3.Size = New System.Drawing.Size(13, 15)
        Me.ns_star3.TabIndex = 38
        Me.ns_star3.Text = "*"
        '
        'ns_star8
        '
        Me.ns_star8.AutoSize = True
        Me.ns_star8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star8.ForeColor = System.Drawing.Color.Red
        Me.ns_star8.Location = New System.Drawing.Point(248, 251)
        Me.ns_star8.Name = "ns_star8"
        Me.ns_star8.Size = New System.Drawing.Size(13, 15)
        Me.ns_star8.TabIndex = 35
        Me.ns_star8.Text = "*"
        '
        'lbl_ns_str_faxline
        '
        Me.lbl_ns_str_faxline.AutoSize = True
        Me.lbl_ns_str_faxline.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_faxline.Location = New System.Drawing.Point(14, 346)
        Me.lbl_ns_str_faxline.Name = "lbl_ns_str_faxline"
        Me.lbl_ns_str_faxline.Size = New System.Drawing.Size(66, 15)
        Me.lbl_ns_str_faxline.TabIndex = 39
        Me.lbl_ns_str_faxline.Text = "Fax Line:"
        '
        'ns_star7
        '
        Me.ns_star7.AutoSize = True
        Me.ns_star7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star7.ForeColor = System.Drawing.Color.Red
        Me.ns_star7.Location = New System.Drawing.Point(248, 201)
        Me.ns_star7.Name = "ns_star7"
        Me.ns_star7.Size = New System.Drawing.Size(13, 15)
        Me.ns_star7.TabIndex = 34
        Me.ns_star7.Text = "*"
        '
        'lbl_ns_str_boline
        '
        Me.lbl_ns_str_boline.AutoSize = True
        Me.lbl_ns_str_boline.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_boline.Location = New System.Drawing.Point(14, 322)
        Me.lbl_ns_str_boline.Name = "lbl_ns_str_boline"
        Me.lbl_ns_str_boline.Size = New System.Drawing.Size(62, 15)
        Me.lbl_ns_str_boline.TabIndex = 38
        Me.lbl_ns_str_boline.Text = "BO Line:"
        '
        'ns_star6
        '
        Me.ns_star6.AutoSize = True
        Me.ns_star6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star6.ForeColor = System.Drawing.Color.Red
        Me.ns_star6.Location = New System.Drawing.Point(248, 177)
        Me.ns_star6.Name = "ns_star6"
        Me.ns_star6.Size = New System.Drawing.Size(13, 15)
        Me.ns_star6.TabIndex = 33
        Me.ns_star6.Text = "*"
        '
        'cmbbox_ns_iscombo_load
        '
        Me.cmbbox_ns_iscombo_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_iscombo_load.FormattingEnabled = True
        Me.cmbbox_ns_iscombo_load.Items.AddRange(New Object() {"", "NO", "YES"})
        Me.cmbbox_ns_iscombo_load.Location = New System.Drawing.Point(125, 100)
        Me.cmbbox_ns_iscombo_load.Name = "cmbbox_ns_iscombo_load"
        Me.cmbbox_ns_iscombo_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_ns_iscombo_load.TabIndex = 4
        '
        'ns_star5
        '
        Me.ns_star5.AutoSize = True
        Me.ns_star5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star5.ForeColor = System.Drawing.Color.Red
        Me.ns_star5.Location = New System.Drawing.Point(248, 153)
        Me.ns_star5.Name = "ns_star5"
        Me.ns_star5.Size = New System.Drawing.Size(13, 15)
        Me.ns_star5.TabIndex = 32
        Me.ns_star5.Text = "*"
        '
        'lbl_ns_iscombo
        '
        Me.lbl_ns_iscombo.AutoSize = True
        Me.lbl_ns_iscombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_iscombo.Location = New System.Drawing.Point(14, 103)
        Me.lbl_ns_iscombo.Name = "lbl_ns_iscombo"
        Me.lbl_ns_iscombo.Size = New System.Drawing.Size(61, 15)
        Me.lbl_ns_iscombo.TabIndex = 38
        Me.lbl_ns_iscombo.Text = "COMBO:"
        '
        'ns_star2
        '
        Me.ns_star2.AutoSize = True
        Me.ns_star2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star2.ForeColor = System.Drawing.Color.Red
        Me.ns_star2.Location = New System.Drawing.Point(248, 51)
        Me.ns_star2.Name = "ns_star2"
        Me.ns_star2.Size = New System.Drawing.Size(13, 15)
        Me.ns_star2.TabIndex = 31
        Me.ns_star2.Text = "*"
        '
        'ns_star1
        '
        Me.ns_star1.AutoSize = True
        Me.ns_star1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star1.ForeColor = System.Drawing.Color.Red
        Me.ns_star1.Location = New System.Drawing.Point(248, 25)
        Me.ns_star1.Name = "ns_star1"
        Me.ns_star1.Size = New System.Drawing.Size(13, 15)
        Me.ns_star1.TabIndex = 30
        Me.ns_star1.Text = "*"
        '
        'txtbox_ns_mainline_load
        '
        Me.txtbox_ns_mainline_load.Location = New System.Drawing.Point(125, 296)
        Me.txtbox_ns_mainline_load.Name = "txtbox_ns_mainline_load"
        Me.txtbox_ns_mainline_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_ns_mainline_load.TabIndex = 12
        '
        'cmbbox_ns_country_load
        '
        Me.cmbbox_ns_country_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_country_load.FormattingEnabled = True
        Me.cmbbox_ns_country_load.Items.AddRange(New Object() {"", "Canada", "US"})
        Me.cmbbox_ns_country_load.Location = New System.Drawing.Point(125, 222)
        Me.cmbbox_ns_country_load.Name = "cmbbox_ns_country_load"
        Me.cmbbox_ns_country_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_ns_country_load.TabIndex = 9
        '
        'txtbox_ns_pcode_load
        '
        Me.txtbox_ns_pcode_load.Location = New System.Drawing.Point(125, 273)
        Me.txtbox_ns_pcode_load.Name = "txtbox_ns_pcode_load"
        Me.txtbox_ns_pcode_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_ns_pcode_load.TabIndex = 10
        '
        'txtbox_ns_city_load
        '
        Me.txtbox_ns_city_load.Location = New System.Drawing.Point(125, 198)
        Me.txtbox_ns_city_load.Name = "txtbox_ns_city_load"
        Me.txtbox_ns_city_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_ns_city_load.TabIndex = 8
        '
        'txtbox_ns_address_load
        '
        Me.txtbox_ns_address_load.Location = New System.Drawing.Point(125, 174)
        Me.txtbox_ns_address_load.Name = "txtbox_ns_address_load"
        Me.txtbox_ns_address_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_ns_address_load.TabIndex = 7
        '
        'txtbox_ns_mall_load
        '
        Me.txtbox_ns_mall_load.Location = New System.Drawing.Point(125, 150)
        Me.txtbox_ns_mall_load.Name = "txtbox_ns_mall_load"
        Me.txtbox_ns_mall_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_ns_mall_load.TabIndex = 6
        '
        'txtbox_ns_sisstore_load
        '
        Me.txtbox_ns_sisstore_load.Location = New System.Drawing.Point(125, 126)
        Me.txtbox_ns_sisstore_load.Name = "txtbox_ns_sisstore_load"
        Me.txtbox_ns_sisstore_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_ns_sisstore_load.TabIndex = 5
        '
        'cmbbox_ns_banner_load
        '
        Me.cmbbox_ns_banner_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_banner_load.FormattingEnabled = True
        Me.cmbbox_ns_banner_load.Items.AddRange(New Object() {"", "Dynamite", "Dynamite HO", "Dynamite Combo", "Dynamite USA", "Garage", "Garage HO", "Garage Combo", "Garage USA"})
        Me.cmbbox_ns_banner_load.Location = New System.Drawing.Point(125, 48)
        Me.cmbbox_ns_banner_load.Name = "cmbbox_ns_banner_load"
        Me.cmbbox_ns_banner_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_ns_banner_load.TabIndex = 2
        '
        'lbl_ns_str_ph
        '
        Me.lbl_ns_str_ph.AutoSize = True
        Me.lbl_ns_str_ph.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_ph.Location = New System.Drawing.Point(14, 301)
        Me.lbl_ns_str_ph.Name = "lbl_ns_str_ph"
        Me.lbl_ns_str_ph.Size = New System.Drawing.Size(66, 13)
        Me.lbl_ns_str_ph.TabIndex = 19
        Me.lbl_ns_str_ph.Text = "Main Line:"
        '
        'lbl_ns_str_cntry
        '
        Me.lbl_ns_str_cntry.AutoSize = True
        Me.lbl_ns_str_cntry.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_cntry.Location = New System.Drawing.Point(14, 228)
        Me.lbl_ns_str_cntry.Name = "lbl_ns_str_cntry"
        Me.lbl_ns_str_cntry.Size = New System.Drawing.Size(54, 13)
        Me.lbl_ns_str_cntry.TabIndex = 18
        Me.lbl_ns_str_cntry.Text = "Country:"
        '
        'lbl_ns_str_pcode
        '
        Me.lbl_ns_str_pcode.AutoSize = True
        Me.lbl_ns_str_pcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_pcode.Location = New System.Drawing.Point(14, 278)
        Me.lbl_ns_str_pcode.Name = "lbl_ns_str_pcode"
        Me.lbl_ns_str_pcode.Size = New System.Drawing.Size(79, 13)
        Me.lbl_ns_str_pcode.TabIndex = 17
        Me.lbl_ns_str_pcode.Text = "Postal Code:"
        '
        'lbl_ns_str_prov
        '
        Me.lbl_ns_str_prov.AutoSize = True
        Me.lbl_ns_str_prov.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_prov.Location = New System.Drawing.Point(14, 252)
        Me.lbl_ns_str_prov.Name = "lbl_ns_str_prov"
        Me.lbl_ns_str_prov.Size = New System.Drawing.Size(105, 13)
        Me.lbl_ns_str_prov.TabIndex = 16
        Me.lbl_ns_str_prov.Text = "Province / State:"
        '
        'lbl_ns_str_city
        '
        Me.lbl_ns_str_city.AutoSize = True
        Me.lbl_ns_str_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_city.Location = New System.Drawing.Point(14, 203)
        Me.lbl_ns_str_city.Name = "lbl_ns_str_city"
        Me.lbl_ns_str_city.Size = New System.Drawing.Size(32, 13)
        Me.lbl_ns_str_city.TabIndex = 15
        Me.lbl_ns_str_city.Text = "City:"
        '
        'lbl_ns_str_add
        '
        Me.lbl_ns_str_add.AutoSize = True
        Me.lbl_ns_str_add.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_add.Location = New System.Drawing.Point(14, 179)
        Me.lbl_ns_str_add.Name = "lbl_ns_str_add"
        Me.lbl_ns_str_add.Size = New System.Drawing.Size(56, 13)
        Me.lbl_ns_str_add.TabIndex = 14
        Me.lbl_ns_str_add.Text = "Address:"
        '
        'lbl_ns_str_mall
        '
        Me.lbl_ns_str_mall.AutoSize = True
        Me.lbl_ns_str_mall.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_mall.Location = New System.Drawing.Point(14, 155)
        Me.lbl_ns_str_mall.Name = "lbl_ns_str_mall"
        Me.lbl_ns_str_mall.Size = New System.Drawing.Size(34, 13)
        Me.lbl_ns_str_mall.TabIndex = 13
        Me.lbl_ns_str_mall.Text = "Mall:"
        '
        'lbll_ns_str_sisstr
        '
        Me.lbll_ns_str_sisstr.AutoSize = True
        Me.lbll_ns_str_sisstr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbll_ns_str_sisstr.Location = New System.Drawing.Point(14, 131)
        Me.lbll_ns_str_sisstr.Name = "lbll_ns_str_sisstr"
        Me.lbll_ns_str_sisstr.Size = New System.Drawing.Size(77, 13)
        Me.lbll_ns_str_sisstr.TabIndex = 12
        Me.lbll_ns_str_sisstr.Text = "Sister Store:"
        '
        'lbl_ns_str_banner
        '
        Me.lbl_ns_str_banner.AutoSize = True
        Me.lbl_ns_str_banner.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_banner.Location = New System.Drawing.Point(14, 54)
        Me.lbl_ns_str_banner.Name = "lbl_ns_str_banner"
        Me.lbl_ns_str_banner.Size = New System.Drawing.Size(51, 13)
        Me.lbl_ns_str_banner.TabIndex = 11
        Me.lbl_ns_str_banner.Text = "Banner:"
        '
        'txtbox_ns_storeno_load
        '
        Me.txtbox_ns_storeno_load.Location = New System.Drawing.Point(125, 22)
        Me.txtbox_ns_storeno_load.Name = "txtbox_ns_storeno_load"
        Me.txtbox_ns_storeno_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_ns_storeno_load.TabIndex = 1
        '
        'lbl_new_store_info
        '
        Me.lbl_new_store_info.AutoSize = True
        Me.lbl_new_store_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_new_store_info.Location = New System.Drawing.Point(636, 9)
        Me.lbl_new_store_info.Name = "lbl_new_store_info"
        Me.lbl_new_store_info.Size = New System.Drawing.Size(270, 45)
        Me.lbl_new_store_info.TabIndex = 2
        Me.lbl_new_store_info.Text = "Please enter the new store's information." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Mandatory fields marked with a star" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "I" &
    "f value is N/A leave field blank."
        '
        'nb_star0
        '
        Me.nb_star0.AutoSize = True
        Me.nb_star0.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nb_star0.ForeColor = System.Drawing.Color.Red
        Me.nb_star0.Location = New System.Drawing.Point(868, 25)
        Me.nb_star0.Name = "nb_star0"
        Me.nb_star0.Size = New System.Drawing.Size(13, 15)
        Me.nb_star0.TabIndex = 38
        Me.nb_star0.Text = "*"
        '
        'grpbox_ns_operation_info
        '
        Me.grpbox_ns_operation_info.Controls.Add(Me.ns_star23)
        Me.grpbox_ns_operation_info.Controls.Add(Me.cmbbox_ns_location_load)
        Me.grpbox_ns_operation_info.Controls.Add(Me.lbl_ns_location)
        Me.grpbox_ns_operation_info.Controls.Add(Me.ns_star22)
        Me.grpbox_ns_operation_info.Controls.Add(Me.ns_star21)
        Me.grpbox_ns_operation_info.Controls.Add(Me.cmbbox_ns_status_load)
        Me.grpbox_ns_operation_info.Controls.Add(Me.lbl_ns_status)
        Me.grpbox_ns_operation_info.Controls.Add(Me.cmbbox_ns_isremote_load)
        Me.grpbox_ns_operation_info.Controls.Add(Me.lbl_ns_isremote)
        Me.grpbox_ns_operation_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_ns_operation_info.Location = New System.Drawing.Point(695, 133)
        Me.grpbox_ns_operation_info.Name = "grpbox_ns_operation_info"
        Me.grpbox_ns_operation_info.Size = New System.Drawing.Size(211, 109)
        Me.grpbox_ns_operation_info.TabIndex = 4
        Me.grpbox_ns_operation_info.TabStop = False
        Me.grpbox_ns_operation_info.Text = "Operational Information"
        '
        'ns_star23
        '
        Me.ns_star23.AutoSize = True
        Me.ns_star23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star23.ForeColor = System.Drawing.Color.Red
        Me.ns_star23.Location = New System.Drawing.Point(193, 78)
        Me.ns_star23.Name = "ns_star23"
        Me.ns_star23.Size = New System.Drawing.Size(13, 15)
        Me.ns_star23.TabIndex = 47
        Me.ns_star23.Text = "*"
        '
        'cmbbox_ns_location_load
        '
        Me.cmbbox_ns_location_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_location_load.FormattingEnabled = True
        Me.cmbbox_ns_location_load.Items.AddRange(New Object() {"MAIN", "TEMP"})
        Me.cmbbox_ns_location_load.Location = New System.Drawing.Point(105, 75)
        Me.cmbbox_ns_location_load.Name = "cmbbox_ns_location_load"
        Me.cmbbox_ns_location_load.Size = New System.Drawing.Size(86, 23)
        Me.cmbbox_ns_location_load.TabIndex = 52
        '
        'lbl_ns_location
        '
        Me.lbl_ns_location.AutoSize = True
        Me.lbl_ns_location.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_location.Location = New System.Drawing.Point(12, 78)
        Me.lbl_ns_location.Name = "lbl_ns_location"
        Me.lbl_ns_location.Size = New System.Drawing.Size(66, 15)
        Me.lbl_ns_location.TabIndex = 45
        Me.lbl_ns_location.Text = "Location:"
        '
        'ns_star22
        '
        Me.ns_star22.AutoSize = True
        Me.ns_star22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star22.ForeColor = System.Drawing.Color.Red
        Me.ns_star22.Location = New System.Drawing.Point(193, 52)
        Me.ns_star22.Name = "ns_star22"
        Me.ns_star22.Size = New System.Drawing.Size(13, 15)
        Me.ns_star22.TabIndex = 44
        Me.ns_star22.Text = "*"
        '
        'ns_star21
        '
        Me.ns_star21.AutoSize = True
        Me.ns_star21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star21.ForeColor = System.Drawing.Color.Red
        Me.ns_star21.Location = New System.Drawing.Point(193, 26)
        Me.ns_star21.Name = "ns_star21"
        Me.ns_star21.Size = New System.Drawing.Size(13, 15)
        Me.ns_star21.TabIndex = 43
        Me.ns_star21.Text = "*"
        '
        'cmbbox_ns_status_load
        '
        Me.cmbbox_ns_status_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_status_load.FormattingEnabled = True
        Me.cmbbox_ns_status_load.Items.AddRange(New Object() {"CLOSED", "OPEN"})
        Me.cmbbox_ns_status_load.Location = New System.Drawing.Point(105, 49)
        Me.cmbbox_ns_status_load.Name = "cmbbox_ns_status_load"
        Me.cmbbox_ns_status_load.Size = New System.Drawing.Size(86, 23)
        Me.cmbbox_ns_status_load.TabIndex = 51
        '
        'lbl_ns_status
        '
        Me.lbl_ns_status.AutoSize = True
        Me.lbl_ns_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_status.Location = New System.Drawing.Point(12, 52)
        Me.lbl_ns_status.Name = "lbl_ns_status"
        Me.lbl_ns_status.Size = New System.Drawing.Size(51, 15)
        Me.lbl_ns_status.TabIndex = 41
        Me.lbl_ns_status.Text = "Status:"
        '
        'cmbbox_ns_isremote_load
        '
        Me.cmbbox_ns_isremote_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_isremote_load.FormattingEnabled = True
        Me.cmbbox_ns_isremote_load.Items.AddRange(New Object() {"", "NO", "YES"})
        Me.cmbbox_ns_isremote_load.Location = New System.Drawing.Point(105, 23)
        Me.cmbbox_ns_isremote_load.Name = "cmbbox_ns_isremote_load"
        Me.cmbbox_ns_isremote_load.Size = New System.Drawing.Size(86, 23)
        Me.cmbbox_ns_isremote_load.TabIndex = 50
        '
        'lbl_ns_isremote
        '
        Me.lbl_ns_isremote.AutoSize = True
        Me.lbl_ns_isremote.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_isremote.Location = New System.Drawing.Point(12, 26)
        Me.lbl_ns_isremote.Name = "lbl_ns_isremote"
        Me.lbl_ns_isremote.Size = New System.Drawing.Size(61, 15)
        Me.lbl_ns_isremote.TabIndex = 39
        Me.lbl_ns_isremote.Text = "Remote:"
        '
        'txtbox_ns_dslline_load
        '
        Me.txtbox_ns_dslline_load.Location = New System.Drawing.Point(224, 100)
        Me.txtbox_ns_dslline_load.Name = "txtbox_ns_dslline_load"
        Me.txtbox_ns_dslline_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_dslline_load.TabIndex = 17
        '
        'lbl_ns_str_dslline
        '
        Me.lbl_ns_str_dslline.AutoSize = True
        Me.lbl_ns_str_dslline.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_str_dslline.Location = New System.Drawing.Point(13, 103)
        Me.lbl_ns_str_dslline.Name = "lbl_ns_str_dslline"
        Me.lbl_ns_str_dslline.Size = New System.Drawing.Size(92, 15)
        Me.lbl_ns_str_dslline.TabIndex = 41
        Me.lbl_ns_str_dslline.Text = "Internet Line:"
        '
        'grpbox_ns_network_info
        '
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_dslline_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_ispaccount_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_ispaccount)
        Me.grpbox_ns_network_info.Controls.Add(Me.ns_star17)
        Me.grpbox_ns_network_info.Controls.Add(Me.ns_star16)
        Me.grpbox_ns_network_info.Controls.Add(Me.ns_star15)
        Me.grpbox_ns_network_info.Controls.Add(Me.ns_star14)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_str_dslline)
        Me.grpbox_ns_network_info.Controls.Add(Me.ns_star13)
        Me.grpbox_ns_network_info.Controls.Add(Me.ns_star12)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_ispgateway_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_ispgateway)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_ispsubnet_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_ispsubnet)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_ispaddress_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_ispaddress)
        Me.grpbox_ns_network_info.Controls.Add(Me.cmbbox_ns_ispstatus_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_ispstatus)
        Me.grpbox_ns_network_info.Controls.Add(Me.ns_star18)
        Me.grpbox_ns_network_info.Controls.Add(Me.cmbbox_ns_rlocation_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_cablock_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_wifi_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_speed_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_pwd_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_user_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_isp_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.txtbox_ns_ip_load)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_cabinet_lock)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_rtr_location)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_wifispeed)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_speed)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_password)
        Me.grpbox_ns_network_info.Controls.Add(Me.ns_star19)
        Me.grpbox_ns_network_info.Controls.Add(Me.ns_star11)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_username)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_isp)
        Me.grpbox_ns_network_info.Controls.Add(Me.lbl_ns_ip)
        Me.grpbox_ns_network_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_ns_network_info.Location = New System.Drawing.Point(289, 59)
        Me.grpbox_ns_network_info.Name = "grpbox_ns_network_info"
        Me.grpbox_ns_network_info.Size = New System.Drawing.Size(400, 376)
        Me.grpbox_ns_network_info.TabIndex = 2
        Me.grpbox_ns_network_info.TabStop = False
        Me.grpbox_ns_network_info.Text = "Network Details"
        '
        'txtbox_ns_ispaccount_load
        '
        Me.txtbox_ns_ispaccount_load.Location = New System.Drawing.Point(224, 74)
        Me.txtbox_ns_ispaccount_load.Name = "txtbox_ns_ispaccount_load"
        Me.txtbox_ns_ispaccount_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_ispaccount_load.TabIndex = 16
        '
        'lbl_ns_ispaccount
        '
        Me.lbl_ns_ispaccount.AutoSize = True
        Me.lbl_ns_ispaccount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_ispaccount.Location = New System.Drawing.Point(13, 78)
        Me.lbl_ns_ispaccount.Name = "lbl_ns_ispaccount"
        Me.lbl_ns_ispaccount.Size = New System.Drawing.Size(87, 15)
        Me.lbl_ns_ispaccount.TabIndex = 71
        Me.lbl_ns_ispaccount.Text = "ISP Account:"
        '
        'ns_star17
        '
        Me.ns_star17.AutoSize = True
        Me.ns_star17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star17.ForeColor = System.Drawing.Color.Red
        Me.ns_star17.Location = New System.Drawing.Point(377, 225)
        Me.ns_star17.Name = "ns_star17"
        Me.ns_star17.Size = New System.Drawing.Size(13, 15)
        Me.ns_star17.TabIndex = 70
        Me.ns_star17.Text = "*"
        '
        'ns_star16
        '
        Me.ns_star16.AutoSize = True
        Me.ns_star16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star16.ForeColor = System.Drawing.Color.Red
        Me.ns_star16.Location = New System.Drawing.Point(377, 201)
        Me.ns_star16.Name = "ns_star16"
        Me.ns_star16.Size = New System.Drawing.Size(13, 15)
        Me.ns_star16.TabIndex = 69
        Me.ns_star16.Text = "*"
        '
        'ns_star15
        '
        Me.ns_star15.AutoSize = True
        Me.ns_star15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star15.ForeColor = System.Drawing.Color.Red
        Me.ns_star15.Location = New System.Drawing.Point(377, 177)
        Me.ns_star15.Name = "ns_star15"
        Me.ns_star15.Size = New System.Drawing.Size(13, 15)
        Me.ns_star15.TabIndex = 68
        Me.ns_star15.Text = "*"
        '
        'ns_star14
        '
        Me.ns_star14.AutoSize = True
        Me.ns_star14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star14.ForeColor = System.Drawing.Color.Red
        Me.ns_star14.Location = New System.Drawing.Point(377, 156)
        Me.ns_star14.Name = "ns_star14"
        Me.ns_star14.Size = New System.Drawing.Size(13, 15)
        Me.ns_star14.TabIndex = 67
        Me.ns_star14.Text = "*"
        '
        'ns_star13
        '
        Me.ns_star13.AutoSize = True
        Me.ns_star13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star13.ForeColor = System.Drawing.Color.Red
        Me.ns_star13.Location = New System.Drawing.Point(377, 128)
        Me.ns_star13.Name = "ns_star13"
        Me.ns_star13.Size = New System.Drawing.Size(13, 15)
        Me.ns_star13.TabIndex = 66
        Me.ns_star13.Text = "*"
        '
        'ns_star12
        '
        Me.ns_star12.AutoSize = True
        Me.ns_star12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star12.ForeColor = System.Drawing.Color.Red
        Me.ns_star12.Location = New System.Drawing.Point(377, 51)
        Me.ns_star12.Name = "ns_star12"
        Me.ns_star12.Size = New System.Drawing.Size(13, 15)
        Me.ns_star12.TabIndex = 65
        Me.ns_star12.Text = "*"
        '
        'txtbox_ns_ispgateway_load
        '
        Me.txtbox_ns_ispgateway_load.Location = New System.Drawing.Point(224, 199)
        Me.txtbox_ns_ispgateway_load.Name = "txtbox_ns_ispgateway_load"
        Me.txtbox_ns_ispgateway_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_ispgateway_load.TabIndex = 21
        '
        'lbl_ns_ispgateway
        '
        Me.lbl_ns_ispgateway.AutoSize = True
        Me.lbl_ns_ispgateway.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_ispgateway.Location = New System.Drawing.Point(13, 201)
        Me.lbl_ns_ispgateway.Name = "lbl_ns_ispgateway"
        Me.lbl_ns_ispgateway.Size = New System.Drawing.Size(141, 15)
        Me.lbl_ns_ispgateway.TabIndex = 63
        Me.lbl_ns_ispgateway.Text = "ISP Default Gateway:"
        '
        'txtbox_ns_ispsubnet_load
        '
        Me.txtbox_ns_ispsubnet_load.Location = New System.Drawing.Point(224, 176)
        Me.txtbox_ns_ispsubnet_load.Name = "txtbox_ns_ispsubnet_load"
        Me.txtbox_ns_ispsubnet_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_ispsubnet_load.TabIndex = 20
        '
        'lbl_ns_ispsubnet
        '
        Me.lbl_ns_ispsubnet.AutoSize = True
        Me.lbl_ns_ispsubnet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_ispsubnet.Location = New System.Drawing.Point(13, 177)
        Me.lbl_ns_ispsubnet.Name = "lbl_ns_ispsubnet"
        Me.lbl_ns_ispsubnet.Size = New System.Drawing.Size(82, 15)
        Me.lbl_ns_ispsubnet.TabIndex = 61
        Me.lbl_ns_ispsubnet.Text = "ISP Subnet:"
        '
        'txtbox_ns_ispaddress_load
        '
        Me.txtbox_ns_ispaddress_load.Location = New System.Drawing.Point(224, 152)
        Me.txtbox_ns_ispaddress_load.Name = "txtbox_ns_ispaddress_load"
        Me.txtbox_ns_ispaddress_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_ispaddress_load.TabIndex = 19
        '
        'lbl_ns_ispaddress
        '
        Me.lbl_ns_ispaddress.AutoSize = True
        Me.lbl_ns_ispaddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_ispaddress.Location = New System.Drawing.Point(13, 153)
        Me.lbl_ns_ispaddress.Name = "lbl_ns_ispaddress"
        Me.lbl_ns_ispaddress.Size = New System.Drawing.Size(105, 15)
        Me.lbl_ns_ispaddress.TabIndex = 59
        Me.lbl_ns_ispaddress.Text = "ISP IP Address:"
        '
        'cmbbox_ns_ispstatus_load
        '
        Me.cmbbox_ns_ispstatus_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_ispstatus_load.FormattingEnabled = True
        Me.cmbbox_ns_ispstatus_load.Items.AddRange(New Object() {"", "DHCP", "PPPOE", "Static"})
        Me.cmbbox_ns_ispstatus_load.Location = New System.Drawing.Point(224, 125)
        Me.cmbbox_ns_ispstatus_load.Name = "cmbbox_ns_ispstatus_load"
        Me.cmbbox_ns_ispstatus_load.Size = New System.Drawing.Size(151, 23)
        Me.cmbbox_ns_ispstatus_load.TabIndex = 18
        '
        'lbl_ns_ispstatus
        '
        Me.lbl_ns_ispstatus.AutoSize = True
        Me.lbl_ns_ispstatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_ispstatus.Location = New System.Drawing.Point(13, 129)
        Me.lbl_ns_ispstatus.Name = "lbl_ns_ispstatus"
        Me.lbl_ns_ispstatus.Size = New System.Drawing.Size(166, 15)
        Me.lbl_ns_ispstatus.TabIndex = 58
        Me.lbl_ns_ispstatus.Text = "ISP Address Assignment:"
        '
        'ns_star18
        '
        Me.ns_star18.AutoSize = True
        Me.ns_star18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star18.ForeColor = System.Drawing.Color.Red
        Me.ns_star18.Location = New System.Drawing.Point(377, 249)
        Me.ns_star18.Name = "ns_star18"
        Me.ns_star18.Size = New System.Drawing.Size(13, 15)
        Me.ns_star18.TabIndex = 57
        Me.ns_star18.Text = "*"
        '
        'cmbbox_ns_rlocation_load
        '
        Me.cmbbox_ns_rlocation_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_rlocation_load.FormattingEnabled = True
        Me.cmbbox_ns_rlocation_load.Items.AddRange(New Object() {"", "Back - IT Cabinet", "Back - Shelf / Wall", "Front"})
        Me.cmbbox_ns_rlocation_load.Location = New System.Drawing.Point(224, 319)
        Me.cmbbox_ns_rlocation_load.Name = "cmbbox_ns_rlocation_load"
        Me.cmbbox_ns_rlocation_load.Size = New System.Drawing.Size(151, 23)
        Me.cmbbox_ns_rlocation_load.TabIndex = 26
        '
        'txtbox_ns_cablock_load
        '
        Me.txtbox_ns_cablock_load.Location = New System.Drawing.Point(224, 345)
        Me.txtbox_ns_cablock_load.Name = "txtbox_ns_cablock_load"
        Me.txtbox_ns_cablock_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_cablock_load.TabIndex = 27
        '
        'txtbox_ns_wifi_load
        '
        Me.txtbox_ns_wifi_load.Location = New System.Drawing.Point(224, 296)
        Me.txtbox_ns_wifi_load.Name = "txtbox_ns_wifi_load"
        Me.txtbox_ns_wifi_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_wifi_load.TabIndex = 25
        '
        'txtbox_ns_speed_load
        '
        Me.txtbox_ns_speed_load.Location = New System.Drawing.Point(224, 270)
        Me.txtbox_ns_speed_load.Name = "txtbox_ns_speed_load"
        Me.txtbox_ns_speed_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_speed_load.TabIndex = 24
        '
        'txtbox_ns_pwd_load
        '
        Me.txtbox_ns_pwd_load.Location = New System.Drawing.Point(224, 246)
        Me.txtbox_ns_pwd_load.Name = "txtbox_ns_pwd_load"
        Me.txtbox_ns_pwd_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_pwd_load.TabIndex = 23
        '
        'txtbox_ns_user_load
        '
        Me.txtbox_ns_user_load.Location = New System.Drawing.Point(224, 223)
        Me.txtbox_ns_user_load.Name = "txtbox_ns_user_load"
        Me.txtbox_ns_user_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_user_load.TabIndex = 22
        '
        'txtbox_ns_isp_load
        '
        Me.txtbox_ns_isp_load.Location = New System.Drawing.Point(224, 48)
        Me.txtbox_ns_isp_load.Name = "txtbox_ns_isp_load"
        Me.txtbox_ns_isp_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_isp_load.TabIndex = 15
        '
        'txtbox_ns_ip_load
        '
        Me.txtbox_ns_ip_load.Location = New System.Drawing.Point(224, 22)
        Me.txtbox_ns_ip_load.Name = "txtbox_ns_ip_load"
        Me.txtbox_ns_ip_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_ns_ip_load.TabIndex = 14
        '
        'lbl_ns_cabinet_lock
        '
        Me.lbl_ns_cabinet_lock.AutoSize = True
        Me.lbl_ns_cabinet_lock.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_cabinet_lock.Location = New System.Drawing.Point(13, 346)
        Me.lbl_ns_cabinet_lock.Name = "lbl_ns_cabinet_lock"
        Me.lbl_ns_cabinet_lock.Size = New System.Drawing.Size(110, 15)
        Me.lbl_ns_cabinet_lock.TabIndex = 49
        Me.lbl_ns_cabinet_lock.Text = "IT Cabinet Lock:"
        '
        'lbl_rtr_location
        '
        Me.lbl_rtr_location.AutoSize = True
        Me.lbl_rtr_location.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_rtr_location.Location = New System.Drawing.Point(13, 322)
        Me.lbl_rtr_location.Name = "lbl_rtr_location"
        Me.lbl_rtr_location.Size = New System.Drawing.Size(113, 15)
        Me.lbl_rtr_location.TabIndex = 48
        Me.lbl_rtr_location.Text = "Router Location:"
        '
        'lbl_ns_wifispeed
        '
        Me.lbl_ns_wifispeed.AutoSize = True
        Me.lbl_ns_wifispeed.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_wifispeed.Location = New System.Drawing.Point(13, 299)
        Me.lbl_ns_wifispeed.Name = "lbl_ns_wifispeed"
        Me.lbl_ns_wifispeed.Size = New System.Drawing.Size(84, 15)
        Me.lbl_ns_wifispeed.TabIndex = 47
        Me.lbl_ns_wifispeed.Text = "WiFi Speed:"
        '
        'lbl_ns_speed
        '
        Me.lbl_ns_speed.AutoSize = True
        Me.lbl_ns_speed.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_speed.Location = New System.Drawing.Point(13, 273)
        Me.lbl_ns_speed.Name = "lbl_ns_speed"
        Me.lbl_ns_speed.Size = New System.Drawing.Size(90, 15)
        Me.lbl_ns_speed.TabIndex = 46
        Me.lbl_ns_speed.Text = "Store Speed:"
        '
        'lbl_ns_password
        '
        Me.lbl_ns_password.AutoSize = True
        Me.lbl_ns_password.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_password.Location = New System.Drawing.Point(13, 249)
        Me.lbl_ns_password.Name = "lbl_ns_password"
        Me.lbl_ns_password.Size = New System.Drawing.Size(73, 15)
        Me.lbl_ns_password.TabIndex = 45
        Me.lbl_ns_password.Text = "Password:"
        '
        'ns_star19
        '
        Me.ns_star19.AutoSize = True
        Me.ns_star19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star19.ForeColor = System.Drawing.Color.Red
        Me.ns_star19.Location = New System.Drawing.Point(377, 322)
        Me.ns_star19.Name = "ns_star19"
        Me.ns_star19.Size = New System.Drawing.Size(13, 15)
        Me.ns_star19.TabIndex = 44
        Me.ns_star19.Text = "*"
        '
        'ns_star11
        '
        Me.ns_star11.AutoSize = True
        Me.ns_star11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star11.ForeColor = System.Drawing.Color.Red
        Me.ns_star11.Location = New System.Drawing.Point(377, 25)
        Me.ns_star11.Name = "ns_star11"
        Me.ns_star11.Size = New System.Drawing.Size(13, 15)
        Me.ns_star11.TabIndex = 38
        Me.ns_star11.Text = "*"
        '
        'lbl_ns_username
        '
        Me.lbl_ns_username.AutoSize = True
        Me.lbl_ns_username.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_username.Location = New System.Drawing.Point(13, 225)
        Me.lbl_ns_username.Name = "lbl_ns_username"
        Me.lbl_ns_username.Size = New System.Drawing.Size(77, 15)
        Me.lbl_ns_username.TabIndex = 41
        Me.lbl_ns_username.Text = "Username:"
        '
        'lbl_ns_isp
        '
        Me.lbl_ns_isp.AutoSize = True
        Me.lbl_ns_isp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_isp.Location = New System.Drawing.Point(13, 52)
        Me.lbl_ns_isp.Name = "lbl_ns_isp"
        Me.lbl_ns_isp.Size = New System.Drawing.Size(33, 15)
        Me.lbl_ns_isp.TabIndex = 39
        Me.lbl_ns_isp.Text = "ISP:"
        '
        'lbl_ns_ip
        '
        Me.lbl_ns_ip.AutoSize = True
        Me.lbl_ns_ip.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_ip.Location = New System.Drawing.Point(13, 25)
        Me.lbl_ns_ip.Name = "lbl_ns_ip"
        Me.lbl_ns_ip.Size = New System.Drawing.Size(62, 15)
        Me.lbl_ns_ip.TabIndex = 38
        Me.lbl_ns_ip.Text = "Store IP:"
        '
        'lbl_ns_main
        '
        Me.lbl_ns_main.AutoSize = True
        Me.lbl_ns_main.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_main.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_ns_main.Location = New System.Drawing.Point(65, 24)
        Me.lbl_ns_main.Name = "lbl_ns_main"
        Me.lbl_ns_main.Size = New System.Drawing.Size(135, 16)
        Me.lbl_ns_main.TabIndex = 46
        Me.lbl_ns_main.Text = "ADD NEW STORE"
        '
        'btn_ns_cancel
        '
        Me.btn_ns_cancel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_ns_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_ns_cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ns_cancel.Location = New System.Drawing.Point(697, 414)
        Me.btn_ns_cancel.Name = "btn_ns_cancel"
        Me.btn_ns_cancel.Size = New System.Drawing.Size(68, 21)
        Me.btn_ns_cancel.TabIndex = 101
        Me.btn_ns_cancel.Text = "CLOSE"
        Me.btn_ns_cancel.UseVisualStyleBackColor = False
        '
        'btn_ns_save
        '
        Me.btn_ns_save.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_ns_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_ns_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ns_save.Location = New System.Drawing.Point(845, 414)
        Me.btn_ns_save.Name = "btn_ns_save"
        Me.btn_ns_save.Size = New System.Drawing.Size(68, 21)
        Me.btn_ns_save.TabIndex = 99
        Me.btn_ns_save.Text = "ADD"
        Me.btn_ns_save.UseVisualStyleBackColor = False
        '
        'btn_ns_clear
        '
        Me.btn_ns_clear.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_ns_clear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_ns_clear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_ns_clear.Location = New System.Drawing.Point(771, 414)
        Me.btn_ns_clear.Name = "btn_ns_clear"
        Me.btn_ns_clear.Size = New System.Drawing.Size(68, 21)
        Me.btn_ns_clear.TabIndex = 100
        Me.btn_ns_clear.Text = "Clear"
        Me.btn_ns_clear.UseVisualStyleBackColor = False
        '
        'lbl_ns_spare_modem
        '
        Me.lbl_ns_spare_modem.AutoSize = True
        Me.lbl_ns_spare_modem.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_spare_modem.Location = New System.Drawing.Point(14, 26)
        Me.lbl_ns_spare_modem.Name = "lbl_ns_spare_modem"
        Me.lbl_ns_spare_modem.Size = New System.Drawing.Size(59, 15)
        Me.lbl_ns_spare_modem.TabIndex = 38
        Me.lbl_ns_spare_modem.Text = "Modem:"
        '
        'cmbbox_ns_smodem_load
        '
        Me.cmbbox_ns_smodem_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_smodem_load.FormattingEnabled = True
        Me.cmbbox_ns_smodem_load.Items.AddRange(New Object() {"", "NO", "YES"})
        Me.cmbbox_ns_smodem_load.Location = New System.Drawing.Point(130, 23)
        Me.cmbbox_ns_smodem_load.Name = "cmbbox_ns_smodem_load"
        Me.cmbbox_ns_smodem_load.Size = New System.Drawing.Size(61, 23)
        Me.cmbbox_ns_smodem_load.TabIndex = 53
        '
        'lbl_ns_spare_router
        '
        Me.lbl_ns_spare_router.AutoSize = True
        Me.lbl_ns_spare_router.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_spare_router.Location = New System.Drawing.Point(14, 52)
        Me.lbl_ns_spare_router.Name = "lbl_ns_spare_router"
        Me.lbl_ns_spare_router.Size = New System.Drawing.Size(54, 15)
        Me.lbl_ns_spare_router.TabIndex = 39
        Me.lbl_ns_spare_router.Text = "Router:"
        '
        'cmbbox_ns_srouter_load
        '
        Me.cmbbox_ns_srouter_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_srouter_load.FormattingEnabled = True
        Me.cmbbox_ns_srouter_load.Items.AddRange(New Object() {"", "NO", "YES"})
        Me.cmbbox_ns_srouter_load.Location = New System.Drawing.Point(130, 49)
        Me.cmbbox_ns_srouter_load.Name = "cmbbox_ns_srouter_load"
        Me.cmbbox_ns_srouter_load.Size = New System.Drawing.Size(61, 23)
        Me.cmbbox_ns_srouter_load.TabIndex = 54
        '
        'lbl_ns_spare_wireless_card
        '
        Me.lbl_ns_spare_wireless_card.AutoSize = True
        Me.lbl_ns_spare_wireless_card.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_spare_wireless_card.Location = New System.Drawing.Point(14, 78)
        Me.lbl_ns_spare_wireless_card.Name = "lbl_ns_spare_wireless_card"
        Me.lbl_ns_spare_wireless_card.Size = New System.Drawing.Size(100, 15)
        Me.lbl_ns_spare_wireless_card.TabIndex = 41
        Me.lbl_ns_spare_wireless_card.Text = "Wireless Card:"
        '
        'cmbbox_ns_swc_load
        '
        Me.cmbbox_ns_swc_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_swc_load.FormattingEnabled = True
        Me.cmbbox_ns_swc_load.Items.AddRange(New Object() {"", "NO", "YES"})
        Me.cmbbox_ns_swc_load.Location = New System.Drawing.Point(130, 75)
        Me.cmbbox_ns_swc_load.Name = "cmbbox_ns_swc_load"
        Me.cmbbox_ns_swc_load.Size = New System.Drawing.Size(61, 23)
        Me.cmbbox_ns_swc_load.TabIndex = 55
        '
        'ns_star24
        '
        Me.ns_star24.AutoSize = True
        Me.ns_star24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star24.ForeColor = System.Drawing.Color.Red
        Me.ns_star24.Location = New System.Drawing.Point(193, 26)
        Me.ns_star24.Name = "ns_star24"
        Me.ns_star24.Size = New System.Drawing.Size(13, 15)
        Me.ns_star24.TabIndex = 38
        Me.ns_star24.Text = "*"
        '
        'ns_star25
        '
        Me.ns_star25.AutoSize = True
        Me.ns_star25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star25.ForeColor = System.Drawing.Color.Red
        Me.ns_star25.Location = New System.Drawing.Point(193, 52)
        Me.ns_star25.Name = "ns_star25"
        Me.ns_star25.Size = New System.Drawing.Size(13, 15)
        Me.ns_star25.TabIndex = 43
        Me.ns_star25.Text = "*"
        '
        'ns_star26
        '
        Me.ns_star26.AutoSize = True
        Me.ns_star26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star26.ForeColor = System.Drawing.Color.Red
        Me.ns_star26.Location = New System.Drawing.Point(193, 78)
        Me.ns_star26.Name = "ns_star26"
        Me.ns_star26.Size = New System.Drawing.Size(13, 15)
        Me.ns_star26.TabIndex = 44
        Me.ns_star26.Text = "*"
        '
        'lbl_ns_spare_holiday_cash
        '
        Me.lbl_ns_spare_holiday_cash.AutoSize = True
        Me.lbl_ns_spare_holiday_cash.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_spare_holiday_cash.Location = New System.Drawing.Point(14, 104)
        Me.lbl_ns_spare_holiday_cash.Name = "lbl_ns_spare_holiday_cash"
        Me.lbl_ns_spare_holiday_cash.Size = New System.Drawing.Size(81, 15)
        Me.lbl_ns_spare_holiday_cash.TabIndex = 45
        Me.lbl_ns_spare_holiday_cash.Text = "Holiday PC:"
        '
        'lbl_ns_spare_pinpad
        '
        Me.lbl_ns_spare_pinpad.AutoSize = True
        Me.lbl_ns_spare_pinpad.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_spare_pinpad.Location = New System.Drawing.Point(14, 130)
        Me.lbl_ns_spare_pinpad.Name = "lbl_ns_spare_pinpad"
        Me.lbl_ns_spare_pinpad.Size = New System.Drawing.Size(56, 15)
        Me.lbl_ns_spare_pinpad.TabIndex = 46
        Me.lbl_ns_spare_pinpad.Text = "Pinpad:"
        '
        'cmbbox_ns_hpc_load
        '
        Me.cmbbox_ns_hpc_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_hpc_load.FormattingEnabled = True
        Me.cmbbox_ns_hpc_load.Items.AddRange(New Object() {"", "NO", "YES"})
        Me.cmbbox_ns_hpc_load.Location = New System.Drawing.Point(130, 101)
        Me.cmbbox_ns_hpc_load.Name = "cmbbox_ns_hpc_load"
        Me.cmbbox_ns_hpc_load.Size = New System.Drawing.Size(61, 23)
        Me.cmbbox_ns_hpc_load.TabIndex = 56
        '
        'cmbbox_ns_sppad_load
        '
        Me.cmbbox_ns_sppad_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_sppad_load.FormattingEnabled = True
        Me.cmbbox_ns_sppad_load.Items.AddRange(New Object() {"", "NO", "YES"})
        Me.cmbbox_ns_sppad_load.Location = New System.Drawing.Point(130, 127)
        Me.cmbbox_ns_sppad_load.Name = "cmbbox_ns_sppad_load"
        Me.cmbbox_ns_sppad_load.Size = New System.Drawing.Size(61, 23)
        Me.cmbbox_ns_sppad_load.TabIndex = 57
        '
        'ns_star27
        '
        Me.ns_star27.AutoSize = True
        Me.ns_star27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star27.ForeColor = System.Drawing.Color.Red
        Me.ns_star27.Location = New System.Drawing.Point(193, 104)
        Me.ns_star27.Name = "ns_star27"
        Me.ns_star27.Size = New System.Drawing.Size(13, 15)
        Me.ns_star27.TabIndex = 49
        Me.ns_star27.Text = "*"
        '
        'ns_star28
        '
        Me.ns_star28.AutoSize = True
        Me.ns_star28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star28.ForeColor = System.Drawing.Color.Red
        Me.ns_star28.Location = New System.Drawing.Point(193, 130)
        Me.ns_star28.Name = "ns_star28"
        Me.ns_star28.Size = New System.Drawing.Size(13, 15)
        Me.ns_star28.TabIndex = 50
        Me.ns_star28.Text = "*"
        '
        'cmbbox_ns_spare_equip
        '
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.ns_star28)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.ns_star27)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.cmbbox_ns_sppad_load)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.cmbbox_ns_hpc_load)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.lbl_ns_spare_pinpad)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.lbl_ns_spare_holiday_cash)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.ns_star26)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.ns_star25)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.ns_star24)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.cmbbox_ns_swc_load)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.lbl_ns_spare_wireless_card)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.cmbbox_ns_srouter_load)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.lbl_ns_spare_router)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.cmbbox_ns_smodem_load)
        Me.cmbbox_ns_spare_equip.Controls.Add(Me.lbl_ns_spare_modem)
        Me.cmbbox_ns_spare_equip.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbbox_ns_spare_equip.Location = New System.Drawing.Point(695, 248)
        Me.cmbbox_ns_spare_equip.Name = "cmbbox_ns_spare_equip"
        Me.cmbbox_ns_spare_equip.Size = New System.Drawing.Size(211, 160)
        Me.cmbbox_ns_spare_equip.TabIndex = 5
        Me.cmbbox_ns_spare_equip.TabStop = False
        Me.cmbbox_ns_spare_equip.Text = "Spare Equipment"
        '
        'grpbox_ns_ip_pinpads
        '
        Me.grpbox_ns_ip_pinpads.Controls.Add(Me.ns_star30)
        Me.grpbox_ns_ip_pinpads.Controls.Add(Me.cmbbox_ns_pinpad_load)
        Me.grpbox_ns_ip_pinpads.Controls.Add(Me.lbl_ns_pinpads)
        Me.grpbox_ns_ip_pinpads.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_ns_ip_pinpads.Location = New System.Drawing.Point(695, 59)
        Me.grpbox_ns_ip_pinpads.Name = "grpbox_ns_ip_pinpads"
        Me.grpbox_ns_ip_pinpads.Size = New System.Drawing.Size(211, 71)
        Me.grpbox_ns_ip_pinpads.TabIndex = 3
        Me.grpbox_ns_ip_pinpads.TabStop = False
        Me.grpbox_ns_ip_pinpads.Text = "Pinpad Information"
        '
        'ns_star30
        '
        Me.ns_star30.AutoSize = True
        Me.ns_star30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ns_star30.ForeColor = System.Drawing.Color.Red
        Me.ns_star30.Location = New System.Drawing.Point(193, 32)
        Me.ns_star30.Name = "ns_star30"
        Me.ns_star30.Size = New System.Drawing.Size(13, 15)
        Me.ns_star30.TabIndex = 53
        Me.ns_star30.Text = "*"
        '
        'cmbbox_ns_pinpad_load
        '
        Me.cmbbox_ns_pinpad_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_pinpad_load.FormattingEnabled = True
        Me.cmbbox_ns_pinpad_load.Items.AddRange(New Object() {"", "COM2", "overIP"})
        Me.cmbbox_ns_pinpad_load.Location = New System.Drawing.Point(105, 29)
        Me.cmbbox_ns_pinpad_load.Name = "cmbbox_ns_pinpad_load"
        Me.cmbbox_ns_pinpad_load.Size = New System.Drawing.Size(86, 23)
        Me.cmbbox_ns_pinpad_load.TabIndex = 1
        '
        'lbl_ns_pinpads
        '
        Me.lbl_ns_pinpads.AutoSize = True
        Me.lbl_ns_pinpads.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ns_pinpads.Location = New System.Drawing.Point(14, 32)
        Me.lbl_ns_pinpads.Name = "lbl_ns_pinpads"
        Me.lbl_ns_pinpads.Size = New System.Drawing.Size(83, 15)
        Me.lbl_ns_pinpads.TabIndex = 53
        Me.lbl_ns_pinpads.Text = "Pinpad con:"
        '
        'cmbbox_ns_province_load
        '
        Me.cmbbox_ns_province_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_ns_province_load.FormattingEnabled = True
        Me.cmbbox_ns_province_load.Location = New System.Drawing.Point(125, 248)
        Me.cmbbox_ns_province_load.Name = "cmbbox_ns_province_load"
        Me.cmbbox_ns_province_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_ns_province_load.TabIndex = 10
        '
        'Store_new
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(930, 449)
        Me.Controls.Add(Me.grpbox_ns_ip_pinpads)
        Me.Controls.Add(Me.cmbbox_ns_spare_equip)
        Me.Controls.Add(Me.btn_ns_clear)
        Me.Controls.Add(Me.btn_ns_save)
        Me.Controls.Add(Me.btn_ns_cancel)
        Me.Controls.Add(Me.lbl_ns_main)
        Me.Controls.Add(Me.grpbox_ns_network_info)
        Me.Controls.Add(Me.grpbox_ns_operation_info)
        Me.Controls.Add(Me.nb_star0)
        Me.Controls.Add(Me.lbl_new_store_info)
        Me.Controls.Add(Me.grpbox_ns_store)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Store_new"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - NEW STORE"
        Me.grpbox_ns_store.ResumeLayout(False)
        Me.grpbox_ns_store.PerformLayout()
        Me.grpbox_ns_operation_info.ResumeLayout(False)
        Me.grpbox_ns_operation_info.PerformLayout()
        Me.grpbox_ns_network_info.ResumeLayout(False)
        Me.grpbox_ns_network_info.PerformLayout()
        Me.cmbbox_ns_spare_equip.ResumeLayout(False)
        Me.cmbbox_ns_spare_equip.PerformLayout()
        Me.grpbox_ns_ip_pinpads.ResumeLayout(False)
        Me.grpbox_ns_ip_pinpads.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_ns_storeno As Label
    Friend WithEvents grpbox_ns_store As GroupBox
    Friend WithEvents lbl_new_store_info As Label
    Friend WithEvents txtbox_ns_storeno_load As TextBox
    Friend WithEvents lbl_ns_str_ph As Label
    Friend WithEvents lbl_ns_str_cntry As Label
    Friend WithEvents lbl_ns_str_pcode As Label
    Friend WithEvents lbl_ns_str_prov As Label
    Friend WithEvents lbl_ns_str_city As Label
    Friend WithEvents lbl_ns_str_add As Label
    Friend WithEvents lbl_ns_str_mall As Label
    Friend WithEvents lbll_ns_str_sisstr As Label
    Friend WithEvents lbl_ns_str_banner As Label
    Friend WithEvents txtbox_ns_sisstore_load As TextBox
    Friend WithEvents cmbbox_ns_banner_load As ComboBox
    Friend WithEvents txtbox_ns_mall_load As TextBox
    Friend WithEvents txtbox_ns_address_load As TextBox
    Friend WithEvents txtbox_ns_mainline_load As TextBox
    Friend WithEvents cmbbox_ns_country_load As ComboBox
    Friend WithEvents txtbox_ns_pcode_load As TextBox
    Friend WithEvents txtbox_ns_city_load As TextBox
    Friend WithEvents ns_star10 As Label
    Friend WithEvents ns_star9 As Label
    Friend WithEvents ns_star8 As Label
    Friend WithEvents ns_star7 As Label
    Friend WithEvents ns_star6 As Label
    Friend WithEvents ns_star5 As Label
    Friend WithEvents ns_star2 As Label
    Friend WithEvents ns_star1 As Label
    Friend WithEvents nb_star0 As Label
    Friend WithEvents grpbox_ns_operation_info As GroupBox
    Friend WithEvents lbl_ns_iscombo As Label
    Friend WithEvents cmbbox_ns_iscombo_load As ComboBox
    Friend WithEvents lbl_ns_isremote As Label
    Friend WithEvents cmbbox_ns_isremote_load As ComboBox
    Friend WithEvents lbl_ns_status As Label
    Friend WithEvents cmbbox_ns_status_load As ComboBox
    Friend WithEvents lbl_ns_str_dslline As Label
    Friend WithEvents lbl_ns_str_faxline As Label
    Friend WithEvents lbl_ns_str_boline As Label
    Friend WithEvents txtbox_ns_dslline_load As TextBox
    Friend WithEvents txtbox_ns_faxline_load As TextBox
    Friend WithEvents txtbox_ns_boline_load As TextBox
    Friend WithEvents ns_star3 As Label
    Friend WithEvents ns_star21 As Label
    Friend WithEvents ns_star22 As Label
    Friend WithEvents grpbox_ns_network_info As GroupBox
    Friend WithEvents ns_star19 As Label
    Friend WithEvents ns_star11 As Label
    Friend WithEvents lbl_ns_username As Label
    Friend WithEvents lbl_ns_isp As Label
    Friend WithEvents lbl_ns_ip As Label
    Friend WithEvents lbl_ns_password As Label
    Friend WithEvents lbl_ns_speed As Label
    Friend WithEvents lbl_ns_wifispeed As Label
    Friend WithEvents lbl_rtr_location As Label
    Friend WithEvents lbl_ns_cabinet_lock As Label
    Friend WithEvents txtbox_ns_ip_load As TextBox
    Friend WithEvents txtbox_ns_isp_load As TextBox
    Friend WithEvents txtbox_ns_user_load As TextBox
    Friend WithEvents txtbox_ns_pwd_load As TextBox
    Friend WithEvents txtbox_ns_speed_load As TextBox
    Friend WithEvents txtbox_ns_cablock_load As TextBox
    Friend WithEvents txtbox_ns_wifi_load As TextBox
    Friend WithEvents cmbbox_ns_rlocation_load As ComboBox
    Friend WithEvents lbl_ns_main As Label
    Friend WithEvents btn_ns_cancel As Button
    Friend WithEvents btn_ns_save As Button
    Friend WithEvents btn_ns_clear As Button
    Friend WithEvents ns_star18 As Label
    Friend WithEvents lbl_ns_location As Label
    Friend WithEvents ns_star23 As Label
    Friend WithEvents cmbbox_ns_location_load As ComboBox
    Friend WithEvents ns_star4 As Label
    Friend WithEvents cmbbox_ns_ispstatus_load As ComboBox
    Friend WithEvents lbl_ns_ispstatus As Label
    Friend WithEvents lbl_ns_ispaddress As Label
    Friend WithEvents txtbox_ns_ispaddress_load As TextBox
    Friend WithEvents txtbox_ns_ispgateway_load As TextBox
    Friend WithEvents lbl_ns_ispgateway As Label
    Friend WithEvents txtbox_ns_ispsubnet_load As TextBox
    Friend WithEvents lbl_ns_ispsubnet As Label
    Friend WithEvents lbl_ns_ispaccount As Label
    Friend WithEvents ns_star17 As Label
    Friend WithEvents ns_star16 As Label
    Friend WithEvents ns_star15 As Label
    Friend WithEvents ns_star14 As Label
    Friend WithEvents ns_star13 As Label
    Friend WithEvents ns_star12 As Label
    Friend WithEvents txtbox_ns_ispaccount_load As TextBox
    Friend WithEvents lbl_ns_spare_modem As Label
    Friend WithEvents cmbbox_ns_smodem_load As ComboBox
    Friend WithEvents lbl_ns_spare_router As Label
    Friend WithEvents cmbbox_ns_srouter_load As ComboBox
    Friend WithEvents lbl_ns_spare_wireless_card As Label
    Friend WithEvents cmbbox_ns_swc_load As ComboBox
    Friend WithEvents ns_star24 As Label
    Friend WithEvents ns_star25 As Label
    Friend WithEvents ns_star26 As Label
    Friend WithEvents lbl_ns_spare_holiday_cash As Label
    Friend WithEvents lbl_ns_spare_pinpad As Label
    Friend WithEvents cmbbox_ns_hpc_load As ComboBox
    Friend WithEvents cmbbox_ns_sppad_load As ComboBox
    Friend WithEvents ns_star27 As Label
    Friend WithEvents ns_star28 As Label
    Friend WithEvents cmbbox_ns_spare_equip As GroupBox
    Friend WithEvents lbl_ns_store_system As Label
    Friend WithEvents cmbbox_ns_store_system As ComboBox
    Friend WithEvents ns_star29 As Label
    Friend WithEvents grpbox_ns_ip_pinpads As GroupBox
    Friend WithEvents lbl_ns_pinpads As Label
    Friend WithEvents cmbbox_ns_pinpad_load As ComboBox
    Friend WithEvents ns_star30 As Label
    Friend WithEvents cmbbox_ns_province_load As ComboBox
End Class
