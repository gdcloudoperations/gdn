﻿'AUTHOR: Alex Dumitrascu
'DATE: 31-01-2018
'UPDATE: 28-02-2018
'
'FORM: Store_new.vb (access from MAIN)


Imports System.Text.RegularExpressions

Public Class Store_new

    Private regEx As Regex                       'for regular exp

    Private Sub lbl_ns_status_Click(sender As Object, e As EventArgs) Handles lbl_ns_status.Click

    End Sub

    'ON LOAD: STORE_NEW
    Private Sub Store_new_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'STORE ALWAYS SET TO OPEN ON A NEW STORE
        cmbbox_ns_status_load.SelectedItem = "OPEN"
        cmbbox_ns_status_load.Enabled = False

        'STORE ALWAYS ON MAIN LOCATION ON A NEW STORE
        cmbbox_ns_location_load.SelectedItem = "MAIN"
        cmbbox_ns_location_load.Enabled = False

        'LOAD TOOLTIPS
        gdx_main.loadToolTips(lbl_ns_storeno, "", "ns_store")         'store number
        gdx_main.loadToolTips(lbl_ns_str_banner, "", "ns_banner")     'banner
        gdx_main.loadToolTips(lbl_ns_store_system, "", "ns_ssystem")  'store system
        gdx_main.loadToolTips(lbll_ns_str_sisstr, "", "ns_sstore")    'sister store number
        gdx_main.loadToolTips(lbl_ns_str_mall, "", "ns_mall")         'mall
        gdx_main.loadToolTips(lbl_ns_str_add, "", "ns_address")       'address
        gdx_main.loadToolTips(lbl_ns_str_city, "", "ns_city")         'city
        gdx_main.loadToolTips(lbl_ns_str_prov, "", "ns_state")        'state/province
        gdx_main.loadToolTips(lbl_ns_str_pcode, "", "ns_pcode")       'postal code
        gdx_main.loadToolTips(lbl_ns_str_cntry, "", "ns_countrye")    'Country
        gdx_main.loadToolTips(lbl_ns_str_ph, "", "ns_mline")          'main line
        gdx_main.loadToolTips(lbl_ns_str_boline, "", "ns_boline")     'bo line
        gdx_main.loadToolTips(lbl_ns_str_faxline, "", "ns_faxline")   'fax line
        gdx_main.loadToolTips(lbl_ns_str_dslline, "", "ns_dsl")       'internet line
        gdx_main.loadToolTips(lbl_ns_ip, "", "ns_ip")                 'ip 3 octets
        gdx_main.loadToolTips(lbl_ns_isp, "", "ns_isp")               'ISP
        gdx_main.loadToolTips(lbl_ns_ispaccount, "", "ns_ispaccount") 'ISP Account
        gdx_main.loadToolTips(lbl_ns_ispstatus, "", "ns_ispstatus")   'ISP Address Asign
        gdx_main.loadToolTips(lbl_ns_ispaddress, "", "ns_ispaddress") 'ISP IP Address
        gdx_main.loadToolTips(lbl_ns_ispsubnet, "", "ns_ispsubnet")   'ISP Subnet
        gdx_main.loadToolTips(lbl_ns_ispgateway, "", "ns_ispgateway") 'ISP Default Gateway
        gdx_main.loadToolTips(lbl_ns_username, "", "ns_user")         'Username
        gdx_main.loadToolTips(lbl_ns_password, "", "ns_pwd")          'Password
        gdx_main.loadToolTips(lbl_ns_speed, "", "ns_speed")           'store speed
        gdx_main.loadToolTips(lbl_ns_wifispeed, "", "ns_wifi")        'WiFi speed
        gdx_main.loadToolTips(lbl_rtr_location, "", "ns_rlocation")   'Router location
        gdx_main.loadToolTips(lbl_ns_cabinet_lock, "", "ns_cabinet")  'it cabinet lock
        gdx_main.loadToolTips(lbl_ns_iscombo, "", "ns_combo")         'is combo
        gdx_main.loadToolTips(lbl_ns_pinpads, "", "ns_pinpads")       'pinpads connection
        gdx_main.loadToolTips(lbl_ns_isremote, "", "ns_remote")       'is remote
        gdx_main.loadToolTips(lbl_ns_status, "", "ns_status")         'store status
        gdx_main.loadToolTips(lbl_ns_location, "", "ns_location")     'store location

        'remove * from mandatory fields (case fields only)
        ns_star18.Text = ""     'password field
        ns_star4.Text = ""      'Sister store (for combo)
        ns_star17.Text = ""     'ISP username
        ns_star18.Text = ""     'ISP password
        ns_star14.Text = ""     'ISP Static IP
        ns_star15.Text = ""     'ISP Static Subnet
        ns_star16.Text = ""     'ISP Static Gateway


    End Sub

    'BUTTON: CLOSE - cancel new store
    Private Sub btn_ns_cancel_Click(sender As Object, e As EventArgs) Handles btn_ns_cancel.Click
        Me.Dispose()
    End Sub

    'BUTTON: SAVE - add new store
    Private Sub btn_ns_save_Click(sender As Object, e As EventArgs) Handles btn_ns_save.Click

        'CHECK FIELDS INPUT
        Dim error_fields As String = Nothing
        Dim sstore_str As String = Nothing
        Dim storeno_int As Integer = Nothing
        'STORE NO
        If txtbox_ns_storeno_load.Text = "" Then
            error_fields = "Missing store number!" & vbNewLine
            txtbox_ns_storeno_load.Clear()
            txtbox_ns_storeno_load.BackColor = Color.DarkSalmon
        ElseIf Not IsNumeric(txtbox_ns_storeno_load.Text) Then
            error_fields = "Invalid store #. Digits only!" & vbNewLine
            txtbox_ns_storeno_load.Clear()
            txtbox_ns_storeno_load.BackColor = Color.DarkSalmon
        Else
            txtbox_ns_storeno_load.BackColor = Color.White
            error_fields = ""
            storeno_int = Integer.Parse(txtbox_ns_storeno_load.Text)
        End If

        'BANNER
        If cmbbox_ns_banner_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing store banner!" & vbNewLine
            cmbbox_ns_banner_load.ResetText()
            cmbbox_ns_banner_load.BackColor = Color.DarkSalmon
        Else
            cmbbox_ns_banner_load.BackColor = Color.White
            error_fields = error_fields & ""
        End If

        'storeSystem
        If cmbbox_ns_store_system.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing storeSystem!" & vbNewLine
            cmbbox_ns_store_system.BackColor = Color.DarkSalmon
        Else
            cmbbox_ns_store_system.BackColor = Color.White
            error_fields = error_fields & ""
        End If

        'SISTER STORE (if combo sis store mandatory)
        If cmbbox_ns_iscombo_load.SelectedItem = "YES" Then
            If txtbox_ns_sisstore_load.Text = Nothing Then
                error_fields = error_fields & "Combo store selected. Must have a sister store!" & vbNewLine
                txtbox_ns_sisstore_load.BackColor = Color.DarkSalmon
                txtbox_ns_sisstore_load.Clear()
            ElseIf Not IsNumeric(txtbox_ns_sisstore_load.Text) Then
                error_fields = error_fields & "Invalid sister store #. Digits only!" & vbNewLine
                txtbox_ns_sisstore_load.BackColor = Color.DarkSalmon
                txtbox_ns_sisstore_load.Clear()
            ElseIf txtbox_ns_storeno_load.Text = txtbox_ns_sisstore_load.Text Then
                error_fields = error_fields & "Invalid sister store #. Cannot match store number!" & vbNewLine
                txtbox_ns_sisstore_load.Clear()
                txtbox_ns_sisstore_load.BackColor = Color.DarkSalmon
            Else
                txtbox_ns_sisstore_load.BackColor = Color.White
                sstore_str = txtbox_ns_sisstore_load.Text
            End If

        ElseIf cmbbox_ns_iscombo_load.SelectedItem = "NO" Then
            If txtbox_ns_sisstore_load.Text = Nothing Then
                error_fields = error_fields & ""
                txtbox_ns_sisstore_load.BackColor = Color.White
            ElseIf Not IsNumeric(txtbox_ns_sisstore_load.Text) Then
                error_fields = error_fields & "Invalid sister store #. Digits only!" & vbNewLine
                txtbox_ns_sisstore_load.BackColor = Color.DarkSalmon
                txtbox_ns_sisstore_load.Clear()
            ElseIf txtbox_ns_storeno_load.Text = txtbox_ns_sisstore_load.Text Then
                error_fields = error_fields & "Invalid sister store #. Cannot match store number!" & vbNewLine
                txtbox_ns_sisstore_load.Clear()
                txtbox_ns_sisstore_load.BackColor = Color.DarkSalmon
            Else
                'sstore_str = Integer.Parse(txtbox_ns_sisstore_load.Text)
                sstore_str = txtbox_ns_sisstore_load.Text
                txtbox_ns_sisstore_load.BackColor = Color.White
            End If

        End If

        'MALL
        If txtbox_ns_mall_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: MALL" & vbNewLine
            txtbox_ns_mall_load.Clear()
            txtbox_ns_mall_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_ns_mall_load.BackColor = Color.White
        End If

        'ADDRESS
        If txtbox_ns_address_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: ADDRESS" & vbNewLine
            txtbox_ns_address_load.Clear()
            txtbox_ns_address_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_ns_address_load.BackColor = Color.White
        End If

        'CITY
        If txtbox_ns_city_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: CITY" & vbNewLine
            txtbox_ns_city_load.Clear()
            txtbox_ns_city_load.BackColor = Color.DarkSalmon
        ElseIf Not IsNumeric(txtbox_ns_city_load.Text) Then
            error_fields = error_fields & ""
            txtbox_ns_city_load.BackColor = Color.White
        Else
            error_fields = error_fields & "Invalid city. Characters only!" & vbNewLine
            txtbox_ns_city_load.Clear()
            txtbox_ns_city_load.BackColor = Color.DarkSalmon
        End If

        'STATE
        If cmbbox_ns_province_load.SelectedItem = "" Then
            error_fields = error_fields & "Incomplete address: PROVINCE/STATE" & vbNewLine
            cmbbox_ns_province_load.ResetText()
            cmbbox_ns_province_load.BackColor = Color.DarkSalmon
        ElseIf IsNumeric(cmbbox_ns_province_load.SelectedItem) Then
            error_fields = error_fields & "Invalid Province/State. Digits only!" & vbNewLine
            cmbbox_ns_province_load.ResetText()
            cmbbox_ns_province_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_province_load.BackColor = Color.White
        End If

        'POSTAL CODE        
        Dim pcode_patern_ca As String = "[A-Za-z]\d[A-Za-z] \d[A-Za-z]\d"
        Dim pcode_patern_us As String = "^\d{5}$"

        If txtbox_ns_pcode_load.Text = "" Then
            error_fields = error_fields & "Incomplete address: POSTAL CODE" & vbNewLine
            txtbox_ns_pcode_load.Clear()
            txtbox_ns_pcode_load.BackColor = Color.DarkSalmon

            'CHECK CANADIAN POSTAL CODES
        ElseIf cmbbox_ns_country_load.SelectedItem = "Canada" Then
            If Regex.IsMatch(txtbox_ns_pcode_load.Text, pcode_patern_ca) = False Then
                error_fields = error_fields & "Invalid Postal Code. Canadian postal code only!" & vbNewLine
                txtbox_ns_pcode_load.Clear()
                txtbox_ns_pcode_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_pcode_load.BackColor = Color.White
            End If

            'CHECK US POSTAL CODES
        ElseIf cmbbox_ns_country_load.SelectedItem = "US" Then
            If Regex.IsMatch(txtbox_ns_pcode_load.Text, pcode_patern_us) = False Then
                error_fields = error_fields & "Invalid Postal Code. US postal code only!" & vbNewLine
                txtbox_ns_pcode_load.Clear()
                txtbox_ns_pcode_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_pcode_load.BackColor = Color.White
            End If
        Else
            error_fields = error_fields & ""
            txtbox_ns_pcode_load.BackColor = Color.White
        End If

        'COUNTRY
        If cmbbox_ns_country_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Incomplete address: COUNTRY" & vbNewLine
            cmbbox_ns_country_load.ResetText()
            cmbbox_ns_country_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_country_load.BackColor = Color.White
        End If

        'MAIN LINE
        Dim phone_patern = "\d{10}"
        If txtbox_ns_mainline_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_ns_mainline_load.Text = ""
            txtbox_ns_mainline_load.BackColor = Color.White
        Else
            If Not IsNumeric(txtbox_ns_mainline_load.Text) Then
                error_fields = error_fields & "Invalid Main Line. Digits only!" & vbNewLine
                txtbox_ns_mainline_load.Clear()
                txtbox_ns_mainline_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_ns_mainline_load.Text, phone_patern) = False Then
                error_fields = error_fields & "Invalid Main Line. Follow format 5145557777" & vbNewLine
                txtbox_ns_mainline_load.Clear()
                txtbox_ns_mainline_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_mainline_load.BackColor = Color.White
            End If
        End If


        'BO LINE
        If txtbox_ns_boline_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_ns_boline_load.BackColor = Color.White
        Else
            If Not IsNumeric(txtbox_ns_boline_load.Text) Then
                error_fields = error_fields & "Invalid BO Line. Digits only!" & vbNewLine
                txtbox_ns_boline_load.Clear()
                txtbox_ns_boline_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_ns_boline_load.Text, phone_patern) = False Then
                error_fields = error_fields & "Invalid BO Line. Follow format 5145557777" & vbNewLine
                txtbox_ns_boline_load.Clear()
                txtbox_ns_boline_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_boline_load.BackColor = Color.White
            End If
        End If

        'FAX LINE
        If txtbox_ns_faxline_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_ns_faxline_load.BackColor = Color.White
        Else
            If Not IsNumeric(txtbox_ns_faxline_load.Text) Then
                error_fields = error_fields & "Invalid Fax Line. Digits only!" & vbNewLine
                txtbox_ns_faxline_load.Clear()
                txtbox_ns_faxline_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_ns_faxline_load.Text, phone_patern) = False Then
                error_fields = error_fields & "Invalid Fax Line. Follow format 5145557777" & vbNewLine
                txtbox_ns_faxline_load.Clear()
                txtbox_ns_faxline_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_faxline_load.BackColor = Color.White
            End If
        End If

        'Internet LINE
        If txtbox_ns_dslline_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_ns_dslline_load.BackColor = Color.White
        Else

            If Not IsNumeric(txtbox_ns_dslline_load.Text) Then
                error_fields = error_fields & "Invalid Internet Line. Digits only!" & vbNewLine
                txtbox_ns_dslline_load.Clear()
                txtbox_ns_dslline_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_ns_dslline_load.Text, phone_patern) = False Then
                error_fields = error_fields & "Invalid Internet Line. Follow format 5145557777" & vbNewLine
                txtbox_ns_dslline_load.Clear()
                txtbox_ns_dslline_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_dslline_load.BackColor = Color.White
            End If
        End If

        'IP
        Dim ip_patern = "10.([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).$"
        Dim isp_ip_patern = "^([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5]).([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$"

        If Regex.IsMatch(txtbox_ns_ip_load.Text, ip_patern) = False Then
            error_fields = error_fields & "Invalid Store IP Address. Follow format 10.99.99." & vbNewLine
            txtbox_ns_ip_load.Clear()
            txtbox_ns_ip_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_ns_ip_load.BackColor = Color.White
        End If

        'ISP check
        If txtbox_ns_isp_load.Text = "" Then
            error_fields = error_fields & "Missing ISP name." & vbNewLine
            txtbox_ns_isp_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_ns_isp_load.BackColor = Color.White
        End If

        'ISP Address assignment, Username, Password, ISP_IP, ISP_Subnet, ISP_Gateway
        If cmbbox_ns_ispstatus_load.SelectedItem = "PPPOE" Then
            If txtbox_ns_user_load.Text = "" Then
                error_fields = error_fields & "PPPOE selected. Missing ISP username!" & vbNewLine
                txtbox_ns_user_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_user_load.BackColor = Color.White
            End If

            If txtbox_ns_pwd_load.Text = "" Then
                error_fields = error_fields & "PPPOE selected. Missing ISP Password!" & vbNewLine
                txtbox_ns_pwd_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_pwd_load.BackColor = Color.White
            End If

            'clear unnecessary info
            txtbox_ns_ispaddress_load.Clear()
            txtbox_ns_ispsubnet_load.Clear()
            txtbox_ns_ispgateway_load.Clear()

        ElseIf cmbbox_ns_ispstatus_load.SelectedItem = "Static" Then
            If txtbox_ns_ispaddress_load.Text = "" Then
                error_fields = error_fields & "Static selected. Missing ISP IP Address!" & vbNewLine
                txtbox_ns_ispaddress_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_ns_ispaddress_load.Text, isp_ip_patern) = False Then
                error_fields = error_fields & "Invalid ISP IP Address. Follow format 192.168.1.1" & vbNewLine
                txtbox_ns_ispaddress_load.Clear()
                txtbox_ns_ispaddress_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_ispaddress_load.BackColor = Color.White
            End If

            If txtbox_ns_ispsubnet_load.Text = "" Then
                error_fields = error_fields & "Static selected. Missing ISP Subnet!" & vbNewLine
                txtbox_ns_ispsubnet_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_ns_ispsubnet_load.Text, isp_ip_patern) = False Then
                error_fields = error_fields & "Invalid ISP Subnet. Follow format 255.255.255.0" & vbNewLine
                txtbox_ns_ispsubnet_load.Clear()
                txtbox_ns_ispsubnet_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_ispsubnet_load.BackColor = Color.White
            End If

            If txtbox_ns_ispgateway_load.Text = "" Then
                error_fields = error_fields & "Static selected. Missing ISP Gateway!" & vbNewLine
                txtbox_ns_ispgateway_load.BackColor = Color.DarkSalmon
            ElseIf Regex.IsMatch(txtbox_ns_ispgateway_load.Text, isp_ip_patern) = False Then
                error_fields = error_fields & "Invalid ISP Gateway. Follow format 192.168.2.1" & vbNewLine
                txtbox_ns_ispgateway_load.Clear()
                txtbox_ns_ispgateway_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_ispgateway_load.BackColor = Color.White
            End If

            'clear unnecessary info
            txtbox_ns_user_load.Clear()
            txtbox_ns_pwd_load.Clear()

        ElseIf cmbbox_ns_ispstatus_load.SelectedItem = "DHCP" Then
            error_fields = error_fields & ""
            cmbbox_ns_ispstatus_load.BackColor = Color.White

            'clear unnecessary info
            txtbox_ns_ispaddress_load.Clear()
            txtbox_ns_ispsubnet_load.Clear()
            txtbox_ns_ispgateway_load.Clear()
            txtbox_ns_user_load.Clear()
            txtbox_ns_pwd_load.Clear()

        Else
            error_fields = error_fields & "Missing ISP Address Assignment!" & vbNewLine
            cmbbox_ns_ispstatus_load.BackColor = Color.DarkSalmon
        End If


        'Store Speed
        Dim sspeed_patern = "[0-9]*\.?[0-9]+ \/ ?[0-9]*\.?[0-9]+"
        If txtbox_ns_speed_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_ns_speed_load.BackColor = Color.White
        ElseIf Regex.IsMatch(txtbox_ns_speed_load.Text, sspeed_patern) = False Then
            error_fields = error_fields & "Invalid Store Speed. Follow format 10 / 10" & vbNewLine
            txtbox_ns_speed_load.Clear()
            txtbox_ns_speed_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_ns_speed_load.BackColor = Color.White
        End If

        'WiFi Speed
        Dim wifi_patern = "^[0-9]*\.?[0-9]+$"
        If txtbox_ns_wifi_load.Text = "" Then
            error_fields = error_fields & ""
            txtbox_ns_wifi_load.BackColor = Color.White
        ElseIf Regex.IsMatch(txtbox_ns_wifi_load.Text, wifi_patern) = False Then
            error_fields = error_fields & "Invalid Store WiFi Speed. Follow format 0.6" & vbNewLine
            txtbox_ns_wifi_load.Clear()
            txtbox_ns_wifi_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            txtbox_ns_wifi_load.BackColor = Color.White
        End If

        'Router Location
        If cmbbox_ns_rlocation_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing Router Location!" & vbNewLine
            cmbbox_ns_rlocation_load.ResetText()
            cmbbox_ns_rlocation_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_rlocation_load.BackColor = Color.White
        End If

        'Cabinet Lock
        Dim itlock = "^\d{4}$"

        If Not txtbox_ns_cablock_load.Text = "" Then

            If Regex.IsMatch(txtbox_ns_cablock_load.Text, itlock) = False Then
                error_fields = error_fields & "Invalid IT Cabinet Lock. Digits only!" & vbNewLine
                txtbox_ns_cablock_load.Clear()
                txtbox_ns_cablock_load.BackColor = Color.DarkSalmon
            Else
                error_fields = error_fields & ""
                txtbox_ns_cablock_load.BackColor = Color.White
            End If
        Else
            error_fields = error_fields & ""
            txtbox_ns_cablock_load.BackColor = Color.White
        End If

        'SPARE: modem
        If cmbbox_ns_smodem_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Modem info!" & vbNewLine
            cmbbox_ns_smodem_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_smodem_load.BackColor = Color.White
        End If

        'SPARE: router
        If cmbbox_ns_srouter_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Router info!" & vbNewLine
            cmbbox_ns_srouter_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_srouter_load.BackColor = Color.White
        End If

        'SPARE: wireless card
        If cmbbox_ns_swc_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Wireless Card info!" & vbNewLine
            cmbbox_ns_swc_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_swc_load.BackColor = Color.White
        End If

        'SPARE: holiday cash
        If cmbbox_ns_hpc_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Holiday PC info!" & vbNewLine
            cmbbox_ns_hpc_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_hpc_load.BackColor = Color.White
        End If

        'SPARE: pinpad
        If cmbbox_ns_sppad_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing Spare Pinpad info!" & vbNewLine
            cmbbox_ns_sppad_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_sppad_load.BackColor = Color.White
        End If

        'isCOMBO
        If cmbbox_ns_iscombo_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing COMBO status!" & vbNewLine
            cmbbox_ns_iscombo_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_iscombo_load.BackColor = Color.White
        End If

        'pinpads
        If cmbbox_ns_pinpad_load.SelectedItem = Nothing Then
            error_fields = error_fields & "Missing Pinpad connection info!" & vbNewLine
            cmbbox_ns_pinpad_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_pinpad_load.BackColor = Color.White
        End If

        'isREMOTE
        If cmbbox_ns_isremote_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing REMOTE status!" & vbNewLine
            cmbbox_ns_isremote_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_isremote_load.BackColor = Color.White
        End If

        'STATUS (ALWAYS OPEN FOR NOW)
        If cmbbox_ns_status_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing store STATUS!" & vbNewLine
            cmbbox_ns_status_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_status_load.BackColor = Color.White
        End If

        'STORE LOCATION
        If cmbbox_ns_location_load.SelectedItem = "" Then
            error_fields = error_fields & "Missing store Location!" & vbNewLine
            cmbbox_ns_location_load.BackColor = Color.DarkSalmon
        Else
            error_fields = error_fields & ""
            cmbbox_ns_location_load.BackColor = Color.White
        End If

        'SET YES/NO to BITS
        'COMBO
        Dim combo_int As Integer = Nothing
        If cmbbox_ns_iscombo_load.SelectedItem = "YES" Then
            combo_int = 1
        Else
            combo_int = 0
        End If
        'REMOTE
        Dim remote_int As Integer = Nothing
        If cmbbox_ns_isremote_load.SelectedItem = "YES" Then
            remote_int = 1
        Else
            remote_int = 0
        End If
        'STATUS
        Dim status_int As Integer = Nothing
        If cmbbox_ns_status_load.SelectedItem = "OPEN" Then
            status_int = 1
        Else
            status_int = 0
        End If

        'STORE LOCATION
        Dim location_int As Integer = Nothing
        If cmbbox_ns_location_load.SelectedItem = "TEMP" Then
            location_int = 1
        Else
            location_int = 0
        End If

        'MODEM
        Dim smodem_int As Integer = Nothing
        If cmbbox_ns_smodem_load.SelectedItem = "YES" Then
            smodem_int = 1
        Else
            smodem_int = 0
        End If
        'ROUTER
        Dim srouter_int As Integer = Nothing
        If cmbbox_ns_srouter_load.SelectedItem = "YES" Then
            srouter_int = 1
        Else
            srouter_int = 0
        End If
        'WIRELESS CARD
        Dim swc_int As Integer = Nothing
        If cmbbox_ns_swc_load.SelectedItem = "YES" Then
            swc_int = 1
        Else
            swc_int = 0
        End If
        'HOLIDAY PC
        Dim spc_int As Integer = Nothing
        If cmbbox_ns_hpc_load.SelectedItem = "YES" Then
            spc_int = 1
        Else
            spc_int = 0
        End If
        'PINPAD
        Dim spp_int As Integer = Nothing
        If cmbbox_ns_sppad_load.SelectedItem = "YES" Then
            spp_int = 1
        Else
            spp_int = 0
        End If


        'Display error
        If error_fields = "" Then

            gdx_main.addStore(storeno_int, cmbbox_ns_banner_load.SelectedItem, cmbbox_ns_store_system.SelectedItem, combo_int, cmbbox_ns_pinpad_load.SelectedItem, remote_int,
                              status_int, location_int, sstore_str, txtbox_ns_mall_load.Text, txtbox_ns_address_load.Text, txtbox_ns_city_load.Text, cmbbox_ns_province_load.SelectedItem,
                              UCase(txtbox_ns_pcode_load.Text), cmbbox_ns_country_load.SelectedItem, txtbox_ns_mainline_load.Text, txtbox_ns_boline_load.Text, txtbox_ns_faxline_load.Text,
                              txtbox_ns_dslline_load.Text, txtbox_ns_ip_load.Text, txtbox_ns_isp_load.Text, txtbox_ns_ispaccount_load.Text, cmbbox_ns_ispstatus_load.SelectedItem,
                              txtbox_ns_ispaddress_load.Text, txtbox_ns_ispsubnet_load.Text, txtbox_ns_ispgateway_load.Text, txtbox_ns_speed_load.Text, txtbox_ns_wifi_load.Text,
                              txtbox_ns_user_load.Text, txtbox_ns_pwd_load.Text, cmbbox_ns_rlocation_load.SelectedItem, txtbox_ns_cablock_load.Text,
                              smodem_int, srouter_int, swc_int, spc_int, spp_int)

            If gdx_main.add_flag = True Then
                clear_addstore("no", False)
                gdx_main.add_flag = False 'set flag to false

            End If

            'CHECK AND AUTO-ADD SIS STORE IF NEEDED OR THROW EXEPTION
            If cmbbox_ns_iscombo_load.SelectedItem = "NO" Then
                'IF COMBO NO BUT HAS SISTER STORE
                If txtbox_ns_sisstore_load.Text = "" Then
                    'clear all form
                    clear_addstore("no", False)
                Else
                    MsgBox("Please enter sister store information.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (sister_store - add)")
                    'clear only info that is different
                    clear_addstore("yes", False)
                    gdx_main.add_flag = True
                End If
            ElseIf cmbbox_ns_iscombo_load.SelectedItem = "YES" Then

                MsgBox("Please check if sister store COMBO info is correct!", MsgBoxStyle.Exclamation, Title:="GDnetworks - Info! (combo - add)")

                clear_addstore("yes", True)

                gdx_main.add_flag = True

            End If

        Else
            MsgBox("Please correct the following errors before adding the new store:" & vbNewLine & vbNewLine & error_fields)
        End If

    End Sub

    'clear entire form or clear form without store
    Protected Friend Sub clear_addstore(sstore As String, combo As Boolean)

        'IF NEW STORE HAS SISTER STORE
        If sstore = "yes" Then

            Dim new_main As String = txtbox_ns_sisstore_load.Text
            Dim new_sister As String = txtbox_ns_storeno_load.Text
            Dim old_banner As String = cmbbox_ns_banner_load.SelectedItem

            'load SS store#
            txtbox_ns_storeno_load.Text = new_main
            'load SS banner
            If old_banner = "Dynamite" Then
                cmbbox_ns_banner_load.SelectedItem = "Garage"
            ElseIf old_banner = "Dynamite HO" Then
                cmbbox_ns_banner_load.SelectedItem = "Garage HO"
            ElseIf old_banner = "Dynamite combo" Then
                cmbbox_ns_banner_load.SelectedItem = "Garage combo"
            ElseIf old_banner = "Dynamite USA" Then
                cmbbox_ns_banner_load.SelectedItem = "Garage USA"
            ElseIf old_banner = "Garage" Then
                cmbbox_ns_banner_load.SelectedItem = "Dynamite"
            ElseIf old_banner = "Garage HO" Then
                cmbbox_ns_banner_load.SelectedItem = "Dynamite HO"
            ElseIf old_banner = "Garage combo" Then
                cmbbox_ns_banner_load.SelectedItem = "Dynamite combo"
            ElseIf old_banner = "Garage USA" Then
                cmbbox_ns_banner_load.SelectedItem = "Dynamite USA"
            End If
            'load ss
            txtbox_ns_sisstore_load.Text = new_sister

            'IF STORE IS COMBO
            If combo = False Then

                cmbbox_ns_store_system.SelectedValue = -1
                cmbbox_ns_iscombo_load.SelectedItem = "NO"
                txtbox_ns_address_load.Text = ""
                txtbox_ns_pcode_load.Text = ""
                txtbox_ns_mainline_load.Text = ""
                txtbox_ns_boline_load.Text = ""
                txtbox_ns_faxline_load.Text = ""
                txtbox_ns_dslline_load.Text = ""
                txtbox_ns_ip_load.Text = ""
                txtbox_ns_isp_load.Text = ""
                txtbox_ns_ispaccount_load.Text = ""
                cmbbox_ns_ispstatus_load.SelectedValue = -1
                txtbox_ns_ispaddress_load.Text = ""
                txtbox_ns_ispsubnet_load.Text = ""
                txtbox_ns_ispgateway_load.Text = ""
                txtbox_ns_user_load.Text = ""
                txtbox_ns_pwd_load.Text = ""
                txtbox_ns_speed_load.Text = ""
                txtbox_ns_wifi_load.Text = ""
                cmbbox_ns_rlocation_load.ResetText()
                txtbox_ns_cablock_load.Text = ""
                cmbbox_ns_smodem_load.ResetText()
                cmbbox_ns_srouter_load.ResetText()
                cmbbox_ns_swc_load.ResetText()
                cmbbox_ns_hpc_load.ResetText()
                cmbbox_ns_sppad_load.ResetText()
                cmbbox_ns_isremote_load.ResetText()
                cmbbox_ns_pinpad_load.SelectedValue = -1

            Else

            End If

        ElseIf sstore = "no" Then

            txtbox_ns_storeno_load.Text = ""
            cmbbox_ns_banner_load.ResetText()
            cmbbox_ns_store_system.ResetText()
            cmbbox_ns_iscombo_load.ResetText()
            txtbox_ns_sisstore_load.Text = ""
            txtbox_ns_mall_load.Text = ""
            txtbox_ns_address_load.Text = ""
            txtbox_ns_city_load.Text = ""
            cmbbox_ns_province_load.ResetText()
            txtbox_ns_pcode_load.Text = ""
            cmbbox_ns_country_load.ResetText()
            txtbox_ns_mainline_load.Text = ""
            txtbox_ns_boline_load.Text = ""
            txtbox_ns_faxline_load.Text = ""
            txtbox_ns_dslline_load.Text = ""
            txtbox_ns_ip_load.Text = ""
            txtbox_ns_isp_load.Text = ""
            txtbox_ns_ispaccount_load.Text = ""
            cmbbox_ns_ispstatus_load.ResetText()
            txtbox_ns_ispaddress_load.Text = ""
            txtbox_ns_ispsubnet_load.Text = ""
            txtbox_ns_ispgateway_load.Text = ""
            txtbox_ns_user_load.Text = ""
            txtbox_ns_pwd_load.Text = ""
            txtbox_ns_speed_load.Text = ""
            txtbox_ns_wifi_load.Text = ""
            cmbbox_ns_rlocation_load.ResetText()
            txtbox_ns_cablock_load.Text = ""
            cmbbox_ns_smodem_load.ResetText()
            cmbbox_ns_srouter_load.ResetText()
            cmbbox_ns_swc_load.ResetText()
            cmbbox_ns_hpc_load.ResetText()
            cmbbox_ns_sppad_load.ResetText()
            cmbbox_ns_isremote_load.ResetText()
            cmbbox_ns_pinpad_load.ResetText()

        End If

    End Sub

    'BUTTON : CLEAR FOR
    Private Sub btn_ns_clear_Click(sender As Object, e As EventArgs) Handles btn_ns_clear.Click

        clear_addstore("no", False)

    End Sub

    'ENABLE MANDATORY FIELD USER & PASSWORD
    Private Sub txtbox_ns_user_load_TextChanged(sender As Object, e As EventArgs) Handles txtbox_ns_user_load.TextChanged

        If Not txtbox_ns_user_load.Text = "" Then
            ns_star18.Text = "*"
        Else
            ns_star18.Text = ""
        End If

    End Sub

    'ENABLE MANDATORY RouterLocation and IT Cabinet lock
    Private Sub cmbbox_ns_rlocation_load_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_ns_rlocation_load.SelectedIndexChanged

    End Sub

    'ENABLE MANDATORY COMBO & SIS STORE
    Private Sub cmbbox_ns_iscombo_load_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_ns_iscombo_load.SelectedIndexChanged

        If cmbbox_ns_iscombo_load.SelectedItem = "YES" Then
            ns_star4.Text = "*"
        Else
            ns_star4.Text = ""
        End If

    End Sub

    'CHECK PPPOE. Static or DHCP input
    Private Sub cmbbox_ns_ispstatus_load_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_ns_ispstatus_load.SelectedIndexChanged

        If cmbbox_ns_ispstatus_load.SelectedItem = "PPPOE" Then
            ns_star17.Text = "*"
            ns_star18.Text = "*"
            ns_star14.Text = ""
            ns_star15.Text = ""
            ns_star16.Text = ""

            txtbox_ns_ispaddress_load.Clear()
            txtbox_ns_ispsubnet_load.Clear()
            txtbox_ns_ispgateway_load.Clear()

        ElseIf cmbbox_ns_ispstatus_load.SelectedItem = "Static" Then

            ns_star17.Text = ""
            ns_star18.Text = ""
            ns_star14.Text = "*"
            ns_star15.Text = "*"
            ns_star16.Text = "*"

            txtbox_ns_user_load.Clear()
            txtbox_ns_pwd_load.Clear()

        ElseIf cmbbox_ns_ispstatus_load.SelectedItem = "DHCP" Then

            ns_star17.Text = ""
            ns_star18.Text = ""
            ns_star14.Text = ""
            ns_star15.Text = ""
            ns_star16.Text = ""

            txtbox_ns_ispaddress_load.Clear()
            txtbox_ns_ispsubnet_load.Clear()
            txtbox_ns_ispgateway_load.Clear()
            txtbox_ns_user_load.Clear()
            txtbox_ns_pwd_load.Clear()

        End If

    End Sub

    'ON CLOSING FORM
    Private Sub store_new_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Me.Dispose()
    End Sub

    Private Sub cmbbox_ns_banner_load_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_ns_banner_load.SelectedIndexChanged

    End Sub

    Private Sub lbl_ns_speed_Click(sender As Object, e As EventArgs) Handles lbl_ns_speed.Click

    End Sub

    'ADD ITEMS FOR PROVINCE COMBO
    Private Sub cmbbox_ns_country_load_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_ns_country_load.SelectedIndexChanged

        'ADD CANADIAN PROVINCES
        If cmbbox_ns_country_load.SelectedItem = "Canada" Then

            'clear all items
            cmbbox_ns_province_load.Items.Clear()

            'add items
            cmbbox_ns_province_load.Items.Add("Alberta")
            cmbbox_ns_province_load.Items.Add("BC")
            cmbbox_ns_province_load.Items.Add("Manitoba")
            cmbbox_ns_province_load.Items.Add("NB")
            cmbbox_ns_province_load.Items.Add("NS")
            cmbbox_ns_province_load.Items.Add("NFLD")
            cmbbox_ns_province_load.Items.Add("Nunavut")
            cmbbox_ns_province_load.Items.Add("NWT")
            cmbbox_ns_province_load.Items.Add("Ontario")
            cmbbox_ns_province_load.Items.Add("PEI")
            cmbbox_ns_province_load.Items.Add("Quebec")
            cmbbox_ns_province_load.Items.Add("Saskatchewan")
            cmbbox_ns_province_load.Items.Add("Yukon")

            'ADD US STATES
        ElseIf cmbbox_ns_country_load.SelectedItem = "US" Then

            'clear all items
            cmbbox_ns_province_load.Items.Clear()

            'add items
            cmbbox_ns_province_load.Items.Add("Alabama")
            cmbbox_ns_province_load.Items.Add("Alaska")
            cmbbox_ns_province_load.Items.Add("American Samoa")
            cmbbox_ns_province_load.Items.Add("Arizona")
            cmbbox_ns_province_load.Items.Add("Arkansas")
            cmbbox_ns_province_load.Items.Add("California")
            cmbbox_ns_province_load.Items.Add("Colorado")
            cmbbox_ns_province_load.Items.Add("Connecticut")
            cmbbox_ns_province_load.Items.Add("Delaware")
            cmbbox_ns_province_load.Items.Add("District of Columbia")
            cmbbox_ns_province_load.Items.Add("Federated States of Micronesia")
            cmbbox_ns_province_load.Items.Add("Florida")
            cmbbox_ns_province_load.Items.Add("Georgia")
            cmbbox_ns_province_load.Items.Add("Guam")
            cmbbox_ns_province_load.Items.Add("Hawaii")
            cmbbox_ns_province_load.Items.Add("Idaho")
            cmbbox_ns_province_load.Items.Add("Illinois")
            cmbbox_ns_province_load.Items.Add("Indiana")
            cmbbox_ns_province_load.Items.Add("Iowa")
            cmbbox_ns_province_load.Items.Add("Kansas")
            cmbbox_ns_province_load.Items.Add("Kentucky")
            cmbbox_ns_province_load.Items.Add("Louisiana")
            cmbbox_ns_province_load.Items.Add("Maine")
            cmbbox_ns_province_load.Items.Add("Marshall Islands")
            cmbbox_ns_province_load.Items.Add("Maryland")
            cmbbox_ns_province_load.Items.Add("Massachusetts")
            cmbbox_ns_province_load.Items.Add("Michigan")
            cmbbox_ns_province_load.Items.Add("Minnesota")
            cmbbox_ns_province_load.Items.Add("Mississippi")
            cmbbox_ns_province_load.Items.Add("Missouri")
            cmbbox_ns_province_load.Items.Add("Montana")
            cmbbox_ns_province_load.Items.Add("Nebraska")
            cmbbox_ns_province_load.Items.Add("Nevada")
            cmbbox_ns_province_load.Items.Add("New Hampshire")
            cmbbox_ns_province_load.Items.Add("New Jersey")
            cmbbox_ns_province_load.Items.Add("New Mexico")
            cmbbox_ns_province_load.Items.Add("New York")
            cmbbox_ns_province_load.Items.Add("North Carolina")
            cmbbox_ns_province_load.Items.Add("North Dakota")
            cmbbox_ns_province_load.Items.Add("Northern Mariana Islands")
            cmbbox_ns_province_load.Items.Add("Ohio")
            cmbbox_ns_province_load.Items.Add("Oklahoma")
            cmbbox_ns_province_load.Items.Add("Oregon")
            cmbbox_ns_province_load.Items.Add("Palau")
            cmbbox_ns_province_load.Items.Add("Pennsylvania")
            cmbbox_ns_province_load.Items.Add("Puerto Rico")
            cmbbox_ns_province_load.Items.Add("Rhode Island")
            cmbbox_ns_province_load.Items.Add("South Carolina")
            cmbbox_ns_province_load.Items.Add("South Dakota")
            cmbbox_ns_province_load.Items.Add("Tennessee")
            cmbbox_ns_province_load.Items.Add("Texas")
            cmbbox_ns_province_load.Items.Add("Utah")
            cmbbox_ns_province_load.Items.Add("Vermont")
            cmbbox_ns_province_load.Items.Add("Virgin Islands")
            cmbbox_ns_province_load.Items.Add("Virginia")
            cmbbox_ns_province_load.Items.Add("Washington")
            cmbbox_ns_province_load.Items.Add("West Virginia")
            cmbbox_ns_province_load.Items.Add("Wisconsin")
            cmbbox_ns_province_load.Items.Add("Wyoming")

        Else

            'clear all items
            cmbbox_ns_province_load.Items.Clear()

        End If

    End Sub

    Private Sub cmbbox_ns_province_load_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbbox_ns_province_load.SelectedIndexChanged

    End Sub
End Class