﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Inventory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Inventory))
        Me.listview_inventory_routers = New System.Windows.Forms.ListView()
        Me.col_rtr_sn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_rtr_device = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_rtr_os = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_rtr_stauts = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_rtr_location = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_rtr_notes = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.contextmenu_inventory_list = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.context_inventory_copy = New System.Windows.Forms.ToolStripMenuItem()
        Me.context_inventory_edit = New System.Windows.Forms.ToolStripMenuItem()
        Me.context_inventory_delete = New System.Windows.Forms.ToolStripMenuItem()
        Me.tab_inventory = New System.Windows.Forms.TabControl()
        Me.tab_inventory_routers = New System.Windows.Forms.TabPage()
        Me.tab_inventory_switches = New System.Windows.Forms.TabPage()
        Me.listview_inventory_switches = New System.Windows.Forms.ListView()
        Me.col_sw_sn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_sw_device = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_sw_os = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_sw_stauts = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_sw_location = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_sw_notes = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.tab_inventory_aps = New System.Windows.Forms.TabPage()
        Me.listview_inventory_aps = New System.Windows.Forms.ListView()
        Me.col_ap_sn = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_ap_device = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_ap_os = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_ap_stauts = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_ap_location = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_ap_notes = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lbl_inventory = New System.Windows.Forms.Label()
        Me.grpbox_inventory_options = New System.Windows.Forms.GroupBox()
        Me.btn_inventory_save = New System.Windows.Forms.Button()
        Me.txtbox_inventory_searchSN = New System.Windows.Forms.TextBox()
        Me.lbl_inventory_searchSN = New System.Windows.Forms.Label()
        Me.btn_inventory_clear = New System.Windows.Forms.Button()
        Me.cmbbox_inventory_notes = New System.Windows.Forms.ComboBox()
        Me.lbl_inventory_notes = New System.Windows.Forms.Label()
        Me.cmbbox_inventory_location = New System.Windows.Forms.ComboBox()
        Me.lbl_inventory_location = New System.Windows.Forms.Label()
        Me.cmbbox_inventory_status = New System.Windows.Forms.ComboBox()
        Me.lbl_inventory_stauts = New System.Windows.Forms.Label()
        Me.lbl_inventory_os = New System.Windows.Forms.Label()
        Me.cmbbox_inventory_os = New System.Windows.Forms.ComboBox()
        Me.btn_inventory_refresh = New System.Windows.Forms.Button()
        Me.chkbox_inventory_adddev = New System.Windows.Forms.CheckBox()
        Me.btn_inventory_add = New System.Windows.Forms.Button()
        Me.btn_inventory_close = New System.Windows.Forms.Button()
        Me.lbl_inventory_total = New System.Windows.Forms.Label()
        Me.btn_inventory_export = New System.Windows.Forms.Button()
        Me.btn_inventory_updateOrion = New System.Windows.Forms.Button()
        Me.contextmenu_inventory_list.SuspendLayout()
        Me.tab_inventory.SuspendLayout()
        Me.tab_inventory_routers.SuspendLayout()
        Me.tab_inventory_switches.SuspendLayout()
        Me.tab_inventory_aps.SuspendLayout()
        Me.grpbox_inventory_options.SuspendLayout()
        Me.SuspendLayout()
        '
        'listview_inventory_routers
        '
        Me.listview_inventory_routers.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.col_rtr_sn, Me.col_rtr_device, Me.col_rtr_os, Me.col_rtr_stauts, Me.col_rtr_location, Me.col_rtr_notes})
        Me.listview_inventory_routers.FullRowSelect = True
        Me.listview_inventory_routers.HideSelection = False
        Me.listview_inventory_routers.Location = New System.Drawing.Point(6, 6)
        Me.listview_inventory_routers.MultiSelect = False
        Me.listview_inventory_routers.Name = "listview_inventory_routers"
        Me.listview_inventory_routers.Size = New System.Drawing.Size(469, 196)
        Me.listview_inventory_routers.TabIndex = 0
        Me.listview_inventory_routers.UseCompatibleStateImageBehavior = False
        Me.listview_inventory_routers.View = System.Windows.Forms.View.Details
        '
        'col_rtr_sn
        '
        Me.col_rtr_sn.Text = "SN"
        '
        'col_rtr_device
        '
        Me.col_rtr_device.Text = "Device"
        '
        'col_rtr_os
        '
        Me.col_rtr_os.Text = "OS"
        '
        'col_rtr_stauts
        '
        Me.col_rtr_stauts.Text = "Stauts"
        '
        'col_rtr_location
        '
        Me.col_rtr_location.Text = "Location"
        '
        'col_rtr_notes
        '
        Me.col_rtr_notes.Text = "Notes"
        '
        'contextmenu_inventory_list
        '
        Me.contextmenu_inventory_list.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.context_inventory_copy, Me.context_inventory_edit, Me.context_inventory_delete})
        Me.contextmenu_inventory_list.Name = "contextmenu_inventory_list"
        Me.contextmenu_inventory_list.Size = New System.Drawing.Size(114, 70)
        '
        'context_inventory_copy
        '
        Me.context_inventory_copy.Name = "context_inventory_copy"
        Me.context_inventory_copy.Size = New System.Drawing.Size(113, 22)
        Me.context_inventory_copy.Text = "COPY"
        '
        'context_inventory_edit
        '
        Me.context_inventory_edit.Name = "context_inventory_edit"
        Me.context_inventory_edit.Size = New System.Drawing.Size(113, 22)
        Me.context_inventory_edit.Text = "EDIT"
        '
        'context_inventory_delete
        '
        Me.context_inventory_delete.Name = "context_inventory_delete"
        Me.context_inventory_delete.Size = New System.Drawing.Size(113, 22)
        Me.context_inventory_delete.Text = "DELETE"
        '
        'tab_inventory
        '
        Me.tab_inventory.Controls.Add(Me.tab_inventory_routers)
        Me.tab_inventory.Controls.Add(Me.tab_inventory_switches)
        Me.tab_inventory.Controls.Add(Me.tab_inventory_aps)
        Me.tab_inventory.Location = New System.Drawing.Point(12, 50)
        Me.tab_inventory.Name = "tab_inventory"
        Me.tab_inventory.SelectedIndex = 0
        Me.tab_inventory.Size = New System.Drawing.Size(489, 234)
        Me.tab_inventory.TabIndex = 1
        '
        'tab_inventory_routers
        '
        Me.tab_inventory_routers.Controls.Add(Me.listview_inventory_routers)
        Me.tab_inventory_routers.Location = New System.Drawing.Point(4, 22)
        Me.tab_inventory_routers.Name = "tab_inventory_routers"
        Me.tab_inventory_routers.Padding = New System.Windows.Forms.Padding(3)
        Me.tab_inventory_routers.Size = New System.Drawing.Size(481, 208)
        Me.tab_inventory_routers.TabIndex = 0
        Me.tab_inventory_routers.Text = "Routers"
        Me.tab_inventory_routers.UseVisualStyleBackColor = True
        '
        'tab_inventory_switches
        '
        Me.tab_inventory_switches.Controls.Add(Me.listview_inventory_switches)
        Me.tab_inventory_switches.Location = New System.Drawing.Point(4, 22)
        Me.tab_inventory_switches.Name = "tab_inventory_switches"
        Me.tab_inventory_switches.Padding = New System.Windows.Forms.Padding(3)
        Me.tab_inventory_switches.Size = New System.Drawing.Size(481, 218)
        Me.tab_inventory_switches.TabIndex = 1
        Me.tab_inventory_switches.Text = "Switches"
        Me.tab_inventory_switches.UseVisualStyleBackColor = True
        '
        'listview_inventory_switches
        '
        Me.listview_inventory_switches.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.col_sw_sn, Me.col_sw_device, Me.col_sw_os, Me.col_sw_stauts, Me.col_sw_location, Me.col_sw_notes})
        Me.listview_inventory_switches.FullRowSelect = True
        Me.listview_inventory_switches.HideSelection = False
        Me.listview_inventory_switches.Location = New System.Drawing.Point(6, 6)
        Me.listview_inventory_switches.MultiSelect = False
        Me.listview_inventory_switches.Name = "listview_inventory_switches"
        Me.listview_inventory_switches.Size = New System.Drawing.Size(469, 196)
        Me.listview_inventory_switches.TabIndex = 1
        Me.listview_inventory_switches.UseCompatibleStateImageBehavior = False
        Me.listview_inventory_switches.View = System.Windows.Forms.View.Details
        '
        'col_sw_sn
        '
        Me.col_sw_sn.Text = "SN"
        '
        'col_sw_device
        '
        Me.col_sw_device.Text = "Device"
        '
        'col_sw_os
        '
        Me.col_sw_os.Text = "OS"
        '
        'col_sw_stauts
        '
        Me.col_sw_stauts.Text = "Stauts"
        '
        'col_sw_location
        '
        Me.col_sw_location.Text = "Location"
        '
        'col_sw_notes
        '
        Me.col_sw_notes.Text = "Notes"
        '
        'tab_inventory_aps
        '
        Me.tab_inventory_aps.Controls.Add(Me.listview_inventory_aps)
        Me.tab_inventory_aps.Location = New System.Drawing.Point(4, 22)
        Me.tab_inventory_aps.Name = "tab_inventory_aps"
        Me.tab_inventory_aps.Padding = New System.Windows.Forms.Padding(3)
        Me.tab_inventory_aps.Size = New System.Drawing.Size(481, 218)
        Me.tab_inventory_aps.TabIndex = 2
        Me.tab_inventory_aps.Text = "APs"
        Me.tab_inventory_aps.UseVisualStyleBackColor = True
        '
        'listview_inventory_aps
        '
        Me.listview_inventory_aps.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.col_ap_sn, Me.col_ap_device, Me.col_ap_os, Me.col_ap_stauts, Me.col_ap_location, Me.col_ap_notes})
        Me.listview_inventory_aps.FullRowSelect = True
        Me.listview_inventory_aps.HideSelection = False
        Me.listview_inventory_aps.Location = New System.Drawing.Point(6, 6)
        Me.listview_inventory_aps.MultiSelect = False
        Me.listview_inventory_aps.Name = "listview_inventory_aps"
        Me.listview_inventory_aps.Size = New System.Drawing.Size(469, 196)
        Me.listview_inventory_aps.TabIndex = 2
        Me.listview_inventory_aps.UseCompatibleStateImageBehavior = False
        Me.listview_inventory_aps.View = System.Windows.Forms.View.Details
        '
        'col_ap_sn
        '
        Me.col_ap_sn.Text = "SN"
        '
        'col_ap_device
        '
        Me.col_ap_device.Text = "Device"
        '
        'col_ap_os
        '
        Me.col_ap_os.Text = "OS"
        '
        'col_ap_stauts
        '
        Me.col_ap_stauts.Text = "Stauts"
        '
        'col_ap_location
        '
        Me.col_ap_location.Text = "Location"
        '
        'col_ap_notes
        '
        Me.col_ap_notes.Text = "Notes"
        '
        'lbl_inventory
        '
        Me.lbl_inventory.AutoSize = True
        Me.lbl_inventory.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_inventory.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_inventory.Location = New System.Drawing.Point(39, 21)
        Me.lbl_inventory.Name = "lbl_inventory"
        Me.lbl_inventory.Size = New System.Drawing.Size(131, 16)
        Me.lbl_inventory.TabIndex = 69
        Me.lbl_inventory.Text = "Network Inventory"
        '
        'grpbox_inventory_options
        '
        Me.grpbox_inventory_options.Controls.Add(Me.btn_inventory_save)
        Me.grpbox_inventory_options.Controls.Add(Me.txtbox_inventory_searchSN)
        Me.grpbox_inventory_options.Controls.Add(Me.lbl_inventory_searchSN)
        Me.grpbox_inventory_options.Controls.Add(Me.btn_inventory_clear)
        Me.grpbox_inventory_options.Controls.Add(Me.cmbbox_inventory_notes)
        Me.grpbox_inventory_options.Controls.Add(Me.lbl_inventory_notes)
        Me.grpbox_inventory_options.Controls.Add(Me.cmbbox_inventory_location)
        Me.grpbox_inventory_options.Controls.Add(Me.lbl_inventory_location)
        Me.grpbox_inventory_options.Controls.Add(Me.cmbbox_inventory_status)
        Me.grpbox_inventory_options.Controls.Add(Me.lbl_inventory_stauts)
        Me.grpbox_inventory_options.Controls.Add(Me.lbl_inventory_os)
        Me.grpbox_inventory_options.Controls.Add(Me.cmbbox_inventory_os)
        Me.grpbox_inventory_options.Controls.Add(Me.btn_inventory_refresh)
        Me.grpbox_inventory_options.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_inventory_options.Location = New System.Drawing.Point(507, 50)
        Me.grpbox_inventory_options.Name = "grpbox_inventory_options"
        Me.grpbox_inventory_options.Size = New System.Drawing.Size(254, 234)
        Me.grpbox_inventory_options.TabIndex = 124
        Me.grpbox_inventory_options.TabStop = False
        Me.grpbox_inventory_options.Text = "Search Options"
        '
        'btn_inventory_save
        '
        Me.btn_inventory_save.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_inventory_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_inventory_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventory_save.Location = New System.Drawing.Point(93, 194)
        Me.btn_inventory_save.Name = "btn_inventory_save"
        Me.btn_inventory_save.Size = New System.Drawing.Size(72, 21)
        Me.btn_inventory_save.TabIndex = 129
        Me.btn_inventory_save.Text = "Save"
        Me.btn_inventory_save.UseVisualStyleBackColor = False
        '
        'txtbox_inventory_searchSN
        '
        Me.txtbox_inventory_searchSN.Location = New System.Drawing.Point(68, 35)
        Me.txtbox_inventory_searchSN.Name = "txtbox_inventory_searchSN"
        Me.txtbox_inventory_searchSN.Size = New System.Drawing.Size(166, 21)
        Me.txtbox_inventory_searchSN.TabIndex = 125
        '
        'lbl_inventory_searchSN
        '
        Me.lbl_inventory_searchSN.AutoSize = True
        Me.lbl_inventory_searchSN.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_inventory_searchSN.Location = New System.Drawing.Point(15, 40)
        Me.lbl_inventory_searchSN.Name = "lbl_inventory_searchSN"
        Me.lbl_inventory_searchSN.Size = New System.Drawing.Size(28, 13)
        Me.lbl_inventory_searchSN.TabIndex = 126
        Me.lbl_inventory_searchSN.Text = "SN:"
        '
        'btn_inventory_clear
        '
        Me.btn_inventory_clear.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_inventory_clear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_inventory_clear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventory_clear.Location = New System.Drawing.Point(14, 194)
        Me.btn_inventory_clear.Name = "btn_inventory_clear"
        Me.btn_inventory_clear.Size = New System.Drawing.Size(72, 21)
        Me.btn_inventory_clear.TabIndex = 117
        Me.btn_inventory_clear.Text = "Clear"
        Me.btn_inventory_clear.UseVisualStyleBackColor = False
        '
        'cmbbox_inventory_notes
        '
        Me.cmbbox_inventory_notes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbbox_inventory_notes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbbox_inventory_notes.FormattingEnabled = True
        Me.cmbbox_inventory_notes.Location = New System.Drawing.Point(68, 153)
        Me.cmbbox_inventory_notes.Name = "cmbbox_inventory_notes"
        Me.cmbbox_inventory_notes.Size = New System.Drawing.Size(102, 23)
        Me.cmbbox_inventory_notes.TabIndex = 116
        '
        'lbl_inventory_notes
        '
        Me.lbl_inventory_notes.AutoSize = True
        Me.lbl_inventory_notes.Location = New System.Drawing.Point(15, 156)
        Me.lbl_inventory_notes.Name = "lbl_inventory_notes"
        Me.lbl_inventory_notes.Size = New System.Drawing.Size(44, 15)
        Me.lbl_inventory_notes.TabIndex = 115
        Me.lbl_inventory_notes.Text = "Notes"
        '
        'cmbbox_inventory_location
        '
        Me.cmbbox_inventory_location.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbbox_inventory_location.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbbox_inventory_location.FormattingEnabled = True
        Me.cmbbox_inventory_location.Location = New System.Drawing.Point(68, 123)
        Me.cmbbox_inventory_location.Name = "cmbbox_inventory_location"
        Me.cmbbox_inventory_location.Size = New System.Drawing.Size(102, 23)
        Me.cmbbox_inventory_location.TabIndex = 114
        '
        'lbl_inventory_location
        '
        Me.lbl_inventory_location.AutoSize = True
        Me.lbl_inventory_location.Location = New System.Drawing.Point(15, 126)
        Me.lbl_inventory_location.Name = "lbl_inventory_location"
        Me.lbl_inventory_location.Size = New System.Drawing.Size(34, 15)
        Me.lbl_inventory_location.TabIndex = 113
        Me.lbl_inventory_location.Text = "Loc."
        '
        'cmbbox_inventory_status
        '
        Me.cmbbox_inventory_status.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbbox_inventory_status.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbbox_inventory_status.FormattingEnabled = True
        Me.cmbbox_inventory_status.Location = New System.Drawing.Point(68, 93)
        Me.cmbbox_inventory_status.Name = "cmbbox_inventory_status"
        Me.cmbbox_inventory_status.Size = New System.Drawing.Size(102, 23)
        Me.cmbbox_inventory_status.TabIndex = 112
        '
        'lbl_inventory_stauts
        '
        Me.lbl_inventory_stauts.AutoSize = True
        Me.lbl_inventory_stauts.Location = New System.Drawing.Point(15, 96)
        Me.lbl_inventory_stauts.Name = "lbl_inventory_stauts"
        Me.lbl_inventory_stauts.Size = New System.Drawing.Size(47, 15)
        Me.lbl_inventory_stauts.TabIndex = 111
        Me.lbl_inventory_stauts.Text = "Status"
        '
        'lbl_inventory_os
        '
        Me.lbl_inventory_os.AutoSize = True
        Me.lbl_inventory_os.Location = New System.Drawing.Point(15, 66)
        Me.lbl_inventory_os.Name = "lbl_inventory_os"
        Me.lbl_inventory_os.Size = New System.Drawing.Size(26, 15)
        Me.lbl_inventory_os.TabIndex = 110
        Me.lbl_inventory_os.Text = "OS"
        '
        'cmbbox_inventory_os
        '
        Me.cmbbox_inventory_os.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbbox_inventory_os.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbbox_inventory_os.FormattingEnabled = True
        Me.cmbbox_inventory_os.Location = New System.Drawing.Point(68, 63)
        Me.cmbbox_inventory_os.Name = "cmbbox_inventory_os"
        Me.cmbbox_inventory_os.Size = New System.Drawing.Size(102, 23)
        Me.cmbbox_inventory_os.TabIndex = 109
        '
        'btn_inventory_refresh
        '
        Me.btn_inventory_refresh.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_inventory_refresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_inventory_refresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventory_refresh.Location = New System.Drawing.Point(172, 194)
        Me.btn_inventory_refresh.Name = "btn_inventory_refresh"
        Me.btn_inventory_refresh.Size = New System.Drawing.Size(72, 21)
        Me.btn_inventory_refresh.TabIndex = 108
        Me.btn_inventory_refresh.Text = "Search"
        Me.btn_inventory_refresh.UseVisualStyleBackColor = False
        '
        'chkbox_inventory_adddev
        '
        Me.chkbox_inventory_adddev.AutoSize = True
        Me.chkbox_inventory_adddev.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkbox_inventory_adddev.Location = New System.Drawing.Point(588, 21)
        Me.chkbox_inventory_adddev.Name = "chkbox_inventory_adddev"
        Me.chkbox_inventory_adddev.Size = New System.Drawing.Size(95, 17)
        Me.chkbox_inventory_adddev.TabIndex = 128
        Me.chkbox_inventory_adddev.Text = "Enable ADD"
        Me.chkbox_inventory_adddev.UseVisualStyleBackColor = True
        '
        'btn_inventory_add
        '
        Me.btn_inventory_add.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_inventory_add.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_inventory_add.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventory_add.Location = New System.Drawing.Point(689, 18)
        Me.btn_inventory_add.Name = "btn_inventory_add"
        Me.btn_inventory_add.Size = New System.Drawing.Size(72, 21)
        Me.btn_inventory_add.TabIndex = 127
        Me.btn_inventory_add.Text = "Add"
        Me.btn_inventory_add.UseVisualStyleBackColor = False
        '
        'btn_inventory_close
        '
        Me.btn_inventory_close.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_inventory_close.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_inventory_close.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventory_close.Location = New System.Drawing.Point(669, 288)
        Me.btn_inventory_close.Name = "btn_inventory_close"
        Me.btn_inventory_close.Size = New System.Drawing.Size(72, 21)
        Me.btn_inventory_close.TabIndex = 109
        Me.btn_inventory_close.Text = "Close"
        Me.btn_inventory_close.UseVisualStyleBackColor = False
        '
        'lbl_inventory_total
        '
        Me.lbl_inventory_total.AutoSize = True
        Me.lbl_inventory_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_inventory_total.Location = New System.Drawing.Point(39, 293)
        Me.lbl_inventory_total.Name = "lbl_inventory_total"
        Me.lbl_inventory_total.Size = New System.Drawing.Size(72, 13)
        Me.lbl_inventory_total.TabIndex = 117
        Me.lbl_inventory_total.Text = "#ofDevices"
        '
        'btn_inventory_export
        '
        Me.btn_inventory_export.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_inventory_export.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_inventory_export.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventory_export.Location = New System.Drawing.Point(588, 288)
        Me.btn_inventory_export.Name = "btn_inventory_export"
        Me.btn_inventory_export.Size = New System.Drawing.Size(72, 21)
        Me.btn_inventory_export.TabIndex = 129
        Me.btn_inventory_export.Text = "Export"
        Me.btn_inventory_export.UseVisualStyleBackColor = False
        '
        'btn_inventory_updateOrion
        '
        Me.btn_inventory_updateOrion.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_inventory_updateOrion.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_inventory_updateOrion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventory_updateOrion.Location = New System.Drawing.Point(412, 34)
        Me.btn_inventory_updateOrion.Name = "btn_inventory_updateOrion"
        Me.btn_inventory_updateOrion.Size = New System.Drawing.Size(85, 21)
        Me.btn_inventory_updateOrion.TabIndex = 130
        Me.btn_inventory_updateOrion.Text = "Orion DB"
        Me.btn_inventory_updateOrion.UseVisualStyleBackColor = False
        '
        'Inventory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(775, 324)
        Me.Controls.Add(Me.btn_inventory_updateOrion)
        Me.Controls.Add(Me.btn_inventory_export)
        Me.Controls.Add(Me.chkbox_inventory_adddev)
        Me.Controls.Add(Me.lbl_inventory_total)
        Me.Controls.Add(Me.btn_inventory_add)
        Me.Controls.Add(Me.btn_inventory_close)
        Me.Controls.Add(Me.grpbox_inventory_options)
        Me.Controls.Add(Me.lbl_inventory)
        Me.Controls.Add(Me.tab_inventory)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Inventory"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Inventory"
        Me.contextmenu_inventory_list.ResumeLayout(False)
        Me.tab_inventory.ResumeLayout(False)
        Me.tab_inventory_routers.ResumeLayout(False)
        Me.tab_inventory_switches.ResumeLayout(False)
        Me.tab_inventory_aps.ResumeLayout(False)
        Me.grpbox_inventory_options.ResumeLayout(False)
        Me.grpbox_inventory_options.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents listview_inventory_routers As ListView
    Friend WithEvents col_rtr_sn As ColumnHeader
    Friend WithEvents col_rtr_os As ColumnHeader
    Friend WithEvents col_rtr_stauts As ColumnHeader
    Friend WithEvents tab_inventory As TabControl
    Friend WithEvents tab_inventory_routers As TabPage
    Friend WithEvents tab_inventory_switches As TabPage
    Friend WithEvents tab_inventory_aps As TabPage
    Friend WithEvents col_rtr_location As ColumnHeader
    Friend WithEvents col_rtr_notes As ColumnHeader
    Friend WithEvents col_rtr_device As ColumnHeader
    Friend WithEvents listview_inventory_switches As ListView
    Friend WithEvents col_sw_sn As ColumnHeader
    Friend WithEvents col_sw_device As ColumnHeader
    Friend WithEvents col_sw_os As ColumnHeader
    Friend WithEvents col_sw_stauts As ColumnHeader
    Friend WithEvents col_sw_location As ColumnHeader
    Friend WithEvents col_sw_notes As ColumnHeader
    Friend WithEvents listview_inventory_aps As ListView
    Friend WithEvents col_ap_sn As ColumnHeader
    Friend WithEvents col_ap_device As ColumnHeader
    Friend WithEvents col_ap_os As ColumnHeader
    Friend WithEvents col_ap_stauts As ColumnHeader
    Friend WithEvents col_ap_location As ColumnHeader
    Friend WithEvents col_ap_notes As ColumnHeader
    Friend WithEvents lbl_inventory As Label
    Friend WithEvents grpbox_inventory_options As GroupBox
    Friend WithEvents btn_inventory_refresh As Button
    Friend WithEvents btn_inventory_close As Button
    Friend WithEvents lbl_inventory_os As Label
    Friend WithEvents cmbbox_inventory_os As ComboBox
    Friend WithEvents lbl_inventory_stauts As Label
    Friend WithEvents lbl_inventory_location As Label
    Friend WithEvents cmbbox_inventory_status As ComboBox
    Friend WithEvents cmbbox_inventory_location As ComboBox
    Friend WithEvents cmbbox_inventory_notes As ComboBox
    Friend WithEvents lbl_inventory_notes As Label
    Friend WithEvents lbl_inventory_total As Label
    Friend WithEvents btn_inventory_clear As Button
    Friend WithEvents txtbox_inventory_searchSN As TextBox
    Friend WithEvents lbl_inventory_searchSN As Label
    Friend WithEvents contextmenu_inventory_list As ContextMenuStrip
    Friend WithEvents context_inventory_edit As ToolStripMenuItem
    Friend WithEvents context_inventory_delete As ToolStripMenuItem
    Friend WithEvents btn_inventory_add As Button
    Friend WithEvents chkbox_inventory_adddev As CheckBox
    Friend WithEvents context_inventory_copy As ToolStripMenuItem
    Friend WithEvents btn_inventory_save As Button
    Friend WithEvents btn_inventory_export As Button
    Friend WithEvents btn_inventory_updateOrion As Button
End Class
