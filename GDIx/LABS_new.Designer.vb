﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LABS_new
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LABS_new))
        Me.lbl_nl_txt = New System.Windows.Forms.Label()
        Me.lbl_nl_info = New System.Windows.Forms.Label()
        Me.nl_star0 = New System.Windows.Forms.Label()
        Me.grpbox_nl_store = New System.Windows.Forms.GroupBox()
        Me.btn_nl_clear = New System.Windows.Forms.Button()
        Me.btn_nl_getinfo = New System.Windows.Forms.Button()
        Me.nl_star2 = New System.Windows.Forms.Label()
        Me.cmbbox_nl_env_load = New System.Windows.Forms.ComboBox()
        Me.lbl_nl_store_system = New System.Windows.Forms.Label()
        Me.cmbbox_nl_store_system = New System.Windows.Forms.ComboBox()
        Me.lbl_nl_env = New System.Windows.Forms.Label()
        Me.nl_star4 = New System.Windows.Forms.Label()
        Me.cmbbox_nl_province_load = New System.Windows.Forms.ComboBox()
        Me.nl_star6 = New System.Windows.Forms.Label()
        Me.nl_star9 = New System.Windows.Forms.Label()
        Me.nl_star5 = New System.Windows.Forms.Label()
        Me.nl_star10 = New System.Windows.Forms.Label()
        Me.nl_star8 = New System.Windows.Forms.Label()
        Me.lbl_nl_str_cntry = New System.Windows.Forms.Label()
        Me.cmbbox_nl_country_load = New System.Windows.Forms.ComboBox()
        Me.nl_star7 = New System.Windows.Forms.Label()
        Me.cmbbox_nl_iscombo_load = New System.Windows.Forms.ComboBox()
        Me.lbl_nl_iscombo = New System.Windows.Forms.Label()
        Me.nl_star3 = New System.Windows.Forms.Label()
        Me.nl_star1 = New System.Windows.Forms.Label()
        Me.txtbox_nl_city_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_address_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_sisstore_load = New System.Windows.Forms.TextBox()
        Me.cmbbox_nl_banner_load = New System.Windows.Forms.ComboBox()
        Me.lbl_nl_str_prov = New System.Windows.Forms.Label()
        Me.lbl_nl_str_city = New System.Windows.Forms.Label()
        Me.lbl_nl_str_add = New System.Windows.Forms.Label()
        Me.lbll_nl_str_sisstr = New System.Windows.Forms.Label()
        Me.lbl_nl_str_banner = New System.Windows.Forms.Label()
        Me.txtbox_nl_storeno_load = New System.Windows.Forms.TextBox()
        Me.lbl_nl_storeno = New System.Windows.Forms.Label()
        Me.grpbox_nl_network = New System.Windows.Forms.GroupBox()
        Me.grpbox_nl_bo = New System.Windows.Forms.GroupBox()
        Me.datetimepick_nl_boinstalldate_load = New System.Windows.Forms.DateTimePicker()
        Me.nl_star23 = New System.Windows.Forms.Label()
        Me.nl_star22 = New System.Windows.Forms.Label()
        Me.lbl_nl_bohostname = New System.Windows.Forms.Label()
        Me.txtbox_nl_boipaddress_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_bolocation_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_bohostname_load = New System.Windows.Forms.TextBox()
        Me.nl_star21 = New System.Windows.Forms.Label()
        Me.lbl_nl_bolocation = New System.Windows.Forms.Label()
        Me.nl_star20 = New System.Windows.Forms.Label()
        Me.lbl_nl_boinstalldate = New System.Windows.Forms.Label()
        Me.lbl_nl_boipaddress = New System.Windows.Forms.Label()
        Me.grpbox_nl_regs = New System.Windows.Forms.GroupBox()
        Me.datetimepick_nl_reg2installdate_load = New System.Windows.Forms.DateTimePicker()
        Me.datetimepick_nl_reg1installdate_load = New System.Windows.Forms.DateTimePicker()
        Me.nl_star19 = New System.Windows.Forms.Label()
        Me.nl_star18 = New System.Windows.Forms.Label()
        Me.nl_star17 = New System.Windows.Forms.Label()
        Me.lbl_nl_reg1hostname = New System.Windows.Forms.Label()
        Me.nl_star15 = New System.Windows.Forms.Label()
        Me.lbl_nl_reg2ipaddress = New System.Windows.Forms.Label()
        Me.lbl_nl_reg2location = New System.Windows.Forms.Label()
        Me.nl_star14 = New System.Windows.Forms.Label()
        Me.lbl_nl_reg2hostname = New System.Windows.Forms.Label()
        Me.lbl_nl_reg2installdate = New System.Windows.Forms.Label()
        Me.nl_star13 = New System.Windows.Forms.Label()
        Me.lbl_nl_reg1installdate = New System.Windows.Forms.Label()
        Me.nl_star12 = New System.Windows.Forms.Label()
        Me.txtbox_nl_reg2hostname_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_reg2ipaddress_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_reg1location_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_reg2location_load = New System.Windows.Forms.TextBox()
        Me.lbl_nl_reg1location = New System.Windows.Forms.Label()
        Me.nl_star16 = New System.Windows.Forms.Label()
        Me.txtbox_nl_reg1hostname_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_reg1ipaddress_load = New System.Windows.Forms.TextBox()
        Me.lbl_nl_reg1ipaddress = New System.Windows.Forms.Label()
        Me.grpbox_nl_equip = New System.Windows.Forms.GroupBox()
        Me.lbl_nl_na_info2 = New System.Windows.Forms.Label()
        Me.lbl_nl_na_info1 = New System.Windows.Forms.Label()
        Me.grpbox_nl_reg2 = New System.Windows.Forms.GroupBox()
        Me.datetimepick_nl_print2_installdate_load = New System.Windows.Forms.DateTimePicker()
        Me.datetimepick_nl_pin2_installdate_load = New System.Windows.Forms.DateTimePicker()
        Me.lbl_nl_pin2_ip = New System.Windows.Forms.Label()
        Me.nl_star35 = New System.Windows.Forms.Label()
        Me.lbl_nl_pin2_location = New System.Windows.Forms.Label()
        Me.txtbox_nl_pin2_location_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_pin2_ip_load = New System.Windows.Forms.TextBox()
        Me.lbl_nl_print2_installdate = New System.Windows.Forms.Label()
        Me.nl_star34 = New System.Windows.Forms.Label()
        Me.nl_star32 = New System.Windows.Forms.Label()
        Me.lbl_nl_pin2_installdate = New System.Windows.Forms.Label()
        Me.lbl_nl_print2_location = New System.Windows.Forms.Label()
        Me.nl_star31 = New System.Windows.Forms.Label()
        Me.lbl_nl_print2_ip = New System.Windows.Forms.Label()
        Me.txtbox_nl_print2_location_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_print2_ip_load = New System.Windows.Forms.TextBox()
        Me.nl_star30 = New System.Windows.Forms.Label()
        Me.grpbox_nl_reg1 = New System.Windows.Forms.GroupBox()
        Me.datetimepick_nl_print1_installdate_load = New System.Windows.Forms.DateTimePicker()
        Me.datetimepick_nl_pin1_installdate_load = New System.Windows.Forms.DateTimePicker()
        Me.lbl_nl_pin1_ip = New System.Windows.Forms.Label()
        Me.nl_star29 = New System.Windows.Forms.Label()
        Me.lbl_nl_pin1_location = New System.Windows.Forms.Label()
        Me.txtbox_nl_pin1_location_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_pin1_ip_load = New System.Windows.Forms.TextBox()
        Me.lbl_nl_print1_installdate = New System.Windows.Forms.Label()
        Me.nl_star28 = New System.Windows.Forms.Label()
        Me.nl_star26 = New System.Windows.Forms.Label()
        Me.lbl_nl_pin1_installdate = New System.Windows.Forms.Label()
        Me.lbl_nl_print1_location = New System.Windows.Forms.Label()
        Me.nl_star25 = New System.Windows.Forms.Label()
        Me.lbl_nl_print1_ip = New System.Windows.Forms.Label()
        Me.txtbox_nl_print1_location_load = New System.Windows.Forms.TextBox()
        Me.txtbox_nl_print1_ip_load = New System.Windows.Forms.TextBox()
        Me.grpbox_nl_lab_details = New System.Windows.Forms.GroupBox()
        Me.txtbox_labs_comments_load = New System.Windows.Forms.RichTextBox()
        Me.lbl_nl_comments = New System.Windows.Forms.Label()
        Me.txtbox_nl_epatch_load = New System.Windows.Forms.TextBox()
        Me.lbl_nl_epatch = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtbox_nl_pm_load = New System.Windows.Forms.TextBox()
        Me.lbl_nl_pm = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtbox_nl_project_load = New System.Windows.Forms.TextBox()
        Me.lbl_nl_project = New System.Windows.Forms.Label()
        Me.lbl_nl_status = New System.Windows.Forms.Label()
        Me.cmbbox_nl_status_load = New System.Windows.Forms.ComboBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.grpbox_nl_nbdev = New System.Windows.Forms.GroupBox()
        Me.nl_star11 = New System.Windows.Forms.Label()
        Me.cmbbox_nl_nbofreg_load = New System.Windows.Forms.ComboBox()
        Me.lbl_nl_nbofreg = New System.Windows.Forms.Label()
        Me.btn_nl_close = New System.Windows.Forms.Button()
        Me.btn_nl_clear_form = New System.Windows.Forms.Button()
        Me.btn_nl_add = New System.Windows.Forms.Button()
        Me.grpbox_nl_store.SuspendLayout()
        Me.grpbox_nl_network.SuspendLayout()
        Me.grpbox_nl_bo.SuspendLayout()
        Me.grpbox_nl_regs.SuspendLayout()
        Me.grpbox_nl_equip.SuspendLayout()
        Me.grpbox_nl_reg2.SuspendLayout()
        Me.grpbox_nl_reg1.SuspendLayout()
        Me.grpbox_nl_lab_details.SuspendLayout()
        Me.grpbox_nl_nbdev.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_nl_txt
        '
        Me.lbl_nl_txt.AutoSize = True
        Me.lbl_nl_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_txt.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_nl_txt.Location = New System.Drawing.Point(65, 24)
        Me.lbl_nl_txt.Name = "lbl_nl_txt"
        Me.lbl_nl_txt.Size = New System.Drawing.Size(111, 16)
        Me.lbl_nl_txt.TabIndex = 47
        Me.lbl_nl_txt.Text = "ADD NEW LAB"
        '
        'lbl_nl_info
        '
        Me.lbl_nl_info.AutoSize = True
        Me.lbl_nl_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_info.Location = New System.Drawing.Point(986, 9)
        Me.lbl_nl_info.Name = "lbl_nl_info"
        Me.lbl_nl_info.Size = New System.Drawing.Size(364, 45)
        Me.lbl_nl_info.TabIndex = 48
        Me.lbl_nl_info.Text = "Please enter the new store's information." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Mandatory fields marked with a star" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "I" &
    "f value is N/A leave field blank except device's location."
        '
        'nl_star0
        '
        Me.nl_star0.AutoSize = True
        Me.nl_star0.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star0.ForeColor = System.Drawing.Color.Red
        Me.nl_star0.Location = New System.Drawing.Point(1223, 24)
        Me.nl_star0.Name = "nl_star0"
        Me.nl_star0.Size = New System.Drawing.Size(13, 15)
        Me.nl_star0.TabIndex = 49
        Me.nl_star0.Text = "*"
        '
        'grpbox_nl_store
        '
        Me.grpbox_nl_store.Controls.Add(Me.btn_nl_clear)
        Me.grpbox_nl_store.Controls.Add(Me.btn_nl_getinfo)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star2)
        Me.grpbox_nl_store.Controls.Add(Me.cmbbox_nl_env_load)
        Me.grpbox_nl_store.Controls.Add(Me.lbl_nl_store_system)
        Me.grpbox_nl_store.Controls.Add(Me.cmbbox_nl_store_system)
        Me.grpbox_nl_store.Controls.Add(Me.lbl_nl_env)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star4)
        Me.grpbox_nl_store.Controls.Add(Me.cmbbox_nl_province_load)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star6)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star9)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star5)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star10)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star8)
        Me.grpbox_nl_store.Controls.Add(Me.lbl_nl_str_cntry)
        Me.grpbox_nl_store.Controls.Add(Me.cmbbox_nl_country_load)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star7)
        Me.grpbox_nl_store.Controls.Add(Me.cmbbox_nl_iscombo_load)
        Me.grpbox_nl_store.Controls.Add(Me.lbl_nl_iscombo)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star3)
        Me.grpbox_nl_store.Controls.Add(Me.nl_star1)
        Me.grpbox_nl_store.Controls.Add(Me.txtbox_nl_city_load)
        Me.grpbox_nl_store.Controls.Add(Me.txtbox_nl_address_load)
        Me.grpbox_nl_store.Controls.Add(Me.txtbox_nl_sisstore_load)
        Me.grpbox_nl_store.Controls.Add(Me.cmbbox_nl_banner_load)
        Me.grpbox_nl_store.Controls.Add(Me.lbl_nl_str_prov)
        Me.grpbox_nl_store.Controls.Add(Me.lbl_nl_str_city)
        Me.grpbox_nl_store.Controls.Add(Me.lbl_nl_str_add)
        Me.grpbox_nl_store.Controls.Add(Me.lbll_nl_str_sisstr)
        Me.grpbox_nl_store.Controls.Add(Me.lbl_nl_str_banner)
        Me.grpbox_nl_store.Controls.Add(Me.txtbox_nl_storeno_load)
        Me.grpbox_nl_store.Controls.Add(Me.lbl_nl_storeno)
        Me.grpbox_nl_store.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_nl_store.Location = New System.Drawing.Point(12, 62)
        Me.grpbox_nl_store.Name = "grpbox_nl_store"
        Me.grpbox_nl_store.Size = New System.Drawing.Size(271, 313)
        Me.grpbox_nl_store.TabIndex = 1
        Me.grpbox_nl_store.TabStop = False
        Me.grpbox_nl_store.Text = "Store Details"
        '
        'btn_nl_clear
        '
        Me.btn_nl_clear.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_nl_clear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_nl_clear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_nl_clear.Location = New System.Drawing.Point(93, 98)
        Me.btn_nl_clear.Name = "btn_nl_clear"
        Me.btn_nl_clear.Size = New System.Drawing.Size(81, 21)
        Me.btn_nl_clear.TabIndex = 107
        Me.btn_nl_clear.Text = "Clear"
        Me.btn_nl_clear.UseVisualStyleBackColor = False
        '
        'btn_nl_getinfo
        '
        Me.btn_nl_getinfo.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_nl_getinfo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_nl_getinfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_nl_getinfo.Location = New System.Drawing.Point(180, 98)
        Me.btn_nl_getinfo.Name = "btn_nl_getinfo"
        Me.btn_nl_getinfo.Size = New System.Drawing.Size(81, 21)
        Me.btn_nl_getinfo.TabIndex = 4
        Me.btn_nl_getinfo.Text = "Get info"
        Me.btn_nl_getinfo.UseVisualStyleBackColor = False
        '
        'nl_star2
        '
        Me.nl_star2.AutoSize = True
        Me.nl_star2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star2.ForeColor = System.Drawing.Color.Red
        Me.nl_star2.Location = New System.Drawing.Point(248, 48)
        Me.nl_star2.Name = "nl_star2"
        Me.nl_star2.Size = New System.Drawing.Size(13, 15)
        Me.nl_star2.TabIndex = 51
        Me.nl_star2.Text = "*"
        '
        'cmbbox_nl_env_load
        '
        Me.cmbbox_nl_env_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_nl_env_load.FormattingEnabled = True
        Me.cmbbox_nl_env_load.Items.AddRange(New Object() {"", "DEV", "Other", "TST", "UAT"})
        Me.cmbbox_nl_env_load.Location = New System.Drawing.Point(125, 45)
        Me.cmbbox_nl_env_load.Name = "cmbbox_nl_env_load"
        Me.cmbbox_nl_env_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_nl_env_load.TabIndex = 2
        '
        'lbl_nl_store_system
        '
        Me.lbl_nl_store_system.AutoSize = True
        Me.lbl_nl_store_system.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_store_system.Location = New System.Drawing.Point(14, 76)
        Me.lbl_nl_store_system.Name = "lbl_nl_store_system"
        Me.lbl_nl_store_system.Size = New System.Drawing.Size(81, 13)
        Me.lbl_nl_store_system.TabIndex = 46
        Me.lbl_nl_store_system.Text = "StoreSystem:"
        '
        'cmbbox_nl_store_system
        '
        Me.cmbbox_nl_store_system.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_nl_store_system.FormattingEnabled = True
        Me.cmbbox_nl_store_system.Items.AddRange(New Object() {"", "ORPOS", "XSTORE"})
        Me.cmbbox_nl_store_system.Location = New System.Drawing.Point(125, 70)
        Me.cmbbox_nl_store_system.Name = "cmbbox_nl_store_system"
        Me.cmbbox_nl_store_system.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_nl_store_system.TabIndex = 3
        '
        'lbl_nl_env
        '
        Me.lbl_nl_env.AutoSize = True
        Me.lbl_nl_env.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_env.Location = New System.Drawing.Point(14, 48)
        Me.lbl_nl_env.Name = "lbl_nl_env"
        Me.lbl_nl_env.Size = New System.Drawing.Size(91, 15)
        Me.lbl_nl_env.TabIndex = 2
        Me.lbl_nl_env.Text = "Environment:"
        '
        'nl_star4
        '
        Me.nl_star4.AutoSize = True
        Me.nl_star4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star4.ForeColor = System.Drawing.Color.Red
        Me.nl_star4.Location = New System.Drawing.Point(248, 73)
        Me.nl_star4.Name = "nl_star4"
        Me.nl_star4.Size = New System.Drawing.Size(13, 15)
        Me.nl_star4.TabIndex = 48
        Me.nl_star4.Text = "*"
        '
        'cmbbox_nl_province_load
        '
        Me.cmbbox_nl_province_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_nl_province_load.FormattingEnabled = True
        Me.cmbbox_nl_province_load.Location = New System.Drawing.Point(125, 275)
        Me.cmbbox_nl_province_load.Name = "cmbbox_nl_province_load"
        Me.cmbbox_nl_province_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_nl_province_load.TabIndex = 11
        '
        'nl_star6
        '
        Me.nl_star6.AutoSize = True
        Me.nl_star6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star6.ForeColor = System.Drawing.Color.Red
        Me.nl_star6.Location = New System.Drawing.Point(248, 180)
        Me.nl_star6.Name = "nl_star6"
        Me.nl_star6.Size = New System.Drawing.Size(13, 15)
        Me.nl_star6.TabIndex = 45
        Me.nl_star6.Text = "*"
        '
        'nl_star9
        '
        Me.nl_star9.AutoSize = True
        Me.nl_star9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star9.ForeColor = System.Drawing.Color.Red
        Me.nl_star9.Location = New System.Drawing.Point(248, 252)
        Me.nl_star9.Name = "nl_star9"
        Me.nl_star9.Size = New System.Drawing.Size(13, 15)
        Me.nl_star9.TabIndex = 37
        Me.nl_star9.Text = "*"
        '
        'nl_star5
        '
        Me.nl_star5.AutoSize = True
        Me.nl_star5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star5.ForeColor = System.Drawing.Color.Red
        Me.nl_star5.Location = New System.Drawing.Point(248, 154)
        Me.nl_star5.Name = "nl_star5"
        Me.nl_star5.Size = New System.Drawing.Size(13, 15)
        Me.nl_star5.TabIndex = 38
        Me.nl_star5.Text = "*"
        '
        'nl_star10
        '
        Me.nl_star10.AutoSize = True
        Me.nl_star10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star10.ForeColor = System.Drawing.Color.Red
        Me.nl_star10.Location = New System.Drawing.Point(248, 278)
        Me.nl_star10.Name = "nl_star10"
        Me.nl_star10.Size = New System.Drawing.Size(13, 15)
        Me.nl_star10.TabIndex = 35
        Me.nl_star10.Text = "*"
        '
        'nl_star8
        '
        Me.nl_star8.AutoSize = True
        Me.nl_star8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star8.ForeColor = System.Drawing.Color.Red
        Me.nl_star8.Location = New System.Drawing.Point(248, 228)
        Me.nl_star8.Name = "nl_star8"
        Me.nl_star8.Size = New System.Drawing.Size(13, 15)
        Me.nl_star8.TabIndex = 34
        Me.nl_star8.Text = "*"
        '
        'lbl_nl_str_cntry
        '
        Me.lbl_nl_str_cntry.AutoSize = True
        Me.lbl_nl_str_cntry.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_str_cntry.Location = New System.Drawing.Point(14, 255)
        Me.lbl_nl_str_cntry.Name = "lbl_nl_str_cntry"
        Me.lbl_nl_str_cntry.Size = New System.Drawing.Size(54, 13)
        Me.lbl_nl_str_cntry.TabIndex = 18
        Me.lbl_nl_str_cntry.Text = "Country:"
        '
        'cmbbox_nl_country_load
        '
        Me.cmbbox_nl_country_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_nl_country_load.FormattingEnabled = True
        Me.cmbbox_nl_country_load.Items.AddRange(New Object() {"", "Canada", "US"})
        Me.cmbbox_nl_country_load.Location = New System.Drawing.Point(125, 249)
        Me.cmbbox_nl_country_load.Name = "cmbbox_nl_country_load"
        Me.cmbbox_nl_country_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_nl_country_load.TabIndex = 10
        '
        'nl_star7
        '
        Me.nl_star7.AutoSize = True
        Me.nl_star7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star7.ForeColor = System.Drawing.Color.Red
        Me.nl_star7.Location = New System.Drawing.Point(248, 204)
        Me.nl_star7.Name = "nl_star7"
        Me.nl_star7.Size = New System.Drawing.Size(13, 15)
        Me.nl_star7.TabIndex = 33
        Me.nl_star7.Text = "*"
        '
        'cmbbox_nl_iscombo_load
        '
        Me.cmbbox_nl_iscombo_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_nl_iscombo_load.FormattingEnabled = True
        Me.cmbbox_nl_iscombo_load.Items.AddRange(New Object() {"", "NO", "YES"})
        Me.cmbbox_nl_iscombo_load.Location = New System.Drawing.Point(125, 151)
        Me.cmbbox_nl_iscombo_load.Name = "cmbbox_nl_iscombo_load"
        Me.cmbbox_nl_iscombo_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_nl_iscombo_load.TabIndex = 6
        '
        'lbl_nl_iscombo
        '
        Me.lbl_nl_iscombo.AutoSize = True
        Me.lbl_nl_iscombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_iscombo.Location = New System.Drawing.Point(14, 154)
        Me.lbl_nl_iscombo.Name = "lbl_nl_iscombo"
        Me.lbl_nl_iscombo.Size = New System.Drawing.Size(61, 15)
        Me.lbl_nl_iscombo.TabIndex = 38
        Me.lbl_nl_iscombo.Text = "COMBO:"
        '
        'nl_star3
        '
        Me.nl_star3.AutoSize = True
        Me.nl_star3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star3.ForeColor = System.Drawing.Color.Red
        Me.nl_star3.Location = New System.Drawing.Point(248, 128)
        Me.nl_star3.Name = "nl_star3"
        Me.nl_star3.Size = New System.Drawing.Size(13, 15)
        Me.nl_star3.TabIndex = 31
        Me.nl_star3.Text = "*"
        '
        'nl_star1
        '
        Me.nl_star1.AutoSize = True
        Me.nl_star1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star1.ForeColor = System.Drawing.Color.Red
        Me.nl_star1.Location = New System.Drawing.Point(248, 25)
        Me.nl_star1.Name = "nl_star1"
        Me.nl_star1.Size = New System.Drawing.Size(13, 15)
        Me.nl_star1.TabIndex = 30
        Me.nl_star1.Text = "*"
        '
        'txtbox_nl_city_load
        '
        Me.txtbox_nl_city_load.Location = New System.Drawing.Point(125, 225)
        Me.txtbox_nl_city_load.Name = "txtbox_nl_city_load"
        Me.txtbox_nl_city_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_nl_city_load.TabIndex = 9
        '
        'txtbox_nl_address_load
        '
        Me.txtbox_nl_address_load.Location = New System.Drawing.Point(125, 201)
        Me.txtbox_nl_address_load.Name = "txtbox_nl_address_load"
        Me.txtbox_nl_address_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_nl_address_load.TabIndex = 8
        '
        'txtbox_nl_sisstore_load
        '
        Me.txtbox_nl_sisstore_load.Location = New System.Drawing.Point(125, 177)
        Me.txtbox_nl_sisstore_load.Name = "txtbox_nl_sisstore_load"
        Me.txtbox_nl_sisstore_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_nl_sisstore_load.TabIndex = 7
        '
        'cmbbox_nl_banner_load
        '
        Me.cmbbox_nl_banner_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_nl_banner_load.FormattingEnabled = True
        Me.cmbbox_nl_banner_load.Items.AddRange(New Object() {"", "Dynamite", "Dynamite HO", "Dynamite Combo", "Dynamite USA", "Garage", "Garage HO", "Garage Combo", "Garage USA"})
        Me.cmbbox_nl_banner_load.Location = New System.Drawing.Point(125, 125)
        Me.cmbbox_nl_banner_load.Name = "cmbbox_nl_banner_load"
        Me.cmbbox_nl_banner_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_nl_banner_load.TabIndex = 5
        '
        'lbl_nl_str_prov
        '
        Me.lbl_nl_str_prov.AutoSize = True
        Me.lbl_nl_str_prov.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_str_prov.Location = New System.Drawing.Point(14, 279)
        Me.lbl_nl_str_prov.Name = "lbl_nl_str_prov"
        Me.lbl_nl_str_prov.Size = New System.Drawing.Size(105, 13)
        Me.lbl_nl_str_prov.TabIndex = 16
        Me.lbl_nl_str_prov.Text = "Province / State:"
        '
        'lbl_nl_str_city
        '
        Me.lbl_nl_str_city.AutoSize = True
        Me.lbl_nl_str_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_str_city.Location = New System.Drawing.Point(14, 230)
        Me.lbl_nl_str_city.Name = "lbl_nl_str_city"
        Me.lbl_nl_str_city.Size = New System.Drawing.Size(32, 13)
        Me.lbl_nl_str_city.TabIndex = 15
        Me.lbl_nl_str_city.Text = "City:"
        '
        'lbl_nl_str_add
        '
        Me.lbl_nl_str_add.AutoSize = True
        Me.lbl_nl_str_add.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_str_add.Location = New System.Drawing.Point(14, 206)
        Me.lbl_nl_str_add.Name = "lbl_nl_str_add"
        Me.lbl_nl_str_add.Size = New System.Drawing.Size(56, 13)
        Me.lbl_nl_str_add.TabIndex = 14
        Me.lbl_nl_str_add.Text = "Address:"
        '
        'lbll_nl_str_sisstr
        '
        Me.lbll_nl_str_sisstr.AutoSize = True
        Me.lbll_nl_str_sisstr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbll_nl_str_sisstr.Location = New System.Drawing.Point(14, 182)
        Me.lbll_nl_str_sisstr.Name = "lbll_nl_str_sisstr"
        Me.lbll_nl_str_sisstr.Size = New System.Drawing.Size(77, 13)
        Me.lbll_nl_str_sisstr.TabIndex = 12
        Me.lbll_nl_str_sisstr.Text = "Sister Store:"
        '
        'lbl_nl_str_banner
        '
        Me.lbl_nl_str_banner.AutoSize = True
        Me.lbl_nl_str_banner.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_str_banner.Location = New System.Drawing.Point(14, 131)
        Me.lbl_nl_str_banner.Name = "lbl_nl_str_banner"
        Me.lbl_nl_str_banner.Size = New System.Drawing.Size(51, 13)
        Me.lbl_nl_str_banner.TabIndex = 11
        Me.lbl_nl_str_banner.Text = "Banner:"
        '
        'txtbox_nl_storeno_load
        '
        Me.txtbox_nl_storeno_load.Location = New System.Drawing.Point(125, 22)
        Me.txtbox_nl_storeno_load.Name = "txtbox_nl_storeno_load"
        Me.txtbox_nl_storeno_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_nl_storeno_load.TabIndex = 1
        '
        'lbl_nl_storeno
        '
        Me.lbl_nl_storeno.AutoSize = True
        Me.lbl_nl_storeno.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_storeno.Location = New System.Drawing.Point(14, 25)
        Me.lbl_nl_storeno.Name = "lbl_nl_storeno"
        Me.lbl_nl_storeno.Size = New System.Drawing.Size(67, 15)
        Me.lbl_nl_storeno.TabIndex = 0
        Me.lbl_nl_storeno.Text = "Store No:"
        '
        'grpbox_nl_network
        '
        Me.grpbox_nl_network.Controls.Add(Me.grpbox_nl_bo)
        Me.grpbox_nl_network.Controls.Add(Me.grpbox_nl_regs)
        Me.grpbox_nl_network.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_nl_network.Location = New System.Drawing.Point(289, 62)
        Me.grpbox_nl_network.Name = "grpbox_nl_network"
        Me.grpbox_nl_network.Size = New System.Drawing.Size(360, 393)
        Me.grpbox_nl_network.TabIndex = 51
        Me.grpbox_nl_network.TabStop = False
        Me.grpbox_nl_network.Text = "Network Details"
        '
        'grpbox_nl_bo
        '
        Me.grpbox_nl_bo.Controls.Add(Me.datetimepick_nl_boinstalldate_load)
        Me.grpbox_nl_bo.Controls.Add(Me.nl_star23)
        Me.grpbox_nl_bo.Controls.Add(Me.nl_star22)
        Me.grpbox_nl_bo.Controls.Add(Me.lbl_nl_bohostname)
        Me.grpbox_nl_bo.Controls.Add(Me.txtbox_nl_boipaddress_load)
        Me.grpbox_nl_bo.Controls.Add(Me.txtbox_nl_bolocation_load)
        Me.grpbox_nl_bo.Controls.Add(Me.txtbox_nl_bohostname_load)
        Me.grpbox_nl_bo.Controls.Add(Me.nl_star21)
        Me.grpbox_nl_bo.Controls.Add(Me.lbl_nl_bolocation)
        Me.grpbox_nl_bo.Controls.Add(Me.nl_star20)
        Me.grpbox_nl_bo.Controls.Add(Me.lbl_nl_boinstalldate)
        Me.grpbox_nl_bo.Controls.Add(Me.lbl_nl_boipaddress)
        Me.grpbox_nl_bo.Location = New System.Drawing.Point(16, 251)
        Me.grpbox_nl_bo.Name = "grpbox_nl_bo"
        Me.grpbox_nl_bo.Size = New System.Drawing.Size(328, 132)
        Me.grpbox_nl_bo.TabIndex = 72
        Me.grpbox_nl_bo.TabStop = False
        Me.grpbox_nl_bo.Text = "BO info"
        '
        'datetimepick_nl_boinstalldate_load
        '
        Me.datetimepick_nl_boinstalldate_load.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datetimepick_nl_boinstalldate_load.Location = New System.Drawing.Point(151, 95)
        Me.datetimepick_nl_boinstalldate_load.Name = "datetimepick_nl_boinstalldate_load"
        Me.datetimepick_nl_boinstalldate_load.Size = New System.Drawing.Size(151, 21)
        Me.datetimepick_nl_boinstalldate_load.TabIndex = 76
        Me.datetimepick_nl_boinstalldate_load.Value = New Date(2018, 5, 3, 0, 0, 0, 0)
        '
        'nl_star23
        '
        Me.nl_star23.AutoSize = True
        Me.nl_star23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star23.ForeColor = System.Drawing.Color.Red
        Me.nl_star23.Location = New System.Drawing.Point(304, 98)
        Me.nl_star23.Name = "nl_star23"
        Me.nl_star23.Size = New System.Drawing.Size(13, 15)
        Me.nl_star23.TabIndex = 73
        Me.nl_star23.Text = "*"
        '
        'nl_star22
        '
        Me.nl_star22.AutoSize = True
        Me.nl_star22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star22.ForeColor = System.Drawing.Color.Red
        Me.nl_star22.Location = New System.Drawing.Point(304, 74)
        Me.nl_star22.Name = "nl_star22"
        Me.nl_star22.Size = New System.Drawing.Size(13, 15)
        Me.nl_star22.TabIndex = 72
        Me.nl_star22.Text = "*"
        '
        'lbl_nl_bohostname
        '
        Me.lbl_nl_bohostname.AutoSize = True
        Me.lbl_nl_bohostname.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_bohostname.Location = New System.Drawing.Point(21, 26)
        Me.lbl_nl_bohostname.Name = "lbl_nl_bohostname"
        Me.lbl_nl_bohostname.Size = New System.Drawing.Size(99, 15)
        Me.lbl_nl_bohostname.TabIndex = 38
        Me.lbl_nl_bohostname.Text = "BO Hostname:"
        '
        'txtbox_nl_boipaddress_load
        '
        Me.txtbox_nl_boipaddress_load.Location = New System.Drawing.Point(151, 47)
        Me.txtbox_nl_boipaddress_load.Name = "txtbox_nl_boipaddress_load"
        Me.txtbox_nl_boipaddress_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_boipaddress_load.TabIndex = 15
        '
        'txtbox_nl_bolocation_load
        '
        Me.txtbox_nl_bolocation_load.Location = New System.Drawing.Point(151, 71)
        Me.txtbox_nl_bolocation_load.Name = "txtbox_nl_bolocation_load"
        Me.txtbox_nl_bolocation_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_bolocation_load.TabIndex = 16
        '
        'txtbox_nl_bohostname_load
        '
        Me.txtbox_nl_bohostname_load.Location = New System.Drawing.Point(151, 23)
        Me.txtbox_nl_bohostname_load.Name = "txtbox_nl_bohostname_load"
        Me.txtbox_nl_bohostname_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_bohostname_load.TabIndex = 14
        '
        'nl_star21
        '
        Me.nl_star21.AutoSize = True
        Me.nl_star21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star21.ForeColor = System.Drawing.Color.Red
        Me.nl_star21.Location = New System.Drawing.Point(304, 50)
        Me.nl_star21.Name = "nl_star21"
        Me.nl_star21.Size = New System.Drawing.Size(13, 15)
        Me.nl_star21.TabIndex = 65
        Me.nl_star21.Text = "*"
        '
        'lbl_nl_bolocation
        '
        Me.lbl_nl_bolocation.AutoSize = True
        Me.lbl_nl_bolocation.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_bolocation.Location = New System.Drawing.Point(21, 74)
        Me.lbl_nl_bolocation.Name = "lbl_nl_bolocation"
        Me.lbl_nl_bolocation.Size = New System.Drawing.Size(89, 15)
        Me.lbl_nl_bolocation.TabIndex = 71
        Me.lbl_nl_bolocation.Text = "BO Location:"
        '
        'nl_star20
        '
        Me.nl_star20.AutoSize = True
        Me.nl_star20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star20.ForeColor = System.Drawing.Color.Red
        Me.nl_star20.Location = New System.Drawing.Point(304, 26)
        Me.nl_star20.Name = "nl_star20"
        Me.nl_star20.Size = New System.Drawing.Size(13, 15)
        Me.nl_star20.TabIndex = 38
        Me.nl_star20.Text = "*"
        '
        'lbl_nl_boinstalldate
        '
        Me.lbl_nl_boinstalldate.AutoSize = True
        Me.lbl_nl_boinstalldate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_boinstalldate.Location = New System.Drawing.Point(21, 98)
        Me.lbl_nl_boinstalldate.Name = "lbl_nl_boinstalldate"
        Me.lbl_nl_boinstalldate.Size = New System.Drawing.Size(107, 15)
        Me.lbl_nl_boinstalldate.TabIndex = 41
        Me.lbl_nl_boinstalldate.Text = "BO Install Date:"
        '
        'lbl_nl_boipaddress
        '
        Me.lbl_nl_boipaddress.AutoSize = True
        Me.lbl_nl_boipaddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_boipaddress.Location = New System.Drawing.Point(21, 50)
        Me.lbl_nl_boipaddress.Name = "lbl_nl_boipaddress"
        Me.lbl_nl_boipaddress.Size = New System.Drawing.Size(98, 15)
        Me.lbl_nl_boipaddress.TabIndex = 39
        Me.lbl_nl_boipaddress.Text = "BO IPaddress:"
        '
        'grpbox_nl_regs
        '
        Me.grpbox_nl_regs.Controls.Add(Me.datetimepick_nl_reg2installdate_load)
        Me.grpbox_nl_regs.Controls.Add(Me.datetimepick_nl_reg1installdate_load)
        Me.grpbox_nl_regs.Controls.Add(Me.nl_star19)
        Me.grpbox_nl_regs.Controls.Add(Me.nl_star18)
        Me.grpbox_nl_regs.Controls.Add(Me.nl_star17)
        Me.grpbox_nl_regs.Controls.Add(Me.lbl_nl_reg1hostname)
        Me.grpbox_nl_regs.Controls.Add(Me.nl_star15)
        Me.grpbox_nl_regs.Controls.Add(Me.lbl_nl_reg2ipaddress)
        Me.grpbox_nl_regs.Controls.Add(Me.lbl_nl_reg2location)
        Me.grpbox_nl_regs.Controls.Add(Me.nl_star14)
        Me.grpbox_nl_regs.Controls.Add(Me.lbl_nl_reg2hostname)
        Me.grpbox_nl_regs.Controls.Add(Me.lbl_nl_reg2installdate)
        Me.grpbox_nl_regs.Controls.Add(Me.nl_star13)
        Me.grpbox_nl_regs.Controls.Add(Me.lbl_nl_reg1installdate)
        Me.grpbox_nl_regs.Controls.Add(Me.nl_star12)
        Me.grpbox_nl_regs.Controls.Add(Me.txtbox_nl_reg2hostname_load)
        Me.grpbox_nl_regs.Controls.Add(Me.txtbox_nl_reg2ipaddress_load)
        Me.grpbox_nl_regs.Controls.Add(Me.txtbox_nl_reg1location_load)
        Me.grpbox_nl_regs.Controls.Add(Me.txtbox_nl_reg2location_load)
        Me.grpbox_nl_regs.Controls.Add(Me.lbl_nl_reg1location)
        Me.grpbox_nl_regs.Controls.Add(Me.nl_star16)
        Me.grpbox_nl_regs.Controls.Add(Me.txtbox_nl_reg1hostname_load)
        Me.grpbox_nl_regs.Controls.Add(Me.txtbox_nl_reg1ipaddress_load)
        Me.grpbox_nl_regs.Controls.Add(Me.lbl_nl_reg1ipaddress)
        Me.grpbox_nl_regs.Location = New System.Drawing.Point(16, 26)
        Me.grpbox_nl_regs.Name = "grpbox_nl_regs"
        Me.grpbox_nl_regs.Size = New System.Drawing.Size(328, 219)
        Me.grpbox_nl_regs.TabIndex = 71
        Me.grpbox_nl_regs.TabStop = False
        Me.grpbox_nl_regs.Text = "Registers info"
        '
        'datetimepick_nl_reg2installdate_load
        '
        Me.datetimepick_nl_reg2installdate_load.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datetimepick_nl_reg2installdate_load.Location = New System.Drawing.Point(151, 183)
        Me.datetimepick_nl_reg2installdate_load.Name = "datetimepick_nl_reg2installdate_load"
        Me.datetimepick_nl_reg2installdate_load.Size = New System.Drawing.Size(151, 21)
        Me.datetimepick_nl_reg2installdate_load.TabIndex = 75
        Me.datetimepick_nl_reg2installdate_load.Value = New Date(2018, 5, 3, 0, 0, 0, 0)
        '
        'datetimepick_nl_reg1installdate_load
        '
        Me.datetimepick_nl_reg1installdate_load.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datetimepick_nl_reg1installdate_load.Location = New System.Drawing.Point(151, 91)
        Me.datetimepick_nl_reg1installdate_load.Name = "datetimepick_nl_reg1installdate_load"
        Me.datetimepick_nl_reg1installdate_load.Size = New System.Drawing.Size(151, 21)
        Me.datetimepick_nl_reg1installdate_load.TabIndex = 74
        Me.datetimepick_nl_reg1installdate_load.Value = New Date(2018, 5, 3, 0, 0, 0, 0)
        '
        'nl_star19
        '
        Me.nl_star19.AutoSize = True
        Me.nl_star19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star19.ForeColor = System.Drawing.Color.Red
        Me.nl_star19.Location = New System.Drawing.Point(304, 186)
        Me.nl_star19.Name = "nl_star19"
        Me.nl_star19.Size = New System.Drawing.Size(13, 15)
        Me.nl_star19.TabIndex = 73
        Me.nl_star19.Text = "*"
        '
        'nl_star18
        '
        Me.nl_star18.AutoSize = True
        Me.nl_star18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star18.ForeColor = System.Drawing.Color.Red
        Me.nl_star18.Location = New System.Drawing.Point(304, 163)
        Me.nl_star18.Name = "nl_star18"
        Me.nl_star18.Size = New System.Drawing.Size(13, 15)
        Me.nl_star18.TabIndex = 72
        Me.nl_star18.Text = "*"
        '
        'nl_star17
        '
        Me.nl_star17.AutoSize = True
        Me.nl_star17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star17.ForeColor = System.Drawing.Color.Red
        Me.nl_star17.Location = New System.Drawing.Point(304, 140)
        Me.nl_star17.Name = "nl_star17"
        Me.nl_star17.Size = New System.Drawing.Size(13, 15)
        Me.nl_star17.TabIndex = 71
        Me.nl_star17.Text = "*"
        '
        'lbl_nl_reg1hostname
        '
        Me.lbl_nl_reg1hostname.AutoSize = True
        Me.lbl_nl_reg1hostname.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_reg1hostname.Location = New System.Drawing.Point(21, 24)
        Me.lbl_nl_reg1hostname.Name = "lbl_nl_reg1hostname"
        Me.lbl_nl_reg1hostname.Size = New System.Drawing.Size(114, 15)
        Me.lbl_nl_reg1hostname.TabIndex = 59
        Me.lbl_nl_reg1hostname.Text = "Reg1 Hostname:"
        '
        'nl_star15
        '
        Me.nl_star15.AutoSize = True
        Me.nl_star15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star15.ForeColor = System.Drawing.Color.Red
        Me.nl_star15.Location = New System.Drawing.Point(304, 91)
        Me.nl_star15.Name = "nl_star15"
        Me.nl_star15.Size = New System.Drawing.Size(13, 15)
        Me.nl_star15.TabIndex = 70
        Me.nl_star15.Text = "*"
        '
        'lbl_nl_reg2ipaddress
        '
        Me.lbl_nl_reg2ipaddress.AutoSize = True
        Me.lbl_nl_reg2ipaddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_reg2ipaddress.Location = New System.Drawing.Point(21, 140)
        Me.lbl_nl_reg2ipaddress.Name = "lbl_nl_reg2ipaddress"
        Me.lbl_nl_reg2ipaddress.Size = New System.Drawing.Size(113, 15)
        Me.lbl_nl_reg2ipaddress.TabIndex = 46
        Me.lbl_nl_reg2ipaddress.Text = "Reg2 IPaddress:"
        '
        'lbl_nl_reg2location
        '
        Me.lbl_nl_reg2location.AutoSize = True
        Me.lbl_nl_reg2location.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_reg2location.Location = New System.Drawing.Point(21, 163)
        Me.lbl_nl_reg2location.Name = "lbl_nl_reg2location"
        Me.lbl_nl_reg2location.Size = New System.Drawing.Size(100, 15)
        Me.lbl_nl_reg2location.TabIndex = 47
        Me.lbl_nl_reg2location.Text = "Reg2 Location"
        '
        'nl_star14
        '
        Me.nl_star14.AutoSize = True
        Me.nl_star14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star14.ForeColor = System.Drawing.Color.Red
        Me.nl_star14.Location = New System.Drawing.Point(304, 71)
        Me.nl_star14.Name = "nl_star14"
        Me.nl_star14.Size = New System.Drawing.Size(13, 15)
        Me.nl_star14.TabIndex = 69
        Me.nl_star14.Text = "*"
        '
        'lbl_nl_reg2hostname
        '
        Me.lbl_nl_reg2hostname.AutoSize = True
        Me.lbl_nl_reg2hostname.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_reg2hostname.Location = New System.Drawing.Point(21, 117)
        Me.lbl_nl_reg2hostname.Name = "lbl_nl_reg2hostname"
        Me.lbl_nl_reg2hostname.Size = New System.Drawing.Size(114, 15)
        Me.lbl_nl_reg2hostname.TabIndex = 45
        Me.lbl_nl_reg2hostname.Text = "Reg2 Hostname:"
        '
        'lbl_nl_reg2installdate
        '
        Me.lbl_nl_reg2installdate.AutoSize = True
        Me.lbl_nl_reg2installdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_reg2installdate.Location = New System.Drawing.Point(21, 186)
        Me.lbl_nl_reg2installdate.Name = "lbl_nl_reg2installdate"
        Me.lbl_nl_reg2installdate.Size = New System.Drawing.Size(122, 15)
        Me.lbl_nl_reg2installdate.TabIndex = 49
        Me.lbl_nl_reg2installdate.Text = "Reg2 Install Date:"
        '
        'nl_star13
        '
        Me.nl_star13.AutoSize = True
        Me.nl_star13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star13.ForeColor = System.Drawing.Color.Red
        Me.nl_star13.Location = New System.Drawing.Point(304, 47)
        Me.nl_star13.Name = "nl_star13"
        Me.nl_star13.Size = New System.Drawing.Size(13, 15)
        Me.nl_star13.TabIndex = 68
        Me.nl_star13.Text = "*"
        '
        'lbl_nl_reg1installdate
        '
        Me.lbl_nl_reg1installdate.AutoSize = True
        Me.lbl_nl_reg1installdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_reg1installdate.Location = New System.Drawing.Point(21, 94)
        Me.lbl_nl_reg1installdate.Name = "lbl_nl_reg1installdate"
        Me.lbl_nl_reg1installdate.Size = New System.Drawing.Size(122, 15)
        Me.lbl_nl_reg1installdate.TabIndex = 41
        Me.lbl_nl_reg1installdate.Text = "Reg1 Install Date:"
        '
        'nl_star12
        '
        Me.nl_star12.AutoSize = True
        Me.nl_star12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star12.ForeColor = System.Drawing.Color.Red
        Me.nl_star12.Location = New System.Drawing.Point(304, 24)
        Me.nl_star12.Name = "nl_star12"
        Me.nl_star12.Size = New System.Drawing.Size(13, 15)
        Me.nl_star12.TabIndex = 67
        Me.nl_star12.Text = "*"
        '
        'txtbox_nl_reg2hostname_load
        '
        Me.txtbox_nl_reg2hostname_load.Location = New System.Drawing.Point(151, 114)
        Me.txtbox_nl_reg2hostname_load.Name = "txtbox_nl_reg2hostname_load"
        Me.txtbox_nl_reg2hostname_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_reg2hostname_load.TabIndex = 23
        '
        'txtbox_nl_reg2ipaddress_load
        '
        Me.txtbox_nl_reg2ipaddress_load.Location = New System.Drawing.Point(151, 137)
        Me.txtbox_nl_reg2ipaddress_load.Name = "txtbox_nl_reg2ipaddress_load"
        Me.txtbox_nl_reg2ipaddress_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_reg2ipaddress_load.TabIndex = 24
        '
        'txtbox_nl_reg1location_load
        '
        Me.txtbox_nl_reg1location_load.Location = New System.Drawing.Point(151, 68)
        Me.txtbox_nl_reg1location_load.Name = "txtbox_nl_reg1location_load"
        Me.txtbox_nl_reg1location_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_reg1location_load.TabIndex = 21
        '
        'txtbox_nl_reg2location_load
        '
        Me.txtbox_nl_reg2location_load.Location = New System.Drawing.Point(151, 160)
        Me.txtbox_nl_reg2location_load.Name = "txtbox_nl_reg2location_load"
        Me.txtbox_nl_reg2location_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_reg2location_load.TabIndex = 25
        '
        'lbl_nl_reg1location
        '
        Me.lbl_nl_reg1location.AutoSize = True
        Me.lbl_nl_reg1location.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_reg1location.Location = New System.Drawing.Point(21, 71)
        Me.lbl_nl_reg1location.Name = "lbl_nl_reg1location"
        Me.lbl_nl_reg1location.Size = New System.Drawing.Size(100, 15)
        Me.lbl_nl_reg1location.TabIndex = 63
        Me.lbl_nl_reg1location.Text = "Reg1 Location"
        '
        'nl_star16
        '
        Me.nl_star16.AutoSize = True
        Me.nl_star16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star16.ForeColor = System.Drawing.Color.Red
        Me.nl_star16.Location = New System.Drawing.Point(304, 117)
        Me.nl_star16.Name = "nl_star16"
        Me.nl_star16.Size = New System.Drawing.Size(13, 15)
        Me.nl_star16.TabIndex = 57
        Me.nl_star16.Text = "*"
        '
        'txtbox_nl_reg1hostname_load
        '
        Me.txtbox_nl_reg1hostname_load.Location = New System.Drawing.Point(151, 22)
        Me.txtbox_nl_reg1hostname_load.Name = "txtbox_nl_reg1hostname_load"
        Me.txtbox_nl_reg1hostname_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_reg1hostname_load.TabIndex = 19
        '
        'txtbox_nl_reg1ipaddress_load
        '
        Me.txtbox_nl_reg1ipaddress_load.Location = New System.Drawing.Point(151, 45)
        Me.txtbox_nl_reg1ipaddress_load.Name = "txtbox_nl_reg1ipaddress_load"
        Me.txtbox_nl_reg1ipaddress_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_reg1ipaddress_load.TabIndex = 20
        '
        'lbl_nl_reg1ipaddress
        '
        Me.lbl_nl_reg1ipaddress.AutoSize = True
        Me.lbl_nl_reg1ipaddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_reg1ipaddress.Location = New System.Drawing.Point(21, 47)
        Me.lbl_nl_reg1ipaddress.Name = "lbl_nl_reg1ipaddress"
        Me.lbl_nl_reg1ipaddress.Size = New System.Drawing.Size(113, 15)
        Me.lbl_nl_reg1ipaddress.TabIndex = 61
        Me.lbl_nl_reg1ipaddress.Text = "Reg1 IPaddress:"
        '
        'grpbox_nl_equip
        '
        Me.grpbox_nl_equip.Controls.Add(Me.lbl_nl_na_info2)
        Me.grpbox_nl_equip.Controls.Add(Me.lbl_nl_na_info1)
        Me.grpbox_nl_equip.Controls.Add(Me.grpbox_nl_reg2)
        Me.grpbox_nl_equip.Controls.Add(Me.grpbox_nl_reg1)
        Me.grpbox_nl_equip.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_nl_equip.Location = New System.Drawing.Point(655, 62)
        Me.grpbox_nl_equip.Name = "grpbox_nl_equip"
        Me.grpbox_nl_equip.Size = New System.Drawing.Size(386, 393)
        Me.grpbox_nl_equip.TabIndex = 52
        Me.grpbox_nl_equip.TabStop = False
        Me.grpbox_nl_equip.Text = "External Devices"
        '
        'lbl_nl_na_info2
        '
        Me.lbl_nl_na_info2.AutoSize = True
        Me.lbl_nl_na_info2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_na_info2.ForeColor = System.Drawing.Color.Red
        Me.lbl_nl_na_info2.Location = New System.Drawing.Point(141, 198)
        Me.lbl_nl_na_info2.Name = "lbl_nl_na_info2"
        Me.lbl_nl_na_info2.Size = New System.Drawing.Size(238, 13)
        Me.lbl_nl_na_info2.TabIndex = 137
        Me.lbl_nl_na_info2.Text = "If device is available set Location else leave N/A"
        '
        'lbl_nl_na_info1
        '
        Me.lbl_nl_na_info1.AutoSize = True
        Me.lbl_nl_na_info1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_na_info1.ForeColor = System.Drawing.Color.Red
        Me.lbl_nl_na_info1.Location = New System.Drawing.Point(141, 15)
        Me.lbl_nl_na_info1.Name = "lbl_nl_na_info1"
        Me.lbl_nl_na_info1.Size = New System.Drawing.Size(238, 13)
        Me.lbl_nl_na_info1.TabIndex = 136
        Me.lbl_nl_na_info1.Text = "If device is available set Location else leave N/A"
        '
        'grpbox_nl_reg2
        '
        Me.grpbox_nl_reg2.Controls.Add(Me.datetimepick_nl_print2_installdate_load)
        Me.grpbox_nl_reg2.Controls.Add(Me.datetimepick_nl_pin2_installdate_load)
        Me.grpbox_nl_reg2.Controls.Add(Me.lbl_nl_pin2_ip)
        Me.grpbox_nl_reg2.Controls.Add(Me.nl_star35)
        Me.grpbox_nl_reg2.Controls.Add(Me.lbl_nl_pin2_location)
        Me.grpbox_nl_reg2.Controls.Add(Me.txtbox_nl_pin2_location_load)
        Me.grpbox_nl_reg2.Controls.Add(Me.txtbox_nl_pin2_ip_load)
        Me.grpbox_nl_reg2.Controls.Add(Me.lbl_nl_print2_installdate)
        Me.grpbox_nl_reg2.Controls.Add(Me.nl_star34)
        Me.grpbox_nl_reg2.Controls.Add(Me.nl_star32)
        Me.grpbox_nl_reg2.Controls.Add(Me.lbl_nl_pin2_installdate)
        Me.grpbox_nl_reg2.Controls.Add(Me.lbl_nl_print2_location)
        Me.grpbox_nl_reg2.Controls.Add(Me.nl_star31)
        Me.grpbox_nl_reg2.Controls.Add(Me.lbl_nl_print2_ip)
        Me.grpbox_nl_reg2.Controls.Add(Me.txtbox_nl_print2_location_load)
        Me.grpbox_nl_reg2.Controls.Add(Me.txtbox_nl_print2_ip_load)
        Me.grpbox_nl_reg2.Controls.Add(Me.nl_star30)
        Me.grpbox_nl_reg2.Location = New System.Drawing.Point(18, 209)
        Me.grpbox_nl_reg2.Name = "grpbox_nl_reg2"
        Me.grpbox_nl_reg2.Size = New System.Drawing.Size(347, 168)
        Me.grpbox_nl_reg2.TabIndex = 97
        Me.grpbox_nl_reg2.TabStop = False
        Me.grpbox_nl_reg2.Text = "Register 2"
        '
        'datetimepick_nl_print2_installdate_load
        '
        Me.datetimepick_nl_print2_installdate_load.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datetimepick_nl_print2_installdate_load.Location = New System.Drawing.Point(173, 138)
        Me.datetimepick_nl_print2_installdate_load.Name = "datetimepick_nl_print2_installdate_load"
        Me.datetimepick_nl_print2_installdate_load.Size = New System.Drawing.Size(151, 21)
        Me.datetimepick_nl_print2_installdate_load.TabIndex = 98
        Me.datetimepick_nl_print2_installdate_load.Value = New Date(2018, 5, 3, 0, 0, 0, 0)
        '
        'datetimepick_nl_pin2_installdate_load
        '
        Me.datetimepick_nl_pin2_installdate_load.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datetimepick_nl_pin2_installdate_load.Location = New System.Drawing.Point(173, 69)
        Me.datetimepick_nl_pin2_installdate_load.Name = "datetimepick_nl_pin2_installdate_load"
        Me.datetimepick_nl_pin2_installdate_load.Size = New System.Drawing.Size(151, 21)
        Me.datetimepick_nl_pin2_installdate_load.TabIndex = 97
        Me.datetimepick_nl_pin2_installdate_load.Value = New Date(2018, 5, 3, 0, 0, 0, 0)
        '
        'lbl_nl_pin2_ip
        '
        Me.lbl_nl_pin2_ip.AutoSize = True
        Me.lbl_nl_pin2_ip.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_pin2_ip.Location = New System.Drawing.Point(11, 26)
        Me.lbl_nl_pin2_ip.Name = "lbl_nl_pin2_ip"
        Me.lbl_nl_pin2_ip.Size = New System.Drawing.Size(132, 15)
        Me.lbl_nl_pin2_ip.TabIndex = 88
        Me.lbl_nl_pin2_ip.Text = "Pinpad2 IPaddress:"
        '
        'nl_star35
        '
        Me.nl_star35.AutoSize = True
        Me.nl_star35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star35.ForeColor = System.Drawing.Color.Red
        Me.nl_star35.Location = New System.Drawing.Point(326, 141)
        Me.nl_star35.Name = "nl_star35"
        Me.nl_star35.Size = New System.Drawing.Size(13, 15)
        Me.nl_star35.TabIndex = 95
        Me.nl_star35.Text = "*"
        '
        'lbl_nl_pin2_location
        '
        Me.lbl_nl_pin2_location.AutoSize = True
        Me.lbl_nl_pin2_location.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_pin2_location.Location = New System.Drawing.Point(11, 49)
        Me.lbl_nl_pin2_location.Name = "lbl_nl_pin2_location"
        Me.lbl_nl_pin2_location.Size = New System.Drawing.Size(123, 15)
        Me.lbl_nl_pin2_location.TabIndex = 89
        Me.lbl_nl_pin2_location.Text = "Pinpad2 Location:"
        '
        'txtbox_nl_pin2_location_load
        '
        Me.txtbox_nl_pin2_location_load.Location = New System.Drawing.Point(173, 46)
        Me.txtbox_nl_pin2_location_load.Name = "txtbox_nl_pin2_location_load"
        Me.txtbox_nl_pin2_location_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_pin2_location_load.TabIndex = 75
        '
        'txtbox_nl_pin2_ip_load
        '
        Me.txtbox_nl_pin2_ip_load.Location = New System.Drawing.Point(173, 23)
        Me.txtbox_nl_pin2_ip_load.Name = "txtbox_nl_pin2_ip_load"
        Me.txtbox_nl_pin2_ip_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_pin2_ip_load.TabIndex = 74
        '
        'lbl_nl_print2_installdate
        '
        Me.lbl_nl_print2_installdate.AutoSize = True
        Me.lbl_nl_print2_installdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_print2_installdate.Location = New System.Drawing.Point(11, 142)
        Me.lbl_nl_print2_installdate.Name = "lbl_nl_print2_installdate"
        Me.lbl_nl_print2_installdate.Size = New System.Drawing.Size(139, 15)
        Me.lbl_nl_print2_installdate.TabIndex = 84
        Me.lbl_nl_print2_installdate.Text = "Printer2 Install Date:"
        '
        'nl_star34
        '
        Me.nl_star34.AutoSize = True
        Me.nl_star34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star34.ForeColor = System.Drawing.Color.Red
        Me.nl_star34.Location = New System.Drawing.Point(326, 118)
        Me.nl_star34.Name = "nl_star34"
        Me.nl_star34.Size = New System.Drawing.Size(13, 15)
        Me.nl_star34.TabIndex = 87
        Me.nl_star34.Text = "*"
        '
        'nl_star32
        '
        Me.nl_star32.AutoSize = True
        Me.nl_star32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star32.ForeColor = System.Drawing.Color.Red
        Me.nl_star32.Location = New System.Drawing.Point(326, 72)
        Me.nl_star32.Name = "nl_star32"
        Me.nl_star32.Size = New System.Drawing.Size(13, 15)
        Me.nl_star32.TabIndex = 93
        Me.nl_star32.Text = "*"
        '
        'lbl_nl_pin2_installdate
        '
        Me.lbl_nl_pin2_installdate.AutoSize = True
        Me.lbl_nl_pin2_installdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_pin2_installdate.Location = New System.Drawing.Point(11, 73)
        Me.lbl_nl_pin2_installdate.Name = "lbl_nl_pin2_installdate"
        Me.lbl_nl_pin2_installdate.Size = New System.Drawing.Size(141, 15)
        Me.lbl_nl_pin2_installdate.TabIndex = 90
        Me.lbl_nl_pin2_installdate.Text = "Pinpad2 Install Date:"
        '
        'lbl_nl_print2_location
        '
        Me.lbl_nl_print2_location.AutoSize = True
        Me.lbl_nl_print2_location.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_print2_location.Location = New System.Drawing.Point(11, 119)
        Me.lbl_nl_print2_location.Name = "lbl_nl_print2_location"
        Me.lbl_nl_print2_location.Size = New System.Drawing.Size(121, 15)
        Me.lbl_nl_print2_location.TabIndex = 83
        Me.lbl_nl_print2_location.Text = "Printer2 Location:"
        '
        'nl_star31
        '
        Me.nl_star31.AutoSize = True
        Me.nl_star31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star31.ForeColor = System.Drawing.Color.Red
        Me.nl_star31.Location = New System.Drawing.Point(326, 48)
        Me.nl_star31.Name = "nl_star31"
        Me.nl_star31.Size = New System.Drawing.Size(13, 15)
        Me.nl_star31.TabIndex = 92
        Me.nl_star31.Text = "*"
        '
        'lbl_nl_print2_ip
        '
        Me.lbl_nl_print2_ip.AutoSize = True
        Me.lbl_nl_print2_ip.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_print2_ip.Location = New System.Drawing.Point(11, 96)
        Me.lbl_nl_print2_ip.Name = "lbl_nl_print2_ip"
        Me.lbl_nl_print2_ip.Size = New System.Drawing.Size(130, 15)
        Me.lbl_nl_print2_ip.TabIndex = 82
        Me.lbl_nl_print2_ip.Text = "Printer2 IPaddress:"
        '
        'txtbox_nl_print2_location_load
        '
        Me.txtbox_nl_print2_location_load.Location = New System.Drawing.Point(173, 115)
        Me.txtbox_nl_print2_location_load.Name = "txtbox_nl_print2_location_load"
        Me.txtbox_nl_print2_location_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_print2_location_load.TabIndex = 78
        '
        'txtbox_nl_print2_ip_load
        '
        Me.txtbox_nl_print2_ip_load.Location = New System.Drawing.Point(173, 92)
        Me.txtbox_nl_print2_ip_load.Name = "txtbox_nl_print2_ip_load"
        Me.txtbox_nl_print2_ip_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_print2_ip_load.TabIndex = 77
        '
        'nl_star30
        '
        Me.nl_star30.AutoSize = True
        Me.nl_star30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star30.ForeColor = System.Drawing.Color.Red
        Me.nl_star30.Location = New System.Drawing.Point(326, 25)
        Me.nl_star30.Name = "nl_star30"
        Me.nl_star30.Size = New System.Drawing.Size(13, 15)
        Me.nl_star30.TabIndex = 91
        Me.nl_star30.Text = "*"
        '
        'grpbox_nl_reg1
        '
        Me.grpbox_nl_reg1.Controls.Add(Me.datetimepick_nl_print1_installdate_load)
        Me.grpbox_nl_reg1.Controls.Add(Me.datetimepick_nl_pin1_installdate_load)
        Me.grpbox_nl_reg1.Controls.Add(Me.lbl_nl_pin1_ip)
        Me.grpbox_nl_reg1.Controls.Add(Me.nl_star29)
        Me.grpbox_nl_reg1.Controls.Add(Me.lbl_nl_pin1_location)
        Me.grpbox_nl_reg1.Controls.Add(Me.txtbox_nl_pin1_location_load)
        Me.grpbox_nl_reg1.Controls.Add(Me.txtbox_nl_pin1_ip_load)
        Me.grpbox_nl_reg1.Controls.Add(Me.lbl_nl_print1_installdate)
        Me.grpbox_nl_reg1.Controls.Add(Me.nl_star28)
        Me.grpbox_nl_reg1.Controls.Add(Me.nl_star26)
        Me.grpbox_nl_reg1.Controls.Add(Me.lbl_nl_pin1_installdate)
        Me.grpbox_nl_reg1.Controls.Add(Me.lbl_nl_print1_location)
        Me.grpbox_nl_reg1.Controls.Add(Me.nl_star25)
        Me.grpbox_nl_reg1.Controls.Add(Me.lbl_nl_print1_ip)
        Me.grpbox_nl_reg1.Controls.Add(Me.txtbox_nl_print1_location_load)
        Me.grpbox_nl_reg1.Controls.Add(Me.txtbox_nl_print1_ip_load)
        Me.grpbox_nl_reg1.Location = New System.Drawing.Point(18, 26)
        Me.grpbox_nl_reg1.Name = "grpbox_nl_reg1"
        Me.grpbox_nl_reg1.Size = New System.Drawing.Size(347, 169)
        Me.grpbox_nl_reg1.TabIndex = 96
        Me.grpbox_nl_reg1.TabStop = False
        Me.grpbox_nl_reg1.Text = "Register 1"
        '
        'datetimepick_nl_print1_installdate_load
        '
        Me.datetimepick_nl_print1_installdate_load.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datetimepick_nl_print1_installdate_load.Location = New System.Drawing.Point(173, 137)
        Me.datetimepick_nl_print1_installdate_load.Name = "datetimepick_nl_print1_installdate_load"
        Me.datetimepick_nl_print1_installdate_load.Size = New System.Drawing.Size(151, 21)
        Me.datetimepick_nl_print1_installdate_load.TabIndex = 96
        Me.datetimepick_nl_print1_installdate_load.Value = New Date(2018, 5, 3, 0, 0, 0, 0)
        '
        'datetimepick_nl_pin1_installdate_load
        '
        Me.datetimepick_nl_pin1_installdate_load.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.datetimepick_nl_pin1_installdate_load.Location = New System.Drawing.Point(173, 68)
        Me.datetimepick_nl_pin1_installdate_load.Name = "datetimepick_nl_pin1_installdate_load"
        Me.datetimepick_nl_pin1_installdate_load.Size = New System.Drawing.Size(151, 21)
        Me.datetimepick_nl_pin1_installdate_load.TabIndex = 77
        Me.datetimepick_nl_pin1_installdate_load.Value = New Date(2018, 5, 3, 0, 0, 0, 0)
        '
        'lbl_nl_pin1_ip
        '
        Me.lbl_nl_pin1_ip.AutoSize = True
        Me.lbl_nl_pin1_ip.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_pin1_ip.Location = New System.Drawing.Point(11, 26)
        Me.lbl_nl_pin1_ip.Name = "lbl_nl_pin1_ip"
        Me.lbl_nl_pin1_ip.Size = New System.Drawing.Size(132, 15)
        Me.lbl_nl_pin1_ip.TabIndex = 88
        Me.lbl_nl_pin1_ip.Text = "Pinpad1 IPaddress:"
        '
        'nl_star29
        '
        Me.nl_star29.AutoSize = True
        Me.nl_star29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star29.ForeColor = System.Drawing.Color.Red
        Me.nl_star29.Location = New System.Drawing.Point(326, 141)
        Me.nl_star29.Name = "nl_star29"
        Me.nl_star29.Size = New System.Drawing.Size(13, 15)
        Me.nl_star29.TabIndex = 95
        Me.nl_star29.Text = "*"
        '
        'lbl_nl_pin1_location
        '
        Me.lbl_nl_pin1_location.AutoSize = True
        Me.lbl_nl_pin1_location.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_pin1_location.Location = New System.Drawing.Point(11, 49)
        Me.lbl_nl_pin1_location.Name = "lbl_nl_pin1_location"
        Me.lbl_nl_pin1_location.Size = New System.Drawing.Size(123, 15)
        Me.lbl_nl_pin1_location.TabIndex = 89
        Me.lbl_nl_pin1_location.Text = "Pinpad1 Location:"
        '
        'txtbox_nl_pin1_location_load
        '
        Me.txtbox_nl_pin1_location_load.Location = New System.Drawing.Point(173, 46)
        Me.txtbox_nl_pin1_location_load.Name = "txtbox_nl_pin1_location_load"
        Me.txtbox_nl_pin1_location_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_pin1_location_load.TabIndex = 75
        '
        'txtbox_nl_pin1_ip_load
        '
        Me.txtbox_nl_pin1_ip_load.Location = New System.Drawing.Point(173, 23)
        Me.txtbox_nl_pin1_ip_load.Name = "txtbox_nl_pin1_ip_load"
        Me.txtbox_nl_pin1_ip_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_pin1_ip_load.TabIndex = 74
        '
        'lbl_nl_print1_installdate
        '
        Me.lbl_nl_print1_installdate.AutoSize = True
        Me.lbl_nl_print1_installdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_print1_installdate.Location = New System.Drawing.Point(11, 142)
        Me.lbl_nl_print1_installdate.Name = "lbl_nl_print1_installdate"
        Me.lbl_nl_print1_installdate.Size = New System.Drawing.Size(139, 15)
        Me.lbl_nl_print1_installdate.TabIndex = 84
        Me.lbl_nl_print1_installdate.Text = "Printer1 Install Date:"
        '
        'nl_star28
        '
        Me.nl_star28.AutoSize = True
        Me.nl_star28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star28.ForeColor = System.Drawing.Color.Red
        Me.nl_star28.Location = New System.Drawing.Point(326, 118)
        Me.nl_star28.Name = "nl_star28"
        Me.nl_star28.Size = New System.Drawing.Size(13, 15)
        Me.nl_star28.TabIndex = 87
        Me.nl_star28.Text = "*"
        '
        'nl_star26
        '
        Me.nl_star26.AutoSize = True
        Me.nl_star26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star26.ForeColor = System.Drawing.Color.Red
        Me.nl_star26.Location = New System.Drawing.Point(326, 72)
        Me.nl_star26.Name = "nl_star26"
        Me.nl_star26.Size = New System.Drawing.Size(13, 15)
        Me.nl_star26.TabIndex = 93
        Me.nl_star26.Text = "*"
        '
        'lbl_nl_pin1_installdate
        '
        Me.lbl_nl_pin1_installdate.AutoSize = True
        Me.lbl_nl_pin1_installdate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_pin1_installdate.Location = New System.Drawing.Point(11, 73)
        Me.lbl_nl_pin1_installdate.Name = "lbl_nl_pin1_installdate"
        Me.lbl_nl_pin1_installdate.Size = New System.Drawing.Size(141, 15)
        Me.lbl_nl_pin1_installdate.TabIndex = 90
        Me.lbl_nl_pin1_installdate.Text = "Pinpad1 Install Date:"
        '
        'lbl_nl_print1_location
        '
        Me.lbl_nl_print1_location.AutoSize = True
        Me.lbl_nl_print1_location.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_print1_location.Location = New System.Drawing.Point(11, 119)
        Me.lbl_nl_print1_location.Name = "lbl_nl_print1_location"
        Me.lbl_nl_print1_location.Size = New System.Drawing.Size(121, 15)
        Me.lbl_nl_print1_location.TabIndex = 83
        Me.lbl_nl_print1_location.Text = "Printer1 Location:"
        '
        'nl_star25
        '
        Me.nl_star25.AutoSize = True
        Me.nl_star25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star25.ForeColor = System.Drawing.Color.Red
        Me.nl_star25.Location = New System.Drawing.Point(326, 48)
        Me.nl_star25.Name = "nl_star25"
        Me.nl_star25.Size = New System.Drawing.Size(13, 15)
        Me.nl_star25.TabIndex = 92
        Me.nl_star25.Text = "*"
        '
        'lbl_nl_print1_ip
        '
        Me.lbl_nl_print1_ip.AutoSize = True
        Me.lbl_nl_print1_ip.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_print1_ip.Location = New System.Drawing.Point(11, 96)
        Me.lbl_nl_print1_ip.Name = "lbl_nl_print1_ip"
        Me.lbl_nl_print1_ip.Size = New System.Drawing.Size(130, 15)
        Me.lbl_nl_print1_ip.TabIndex = 82
        Me.lbl_nl_print1_ip.Text = "Printer1 IPaddress:"
        '
        'txtbox_nl_print1_location_load
        '
        Me.txtbox_nl_print1_location_load.Location = New System.Drawing.Point(173, 115)
        Me.txtbox_nl_print1_location_load.Name = "txtbox_nl_print1_location_load"
        Me.txtbox_nl_print1_location_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_print1_location_load.TabIndex = 78
        '
        'txtbox_nl_print1_ip_load
        '
        Me.txtbox_nl_print1_ip_load.Location = New System.Drawing.Point(173, 92)
        Me.txtbox_nl_print1_ip_load.Name = "txtbox_nl_print1_ip_load"
        Me.txtbox_nl_print1_ip_load.Size = New System.Drawing.Size(151, 21)
        Me.txtbox_nl_print1_ip_load.TabIndex = 77
        '
        'grpbox_nl_lab_details
        '
        Me.grpbox_nl_lab_details.Controls.Add(Me.txtbox_labs_comments_load)
        Me.grpbox_nl_lab_details.Controls.Add(Me.lbl_nl_comments)
        Me.grpbox_nl_lab_details.Controls.Add(Me.txtbox_nl_epatch_load)
        Me.grpbox_nl_lab_details.Controls.Add(Me.lbl_nl_epatch)
        Me.grpbox_nl_lab_details.Controls.Add(Me.Label34)
        Me.grpbox_nl_lab_details.Controls.Add(Me.txtbox_nl_pm_load)
        Me.grpbox_nl_lab_details.Controls.Add(Me.lbl_nl_pm)
        Me.grpbox_nl_lab_details.Controls.Add(Me.Label32)
        Me.grpbox_nl_lab_details.Controls.Add(Me.txtbox_nl_project_load)
        Me.grpbox_nl_lab_details.Controls.Add(Me.lbl_nl_project)
        Me.grpbox_nl_lab_details.Controls.Add(Me.lbl_nl_status)
        Me.grpbox_nl_lab_details.Controls.Add(Me.cmbbox_nl_status_load)
        Me.grpbox_nl_lab_details.Controls.Add(Me.Label31)
        Me.grpbox_nl_lab_details.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_nl_lab_details.Location = New System.Drawing.Point(1047, 62)
        Me.grpbox_nl_lab_details.Name = "grpbox_nl_lab_details"
        Me.grpbox_nl_lab_details.Size = New System.Drawing.Size(303, 313)
        Me.grpbox_nl_lab_details.TabIndex = 53
        Me.grpbox_nl_lab_details.TabStop = False
        Me.grpbox_nl_lab_details.Text = "LAB details"
        '
        'txtbox_labs_comments_load
        '
        Me.txtbox_labs_comments_load.Location = New System.Drawing.Point(15, 144)
        Me.txtbox_labs_comments_load.Name = "txtbox_labs_comments_load"
        Me.txtbox_labs_comments_load.Size = New System.Drawing.Size(276, 155)
        Me.txtbox_labs_comments_load.TabIndex = 62
        Me.txtbox_labs_comments_load.Text = ""
        '
        'lbl_nl_comments
        '
        Me.lbl_nl_comments.AutoSize = True
        Me.lbl_nl_comments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_comments.Location = New System.Drawing.Point(6, 122)
        Me.lbl_nl_comments.Name = "lbl_nl_comments"
        Me.lbl_nl_comments.Size = New System.Drawing.Size(68, 13)
        Me.lbl_nl_comments.TabIndex = 61
        Me.lbl_nl_comments.Text = "Comments:"
        '
        'txtbox_nl_epatch_load
        '
        Me.txtbox_nl_epatch_load.Location = New System.Drawing.Point(117, 94)
        Me.txtbox_nl_epatch_load.Name = "txtbox_nl_epatch_load"
        Me.txtbox_nl_epatch_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_nl_epatch_load.TabIndex = 58
        '
        'lbl_nl_epatch
        '
        Me.lbl_nl_epatch.AutoSize = True
        Me.lbl_nl_epatch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_epatch.Location = New System.Drawing.Point(6, 99)
        Me.lbl_nl_epatch.Name = "lbl_nl_epatch"
        Me.lbl_nl_epatch.Size = New System.Drawing.Size(90, 13)
        Me.lbl_nl_epatch.TabIndex = 59
        Me.lbl_nl_epatch.Text = "Extra Patches:"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.Color.Red
        Me.Label34.Location = New System.Drawing.Point(240, 73)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(13, 15)
        Me.Label34.TabIndex = 57
        Me.Label34.Text = "*"
        '
        'txtbox_nl_pm_load
        '
        Me.txtbox_nl_pm_load.Location = New System.Drawing.Point(117, 71)
        Me.txtbox_nl_pm_load.Name = "txtbox_nl_pm_load"
        Me.txtbox_nl_pm_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_nl_pm_load.TabIndex = 55
        '
        'lbl_nl_pm
        '
        Me.lbl_nl_pm.AutoSize = True
        Me.lbl_nl_pm.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_pm.Location = New System.Drawing.Point(6, 77)
        Me.lbl_nl_pm.Name = "lbl_nl_pm"
        Me.lbl_nl_pm.Size = New System.Drawing.Size(29, 13)
        Me.lbl_nl_pm.TabIndex = 56
        Me.lbl_nl_pm.Text = "PM:"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.Red
        Me.Label32.Location = New System.Drawing.Point(240, 48)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(13, 15)
        Me.Label32.TabIndex = 54
        Me.Label32.Text = "*"
        '
        'txtbox_nl_project_load
        '
        Me.txtbox_nl_project_load.Location = New System.Drawing.Point(117, 47)
        Me.txtbox_nl_project_load.Name = "txtbox_nl_project_load"
        Me.txtbox_nl_project_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_nl_project_load.TabIndex = 52
        '
        'lbl_nl_project
        '
        Me.lbl_nl_project.AutoSize = True
        Me.lbl_nl_project.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_project.Location = New System.Drawing.Point(6, 55)
        Me.lbl_nl_project.Name = "lbl_nl_project"
        Me.lbl_nl_project.Size = New System.Drawing.Size(51, 13)
        Me.lbl_nl_project.TabIndex = 53
        Me.lbl_nl_project.Text = "Project:"
        '
        'lbl_nl_status
        '
        Me.lbl_nl_status.AutoSize = True
        Me.lbl_nl_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_status.Location = New System.Drawing.Point(6, 27)
        Me.lbl_nl_status.Name = "lbl_nl_status"
        Me.lbl_nl_status.Size = New System.Drawing.Size(47, 13)
        Me.lbl_nl_status.TabIndex = 50
        Me.lbl_nl_status.Text = "Status:"
        '
        'cmbbox_nl_status_load
        '
        Me.cmbbox_nl_status_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_nl_status_load.FormattingEnabled = True
        Me.cmbbox_nl_status_load.Items.AddRange(New Object() {"", "Available", "Decommissioned", "Reserved by Project", "Staging In progress", "To Rebuild"})
        Me.cmbbox_nl_status_load.Location = New System.Drawing.Point(117, 21)
        Me.cmbbox_nl_status_load.Name = "cmbbox_nl_status_load"
        Me.cmbbox_nl_status_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_nl_status_load.TabIndex = 49
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.Red
        Me.Label31.Location = New System.Drawing.Point(240, 25)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(13, 15)
        Me.Label31.TabIndex = 51
        Me.Label31.Text = "*"
        '
        'grpbox_nl_nbdev
        '
        Me.grpbox_nl_nbdev.Controls.Add(Me.nl_star11)
        Me.grpbox_nl_nbdev.Controls.Add(Me.cmbbox_nl_nbofreg_load)
        Me.grpbox_nl_nbdev.Controls.Add(Me.lbl_nl_nbofreg)
        Me.grpbox_nl_nbdev.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_nl_nbdev.Location = New System.Drawing.Point(12, 381)
        Me.grpbox_nl_nbdev.Name = "grpbox_nl_nbdev"
        Me.grpbox_nl_nbdev.Size = New System.Drawing.Size(271, 74)
        Me.grpbox_nl_nbdev.TabIndex = 2
        Me.grpbox_nl_nbdev.TabStop = False
        Me.grpbox_nl_nbdev.Text = "Equipment:"
        '
        'nl_star11
        '
        Me.nl_star11.AutoSize = True
        Me.nl_star11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nl_star11.ForeColor = System.Drawing.Color.Red
        Me.nl_star11.Location = New System.Drawing.Point(248, 35)
        Me.nl_star11.Name = "nl_star11"
        Me.nl_star11.Size = New System.Drawing.Size(13, 15)
        Me.nl_star11.TabIndex = 40
        Me.nl_star11.Text = "*"
        '
        'cmbbox_nl_nbofreg_load
        '
        Me.cmbbox_nl_nbofreg_load.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbbox_nl_nbofreg_load.FormattingEnabled = True
        Me.cmbbox_nl_nbofreg_load.Items.AddRange(New Object() {"", "0", "1", "2"})
        Me.cmbbox_nl_nbofreg_load.Location = New System.Drawing.Point(125, 32)
        Me.cmbbox_nl_nbofreg_load.Name = "cmbbox_nl_nbofreg_load"
        Me.cmbbox_nl_nbofreg_load.Size = New System.Drawing.Size(121, 23)
        Me.cmbbox_nl_nbofreg_load.TabIndex = 39
        '
        'lbl_nl_nbofreg
        '
        Me.lbl_nl_nbofreg.AutoSize = True
        Me.lbl_nl_nbofreg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nl_nbofreg.Location = New System.Drawing.Point(14, 35)
        Me.lbl_nl_nbofreg.Name = "lbl_nl_nbofreg"
        Me.lbl_nl_nbofreg.Size = New System.Drawing.Size(77, 15)
        Me.lbl_nl_nbofreg.TabIndex = 41
        Me.lbl_nl_nbofreg.Text = "No of regs:"
        '
        'btn_nl_close
        '
        Me.btn_nl_close.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_nl_close.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_nl_close.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_nl_close.Location = New System.Drawing.Point(1089, 424)
        Me.btn_nl_close.Name = "btn_nl_close"
        Me.btn_nl_close.Size = New System.Drawing.Size(81, 21)
        Me.btn_nl_close.TabIndex = 65
        Me.btn_nl_close.Text = "Close"
        Me.btn_nl_close.UseVisualStyleBackColor = False
        '
        'btn_nl_clear_form
        '
        Me.btn_nl_clear_form.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_nl_clear_form.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_nl_clear_form.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_nl_clear_form.Location = New System.Drawing.Point(1176, 424)
        Me.btn_nl_clear_form.Name = "btn_nl_clear_form"
        Me.btn_nl_clear_form.Size = New System.Drawing.Size(81, 21)
        Me.btn_nl_clear_form.TabIndex = 64
        Me.btn_nl_clear_form.Text = "Clear"
        Me.btn_nl_clear_form.UseVisualStyleBackColor = False
        '
        'btn_nl_add
        '
        Me.btn_nl_add.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_nl_add.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_nl_add.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_nl_add.Location = New System.Drawing.Point(1263, 424)
        Me.btn_nl_add.Name = "btn_nl_add"
        Me.btn_nl_add.Size = New System.Drawing.Size(81, 21)
        Me.btn_nl_add.TabIndex = 63
        Me.btn_nl_add.Text = "ADD"
        Me.btn_nl_add.UseVisualStyleBackColor = False
        '
        'LABS_new
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1362, 468)
        Me.Controls.Add(Me.btn_nl_add)
        Me.Controls.Add(Me.btn_nl_clear_form)
        Me.Controls.Add(Me.btn_nl_close)
        Me.Controls.Add(Me.grpbox_nl_nbdev)
        Me.Controls.Add(Me.grpbox_nl_lab_details)
        Me.Controls.Add(Me.grpbox_nl_equip)
        Me.Controls.Add(Me.grpbox_nl_network)
        Me.Controls.Add(Me.grpbox_nl_store)
        Me.Controls.Add(Me.nl_star0)
        Me.Controls.Add(Me.lbl_nl_info)
        Me.Controls.Add(Me.lbl_nl_txt)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "LABS_new"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - NEW LAB"
        Me.grpbox_nl_store.ResumeLayout(False)
        Me.grpbox_nl_store.PerformLayout()
        Me.grpbox_nl_network.ResumeLayout(False)
        Me.grpbox_nl_bo.ResumeLayout(False)
        Me.grpbox_nl_bo.PerformLayout()
        Me.grpbox_nl_regs.ResumeLayout(False)
        Me.grpbox_nl_regs.PerformLayout()
        Me.grpbox_nl_equip.ResumeLayout(False)
        Me.grpbox_nl_equip.PerformLayout()
        Me.grpbox_nl_reg2.ResumeLayout(False)
        Me.grpbox_nl_reg2.PerformLayout()
        Me.grpbox_nl_reg1.ResumeLayout(False)
        Me.grpbox_nl_reg1.PerformLayout()
        Me.grpbox_nl_lab_details.ResumeLayout(False)
        Me.grpbox_nl_lab_details.PerformLayout()
        Me.grpbox_nl_nbdev.ResumeLayout(False)
        Me.grpbox_nl_nbdev.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_nl_txt As Label
    Friend WithEvents lbl_nl_info As Label
    Friend WithEvents nl_star0 As Label
    Friend WithEvents grpbox_nl_store As GroupBox
    Friend WithEvents cmbbox_nl_province_load As ComboBox
    Friend WithEvents nl_star4 As Label
    Friend WithEvents cmbbox_nl_store_system As ComboBox
    Friend WithEvents lbl_nl_store_system As Label
    Friend WithEvents nl_star6 As Label
    Friend WithEvents nl_star9 As Label
    Friend WithEvents nl_star5 As Label
    Friend WithEvents nl_star10 As Label
    Friend WithEvents nl_star8 As Label
    Friend WithEvents lbl_nl_str_cntry As Label
    Friend WithEvents cmbbox_nl_country_load As ComboBox
    Friend WithEvents nl_star7 As Label
    Friend WithEvents cmbbox_nl_iscombo_load As ComboBox
    Friend WithEvents lbl_nl_iscombo As Label
    Friend WithEvents nl_star3 As Label
    Friend WithEvents nl_star1 As Label
    Friend WithEvents txtbox_nl_city_load As TextBox
    Friend WithEvents txtbox_nl_address_load As TextBox
    Friend WithEvents txtbox_nl_sisstore_load As TextBox
    Friend WithEvents cmbbox_nl_banner_load As ComboBox
    Friend WithEvents lbl_nl_str_prov As Label
    Friend WithEvents lbl_nl_str_city As Label
    Friend WithEvents lbl_nl_str_add As Label
    Friend WithEvents lbll_nl_str_sisstr As Label
    Friend WithEvents lbl_nl_str_banner As Label
    Friend WithEvents txtbox_nl_storeno_load As TextBox
    Friend WithEvents lbl_nl_storeno As Label
    Friend WithEvents nl_star2 As Label
    Friend WithEvents cmbbox_nl_env_load As ComboBox
    Friend WithEvents lbl_nl_env As Label
    Friend WithEvents btn_nl_getinfo As Button
    Friend WithEvents btn_nl_clear As Button
    Friend WithEvents grpbox_nl_network As GroupBox
    Friend WithEvents txtbox_nl_bolocation_load As TextBox
    Friend WithEvents lbl_nl_bolocation As Label
    Friend WithEvents nl_star15 As Label
    Friend WithEvents nl_star14 As Label
    Friend WithEvents nl_star13 As Label
    Friend WithEvents nl_star12 As Label
    Friend WithEvents lbl_nl_boinstalldate As Label
    Friend WithEvents nl_star21 As Label
    Friend WithEvents txtbox_nl_reg1location_load As TextBox
    Friend WithEvents lbl_nl_reg1location As Label
    Friend WithEvents txtbox_nl_reg1ipaddress_load As TextBox
    Friend WithEvents lbl_nl_reg1ipaddress As Label
    Friend WithEvents txtbox_nl_reg1hostname_load As TextBox
    Friend WithEvents lbl_nl_reg1hostname As Label
    Friend WithEvents nl_star16 As Label
    Friend WithEvents txtbox_nl_reg2location_load As TextBox
    Friend WithEvents txtbox_nl_reg2ipaddress_load As TextBox
    Friend WithEvents txtbox_nl_reg2hostname_load As TextBox
    Friend WithEvents txtbox_nl_boipaddress_load As TextBox
    Friend WithEvents txtbox_nl_bohostname_load As TextBox
    Friend WithEvents lbl_nl_reg2installdate As Label
    Friend WithEvents lbl_nl_reg2location As Label
    Friend WithEvents lbl_nl_reg2ipaddress As Label
    Friend WithEvents lbl_nl_reg2hostname As Label
    Friend WithEvents nl_star20 As Label
    Friend WithEvents lbl_nl_reg1installdate As Label
    Friend WithEvents lbl_nl_boipaddress As Label
    Friend WithEvents lbl_nl_bohostname As Label
    Friend WithEvents grpbox_nl_regs As GroupBox
    Friend WithEvents grpbox_nl_bo As GroupBox
    Friend WithEvents nl_star19 As Label
    Friend WithEvents nl_star18 As Label
    Friend WithEvents nl_star17 As Label
    Friend WithEvents nl_star23 As Label
    Friend WithEvents nl_star22 As Label
    Friend WithEvents grpbox_nl_equip As GroupBox
    Friend WithEvents nl_star29 As Label
    Friend WithEvents lbl_nl_pin1_ip As Label
    Friend WithEvents lbl_nl_print1_installdate As Label
    Friend WithEvents nl_star26 As Label
    Friend WithEvents lbl_nl_print1_location As Label
    Friend WithEvents nl_star25 As Label
    Friend WithEvents lbl_nl_print1_ip As Label
    Friend WithEvents txtbox_nl_print1_ip_load As TextBox
    Friend WithEvents txtbox_nl_print1_location_load As TextBox
    Friend WithEvents lbl_nl_pin1_installdate As Label
    Friend WithEvents nl_star28 As Label
    Friend WithEvents txtbox_nl_pin1_ip_load As TextBox
    Friend WithEvents txtbox_nl_pin1_location_load As TextBox
    Friend WithEvents lbl_nl_pin1_location As Label
    Friend WithEvents grpbox_nl_reg1 As GroupBox
    Friend WithEvents grpbox_nl_reg2 As GroupBox
    Friend WithEvents lbl_nl_pin2_ip As Label
    Friend WithEvents nl_star35 As Label
    Friend WithEvents lbl_nl_pin2_location As Label
    Friend WithEvents txtbox_nl_pin2_location_load As TextBox
    Friend WithEvents txtbox_nl_pin2_ip_load As TextBox
    Friend WithEvents lbl_nl_print2_installdate As Label
    Friend WithEvents nl_star34 As Label
    Friend WithEvents nl_star32 As Label
    Friend WithEvents lbl_nl_pin2_installdate As Label
    Friend WithEvents lbl_nl_print2_location As Label
    Friend WithEvents nl_star31 As Label
    Friend WithEvents lbl_nl_print2_ip As Label
    Friend WithEvents txtbox_nl_print2_location_load As TextBox
    Friend WithEvents txtbox_nl_print2_ip_load As TextBox
    Friend WithEvents nl_star30 As Label
    Friend WithEvents grpbox_nl_lab_details As GroupBox
    Friend WithEvents lbl_nl_status As Label
    Friend WithEvents cmbbox_nl_status_load As ComboBox
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents txtbox_nl_project_load As TextBox
    Friend WithEvents lbl_nl_project As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents txtbox_nl_pm_load As TextBox
    Friend WithEvents lbl_nl_pm As Label
    Friend WithEvents txtbox_nl_epatch_load As TextBox
    Friend WithEvents lbl_nl_epatch As Label
    Friend WithEvents lbl_nl_comments As Label
    Friend WithEvents grpbox_nl_nbdev As GroupBox
    Friend WithEvents nl_star11 As Label
    Friend WithEvents cmbbox_nl_nbofreg_load As ComboBox
    Friend WithEvents lbl_nl_nbofreg As Label
    Friend WithEvents btn_nl_close As Button
    Friend WithEvents btn_nl_clear_form As Button
    Friend WithEvents btn_nl_add As Button
    Friend WithEvents txtbox_labs_comments_load As RichTextBox
    Friend WithEvents datetimepick_nl_reg1installdate_load As DateTimePicker
    Friend WithEvents datetimepick_nl_reg2installdate_load As DateTimePicker
    Friend WithEvents datetimepick_nl_boinstalldate_load As DateTimePicker
    Friend WithEvents datetimepick_nl_print2_installdate_load As DateTimePicker
    Friend WithEvents datetimepick_nl_pin2_installdate_load As DateTimePicker
    Friend WithEvents datetimepick_nl_print1_installdate_load As DateTimePicker
    Friend WithEvents datetimepick_nl_pin1_installdate_load As DateTimePicker
    Friend WithEvents lbl_nl_na_info1 As Label
    Friend WithEvents lbl_nl_na_info2 As Label
End Class
