﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inventory_edit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(inventory_edit))
        Me.cmbbox_inventory_edit_notes = New System.Windows.Forms.ComboBox()
        Me.lbl_inventory_notes = New System.Windows.Forms.Label()
        Me.cmbbox_inventory_edit_location = New System.Windows.Forms.ComboBox()
        Me.lbl_inventory_location = New System.Windows.Forms.Label()
        Me.cmbbox_inventory_edit_status = New System.Windows.Forms.ComboBox()
        Me.lbl_inventory_stauts = New System.Windows.Forms.Label()
        Me.lbl_inventory_os = New System.Windows.Forms.Label()
        Me.cmbbox_inventory_edit_os = New System.Windows.Forms.ComboBox()
        Me.lbl_inventory_edit = New System.Windows.Forms.Label()
        Me.grpbox_inventory_edit = New System.Windows.Forms.GroupBox()
        Me.lbl_inventory_searchSN = New System.Windows.Forms.Label()
        Me.txtbox_inventory_edit_searchSN = New System.Windows.Forms.TextBox()
        Me.btn_inventory_edit_close = New System.Windows.Forms.Button()
        Me.btn_inventory_edit_save = New System.Windows.Forms.Button()
        Me.grpbox_inventory_edit.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmbbox_inventory_edit_notes
        '
        Me.cmbbox_inventory_edit_notes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbbox_inventory_edit_notes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbbox_inventory_edit_notes.FormattingEnabled = True
        Me.cmbbox_inventory_edit_notes.Location = New System.Drawing.Point(84, 140)
        Me.cmbbox_inventory_edit_notes.Name = "cmbbox_inventory_edit_notes"
        Me.cmbbox_inventory_edit_notes.Size = New System.Drawing.Size(102, 24)
        Me.cmbbox_inventory_edit_notes.TabIndex = 134
        '
        'lbl_inventory_notes
        '
        Me.lbl_inventory_notes.AutoSize = True
        Me.lbl_inventory_notes.Location = New System.Drawing.Point(31, 143)
        Me.lbl_inventory_notes.Name = "lbl_inventory_notes"
        Me.lbl_inventory_notes.Size = New System.Drawing.Size(49, 16)
        Me.lbl_inventory_notes.TabIndex = 133
        Me.lbl_inventory_notes.Text = "Notes"
        '
        'cmbbox_inventory_edit_location
        '
        Me.cmbbox_inventory_edit_location.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbbox_inventory_edit_location.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbbox_inventory_edit_location.FormattingEnabled = True
        Me.cmbbox_inventory_edit_location.Location = New System.Drawing.Point(84, 110)
        Me.cmbbox_inventory_edit_location.Name = "cmbbox_inventory_edit_location"
        Me.cmbbox_inventory_edit_location.Size = New System.Drawing.Size(102, 24)
        Me.cmbbox_inventory_edit_location.TabIndex = 132
        '
        'lbl_inventory_location
        '
        Me.lbl_inventory_location.AutoSize = True
        Me.lbl_inventory_location.Location = New System.Drawing.Point(31, 113)
        Me.lbl_inventory_location.Name = "lbl_inventory_location"
        Me.lbl_inventory_location.Size = New System.Drawing.Size(37, 16)
        Me.lbl_inventory_location.TabIndex = 131
        Me.lbl_inventory_location.Text = "Loc."
        '
        'cmbbox_inventory_edit_status
        '
        Me.cmbbox_inventory_edit_status.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbbox_inventory_edit_status.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbbox_inventory_edit_status.FormattingEnabled = True
        Me.cmbbox_inventory_edit_status.Location = New System.Drawing.Point(84, 80)
        Me.cmbbox_inventory_edit_status.Name = "cmbbox_inventory_edit_status"
        Me.cmbbox_inventory_edit_status.Size = New System.Drawing.Size(102, 24)
        Me.cmbbox_inventory_edit_status.TabIndex = 130
        '
        'lbl_inventory_stauts
        '
        Me.lbl_inventory_stauts.AutoSize = True
        Me.lbl_inventory_stauts.Location = New System.Drawing.Point(31, 83)
        Me.lbl_inventory_stauts.Name = "lbl_inventory_stauts"
        Me.lbl_inventory_stauts.Size = New System.Drawing.Size(51, 16)
        Me.lbl_inventory_stauts.TabIndex = 129
        Me.lbl_inventory_stauts.Text = "Status"
        '
        'lbl_inventory_os
        '
        Me.lbl_inventory_os.AutoSize = True
        Me.lbl_inventory_os.Location = New System.Drawing.Point(31, 53)
        Me.lbl_inventory_os.Name = "lbl_inventory_os"
        Me.lbl_inventory_os.Size = New System.Drawing.Size(29, 16)
        Me.lbl_inventory_os.TabIndex = 128
        Me.lbl_inventory_os.Text = "OS"
        '
        'cmbbox_inventory_edit_os
        '
        Me.cmbbox_inventory_edit_os.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cmbbox_inventory_edit_os.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbbox_inventory_edit_os.FormattingEnabled = True
        Me.cmbbox_inventory_edit_os.Location = New System.Drawing.Point(84, 50)
        Me.cmbbox_inventory_edit_os.Name = "cmbbox_inventory_edit_os"
        Me.cmbbox_inventory_edit_os.Size = New System.Drawing.Size(102, 24)
        Me.cmbbox_inventory_edit_os.TabIndex = 127
        '
        'lbl_inventory_edit
        '
        Me.lbl_inventory_edit.AutoSize = True
        Me.lbl_inventory_edit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_inventory_edit.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_inventory_edit.Location = New System.Drawing.Point(49, 15)
        Me.lbl_inventory_edit.Name = "lbl_inventory_edit"
        Me.lbl_inventory_edit.Size = New System.Drawing.Size(102, 16)
        Me.lbl_inventory_edit.TabIndex = 137
        Me.lbl_inventory_edit.Text = "EDIT DEVICE"
        '
        'grpbox_inventory_edit
        '
        Me.grpbox_inventory_edit.Controls.Add(Me.lbl_inventory_searchSN)
        Me.grpbox_inventory_edit.Controls.Add(Me.cmbbox_inventory_edit_os)
        Me.grpbox_inventory_edit.Controls.Add(Me.txtbox_inventory_edit_searchSN)
        Me.grpbox_inventory_edit.Controls.Add(Me.lbl_inventory_os)
        Me.grpbox_inventory_edit.Controls.Add(Me.lbl_inventory_stauts)
        Me.grpbox_inventory_edit.Controls.Add(Me.cmbbox_inventory_edit_notes)
        Me.grpbox_inventory_edit.Controls.Add(Me.cmbbox_inventory_edit_status)
        Me.grpbox_inventory_edit.Controls.Add(Me.lbl_inventory_notes)
        Me.grpbox_inventory_edit.Controls.Add(Me.lbl_inventory_location)
        Me.grpbox_inventory_edit.Controls.Add(Me.cmbbox_inventory_edit_location)
        Me.grpbox_inventory_edit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_inventory_edit.Location = New System.Drawing.Point(18, 43)
        Me.grpbox_inventory_edit.Name = "grpbox_inventory_edit"
        Me.grpbox_inventory_edit.Size = New System.Drawing.Size(270, 175)
        Me.grpbox_inventory_edit.TabIndex = 138
        Me.grpbox_inventory_edit.TabStop = False
        Me.grpbox_inventory_edit.Text = "Device Info"
        '
        'lbl_inventory_searchSN
        '
        Me.lbl_inventory_searchSN.AutoSize = True
        Me.lbl_inventory_searchSN.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_inventory_searchSN.Location = New System.Drawing.Point(31, 27)
        Me.lbl_inventory_searchSN.Name = "lbl_inventory_searchSN"
        Me.lbl_inventory_searchSN.Size = New System.Drawing.Size(28, 13)
        Me.lbl_inventory_searchSN.TabIndex = 136
        Me.lbl_inventory_searchSN.Text = "SN:"
        '
        'txtbox_inventory_edit_searchSN
        '
        Me.txtbox_inventory_edit_searchSN.Location = New System.Drawing.Point(84, 22)
        Me.txtbox_inventory_edit_searchSN.Name = "txtbox_inventory_edit_searchSN"
        Me.txtbox_inventory_edit_searchSN.Size = New System.Drawing.Size(166, 22)
        Me.txtbox_inventory_edit_searchSN.TabIndex = 135
        '
        'btn_inventory_edit_close
        '
        Me.btn_inventory_edit_close.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_inventory_edit_close.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_inventory_edit_close.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventory_edit_close.Location = New System.Drawing.Point(125, 224)
        Me.btn_inventory_edit_close.Name = "btn_inventory_edit_close"
        Me.btn_inventory_edit_close.Size = New System.Drawing.Size(72, 21)
        Me.btn_inventory_edit_close.TabIndex = 139
        Me.btn_inventory_edit_close.Text = "Close"
        Me.btn_inventory_edit_close.UseVisualStyleBackColor = False
        '
        'btn_inventory_edit_save
        '
        Me.btn_inventory_edit_save.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_inventory_edit_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_inventory_edit_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_inventory_edit_save.Location = New System.Drawing.Point(205, 224)
        Me.btn_inventory_edit_save.Name = "btn_inventory_edit_save"
        Me.btn_inventory_edit_save.Size = New System.Drawing.Size(72, 21)
        Me.btn_inventory_edit_save.TabIndex = 140
        Me.btn_inventory_edit_save.Text = "Save"
        Me.btn_inventory_edit_save.UseVisualStyleBackColor = False
        '
        'inventory_edit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(303, 258)
        Me.Controls.Add(Me.btn_inventory_edit_save)
        Me.Controls.Add(Me.btn_inventory_edit_close)
        Me.Controls.Add(Me.grpbox_inventory_edit)
        Me.Controls.Add(Me.lbl_inventory_edit)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "inventory_edit"
        Me.Text = "GDnetworks - Inventory - EDIT"
        Me.grpbox_inventory_edit.ResumeLayout(False)
        Me.grpbox_inventory_edit.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmbbox_inventory_edit_notes As ComboBox
    Friend WithEvents lbl_inventory_notes As Label
    Friend WithEvents cmbbox_inventory_edit_location As ComboBox
    Friend WithEvents lbl_inventory_location As Label
    Friend WithEvents cmbbox_inventory_edit_status As ComboBox
    Friend WithEvents lbl_inventory_stauts As Label
    Friend WithEvents lbl_inventory_os As Label
    Friend WithEvents cmbbox_inventory_edit_os As ComboBox
    Friend WithEvents lbl_inventory_edit As Label
    Friend WithEvents grpbox_inventory_edit As GroupBox
    Friend WithEvents lbl_inventory_searchSN As Label
    Friend WithEvents txtbox_inventory_edit_searchSN As TextBox
    Friend WithEvents btn_inventory_edit_close As Button
    Friend WithEvents btn_inventory_edit_save As Button
End Class
