﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Speedtest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Speedtest))
        Me.lbl_speedtest_con = New System.Windows.Forms.Label()
        Me.lbl_speedtest_file = New System.Windows.Forms.Label()
        Me.grpbox_speedtest_info = New System.Windows.Forms.GroupBox()
        Me.lbl_speedtest_time_load = New System.Windows.Forms.Label()
        Me.lbl_speedtest_time = New System.Windows.Forms.Label()
        Me.lbl_speedtest_con_load = New System.Windows.Forms.Label()
        Me.lbl_speedtest_file_load = New System.Windows.Forms.Label()
        Me.lbl_speedtest_store_load = New System.Windows.Forms.Label()
        Me.lbl_speedtest_store = New System.Windows.Forms.Label()
        Me.progbar_speedtest = New System.Windows.Forms.ProgressBar()
        Me.lbl_speedtest_testing = New System.Windows.Forms.Label()
        Me.grpbox_speedtest_results = New System.Windows.Forms.GroupBox()
        Me.lbl_speedtest_up_load = New System.Windows.Forms.Label()
        Me.lbl_speedtest_up = New System.Windows.Forms.Label()
        Me.lbl_speedtest_ping_load = New System.Windows.Forms.Label()
        Me.lbl_speedtest_dl_load = New System.Windows.Forms.Label()
        Me.lbl_speedtest_server_load = New System.Windows.Forms.Label()
        Me.lbl_speedtest_server = New System.Windows.Forms.Label()
        Me.lbl_speedtest_dl = New System.Windows.Forms.Label()
        Me.lbl_speedtest_ping = New System.Windows.Forms.Label()
        Me.btn_speedtest_save = New System.Windows.Forms.Button()
        Me.btn_speedtest_close = New System.Windows.Forms.Button()
        Me.btn_speedtest_run = New System.Windows.Forms.Button()
        Me.grpbox_speedtest_info.SuspendLayout()
        Me.grpbox_speedtest_results.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_speedtest_con
        '
        Me.lbl_speedtest_con.AutoSize = True
        Me.lbl_speedtest_con.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_con.Location = New System.Drawing.Point(19, 54)
        Me.lbl_speedtest_con.Name = "lbl_speedtest_con"
        Me.lbl_speedtest_con.Size = New System.Drawing.Size(121, 13)
        Me.lbl_speedtest_con.TabIndex = 1
        Me.lbl_speedtest_con.Text = "Remote connection:"
        '
        'lbl_speedtest_file
        '
        Me.lbl_speedtest_file.AutoSize = True
        Me.lbl_speedtest_file.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_file.Location = New System.Drawing.Point(19, 78)
        Me.lbl_speedtest_file.Name = "lbl_speedtest_file"
        Me.lbl_speedtest_file.Size = New System.Drawing.Size(99, 13)
        Me.lbl_speedtest_file.TabIndex = 15
        Me.lbl_speedtest_file.Text = "Speedtest tools:"
        '
        'grpbox_speedtest_info
        '
        Me.grpbox_speedtest_info.Controls.Add(Me.lbl_speedtest_time_load)
        Me.grpbox_speedtest_info.Controls.Add(Me.lbl_speedtest_time)
        Me.grpbox_speedtest_info.Controls.Add(Me.lbl_speedtest_con_load)
        Me.grpbox_speedtest_info.Controls.Add(Me.lbl_speedtest_file_load)
        Me.grpbox_speedtest_info.Controls.Add(Me.lbl_speedtest_store_load)
        Me.grpbox_speedtest_info.Controls.Add(Me.lbl_speedtest_store)
        Me.grpbox_speedtest_info.Controls.Add(Me.lbl_speedtest_file)
        Me.grpbox_speedtest_info.Controls.Add(Me.lbl_speedtest_con)
        Me.grpbox_speedtest_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_speedtest_info.Location = New System.Drawing.Point(25, 17)
        Me.grpbox_speedtest_info.Name = "grpbox_speedtest_info"
        Me.grpbox_speedtest_info.Size = New System.Drawing.Size(218, 120)
        Me.grpbox_speedtest_info.TabIndex = 16
        Me.grpbox_speedtest_info.TabStop = False
        Me.grpbox_speedtest_info.Text = "Store Info"
        '
        'lbl_speedtest_time_load
        '
        Me.lbl_speedtest_time_load.AutoSize = True
        Me.lbl_speedtest_time_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_time_load.Location = New System.Drawing.Point(143, 102)
        Me.lbl_speedtest_time_load.Name = "lbl_speedtest_time_load"
        Me.lbl_speedtest_time_load.Size = New System.Drawing.Size(34, 13)
        Me.lbl_speedtest_time_load.TabIndex = 22
        Me.lbl_speedtest_time_load.Text = "timer"
        '
        'lbl_speedtest_time
        '
        Me.lbl_speedtest_time.AutoSize = True
        Me.lbl_speedtest_time.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_time.Location = New System.Drawing.Point(19, 102)
        Me.lbl_speedtest_time.Name = "lbl_speedtest_time"
        Me.lbl_speedtest_time.Size = New System.Drawing.Size(115, 13)
        Me.lbl_speedtest_time.TabIndex = 21
        Me.lbl_speedtest_time.Text = "Elapsed time (sec):"
        '
        'lbl_speedtest_con_load
        '
        Me.lbl_speedtest_con_load.AutoSize = True
        Me.lbl_speedtest_con_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_con_load.Location = New System.Drawing.Point(143, 54)
        Me.lbl_speedtest_con_load.Name = "lbl_speedtest_con_load"
        Me.lbl_speedtest_con_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_speedtest_con_load.TabIndex = 20
        Me.lbl_speedtest_con_load.Text = "status"
        '
        'lbl_speedtest_file_load
        '
        Me.lbl_speedtest_file_load.AutoSize = True
        Me.lbl_speedtest_file_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_file_load.Location = New System.Drawing.Point(143, 78)
        Me.lbl_speedtest_file_load.Name = "lbl_speedtest_file_load"
        Me.lbl_speedtest_file_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_speedtest_file_load.TabIndex = 19
        Me.lbl_speedtest_file_load.Text = "status"
        '
        'lbl_speedtest_store_load
        '
        Me.lbl_speedtest_store_load.AutoSize = True
        Me.lbl_speedtest_store_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_store_load.Location = New System.Drawing.Point(143, 30)
        Me.lbl_speedtest_store_load.Name = "lbl_speedtest_store_load"
        Me.lbl_speedtest_store_load.Size = New System.Drawing.Size(42, 13)
        Me.lbl_speedtest_store_load.TabIndex = 18
        Me.lbl_speedtest_store_load.Text = "str_no"
        '
        'lbl_speedtest_store
        '
        Me.lbl_speedtest_store.AutoSize = True
        Me.lbl_speedtest_store.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_store.Location = New System.Drawing.Point(19, 30)
        Me.lbl_speedtest_store.Name = "lbl_speedtest_store"
        Me.lbl_speedtest_store.Size = New System.Drawing.Size(49, 13)
        Me.lbl_speedtest_store.TabIndex = 17
        Me.lbl_speedtest_store.Text = "Store#:"
        '
        'progbar_speedtest
        '
        Me.progbar_speedtest.Location = New System.Drawing.Point(40, 146)
        Me.progbar_speedtest.Name = "progbar_speedtest"
        Me.progbar_speedtest.Size = New System.Drawing.Size(177, 23)
        Me.progbar_speedtest.TabIndex = 17
        '
        'lbl_speedtest_testing
        '
        Me.lbl_speedtest_testing.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_testing.Location = New System.Drawing.Point(57, 151)
        Me.lbl_speedtest_testing.Name = "lbl_speedtest_testing"
        Me.lbl_speedtest_testing.Size = New System.Drawing.Size(146, 13)
        Me.lbl_speedtest_testing.TabIndex = 21
        Me.lbl_speedtest_testing.Text = "Testing internet speed..."
        Me.lbl_speedtest_testing.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'grpbox_speedtest_results
        '
        Me.grpbox_speedtest_results.Controls.Add(Me.lbl_speedtest_up_load)
        Me.grpbox_speedtest_results.Controls.Add(Me.lbl_speedtest_up)
        Me.grpbox_speedtest_results.Controls.Add(Me.lbl_speedtest_ping_load)
        Me.grpbox_speedtest_results.Controls.Add(Me.lbl_speedtest_dl_load)
        Me.grpbox_speedtest_results.Controls.Add(Me.lbl_speedtest_server_load)
        Me.grpbox_speedtest_results.Controls.Add(Me.lbl_speedtest_server)
        Me.grpbox_speedtest_results.Controls.Add(Me.lbl_speedtest_dl)
        Me.grpbox_speedtest_results.Controls.Add(Me.lbl_speedtest_ping)
        Me.grpbox_speedtest_results.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_speedtest_results.Location = New System.Drawing.Point(249, 17)
        Me.grpbox_speedtest_results.Name = "grpbox_speedtest_results"
        Me.grpbox_speedtest_results.Size = New System.Drawing.Size(452, 120)
        Me.grpbox_speedtest_results.TabIndex = 21
        Me.grpbox_speedtest_results.TabStop = False
        Me.grpbox_speedtest_results.Text = "Results"
        '
        'lbl_speedtest_up_load
        '
        Me.lbl_speedtest_up_load.AutoSize = True
        Me.lbl_speedtest_up_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_up_load.Location = New System.Drawing.Point(120, 102)
        Me.lbl_speedtest_up_load.Name = "lbl_speedtest_up_load"
        Me.lbl_speedtest_up_load.Size = New System.Drawing.Size(62, 13)
        Me.lbl_speedtest_up_load.TabIndex = 22
        Me.lbl_speedtest_up_load.Text = "speed_up"
        '
        'lbl_speedtest_up
        '
        Me.lbl_speedtest_up.AutoSize = True
        Me.lbl_speedtest_up.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_up.Location = New System.Drawing.Point(17, 102)
        Me.lbl_speedtest_up.Name = "lbl_speedtest_up"
        Me.lbl_speedtest_up.Size = New System.Drawing.Size(93, 13)
        Me.lbl_speedtest_up.TabIndex = 21
        Me.lbl_speedtest_up.Text = "Upload (Mbps):"
        '
        'lbl_speedtest_ping_load
        '
        Me.lbl_speedtest_ping_load.AutoSize = True
        Me.lbl_speedtest_ping_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_ping_load.Location = New System.Drawing.Point(120, 54)
        Me.lbl_speedtest_ping_load.Name = "lbl_speedtest_ping_load"
        Me.lbl_speedtest_ping_load.Size = New System.Drawing.Size(72, 13)
        Me.lbl_speedtest_ping_load.TabIndex = 20
        Me.lbl_speedtest_ping_load.Text = "speed_ping"
        '
        'lbl_speedtest_dl_load
        '
        Me.lbl_speedtest_dl_load.AutoSize = True
        Me.lbl_speedtest_dl_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_dl_load.Location = New System.Drawing.Point(120, 78)
        Me.lbl_speedtest_dl_load.Name = "lbl_speedtest_dl_load"
        Me.lbl_speedtest_dl_load.Size = New System.Drawing.Size(58, 13)
        Me.lbl_speedtest_dl_load.TabIndex = 19
        Me.lbl_speedtest_dl_load.Text = "speed_dl"
        '
        'lbl_speedtest_server_load
        '
        Me.lbl_speedtest_server_load.AutoSize = True
        Me.lbl_speedtest_server_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_server_load.Location = New System.Drawing.Point(120, 30)
        Me.lbl_speedtest_server_load.Name = "lbl_speedtest_server_load"
        Me.lbl_speedtest_server_load.Size = New System.Drawing.Size(66, 13)
        Me.lbl_speedtest_server_load.TabIndex = 18
        Me.lbl_speedtest_server_load.Text = "server_loc"
        '
        'lbl_speedtest_server
        '
        Me.lbl_speedtest_server.AutoSize = True
        Me.lbl_speedtest_server.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_server.Location = New System.Drawing.Point(17, 30)
        Me.lbl_speedtest_server.Name = "lbl_speedtest_server"
        Me.lbl_speedtest_server.Size = New System.Drawing.Size(48, 13)
        Me.lbl_speedtest_server.TabIndex = 17
        Me.lbl_speedtest_server.Text = "Server:"
        '
        'lbl_speedtest_dl
        '
        Me.lbl_speedtest_dl.AutoSize = True
        Me.lbl_speedtest_dl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_dl.Location = New System.Drawing.Point(17, 78)
        Me.lbl_speedtest_dl.Name = "lbl_speedtest_dl"
        Me.lbl_speedtest_dl.Size = New System.Drawing.Size(102, 13)
        Me.lbl_speedtest_dl.TabIndex = 15
        Me.lbl_speedtest_dl.Text = "Dowload (Mbps):"
        '
        'lbl_speedtest_ping
        '
        Me.lbl_speedtest_ping.AutoSize = True
        Me.lbl_speedtest_ping.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtest_ping.Location = New System.Drawing.Point(17, 54)
        Me.lbl_speedtest_ping.Name = "lbl_speedtest_ping"
        Me.lbl_speedtest_ping.Size = New System.Drawing.Size(63, 13)
        Me.lbl_speedtest_ping.TabIndex = 1
        Me.lbl_speedtest_ping.Text = "Ping (ms):"
        '
        'btn_speedtest_save
        '
        Me.btn_speedtest_save.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_speedtest_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_speedtest_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_speedtest_save.Location = New System.Drawing.Point(500, 146)
        Me.btn_speedtest_save.Name = "btn_speedtest_save"
        Me.btn_speedtest_save.Size = New System.Drawing.Size(68, 21)
        Me.btn_speedtest_save.TabIndex = 102
        Me.btn_speedtest_save.Text = "SAVE"
        Me.btn_speedtest_save.UseVisualStyleBackColor = False
        '
        'btn_speedtest_close
        '
        Me.btn_speedtest_close.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_speedtest_close.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_speedtest_close.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_speedtest_close.Location = New System.Drawing.Point(426, 146)
        Me.btn_speedtest_close.Name = "btn_speedtest_close"
        Me.btn_speedtest_close.Size = New System.Drawing.Size(68, 21)
        Me.btn_speedtest_close.TabIndex = 104
        Me.btn_speedtest_close.Text = "CLOSE"
        Me.btn_speedtest_close.UseVisualStyleBackColor = False
        '
        'btn_speedtest_run
        '
        Me.btn_speedtest_run.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_speedtest_run.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_speedtest_run.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_speedtest_run.Location = New System.Drawing.Point(574, 146)
        Me.btn_speedtest_run.Name = "btn_speedtest_run"
        Me.btn_speedtest_run.Size = New System.Drawing.Size(116, 21)
        Me.btn_speedtest_run.TabIndex = 105
        Me.btn_speedtest_run.Text = "Run Speedtest"
        Me.btn_speedtest_run.UseVisualStyleBackColor = False
        '
        'Speedtest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(713, 178)
        Me.Controls.Add(Me.btn_speedtest_run)
        Me.Controls.Add(Me.btn_speedtest_save)
        Me.Controls.Add(Me.btn_speedtest_close)
        Me.Controls.Add(Me.grpbox_speedtest_results)
        Me.Controls.Add(Me.lbl_speedtest_testing)
        Me.Controls.Add(Me.progbar_speedtest)
        Me.Controls.Add(Me.grpbox_speedtest_info)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Speedtest"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Speedtest"
        Me.grpbox_speedtest_info.ResumeLayout(False)
        Me.grpbox_speedtest_info.PerformLayout()
        Me.grpbox_speedtest_results.ResumeLayout(False)
        Me.grpbox_speedtest_results.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lbl_speedtest_con As Label
    Friend WithEvents lbl_speedtest_file As Label
    Friend WithEvents grpbox_speedtest_info As GroupBox
    Friend WithEvents lbl_speedtest_store As Label
    Friend WithEvents lbl_speedtest_store_load As Label
    Friend WithEvents lbl_speedtest_con_load As Label
    Friend WithEvents lbl_speedtest_file_load As Label
    Friend WithEvents progbar_speedtest As ProgressBar
    Friend WithEvents lbl_speedtest_testing As Label
    Friend WithEvents grpbox_speedtest_results As GroupBox
    Friend WithEvents lbl_speedtest_ping_load As Label
    Friend WithEvents lbl_speedtest_dl_load As Label
    Friend WithEvents lbl_speedtest_server_load As Label
    Friend WithEvents lbl_speedtest_server As Label
    Friend WithEvents lbl_speedtest_dl As Label
    Friend WithEvents lbl_speedtest_ping As Label
    Friend WithEvents lbl_speedtest_up As Label
    Friend WithEvents lbl_speedtest_up_load As Label
    Friend WithEvents btn_speedtest_save As Button
    Friend WithEvents btn_speedtest_close As Button
    Friend WithEvents btn_speedtest_run As Button
    Friend WithEvents lbl_speedtest_time As Label
    Friend WithEvents lbl_speedtest_time_load As Label
End Class
