﻿Imports System.Windows.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class form_about
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_about))
        Me.txt_about = New System.Windows.Forms.TextBox()
        Me.pic_about = New System.Windows.Forms.PictureBox()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.lbl_about_version = New System.Windows.Forms.Label()
        Me.lbl_about_platform = New System.Windows.Forms.Label()
        CType(Me.pic_about, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txt_about
        '
        Me.txt_about.Enabled = False
        Me.txt_about.Location = New System.Drawing.Point(12, 177)
        Me.txt_about.Multiline = True
        Me.txt_about.Name = "txt_about"
        Me.txt_about.ReadOnly = True
        Me.txt_about.Size = New System.Drawing.Size(350, 50)
        Me.txt_about.TabIndex = 0
        Me.txt_about.Text = "@ 2017 - 2018 Groupe Dynamite Inc. All rights reserved." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "This application is for " &
    "internal use only and not to be distributed outside Groupe Dynamite."
        '
        'pic_about
        '
        Me.pic_about.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.pic_about.ErrorImage = CType(resources.GetObject("pic_about.ErrorImage"), System.Drawing.Image)
        Me.pic_about.Image = CType(resources.GetObject("pic_about.Image"), System.Drawing.Image)
        Me.pic_about.InitialImage = Nothing
        Me.pic_about.Location = New System.Drawing.Point(12, 12)
        Me.pic_about.Name = "pic_about"
        Me.pic_about.Size = New System.Drawing.Size(350, 115)
        Me.pic_about.TabIndex = 1
        Me.pic_about.TabStop = False
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'lbl_about_version
        '
        Me.lbl_about_version.AutoSize = True
        Me.lbl_about_version.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_about_version.ForeColor = System.Drawing.Color.SlateGray
        Me.lbl_about_version.Location = New System.Drawing.Point(12, 139)
        Me.lbl_about_version.Name = "lbl_about_version"
        Me.lbl_about_version.Size = New System.Drawing.Size(52, 13)
        Me.lbl_about_version.TabIndex = 2
        Me.lbl_about_version.Text = "version:"
        '
        'lbl_about_platform
        '
        Me.lbl_about_platform.AutoSize = True
        Me.lbl_about_platform.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_about_platform.ForeColor = System.Drawing.Color.SlateGray
        Me.lbl_about_platform.Location = New System.Drawing.Point(12, 155)
        Me.lbl_about_platform.Name = "lbl_about_platform"
        Me.lbl_about_platform.Size = New System.Drawing.Size(56, 13)
        Me.lbl_about_platform.TabIndex = 3
        Me.lbl_about_platform.Text = "platform:"
        '
        'form_about
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(376, 239)
        Me.Controls.Add(Me.lbl_about_platform)
        Me.Controls.Add(Me.lbl_about_version)
        Me.Controls.Add(Me.pic_about)
        Me.Controls.Add(Me.txt_about)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_about"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - About"
        Me.TopMost = True
        CType(Me.pic_about, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txt_about As TextBox
    Friend WithEvents pic_about As PictureBox
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents lbl_about_version As Label
    Friend WithEvents lbl_about_platform As Label
End Class
