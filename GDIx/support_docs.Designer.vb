﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class support_docs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(support_docs))
        Me.lbl_docs_local = New System.Windows.Forms.Label()
        Me.lbl_docs_jira = New System.Windows.Forms.Label()
        Me.lbl_docs_gdrive = New System.Windows.Forms.Label()
        Me.lbl_docs_info = New System.Windows.Forms.Label()
        Me.txtbox_docs_gdrive = New System.Windows.Forms.TextBox()
        Me.txtbox_docs_jira = New System.Windows.Forms.TextBox()
        Me.txtbox_docs_local = New System.Windows.Forms.TextBox()
        Me.btn_docs_local = New System.Windows.Forms.Button()
        Me.btn_docs_cancel = New System.Windows.Forms.Button()
        Me.btn_docs_save = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lbl_docs_local
        '
        Me.lbl_docs_local.AutoSize = True
        Me.lbl_docs_local.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_docs_local.Location = New System.Drawing.Point(35, 90)
        Me.lbl_docs_local.Name = "lbl_docs_local"
        Me.lbl_docs_local.Size = New System.Drawing.Size(46, 15)
        Me.lbl_docs_local.TabIndex = 116
        Me.lbl_docs_local.Text = "Local:"
        '
        'lbl_docs_jira
        '
        Me.lbl_docs_jira.AutoSize = True
        Me.lbl_docs_jira.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_docs_jira.Location = New System.Drawing.Point(35, 65)
        Me.lbl_docs_jira.Name = "lbl_docs_jira"
        Me.lbl_docs_jira.Size = New System.Drawing.Size(35, 15)
        Me.lbl_docs_jira.TabIndex = 117
        Me.lbl_docs_jira.Text = "Jira:"
        '
        'lbl_docs_gdrive
        '
        Me.lbl_docs_gdrive.AutoSize = True
        Me.lbl_docs_gdrive.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_docs_gdrive.Location = New System.Drawing.Point(35, 40)
        Me.lbl_docs_gdrive.Name = "lbl_docs_gdrive"
        Me.lbl_docs_gdrive.Size = New System.Drawing.Size(62, 15)
        Me.lbl_docs_gdrive.TabIndex = 118
        Me.lbl_docs_gdrive.Text = "G! Drive:"
        '
        'lbl_docs_info
        '
        Me.lbl_docs_info.AutoSize = True
        Me.lbl_docs_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_docs_info.Location = New System.Drawing.Point(12, 9)
        Me.lbl_docs_info.Name = "lbl_docs_info"
        Me.lbl_docs_info.Size = New System.Drawing.Size(319, 15)
        Me.lbl_docs_info.TabIndex = 119
        Me.lbl_docs_info.Text = "Please enter your default support docs locations:"
        '
        'txtbox_docs_gdrive
        '
        Me.txtbox_docs_gdrive.Location = New System.Drawing.Point(103, 39)
        Me.txtbox_docs_gdrive.Name = "txtbox_docs_gdrive"
        Me.txtbox_docs_gdrive.Size = New System.Drawing.Size(194, 20)
        Me.txtbox_docs_gdrive.TabIndex = 120
        '
        'txtbox_docs_jira
        '
        Me.txtbox_docs_jira.Location = New System.Drawing.Point(103, 64)
        Me.txtbox_docs_jira.Name = "txtbox_docs_jira"
        Me.txtbox_docs_jira.Size = New System.Drawing.Size(194, 20)
        Me.txtbox_docs_jira.TabIndex = 121
        '
        'txtbox_docs_local
        '
        Me.txtbox_docs_local.Location = New System.Drawing.Point(103, 89)
        Me.txtbox_docs_local.Name = "txtbox_docs_local"
        Me.txtbox_docs_local.Size = New System.Drawing.Size(194, 20)
        Me.txtbox_docs_local.TabIndex = 122
        '
        'btn_docs_local
        '
        Me.btn_docs_local.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_docs_local.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_docs_local.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_docs_local.Location = New System.Drawing.Point(303, 90)
        Me.btn_docs_local.Name = "btn_docs_local"
        Me.btn_docs_local.Size = New System.Drawing.Size(68, 21)
        Me.btn_docs_local.TabIndex = 125
        Me.btn_docs_local.Text = "Browse"
        Me.btn_docs_local.UseVisualStyleBackColor = False
        '
        'btn_docs_cancel
        '
        Me.btn_docs_cancel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_docs_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_docs_cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_docs_cancel.Location = New System.Drawing.Point(229, 117)
        Me.btn_docs_cancel.Name = "btn_docs_cancel"
        Me.btn_docs_cancel.Size = New System.Drawing.Size(68, 21)
        Me.btn_docs_cancel.TabIndex = 126
        Me.btn_docs_cancel.Text = "Cancel"
        Me.btn_docs_cancel.UseVisualStyleBackColor = False
        '
        'btn_docs_save
        '
        Me.btn_docs_save.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_docs_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_docs_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_docs_save.Location = New System.Drawing.Point(303, 117)
        Me.btn_docs_save.Name = "btn_docs_save"
        Me.btn_docs_save.Size = New System.Drawing.Size(68, 21)
        Me.btn_docs_save.TabIndex = 127
        Me.btn_docs_save.Text = "Save"
        Me.btn_docs_save.UseVisualStyleBackColor = False
        '
        'support_docs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(403, 150)
        Me.Controls.Add(Me.btn_docs_save)
        Me.Controls.Add(Me.btn_docs_cancel)
        Me.Controls.Add(Me.btn_docs_local)
        Me.Controls.Add(Me.txtbox_docs_local)
        Me.Controls.Add(Me.txtbox_docs_jira)
        Me.Controls.Add(Me.txtbox_docs_gdrive)
        Me.Controls.Add(Me.lbl_docs_info)
        Me.Controls.Add(Me.lbl_docs_gdrive)
        Me.Controls.Add(Me.lbl_docs_jira)
        Me.Controls.Add(Me.lbl_docs_local)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "support_docs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Support docs"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbl_docs_local As Label
    Friend WithEvents lbl_docs_jira As Label
    Friend WithEvents lbl_docs_gdrive As Label
    Friend WithEvents lbl_docs_info As Label
    Friend WithEvents txtbox_docs_gdrive As TextBox
    Friend WithEvents txtbox_docs_jira As TextBox
    Friend WithEvents txtbox_docs_local As TextBox
    Friend WithEvents btn_docs_local As Button
    Friend WithEvents btn_docs_cancel As Button
    Friend WithEvents btn_docs_save As Button
End Class
