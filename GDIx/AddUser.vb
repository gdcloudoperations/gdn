﻿'AUTHOR: Alex Dumitrascu
'DATE: 04-01-2018
'UPDATE: 15-03-2018
'
'FORM: AddUser.vb (access from admin.vb)


Public Class AddUser

    'LOCAL VARS
    Dim user_status As String = Nothing     'store user status
    Dim errors As String = Nothing          'store error msg

    'BUTTON: CANCEL - close form
    Private Sub btn_adduser_cancel_Click(sender As Object, e As EventArgs) Handles btn_adduser_cancel.Click
        Me.Dispose()
    End Sub

    'ON LOAD: AddUser.vb
    Private Sub AddUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtbox_adduser_usr_load.Text = ""
        combo_adduser_role.SelectedText = ""
        txtbox_adduser_usr_load.Focus()

    End Sub

    'BUTTON: ADD - add user
    Private Sub btn_adduser_add_Click(sender As Object, e As EventArgs) Handles btn_adduser_add.Click

        'Check if username is missing
        If txtbox_adduser_usr_load.Text = "" Then
            errors = errors & "Missing Username!" & vbNewLine
        Else
            errors = errors & ""
        End If

        'check if user-role is missing
        If combo_adduser_role.SelectedItem = "" Then
            errors = errors & "Missing User-role!" & vbNewLine
        Else
            errors = errors & ""
        End If

        'check user status
        If radio_adduser_enabled.Checked Then
            user_status = "Enabled"
            errors = errors & ""
        ElseIf radio_adduser_disabled.Checked Then
            user_status = "Disabled"
            errors = errors & ""
        Else
            user_status = Nothing
            errors = errors & "Missing user status (enabled / disabled)" & vbNewLine
        End If


        If Not errors = "" Then
            MsgBox("Missing information: " & vbNewLine & vbNewLine & errors, MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (not completed)")
            errors = Nothing
        Else

            If gdx_main.user_level = "limited+" And Not combo_adduser_role.SelectedItem = "SAM" And Not combo_adduser_role.SelectedItem = "Guest" Then
                MsgBox("You don't have enough rights to create a user with this role.", MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            ElseIf gdx_main.user_level = "general+" And Not combo_adduser_role.SelectedItem = "SAM" And Not combo_adduser_role.SelectedItem = "Tech Support" And Not combo_adduser_role.SelectedItem = "Guest" Then
                MsgBox("You don't have enough rights to create a user with this role.", MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            ElseIf gdx_main.user_level = "elevated" And combo_adduser_role.SelectedItem = "*SYS" Then
                MsgBox("You don't have enough rights to create a user with this role.", MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            Else

                gdx_main.add_user(txtbox_adduser_usr_load.Text, combo_adduser_role.SelectedItem, user_status)

            End If
        End If

    End Sub

    'Clean form
    Protected Friend Sub clearAddUserForm()

        txtbox_adduser_usr_load.Text = ""
        combo_adduser_role.ResetText()
        combo_adduser_role.SelectedText = ""
        txtbox_adduser_usr_load.Focus()
        radio_adduser_enabled.Checked = False
        radio_adduser_disabled.Checked = False

    End Sub

End Class