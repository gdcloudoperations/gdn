﻿'AUTHOR: Alex Dumitrascu
'DATE: 24-04-2019
'UPDATE: 25-04-2018
'
'FORM: Speedtest_list.vb (access from MAIN)

Imports System.Data.SqlClient
Imports System.IO

Public Class speedtest_list

    'VARS
    'count vars
    Private stores As Integer = 0
    Private speed As Integer = 0
    Private wifi As Integer = 0


    'SPEEDTEST LIST (ON LOAD)
    Private Sub speedtest_list_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        get_speedtest_list()

    End Sub

    Private Sub get_speedtest_list()

        'db vars
        Dim network_connection As SqlConnection
        Dim cmd_net As SqlCommand
        Dim sql_store_network As String = "Select Store, Speed, [WiFi Speed], [Speedtest Date] from stores_network order by store"
        network_connection = New SqlConnection(gdx_main.connectionString)

        Try

            network_connection.Open()
            cmd_net = New SqlCommand(sql_store_network, network_connection)
            Dim networkReader As SqlDataReader = cmd_net.ExecuteReader()

            While networkReader.Read()

                Dim newitem As New ListViewItem()
                newitem.UseItemStyleForSubItems = False
                newitem.Text = networkReader.GetValue(0).ToString           'store number
                stores = stores + 1                                         'counter
                'separate download and upload speeds in 2
                If IsDBNull(networkReader.GetValue(1)) Then
                    newitem.SubItems.Add("n/a")                                 'dl speed
                    newitem.SubItems.Add("n/a")                                 'up speed
                Else
                    Dim split_speed() = networkReader.GetValue(1).ToString.Split(" / ")
                    newitem.SubItems.Add(split_speed(0).ToString)               'dl speed
                    newitem.SubItems.Add(split_speed(2).ToString)               'up speed
                    speed = speed + 1
                End If

                If IsDBNull(networkReader.GetValue(2)) Then
                    newitem.SubItems.Add("n/a")                                 'guest wifi speed
                Else
                    newitem.SubItems.Add(networkReader.GetValue(2).ToString)    'guest wifi speed
                    wifi = wifi + 1
                End If

                If IsDBNull(networkReader.GetValue(3)) Then
                    newitem.SubItems.Add("n/a")                                 'speedtest date
                Else
                    newitem.SubItems.Add(networkReader.GetValue(3).ToString)    'speedtest date
                End If

                listview_speed.Items.Add(newitem).ToString()
            End While

            'close connection
            networkReader.Close()
            network_connection.Close()
            cmd_net.Dispose()

            'autosize columns & header
            listview_speed.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
            listview_speed.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)

            'display results
            lbl_speedtestlist_stores_result.Text = stores.ToString
            lbl_speedtestlist_sp_result.Text = speed.ToString
            lbl_speedtestlist_wifi_result.Text = wifi.ToString


        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: stpre_speeds]")
        End Try

    End Sub

    Private Sub speedtest_closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        listview_speed.Clear()
        Me.Dispose()
    End Sub

    'BUTTON: CLOSE
    Private Sub btn_searchby_close_Click(sender As Object, e As EventArgs) Handles btn_searchby_close.Click
        listview_speed.Clear()
        Me.Dispose()
    End Sub

    'BUTTON: EXPORT
    Private Sub btn_searchby_save_Click(sender As Object, e As EventArgs) Handles btn_searchby_save.Click

        Dim sfd As New SaveFileDialog With {
        .Title = "Select file location",
        .FileName = "GDnReports_SpeedtestList.csv",
        .Filter = "CSV (*.csv)|*.csv",
        .FilterIndex = 0,
        .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}

        'save CSV fIle 

        If sfd.ShowDialog = DialogResult.OK Then

            Dim headers = (From ch In listview_speed.Columns Let header = DirectCast(ch, ColumnHeader) Select header.Text).ToArray()

            Dim items() = (From item In listview_speed.Items Let lvi = DirectCast(item, ListViewItem) Select (From subitem In lvi.SubItems
                                                                                                              Let si = DirectCast(subitem, ListViewItem.ListViewSubItem)
                                                                                                              Select si.Text).ToArray()).ToArray()

            Dim table As String = String.Join(",", headers) & Environment.NewLine

            For Each a In items
                table &= String.Join(",", a) & Environment.NewLine
            Next

            table = table.TrimEnd(CChar(vbCr), CChar(vbLf))
            IO.File.WriteAllText(sfd.FileName, table)

        End If

    End Sub


    'Open selected store (double click item)
    Private Sub listview_speed_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles listview_speed.MouseDoubleClick

        Me.Dispose()
        gdx_main.txtbox_storeno.Text = listview_speed.SelectedItems(0).SubItems(0).Text
        gdx_main.cmbbox_searchby.SelectedItem = "Store"
        gdx_main.btn_search_store.PerformClick()

    End Sub

    'ORDER LIST BY COLUMN USING admin_users_compare CLASS
    Private Sub listview_speed_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles listview_speed.ColumnClick


        Dim sortColumn As Integer = -1

        If e.Column <> sortColumn Then
            ' Set the sort column to the new column.
            sortColumn = e.Column
            ' Set the sort order to ascending by default.
            listview_speed.Sorting = SortOrder.Ascending
        Else
            ' Determine what the last sort order was and change it.
            If listview_speed.Sorting = SortOrder.Ascending Then
                listview_speed.Sorting = SortOrder.Descending
            Else
                listview_speed.Sorting = SortOrder.Ascending
            End If
        End If

        listview_speed.ListViewItemSorter = New ListViewItemCompare(e.Column, listview_speed.Sorting)

        listview_speed.Sort()


    End Sub

End Class