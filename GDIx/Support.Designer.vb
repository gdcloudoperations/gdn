﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class form_support
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_support))
        Me.grpbox_support_info = New System.Windows.Forms.GroupBox()
        Me.txtbox_support_eaddress = New System.Windows.Forms.TextBox()
        Me.txtbox_support_department = New System.Windows.Forms.TextBox()
        Me.txtbox_support_name = New System.Windows.Forms.TextBox()
        Me.lbl_support_department = New System.Windows.Forms.Label()
        Me.lbl_support_eaddress = New System.Windows.Forms.Label()
        Me.lbl_support_name = New System.Windows.Forms.Label()
        Me.grpbox_support_issue = New System.Windows.Forms.GroupBox()
        Me.txtbox_support_issue = New System.Windows.Forms.TextBox()
        Me.btn_support_send = New System.Windows.Forms.Button()
        Me.btn_support_cancel = New System.Windows.Forms.Button()
        Me.grpbox_support_info.SuspendLayout()
        Me.grpbox_support_issue.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpbox_support_info
        '
        Me.grpbox_support_info.Controls.Add(Me.txtbox_support_eaddress)
        Me.grpbox_support_info.Controls.Add(Me.txtbox_support_department)
        Me.grpbox_support_info.Controls.Add(Me.txtbox_support_name)
        Me.grpbox_support_info.Controls.Add(Me.lbl_support_department)
        Me.grpbox_support_info.Controls.Add(Me.lbl_support_eaddress)
        Me.grpbox_support_info.Controls.Add(Me.lbl_support_name)
        Me.grpbox_support_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_support_info.Location = New System.Drawing.Point(12, 12)
        Me.grpbox_support_info.Name = "grpbox_support_info"
        Me.grpbox_support_info.Size = New System.Drawing.Size(288, 108)
        Me.grpbox_support_info.TabIndex = 0
        Me.grpbox_support_info.TabStop = False
        Me.grpbox_support_info.Text = "Your Information"
        '
        'txtbox_support_eaddress
        '
        Me.txtbox_support_eaddress.Location = New System.Drawing.Point(124, 73)
        Me.txtbox_support_eaddress.Name = "txtbox_support_eaddress"
        Me.txtbox_support_eaddress.Size = New System.Drawing.Size(158, 21)
        Me.txtbox_support_eaddress.TabIndex = 5
        '
        'txtbox_support_department
        '
        Me.txtbox_support_department.Location = New System.Drawing.Point(124, 45)
        Me.txtbox_support_department.Name = "txtbox_support_department"
        Me.txtbox_support_department.Size = New System.Drawing.Size(158, 21)
        Me.txtbox_support_department.TabIndex = 4
        '
        'txtbox_support_name
        '
        Me.txtbox_support_name.Location = New System.Drawing.Point(124, 21)
        Me.txtbox_support_name.Name = "txtbox_support_name"
        Me.txtbox_support_name.Size = New System.Drawing.Size(158, 21)
        Me.txtbox_support_name.TabIndex = 3
        '
        'lbl_support_department
        '
        Me.lbl_support_department.AutoSize = True
        Me.lbl_support_department.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_support_department.Location = New System.Drawing.Point(24, 50)
        Me.lbl_support_department.Name = "lbl_support_department"
        Me.lbl_support_department.Size = New System.Drawing.Size(76, 13)
        Me.lbl_support_department.TabIndex = 2
        Me.lbl_support_department.Text = "Department:"
        '
        'lbl_support_eaddress
        '
        Me.lbl_support_eaddress.AutoSize = True
        Me.lbl_support_eaddress.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_support_eaddress.Location = New System.Drawing.Point(24, 76)
        Me.lbl_support_eaddress.Name = "lbl_support_eaddress"
        Me.lbl_support_eaddress.Size = New System.Drawing.Size(90, 13)
        Me.lbl_support_eaddress.TabIndex = 1
        Me.lbl_support_eaddress.Text = "Email Address:"
        '
        'lbl_support_name
        '
        Me.lbl_support_name.AutoSize = True
        Me.lbl_support_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_support_name.Location = New System.Drawing.Point(24, 26)
        Me.lbl_support_name.Name = "lbl_support_name"
        Me.lbl_support_name.Size = New System.Drawing.Size(43, 13)
        Me.lbl_support_name.TabIndex = 0
        Me.lbl_support_name.Text = "Name:"
        '
        'grpbox_support_issue
        '
        Me.grpbox_support_issue.Controls.Add(Me.txtbox_support_issue)
        Me.grpbox_support_issue.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_support_issue.Location = New System.Drawing.Point(12, 126)
        Me.grpbox_support_issue.Name = "grpbox_support_issue"
        Me.grpbox_support_issue.Size = New System.Drawing.Size(288, 97)
        Me.grpbox_support_issue.TabIndex = 6
        Me.grpbox_support_issue.TabStop = False
        Me.grpbox_support_issue.Text = "Additional Information"
        '
        'txtbox_support_issue
        '
        Me.txtbox_support_issue.Location = New System.Drawing.Point(27, 20)
        Me.txtbox_support_issue.Multiline = True
        Me.txtbox_support_issue.Name = "txtbox_support_issue"
        Me.txtbox_support_issue.Size = New System.Drawing.Size(255, 69)
        Me.txtbox_support_issue.TabIndex = 1
        '
        'btn_support_send
        '
        Me.btn_support_send.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_support_send.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_support_send.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_support_send.Location = New System.Drawing.Point(221, 227)
        Me.btn_support_send.Name = "btn_support_send"
        Me.btn_support_send.Size = New System.Drawing.Size(73, 21)
        Me.btn_support_send.TabIndex = 10
        Me.btn_support_send.Text = "Send"
        Me.btn_support_send.UseVisualStyleBackColor = False
        '
        'btn_support_cancel
        '
        Me.btn_support_cancel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_support_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_support_cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_support_cancel.Location = New System.Drawing.Point(142, 227)
        Me.btn_support_cancel.Name = "btn_support_cancel"
        Me.btn_support_cancel.Size = New System.Drawing.Size(73, 21)
        Me.btn_support_cancel.TabIndex = 11
        Me.btn_support_cancel.Text = "Cancel"
        Me.btn_support_cancel.UseVisualStyleBackColor = False
        '
        'form_support
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(314, 255)
        Me.Controls.Add(Me.btn_support_cancel)
        Me.Controls.Add(Me.btn_support_send)
        Me.Controls.Add(Me.grpbox_support_issue)
        Me.Controls.Add(Me.grpbox_support_info)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_support"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Support"
        Me.grpbox_support_info.ResumeLayout(False)
        Me.grpbox_support_info.PerformLayout()
        Me.grpbox_support_issue.ResumeLayout(False)
        Me.grpbox_support_issue.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents grpbox_support_info As GroupBox
    Friend WithEvents txtbox_support_eaddress As TextBox
    Friend WithEvents txtbox_support_department As TextBox
    Friend WithEvents txtbox_support_name As TextBox
    Friend WithEvents lbl_support_department As Label
    Friend WithEvents lbl_support_eaddress As Label
    Friend WithEvents lbl_support_name As Label
    Friend WithEvents grpbox_support_issue As GroupBox
    Friend WithEvents txtbox_support_issue As TextBox
    Friend WithEvents btn_support_send As Button
    Friend WithEvents btn_support_cancel As Button
End Class
