﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Labs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Labs))
        Me.top_menu = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LABSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PRODToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReloadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StoreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteStoreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OnMapToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupportDocsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GDriveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JiraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LocalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WebToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GarageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DynamiteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DynamiteCAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DynamiteUSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GRPDYNToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoteDesktopToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TeamViewerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AutoUpdateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OffToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BrowserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChromeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RDPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TeamViewerToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshRateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.topmenu_refresh_0sec = New System.Windows.Forms.ToolStripMenuItem()
        Me.topmenu_refresh_10sec = New System.Windows.Forms.ToolStripMenuItem()
        Me.topmenu_refresh_30sec = New System.Windows.Forms.ToolStripMenuItem()
        Me.topmenu_refresh_60sec = New System.Windows.Forms.ToolStripMenuItem()
        Me.SavePositionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupportLocationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpF1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.UpdatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KeyboardShortcutsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lbl_labs_refresh_load = New System.Windows.Forms.Label()
        Me.lbl_labs_vpn_con_load = New System.Windows.Forms.Label()
        Me.lbl_labs_user_load = New System.Windows.Forms.Label()
        Me.lbl_labs_role_load = New System.Windows.Forms.Label()
        Me.lbl_labs_reload = New System.Windows.Forms.Label()
        Me.lbl_labs_vpn_status = New System.Windows.Forms.Label()
        Me.lbl_labs_user = New System.Windows.Forms.Label()
        Me.lbl_labs_role = New System.Windows.Forms.Label()
        Me.grp_labs_store = New System.Windows.Forms.GroupBox()
        Me.lbl_labs_status_load = New System.Windows.Forms.Label()
        Me.lbl_labs_status = New System.Windows.Forms.Label()
        Me.lbl_labs_str_system_load = New System.Windows.Forms.Label()
        Me.lbl_labs_str_system = New System.Windows.Forms.Label()
        Me.lbl_labs_store_iscombo_load = New System.Windows.Forms.Label()
        Me.lbl_labs_store_iscombo = New System.Windows.Forms.Label()
        Me.lbl_labs_country_load = New System.Windows.Forms.Label()
        Me.lbl_labs_state_load = New System.Windows.Forms.Label()
        Me.lbl_labs_city_load = New System.Windows.Forms.Label()
        Me.lbl_labs_address_load = New System.Windows.Forms.Label()
        Me.lbl_labs_sis_store_load = New System.Windows.Forms.Label()
        Me.lbl_labs_ban_load = New System.Windows.Forms.Label()
        Me.lbl_labs_strno_load = New System.Windows.Forms.Label()
        Me.lbl_labs_str_cntry = New System.Windows.Forms.Label()
        Me.lbl_labs_str_prov = New System.Windows.Forms.Label()
        Me.lbl_labs_str_city = New System.Windows.Forms.Label()
        Me.lbl_labs_str_add = New System.Windows.Forms.Label()
        Me.lbl_labs_str_sisstr = New System.Windows.Forms.Label()
        Me.lbl_labs_str_banner = New System.Windows.Forms.Label()
        Me.lbl_labs_str_no = New System.Windows.Forms.Label()
        Me.grp_labs_store_list = New System.Windows.Forms.GroupBox()
        Me.lbl_labs_torebuild_load = New System.Windows.Forms.Label()
        Me.lbl_labs_torebuild = New System.Windows.Forms.Label()
        Me.lbl_labs_reserved_load = New System.Windows.Forms.Label()
        Me.lbl_labs_reserved = New System.Windows.Forms.Label()
        Me.lbl_labs_abailable_load = New System.Windows.Forms.Label()
        Me.lbl_labs_total = New System.Windows.Forms.Label()
        Me.btn_labs_ping = New System.Windows.Forms.Button()
        Me.btn_labs_export = New System.Windows.Forms.Button()
        Me.btn_labs_reload = New System.Windows.Forms.Button()
        Me.listview_labs_stores = New System.Windows.Forms.ListView()
        Me.col_store = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_environment = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_system = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_banner = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_country = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_language = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_bo_location = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_project = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_pm = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_status = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_bo_date = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.grp_labs_cashes = New System.Windows.Forms.GroupBox()
        Me.pic_labs_printer_c2 = New System.Windows.Forms.PictureBox()
        Me.pic_labs_printer_c1 = New System.Windows.Forms.PictureBox()
        Me.pic_labs_pinpad_c2 = New System.Windows.Forms.PictureBox()
        Me.pic_labs_pinpad_c1 = New System.Windows.Forms.PictureBox()
        Me.lbl_labs_bo_load = New System.Windows.Forms.Label()
        Me.lbl_labs_cash2_load = New System.Windows.Forms.Label()
        Me.lbl_labs_cash1_load = New System.Windows.Forms.Label()
        Me.btn_labs_tv_bo = New System.Windows.Forms.Button()
        Me.btn_labs_tv_c2 = New System.Windows.Forms.Button()
        Me.btn_labs_tv_c1 = New System.Windows.Forms.Button()
        Me.lbl_labs_bo = New System.Windows.Forms.Label()
        Me.lbl_labs_cash2 = New System.Windows.Forms.Label()
        Me.lbl_labs_cash1 = New System.Windows.Forms.Label()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.grpbox_labs_patches = New System.Windows.Forms.GroupBox()
        Me.ListView_labs_patch = New System.Windows.Forms.ListView()
        Me.col_patchid = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.grpbox_labs_comments = New System.Windows.Forms.GroupBox()
        Me.lbl_labs_save_comments = New System.Windows.Forms.Label()
        Me.btn_labs_clear_comments = New System.Windows.Forms.Button()
        Me.btn_labs_save = New System.Windows.Forms.Button()
        Me.txtbox_labs_comments = New System.Windows.Forms.RichTextBox()
        Me.pic_labs = New System.Windows.Forms.PictureBox()
        Me.btn_labs_refresh_store = New System.Windows.Forms.Button()
        Me.btn_labs_clear = New System.Windows.Forms.Button()
        Me.ping_timer_labs = New System.Windows.Forms.Timer(Me.components)
        Me.top_menu.SuspendLayout()
        Me.grp_labs_store.SuspendLayout()
        Me.grp_labs_store_list.SuspendLayout()
        Me.grp_labs_cashes.SuspendLayout()
        CType(Me.pic_labs_printer_c2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_labs_printer_c1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_labs_pinpad_c2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_labs_pinpad_c1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpbox_labs_patches.SuspendLayout()
        Me.grpbox_labs_comments.SuspendLayout()
        CType(Me.pic_labs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'top_menu
        '
        Me.top_menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ViewToolStripMenuItem, Me.ToolsToolStripMenuItem, Me.OptionsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.top_menu.Location = New System.Drawing.Point(0, 0)
        Me.top_menu.Name = "top_menu"
        Me.top_menu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.top_menu.Size = New System.Drawing.Size(813, 24)
        Me.top_menu.TabIndex = 1
        Me.top_menu.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModeToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.ReloadToolStripMenuItem, Me.StoreToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ModeToolStripMenuItem
        '
        Me.ModeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdminToolStripMenuItem, Me.LABSToolStripMenuItem, Me.PRODToolStripMenuItem})
        Me.ModeToolStripMenuItem.Name = "ModeToolStripMenuItem"
        Me.ModeToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.ModeToolStripMenuItem.Text = "Mode"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.AdminToolStripMenuItem.Text = "Admin"
        '
        'LABSToolStripMenuItem
        '
        Me.LABSToolStripMenuItem.Name = "LABSToolStripMenuItem"
        Me.LABSToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.LABSToolStripMenuItem.Text = "LABS   F7"
        '
        'PRODToolStripMenuItem
        '
        Me.PRODToolStripMenuItem.Name = "PRODToolStripMenuItem"
        Me.PRODToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.PRODToolStripMenuItem.Text = "PROD   F8"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh   F5"
        '
        'ReloadToolStripMenuItem
        '
        Me.ReloadToolStripMenuItem.Name = "ReloadToolStripMenuItem"
        Me.ReloadToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.ReloadToolStripMenuItem.Text = "Reload"
        '
        'StoreToolStripMenuItem
        '
        Me.StoreToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditToolStripMenuItem, Me.DeleteStoreToolStripMenuItem, Me.AddToolStripMenuItem})
        Me.StoreToolStripMenuItem.Name = "StoreToolStripMenuItem"
        Me.StoreToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.StoreToolStripMenuItem.Text = "Labs"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.EditToolStripMenuItem.Text = "Edit Lab"
        '
        'DeleteStoreToolStripMenuItem
        '
        Me.DeleteStoreToolStripMenuItem.Name = "DeleteStoreToolStripMenuItem"
        Me.DeleteStoreToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.DeleteStoreToolStripMenuItem.Text = "Delete Lab"
        '
        'AddToolStripMenuItem
        '
        Me.AddToolStripMenuItem.Name = "AddToolStripMenuItem"
        Me.AddToolStripMenuItem.Size = New System.Drawing.Size(129, 22)
        Me.AddToolStripMenuItem.Text = "New Lab"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.ExitToolStripMenuItem.Text = "Exit   F12"
        '
        'ViewToolStripMenuItem
        '
        Me.ViewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OnMapToolStripMenuItem, Me.SupportDocsToolStripMenuItem, Me.WebToolStripMenuItem})
        Me.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem"
        Me.ViewToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.ViewToolStripMenuItem.Text = "View"
        '
        'OnMapToolStripMenuItem
        '
        Me.OnMapToolStripMenuItem.Name = "OnMapToolStripMenuItem"
        Me.OnMapToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.OnMapToolStripMenuItem.Text = "onMap"
        '
        'SupportDocsToolStripMenuItem
        '
        Me.SupportDocsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GDriveToolStripMenuItem, Me.JiraToolStripMenuItem, Me.LocalToolStripMenuItem})
        Me.SupportDocsToolStripMenuItem.Name = "SupportDocsToolStripMenuItem"
        Me.SupportDocsToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.SupportDocsToolStripMenuItem.Text = "Support Docs"
        '
        'GDriveToolStripMenuItem
        '
        Me.GDriveToolStripMenuItem.Name = "GDriveToolStripMenuItem"
        Me.GDriveToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
        Me.GDriveToolStripMenuItem.Text = "G! drive"
        '
        'JiraToolStripMenuItem
        '
        Me.JiraToolStripMenuItem.Name = "JiraToolStripMenuItem"
        Me.JiraToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
        Me.JiraToolStripMenuItem.Text = "Jira"
        '
        'LocalToolStripMenuItem
        '
        Me.LocalToolStripMenuItem.Name = "LocalToolStripMenuItem"
        Me.LocalToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
        Me.LocalToolStripMenuItem.Text = "Local"
        '
        'WebToolStripMenuItem
        '
        Me.WebToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GarageToolStripMenuItem, Me.DynamiteToolStripMenuItem, Me.DynamiteCAToolStripMenuItem, Me.DynamiteUSToolStripMenuItem, Me.GRPDYNToolStripMenuItem})
        Me.WebToolStripMenuItem.Name = "WebToolStripMenuItem"
        Me.WebToolStripMenuItem.Size = New System.Drawing.Size(145, 22)
        Me.WebToolStripMenuItem.Text = "Web"
        '
        'GarageToolStripMenuItem
        '
        Me.GarageToolStripMenuItem.Name = "GarageToolStripMenuItem"
        Me.GarageToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.GarageToolStripMenuItem.Text = "Garage CA"
        '
        'DynamiteToolStripMenuItem
        '
        Me.DynamiteToolStripMenuItem.Name = "DynamiteToolStripMenuItem"
        Me.DynamiteToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.DynamiteToolStripMenuItem.Text = "Garage US"
        '
        'DynamiteCAToolStripMenuItem
        '
        Me.DynamiteCAToolStripMenuItem.Name = "DynamiteCAToolStripMenuItem"
        Me.DynamiteCAToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.DynamiteCAToolStripMenuItem.Text = "Dynamite CA"
        '
        'DynamiteUSToolStripMenuItem
        '
        Me.DynamiteUSToolStripMenuItem.Name = "DynamiteUSToolStripMenuItem"
        Me.DynamiteUSToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.DynamiteUSToolStripMenuItem.Text = "Dynamite US"
        '
        'GRPDYNToolStripMenuItem
        '
        Me.GRPDYNToolStripMenuItem.Name = "GRPDYNToolStripMenuItem"
        Me.GRPDYNToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.GRPDYNToolStripMenuItem.Text = "GRP DYN"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RemoteDesktopToolStripMenuItem, Me.TeamViewerToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(47, 20)
        Me.ToolsToolStripMenuItem.Text = "Tools"
        '
        'RemoteDesktopToolStripMenuItem
        '
        Me.RemoteDesktopToolStripMenuItem.Name = "RemoteDesktopToolStripMenuItem"
        Me.RemoteDesktopToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.RemoteDesktopToolStripMenuItem.Text = "Remote Desktop"
        '
        'TeamViewerToolStripMenuItem
        '
        Me.TeamViewerToolStripMenuItem.Name = "TeamViewerToolStripMenuItem"
        Me.TeamViewerToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.TeamViewerToolStripMenuItem.Text = "Team Viewer"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AutoUpdateToolStripMenuItem, Me.BrowserToolStripMenuItem, Me.RemoteToolStripMenuItem, Me.RefreshRateToolStripMenuItem, Me.SavePositionToolStripMenuItem, Me.SupportLocationsToolStripMenuItem})
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.OptionsToolStripMenuItem.Text = "Options"
        '
        'AutoUpdateToolStripMenuItem
        '
        Me.AutoUpdateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OnToolStripMenuItem, Me.OffToolStripMenuItem})
        Me.AutoUpdateToolStripMenuItem.Name = "AutoUpdateToolStripMenuItem"
        Me.AutoUpdateToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.AutoUpdateToolStripMenuItem.Text = "Auto update"
        '
        'OnToolStripMenuItem
        '
        Me.OnToolStripMenuItem.Name = "OnToolStripMenuItem"
        Me.OnToolStripMenuItem.Size = New System.Drawing.Size(91, 22)
        Me.OnToolStripMenuItem.Text = "On"
        '
        'OffToolStripMenuItem
        '
        Me.OffToolStripMenuItem.Name = "OffToolStripMenuItem"
        Me.OffToolStripMenuItem.Size = New System.Drawing.Size(91, 22)
        Me.OffToolStripMenuItem.Text = "Off"
        '
        'BrowserToolStripMenuItem
        '
        Me.BrowserToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChromeToolStripMenuItem, Me.IEToolStripMenuItem})
        Me.BrowserToolStripMenuItem.Name = "BrowserToolStripMenuItem"
        Me.BrowserToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.BrowserToolStripMenuItem.Text = "Browser"
        '
        'ChromeToolStripMenuItem
        '
        Me.ChromeToolStripMenuItem.Name = "ChromeToolStripMenuItem"
        Me.ChromeToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.ChromeToolStripMenuItem.Text = "Chrome"
        '
        'IEToolStripMenuItem
        '
        Me.IEToolStripMenuItem.Name = "IEToolStripMenuItem"
        Me.IEToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.IEToolStripMenuItem.Text = "IExplorer"
        '
        'RemoteToolStripMenuItem
        '
        Me.RemoteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RDPToolStripMenuItem, Me.TeamViewerToolStripMenuItem1})
        Me.RemoteToolStripMenuItem.Name = "RemoteToolStripMenuItem"
        Me.RemoteToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.RemoteToolStripMenuItem.Text = "Remote"
        '
        'RDPToolStripMenuItem
        '
        Me.RDPToolStripMenuItem.Name = "RDPToolStripMenuItem"
        Me.RDPToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RDPToolStripMenuItem.Text = "RDP"
        '
        'TeamViewerToolStripMenuItem1
        '
        Me.TeamViewerToolStripMenuItem1.Name = "TeamViewerToolStripMenuItem1"
        Me.TeamViewerToolStripMenuItem1.Size = New System.Drawing.Size(138, 22)
        Me.TeamViewerToolStripMenuItem1.Text = "TeamViewer"
        '
        'RefreshRateToolStripMenuItem
        '
        Me.RefreshRateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.topmenu_refresh_0sec, Me.topmenu_refresh_10sec, Me.topmenu_refresh_30sec, Me.topmenu_refresh_60sec})
        Me.RefreshRateToolStripMenuItem.Name = "RefreshRateToolStripMenuItem"
        Me.RefreshRateToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.RefreshRateToolStripMenuItem.Text = "Refresh rate"
        '
        'topmenu_refresh_0sec
        '
        Me.topmenu_refresh_0sec.Name = "topmenu_refresh_0sec"
        Me.topmenu_refresh_0sec.Size = New System.Drawing.Size(167, 22)
        Me.topmenu_refresh_0sec.Text = "0sec - no updates"
        '
        'topmenu_refresh_10sec
        '
        Me.topmenu_refresh_10sec.Name = "topmenu_refresh_10sec"
        Me.topmenu_refresh_10sec.Size = New System.Drawing.Size(167, 22)
        Me.topmenu_refresh_10sec.Text = "10sec"
        '
        'topmenu_refresh_30sec
        '
        Me.topmenu_refresh_30sec.Name = "topmenu_refresh_30sec"
        Me.topmenu_refresh_30sec.Size = New System.Drawing.Size(167, 22)
        Me.topmenu_refresh_30sec.Text = "30sec"
        '
        'topmenu_refresh_60sec
        '
        Me.topmenu_refresh_60sec.Name = "topmenu_refresh_60sec"
        Me.topmenu_refresh_60sec.Size = New System.Drawing.Size(167, 22)
        Me.topmenu_refresh_60sec.Text = "60sec"
        '
        'SavePositionToolStripMenuItem
        '
        Me.SavePositionToolStripMenuItem.Name = "SavePositionToolStripMenuItem"
        Me.SavePositionToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.SavePositionToolStripMenuItem.Text = "Save Position   F10"
        '
        'SupportLocationsToolStripMenuItem
        '
        Me.SupportLocationsToolStripMenuItem.Name = "SupportLocationsToolStripMenuItem"
        Me.SupportLocationsToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.SupportLocationsToolStripMenuItem.Text = "Support locations"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpF1ToolStripMenuItem, Me.ToolStripSeparator1, Me.UpdatesToolStripMenuItem, Me.KeyboardShortcutsToolStripMenuItem, Me.SupportToolStripMenuItem, Me.ToolStripSeparator2, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'HelpF1ToolStripMenuItem
        '
        Me.HelpF1ToolStripMenuItem.Name = "HelpF1ToolStripMenuItem"
        Me.HelpF1ToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.HelpF1ToolStripMenuItem.Text = "Help   F1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(195, 6)
        '
        'UpdatesToolStripMenuItem
        '
        Me.UpdatesToolStripMenuItem.Name = "UpdatesToolStripMenuItem"
        Me.UpdatesToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.UpdatesToolStripMenuItem.Text = "Check for updates   F11"
        '
        'KeyboardShortcutsToolStripMenuItem
        '
        Me.KeyboardShortcutsToolStripMenuItem.Name = "KeyboardShortcutsToolStripMenuItem"
        Me.KeyboardShortcutsToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.KeyboardShortcutsToolStripMenuItem.Text = "Keyboard Shortcuts   F2"
        '
        'SupportToolStripMenuItem
        '
        Me.SupportToolStripMenuItem.Name = "SupportToolStripMenuItem"
        Me.SupportToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.SupportToolStripMenuItem.Text = "Report errors"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(195, 6)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.AboutToolStripMenuItem.Text = "About GDnetworks"
        '
        'lbl_labs_refresh_load
        '
        Me.lbl_labs_refresh_load.AutoSize = True
        Me.lbl_labs_refresh_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_refresh_load.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbl_labs_refresh_load.Location = New System.Drawing.Point(402, 2)
        Me.lbl_labs_refresh_load.Name = "lbl_labs_refresh_load"
        Me.lbl_labs_refresh_load.Size = New System.Drawing.Size(73, 13)
        Me.lbl_labs_refresh_load.TabIndex = 31
        Me.lbl_labs_refresh_load.Text = "last_refresh"
        '
        'lbl_labs_vpn_con_load
        '
        Me.lbl_labs_vpn_con_load.AutoSize = True
        Me.lbl_labs_vpn_con_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_vpn_con_load.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbl_labs_vpn_con_load.Location = New System.Drawing.Point(402, 19)
        Me.lbl_labs_vpn_con_load.Name = "lbl_labs_vpn_con_load"
        Me.lbl_labs_vpn_con_load.Size = New System.Drawing.Size(98, 13)
        Me.lbl_labs_vpn_con_load.TabIndex = 32
        Me.lbl_labs_vpn_con_load.Text = "vpn_connection"
        '
        'lbl_labs_user_load
        '
        Me.lbl_labs_user_load.AutoSize = True
        Me.lbl_labs_user_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_user_load.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbl_labs_user_load.Location = New System.Drawing.Point(588, 2)
        Me.lbl_labs_user_load.Name = "lbl_labs_user_load"
        Me.lbl_labs_user_load.Size = New System.Drawing.Size(95, 13)
        Me.lbl_labs_user_load.TabIndex = 33
        Me.lbl_labs_user_load.Text = "local_username"
        '
        'lbl_labs_role_load
        '
        Me.lbl_labs_role_load.AutoSize = True
        Me.lbl_labs_role_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_role_load.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbl_labs_role_load.Location = New System.Drawing.Point(588, 19)
        Me.lbl_labs_role_load.Name = "lbl_labs_role_load"
        Me.lbl_labs_role_load.Size = New System.Drawing.Size(59, 13)
        Me.lbl_labs_role_load.TabIndex = 34
        Me.lbl_labs_role_load.Text = "user_role"
        '
        'lbl_labs_reload
        '
        Me.lbl_labs_reload.AutoSize = True
        Me.lbl_labs_reload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_reload.Location = New System.Drawing.Point(326, 2)
        Me.lbl_labs_reload.Name = "lbl_labs_reload"
        Me.lbl_labs_reload.Size = New System.Drawing.Size(79, 13)
        Me.lbl_labs_reload.TabIndex = 35
        Me.lbl_labs_reload.Text = "Last Reload:"
        '
        'lbl_labs_vpn_status
        '
        Me.lbl_labs_vpn_status.AutoSize = True
        Me.lbl_labs_vpn_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_vpn_status.Location = New System.Drawing.Point(326, 19)
        Me.lbl_labs_vpn_status.Name = "lbl_labs_vpn_status"
        Me.lbl_labs_vpn_status.Size = New System.Drawing.Size(76, 13)
        Me.lbl_labs_vpn_status.TabIndex = 36
        Me.lbl_labs_vpn_status.Text = "VPN Status:"
        '
        'lbl_labs_user
        '
        Me.lbl_labs_user.AutoSize = True
        Me.lbl_labs_user.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_user.Location = New System.Drawing.Point(554, 2)
        Me.lbl_labs_user.Name = "lbl_labs_user"
        Me.lbl_labs_user.Size = New System.Drawing.Size(37, 13)
        Me.lbl_labs_user.TabIndex = 37
        Me.lbl_labs_user.Text = "User:"
        '
        'lbl_labs_role
        '
        Me.lbl_labs_role.AutoSize = True
        Me.lbl_labs_role.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_role.Location = New System.Drawing.Point(554, 19)
        Me.lbl_labs_role.Name = "lbl_labs_role"
        Me.lbl_labs_role.Size = New System.Drawing.Size(37, 13)
        Me.lbl_labs_role.TabIndex = 38
        Me.lbl_labs_role.Text = "Role:"
        '
        'grp_labs_store
        '
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_status_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_status)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_str_system_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_str_system)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_store_iscombo_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_store_iscombo)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_country_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_state_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_city_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_address_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_sis_store_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_ban_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_strno_load)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_str_cntry)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_str_prov)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_str_city)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_str_add)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_str_sisstr)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_str_banner)
        Me.grp_labs_store.Controls.Add(Me.lbl_labs_str_no)
        Me.grp_labs_store.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_labs_store.Location = New System.Drawing.Point(12, 230)
        Me.grp_labs_store.Name = "grp_labs_store"
        Me.grp_labs_store.Size = New System.Drawing.Size(387, 251)
        Me.grp_labs_store.TabIndex = 39
        Me.grp_labs_store.TabStop = False
        Me.grp_labs_store.Text = "Store Details"
        '
        'lbl_labs_status_load
        '
        Me.lbl_labs_status_load.AutoSize = True
        Me.lbl_labs_status_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_status_load.Location = New System.Drawing.Point(139, 43)
        Me.lbl_labs_status_load.Name = "lbl_labs_status_load"
        Me.lbl_labs_status_load.Size = New System.Drawing.Size(65, 13)
        Me.lbl_labs_status_load.TabIndex = 28
        Me.lbl_labs_status_load.Text = "lab_status"
        '
        'lbl_labs_status
        '
        Me.lbl_labs_status.AutoSize = True
        Me.lbl_labs_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_status.Location = New System.Drawing.Point(23, 43)
        Me.lbl_labs_status.Name = "lbl_labs_status"
        Me.lbl_labs_status.Size = New System.Drawing.Size(74, 13)
        Me.lbl_labs_status.TabIndex = 27
        Me.lbl_labs_status.Text = "LAB Status:"
        '
        'lbl_labs_str_system_load
        '
        Me.lbl_labs_str_system_load.AutoSize = True
        Me.lbl_labs_str_system_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_str_system_load.Location = New System.Drawing.Point(139, 88)
        Me.lbl_labs_str_system_load.Name = "lbl_labs_str_system_load"
        Me.lbl_labs_str_system_load.Size = New System.Drawing.Size(97, 13)
        Me.lbl_labs_str_system_load.TabIndex = 26
        Me.lbl_labs_str_system_load.Text = "str_system_load"
        '
        'lbl_labs_str_system
        '
        Me.lbl_labs_str_system.AutoSize = True
        Me.lbl_labs_str_system.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_str_system.Location = New System.Drawing.Point(23, 88)
        Me.lbl_labs_str_system.Name = "lbl_labs_str_system"
        Me.lbl_labs_str_system.Size = New System.Drawing.Size(79, 13)
        Me.lbl_labs_str_system.TabIndex = 25
        Me.lbl_labs_str_system.Text = "storeSystem:"
        '
        'lbl_labs_store_iscombo_load
        '
        Me.lbl_labs_store_iscombo_load.AutoSize = True
        Me.lbl_labs_store_iscombo_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_store_iscombo_load.Location = New System.Drawing.Point(139, 112)
        Me.lbl_labs_store_iscombo_load.Name = "lbl_labs_store_iscombo_load"
        Me.lbl_labs_store_iscombo_load.Size = New System.Drawing.Size(76, 13)
        Me.lbl_labs_store_iscombo_load.TabIndex = 24
        Me.lbl_labs_store_iscombo_load.Text = "store_isCombo"
        '
        'lbl_labs_store_iscombo
        '
        Me.lbl_labs_store_iscombo.AutoSize = True
        Me.lbl_labs_store_iscombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_store_iscombo.Location = New System.Drawing.Point(23, 112)
        Me.lbl_labs_store_iscombo.Name = "lbl_labs_store_iscombo"
        Me.lbl_labs_store_iscombo.Size = New System.Drawing.Size(58, 13)
        Me.lbl_labs_store_iscombo.TabIndex = 23
        Me.lbl_labs_store_iscombo.Text = "isCombo:"
        '
        'lbl_labs_country_load
        '
        Me.lbl_labs_country_load.AutoSize = True
        Me.lbl_labs_country_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_country_load.Location = New System.Drawing.Point(139, 232)
        Me.lbl_labs_country_load.Name = "lbl_labs_country_load"
        Me.lbl_labs_country_load.Size = New System.Drawing.Size(71, 13)
        Me.lbl_labs_country_load.TabIndex = 20
        Me.lbl_labs_country_load.Text = "store_country"
        '
        'lbl_labs_state_load
        '
        Me.lbl_labs_state_load.AutoSize = True
        Me.lbl_labs_state_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_state_load.Location = New System.Drawing.Point(139, 209)
        Me.lbl_labs_state_load.Name = "lbl_labs_state_load"
        Me.lbl_labs_state_load.Size = New System.Drawing.Size(59, 13)
        Me.lbl_labs_state_load.TabIndex = 18
        Me.lbl_labs_state_load.Text = "store_state"
        '
        'lbl_labs_city_load
        '
        Me.lbl_labs_city_load.AutoSize = True
        Me.lbl_labs_city_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_city_load.Location = New System.Drawing.Point(139, 185)
        Me.lbl_labs_city_load.Name = "lbl_labs_city_load"
        Me.lbl_labs_city_load.Size = New System.Drawing.Size(52, 13)
        Me.lbl_labs_city_load.TabIndex = 17
        Me.lbl_labs_city_load.Text = "store_city"
        '
        'lbl_labs_address_load
        '
        Me.lbl_labs_address_load.AutoSize = True
        Me.lbl_labs_address_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_address_load.Location = New System.Drawing.Point(139, 160)
        Me.lbl_labs_address_load.Name = "lbl_labs_address_load"
        Me.lbl_labs_address_load.Size = New System.Drawing.Size(73, 13)
        Me.lbl_labs_address_load.TabIndex = 16
        Me.lbl_labs_address_load.Text = "store_address"
        '
        'lbl_labs_sis_store_load
        '
        Me.lbl_labs_sis_store_load.AutoSize = True
        Me.lbl_labs_sis_store_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_sis_store_load.Location = New System.Drawing.Point(139, 136)
        Me.lbl_labs_sis_store_load.Name = "lbl_labs_sis_store_load"
        Me.lbl_labs_sis_store_load.Size = New System.Drawing.Size(77, 13)
        Me.lbl_labs_sis_store_load.TabIndex = 14
        Me.lbl_labs_sis_store_load.Text = "store_sis_store"
        '
        'lbl_labs_ban_load
        '
        Me.lbl_labs_ban_load.AutoSize = True
        Me.lbl_labs_ban_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_ban_load.Location = New System.Drawing.Point(139, 64)
        Me.lbl_labs_ban_load.Name = "lbl_labs_ban_load"
        Me.lbl_labs_ban_load.Size = New System.Drawing.Size(69, 13)
        Me.lbl_labs_ban_load.TabIndex = 13
        Me.lbl_labs_ban_load.Text = "store_banner"
        '
        'lbl_labs_strno_load
        '
        Me.lbl_labs_strno_load.AutoSize = True
        Me.lbl_labs_strno_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_strno_load.Location = New System.Drawing.Point(139, 22)
        Me.lbl_labs_strno_load.Name = "lbl_labs_strno_load"
        Me.lbl_labs_strno_load.Size = New System.Drawing.Size(48, 13)
        Me.lbl_labs_strno_load.TabIndex = 12
        Me.lbl_labs_strno_load.Text = "store_no"
        '
        'lbl_labs_str_cntry
        '
        Me.lbl_labs_str_cntry.AutoSize = True
        Me.lbl_labs_str_cntry.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_str_cntry.Location = New System.Drawing.Point(23, 232)
        Me.lbl_labs_str_cntry.Name = "lbl_labs_str_cntry"
        Me.lbl_labs_str_cntry.Size = New System.Drawing.Size(54, 13)
        Me.lbl_labs_str_cntry.TabIndex = 8
        Me.lbl_labs_str_cntry.Text = "Country:"
        '
        'lbl_labs_str_prov
        '
        Me.lbl_labs_str_prov.AutoSize = True
        Me.lbl_labs_str_prov.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_str_prov.Location = New System.Drawing.Point(23, 208)
        Me.lbl_labs_str_prov.Name = "lbl_labs_str_prov"
        Me.lbl_labs_str_prov.Size = New System.Drawing.Size(105, 13)
        Me.lbl_labs_str_prov.TabIndex = 6
        Me.lbl_labs_str_prov.Text = "Province / State:"
        '
        'lbl_labs_str_city
        '
        Me.lbl_labs_str_city.AutoSize = True
        Me.lbl_labs_str_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_str_city.Location = New System.Drawing.Point(23, 184)
        Me.lbl_labs_str_city.Name = "lbl_labs_str_city"
        Me.lbl_labs_str_city.Size = New System.Drawing.Size(32, 13)
        Me.lbl_labs_str_city.TabIndex = 5
        Me.lbl_labs_str_city.Text = "City:"
        '
        'lbl_labs_str_add
        '
        Me.lbl_labs_str_add.AutoSize = True
        Me.lbl_labs_str_add.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_str_add.Location = New System.Drawing.Point(23, 160)
        Me.lbl_labs_str_add.Name = "lbl_labs_str_add"
        Me.lbl_labs_str_add.Size = New System.Drawing.Size(56, 13)
        Me.lbl_labs_str_add.TabIndex = 4
        Me.lbl_labs_str_add.Text = "Address:"
        '
        'lbl_labs_str_sisstr
        '
        Me.lbl_labs_str_sisstr.AutoSize = True
        Me.lbl_labs_str_sisstr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_str_sisstr.Location = New System.Drawing.Point(23, 136)
        Me.lbl_labs_str_sisstr.Name = "lbl_labs_str_sisstr"
        Me.lbl_labs_str_sisstr.Size = New System.Drawing.Size(77, 13)
        Me.lbl_labs_str_sisstr.TabIndex = 2
        Me.lbl_labs_str_sisstr.Text = "Sister Store:"
        '
        'lbl_labs_str_banner
        '
        Me.lbl_labs_str_banner.AutoSize = True
        Me.lbl_labs_str_banner.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_str_banner.Location = New System.Drawing.Point(23, 64)
        Me.lbl_labs_str_banner.Name = "lbl_labs_str_banner"
        Me.lbl_labs_str_banner.Size = New System.Drawing.Size(51, 13)
        Me.lbl_labs_str_banner.TabIndex = 1
        Me.lbl_labs_str_banner.Text = "Banner:"
        '
        'lbl_labs_str_no
        '
        Me.lbl_labs_str_no.AutoSize = True
        Me.lbl_labs_str_no.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_str_no.Location = New System.Drawing.Point(23, 22)
        Me.lbl_labs_str_no.Name = "lbl_labs_str_no"
        Me.lbl_labs_str_no.Size = New System.Drawing.Size(61, 13)
        Me.lbl_labs_str_no.TabIndex = 0
        Me.lbl_labs_str_no.Text = "Store No:"
        '
        'grp_labs_store_list
        '
        Me.grp_labs_store_list.Controls.Add(Me.lbl_labs_torebuild_load)
        Me.grp_labs_store_list.Controls.Add(Me.lbl_labs_torebuild)
        Me.grp_labs_store_list.Controls.Add(Me.lbl_labs_reserved_load)
        Me.grp_labs_store_list.Controls.Add(Me.lbl_labs_reserved)
        Me.grp_labs_store_list.Controls.Add(Me.lbl_labs_abailable_load)
        Me.grp_labs_store_list.Controls.Add(Me.lbl_labs_total)
        Me.grp_labs_store_list.Controls.Add(Me.btn_labs_ping)
        Me.grp_labs_store_list.Controls.Add(Me.btn_labs_export)
        Me.grp_labs_store_list.Controls.Add(Me.btn_labs_reload)
        Me.grp_labs_store_list.Controls.Add(Me.listview_labs_stores)
        Me.grp_labs_store_list.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_labs_store_list.Location = New System.Drawing.Point(12, 32)
        Me.grp_labs_store_list.Name = "grp_labs_store_list"
        Me.grp_labs_store_list.Size = New System.Drawing.Size(790, 194)
        Me.grp_labs_store_list.TabIndex = 40
        Me.grp_labs_store_list.TabStop = False
        Me.grp_labs_store_list.Text = "LABS List (double-click store to load)"
        '
        'lbl_labs_torebuild_load
        '
        Me.lbl_labs_torebuild_load.AutoSize = True
        Me.lbl_labs_torebuild_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_torebuild_load.Location = New System.Drawing.Point(438, 171)
        Me.lbl_labs_torebuild_load.Name = "lbl_labs_torebuild_load"
        Me.lbl_labs_torebuild_load.Size = New System.Drawing.Size(54, 13)
        Me.lbl_labs_torebuild_load.TabIndex = 50
        Me.lbl_labs_torebuild_load.Text = "#_of_labs"
        '
        'lbl_labs_torebuild
        '
        Me.lbl_labs_torebuild.AutoSize = True
        Me.lbl_labs_torebuild.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_torebuild.Location = New System.Drawing.Point(368, 170)
        Me.lbl_labs_torebuild.Name = "lbl_labs_torebuild"
        Me.lbl_labs_torebuild.Size = New System.Drawing.Size(73, 13)
        Me.lbl_labs_torebuild.TabIndex = 49
        Me.lbl_labs_torebuild.Text = "To Rebuild;"
        '
        'lbl_labs_reserved_load
        '
        Me.lbl_labs_reserved_load.AutoSize = True
        Me.lbl_labs_reserved_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_reserved_load.Location = New System.Drawing.Point(316, 170)
        Me.lbl_labs_reserved_load.Name = "lbl_labs_reserved_load"
        Me.lbl_labs_reserved_load.Size = New System.Drawing.Size(54, 13)
        Me.lbl_labs_reserved_load.TabIndex = 48
        Me.lbl_labs_reserved_load.Text = "#_of_labs"
        '
        'lbl_labs_reserved
        '
        Me.lbl_labs_reserved.AutoSize = True
        Me.lbl_labs_reserved.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_reserved.Location = New System.Drawing.Point(254, 170)
        Me.lbl_labs_reserved.Name = "lbl_labs_reserved"
        Me.lbl_labs_reserved.Size = New System.Drawing.Size(65, 13)
        Me.lbl_labs_reserved.TabIndex = 47
        Me.lbl_labs_reserved.Text = "Reserved:"
        '
        'lbl_labs_abailable_load
        '
        Me.lbl_labs_abailable_load.AutoSize = True
        Me.lbl_labs_abailable_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_abailable_load.Location = New System.Drawing.Point(202, 170)
        Me.lbl_labs_abailable_load.Name = "lbl_labs_abailable_load"
        Me.lbl_labs_abailable_load.Size = New System.Drawing.Size(54, 13)
        Me.lbl_labs_abailable_load.TabIndex = 29
        Me.lbl_labs_abailable_load.Text = "#_of_labs"
        '
        'lbl_labs_total
        '
        Me.lbl_labs_total.AutoSize = True
        Me.lbl_labs_total.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_total.Location = New System.Drawing.Point(142, 171)
        Me.lbl_labs_total.Name = "lbl_labs_total"
        Me.lbl_labs_total.Size = New System.Drawing.Size(63, 13)
        Me.lbl_labs_total.TabIndex = 29
        Me.lbl_labs_total.Text = "Available:"
        '
        'btn_labs_ping
        '
        Me.btn_labs_ping.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_ping.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_ping.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_labs_ping.Location = New System.Drawing.Point(22, 165)
        Me.btn_labs_ping.Name = "btn_labs_ping"
        Me.btn_labs_ping.Size = New System.Drawing.Size(88, 21)
        Me.btn_labs_ping.TabIndex = 27
        Me.btn_labs_ping.Text = "PING List"
        Me.btn_labs_ping.UseVisualStyleBackColor = False
        '
        'btn_labs_export
        '
        Me.btn_labs_export.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_export.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_export.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_labs_export.Location = New System.Drawing.Point(671, 166)
        Me.btn_labs_export.Name = "btn_labs_export"
        Me.btn_labs_export.Size = New System.Drawing.Size(100, 21)
        Me.btn_labs_export.TabIndex = 46
        Me.btn_labs_export.Text = "Export LABS"
        Me.btn_labs_export.UseVisualStyleBackColor = False
        '
        'btn_labs_reload
        '
        Me.btn_labs_reload.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_reload.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_reload.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_labs_reload.Location = New System.Drawing.Point(565, 166)
        Me.btn_labs_reload.Name = "btn_labs_reload"
        Me.btn_labs_reload.Size = New System.Drawing.Size(100, 21)
        Me.btn_labs_reload.TabIndex = 45
        Me.btn_labs_reload.Text = "Refresh LABS"
        Me.btn_labs_reload.UseVisualStyleBackColor = False
        '
        'listview_labs_stores
        '
        Me.listview_labs_stores.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.col_store, Me.col_environment, Me.col_system, Me.col_banner, Me.col_country, Me.col_language, Me.col_bo_location, Me.col_project, Me.col_pm, Me.col_status, Me.col_bo_date})
        Me.listview_labs_stores.FullRowSelect = True
        Me.listview_labs_stores.HideSelection = False
        Me.listview_labs_stores.Location = New System.Drawing.Point(17, 18)
        Me.listview_labs_stores.MultiSelect = False
        Me.listview_labs_stores.Name = "listview_labs_stores"
        Me.listview_labs_stores.Size = New System.Drawing.Size(760, 142)
        Me.listview_labs_stores.TabIndex = 0
        Me.listview_labs_stores.UseCompatibleStateImageBehavior = False
        Me.listview_labs_stores.View = System.Windows.Forms.View.Details
        '
        'col_store
        '
        Me.col_store.Text = "Store"
        Me.col_store.Width = 48
        '
        'col_environment
        '
        Me.col_environment.Text = "ENV"
        Me.col_environment.Width = 46
        '
        'col_system
        '
        Me.col_system.Text = "System"
        '
        'col_banner
        '
        Me.col_banner.Text = "Banner"
        Me.col_banner.Width = 59
        '
        'col_country
        '
        Me.col_country.Text = "Country"
        '
        'col_language
        '
        Me.col_language.Text = "Language"
        Me.col_language.Width = 76
        '
        'col_bo_location
        '
        Me.col_bo_location.Text = "BO Location"
        Me.col_bo_location.Width = 95
        '
        'col_project
        '
        Me.col_project.Text = "Project"
        '
        'col_pm
        '
        Me.col_pm.Text = "PM"
        Me.col_pm.Width = 42
        '
        'col_status
        '
        Me.col_status.Text = "Status"
        '
        'col_bo_date
        '
        Me.col_bo_date.Text = "BO Build Date"
        Me.col_bo_date.Width = 105
        '
        'grp_labs_cashes
        '
        Me.grp_labs_cashes.Controls.Add(Me.pic_labs_printer_c2)
        Me.grp_labs_cashes.Controls.Add(Me.pic_labs_printer_c1)
        Me.grp_labs_cashes.Controls.Add(Me.pic_labs_pinpad_c2)
        Me.grp_labs_cashes.Controls.Add(Me.pic_labs_pinpad_c1)
        Me.grp_labs_cashes.Controls.Add(Me.lbl_labs_bo_load)
        Me.grp_labs_cashes.Controls.Add(Me.lbl_labs_cash2_load)
        Me.grp_labs_cashes.Controls.Add(Me.lbl_labs_cash1_load)
        Me.grp_labs_cashes.Controls.Add(Me.btn_labs_tv_bo)
        Me.grp_labs_cashes.Controls.Add(Me.btn_labs_tv_c2)
        Me.grp_labs_cashes.Controls.Add(Me.btn_labs_tv_c1)
        Me.grp_labs_cashes.Controls.Add(Me.lbl_labs_bo)
        Me.grp_labs_cashes.Controls.Add(Me.lbl_labs_cash2)
        Me.grp_labs_cashes.Controls.Add(Me.lbl_labs_cash1)
        Me.grp_labs_cashes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_labs_cashes.Location = New System.Drawing.Point(405, 239)
        Me.grp_labs_cashes.Name = "grp_labs_cashes"
        Me.grp_labs_cashes.Size = New System.Drawing.Size(255, 110)
        Me.grp_labs_cashes.TabIndex = 41
        Me.grp_labs_cashes.TabStop = False
        Me.grp_labs_cashes.Text = "Store Cashes"
        '
        'pic_labs_printer_c2
        '
        Me.pic_labs_printer_c2.ErrorImage = Nothing
        Me.pic_labs_printer_c2.Image = Global.GDnetworks.My.Resources.Resources.printer_manual
        Me.pic_labs_printer_c2.Location = New System.Drawing.Point(121, 51)
        Me.pic_labs_printer_c2.Name = "pic_labs_printer_c2"
        Me.pic_labs_printer_c2.Size = New System.Drawing.Size(18, 21)
        Me.pic_labs_printer_c2.TabIndex = 42
        Me.pic_labs_printer_c2.TabStop = False
        '
        'pic_labs_printer_c1
        '
        Me.pic_labs_printer_c1.ErrorImage = Nothing
        Me.pic_labs_printer_c1.Image = Global.GDnetworks.My.Resources.Resources.printer_manual
        Me.pic_labs_printer_c1.Location = New System.Drawing.Point(121, 27)
        Me.pic_labs_printer_c1.Name = "pic_labs_printer_c1"
        Me.pic_labs_printer_c1.Size = New System.Drawing.Size(18, 21)
        Me.pic_labs_printer_c1.TabIndex = 41
        Me.pic_labs_printer_c1.TabStop = False
        '
        'pic_labs_pinpad_c2
        '
        Me.pic_labs_pinpad_c2.ErrorImage = Nothing
        Me.pic_labs_pinpad_c2.Image = Global.GDnetworks.My.Resources.Resources.pinpad_manual
        Me.pic_labs_pinpad_c2.Location = New System.Drawing.Point(145, 51)
        Me.pic_labs_pinpad_c2.Name = "pic_labs_pinpad_c2"
        Me.pic_labs_pinpad_c2.Size = New System.Drawing.Size(18, 21)
        Me.pic_labs_pinpad_c2.TabIndex = 40
        Me.pic_labs_pinpad_c2.TabStop = False
        '
        'pic_labs_pinpad_c1
        '
        Me.pic_labs_pinpad_c1.ErrorImage = Nothing
        Me.pic_labs_pinpad_c1.Image = Global.GDnetworks.My.Resources.Resources.pinpad_manual
        Me.pic_labs_pinpad_c1.Location = New System.Drawing.Point(145, 28)
        Me.pic_labs_pinpad_c1.Name = "pic_labs_pinpad_c1"
        Me.pic_labs_pinpad_c1.Size = New System.Drawing.Size(18, 21)
        Me.pic_labs_pinpad_c1.TabIndex = 39
        Me.pic_labs_pinpad_c1.TabStop = False
        '
        'lbl_labs_bo_load
        '
        Me.lbl_labs_bo_load.AutoSize = True
        Me.lbl_labs_bo_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_bo_load.Location = New System.Drawing.Point(69, 80)
        Me.lbl_labs_bo_load.Name = "lbl_labs_bo_load"
        Me.lbl_labs_bo_load.Size = New System.Drawing.Size(21, 13)
        Me.lbl_labs_bo_load.TabIndex = 38
        Me.lbl_labs_bo_load.Text = "bo"
        '
        'lbl_labs_cash2_load
        '
        Me.lbl_labs_cash2_load.AutoSize = True
        Me.lbl_labs_cash2_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_cash2_load.Location = New System.Drawing.Point(69, 56)
        Me.lbl_labs_cash2_load.Name = "lbl_labs_cash2_load"
        Me.lbl_labs_cash2_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_labs_cash2_load.TabIndex = 33
        Me.lbl_labs_cash2_load.Text = "cash2"
        '
        'lbl_labs_cash1_load
        '
        Me.lbl_labs_cash1_load.AutoSize = True
        Me.lbl_labs_cash1_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_cash1_load.Location = New System.Drawing.Point(69, 32)
        Me.lbl_labs_cash1_load.Name = "lbl_labs_cash1_load"
        Me.lbl_labs_cash1_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_labs_cash1_load.TabIndex = 32
        Me.lbl_labs_cash1_load.Text = "cash1"
        '
        'btn_labs_tv_bo
        '
        Me.btn_labs_tv_bo.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_tv_bo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_tv_bo.Location = New System.Drawing.Point(169, 75)
        Me.btn_labs_tv_bo.Name = "btn_labs_tv_bo"
        Me.btn_labs_tv_bo.Size = New System.Drawing.Size(73, 21)
        Me.btn_labs_tv_bo.TabIndex = 24
        Me.btn_labs_tv_bo.Text = "Remote"
        Me.btn_labs_tv_bo.UseVisualStyleBackColor = False
        '
        'btn_labs_tv_c2
        '
        Me.btn_labs_tv_c2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_tv_c2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_tv_c2.Location = New System.Drawing.Point(169, 51)
        Me.btn_labs_tv_c2.Name = "btn_labs_tv_c2"
        Me.btn_labs_tv_c2.Size = New System.Drawing.Size(73, 21)
        Me.btn_labs_tv_c2.TabIndex = 19
        Me.btn_labs_tv_c2.Text = "Remote"
        Me.btn_labs_tv_c2.UseVisualStyleBackColor = False
        '
        'btn_labs_tv_c1
        '
        Me.btn_labs_tv_c1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_tv_c1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_tv_c1.Location = New System.Drawing.Point(169, 27)
        Me.btn_labs_tv_c1.Name = "btn_labs_tv_c1"
        Me.btn_labs_tv_c1.Size = New System.Drawing.Size(73, 21)
        Me.btn_labs_tv_c1.TabIndex = 18
        Me.btn_labs_tv_c1.Text = "Remote"
        Me.btn_labs_tv_c1.UseVisualStyleBackColor = False
        '
        'lbl_labs_bo
        '
        Me.lbl_labs_bo.AutoSize = True
        Me.lbl_labs_bo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_bo.Location = New System.Drawing.Point(17, 80)
        Me.lbl_labs_bo.Name = "lbl_labs_bo"
        Me.lbl_labs_bo.Size = New System.Drawing.Size(48, 13)
        Me.lbl_labs_bo.TabIndex = 6
        Me.lbl_labs_bo.Text = "BO PC:"
        '
        'lbl_labs_cash2
        '
        Me.lbl_labs_cash2.AutoSize = True
        Me.lbl_labs_cash2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_cash2.Location = New System.Drawing.Point(17, 56)
        Me.lbl_labs_cash2.Name = "lbl_labs_cash2"
        Me.lbl_labs_cash2.Size = New System.Drawing.Size(46, 13)
        Me.lbl_labs_cash2.TabIndex = 1
        Me.lbl_labs_cash2.Text = "Cash2:"
        '
        'lbl_labs_cash1
        '
        Me.lbl_labs_cash1.AutoSize = True
        Me.lbl_labs_cash1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_cash1.Location = New System.Drawing.Point(17, 32)
        Me.lbl_labs_cash1.Name = "lbl_labs_cash1"
        Me.lbl_labs_cash1.Size = New System.Drawing.Size(46, 13)
        Me.lbl_labs_cash1.TabIndex = 0
        Me.lbl_labs_cash1.Text = "Cash1:"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(61, 4)
        '
        'grpbox_labs_patches
        '
        Me.grpbox_labs_patches.Controls.Add(Me.ListView_labs_patch)
        Me.grpbox_labs_patches.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_labs_patches.Location = New System.Drawing.Point(666, 239)
        Me.grpbox_labs_patches.Name = "grpbox_labs_patches"
        Me.grpbox_labs_patches.Size = New System.Drawing.Size(136, 110)
        Me.grpbox_labs_patches.TabIndex = 43
        Me.grpbox_labs_patches.TabStop = False
        Me.grpbox_labs_patches.Text = "Extra Patches"
        '
        'ListView_labs_patch
        '
        Me.ListView_labs_patch.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.col_patchid})
        Me.ListView_labs_patch.FullRowSelect = True
        Me.ListView_labs_patch.HideSelection = False
        Me.ListView_labs_patch.Location = New System.Drawing.Point(12, 21)
        Me.ListView_labs_patch.MultiSelect = False
        Me.ListView_labs_patch.Name = "ListView_labs_patch"
        Me.ListView_labs_patch.Size = New System.Drawing.Size(113, 76)
        Me.ListView_labs_patch.TabIndex = 1
        Me.ListView_labs_patch.UseCompatibleStateImageBehavior = False
        Me.ListView_labs_patch.View = System.Windows.Forms.View.Details
        '
        'col_patchid
        '
        Me.col_patchid.Text = "PatchID"
        Me.col_patchid.Width = 110
        '
        'grpbox_labs_comments
        '
        Me.grpbox_labs_comments.Controls.Add(Me.lbl_labs_save_comments)
        Me.grpbox_labs_comments.Controls.Add(Me.btn_labs_clear_comments)
        Me.grpbox_labs_comments.Controls.Add(Me.btn_labs_save)
        Me.grpbox_labs_comments.Controls.Add(Me.txtbox_labs_comments)
        Me.grpbox_labs_comments.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_labs_comments.Location = New System.Drawing.Point(405, 358)
        Me.grpbox_labs_comments.Name = "grpbox_labs_comments"
        Me.grpbox_labs_comments.Size = New System.Drawing.Size(393, 164)
        Me.grpbox_labs_comments.TabIndex = 44
        Me.grpbox_labs_comments.TabStop = False
        Me.grpbox_labs_comments.Text = "Comments"
        '
        'lbl_labs_save_comments
        '
        Me.lbl_labs_save_comments.AutoSize = True
        Me.lbl_labs_save_comments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_labs_save_comments.ForeColor = System.Drawing.Color.Red
        Me.lbl_labs_save_comments.Location = New System.Drawing.Point(61, 138)
        Me.lbl_labs_save_comments.Name = "lbl_labs_save_comments"
        Me.lbl_labs_save_comments.Size = New System.Drawing.Size(165, 13)
        Me.lbl_labs_save_comments.TabIndex = 134
        Me.lbl_labs_save_comments.Text = "To save comments click on Save"
        '
        'btn_labs_clear_comments
        '
        Me.btn_labs_clear_comments.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_clear_comments.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_clear_comments.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_labs_clear_comments.Location = New System.Drawing.Point(232, 134)
        Me.btn_labs_clear_comments.Name = "btn_labs_clear_comments"
        Me.btn_labs_clear_comments.Size = New System.Drawing.Size(73, 21)
        Me.btn_labs_clear_comments.TabIndex = 46
        Me.btn_labs_clear_comments.Text = "Clear"
        Me.btn_labs_clear_comments.UseVisualStyleBackColor = False
        '
        'btn_labs_save
        '
        Me.btn_labs_save.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_labs_save.Location = New System.Drawing.Point(311, 134)
        Me.btn_labs_save.Name = "btn_labs_save"
        Me.btn_labs_save.Size = New System.Drawing.Size(73, 21)
        Me.btn_labs_save.TabIndex = 45
        Me.btn_labs_save.Text = "Save"
        Me.btn_labs_save.UseVisualStyleBackColor = False
        '
        'txtbox_labs_comments
        '
        Me.txtbox_labs_comments.Location = New System.Drawing.Point(20, 21)
        Me.txtbox_labs_comments.Name = "txtbox_labs_comments"
        Me.txtbox_labs_comments.Size = New System.Drawing.Size(353, 108)
        Me.txtbox_labs_comments.TabIndex = 0
        Me.txtbox_labs_comments.Text = ""
        '
        'pic_labs
        '
        Me.pic_labs.ErrorImage = Nothing
        Me.pic_labs.Image = CType(resources.GetObject("pic_labs.Image"), System.Drawing.Image)
        Me.pic_labs.InitialImage = Nothing
        Me.pic_labs.Location = New System.Drawing.Point(55, 485)
        Me.pic_labs.Name = "pic_labs"
        Me.pic_labs.Size = New System.Drawing.Size(136, 41)
        Me.pic_labs.TabIndex = 21
        Me.pic_labs.TabStop = False
        '
        'btn_labs_refresh_store
        '
        Me.btn_labs_refresh_store.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_refresh_store.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_refresh_store.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_labs_refresh_store.Location = New System.Drawing.Point(203, 495)
        Me.btn_labs_refresh_store.Name = "btn_labs_refresh_store"
        Me.btn_labs_refresh_store.Size = New System.Drawing.Size(100, 21)
        Me.btn_labs_refresh_store.TabIndex = 46
        Me.btn_labs_refresh_store.Text = "Refresh store"
        Me.btn_labs_refresh_store.UseVisualStyleBackColor = False
        '
        'btn_labs_clear
        '
        Me.btn_labs_clear.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_labs_clear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_labs_clear.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_labs_clear.Location = New System.Drawing.Point(309, 495)
        Me.btn_labs_clear.Name = "btn_labs_clear"
        Me.btn_labs_clear.Size = New System.Drawing.Size(90, 21)
        Me.btn_labs_clear.TabIndex = 47
        Me.btn_labs_clear.Text = "Clear store"
        Me.btn_labs_clear.UseVisualStyleBackColor = False
        '
        'ping_timer_labs
        '
        '
        'Labs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(813, 538)
        Me.Controls.Add(Me.btn_labs_clear)
        Me.Controls.Add(Me.btn_labs_refresh_store)
        Me.Controls.Add(Me.grpbox_labs_comments)
        Me.Controls.Add(Me.grpbox_labs_patches)
        Me.Controls.Add(Me.grp_labs_cashes)
        Me.Controls.Add(Me.grp_labs_store_list)
        Me.Controls.Add(Me.grp_labs_store)
        Me.Controls.Add(Me.lbl_labs_role)
        Me.Controls.Add(Me.lbl_labs_user)
        Me.Controls.Add(Me.lbl_labs_vpn_status)
        Me.Controls.Add(Me.lbl_labs_reload)
        Me.Controls.Add(Me.lbl_labs_role_load)
        Me.Controls.Add(Me.lbl_labs_user_load)
        Me.Controls.Add(Me.lbl_labs_vpn_con_load)
        Me.Controls.Add(Me.lbl_labs_refresh_load)
        Me.Controls.Add(Me.pic_labs)
        Me.Controls.Add(Me.top_menu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "Labs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks (Labs)"
        Me.top_menu.ResumeLayout(False)
        Me.top_menu.PerformLayout()
        Me.grp_labs_store.ResumeLayout(False)
        Me.grp_labs_store.PerformLayout()
        Me.grp_labs_store_list.ResumeLayout(False)
        Me.grp_labs_store_list.PerformLayout()
        Me.grp_labs_cashes.ResumeLayout(False)
        Me.grp_labs_cashes.PerformLayout()
        CType(Me.pic_labs_printer_c2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_labs_printer_c1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_labs_pinpad_c2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_labs_pinpad_c1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpbox_labs_patches.ResumeLayout(False)
        Me.grpbox_labs_comments.ResumeLayout(False)
        Me.grpbox_labs_comments.PerformLayout()
        CType(Me.pic_labs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents top_menu As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AdminToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LABSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PRODToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReloadToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StoreToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeleteStoreToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ViewToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SupportDocsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GDriveToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents JiraToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LocalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents WebToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GarageToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DynamiteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DynamiteCAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DynamiteUSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GRPDYNToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoteDesktopToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TeamViewerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AutoUpdateToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OffToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BrowserToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ChromeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IEToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshRateToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents topmenu_refresh_0sec As ToolStripMenuItem
    Friend WithEvents topmenu_refresh_10sec As ToolStripMenuItem
    Friend WithEvents topmenu_refresh_30sec As ToolStripMenuItem
    Friend WithEvents topmenu_refresh_60sec As ToolStripMenuItem
    Friend WithEvents SavePositionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SupportLocationsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpF1ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents UpdatesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents KeyboardShortcutsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SupportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RDPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TeamViewerToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents pic_labs As PictureBox
    Friend WithEvents lbl_labs_refresh_load As Label
    Friend WithEvents lbl_labs_vpn_con_load As Label
    Friend WithEvents lbl_labs_user_load As Label
    Friend WithEvents lbl_labs_role_load As Label
    Friend WithEvents lbl_labs_reload As Label
    Friend WithEvents lbl_labs_vpn_status As Label
    Friend WithEvents lbl_labs_user As Label
    Friend WithEvents lbl_labs_role As Label
    Friend WithEvents grp_labs_store As GroupBox
    Friend WithEvents lbl_labs_str_system_load As Label
    Friend WithEvents lbl_labs_str_system As Label
    Friend WithEvents lbl_labs_store_iscombo_load As Label
    Friend WithEvents lbl_labs_store_iscombo As Label
    Friend WithEvents lbl_labs_country_load As Label
    Friend WithEvents lbl_labs_state_load As Label
    Friend WithEvents lbl_labs_city_load As Label
    Friend WithEvents lbl_labs_address_load As Label
    Friend WithEvents lbl_labs_sis_store_load As Label
    Friend WithEvents lbl_labs_ban_load As Label
    Friend WithEvents lbl_labs_strno_load As Label
    Friend WithEvents lbl_labs_str_cntry As Label
    Friend WithEvents lbl_labs_str_prov As Label
    Friend WithEvents lbl_labs_str_city As Label
    Friend WithEvents lbl_labs_str_add As Label
    Friend WithEvents lbl_labs_str_sisstr As Label
    Friend WithEvents lbl_labs_str_banner As Label
    Friend WithEvents lbl_labs_str_no As Label
    Friend WithEvents grp_labs_store_list As GroupBox
    Friend WithEvents listview_labs_stores As ListView
    Friend WithEvents grp_labs_cashes As GroupBox
    Friend WithEvents pic_labs_pinpad_c2 As PictureBox
    Friend WithEvents pic_labs_pinpad_c1 As PictureBox
    Friend WithEvents lbl_labs_bo_load As Label
    Friend WithEvents lbl_labs_cash2_load As Label
    Friend WithEvents lbl_labs_cash1_load As Label
    Friend WithEvents btn_labs_tv_bo As Button
    Friend WithEvents btn_labs_tv_c2 As Button
    Friend WithEvents btn_labs_tv_c1 As Button
    Friend WithEvents lbl_labs_bo As Label
    Friend WithEvents lbl_labs_cash2 As Label
    Friend WithEvents lbl_labs_cash1 As Label
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents pic_labs_printer_c1 As PictureBox
    Friend WithEvents pic_labs_printer_c2 As PictureBox
    Friend WithEvents grpbox_labs_patches As GroupBox
    Friend WithEvents grpbox_labs_comments As GroupBox
    Friend WithEvents txtbox_labs_comments As RichTextBox
    Friend WithEvents col_store As ColumnHeader
    Friend WithEvents col_environment As ColumnHeader
    Friend WithEvents col_banner As ColumnHeader
    Friend WithEvents col_country As ColumnHeader
    Friend WithEvents col_language As ColumnHeader
    Friend WithEvents col_bo_location As ColumnHeader
    Friend WithEvents col_project As ColumnHeader
    Friend WithEvents col_status As ColumnHeader
    Friend WithEvents col_pm As ColumnHeader
    Friend WithEvents col_bo_date As ColumnHeader
    Friend WithEvents btn_labs_reload As Button
    Friend WithEvents col_system As ColumnHeader
    Friend WithEvents ListView_labs_patch As ListView
    Friend WithEvents col_patchid As ColumnHeader
    Friend WithEvents OnMapToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btn_labs_export As Button
    Friend WithEvents btn_labs_save As Button
    Friend WithEvents btn_labs_clear_comments As Button
    Friend WithEvents lbl_labs_save_comments As Label
    Friend WithEvents btn_labs_refresh_store As Button
    Friend WithEvents btn_labs_clear As Button
    Friend WithEvents btn_labs_ping As Button
    Friend WithEvents lbl_labs_status As Label
    Friend WithEvents lbl_labs_status_load As Label
    Friend WithEvents lbl_labs_total As Label
    Friend WithEvents lbl_labs_abailable_load As Label
    Friend WithEvents lbl_labs_reserved As Label
    Friend WithEvents lbl_labs_reserved_load As Label
    Friend WithEvents lbl_labs_torebuild As Label
    Friend WithEvents lbl_labs_torebuild_load As Label
    Friend WithEvents ping_timer_labs As Timer
End Class
