﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class speedtest_list
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(speedtest_list))
        Me.grpbox_speedtestlist_list = New System.Windows.Forms.GroupBox()
        Me.lbl_speedtestlist_info = New System.Windows.Forms.Label()
        Me.listview_speed = New System.Windows.Forms.ListView()
        Me.speed_store = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.speed_dl = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.speed_up = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.speed_wif = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.speed_date = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lbl_speedtestlist_wifi_result = New System.Windows.Forms.Label()
        Me.lbl_speedtestlist_sp_result = New System.Windows.Forms.Label()
        Me.lbl_speedtestlist_stores_result = New System.Windows.Forms.Label()
        Me.lbl_speedtestlist_wifi = New System.Windows.Forms.Label()
        Me.lbl_speedtestlist_sp = New System.Windows.Forms.Label()
        Me.lbl_speedtestlist_stores = New System.Windows.Forms.Label()
        Me.btn_searchby_close = New System.Windows.Forms.Button()
        Me.btn_searchby_save = New System.Windows.Forms.Button()
        Me.grpbox_speedtestlist_list.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpbox_speedtestlist_list
        '
        Me.grpbox_speedtestlist_list.Controls.Add(Me.lbl_speedtestlist_info)
        Me.grpbox_speedtestlist_list.Controls.Add(Me.listview_speed)
        Me.grpbox_speedtestlist_list.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_speedtestlist_list.Location = New System.Drawing.Point(12, 12)
        Me.grpbox_speedtestlist_list.Name = "grpbox_speedtestlist_list"
        Me.grpbox_speedtestlist_list.Size = New System.Drawing.Size(562, 276)
        Me.grpbox_speedtestlist_list.TabIndex = 0
        Me.grpbox_speedtestlist_list.TabStop = False
        Me.grpbox_speedtestlist_list.Text = "Speedtest list by store"
        '
        'lbl_speedtestlist_info
        '
        Me.lbl_speedtestlist_info.AutoSize = True
        Me.lbl_speedtestlist_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtestlist_info.ForeColor = System.Drawing.Color.Red
        Me.lbl_speedtestlist_info.Location = New System.Drawing.Point(338, 13)
        Me.lbl_speedtestlist_info.Name = "lbl_speedtestlist_info"
        Me.lbl_speedtestlist_info.Size = New System.Drawing.Size(192, 13)
        Me.lbl_speedtestlist_info.TabIndex = 133
        Me.lbl_speedtestlist_info.Text = "Double-click on any store to see details"
        '
        'listview_speed
        '
        Me.listview_speed.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.speed_store, Me.speed_dl, Me.speed_up, Me.speed_wif, Me.speed_date})
        Me.listview_speed.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listview_speed.FullRowSelect = True
        Me.listview_speed.HideSelection = False
        Me.listview_speed.Location = New System.Drawing.Point(21, 31)
        Me.listview_speed.MultiSelect = False
        Me.listview_speed.Name = "listview_speed"
        Me.listview_speed.Size = New System.Drawing.Size(520, 230)
        Me.listview_speed.TabIndex = 3
        Me.listview_speed.UseCompatibleStateImageBehavior = False
        Me.listview_speed.View = System.Windows.Forms.View.Details
        '
        'speed_store
        '
        Me.speed_store.Text = "Store#"
        Me.speed_store.Width = 52
        '
        'speed_dl
        '
        Me.speed_dl.Text = "Download (Mbps)"
        Me.speed_dl.Width = 104
        '
        'speed_up
        '
        Me.speed_up.Text = "Upload (Mbps)"
        Me.speed_up.Width = 104
        '
        'speed_wif
        '
        Me.speed_wif.Text = "Guest WiFi (Mbps)"
        Me.speed_wif.Width = 104
        '
        'speed_date
        '
        Me.speed_date.Text = "Performed On"
        Me.speed_date.Width = 150
        '
        'lbl_speedtestlist_wifi_result
        '
        Me.lbl_speedtestlist_wifi_result.AutoSize = True
        Me.lbl_speedtestlist_wifi_result.Location = New System.Drawing.Point(271, 299)
        Me.lbl_speedtestlist_wifi_result.Name = "lbl_speedtestlist_wifi_result"
        Me.lbl_speedtestlist_wifi_result.Size = New System.Drawing.Size(19, 13)
        Me.lbl_speedtestlist_wifi_result.TabIndex = 132
        Me.lbl_speedtestlist_wifi_result.Text = "nb"
        '
        'lbl_speedtestlist_sp_result
        '
        Me.lbl_speedtestlist_sp_result.AutoSize = True
        Me.lbl_speedtestlist_sp_result.Location = New System.Drawing.Point(173, 299)
        Me.lbl_speedtestlist_sp_result.Name = "lbl_speedtestlist_sp_result"
        Me.lbl_speedtestlist_sp_result.Size = New System.Drawing.Size(19, 13)
        Me.lbl_speedtestlist_sp_result.TabIndex = 131
        Me.lbl_speedtestlist_sp_result.Text = "nb"
        '
        'lbl_speedtestlist_stores_result
        '
        Me.lbl_speedtestlist_stores_result.AutoSize = True
        Me.lbl_speedtestlist_stores_result.Location = New System.Drawing.Point(74, 299)
        Me.lbl_speedtestlist_stores_result.Name = "lbl_speedtestlist_stores_result"
        Me.lbl_speedtestlist_stores_result.Size = New System.Drawing.Size(19, 13)
        Me.lbl_speedtestlist_stores_result.TabIndex = 130
        Me.lbl_speedtestlist_stores_result.Text = "nb"
        '
        'lbl_speedtestlist_wifi
        '
        Me.lbl_speedtestlist_wifi.AutoSize = True
        Me.lbl_speedtestlist_wifi.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtestlist_wifi.Location = New System.Drawing.Point(238, 299)
        Me.lbl_speedtestlist_wifi.Name = "lbl_speedtestlist_wifi"
        Me.lbl_speedtestlist_wifi.Size = New System.Drawing.Size(36, 13)
        Me.lbl_speedtestlist_wifi.TabIndex = 129
        Me.lbl_speedtestlist_wifi.Text = "WiFi:"
        '
        'lbl_speedtestlist_sp
        '
        Me.lbl_speedtestlist_sp.AutoSize = True
        Me.lbl_speedtestlist_sp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtestlist_sp.Location = New System.Drawing.Point(126, 299)
        Me.lbl_speedtestlist_sp.Name = "lbl_speedtestlist_sp"
        Me.lbl_speedtestlist_sp.Size = New System.Drawing.Size(50, 13)
        Me.lbl_speedtestlist_sp.TabIndex = 128
        Me.lbl_speedtestlist_sp.Text = "DL/UP:"
        '
        'lbl_speedtestlist_stores
        '
        Me.lbl_speedtestlist_stores.AutoSize = True
        Me.lbl_speedtestlist_stores.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speedtestlist_stores.Location = New System.Drawing.Point(30, 299)
        Me.lbl_speedtestlist_stores.Name = "lbl_speedtestlist_stores"
        Me.lbl_speedtestlist_stores.Size = New System.Drawing.Size(47, 13)
        Me.lbl_speedtestlist_stores.TabIndex = 127
        Me.lbl_speedtestlist_stores.Text = "Stores:"
        '
        'btn_searchby_close
        '
        Me.btn_searchby_close.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_searchby_close.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_searchby_close.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_searchby_close.Location = New System.Drawing.Point(407, 294)
        Me.btn_searchby_close.Name = "btn_searchby_close"
        Me.btn_searchby_close.Size = New System.Drawing.Size(68, 21)
        Me.btn_searchby_close.TabIndex = 126
        Me.btn_searchby_close.Text = "CLOSE"
        Me.btn_searchby_close.UseVisualStyleBackColor = False
        '
        'btn_searchby_save
        '
        Me.btn_searchby_save.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_searchby_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_searchby_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_searchby_save.Location = New System.Drawing.Point(481, 294)
        Me.btn_searchby_save.Name = "btn_searchby_save"
        Me.btn_searchby_save.Size = New System.Drawing.Size(83, 21)
        Me.btn_searchby_save.TabIndex = 125
        Me.btn_searchby_save.Text = "EXPORT"
        Me.btn_searchby_save.UseVisualStyleBackColor = False
        '
        'speedtest_list
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(587, 323)
        Me.Controls.Add(Me.grpbox_speedtestlist_list)
        Me.Controls.Add(Me.lbl_speedtestlist_wifi_result)
        Me.Controls.Add(Me.btn_searchby_save)
        Me.Controls.Add(Me.lbl_speedtestlist_sp_result)
        Me.Controls.Add(Me.btn_searchby_close)
        Me.Controls.Add(Me.lbl_speedtestlist_stores_result)
        Me.Controls.Add(Me.lbl_speedtestlist_wifi)
        Me.Controls.Add(Me.lbl_speedtestlist_stores)
        Me.Controls.Add(Me.lbl_speedtestlist_sp)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "speedtest_list"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Speedtest List"
        Me.grpbox_speedtestlist_list.ResumeLayout(False)
        Me.grpbox_speedtestlist_list.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents grpbox_speedtestlist_list As GroupBox
    Friend WithEvents listview_speed As ListView
    Friend WithEvents speed_store As ColumnHeader
    Friend WithEvents speed_dl As ColumnHeader
    Friend WithEvents speed_up As ColumnHeader
    Friend WithEvents speed_wif As ColumnHeader
    Friend WithEvents speed_date As ColumnHeader
    Friend WithEvents btn_searchby_save As Button
    Friend WithEvents btn_searchby_close As Button
    Friend WithEvents lbl_speedtestlist_stores As Label
    Friend WithEvents lbl_speedtestlist_sp As Label
    Friend WithEvents lbl_speedtestlist_wifi As Label
    Friend WithEvents lbl_speedtestlist_stores_result As Label
    Friend WithEvents lbl_speedtestlist_wifi_result As Label
    Friend WithEvents lbl_speedtestlist_sp_result As Label
    Friend WithEvents lbl_speedtestlist_info As Label
End Class
