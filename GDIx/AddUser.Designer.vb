﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AddUser))
        Me.lbl_adduser_user = New System.Windows.Forms.Label()
        Me.grpbox_adduser_info = New System.Windows.Forms.GroupBox()
        Me.combo_adduser_role = New System.Windows.Forms.ComboBox()
        Me.lbl_adduser_role = New System.Windows.Forms.Label()
        Me.txtbox_adduser_usr_load = New System.Windows.Forms.TextBox()
        Me.lbl_adduser_status = New System.Windows.Forms.Label()
        Me.radio_adduser_enabled = New System.Windows.Forms.RadioButton()
        Me.radio_adduser_disabled = New System.Windows.Forms.RadioButton()
        Me.grpbox_adduser_account = New System.Windows.Forms.GroupBox()
        Me.grpbox_adduser_details = New System.Windows.Forms.GroupBox()
        Me.lbl_adduser_details = New System.Windows.Forms.Label()
        Me.btn_adduser_add = New System.Windows.Forms.Button()
        Me.btn_adduser_cancel = New System.Windows.Forms.Button()
        Me.grpbox_adduser_info.SuspendLayout()
        Me.grpbox_adduser_account.SuspendLayout()
        Me.grpbox_adduser_details.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbl_adduser_user
        '
        Me.lbl_adduser_user.AutoSize = True
        Me.lbl_adduser_user.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_adduser_user.Location = New System.Drawing.Point(22, 26)
        Me.lbl_adduser_user.Name = "lbl_adduser_user"
        Me.lbl_adduser_user.Size = New System.Drawing.Size(67, 13)
        Me.lbl_adduser_user.TabIndex = 1
        Me.lbl_adduser_user.Text = "Username:"
        '
        'grpbox_adduser_info
        '
        Me.grpbox_adduser_info.Controls.Add(Me.combo_adduser_role)
        Me.grpbox_adduser_info.Controls.Add(Me.lbl_adduser_role)
        Me.grpbox_adduser_info.Controls.Add(Me.txtbox_adduser_usr_load)
        Me.grpbox_adduser_info.Controls.Add(Me.lbl_adduser_user)
        Me.grpbox_adduser_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_adduser_info.Location = New System.Drawing.Point(12, 12)
        Me.grpbox_adduser_info.Name = "grpbox_adduser_info"
        Me.grpbox_adduser_info.Size = New System.Drawing.Size(225, 83)
        Me.grpbox_adduser_info.TabIndex = 2
        Me.grpbox_adduser_info.TabStop = False
        Me.grpbox_adduser_info.Text = "User Information"
        '
        'combo_adduser_role
        '
        Me.combo_adduser_role.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.combo_adduser_role.FormattingEnabled = True
        Me.combo_adduser_role.Items.AddRange(New Object() {"*SYS", "Admin", "DBA", "DEV", "Guest", "HO Support", "ORPOS", "Tech Support", "Tech Support*", "SAM", "SAM*"})
        Me.combo_adduser_role.Location = New System.Drawing.Point(92, 51)
        Me.combo_adduser_role.Name = "combo_adduser_role"
        Me.combo_adduser_role.Size = New System.Drawing.Size(121, 23)
        Me.combo_adduser_role.TabIndex = 4
        '
        'lbl_adduser_role
        '
        Me.lbl_adduser_role.AutoSize = True
        Me.lbl_adduser_role.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_adduser_role.Location = New System.Drawing.Point(22, 56)
        Me.lbl_adduser_role.Name = "lbl_adduser_role"
        Me.lbl_adduser_role.Size = New System.Drawing.Size(37, 13)
        Me.lbl_adduser_role.TabIndex = 3
        Me.lbl_adduser_role.Text = "Role:"
        '
        'txtbox_adduser_usr_load
        '
        Me.txtbox_adduser_usr_load.Location = New System.Drawing.Point(92, 21)
        Me.txtbox_adduser_usr_load.Name = "txtbox_adduser_usr_load"
        Me.txtbox_adduser_usr_load.Size = New System.Drawing.Size(121, 21)
        Me.txtbox_adduser_usr_load.TabIndex = 2
        '
        'lbl_adduser_status
        '
        Me.lbl_adduser_status.AutoSize = True
        Me.lbl_adduser_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_adduser_status.Location = New System.Drawing.Point(17, 26)
        Me.lbl_adduser_status.Name = "lbl_adduser_status"
        Me.lbl_adduser_status.Size = New System.Drawing.Size(47, 13)
        Me.lbl_adduser_status.TabIndex = 5
        Me.lbl_adduser_status.Text = "Status:"
        '
        'radio_adduser_enabled
        '
        Me.radio_adduser_enabled.AutoSize = True
        Me.radio_adduser_enabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radio_adduser_enabled.Location = New System.Drawing.Point(70, 24)
        Me.radio_adduser_enabled.Name = "radio_adduser_enabled"
        Me.radio_adduser_enabled.Size = New System.Drawing.Size(64, 17)
        Me.radio_adduser_enabled.TabIndex = 6
        Me.radio_adduser_enabled.TabStop = True
        Me.radio_adduser_enabled.Text = "Enabled"
        Me.radio_adduser_enabled.UseVisualStyleBackColor = True
        '
        'radio_adduser_disabled
        '
        Me.radio_adduser_disabled.AutoSize = True
        Me.radio_adduser_disabled.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radio_adduser_disabled.Location = New System.Drawing.Point(140, 24)
        Me.radio_adduser_disabled.Name = "radio_adduser_disabled"
        Me.radio_adduser_disabled.Size = New System.Drawing.Size(66, 17)
        Me.radio_adduser_disabled.TabIndex = 7
        Me.radio_adduser_disabled.TabStop = True
        Me.radio_adduser_disabled.Text = "Disabled"
        Me.radio_adduser_disabled.UseVisualStyleBackColor = True
        '
        'grpbox_adduser_account
        '
        Me.grpbox_adduser_account.Controls.Add(Me.lbl_adduser_status)
        Me.grpbox_adduser_account.Controls.Add(Me.radio_adduser_disabled)
        Me.grpbox_adduser_account.Controls.Add(Me.radio_adduser_enabled)
        Me.grpbox_adduser_account.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_adduser_account.Location = New System.Drawing.Point(12, 101)
        Me.grpbox_adduser_account.Name = "grpbox_adduser_account"
        Me.grpbox_adduser_account.Size = New System.Drawing.Size(225, 49)
        Me.grpbox_adduser_account.TabIndex = 5
        Me.grpbox_adduser_account.TabStop = False
        Me.grpbox_adduser_account.Text = "Account"
        '
        'grpbox_adduser_details
        '
        Me.grpbox_adduser_details.Controls.Add(Me.lbl_adduser_details)
        Me.grpbox_adduser_details.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_adduser_details.Location = New System.Drawing.Point(243, 12)
        Me.grpbox_adduser_details.Name = "grpbox_adduser_details"
        Me.grpbox_adduser_details.Size = New System.Drawing.Size(360, 171)
        Me.grpbox_adduser_details.TabIndex = 8
        Me.grpbox_adduser_details.TabStop = False
        Me.grpbox_adduser_details.Text = "Details"
        '
        'lbl_adduser_details
        '
        Me.lbl_adduser_details.AutoSize = True
        Me.lbl_adduser_details.Location = New System.Drawing.Point(6, 21)
        Me.lbl_adduser_details.Name = "lbl_adduser_details"
        Me.lbl_adduser_details.Size = New System.Drawing.Size(341, 150)
        Me.lbl_adduser_details.TabIndex = 0
        Me.lbl_adduser_details.Text = resources.GetString("lbl_adduser_details.Text")
        '
        'btn_adduser_add
        '
        Me.btn_adduser_add.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_adduser_add.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_adduser_add.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_adduser_add.Location = New System.Drawing.Point(83, 156)
        Me.btn_adduser_add.Name = "btn_adduser_add"
        Me.btn_adduser_add.Size = New System.Drawing.Size(74, 21)
        Me.btn_adduser_add.TabIndex = 15
        Me.btn_adduser_add.Text = "Add"
        Me.btn_adduser_add.UseVisualStyleBackColor = False
        '
        'btn_adduser_cancel
        '
        Me.btn_adduser_cancel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_adduser_cancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_adduser_cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_adduser_cancel.Location = New System.Drawing.Point(163, 156)
        Me.btn_adduser_cancel.Name = "btn_adduser_cancel"
        Me.btn_adduser_cancel.Size = New System.Drawing.Size(74, 21)
        Me.btn_adduser_cancel.TabIndex = 16
        Me.btn_adduser_cancel.Text = "Close"
        Me.btn_adduser_cancel.UseVisualStyleBackColor = False
        '
        'AddUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(613, 194)
        Me.Controls.Add(Me.btn_adduser_cancel)
        Me.Controls.Add(Me.btn_adduser_add)
        Me.Controls.Add(Me.grpbox_adduser_details)
        Me.Controls.Add(Me.grpbox_adduser_account)
        Me.Controls.Add(Me.grpbox_adduser_info)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "AddUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - New User"
        Me.grpbox_adduser_info.ResumeLayout(False)
        Me.grpbox_adduser_info.PerformLayout()
        Me.grpbox_adduser_account.ResumeLayout(False)
        Me.grpbox_adduser_account.PerformLayout()
        Me.grpbox_adduser_details.ResumeLayout(False)
        Me.grpbox_adduser_details.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents lbl_adduser_user As Label
    Friend WithEvents grpbox_adduser_info As GroupBox
    Friend WithEvents txtbox_adduser_usr_load As TextBox
    Friend WithEvents lbl_adduser_role As Label
    Friend WithEvents combo_adduser_role As ComboBox
    Friend WithEvents radio_adduser_enabled As RadioButton
    Friend WithEvents lbl_adduser_status As Label
    Friend WithEvents radio_adduser_disabled As RadioButton
    Friend WithEvents grpbox_adduser_account As GroupBox
    Friend WithEvents grpbox_adduser_details As GroupBox
    Friend WithEvents btn_adduser_add As Button
    Friend WithEvents btn_adduser_cancel As Button
    Friend WithEvents lbl_adduser_details As Label
End Class
