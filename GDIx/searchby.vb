﻿'AUTHOR: Alex Dumitrascu
'DATE: 08-03-2018
'UPDATE: 28-08-2018
'
'FORM: searchby.vb (access from MAIN)

Imports System.Data.SqlClient
Imports System.IO
Imports System.Text.RegularExpressions

Public Class searchby

    ''''APP VARIABLES
    'MAIN   
    Dim isCombo As Boolean = False                                  'check if combo    
    Dim sql_user_settings As String = Nothing                       'SQL string
    Dim field As String = gdx_main.cmbbox_searchby.SelectedItem     'get search variables
    Private tooltip = New ToolTip()                                 'tooltip load

    'SEAERCH PARAMETERS
    Dim excludeCombo As String = "no"                             'exclude combo stores
    Dim excludeClosed As String = "no"                            'exclude closed stores
    Dim comboOnly As String = "no"                                'show combo only
    Dim closedOnly As String = "no"                               'show closed only
    Dim remoteOnly As String = "no"                               'show remote only
    Dim tempOnly As String = "no"                                 'show temp only

    'ON LOAD
    Private Sub searchby_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Me.Text = "GDnetworks - search by " & field.ToUpper()
        lbl_searchby_field_load.Text = field.ToUpper()
        lbl_searchby_value_load.Text = gdx_main.search_value


        'DISABLE EXPORT for noaccess
        If gdx_main.user_level = "noaccess" Or gdx_main.accountRole = "SAM" Then
            btn_searchby_save.Enabled = False
        Else
            btn_searchby_save.Enabled = True
        End If

        'load tooltips
        tooltip.SetToolTip(chkbox_searchby_combo, "If checked, will exclude COMBO duplicates (Dynamite COMBO stores)")

        'search DB with parameters (display all stores: ALL COMBO, ALL OPEN/CLOSED, ALL REMOTE/NO, ALL TEMP/MAIN)
        getResults(excludeCombo, excludeClosed, comboOnly, remoteOnly, tempOnly, closedOnly)

    End Sub


    'get search results
    Private Sub getResults(excludeCombo As String, excludeClosed As String, comboOnly As String, remoteOnly As String, tempOnly As String, closedOnly As String)

        'check if excludeCombo is true
        If excludeCombo = "yes" Then

            'GET SELECTIONS FOR BY GROUP
            If field = "Group" Then

                'check if excludeClosed stores is TRUE
                If excludeClosed = "yes" Then

                    If chkbox_searchby_onlycombo.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and combo = 1 and Province in ('NS','NB','PEI','NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and combo = 1 and Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and combo = 1 and Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and combo = 1 and Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and combo = 1 and Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and combo = 1 and Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and combo = 1 and Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and combo = 1 and Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_remoteonly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and Province in ('NS','NB','PEI','NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_temponly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and Province in ('NS','NB','PEI','NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    Else

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Province in ('NS','NB','PEI','NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    End If

                Else

                    If chkbox_searchby_onlycombo.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and combo = 1 and Province in ('NS','NB','PEI','NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and combo = 1 and Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and combo = 1 and Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and combo = 1 and Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and combo = 1 and Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and combo = 1 and Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and combo = 1 and Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and combo = 1 and Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_remoteonly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and Province in ('NS','NB','PEI','NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_temponly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and Province in ('NS','NB','PEI','NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_closedonly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and Province in ('NS','NB','PEI','NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    Else

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Province in ('NS','NB','PEI','NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    End If

                End If

            ElseIf field = "Province" Then

                'pattern for 2 didget provinces (canada only)
                Dim pattern As String = "^[A-Za-z]{2}$"

                If Regex.IsMatch(gdx_main.search_value, pattern) Or gdx_main.search_value.Contains("pei") Or gdx_main.search_value.Contains("nfld") Then
                    If excludeClosed = "yes" Then

                        If chkbox_searchby_onlycombo.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and COMBO = 1 and " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        ElseIf chkbox_searchby_remoteonly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        ElseIf chkbox_searchby_temponly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        Else
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        End If

                    Else

                        If chkbox_searchby_onlycombo.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and COMBO = 1 and " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        ElseIf chkbox_searchby_remoteonly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        ElseIf chkbox_searchby_temponly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        ElseIf chkbox_searchby_closedonly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        Else
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        End If

                    End If
                Else
                    If excludeClosed = "yes" Then

                        If chkbox_searchby_onlycombo.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and COMBO = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        ElseIf chkbox_searchby_remoteonly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        ElseIf chkbox_searchby_temponly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        Else
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        End If

                    Else

                        If chkbox_searchby_onlycombo.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and COMBO = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        ElseIf chkbox_searchby_remoteonly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        ElseIf chkbox_searchby_temponly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        ElseIf chkbox_searchby_closedonly.Checked Then
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        Else
                            sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        End If

                    End If

                End If

            Else

                If excludeClosed = "yes" Then

                    If chkbox_searchby_onlycombo.Checked Then
                        sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and COMBO = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    ElseIf chkbox_searchby_remoteonly.Checked Then
                        sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and Remote = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    ElseIf chkbox_searchby_temponly.Checked Then
                        sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and TEMP = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    Else
                        sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    End If

                Else

                    If chkbox_searchby_onlycombo.Checked Then
                        sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and COMBO = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    ElseIf chkbox_searchby_remoteonly.Checked Then
                        sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Remote = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    ElseIf chkbox_searchby_temponly.Checked Then
                        sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and TEMP = 1 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    ElseIf chkbox_searchby_closedonly.Checked Then
                        sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and Status = 0 and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    Else
                        sql_user_settings = "Select * from stores_info where Banner not like '%Dynamite Combo%' and " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    End If

                End If

            End If

        Else

            'GET SELECTIONS FOR BY GROUP
            If field = "Group" Then

                If excludeClosed = "yes" Then


                    If chkbox_searchby_onlycombo.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Province in ('NS', 'NB', 'PEI', 'NFLD') and COMBO = 1 Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Garage%' and COMBO = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Dynamite%' and COMBO = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Garage%' and COMBO = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Dynamite%' and COMBO = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Province in ('%Manitoba%', '%Saskatchewan%', '%Alberta%', 'BC') and COMBO = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Country like '%US%' and COMBO = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Country like '%Canada%' and COMBO = 1 and Status = 1 order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_remoteonly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Province in ('NS', 'NB', 'PEI', 'NFLD') and Remote = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Garage%' and Remote = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Dynamite%' and Remote = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Garage%' and Remote = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Dynamite%' and Remote = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Province in ('%Manitoba%', '%Saskatchewan%', '%Alberta%', 'BC') and Remote = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Country like '%US%' and Remote = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Country like '%Canada%' and Remote = 1 and Status = 1 order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_temponly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Province in ('NS', 'NB', 'PEI', 'NFLD') and TEMP = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Garage%' and TEMP = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Dynamite%' and TEMP = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Garage%' and TEMP = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Dynamite%' and TEMP = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Province in ('%Manitoba%', '%Saskatchewan%', '%Alberta%', 'BC') and TEMP = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Country like '%US%' and TEMP = 1 and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Country like '%Canada%' and TEMP = 1 and Status = 1 order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    Else

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Province in ('NS', 'NB', 'PEI', 'NFLD') and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Garage%' and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Dynamite%' and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Garage%' and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Dynamite%' and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Province in ('%Manitoba%', '%Saskatchewan%', '%Alberta%', 'BC') and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Country like '%US%' and Status = 1 order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Country like '%Canada%' and Status = 1 order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    End If

                Else


                    If chkbox_searchby_onlycombo.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Province in ('NS', 'NB', 'PEI', 'NFLD') and COMBO = 1 order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Garage%' and COMBO = 1 order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Dynamite%' and COMBO = 1 order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Garage%' and COMBO = 1 order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Dynamite%' and COMBO = 1 order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') and COMBO = 1  order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Country like '%US%' and COMBO = 1 order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Country like '%Canada%' and COMBO = 1 order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_remoteonly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Province in ('NS', 'NB', 'PEI', 'NFLD') and Remote = 1 order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Garage%' and Remote = 1 order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Dynamite%' and Remote = 1 order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Garage%' and Remote = 1 order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Dynamite%' and Remote = 1 order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') and Remote = 1 order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Country like '%US%' and Remote = 1 order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Country like '%Canada%' and Remote = 1 order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_temponly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Province in ('NS', 'NB', 'PEI', 'NFLD') and TEMP = 1 order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Garage%' and TEMP = 1 order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Dynamite%' and TEMP = 1 order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Garage%' and TEMP = 1 order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Dynamite%' and TEMP = 1 order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') and TEMP = 1 order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Country like '%US%' and TEMP = 1 order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Country like '%Canada%' and TEMP = 1 order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    ElseIf chkbox_searchby_closedonly.Checked Then

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Province in ('NS', 'NB', 'PEI', 'NFLD') and Status = 0 order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Garage%' and Status = 0 order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Dynamite%' and Status = 0 order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Garage%' and Status = 0 order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Dynamite%' and Status = 0 order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') and Status = 0 order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Country like '%US%' and Status = 0 order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Country like '%Canada%' and Status = 0 order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    Else

                        If gdx_main.search_value.Contains("MT") Or gdx_main.search_value.Contains("mt") Then
                            sql_user_settings = "Select * from stores_info where Province in ('NS', 'NB', 'PEI', 'NFLD') order by Store"
                        ElseIf gdx_main.search_value = "GQ" Or gdx_main.search_value.Contains("gq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DQ" Or gdx_main.search_value.Contains("dq") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Quebec%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "GO" Or gdx_main.search_value.Contains("go") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Garage%' order by Store"
                        ElseIf gdx_main.search_value = "DO" Or gdx_main.search_value.Contains("do") Then
                            sql_user_settings = "Select * from stores_info where Province like '%Ontario%' and Banner like '%Dynamite%' order by Store"
                        ElseIf gdx_main.search_value = "WC" Or gdx_main.search_value.Contains("wc") Then
                            sql_user_settings = "Select * from stores_info where Province in ('%Manitoba%', 'Saskatchewan', '%Alberta%', 'BC') order by Store"
                        ElseIf gdx_main.search_value = "US" Or gdx_main.search_value.Contains("us") Then
                            sql_user_settings = "Select * from stores_info where Country like '%US%' order by Store"
                        ElseIf gdx_main.search_value = "CA" Or gdx_main.search_value.Contains("ca") Or gdx_main.search_value.Contains("canada") Then
                            sql_user_settings = "Select * from stores_info where Country like '%Canada%' order by Store"
                        Else
                            MsgBox("Group does not exist!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [wrong group]")
                            gdx_main.txtbox_storeno.ResetText()
                            Me.Dispose()
                            Exit Sub
                        End If

                    End If

                End If

            ElseIf field = "Province" Then

                'pattern for 2 didget provinces (canada only)
                Dim pattern As String = "^[A-Za-z]{2}$"

                If Regex.IsMatch(gdx_main.search_value, pattern) Or gdx_main.search_value.Contains("pei") Or gdx_main.search_value.Contains("nfld") Then
                    If excludeClosed = "yes" Then

                        If chkbox_searchby_onlycombo.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " = '" & gdx_main.search_value & "' and COMBO = 1 and Country = 'Canada' and Status = 1 order by Store"
                        ElseIf chkbox_searchby_remoteonly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " = '" & gdx_main.search_value & "' and Remote = 1 and Country = 'Canada' and Status = 1 order by Store"
                        ElseIf chkbox_searchby_temponly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " = '" & gdx_main.search_value & "' and TEMP = 1 and Country = 'Canada' and Status = 1 order by Store"
                        Else
                            sql_user_settings = "Select * from stores_info where " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' and Status = 1 order by Store"
                        End If

                    Else

                        If chkbox_searchby_onlycombo.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " = '" & gdx_main.search_value & "' and COMBO = 1 and Country = 'Canada' order by Store"
                        ElseIf chkbox_searchby_remoteonly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " = '" & gdx_main.search_value & "' and Remote = 1 and Country = 'Canada' order by Store"
                        ElseIf chkbox_searchby_temponly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " = '" & gdx_main.search_value & "' and TEMP = 1 and Country = 'Canada' order by Store"
                        ElseIf chkbox_searchby_closedonly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " = '" & gdx_main.search_value & "' and Status = 0 and Country = 'Canada' order by Store"
                        Else
                            sql_user_settings = "Select * from stores_info where " & field & " = '" & gdx_main.search_value & "' and Country = 'Canada' order by Store"
                        End If

                    End If

                Else
                    If excludeClosed = "yes" Then

                        If chkbox_searchby_onlycombo.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and COMBO = 1 and Status = 1 order by Store"
                        ElseIf chkbox_searchby_remoteonly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and Remote = 1 and Status = 1 order by Store"
                        ElseIf chkbox_searchby_temponly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and TEMP = 1 and Status = 1 order by Store"
                        Else
                            sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and Status = 1 order by Store"
                        End If

                    Else

                        If chkbox_searchby_onlycombo.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and COMBO = 1 order by Store"
                        ElseIf chkbox_searchby_remoteonly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and Remote = 1 order by Store"
                        ElseIf chkbox_searchby_temponly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and TEMP = 1 order by Store"
                        ElseIf chkbox_searchby_closedonly.Checked Then
                            sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and Status = 0 order by Store"
                        Else
                            sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                        End If

                    End If

                End If

            Else

                If excludeClosed = "yes" Then

                    If chkbox_searchby_onlycombo.Checked Then
                        sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and COMBO = 1 and Status = 1 order by Store"
                    ElseIf chkbox_searchby_remoteonly.Checked Then
                        sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and Remote = 1 and Status = 1 order by Store"
                    ElseIf chkbox_searchby_temponly.Checked Then
                        sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and TEMP = 1 and Status = 1 order by Store"
                    Else
                        sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and Status = 1 order by Store"
                    End If

                Else

                    If chkbox_searchby_onlycombo.Checked Then
                        sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and COMBO = 1 order by Store"
                    ElseIf chkbox_searchby_remoteonly.Checked Then
                        sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and Remote = 1 order by Store"
                    ElseIf chkbox_searchby_temponly.Checked Then
                        sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and TEMP = 1 order by Store"
                    ElseIf chkbox_searchby_closedonly.Checked Then
                        sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' and Status = 0 order by Store"
                    Else
                        sql_user_settings = "Select * from stores_info where " & field & " like '%" & gdx_main.search_value & "%' order by Store"
                    End If

                End If

            End If

        End If


        'connection settings
        Dim usr_connection As SqlConnection
        Dim cmd_usr As SqlCommand
        Dim userReader As SqlDataReader
        usr_connection = New SqlConnection(gdx_main.connectionString)
        cmd_usr = New SqlCommand(sql_user_settings, usr_connection)

        Try
            usr_connection.Open()
            userReader = cmd_usr.ExecuteReader

            'GET USERS SETTINGS INTO LIST
            If userReader.HasRows Then
                While userReader.Read
                    Dim newitem As New ListViewItem()
                    'newitem.UseItemStyleForSubItems = False
                    newitem.Text = userReader.GetValue(0).ToString                                 'Store#
                    newitem.SubItems.Add(userReader.GetValue(1).ToString)                          'Banner
                    isCombo = userReader.GetValue(2).ToString                                      'isCombo                  
                    If IsDBNull(userReader.GetValue(6)) Then                                       'Sister Store
                        newitem.SubItems.Add("N/A")
                    Else
                        newitem.SubItems.Add(userReader.GetValue(6).ToString)
                    End If
                    If userReader.GetValue(4) = True Then                                          'store status
                        newitem.SubItems.Add("OPEN")
                    Else
                        newitem.SubItems.Add("CLOSED")
                    End If
                    newitem.SubItems.Add(userReader.GetValue(7).ToString)                          'Mall
                    newitem.SubItems.Add(userReader.GetValue(8).ToString)                          'Address
                    newitem.SubItems.Add(userReader.GetValue(9).ToString)                          'City
                    newitem.SubItems.Add(userReader.GetValue(10).ToString)                         'Province
                    newitem.SubItems.Add(userReader.GetValue(11).ToString)                         'Postal Code
                    newitem.SubItems.Add(userReader.GetValue(12).ToString)                         'Country                  

                    'add
                    listview_stores.Items.Add(newitem).ToString()
                End While
            End If

            usr_connection.Close()

            'autosize columns & header
            listview_stores.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
            listview_stores.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Information, Title:="GDnetworks - Info! [DB: searchby]")
        End Try

        'listview_stores.Sorting = SortOrder.Ascending

        lbl_searchby_nstores.Text = "Found " & listview_stores.Items.Count & " stores"

    End Sub

    Private Sub listview_stores_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listview_stores.SelectedIndexChanged

    End Sub

    'BUTTON: CLOSE
    Private Sub btn_searchby_close_Click(sender As Object, e As EventArgs) Handles btn_searchby_close.Click
        MyBase.Close()
    End Sub

    'ON CLOSING DISPOSE
    Private Sub searchby_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        gdx_main.cmbbox_searchby.SelectedItem = "Store"
        Me.Dispose()
    End Sub

    'BUTTON: REFRESH
    Private Sub btn_searchby_refresh_Click(sender As Object, e As EventArgs) Handles btn_searchby_refresh.Click

        'EXCLUDE DUPLICATE COMBO (yes/no)
        If chkbox_searchby_combo.Checked Then
            excludeCombo = "yes"
        Else
            excludeCombo = "no"
        End If

        'EXCLUDE CLOSED
        If chkbox_searchby_status.Checked Then
            excludeClosed = "yes"
        Else
            excludeClosed = "no"
        End If

        'COMBO ONLY
        If chkbox_searchby_onlycombo.Checked Then
            comboOnly = "yes"
        Else
            comboOnly = "no"
        End If

        'REMOTE ONLY
        If chkbox_searchby_remoteonly.Checked Then
            remoteOnly = "yes"
        Else
            remoteOnly = "no"
        End If

        'TEMP ONLY
        If chkbox_searchby_temponly.Checked Then
            tempOnly = "yes"
        Else
            tempOnly = "no"
        End If

        'CLOSED ONLY
        If chkbox_searchby_closedonly.Checked Then
            closedOnly = "yes"
        Else
            closedOnly = "no"
        End If

        'perform check (cannot select Exclude CONSED & Closed in the same time)
        If chkbox_searchby_status.Checked And chkbox_searchby_closedonly.Checked Then
            MsgBox("Cannot exclude CLOSED stores while searching for CLOSED only stores! Try again.", MsgBoxStyle.Information, Title:="GDnetworks - Info! (searchby closed)")
            chkbox_searchby_closedonly.CheckState = False
            chkbox_searchby_status.CheckState = False
        Else

            'reset list to 0
            listview_stores.Items.Clear()

            'get new results from DB
            getResults(excludeCombo, excludeClosed, comboOnly, remoteOnly, tempOnly, closedOnly)

        End If

    End Sub

    'BUTTON: SAVE TO FILE
    Private Sub btn_searchby_save_Click(sender As Object, e As EventArgs) Handles btn_searchby_save.Click

        Dim sfd As New SaveFileDialog With {
            .Title = "Select file location",
            .FileName = "GDnReports_storelist.csv",
            .Filter = "CSV (*.csv)|*.csv",
            .FilterIndex = 0,
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)}

        'save CSV fIle 

        If sfd.ShowDialog = DialogResult.OK Then

            Dim headers = (From ch In listview_stores.Columns Let header = DirectCast(ch, ColumnHeader) Select header.Text).ToArray()

            Dim items() = (From item In listview_stores.Items Let lvi = DirectCast(item, ListViewItem) Select (From subitem In lvi.SubItems
                                                                                                               Let si = DirectCast(subitem, ListViewItem.ListViewSubItem)
                                                                                                               Select si.Text).ToArray()).ToArray()

            Dim table As String = String.Join(",", headers) & Environment.NewLine

            For Each a In items
                table &= String.Join(",", a) & Environment.NewLine
            Next

            table = table.TrimEnd(CChar(vbCr), CChar(vbLf))
            IO.File.WriteAllText(sfd.FileName, table)

        End If

    End Sub


    'CHECKBOX COMBO ONLY ENABLED
    Private Sub chkbox_searchby_onlycombo_CheckedChanged(sender As Object, e As EventArgs) Handles chkbox_searchby_onlycombo.CheckedChanged


        If chkbox_searchby_onlycombo.Checked Then
            chkbox_searchby_remoteonly.Enabled = False
            chkbox_searchby_temponly.Enabled = False
            chkbox_searchby_closedonly.Enabled = False
        Else
            chkbox_searchby_remoteonly.Enabled = True
            chkbox_searchby_temponly.Enabled = True
            chkbox_searchby_closedonly.Enabled = True
        End If

    End Sub

    'CHECKBOX REMOTE ONLY ENABLED
    Private Sub chkbox_searchby_remoteonly_CheckedChanged(sender As Object, e As EventArgs) Handles chkbox_searchby_remoteonly.CheckedChanged


        If chkbox_searchby_remoteonly.Checked Then
            chkbox_searchby_onlycombo.Enabled = False
            chkbox_searchby_temponly.Enabled = False
            chkbox_searchby_closedonly.Enabled = False
        Else
            chkbox_searchby_onlycombo.Enabled = True
            chkbox_searchby_temponly.Enabled = True
            chkbox_searchby_closedonly.Enabled = True
        End If

    End Sub

    'CHECKBOX TEMP ONLY ENABLED
    Private Sub chkbox_searchby_temponly_CheckedChanged(sender As Object, e As EventArgs) Handles chkbox_searchby_temponly.CheckedChanged

        If chkbox_searchby_temponly.Checked Then
            chkbox_searchby_onlycombo.Enabled = False
            chkbox_searchby_remoteonly.Enabled = False
            chkbox_searchby_closedonly.Enabled = False
        Else
            chkbox_searchby_onlycombo.Enabled = True
            chkbox_searchby_remoteonly.Enabled = True
            chkbox_searchby_closedonly.Enabled = True
        End If

    End Sub

    'CHECKBOX CLOSED ONLY ENABLED
    Private Sub chkbox_searchby_closedonly_CheckedChanged(sender As Object, e As EventArgs) Handles chkbox_searchby_closedonly.CheckedChanged

        If chkbox_searchby_closedonly.Checked Then
            chkbox_searchby_remoteonly.Enabled = False
            chkbox_searchby_temponly.Enabled = False
            chkbox_searchby_onlycombo.Enabled = False
            chkbox_searchby_status.Enabled = False
            chkbox_searchby_status.CheckState = False
        Else
            chkbox_searchby_remoteonly.Enabled = True
            chkbox_searchby_temponly.Enabled = True
            chkbox_searchby_onlycombo.Enabled = True
            chkbox_searchby_status.Enabled = True
        End If

    End Sub

    'CHECKBOX EXCLUDE CLOSED
    Private Sub chkbox_searchby_status_CheckedChanged(sender As Object, e As EventArgs) Handles chkbox_searchby_status.CheckedChanged

        If chkbox_searchby_status.Checked Then
            chkbox_searchby_closedonly.Enabled = False
            chkbox_searchby_closedonly.CheckState = False
        Else
            chkbox_searchby_closedonly.Enabled = True
        End If

    End Sub


    'open store (double click item)
    Private Sub listview_stores_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles listview_stores.MouseDoubleClick

        Me.Dispose()
        gdx_main.txtbox_storeno.Text = listview_stores.SelectedItems(0).SubItems(0).Text
        gdx_main.cmbbox_searchby.SelectedItem = "Store"
        gdx_main.btn_search_store.PerformClick()

    End Sub

    'SORT LIST USING admin_users_compare CLASS
    Private Sub Listview_users_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles listview_stores.ColumnClick
        Dim sortColumn As Integer = -1

        If e.Column <> sortColumn Then
            ' Set the sort column to the new column.
            sortColumn = e.Column
            ' Set the sort order to ascending by default.
            listview_stores.Sorting = SortOrder.Ascending
        Else
            ' Determine what the last sort order was and change it.
            If listview_stores.Sorting = SortOrder.Ascending Then
                listview_stores.Sorting = SortOrder.Descending
            Else
                listview_stores.Sorting = SortOrder.Ascending
            End If
        End If

        listview_stores.ListViewItemSorter = New ListViewItemCompare(e.Column, listview_stores.Sorting)
        listview_stores.Sort()

    End Sub

End Class