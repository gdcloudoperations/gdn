﻿'AUTHOR: Alex Dumitrascu
'DATE: 08-01-2018
'UPDATE: 15-02-2018
'
'FORM: EditUser.vb (access from admin.vb)

Public Class EditUser

    'LOCAL VARS
    'Dim selected_user As String = Nothing       'user selected from user table
    Dim user_status As String = Nothing         'selected user status
    Dim load_role As String = Nothing           'selected user role
    Dim load_status As String = Nothing         'user status to string
    Dim errors As String = Nothing          'store error msg

    'ON LOAD: EditUser.vb
    Private Sub EditUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'get username
        admin.selected_user = admin.listview_users.SelectedItems(0).SubItems(1).Text
        Dim load_info() = Split(gdx_main.read_user(admin.selected_user), "|")         'preload user role and status
        'get user role
        load_role = load_info(1)
        'get user status
        load_status = load_info(2)

        'LOAD info
        txtbox_edituser_usr_load.Text = admin.selected_user
        combo_edituser_role.Text = load_role

        If load_status = "Enabled" Then
            radio_edituser_enabled.Checked = True
        ElseIf load_status = "Disabled" Then
            radio_edituser_disabled.Checked = True
        End If

    End Sub

    'BUTTON: ON CLOSE
    Private Sub EditUser_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Me.Dispose()
    End Sub

    'BUTTON: CANCEL - close form
    Private Sub btn_edituser_cancel_Click(sender As Object, e As EventArgs) Handles btn_edituser_cancel.Click
        Me.Dispose()
    End Sub

    'BUTTON: SAVE - update user
    Private Sub btn_edituser_save_Click(sender As Object, e As EventArgs) Handles btn_edituser_save.Click

        'Check if username is missing
        If txtbox_edituser_usr_load.Text = "" Then
            errors = errors & "Missing Username!" & vbNewLine
        Else
            errors = errors & ""
        End If

        'check if user-role is missing
        If combo_edituser_role.SelectedItem = "" Then
            errors = errors & "Missing User-role!" & vbNewLine
        Else
            errors = errors & ""
        End If

        'check user status
        If radio_edituser_enabled.Checked Then
            user_status = "Enabled"
            errors = errors & ""
        ElseIf radio_edituser_disabled.Checked Then
            user_status = "Disabled"
            errors = errors & ""
        Else
            user_status = Nothing
            errors = errors & "Missing user status (enabled / disabled)" & vbNewLine
        End If

        If Not errors = "" Then
            MsgBox("Missing information: " & vbNewLine & vbNewLine & errors, MsgBoxStyle.Exclamation, Title:="GDnetworks - Error! (not completed)")
            errors = Nothing
        Else

            If gdx_main.user_level = "limited+" And Not combo_edituser_role.SelectedItem = "SAM" And Not combo_edituser_role.SelectedItem = "Guest" Then
                MsgBox("You don't have enough rights to create a user with this role.", MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            ElseIf gdx_main.user_level = "general+" And Not combo_edituser_role.SelectedItem = "SAM" And Not combo_edituser_role.SelectedItem = "Tech Support" And Not combo_edituser_role.SelectedItem = "Guest" Then
                MsgBox("You don't have enough rights to create a user with this role.", MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            ElseIf gdx_main.user_level = "elevated" And combo_edituser_role.SelectedItem = "*SYS" Then
                MsgBox("You don't have enough rights to create a user with this role.", MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            Else

                'UPLOAD INFORMATION
                gdx_main.update_user(admin.selected_user, "Username", txtbox_edituser_usr_load.Text)
                gdx_main.update_user(admin.selected_user, "Role", combo_edituser_role.SelectedItem)
                gdx_main.update_user(admin.selected_user, "Status", user_status)

                MsgBox(txtbox_edituser_usr_load.Text & " updated successfully!" & vbCr & "Please refresh the user list.", MsgBoxStyle.Information, Title:="GDnetworks - Succes! (update user)")
                Me.Dispose()

            End If

        End If

    End Sub

    Private Sub lbl_edituser_details_Click(sender As Object, e As EventArgs) Handles lbl_edituser_details.Click

    End Sub
End Class