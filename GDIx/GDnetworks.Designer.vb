﻿Imports System.Windows.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class gdx_main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(gdx_main))
        Me.top_menu = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LABSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PRODToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReloadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StoreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteStoreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AddToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalendarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MyCalendarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DBAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ORPOSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TechSupportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ServerSupportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CMRToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HWPicsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InventoryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OnMapToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProdcoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrafficCounterToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CameraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RSGToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrackingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CanadaPostToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CanparToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FedExToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PurolatorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UPSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StorePWDToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupportDocsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GDriveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JiraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LocalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WebToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GarageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DynamiteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DynamiteCAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DynamiteUSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GRPDYNToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BellDialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BeehivrToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IPadSignUPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MerakiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MobiControlToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OrionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintersToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CUPSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BIpublisherToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoteDesktopToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RMSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnlockStoreAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SpeedtestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListSpeedtestByStoreToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RunSpeedtestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TeamViewerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WeblogicToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnlockPOSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AutoUpdateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OnToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OffToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BrowserToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChromeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PSEXECToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RDPToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TeamViewerToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RefreshRateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.topmenu_refresh_0sec = New System.Windows.Forms.ToolStripMenuItem()
        Me.topmenu_refresh_10sec = New System.Windows.Forms.ToolStripMenuItem()
        Me.topmenu_refresh_30sec = New System.Windows.Forms.ToolStripMenuItem()
        Me.topmenu_refresh_60sec = New System.Windows.Forms.ToolStripMenuItem()
        Me.SavePositionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupportLocationsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpF1ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.UpdatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.KeyboardShortcutsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SupportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.grp_search = New System.Windows.Forms.GroupBox()
        Me.cmbbox_searchby = New System.Windows.Forms.ComboBox()
        Me.btn_clear_store = New System.Windows.Forms.Button()
        Me.btn_search_store = New System.Windows.Forms.Button()
        Me.txtbox_storeno = New System.Windows.Forms.TextBox()
        Me.grp_store = New System.Windows.Forms.GroupBox()
        Me.lbl_str_system_load = New System.Windows.Forms.Label()
        Me.lbl_str_system = New System.Windows.Forms.Label()
        Me.lbl_store_iscombo_load = New System.Windows.Forms.Label()
        Me.lbl_store_iscombo = New System.Windows.Forms.Label()
        Me.lbl_district_load = New System.Windows.Forms.Label()
        Me.lbl_mainl_load = New System.Windows.Forms.Label()
        Me.lbl_country_load = New System.Windows.Forms.Label()
        Me.lbl_postal_load = New System.Windows.Forms.Label()
        Me.lbl_state_load = New System.Windows.Forms.Label()
        Me.lbl_city_load = New System.Windows.Forms.Label()
        Me.lbl_address_load = New System.Windows.Forms.Label()
        Me.lbl_mall_load = New System.Windows.Forms.Label()
        Me.lbl_sis_store_load = New System.Windows.Forms.Label()
        Me.lbl_ban_load = New System.Windows.Forms.Label()
        Me.lbl_strno_load = New System.Windows.Forms.Label()
        Me.lbl_str_district = New System.Windows.Forms.Label()
        Me.lbl_str_ph = New System.Windows.Forms.Label()
        Me.lbl_str_cntry = New System.Windows.Forms.Label()
        Me.lbl_str_pcode = New System.Windows.Forms.Label()
        Me.lbl_str_prov = New System.Windows.Forms.Label()
        Me.lbl_str_city = New System.Windows.Forms.Label()
        Me.lbl_str_add = New System.Windows.Forms.Label()
        Me.lbl_str_mall = New System.Windows.Forms.Label()
        Me.lbl_str_sisstr = New System.Windows.Forms.Label()
        Me.lbl_str_banner = New System.Windows.Forms.Label()
        Me.lbl_str_no = New System.Windows.Forms.Label()
        Me.grp_network_top = New System.Windows.Forms.GroupBox()
        Me.lbl_switch2_load = New System.Windows.Forms.Label()
        Me.lbl_switch1_load = New System.Windows.Forms.Label()
        Me.lbl_router_load = New System.Windows.Forms.Label()
        Me.btn_gui_sw2 = New System.Windows.Forms.Button()
        Me.btn_gui_sw1 = New System.Windows.Forms.Button()
        Me.btn_cli_sw2 = New System.Windows.Forms.Button()
        Me.btn_cli_sw1 = New System.Windows.Forms.Button()
        Me.lbl_net_sw2 = New System.Windows.Forms.Label()
        Me.lbl_net_sw1 = New System.Windows.Forms.Label()
        Me.lbl_net_rtr = New System.Windows.Forms.Label()
        Me.btn_gui_router = New System.Windows.Forms.Button()
        Me.btn_cli_router = New System.Windows.Forms.Button()
        Me.grp_devices = New System.Windows.Forms.GroupBox()
        Me.lbl_samsung_load = New System.Windows.Forms.Label()
        Me.lbl_ipad_load = New System.Windows.Forms.Label()
        Me.lbl_music_load = New System.Windows.Forms.Label()
        Me.lbl_tc_load = New System.Windows.Forms.Label()
        Me.lbl_ap2_load = New System.Windows.Forms.Label()
        Me.lbl_ap1_load = New System.Windows.Forms.Label()
        Me.lbl_stbl = New System.Windows.Forms.Label()
        Me.lbl_ipad = New System.Windows.Forms.Label()
        Me.lbl_music = New System.Windows.Forms.Label()
        Me.lbl_tc = New System.Windows.Forms.Label()
        Me.lbl_ap2 = New System.Windows.Forms.Label()
        Me.lbl_ap1 = New System.Windows.Forms.Label()
        Me.grp_cashes = New System.Windows.Forms.GroupBox()
        Me.lbl_bo_load = New System.Windows.Forms.Label()
        Me.lbl_cash6_load = New System.Windows.Forms.Label()
        Me.lbl_cash5_load = New System.Windows.Forms.Label()
        Me.lbl_cash4_load = New System.Windows.Forms.Label()
        Me.lbl_cash3_load = New System.Windows.Forms.Label()
        Me.lbl_cash2_load = New System.Windows.Forms.Label()
        Me.lbl_cash1_load = New System.Windows.Forms.Label()
        Me.btn_tv_bo = New System.Windows.Forms.Button()
        Me.btn_tv_c6 = New System.Windows.Forms.Button()
        Me.btn_tv_c5 = New System.Windows.Forms.Button()
        Me.btn_tv_c4 = New System.Windows.Forms.Button()
        Me.btn_tv_c3 = New System.Windows.Forms.Button()
        Me.btn_tv_c2 = New System.Windows.Forms.Button()
        Me.btn_tv_c1 = New System.Windows.Forms.Button()
        Me.lbl_bo = New System.Windows.Forms.Label()
        Me.lbl_cash6 = New System.Windows.Forms.Label()
        Me.lbl_cash5 = New System.Windows.Forms.Label()
        Me.lbl_cash4 = New System.Windows.Forms.Label()
        Me.lbl_cash3 = New System.Windows.Forms.Label()
        Me.lbl_cash2 = New System.Windows.Forms.Label()
        Me.lbl_cash1 = New System.Windows.Forms.Label()
        Me.grp_moreinfo = New System.Windows.Forms.GroupBox()
        Me.lbl_wifi_load = New System.Windows.Forms.Label()
        Me.lbl_store_speed_load = New System.Windows.Forms.Label()
        Me.lbl_spare_equip_load = New System.Windows.Forms.Label()
        Me.lbl_remote_load = New System.Windows.Forms.Label()
        Me.lbl_wifi_speed = New System.Windows.Forms.Label()
        Me.lbl_speed = New System.Windows.Forms.Label()
        Me.lbl_spare_equip = New System.Windows.Forms.Label()
        Me.lbl_remote = New System.Windows.Forms.Label()
        Me.btn_moreinfo = New System.Windows.Forms.Button()
        Me.btn_list_status = New System.Windows.Forms.Button()
        Me.btn_store_hours = New System.Windows.Forms.Button()
        Me.ping_timer = New System.Windows.Forms.Timer(Me.components)
        Me.lbl_user_load = New System.Windows.Forms.Label()
        Me.lbl_user = New System.Windows.Forms.Label()
        Me.lbl_refresh = New System.Windows.Forms.Label()
        Me.lbl_refresh_load = New System.Windows.Forms.Label()
        Me.lbl_role = New System.Windows.Forms.Label()
        Me.lbl_role_load = New System.Windows.Forms.Label()
        Me.bgwork_ping = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.lbl_vpn_con = New System.Windows.Forms.Label()
        Me.lbl_vpn_con_load = New System.Windows.Forms.Label()
        Me.btn_cb_address = New System.Windows.Forms.Button()
        Me.app_running_timer = New System.Windows.Forms.Timer(Me.components)
        Me.vpn_timer = New System.Windows.Forms.Timer(Me.components)
        Me.pic_gdx_main = New System.Windows.Forms.PictureBox()
        Me.pic_pinpad_c6 = New System.Windows.Forms.PictureBox()
        Me.pic_pinpad_c5 = New System.Windows.Forms.PictureBox()
        Me.pic_pinpad_c4 = New System.Windows.Forms.PictureBox()
        Me.pic_pinpad_c3 = New System.Windows.Forms.PictureBox()
        Me.pic_pinpad_c2 = New System.Windows.Forms.PictureBox()
        Me.pic_pinpad_c1 = New System.Windows.Forms.PictureBox()
        Me.top_menu.SuspendLayout()
        Me.grp_search.SuspendLayout()
        Me.grp_store.SuspendLayout()
        Me.grp_network_top.SuspendLayout()
        Me.grp_devices.SuspendLayout()
        Me.grp_cashes.SuspendLayout()
        Me.grp_moreinfo.SuspendLayout()
        CType(Me.pic_gdx_main, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pinpad_c6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pinpad_c5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pinpad_c4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pinpad_c3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pinpad_c2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pinpad_c1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'top_menu
        '
        Me.top_menu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ViewToolStripMenuItem, Me.ToolsToolStripMenuItem, Me.OptionsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.top_menu.Location = New System.Drawing.Point(0, 0)
        Me.top_menu.Name = "top_menu"
        Me.top_menu.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.top_menu.Size = New System.Drawing.Size(813, 24)
        Me.top_menu.TabIndex = 0
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ModeToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.ReloadToolStripMenuItem, Me.StoreToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ModeToolStripMenuItem
        '
        Me.ModeToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AdminToolStripMenuItem, Me.LABSToolStripMenuItem, Me.PRODToolStripMenuItem})
        Me.ModeToolStripMenuItem.Name = "ModeToolStripMenuItem"
        Me.ModeToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.ModeToolStripMenuItem.Text = "Mode"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.AdminToolStripMenuItem.Text = "Admin"
        '
        'LABSToolStripMenuItem
        '
        Me.LABSToolStripMenuItem.Name = "LABSToolStripMenuItem"
        Me.LABSToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.LABSToolStripMenuItem.Text = "LABS   F7"
        '
        'PRODToolStripMenuItem
        '
        Me.PRODToolStripMenuItem.Name = "PRODToolStripMenuItem"
        Me.PRODToolStripMenuItem.Size = New System.Drawing.Size(126, 22)
        Me.PRODToolStripMenuItem.Text = "PROD   F8"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.RefreshToolStripMenuItem.Text = "Refresh   F5"
        '
        'ReloadToolStripMenuItem
        '
        Me.ReloadToolStripMenuItem.Name = "ReloadToolStripMenuItem"
        Me.ReloadToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.ReloadToolStripMenuItem.Text = "Reload"
        '
        'StoreToolStripMenuItem
        '
        Me.StoreToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EditToolStripMenuItem, Me.DeleteStoreToolStripMenuItem, Me.AddToolStripMenuItem})
        Me.StoreToolStripMenuItem.Name = "StoreToolStripMenuItem"
        Me.StoreToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.StoreToolStripMenuItem.Text = "Store"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.EditToolStripMenuItem.Text = "Edit Store"
        '
        'DeleteStoreToolStripMenuItem
        '
        Me.DeleteStoreToolStripMenuItem.Name = "DeleteStoreToolStripMenuItem"
        Me.DeleteStoreToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.DeleteStoreToolStripMenuItem.Text = "Delete Store"
        '
        'AddToolStripMenuItem
        '
        Me.AddToolStripMenuItem.Name = "AddToolStripMenuItem"
        Me.AddToolStripMenuItem.Size = New System.Drawing.Size(137, 22)
        Me.AddToolStripMenuItem.Text = "New Store"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.ExitToolStripMenuItem.Text = "Exit   F12"
        '
        'ViewToolStripMenuItem
        '
        Me.ViewToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CalendarToolStripMenuItem, Me.CMRToolStripMenuItem, Me.HWPicsToolStripMenuItem, Me.InventoryToolStripMenuItem, Me.OnMapToolStripMenuItem, Me.ProdcoToolStripMenuItem, Me.RSGToolStripMenuItem, Me.TrackingToolStripMenuItem, Me.StorePWDToolStripMenuItem, Me.SupportDocsToolStripMenuItem, Me.WebToolStripMenuItem})
        Me.ViewToolStripMenuItem.Name = "ViewToolStripMenuItem"
        Me.ViewToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.ViewToolStripMenuItem.Text = "View"
        '
        'CalendarToolStripMenuItem
        '
        Me.CalendarToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MyCalendarToolStripMenuItem, Me.DBAToolStripMenuItem, Me.ORPOSToolStripMenuItem, Me.TechSupportToolStripMenuItem, Me.ServerSupportToolStripMenuItem})
        Me.CalendarToolStripMenuItem.Name = "CalendarToolStripMenuItem"
        Me.CalendarToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.CalendarToolStripMenuItem.Text = "Calendars"
        '
        'MyCalendarToolStripMenuItem
        '
        Me.MyCalendarToolStripMenuItem.Name = "MyCalendarToolStripMenuItem"
        Me.MyCalendarToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.MyCalendarToolStripMenuItem.Text = "Calendar (month)"
        '
        'DBAToolStripMenuItem
        '
        Me.DBAToolStripMenuItem.Name = "DBAToolStripMenuItem"
        Me.DBAToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.DBAToolStripMenuItem.Text = "DBA"
        '
        'ORPOSToolStripMenuItem
        '
        Me.ORPOSToolStripMenuItem.Name = "ORPOSToolStripMenuItem"
        Me.ORPOSToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ORPOSToolStripMenuItem.Text = "ORPOS"
        '
        'TechSupportToolStripMenuItem
        '
        Me.TechSupportToolStripMenuItem.Name = "TechSupportToolStripMenuItem"
        Me.TechSupportToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.TechSupportToolStripMenuItem.Text = "Tech Support"
        '
        'ServerSupportToolStripMenuItem
        '
        Me.ServerSupportToolStripMenuItem.Name = "ServerSupportToolStripMenuItem"
        Me.ServerSupportToolStripMenuItem.Size = New System.Drawing.Size(168, 22)
        Me.ServerSupportToolStripMenuItem.Text = "Server Support"
        '
        'CMRToolStripMenuItem
        '
        Me.CMRToolStripMenuItem.Name = "CMRToolStripMenuItem"
        Me.CMRToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.CMRToolStripMenuItem.Text = "CMR"
        '
        'HWPicsToolStripMenuItem
        '
        Me.HWPicsToolStripMenuItem.Name = "HWPicsToolStripMenuItem"
        Me.HWPicsToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.HWPicsToolStripMenuItem.Text = "HW Pics"
        '
        'InventoryToolStripMenuItem
        '
        Me.InventoryToolStripMenuItem.Name = "InventoryToolStripMenuItem"
        Me.InventoryToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.InventoryToolStripMenuItem.Text = "Inventory"
        '
        'OnMapToolStripMenuItem
        '
        Me.OnMapToolStripMenuItem.Name = "OnMapToolStripMenuItem"
        Me.OnMapToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.OnMapToolStripMenuItem.Text = "onMap"
        '
        'ProdcoToolStripMenuItem
        '
        Me.ProdcoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TrafficCounterToolStripMenuItem, Me.CameraToolStripMenuItem})
        Me.ProdcoToolStripMenuItem.Name = "ProdcoToolStripMenuItem"
        Me.ProdcoToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.ProdcoToolStripMenuItem.Text = "Prodco"
        '
        'TrafficCounterToolStripMenuItem
        '
        Me.TrafficCounterToolStripMenuItem.Name = "TrafficCounterToolStripMenuItem"
        Me.TrafficCounterToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.TrafficCounterToolStripMenuItem.Text = "Dashboard"
        '
        'CameraToolStripMenuItem
        '
        Me.CameraToolStripMenuItem.Enabled = False
        Me.CameraToolStripMenuItem.Name = "CameraToolStripMenuItem"
        Me.CameraToolStripMenuItem.Size = New System.Drawing.Size(131, 22)
        Me.CameraToolStripMenuItem.Text = "Camera"
        '
        'RSGToolStripMenuItem
        '
        Me.RSGToolStripMenuItem.Name = "RSGToolStripMenuItem"
        Me.RSGToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.RSGToolStripMenuItem.Text = "RSG"
        '
        'TrackingToolStripMenuItem
        '
        Me.TrackingToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CanadaPostToolStripMenuItem, Me.CanparToolStripMenuItem, Me.FedExToolStripMenuItem, Me.PurolatorToolStripMenuItem, Me.UPSToolStripMenuItem})
        Me.TrackingToolStripMenuItem.Name = "TrackingToolStripMenuItem"
        Me.TrackingToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.TrackingToolStripMenuItem.Text = "Tracking"
        '
        'CanadaPostToolStripMenuItem
        '
        Me.CanadaPostToolStripMenuItem.Name = "CanadaPostToolStripMenuItem"
        Me.CanadaPostToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.CanadaPostToolStripMenuItem.Text = "Canada Post"
        '
        'CanparToolStripMenuItem
        '
        Me.CanparToolStripMenuItem.Name = "CanparToolStripMenuItem"
        Me.CanparToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.CanparToolStripMenuItem.Text = "Canpar"
        '
        'FedExToolStripMenuItem
        '
        Me.FedExToolStripMenuItem.Name = "FedExToolStripMenuItem"
        Me.FedExToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.FedExToolStripMenuItem.Text = "FedEx"
        '
        'PurolatorToolStripMenuItem
        '
        Me.PurolatorToolStripMenuItem.Name = "PurolatorToolStripMenuItem"
        Me.PurolatorToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.PurolatorToolStripMenuItem.Text = "Purolator"
        '
        'UPSToolStripMenuItem
        '
        Me.UPSToolStripMenuItem.Name = "UPSToolStripMenuItem"
        Me.UPSToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.UPSToolStripMenuItem.Text = "UPS"
        '
        'StorePWDToolStripMenuItem
        '
        Me.StorePWDToolStripMenuItem.Name = "StorePWDToolStripMenuItem"
        Me.StorePWDToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.StorePWDToolStripMenuItem.Text = "Store Gmail Acc."
        '
        'SupportDocsToolStripMenuItem
        '
        Me.SupportDocsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GDriveToolStripMenuItem, Me.JiraToolStripMenuItem, Me.LocalToolStripMenuItem})
        Me.SupportDocsToolStripMenuItem.Name = "SupportDocsToolStripMenuItem"
        Me.SupportDocsToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.SupportDocsToolStripMenuItem.Text = "Support Docs"
        '
        'GDriveToolStripMenuItem
        '
        Me.GDriveToolStripMenuItem.Name = "GDriveToolStripMenuItem"
        Me.GDriveToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
        Me.GDriveToolStripMenuItem.Text = "G! drive"
        '
        'JiraToolStripMenuItem
        '
        Me.JiraToolStripMenuItem.Name = "JiraToolStripMenuItem"
        Me.JiraToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
        Me.JiraToolStripMenuItem.Text = "Jira"
        '
        'LocalToolStripMenuItem
        '
        Me.LocalToolStripMenuItem.Name = "LocalToolStripMenuItem"
        Me.LocalToolStripMenuItem.Size = New System.Drawing.Size(114, 22)
        Me.LocalToolStripMenuItem.Text = "Local"
        '
        'WebToolStripMenuItem
        '
        Me.WebToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GarageToolStripMenuItem, Me.DynamiteToolStripMenuItem, Me.DynamiteCAToolStripMenuItem, Me.DynamiteUSToolStripMenuItem, Me.GRPDYNToolStripMenuItem})
        Me.WebToolStripMenuItem.Name = "WebToolStripMenuItem"
        Me.WebToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.WebToolStripMenuItem.Text = "Web"
        '
        'GarageToolStripMenuItem
        '
        Me.GarageToolStripMenuItem.Name = "GarageToolStripMenuItem"
        Me.GarageToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.GarageToolStripMenuItem.Text = "Garage CA"
        '
        'DynamiteToolStripMenuItem
        '
        Me.DynamiteToolStripMenuItem.Name = "DynamiteToolStripMenuItem"
        Me.DynamiteToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.DynamiteToolStripMenuItem.Text = "Garage US"
        '
        'DynamiteCAToolStripMenuItem
        '
        Me.DynamiteCAToolStripMenuItem.Name = "DynamiteCAToolStripMenuItem"
        Me.DynamiteCAToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.DynamiteCAToolStripMenuItem.Text = "Dynamite CA"
        '
        'DynamiteUSToolStripMenuItem
        '
        Me.DynamiteUSToolStripMenuItem.Name = "DynamiteUSToolStripMenuItem"
        Me.DynamiteUSToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.DynamiteUSToolStripMenuItem.Text = "Dynamite US"
        '
        'GRPDYNToolStripMenuItem
        '
        Me.GRPDYNToolStripMenuItem.Name = "GRPDYNToolStripMenuItem"
        Me.GRPDYNToolStripMenuItem.Size = New System.Drawing.Size(144, 22)
        Me.GRPDYNToolStripMenuItem.Text = "GRP DYN"
        '
        'ToolsToolStripMenuItem
        '
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BellDialToolStripMenuItem, Me.BeehivrToolStripMenuItem, Me.IPadSignUPToolStripMenuItem, Me.MerakiToolStripMenuItem, Me.MobiControlToolStripMenuItem, Me.OrionToolStripMenuItem, Me.PrintersToolStripMenuItem, Me.RemoteDesktopToolStripMenuItem, Me.RMSToolStripMenuItem, Me.SpeedtestToolStripMenuItem, Me.TeamViewerToolStripMenuItem, Me.WeblogicToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        Me.ToolsToolStripMenuItem.Size = New System.Drawing.Size(47, 20)
        Me.ToolsToolStripMenuItem.Text = "Tools"
        '
        'BellDialToolStripMenuItem
        '
        Me.BellDialToolStripMenuItem.Name = "BellDialToolStripMenuItem"
        Me.BellDialToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.BellDialToolStripMenuItem.Text = "Bell Dial System"
        '
        'BeehivrToolStripMenuItem
        '
        Me.BeehivrToolStripMenuItem.Name = "BeehivrToolStripMenuItem"
        Me.BeehivrToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.BeehivrToolStripMenuItem.Text = "Beehivr"
        '
        'IPadSignUPToolStripMenuItem
        '
        Me.IPadSignUPToolStripMenuItem.Name = "IPadSignUPToolStripMenuItem"
        Me.IPadSignUPToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.IPadSignUPToolStripMenuItem.Text = "IPad SignUP"
        '
        'MerakiToolStripMenuItem
        '
        Me.MerakiToolStripMenuItem.Name = "MerakiToolStripMenuItem"
        Me.MerakiToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.MerakiToolStripMenuItem.Text = "Meraki"
        '
        'MobiControlToolStripMenuItem
        '
        Me.MobiControlToolStripMenuItem.Name = "MobiControlToolStripMenuItem"
        Me.MobiControlToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.MobiControlToolStripMenuItem.Text = "MobiControl"
        '
        'OrionToolStripMenuItem
        '
        Me.OrionToolStripMenuItem.Name = "OrionToolStripMenuItem"
        Me.OrionToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.OrionToolStripMenuItem.Text = "Orion"
        '
        'PrintersToolStripMenuItem
        '
        Me.PrintersToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CUPSToolStripMenuItem, Me.BIpublisherToolStripMenuItem})
        Me.PrintersToolStripMenuItem.Name = "PrintersToolStripMenuItem"
        Me.PrintersToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.PrintersToolStripMenuItem.Text = "Printers DB"
        '
        'CUPSToolStripMenuItem
        '
        Me.CUPSToolStripMenuItem.Name = "CUPSToolStripMenuItem"
        Me.CUPSToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.CUPSToolStripMenuItem.Text = "CUPS"
        '
        'BIpublisherToolStripMenuItem
        '
        Me.BIpublisherToolStripMenuItem.Name = "BIpublisherToolStripMenuItem"
        Me.BIpublisherToolStripMenuItem.Size = New System.Drawing.Size(133, 22)
        Me.BIpublisherToolStripMenuItem.Text = "BIpublisher"
        '
        'RemoteDesktopToolStripMenuItem
        '
        Me.RemoteDesktopToolStripMenuItem.Name = "RemoteDesktopToolStripMenuItem"
        Me.RemoteDesktopToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.RemoteDesktopToolStripMenuItem.Text = "Remote Desktop"
        '
        'RMSToolStripMenuItem
        '
        Me.RMSToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UnlockStoreAccountToolStripMenuItem})
        Me.RMSToolStripMenuItem.Name = "RMSToolStripMenuItem"
        Me.RMSToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.RMSToolStripMenuItem.Text = "RMS"
        '
        'UnlockStoreAccountToolStripMenuItem
        '
        Me.UnlockStoreAccountToolStripMenuItem.Name = "UnlockStoreAccountToolStripMenuItem"
        Me.UnlockStoreAccountToolStripMenuItem.Size = New System.Drawing.Size(195, 22)
        Me.UnlockStoreAccountToolStripMenuItem.Text = "Unlock Store2Sore user"
        '
        'SpeedtestToolStripMenuItem
        '
        Me.SpeedtestToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListSpeedtestByStoreToolStripMenuItem, Me.RunSpeedtestToolStripMenuItem})
        Me.SpeedtestToolStripMenuItem.Name = "SpeedtestToolStripMenuItem"
        Me.SpeedtestToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.SpeedtestToolStripMenuItem.Text = "Speedtest"
        '
        'ListSpeedtestByStoreToolStripMenuItem
        '
        Me.ListSpeedtestByStoreToolStripMenuItem.Name = "ListSpeedtestByStoreToolStripMenuItem"
        Me.ListSpeedtestByStoreToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.ListSpeedtestByStoreToolStripMenuItem.Text = "List speedtest by store"
        '
        'RunSpeedtestToolStripMenuItem
        '
        Me.RunSpeedtestToolStripMenuItem.Name = "RunSpeedtestToolStripMenuItem"
        Me.RunSpeedtestToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.RunSpeedtestToolStripMenuItem.Text = "Run speedtest"
        '
        'TeamViewerToolStripMenuItem
        '
        Me.TeamViewerToolStripMenuItem.Name = "TeamViewerToolStripMenuItem"
        Me.TeamViewerToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.TeamViewerToolStripMenuItem.Text = "Team Viewer"
        '
        'WeblogicToolStripMenuItem
        '
        Me.WeblogicToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UnlockPOSToolStripMenuItem})
        Me.WeblogicToolStripMenuItem.Name = "WeblogicToolStripMenuItem"
        Me.WeblogicToolStripMenuItem.Size = New System.Drawing.Size(161, 22)
        Me.WeblogicToolStripMenuItem.Text = "Weblogic"
        '
        'UnlockPOSToolStripMenuItem
        '
        Me.UnlockPOSToolStripMenuItem.Name = "UnlockPOSToolStripMenuItem"
        Me.UnlockPOSToolStripMenuItem.Size = New System.Drawing.Size(136, 22)
        Me.UnlockPOSToolStripMenuItem.Text = "Unlock POS"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AutoUpdateToolStripMenuItem, Me.BrowserToolStripMenuItem, Me.RemoteToolStripMenuItem, Me.RefreshRateToolStripMenuItem, Me.SavePositionToolStripMenuItem, Me.SupportLocationsToolStripMenuItem})
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.OptionsToolStripMenuItem.Text = "Options"
        '
        'AutoUpdateToolStripMenuItem
        '
        Me.AutoUpdateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OnToolStripMenuItem, Me.OffToolStripMenuItem})
        Me.AutoUpdateToolStripMenuItem.Name = "AutoUpdateToolStripMenuItem"
        Me.AutoUpdateToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.AutoUpdateToolStripMenuItem.Text = "Auto update"
        '
        'OnToolStripMenuItem
        '
        Me.OnToolStripMenuItem.Name = "OnToolStripMenuItem"
        Me.OnToolStripMenuItem.Size = New System.Drawing.Size(91, 22)
        Me.OnToolStripMenuItem.Text = "On"
        '
        'OffToolStripMenuItem
        '
        Me.OffToolStripMenuItem.Name = "OffToolStripMenuItem"
        Me.OffToolStripMenuItem.Size = New System.Drawing.Size(91, 22)
        Me.OffToolStripMenuItem.Text = "Off"
        '
        'BrowserToolStripMenuItem
        '
        Me.BrowserToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ChromeToolStripMenuItem, Me.IEToolStripMenuItem})
        Me.BrowserToolStripMenuItem.Name = "BrowserToolStripMenuItem"
        Me.BrowserToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.BrowserToolStripMenuItem.Text = "Browser"
        '
        'ChromeToolStripMenuItem
        '
        Me.ChromeToolStripMenuItem.Name = "ChromeToolStripMenuItem"
        Me.ChromeToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.ChromeToolStripMenuItem.Text = "Chrome"
        '
        'IEToolStripMenuItem
        '
        Me.IEToolStripMenuItem.Name = "IEToolStripMenuItem"
        Me.IEToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.IEToolStripMenuItem.Text = "IExplorer"
        '
        'RemoteToolStripMenuItem
        '
        Me.RemoteToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PSEXECToolStripMenuItem, Me.RDPToolStripMenuItem, Me.TeamViewerToolStripMenuItem1})
        Me.RemoteToolStripMenuItem.Name = "RemoteToolStripMenuItem"
        Me.RemoteToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.RemoteToolStripMenuItem.Text = "Remote"
        '
        'PSEXECToolStripMenuItem
        '
        Me.PSEXECToolStripMenuItem.Name = "PSEXECToolStripMenuItem"
        Me.PSEXECToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.PSEXECToolStripMenuItem.Text = "PSEXEC"
        '
        'RDPToolStripMenuItem
        '
        Me.RDPToolStripMenuItem.Name = "RDPToolStripMenuItem"
        Me.RDPToolStripMenuItem.Size = New System.Drawing.Size(138, 22)
        Me.RDPToolStripMenuItem.Text = "RDP"
        '
        'TeamViewerToolStripMenuItem1
        '
        Me.TeamViewerToolStripMenuItem1.Name = "TeamViewerToolStripMenuItem1"
        Me.TeamViewerToolStripMenuItem1.Size = New System.Drawing.Size(138, 22)
        Me.TeamViewerToolStripMenuItem1.Text = "TeamViewer"
        '
        'RefreshRateToolStripMenuItem
        '
        Me.RefreshRateToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.topmenu_refresh_0sec, Me.topmenu_refresh_10sec, Me.topmenu_refresh_30sec, Me.topmenu_refresh_60sec})
        Me.RefreshRateToolStripMenuItem.Name = "RefreshRateToolStripMenuItem"
        Me.RefreshRateToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.RefreshRateToolStripMenuItem.Text = "Refresh rate"
        '
        'topmenu_refresh_0sec
        '
        Me.topmenu_refresh_0sec.Name = "topmenu_refresh_0sec"
        Me.topmenu_refresh_0sec.Size = New System.Drawing.Size(167, 22)
        Me.topmenu_refresh_0sec.Text = "0sec - no updates"
        '
        'topmenu_refresh_10sec
        '
        Me.topmenu_refresh_10sec.Name = "topmenu_refresh_10sec"
        Me.topmenu_refresh_10sec.Size = New System.Drawing.Size(167, 22)
        Me.topmenu_refresh_10sec.Text = "10sec"
        '
        'topmenu_refresh_30sec
        '
        Me.topmenu_refresh_30sec.Name = "topmenu_refresh_30sec"
        Me.topmenu_refresh_30sec.Size = New System.Drawing.Size(167, 22)
        Me.topmenu_refresh_30sec.Text = "30sec"
        '
        'topmenu_refresh_60sec
        '
        Me.topmenu_refresh_60sec.Name = "topmenu_refresh_60sec"
        Me.topmenu_refresh_60sec.Size = New System.Drawing.Size(167, 22)
        Me.topmenu_refresh_60sec.Text = "60sec"
        '
        'SavePositionToolStripMenuItem
        '
        Me.SavePositionToolStripMenuItem.Name = "SavePositionToolStripMenuItem"
        Me.SavePositionToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.SavePositionToolStripMenuItem.Text = "Save Position   F10"
        '
        'SupportLocationsToolStripMenuItem
        '
        Me.SupportLocationsToolStripMenuItem.Name = "SupportLocationsToolStripMenuItem"
        Me.SupportLocationsToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.SupportLocationsToolStripMenuItem.Text = "Support locations"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpF1ToolStripMenuItem, Me.ToolStripSeparator1, Me.UpdatesToolStripMenuItem, Me.KeyboardShortcutsToolStripMenuItem, Me.SupportToolStripMenuItem, Me.ToolStripSeparator2, Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'HelpF1ToolStripMenuItem
        '
        Me.HelpF1ToolStripMenuItem.Name = "HelpF1ToolStripMenuItem"
        Me.HelpF1ToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.HelpF1ToolStripMenuItem.Text = "Help   F1"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(195, 6)
        '
        'UpdatesToolStripMenuItem
        '
        Me.UpdatesToolStripMenuItem.Name = "UpdatesToolStripMenuItem"
        Me.UpdatesToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.UpdatesToolStripMenuItem.Text = "Check for updates   F11"
        '
        'KeyboardShortcutsToolStripMenuItem
        '
        Me.KeyboardShortcutsToolStripMenuItem.Name = "KeyboardShortcutsToolStripMenuItem"
        Me.KeyboardShortcutsToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.KeyboardShortcutsToolStripMenuItem.Text = "Keyboard Shortcuts   F2"
        '
        'SupportToolStripMenuItem
        '
        Me.SupportToolStripMenuItem.Name = "SupportToolStripMenuItem"
        Me.SupportToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.SupportToolStripMenuItem.Text = "Report errors"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(195, 6)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(198, 22)
        Me.AboutToolStripMenuItem.Text = "About GDnetworks"
        '
        'grp_search
        '
        Me.grp_search.Controls.Add(Me.cmbbox_searchby)
        Me.grp_search.Controls.Add(Me.btn_clear_store)
        Me.grp_search.Controls.Add(Me.btn_search_store)
        Me.grp_search.Controls.Add(Me.txtbox_storeno)
        Me.grp_search.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_search.Location = New System.Drawing.Point(12, 44)
        Me.grp_search.Name = "grp_search"
        Me.grp_search.Size = New System.Drawing.Size(358, 56)
        Me.grp_search.TabIndex = 1
        Me.grp_search.TabStop = False
        Me.grp_search.Text = "Search"
        '
        'cmbbox_searchby
        '
        Me.cmbbox_searchby.FormattingEnabled = True
        Me.cmbbox_searchby.Items.AddRange(New Object() {"Address", "Banner", "Group", "Province", "Store"})
        Me.cmbbox_searchby.Location = New System.Drawing.Point(7, 22)
        Me.cmbbox_searchby.Name = "cmbbox_searchby"
        Me.cmbbox_searchby.Size = New System.Drawing.Size(86, 23)
        Me.cmbbox_searchby.TabIndex = 3
        '
        'btn_clear_store
        '
        Me.btn_clear_store.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_clear_store.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_clear_store.Location = New System.Drawing.Point(273, 23)
        Me.btn_clear_store.Name = "btn_clear_store"
        Me.btn_clear_store.Size = New System.Drawing.Size(73, 21)
        Me.btn_clear_store.TabIndex = 2
        Me.btn_clear_store.Text = "Clear"
        Me.btn_clear_store.UseVisualStyleBackColor = True
        '
        'btn_search_store
        '
        Me.btn_search_store.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_search_store.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_search_store.Location = New System.Drawing.Point(189, 23)
        Me.btn_search_store.Name = "btn_search_store"
        Me.btn_search_store.Size = New System.Drawing.Size(79, 21)
        Me.btn_search_store.TabIndex = 1
        Me.btn_search_store.Text = "Search"
        Me.btn_search_store.UseVisualStyleBackColor = False
        '
        'txtbox_storeno
        '
        Me.txtbox_storeno.Location = New System.Drawing.Point(97, 23)
        Me.txtbox_storeno.Name = "txtbox_storeno"
        Me.txtbox_storeno.Size = New System.Drawing.Size(86, 21)
        Me.txtbox_storeno.TabIndex = 0
        '
        'grp_store
        '
        Me.grp_store.Controls.Add(Me.lbl_str_system_load)
        Me.grp_store.Controls.Add(Me.lbl_str_system)
        Me.grp_store.Controls.Add(Me.lbl_store_iscombo_load)
        Me.grp_store.Controls.Add(Me.lbl_store_iscombo)
        Me.grp_store.Controls.Add(Me.lbl_district_load)
        Me.grp_store.Controls.Add(Me.lbl_mainl_load)
        Me.grp_store.Controls.Add(Me.lbl_country_load)
        Me.grp_store.Controls.Add(Me.lbl_postal_load)
        Me.grp_store.Controls.Add(Me.lbl_state_load)
        Me.grp_store.Controls.Add(Me.lbl_city_load)
        Me.grp_store.Controls.Add(Me.lbl_address_load)
        Me.grp_store.Controls.Add(Me.lbl_mall_load)
        Me.grp_store.Controls.Add(Me.lbl_sis_store_load)
        Me.grp_store.Controls.Add(Me.lbl_ban_load)
        Me.grp_store.Controls.Add(Me.lbl_strno_load)
        Me.grp_store.Controls.Add(Me.lbl_str_district)
        Me.grp_store.Controls.Add(Me.lbl_str_ph)
        Me.grp_store.Controls.Add(Me.lbl_str_cntry)
        Me.grp_store.Controls.Add(Me.lbl_str_pcode)
        Me.grp_store.Controls.Add(Me.lbl_str_prov)
        Me.grp_store.Controls.Add(Me.lbl_str_city)
        Me.grp_store.Controls.Add(Me.lbl_str_add)
        Me.grp_store.Controls.Add(Me.lbl_str_mall)
        Me.grp_store.Controls.Add(Me.lbl_str_sisstr)
        Me.grp_store.Controls.Add(Me.lbl_str_banner)
        Me.grp_store.Controls.Add(Me.lbl_str_no)
        Me.grp_store.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_store.Location = New System.Drawing.Point(12, 106)
        Me.grp_store.Name = "grp_store"
        Me.grp_store.Size = New System.Drawing.Size(358, 335)
        Me.grp_store.TabIndex = 2
        Me.grp_store.TabStop = False
        Me.grp_store.Text = "Store Details"
        '
        'lbl_str_system_load
        '
        Me.lbl_str_system_load.AutoSize = True
        Me.lbl_str_system_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_system_load.Location = New System.Drawing.Point(130, 68)
        Me.lbl_str_system_load.Name = "lbl_str_system_load"
        Me.lbl_str_system_load.Size = New System.Drawing.Size(97, 13)
        Me.lbl_str_system_load.TabIndex = 26
        Me.lbl_str_system_load.Text = "str_system_load"
        '
        'lbl_str_system
        '
        Me.lbl_str_system.AutoSize = True
        Me.lbl_str_system.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_system.Location = New System.Drawing.Point(14, 68)
        Me.lbl_str_system.Name = "lbl_str_system"
        Me.lbl_str_system.Size = New System.Drawing.Size(80, 13)
        Me.lbl_str_system.TabIndex = 25
        Me.lbl_str_system.Text = "POS System:"
        '
        'lbl_store_iscombo_load
        '
        Me.lbl_store_iscombo_load.AutoSize = True
        Me.lbl_store_iscombo_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_store_iscombo_load.Location = New System.Drawing.Point(130, 92)
        Me.lbl_store_iscombo_load.Name = "lbl_store_iscombo_load"
        Me.lbl_store_iscombo_load.Size = New System.Drawing.Size(76, 13)
        Me.lbl_store_iscombo_load.TabIndex = 24
        Me.lbl_store_iscombo_load.Text = "store_isCombo"
        '
        'lbl_store_iscombo
        '
        Me.lbl_store_iscombo.AutoSize = True
        Me.lbl_store_iscombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_store_iscombo.Location = New System.Drawing.Point(14, 92)
        Me.lbl_store_iscombo.Name = "lbl_store_iscombo"
        Me.lbl_store_iscombo.Size = New System.Drawing.Size(49, 13)
        Me.lbl_store_iscombo.TabIndex = 23
        Me.lbl_store_iscombo.Text = "Combo:"
        '
        'lbl_district_load
        '
        Me.lbl_district_load.AutoSize = True
        Me.lbl_district_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_district_load.Location = New System.Drawing.Point(130, 140)
        Me.lbl_district_load.Name = "lbl_district_load"
        Me.lbl_district_load.Size = New System.Drawing.Size(66, 13)
        Me.lbl_district_load.TabIndex = 22
        Me.lbl_district_load.Text = "store_district"
        '
        'lbl_mainl_load
        '
        Me.lbl_mainl_load.AutoSize = True
        Me.lbl_mainl_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_mainl_load.Location = New System.Drawing.Point(130, 311)
        Me.lbl_mainl_load.Name = "lbl_mainl_load"
        Me.lbl_mainl_load.Size = New System.Drawing.Size(58, 13)
        Me.lbl_mainl_load.TabIndex = 21
        Me.lbl_mainl_load.Text = "store_main"
        '
        'lbl_country_load
        '
        Me.lbl_country_load.AutoSize = True
        Me.lbl_country_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_country_load.Location = New System.Drawing.Point(130, 287)
        Me.lbl_country_load.Name = "lbl_country_load"
        Me.lbl_country_load.Size = New System.Drawing.Size(71, 13)
        Me.lbl_country_load.TabIndex = 20
        Me.lbl_country_load.Text = "store_country"
        '
        'lbl_postal_load
        '
        Me.lbl_postal_load.AutoSize = True
        Me.lbl_postal_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_postal_load.Location = New System.Drawing.Point(130, 262)
        Me.lbl_postal_load.Name = "lbl_postal_load"
        Me.lbl_postal_load.Size = New System.Drawing.Size(64, 13)
        Me.lbl_postal_load.TabIndex = 19
        Me.lbl_postal_load.Text = "store_postal"
        '
        'lbl_state_load
        '
        Me.lbl_state_load.AutoSize = True
        Me.lbl_state_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_state_load.Location = New System.Drawing.Point(130, 237)
        Me.lbl_state_load.Name = "lbl_state_load"
        Me.lbl_state_load.Size = New System.Drawing.Size(59, 13)
        Me.lbl_state_load.TabIndex = 18
        Me.lbl_state_load.Text = "store_state"
        '
        'lbl_city_load
        '
        Me.lbl_city_load.AutoSize = True
        Me.lbl_city_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_city_load.Location = New System.Drawing.Point(130, 213)
        Me.lbl_city_load.Name = "lbl_city_load"
        Me.lbl_city_load.Size = New System.Drawing.Size(52, 13)
        Me.lbl_city_load.TabIndex = 17
        Me.lbl_city_load.Text = "store_city"
        '
        'lbl_address_load
        '
        Me.lbl_address_load.AutoSize = True
        Me.lbl_address_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_address_load.Location = New System.Drawing.Point(130, 188)
        Me.lbl_address_load.Name = "lbl_address_load"
        Me.lbl_address_load.Size = New System.Drawing.Size(73, 13)
        Me.lbl_address_load.TabIndex = 16
        Me.lbl_address_load.Text = "store_address"
        '
        'lbl_mall_load
        '
        Me.lbl_mall_load.AutoSize = True
        Me.lbl_mall_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_mall_load.Location = New System.Drawing.Point(130, 164)
        Me.lbl_mall_load.Name = "lbl_mall_load"
        Me.lbl_mall_load.Size = New System.Drawing.Size(54, 13)
        Me.lbl_mall_load.TabIndex = 15
        Me.lbl_mall_load.Text = "store_mall"
        '
        'lbl_sis_store_load
        '
        Me.lbl_sis_store_load.AutoSize = True
        Me.lbl_sis_store_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sis_store_load.Location = New System.Drawing.Point(130, 116)
        Me.lbl_sis_store_load.Name = "lbl_sis_store_load"
        Me.lbl_sis_store_load.Size = New System.Drawing.Size(77, 13)
        Me.lbl_sis_store_load.TabIndex = 14
        Me.lbl_sis_store_load.Text = "store_sis_store"
        '
        'lbl_ban_load
        '
        Me.lbl_ban_load.AutoSize = True
        Me.lbl_ban_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ban_load.Location = New System.Drawing.Point(130, 44)
        Me.lbl_ban_load.Name = "lbl_ban_load"
        Me.lbl_ban_load.Size = New System.Drawing.Size(69, 13)
        Me.lbl_ban_load.TabIndex = 13
        Me.lbl_ban_load.Text = "store_banner"
        '
        'lbl_strno_load
        '
        Me.lbl_strno_load.AutoSize = True
        Me.lbl_strno_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_strno_load.Location = New System.Drawing.Point(130, 22)
        Me.lbl_strno_load.Name = "lbl_strno_load"
        Me.lbl_strno_load.Size = New System.Drawing.Size(48, 13)
        Me.lbl_strno_load.TabIndex = 12
        Me.lbl_strno_load.Text = "store_no"
        '
        'lbl_str_district
        '
        Me.lbl_str_district.AutoSize = True
        Me.lbl_str_district.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_district.Location = New System.Drawing.Point(14, 140)
        Me.lbl_str_district.Name = "lbl_str_district"
        Me.lbl_str_district.Size = New System.Drawing.Size(51, 13)
        Me.lbl_str_district.TabIndex = 10
        Me.lbl_str_district.Text = "District:"
        '
        'lbl_str_ph
        '
        Me.lbl_str_ph.AutoSize = True
        Me.lbl_str_ph.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_ph.Location = New System.Drawing.Point(14, 311)
        Me.lbl_str_ph.Name = "lbl_str_ph"
        Me.lbl_str_ph.Size = New System.Drawing.Size(66, 13)
        Me.lbl_str_ph.TabIndex = 9
        Me.lbl_str_ph.Text = "Main Line:"
        '
        'lbl_str_cntry
        '
        Me.lbl_str_cntry.AutoSize = True
        Me.lbl_str_cntry.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_cntry.Location = New System.Drawing.Point(14, 287)
        Me.lbl_str_cntry.Name = "lbl_str_cntry"
        Me.lbl_str_cntry.Size = New System.Drawing.Size(54, 13)
        Me.lbl_str_cntry.TabIndex = 8
        Me.lbl_str_cntry.Text = "Country:"
        '
        'lbl_str_pcode
        '
        Me.lbl_str_pcode.AutoSize = True
        Me.lbl_str_pcode.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_pcode.Location = New System.Drawing.Point(14, 262)
        Me.lbl_str_pcode.Name = "lbl_str_pcode"
        Me.lbl_str_pcode.Size = New System.Drawing.Size(79, 13)
        Me.lbl_str_pcode.TabIndex = 7
        Me.lbl_str_pcode.Text = "Postal Code:"
        '
        'lbl_str_prov
        '
        Me.lbl_str_prov.AutoSize = True
        Me.lbl_str_prov.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_prov.Location = New System.Drawing.Point(14, 237)
        Me.lbl_str_prov.Name = "lbl_str_prov"
        Me.lbl_str_prov.Size = New System.Drawing.Size(105, 13)
        Me.lbl_str_prov.TabIndex = 6
        Me.lbl_str_prov.Text = "Province / State:"
        '
        'lbl_str_city
        '
        Me.lbl_str_city.AutoSize = True
        Me.lbl_str_city.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_city.Location = New System.Drawing.Point(14, 213)
        Me.lbl_str_city.Name = "lbl_str_city"
        Me.lbl_str_city.Size = New System.Drawing.Size(32, 13)
        Me.lbl_str_city.TabIndex = 5
        Me.lbl_str_city.Text = "City:"
        '
        'lbl_str_add
        '
        Me.lbl_str_add.AutoSize = True
        Me.lbl_str_add.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_add.Location = New System.Drawing.Point(14, 188)
        Me.lbl_str_add.Name = "lbl_str_add"
        Me.lbl_str_add.Size = New System.Drawing.Size(56, 13)
        Me.lbl_str_add.TabIndex = 4
        Me.lbl_str_add.Text = "Address:"
        '
        'lbl_str_mall
        '
        Me.lbl_str_mall.AutoSize = True
        Me.lbl_str_mall.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_mall.Location = New System.Drawing.Point(14, 164)
        Me.lbl_str_mall.Name = "lbl_str_mall"
        Me.lbl_str_mall.Size = New System.Drawing.Size(34, 13)
        Me.lbl_str_mall.TabIndex = 3
        Me.lbl_str_mall.Text = "Mall:"
        '
        'lbl_str_sisstr
        '
        Me.lbl_str_sisstr.AutoSize = True
        Me.lbl_str_sisstr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_sisstr.Location = New System.Drawing.Point(14, 116)
        Me.lbl_str_sisstr.Name = "lbl_str_sisstr"
        Me.lbl_str_sisstr.Size = New System.Drawing.Size(77, 13)
        Me.lbl_str_sisstr.TabIndex = 2
        Me.lbl_str_sisstr.Text = "Sister Store:"
        '
        'lbl_str_banner
        '
        Me.lbl_str_banner.AutoSize = True
        Me.lbl_str_banner.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_banner.Location = New System.Drawing.Point(14, 44)
        Me.lbl_str_banner.Name = "lbl_str_banner"
        Me.lbl_str_banner.Size = New System.Drawing.Size(51, 13)
        Me.lbl_str_banner.TabIndex = 1
        Me.lbl_str_banner.Text = "Banner:"
        '
        'lbl_str_no
        '
        Me.lbl_str_no.AutoSize = True
        Me.lbl_str_no.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_str_no.Location = New System.Drawing.Point(14, 22)
        Me.lbl_str_no.Name = "lbl_str_no"
        Me.lbl_str_no.Size = New System.Drawing.Size(61, 13)
        Me.lbl_str_no.TabIndex = 0
        Me.lbl_str_no.Text = "Store No:"
        '
        'grp_network_top
        '
        Me.grp_network_top.Controls.Add(Me.lbl_switch2_load)
        Me.grp_network_top.Controls.Add(Me.lbl_switch1_load)
        Me.grp_network_top.Controls.Add(Me.lbl_router_load)
        Me.grp_network_top.Controls.Add(Me.btn_gui_sw2)
        Me.grp_network_top.Controls.Add(Me.btn_gui_sw1)
        Me.grp_network_top.Controls.Add(Me.btn_cli_sw2)
        Me.grp_network_top.Controls.Add(Me.btn_cli_sw1)
        Me.grp_network_top.Controls.Add(Me.lbl_net_sw2)
        Me.grp_network_top.Controls.Add(Me.lbl_net_sw1)
        Me.grp_network_top.Controls.Add(Me.lbl_net_rtr)
        Me.grp_network_top.Controls.Add(Me.btn_gui_router)
        Me.grp_network_top.Controls.Add(Me.btn_cli_router)
        Me.grp_network_top.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_network_top.Location = New System.Drawing.Point(376, 44)
        Me.grp_network_top.Name = "grp_network_top"
        Me.grp_network_top.Size = New System.Drawing.Size(425, 106)
        Me.grp_network_top.TabIndex = 3
        Me.grp_network_top.TabStop = False
        Me.grp_network_top.Text = "Store Network"
        '
        'lbl_switch2_load
        '
        Me.lbl_switch2_load.AutoSize = True
        Me.lbl_switch2_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_switch2_load.Location = New System.Drawing.Point(83, 78)
        Me.lbl_switch2_load.Name = "lbl_switch2_load"
        Me.lbl_switch2_load.Size = New System.Drawing.Size(85, 13)
        Me.lbl_switch2_load.TabIndex = 25
        Me.lbl_switch2_load.Text = "store_switch2"
        '
        'lbl_switch1_load
        '
        Me.lbl_switch1_load.AutoSize = True
        Me.lbl_switch1_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_switch1_load.Location = New System.Drawing.Point(83, 51)
        Me.lbl_switch1_load.Name = "lbl_switch1_load"
        Me.lbl_switch1_load.Size = New System.Drawing.Size(85, 13)
        Me.lbl_switch1_load.TabIndex = 24
        Me.lbl_switch1_load.Text = "store_switch1"
        '
        'lbl_router_load
        '
        Me.lbl_router_load.AutoSize = True
        Me.lbl_router_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_router_load.Location = New System.Drawing.Point(83, 24)
        Me.lbl_router_load.Name = "lbl_router_load"
        Me.lbl_router_load.Size = New System.Drawing.Size(75, 13)
        Me.lbl_router_load.TabIndex = 23
        Me.lbl_router_load.Text = "store_router"
        '
        'btn_gui_sw2
        '
        Me.btn_gui_sw2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_gui_sw2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_gui_sw2.Location = New System.Drawing.Point(362, 73)
        Me.btn_gui_sw2.Name = "btn_gui_sw2"
        Me.btn_gui_sw2.Size = New System.Drawing.Size(57, 21)
        Me.btn_gui_sw2.TabIndex = 10
        Me.btn_gui_sw2.Text = "GUI"
        Me.btn_gui_sw2.UseVisualStyleBackColor = False
        '
        'btn_gui_sw1
        '
        Me.btn_gui_sw1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_gui_sw1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_gui_sw1.Location = New System.Drawing.Point(362, 46)
        Me.btn_gui_sw1.Name = "btn_gui_sw1"
        Me.btn_gui_sw1.Size = New System.Drawing.Size(57, 21)
        Me.btn_gui_sw1.TabIndex = 9
        Me.btn_gui_sw1.Text = "GUI"
        Me.btn_gui_sw1.UseVisualStyleBackColor = False
        '
        'btn_cli_sw2
        '
        Me.btn_cli_sw2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_cli_sw2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_cli_sw2.Location = New System.Drawing.Point(299, 73)
        Me.btn_cli_sw2.Name = "btn_cli_sw2"
        Me.btn_cli_sw2.Size = New System.Drawing.Size(57, 21)
        Me.btn_cli_sw2.TabIndex = 8
        Me.btn_cli_sw2.Text = "CLI"
        Me.btn_cli_sw2.UseVisualStyleBackColor = False
        '
        'btn_cli_sw1
        '
        Me.btn_cli_sw1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_cli_sw1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_cli_sw1.Location = New System.Drawing.Point(299, 47)
        Me.btn_cli_sw1.Name = "btn_cli_sw1"
        Me.btn_cli_sw1.Size = New System.Drawing.Size(57, 21)
        Me.btn_cli_sw1.TabIndex = 7
        Me.btn_cli_sw1.Text = "CLI"
        Me.btn_cli_sw1.UseVisualStyleBackColor = False
        '
        'lbl_net_sw2
        '
        Me.lbl_net_sw2.AutoSize = True
        Me.lbl_net_sw2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_net_sw2.Location = New System.Drawing.Point(15, 78)
        Me.lbl_net_sw2.Name = "lbl_net_sw2"
        Me.lbl_net_sw2.Size = New System.Drawing.Size(56, 13)
        Me.lbl_net_sw2.TabIndex = 6
        Me.lbl_net_sw2.Text = "Switch2:"
        '
        'lbl_net_sw1
        '
        Me.lbl_net_sw1.AutoSize = True
        Me.lbl_net_sw1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_net_sw1.Location = New System.Drawing.Point(15, 51)
        Me.lbl_net_sw1.Name = "lbl_net_sw1"
        Me.lbl_net_sw1.Size = New System.Drawing.Size(56, 13)
        Me.lbl_net_sw1.TabIndex = 5
        Me.lbl_net_sw1.Text = "Switch1:"
        '
        'lbl_net_rtr
        '
        Me.lbl_net_rtr.AutoSize = True
        Me.lbl_net_rtr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_net_rtr.Location = New System.Drawing.Point(15, 24)
        Me.lbl_net_rtr.Name = "lbl_net_rtr"
        Me.lbl_net_rtr.Size = New System.Drawing.Size(49, 13)
        Me.lbl_net_rtr.TabIndex = 4
        Me.lbl_net_rtr.Text = "Router:"
        '
        'btn_gui_router
        '
        Me.btn_gui_router.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_gui_router.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_gui_router.Location = New System.Drawing.Point(362, 19)
        Me.btn_gui_router.Name = "btn_gui_router"
        Me.btn_gui_router.Size = New System.Drawing.Size(57, 21)
        Me.btn_gui_router.TabIndex = 3
        Me.btn_gui_router.Text = "GUI"
        Me.btn_gui_router.UseVisualStyleBackColor = False
        '
        'btn_cli_router
        '
        Me.btn_cli_router.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_cli_router.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_cli_router.Location = New System.Drawing.Point(299, 19)
        Me.btn_cli_router.Name = "btn_cli_router"
        Me.btn_cli_router.Size = New System.Drawing.Size(57, 21)
        Me.btn_cli_router.TabIndex = 2
        Me.btn_cli_router.Text = "CLI"
        Me.btn_cli_router.UseVisualStyleBackColor = False
        '
        'grp_devices
        '
        Me.grp_devices.Controls.Add(Me.lbl_samsung_load)
        Me.grp_devices.Controls.Add(Me.lbl_ipad_load)
        Me.grp_devices.Controls.Add(Me.lbl_music_load)
        Me.grp_devices.Controls.Add(Me.lbl_tc_load)
        Me.grp_devices.Controls.Add(Me.lbl_ap2_load)
        Me.grp_devices.Controls.Add(Me.lbl_ap1_load)
        Me.grp_devices.Controls.Add(Me.lbl_stbl)
        Me.grp_devices.Controls.Add(Me.lbl_ipad)
        Me.grp_devices.Controls.Add(Me.lbl_music)
        Me.grp_devices.Controls.Add(Me.lbl_tc)
        Me.grp_devices.Controls.Add(Me.lbl_ap2)
        Me.grp_devices.Controls.Add(Me.lbl_ap1)
        Me.grp_devices.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_devices.Location = New System.Drawing.Point(376, 156)
        Me.grp_devices.Name = "grp_devices"
        Me.grp_devices.Size = New System.Drawing.Size(189, 200)
        Me.grp_devices.TabIndex = 4
        Me.grp_devices.TabStop = False
        Me.grp_devices.Text = "Store Devices"
        '
        'lbl_samsung_load
        '
        Me.lbl_samsung_load.AutoSize = True
        Me.lbl_samsung_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_samsung_load.Location = New System.Drawing.Point(83, 145)
        Me.lbl_samsung_load.Name = "lbl_samsung_load"
        Me.lbl_samsung_load.Size = New System.Drawing.Size(91, 13)
        Me.lbl_samsung_load.TabIndex = 31
        Me.lbl_samsung_load.Text = "store_samsung"
        '
        'lbl_ipad_load
        '
        Me.lbl_ipad_load.AutoSize = True
        Me.lbl_ipad_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ipad_load.Location = New System.Drawing.Point(83, 121)
        Me.lbl_ipad_load.Name = "lbl_ipad_load"
        Me.lbl_ipad_load.Size = New System.Drawing.Size(66, 13)
        Me.lbl_ipad_load.TabIndex = 30
        Me.lbl_ipad_load.Text = "store_ipad"
        '
        'lbl_music_load
        '
        Me.lbl_music_load.AutoSize = True
        Me.lbl_music_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_music_load.Location = New System.Drawing.Point(83, 96)
        Me.lbl_music_load.Name = "lbl_music_load"
        Me.lbl_music_load.Size = New System.Drawing.Size(74, 13)
        Me.lbl_music_load.TabIndex = 29
        Me.lbl_music_load.Text = "store_music"
        '
        'lbl_tc_load
        '
        Me.lbl_tc_load.AutoSize = True
        Me.lbl_tc_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_tc_load.Location = New System.Drawing.Point(83, 72)
        Me.lbl_tc_load.Name = "lbl_tc_load"
        Me.lbl_tc_load.Size = New System.Drawing.Size(53, 13)
        Me.lbl_tc_load.TabIndex = 28
        Me.lbl_tc_load.Text = "store_tc"
        '
        'lbl_ap2_load
        '
        Me.lbl_ap2_load.AutoSize = True
        Me.lbl_ap2_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ap2_load.Location = New System.Drawing.Point(83, 48)
        Me.lbl_ap2_load.Name = "lbl_ap2_load"
        Me.lbl_ap2_load.Size = New System.Drawing.Size(63, 13)
        Me.lbl_ap2_load.TabIndex = 27
        Me.lbl_ap2_load.Text = "store_ap2"
        '
        'lbl_ap1_load
        '
        Me.lbl_ap1_load.AutoSize = True
        Me.lbl_ap1_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ap1_load.Location = New System.Drawing.Point(83, 24)
        Me.lbl_ap1_load.Name = "lbl_ap1_load"
        Me.lbl_ap1_load.Size = New System.Drawing.Size(63, 13)
        Me.lbl_ap1_load.TabIndex = 26
        Me.lbl_ap1_load.Text = "store_ap1"
        '
        'lbl_stbl
        '
        Me.lbl_stbl.AutoSize = True
        Me.lbl_stbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_stbl.Location = New System.Drawing.Point(15, 145)
        Me.lbl_stbl.Name = "lbl_stbl"
        Me.lbl_stbl.Size = New System.Drawing.Size(62, 13)
        Me.lbl_stbl.TabIndex = 5
        Me.lbl_stbl.Text = "Samsung:"
        '
        'lbl_ipad
        '
        Me.lbl_ipad.AutoSize = True
        Me.lbl_ipad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ipad.Location = New System.Drawing.Point(15, 121)
        Me.lbl_ipad.Name = "lbl_ipad"
        Me.lbl_ipad.Size = New System.Drawing.Size(40, 13)
        Me.lbl_ipad.TabIndex = 4
        Me.lbl_ipad.Text = "IPAD:"
        '
        'lbl_music
        '
        Me.lbl_music.AutoSize = True
        Me.lbl_music.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_music.Location = New System.Drawing.Point(15, 96)
        Me.lbl_music.Name = "lbl_music"
        Me.lbl_music.Size = New System.Drawing.Size(44, 13)
        Me.lbl_music.TabIndex = 3
        Me.lbl_music.Text = "Music:"
        '
        'lbl_tc
        '
        Me.lbl_tc.AutoSize = True
        Me.lbl_tc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_tc.Location = New System.Drawing.Point(15, 72)
        Me.lbl_tc.Name = "lbl_tc"
        Me.lbl_tc.Size = New System.Drawing.Size(27, 13)
        Me.lbl_tc.TabIndex = 2
        Me.lbl_tc.Text = "TC:"
        '
        'lbl_ap2
        '
        Me.lbl_ap2.AutoSize = True
        Me.lbl_ap2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ap2.Location = New System.Drawing.Point(15, 48)
        Me.lbl_ap2.Name = "lbl_ap2"
        Me.lbl_ap2.Size = New System.Drawing.Size(34, 13)
        Me.lbl_ap2.TabIndex = 1
        Me.lbl_ap2.Text = "AP2:"
        '
        'lbl_ap1
        '
        Me.lbl_ap1.AutoSize = True
        Me.lbl_ap1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ap1.Location = New System.Drawing.Point(15, 24)
        Me.lbl_ap1.Name = "lbl_ap1"
        Me.lbl_ap1.Size = New System.Drawing.Size(34, 13)
        Me.lbl_ap1.TabIndex = 0
        Me.lbl_ap1.Text = "AP1:"
        '
        'grp_cashes
        '
        Me.grp_cashes.Controls.Add(Me.pic_pinpad_c6)
        Me.grp_cashes.Controls.Add(Me.pic_pinpad_c5)
        Me.grp_cashes.Controls.Add(Me.pic_pinpad_c4)
        Me.grp_cashes.Controls.Add(Me.pic_pinpad_c3)
        Me.grp_cashes.Controls.Add(Me.pic_pinpad_c2)
        Me.grp_cashes.Controls.Add(Me.pic_pinpad_c1)
        Me.grp_cashes.Controls.Add(Me.lbl_bo_load)
        Me.grp_cashes.Controls.Add(Me.lbl_cash6_load)
        Me.grp_cashes.Controls.Add(Me.lbl_cash5_load)
        Me.grp_cashes.Controls.Add(Me.lbl_cash4_load)
        Me.grp_cashes.Controls.Add(Me.lbl_cash3_load)
        Me.grp_cashes.Controls.Add(Me.lbl_cash2_load)
        Me.grp_cashes.Controls.Add(Me.lbl_cash1_load)
        Me.grp_cashes.Controls.Add(Me.btn_tv_bo)
        Me.grp_cashes.Controls.Add(Me.btn_tv_c6)
        Me.grp_cashes.Controls.Add(Me.btn_tv_c5)
        Me.grp_cashes.Controls.Add(Me.btn_tv_c4)
        Me.grp_cashes.Controls.Add(Me.btn_tv_c3)
        Me.grp_cashes.Controls.Add(Me.btn_tv_c2)
        Me.grp_cashes.Controls.Add(Me.btn_tv_c1)
        Me.grp_cashes.Controls.Add(Me.lbl_bo)
        Me.grp_cashes.Controls.Add(Me.lbl_cash6)
        Me.grp_cashes.Controls.Add(Me.lbl_cash5)
        Me.grp_cashes.Controls.Add(Me.lbl_cash4)
        Me.grp_cashes.Controls.Add(Me.lbl_cash3)
        Me.grp_cashes.Controls.Add(Me.lbl_cash2)
        Me.grp_cashes.Controls.Add(Me.lbl_cash1)
        Me.grp_cashes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_cashes.Location = New System.Drawing.Point(571, 156)
        Me.grp_cashes.Name = "grp_cashes"
        Me.grp_cashes.Size = New System.Drawing.Size(230, 200)
        Me.grp_cashes.TabIndex = 6
        Me.grp_cashes.TabStop = False
        Me.grp_cashes.Text = "Store Cashes"
        '
        'lbl_bo_load
        '
        Me.lbl_bo_load.AutoSize = True
        Me.lbl_bo_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_bo_load.Location = New System.Drawing.Point(74, 170)
        Me.lbl_bo_load.Name = "lbl_bo_load"
        Me.lbl_bo_load.Size = New System.Drawing.Size(21, 13)
        Me.lbl_bo_load.TabIndex = 38
        Me.lbl_bo_load.Text = "bo"
        '
        'lbl_cash6_load
        '
        Me.lbl_cash6_load.AutoSize = True
        Me.lbl_cash6_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash6_load.Location = New System.Drawing.Point(74, 145)
        Me.lbl_cash6_load.Name = "lbl_cash6_load"
        Me.lbl_cash6_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_cash6_load.TabIndex = 37
        Me.lbl_cash6_load.Text = "cash6"
        '
        'lbl_cash5_load
        '
        Me.lbl_cash5_load.AutoSize = True
        Me.lbl_cash5_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash5_load.Location = New System.Drawing.Point(74, 121)
        Me.lbl_cash5_load.Name = "lbl_cash5_load"
        Me.lbl_cash5_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_cash5_load.TabIndex = 36
        Me.lbl_cash5_load.Text = "cash5"
        '
        'lbl_cash4_load
        '
        Me.lbl_cash4_load.AutoSize = True
        Me.lbl_cash4_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash4_load.Location = New System.Drawing.Point(74, 96)
        Me.lbl_cash4_load.Name = "lbl_cash4_load"
        Me.lbl_cash4_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_cash4_load.TabIndex = 35
        Me.lbl_cash4_load.Text = "cash4"
        '
        'lbl_cash3_load
        '
        Me.lbl_cash3_load.AutoSize = True
        Me.lbl_cash3_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash3_load.Location = New System.Drawing.Point(74, 72)
        Me.lbl_cash3_load.Name = "lbl_cash3_load"
        Me.lbl_cash3_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_cash3_load.TabIndex = 34
        Me.lbl_cash3_load.Text = "cash3"
        '
        'lbl_cash2_load
        '
        Me.lbl_cash2_load.AutoSize = True
        Me.lbl_cash2_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash2_load.Location = New System.Drawing.Point(74, 48)
        Me.lbl_cash2_load.Name = "lbl_cash2_load"
        Me.lbl_cash2_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_cash2_load.TabIndex = 33
        Me.lbl_cash2_load.Text = "cash2"
        '
        'lbl_cash1_load
        '
        Me.lbl_cash1_load.AutoSize = True
        Me.lbl_cash1_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash1_load.Location = New System.Drawing.Point(74, 24)
        Me.lbl_cash1_load.Name = "lbl_cash1_load"
        Me.lbl_cash1_load.Size = New System.Drawing.Size(41, 13)
        Me.lbl_cash1_load.TabIndex = 32
        Me.lbl_cash1_load.Text = "cash1"
        '
        'btn_tv_bo
        '
        Me.btn_tv_bo.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tv_bo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_tv_bo.Location = New System.Drawing.Point(151, 165)
        Me.btn_tv_bo.Name = "btn_tv_bo"
        Me.btn_tv_bo.Size = New System.Drawing.Size(73, 21)
        Me.btn_tv_bo.TabIndex = 24
        Me.btn_tv_bo.Text = "Remote"
        Me.btn_tv_bo.UseVisualStyleBackColor = False
        '
        'btn_tv_c6
        '
        Me.btn_tv_c6.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tv_c6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_tv_c6.Location = New System.Drawing.Point(151, 140)
        Me.btn_tv_c6.Name = "btn_tv_c6"
        Me.btn_tv_c6.Size = New System.Drawing.Size(73, 21)
        Me.btn_tv_c6.TabIndex = 23
        Me.btn_tv_c6.Text = "Remote"
        Me.btn_tv_c6.UseVisualStyleBackColor = False
        '
        'btn_tv_c5
        '
        Me.btn_tv_c5.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tv_c5.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_tv_c5.Location = New System.Drawing.Point(151, 116)
        Me.btn_tv_c5.Name = "btn_tv_c5"
        Me.btn_tv_c5.Size = New System.Drawing.Size(73, 21)
        Me.btn_tv_c5.TabIndex = 22
        Me.btn_tv_c5.Text = "Remote"
        Me.btn_tv_c5.UseVisualStyleBackColor = False
        '
        'btn_tv_c4
        '
        Me.btn_tv_c4.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tv_c4.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_tv_c4.Location = New System.Drawing.Point(151, 91)
        Me.btn_tv_c4.Name = "btn_tv_c4"
        Me.btn_tv_c4.Size = New System.Drawing.Size(73, 21)
        Me.btn_tv_c4.TabIndex = 21
        Me.btn_tv_c4.Text = "Remote"
        Me.btn_tv_c4.UseVisualStyleBackColor = False
        '
        'btn_tv_c3
        '
        Me.btn_tv_c3.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tv_c3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_tv_c3.Location = New System.Drawing.Point(151, 67)
        Me.btn_tv_c3.Name = "btn_tv_c3"
        Me.btn_tv_c3.Size = New System.Drawing.Size(73, 21)
        Me.btn_tv_c3.TabIndex = 20
        Me.btn_tv_c3.Text = "Remote"
        Me.btn_tv_c3.UseVisualStyleBackColor = False
        '
        'btn_tv_c2
        '
        Me.btn_tv_c2.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tv_c2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_tv_c2.Location = New System.Drawing.Point(151, 43)
        Me.btn_tv_c2.Name = "btn_tv_c2"
        Me.btn_tv_c2.Size = New System.Drawing.Size(73, 21)
        Me.btn_tv_c2.TabIndex = 19
        Me.btn_tv_c2.Text = "Remote"
        Me.btn_tv_c2.UseVisualStyleBackColor = False
        '
        'btn_tv_c1
        '
        Me.btn_tv_c1.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_tv_c1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_tv_c1.Location = New System.Drawing.Point(151, 19)
        Me.btn_tv_c1.Name = "btn_tv_c1"
        Me.btn_tv_c1.Size = New System.Drawing.Size(73, 21)
        Me.btn_tv_c1.TabIndex = 18
        Me.btn_tv_c1.Text = "Remote"
        Me.btn_tv_c1.UseVisualStyleBackColor = False
        '
        'lbl_bo
        '
        Me.lbl_bo.AutoSize = True
        Me.lbl_bo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_bo.Location = New System.Drawing.Point(22, 170)
        Me.lbl_bo.Name = "lbl_bo"
        Me.lbl_bo.Size = New System.Drawing.Size(48, 13)
        Me.lbl_bo.TabIndex = 6
        Me.lbl_bo.Text = "BO PC:"
        '
        'lbl_cash6
        '
        Me.lbl_cash6.AutoSize = True
        Me.lbl_cash6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash6.Location = New System.Drawing.Point(22, 145)
        Me.lbl_cash6.Name = "lbl_cash6"
        Me.lbl_cash6.Size = New System.Drawing.Size(46, 13)
        Me.lbl_cash6.TabIndex = 5
        Me.lbl_cash6.Text = "Cash6:"
        '
        'lbl_cash5
        '
        Me.lbl_cash5.AutoSize = True
        Me.lbl_cash5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash5.Location = New System.Drawing.Point(22, 121)
        Me.lbl_cash5.Name = "lbl_cash5"
        Me.lbl_cash5.Size = New System.Drawing.Size(46, 13)
        Me.lbl_cash5.TabIndex = 4
        Me.lbl_cash5.Text = "Cash5:"
        '
        'lbl_cash4
        '
        Me.lbl_cash4.AutoSize = True
        Me.lbl_cash4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash4.Location = New System.Drawing.Point(22, 96)
        Me.lbl_cash4.Name = "lbl_cash4"
        Me.lbl_cash4.Size = New System.Drawing.Size(42, 13)
        Me.lbl_cash4.TabIndex = 3
        Me.lbl_cash4.Text = "Cash4"
        '
        'lbl_cash3
        '
        Me.lbl_cash3.AutoSize = True
        Me.lbl_cash3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash3.Location = New System.Drawing.Point(22, 72)
        Me.lbl_cash3.Name = "lbl_cash3"
        Me.lbl_cash3.Size = New System.Drawing.Size(46, 13)
        Me.lbl_cash3.TabIndex = 2
        Me.lbl_cash3.Text = "Cash3:"
        '
        'lbl_cash2
        '
        Me.lbl_cash2.AutoSize = True
        Me.lbl_cash2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash2.Location = New System.Drawing.Point(22, 48)
        Me.lbl_cash2.Name = "lbl_cash2"
        Me.lbl_cash2.Size = New System.Drawing.Size(46, 13)
        Me.lbl_cash2.TabIndex = 1
        Me.lbl_cash2.Text = "Cash2:"
        '
        'lbl_cash1
        '
        Me.lbl_cash1.AutoSize = True
        Me.lbl_cash1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cash1.Location = New System.Drawing.Point(22, 24)
        Me.lbl_cash1.Name = "lbl_cash1"
        Me.lbl_cash1.Size = New System.Drawing.Size(46, 13)
        Me.lbl_cash1.TabIndex = 0
        Me.lbl_cash1.Text = "Cash1:"
        '
        'grp_moreinfo
        '
        Me.grp_moreinfo.Controls.Add(Me.lbl_wifi_load)
        Me.grp_moreinfo.Controls.Add(Me.lbl_store_speed_load)
        Me.grp_moreinfo.Controls.Add(Me.lbl_spare_equip_load)
        Me.grp_moreinfo.Controls.Add(Me.lbl_remote_load)
        Me.grp_moreinfo.Controls.Add(Me.lbl_wifi_speed)
        Me.grp_moreinfo.Controls.Add(Me.lbl_speed)
        Me.grp_moreinfo.Controls.Add(Me.lbl_spare_equip)
        Me.grp_moreinfo.Controls.Add(Me.lbl_remote)
        Me.grp_moreinfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grp_moreinfo.Location = New System.Drawing.Point(376, 362)
        Me.grp_moreinfo.Name = "grp_moreinfo"
        Me.grp_moreinfo.Size = New System.Drawing.Size(425, 79)
        Me.grp_moreinfo.TabIndex = 7
        Me.grp_moreinfo.TabStop = False
        Me.grp_moreinfo.Text = "More Information"
        '
        'lbl_wifi_load
        '
        Me.lbl_wifi_load.AutoSize = True
        Me.lbl_wifi_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_wifi_load.Location = New System.Drawing.Point(324, 48)
        Me.lbl_wifi_load.Name = "lbl_wifi_load"
        Me.lbl_wifi_load.Size = New System.Drawing.Size(61, 13)
        Me.lbl_wifi_load.TabIndex = 40
        Me.lbl_wifi_load.Text = "store_wifi"
        '
        'lbl_store_speed_load
        '
        Me.lbl_store_speed_load.AutoSize = True
        Me.lbl_store_speed_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_store_speed_load.Location = New System.Drawing.Point(324, 24)
        Me.lbl_store_speed_load.Name = "lbl_store_speed_load"
        Me.lbl_store_speed_load.Size = New System.Drawing.Size(76, 13)
        Me.lbl_store_speed_load.TabIndex = 39
        Me.lbl_store_speed_load.Text = "store_speed"
        '
        'lbl_spare_equip_load
        '
        Me.lbl_spare_equip_load.AutoSize = True
        Me.lbl_spare_equip_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_equip_load.Location = New System.Drawing.Point(111, 48)
        Me.lbl_spare_equip_load.Name = "lbl_spare_equip_load"
        Me.lbl_spare_equip_load.Size = New System.Drawing.Size(73, 13)
        Me.lbl_spare_equip_load.TabIndex = 33
        Me.lbl_spare_equip_load.Text = "store_equip"
        '
        'lbl_remote_load
        '
        Me.lbl_remote_load.AutoSize = True
        Me.lbl_remote_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_remote_load.Location = New System.Drawing.Point(111, 24)
        Me.lbl_remote_load.Name = "lbl_remote_load"
        Me.lbl_remote_load.Size = New System.Drawing.Size(80, 13)
        Me.lbl_remote_load.TabIndex = 32
        Me.lbl_remote_load.Text = "store_remote"
        '
        'lbl_wifi_speed
        '
        Me.lbl_wifi_speed.AutoSize = True
        Me.lbl_wifi_speed.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_wifi_speed.Location = New System.Drawing.Point(192, 48)
        Me.lbl_wifi_speed.Name = "lbl_wifi_speed"
        Me.lbl_wifi_speed.Size = New System.Drawing.Size(118, 13)
        Me.lbl_wifi_speed.TabIndex = 3
        Me.lbl_wifi_speed.Text = "WiFi Speed (Mbps):"
        '
        'lbl_speed
        '
        Me.lbl_speed.AutoSize = True
        Me.lbl_speed.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_speed.Location = New System.Drawing.Point(192, 24)
        Me.lbl_speed.Name = "lbl_speed"
        Me.lbl_speed.Size = New System.Drawing.Size(126, 13)
        Me.lbl_speed.TabIndex = 2
        Me.lbl_speed.Text = "Store (DL/UP Mbps):"
        '
        'lbl_spare_equip
        '
        Me.lbl_spare_equip.AutoSize = True
        Me.lbl_spare_equip.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_spare_equip.Location = New System.Drawing.Point(22, 49)
        Me.lbl_spare_equip.Name = "lbl_spare_equip"
        Me.lbl_spare_equip.Size = New System.Drawing.Size(80, 13)
        Me.lbl_spare_equip.TabIndex = 1
        Me.lbl_spare_equip.Text = "Spare Equip:"
        '
        'lbl_remote
        '
        Me.lbl_remote.AutoSize = True
        Me.lbl_remote.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_remote.Location = New System.Drawing.Point(22, 24)
        Me.lbl_remote.Name = "lbl_remote"
        Me.lbl_remote.Size = New System.Drawing.Size(54, 13)
        Me.lbl_remote.TabIndex = 0
        Me.lbl_remote.Text = "Remote:"
        '
        'btn_moreinfo
        '
        Me.btn_moreinfo.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_moreinfo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_moreinfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_moreinfo.Location = New System.Drawing.Point(666, 456)
        Me.btn_moreinfo.Name = "btn_moreinfo"
        Me.btn_moreinfo.Size = New System.Drawing.Size(129, 21)
        Me.btn_moreinfo.TabIndex = 10
        Me.btn_moreinfo.Text = "More Information"
        Me.btn_moreinfo.UseVisualStyleBackColor = False
        '
        'btn_list_status
        '
        Me.btn_list_status.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_list_status.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_list_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_list_status.Location = New System.Drawing.Point(572, 456)
        Me.btn_list_status.Name = "btn_list_status"
        Me.btn_list_status.Size = New System.Drawing.Size(88, 21)
        Me.btn_list_status.TabIndex = 12
        Me.btn_list_status.Text = "PING List"
        Me.btn_list_status.UseVisualStyleBackColor = False
        '
        'btn_store_hours
        '
        Me.btn_store_hours.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_store_hours.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_store_hours.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_store_hours.Location = New System.Drawing.Point(471, 456)
        Me.btn_store_hours.Name = "btn_store_hours"
        Me.btn_store_hours.Size = New System.Drawing.Size(95, 21)
        Me.btn_store_hours.TabIndex = 13
        Me.btn_store_hours.Text = "Store Hours"
        Me.btn_store_hours.UseVisualStyleBackColor = False
        '
        'ping_timer
        '
        Me.ping_timer.Interval = 1000
        '
        'lbl_user_load
        '
        Me.lbl_user_load.AutoSize = True
        Me.lbl_user_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_user_load.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbl_user_load.Location = New System.Drawing.Point(592, 5)
        Me.lbl_user_load.Name = "lbl_user_load"
        Me.lbl_user_load.Size = New System.Drawing.Size(95, 13)
        Me.lbl_user_load.TabIndex = 14
        Me.lbl_user_load.Text = "local_username"
        '
        'lbl_user
        '
        Me.lbl_user.AutoSize = True
        Me.lbl_user.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_user.Location = New System.Drawing.Point(558, 5)
        Me.lbl_user.Name = "lbl_user"
        Me.lbl_user.Size = New System.Drawing.Size(37, 13)
        Me.lbl_user.TabIndex = 15
        Me.lbl_user.Text = "User:"
        '
        'lbl_refresh
        '
        Me.lbl_refresh.AutoSize = True
        Me.lbl_refresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_refresh.Location = New System.Drawing.Point(330, 5)
        Me.lbl_refresh.Name = "lbl_refresh"
        Me.lbl_refresh.Size = New System.Drawing.Size(79, 13)
        Me.lbl_refresh.TabIndex = 16
        Me.lbl_refresh.Text = "Last Reload:"
        '
        'lbl_refresh_load
        '
        Me.lbl_refresh_load.AutoSize = True
        Me.lbl_refresh_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_refresh_load.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbl_refresh_load.Location = New System.Drawing.Point(406, 5)
        Me.lbl_refresh_load.Name = "lbl_refresh_load"
        Me.lbl_refresh_load.Size = New System.Drawing.Size(73, 13)
        Me.lbl_refresh_load.TabIndex = 17
        Me.lbl_refresh_load.Text = "last_refresh"
        '
        'lbl_role
        '
        Me.lbl_role.AutoSize = True
        Me.lbl_role.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_role.Location = New System.Drawing.Point(558, 22)
        Me.lbl_role.Name = "lbl_role"
        Me.lbl_role.Size = New System.Drawing.Size(37, 13)
        Me.lbl_role.TabIndex = 18
        Me.lbl_role.Text = "Role:"
        '
        'lbl_role_load
        '
        Me.lbl_role_load.AutoSize = True
        Me.lbl_role_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_role_load.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbl_role_load.Location = New System.Drawing.Point(592, 22)
        Me.lbl_role_load.Name = "lbl_role_load"
        Me.lbl_role_load.Size = New System.Drawing.Size(59, 13)
        Me.lbl_role_load.TabIndex = 19
        Me.lbl_role_load.Text = "user_role"
        '
        'lbl_vpn_con
        '
        Me.lbl_vpn_con.AutoSize = True
        Me.lbl_vpn_con.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_vpn_con.Location = New System.Drawing.Point(330, 22)
        Me.lbl_vpn_con.Name = "lbl_vpn_con"
        Me.lbl_vpn_con.Size = New System.Drawing.Size(76, 13)
        Me.lbl_vpn_con.TabIndex = 21
        Me.lbl_vpn_con.Text = "VPN Status:"
        '
        'lbl_vpn_con_load
        '
        Me.lbl_vpn_con_load.AutoSize = True
        Me.lbl_vpn_con_load.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_vpn_con_load.ForeColor = System.Drawing.Color.RoyalBlue
        Me.lbl_vpn_con_load.Location = New System.Drawing.Point(406, 22)
        Me.lbl_vpn_con_load.Name = "lbl_vpn_con_load"
        Me.lbl_vpn_con_load.Size = New System.Drawing.Size(98, 13)
        Me.lbl_vpn_con_load.TabIndex = 22
        Me.lbl_vpn_con_load.Text = "vpn_connection"
        '
        'btn_cb_address
        '
        Me.btn_cb_address.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_cb_address.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_cb_address.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_cb_address.Location = New System.Drawing.Point(363, 456)
        Me.btn_cb_address.Name = "btn_cb_address"
        Me.btn_cb_address.Size = New System.Drawing.Size(102, 21)
        Me.btn_cb_address.TabIndex = 23
        Me.btn_cb_address.Text = "CB Address"
        Me.btn_cb_address.UseVisualStyleBackColor = False
        '
        'app_running_timer
        '
        '
        'vpn_timer
        '
        '
        'pic_gdx_main
        '
        Me.pic_gdx_main.ErrorImage = Nothing
        Me.pic_gdx_main.Image = CType(resources.GetObject("pic_gdx_main.Image"), System.Drawing.Image)
        Me.pic_gdx_main.InitialImage = Nothing
        Me.pic_gdx_main.Location = New System.Drawing.Point(55, 446)
        Me.pic_gdx_main.Name = "pic_gdx_main"
        Me.pic_gdx_main.Size = New System.Drawing.Size(136, 41)
        Me.pic_gdx_main.TabIndex = 20
        Me.pic_gdx_main.TabStop = False
        '
        'pic_pinpad_c6
        '
        Me.pic_pinpad_c6.ErrorImage = Nothing
        Me.pic_pinpad_c6.Image = Global.GDnetworks.My.Resources.Resources.pinpad_offline
        Me.pic_pinpad_c6.Location = New System.Drawing.Point(127, 140)
        Me.pic_pinpad_c6.Name = "pic_pinpad_c6"
        Me.pic_pinpad_c6.Size = New System.Drawing.Size(18, 21)
        Me.pic_pinpad_c6.TabIndex = 44
        Me.pic_pinpad_c6.TabStop = False
        '
        'pic_pinpad_c5
        '
        Me.pic_pinpad_c5.ErrorImage = Nothing
        Me.pic_pinpad_c5.Image = Global.GDnetworks.My.Resources.Resources.pinpad_offline
        Me.pic_pinpad_c5.Location = New System.Drawing.Point(127, 116)
        Me.pic_pinpad_c5.Name = "pic_pinpad_c5"
        Me.pic_pinpad_c5.Size = New System.Drawing.Size(18, 21)
        Me.pic_pinpad_c5.TabIndex = 43
        Me.pic_pinpad_c5.TabStop = False
        '
        'pic_pinpad_c4
        '
        Me.pic_pinpad_c4.ErrorImage = Nothing
        Me.pic_pinpad_c4.Image = Global.GDnetworks.My.Resources.Resources.pinpad_offline
        Me.pic_pinpad_c4.Location = New System.Drawing.Point(127, 91)
        Me.pic_pinpad_c4.Name = "pic_pinpad_c4"
        Me.pic_pinpad_c4.Size = New System.Drawing.Size(18, 21)
        Me.pic_pinpad_c4.TabIndex = 42
        Me.pic_pinpad_c4.TabStop = False
        '
        'pic_pinpad_c3
        '
        Me.pic_pinpad_c3.ErrorImage = Nothing
        Me.pic_pinpad_c3.Image = Global.GDnetworks.My.Resources.Resources.pinpad_offline
        Me.pic_pinpad_c3.Location = New System.Drawing.Point(127, 67)
        Me.pic_pinpad_c3.Name = "pic_pinpad_c3"
        Me.pic_pinpad_c3.Size = New System.Drawing.Size(18, 21)
        Me.pic_pinpad_c3.TabIndex = 41
        Me.pic_pinpad_c3.TabStop = False
        '
        'pic_pinpad_c2
        '
        Me.pic_pinpad_c2.ErrorImage = Nothing
        Me.pic_pinpad_c2.Image = Global.GDnetworks.My.Resources.Resources.pinpad_offline
        Me.pic_pinpad_c2.Location = New System.Drawing.Point(127, 43)
        Me.pic_pinpad_c2.Name = "pic_pinpad_c2"
        Me.pic_pinpad_c2.Size = New System.Drawing.Size(18, 21)
        Me.pic_pinpad_c2.TabIndex = 40
        Me.pic_pinpad_c2.TabStop = False
        '
        'pic_pinpad_c1
        '
        Me.pic_pinpad_c1.ErrorImage = Nothing
        Me.pic_pinpad_c1.Image = Global.GDnetworks.My.Resources.Resources.pinpad_offline
        Me.pic_pinpad_c1.Location = New System.Drawing.Point(127, 20)
        Me.pic_pinpad_c1.Name = "pic_pinpad_c1"
        Me.pic_pinpad_c1.Size = New System.Drawing.Size(18, 21)
        Me.pic_pinpad_c1.TabIndex = 39
        Me.pic_pinpad_c1.TabStop = False
        '
        'gdx_main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.GhostWhite
        Me.ClientSize = New System.Drawing.Size(813, 499)
        Me.Controls.Add(Me.btn_cb_address)
        Me.Controls.Add(Me.lbl_vpn_con_load)
        Me.Controls.Add(Me.lbl_vpn_con)
        Me.Controls.Add(Me.pic_gdx_main)
        Me.Controls.Add(Me.lbl_role_load)
        Me.Controls.Add(Me.lbl_role)
        Me.Controls.Add(Me.lbl_refresh_load)
        Me.Controls.Add(Me.lbl_refresh)
        Me.Controls.Add(Me.lbl_user)
        Me.Controls.Add(Me.lbl_user_load)
        Me.Controls.Add(Me.btn_store_hours)
        Me.Controls.Add(Me.btn_list_status)
        Me.Controls.Add(Me.btn_moreinfo)
        Me.Controls.Add(Me.grp_moreinfo)
        Me.Controls.Add(Me.grp_cashes)
        Me.Controls.Add(Me.grp_devices)
        Me.Controls.Add(Me.grp_network_top)
        Me.Controls.Add(Me.grp_store)
        Me.Controls.Add(Me.grp_search)
        Me.Controls.Add(Me.top_menu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.top_menu
        Me.MaximizeBox = False
        Me.Name = "gdx_main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "GDnetworks (Prod)"
        Me.top_menu.ResumeLayout(False)
        Me.top_menu.PerformLayout()
        Me.grp_search.ResumeLayout(False)
        Me.grp_search.PerformLayout()
        Me.grp_store.ResumeLayout(False)
        Me.grp_store.PerformLayout()
        Me.grp_network_top.ResumeLayout(False)
        Me.grp_network_top.PerformLayout()
        Me.grp_devices.ResumeLayout(False)
        Me.grp_devices.PerformLayout()
        Me.grp_cashes.ResumeLayout(False)
        Me.grp_cashes.PerformLayout()
        Me.grp_moreinfo.ResumeLayout(False)
        Me.grp_moreinfo.PerformLayout()
        CType(Me.pic_gdx_main, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pinpad_c6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pinpad_c5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pinpad_c4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pinpad_c3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pinpad_c2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pinpad_c1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents top_menu As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ViewToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RemoteDesktopToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TeamViewerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OrionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BrowserToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ChromeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IEToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LABSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PRODToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CalendarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CMRToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RSGToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TrackingToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CanparToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FedExToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PurolatorToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UPSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StorePWDToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SupportDocsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HWPicsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProdcoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CameraToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TrafficCounterToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TechSupportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DBAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ServerSupportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ORPOSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents grp_search As GroupBox
    Friend WithEvents txtbox_storeno As TextBox
    Friend WithEvents btn_clear_store As Button
    Friend WithEvents grp_store As GroupBox
    Friend WithEvents lbl_str_ph As Label
    Friend WithEvents lbl_str_cntry As Label
    Friend WithEvents lbl_str_pcode As Label
    Friend WithEvents lbl_str_prov As Label
    Friend WithEvents lbl_str_city As Label
    Friend WithEvents lbl_str_add As Label
    Friend WithEvents lbl_str_mall As Label
    Friend WithEvents lbl_str_sisstr As Label
    Friend WithEvents lbl_str_banner As Label
    Friend WithEvents lbl_str_no As Label
    Friend WithEvents grp_network_top As GroupBox
    Friend WithEvents btn_gui_router As Button
    Friend WithEvents btn_cli_router As Button
    Friend WithEvents lbl_net_rtr As Label
    Friend WithEvents SavePositionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lbl_net_sw2 As Label
    Friend WithEvents lbl_net_sw1 As Label
    Friend WithEvents btn_gui_sw2 As Button
    Friend WithEvents btn_gui_sw1 As Button
    Friend WithEvents btn_cli_sw2 As Button
    Friend WithEvents btn_cli_sw1 As Button
    Friend WithEvents grp_devices As GroupBox
    Friend WithEvents lbl_stbl As Label
    Friend WithEvents lbl_ipad As Label
    Friend WithEvents lbl_music As Label
    Friend WithEvents lbl_tc As Label
    Friend WithEvents lbl_ap2 As Label
    Friend WithEvents lbl_ap1 As Label
    Friend WithEvents grp_cashes As GroupBox
    Friend WithEvents lbl_bo As Label
    Friend WithEvents lbl_cash6 As Label
    Friend WithEvents lbl_cash5 As Label
    Friend WithEvents lbl_cash4 As Label
    Friend WithEvents lbl_cash3 As Label
    Friend WithEvents lbl_cash2 As Label
    Friend WithEvents lbl_cash1 As Label
    Friend WithEvents grp_moreinfo As GroupBox
    Friend WithEvents btn_tv_c1 As Button
    Friend WithEvents btn_tv_bo As Button
    Friend WithEvents btn_tv_c6 As Button
    Friend WithEvents btn_tv_c5 As Button
    Friend WithEvents btn_tv_c4 As Button
    Friend WithEvents btn_tv_c3 As Button
    Friend WithEvents btn_tv_c2 As Button
    Friend WithEvents RefreshRateToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lbl_wifi_speed As Label
    Friend WithEvents lbl_speed As Label
    Friend WithEvents lbl_spare_equip As Label
    Friend WithEvents lbl_remote As Label
    Friend WithEvents lbl_str_district As Label
    Friend WithEvents btn_moreinfo As Button
    Friend WithEvents lbl_district_load As Label
    Friend WithEvents lbl_mainl_load As Label
    Friend WithEvents lbl_country_load As Label
    Friend WithEvents lbl_postal_load As Label
    Friend WithEvents lbl_state_load As Label
    Friend WithEvents lbl_city_load As Label
    Friend WithEvents lbl_address_load As Label
    Friend WithEvents lbl_mall_load As Label
    Friend WithEvents lbl_sis_store_load As Label
    Friend WithEvents lbl_ban_load As Label
    Friend WithEvents lbl_strno_load As Label
    Friend WithEvents lbl_switch1_load As Label
    Friend WithEvents lbl_router_load As Label
    Friend WithEvents lbl_switch2_load As Label
    Friend WithEvents lbl_ap2_load As Label
    Friend WithEvents lbl_ap1_load As Label
    Friend WithEvents lbl_samsung_load As Label
    Friend WithEvents lbl_ipad_load As Label
    Friend WithEvents lbl_music_load As Label
    Friend WithEvents lbl_tc_load As Label
    Friend WithEvents lbl_bo_load As Label
    Friend WithEvents lbl_cash6_load As Label
    Friend WithEvents lbl_cash5_load As Label
    Friend WithEvents lbl_cash4_load As Label
    Friend WithEvents lbl_cash3_load As Label
    Friend WithEvents lbl_cash2_load As Label
    Friend WithEvents lbl_cash1_load As Label
    Friend WithEvents btn_list_status As Button
    Friend WithEvents RemoteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RDPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TeamViewerToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents HelpF1ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As ToolStripSeparator
    Friend WithEvents SupportToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UpdatesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
    Friend WithEvents btn_store_hours As Button
    Friend WithEvents topmenu_refresh_0sec As ToolStripMenuItem
    Friend WithEvents topmenu_refresh_10sec As ToolStripMenuItem
    Friend WithEvents topmenu_refresh_30sec As ToolStripMenuItem
    Friend WithEvents topmenu_refresh_60sec As ToolStripMenuItem
    Friend WithEvents ping_timer As Timer
    Friend WithEvents AdminToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lbl_user_load As Label
    Friend WithEvents lbl_user As Label
    Friend WithEvents lbl_refresh As Label
    Friend WithEvents lbl_refresh_load As Label
    Friend WithEvents lbl_role As Label
    Friend WithEvents lbl_role_load As Label
    Friend WithEvents pic_gdx_main As PictureBox
    Friend WithEvents KeyboardShortcutsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RefreshToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReloadToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents bgwork_ping As System.ComponentModel.BackgroundWorker
    Friend WithEvents lbl_remote_load As Label
    Friend WithEvents lbl_spare_equip_load As Label
    Friend WithEvents lbl_store_speed_load As Label
    Friend WithEvents lbl_wifi_load As Label
    Friend WithEvents StoreToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EditToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DeleteStoreToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AddToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lbl_store_iscombo As Label
    Friend WithEvents lbl_store_iscombo_load As Label
    Friend WithEvents lbl_vpn_con As Label
    Friend WithEvents lbl_vpn_con_load As Label
    Friend WithEvents btn_cb_address As Button
    Friend WithEvents OnMapToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AutoUpdateToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lbl_str_system As Label
    Friend WithEvents lbl_str_system_load As Label
    Friend WithEvents pic_pinpad_c1 As PictureBox
    Friend WithEvents pic_pinpad_c2 As PictureBox
    Friend WithEvents pic_pinpad_c6 As PictureBox
    Friend WithEvents pic_pinpad_c5 As PictureBox
    Friend WithEvents pic_pinpad_c4 As PictureBox
    Friend WithEvents pic_pinpad_c3 As PictureBox
    Friend WithEvents BeehivrToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MerakiToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MobiControlToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OnToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OffToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents cmbbox_searchby As ComboBox
    Friend WithEvents btn_search_store As Button
    Friend WithEvents MyCalendarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GDriveToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents JiraToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LocalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SupportLocationsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents app_running_timer As Timer
    Friend WithEvents IPadSignUPToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BellDialToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CanadaPostToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintersToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CUPSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BIpublisherToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SpeedtestToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListSpeedtestByStoreToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RunSpeedtestToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents WebToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GarageToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DynamiteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DynamiteCAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DynamiteUSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GRPDYNToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents vpn_timer As Timer
    Friend WithEvents RMSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UnlockStoreAccountToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents WeblogicToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UnlockPOSToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PSEXECToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InventoryToolStripMenuItem As ToolStripMenuItem
End Class
