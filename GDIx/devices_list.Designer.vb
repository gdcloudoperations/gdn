﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class form_devices_list
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(form_devices_list))
        Me.listview_devices = New System.Windows.Forms.ListView()
        Me.btn_dev_list_export = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'listview_devices
        '
        Me.listview_devices.HideSelection = False
        resources.ApplyResources(Me.listview_devices, "listview_devices")
        Me.listview_devices.Name = "listview_devices"
        Me.listview_devices.UseCompatibleStateImageBehavior = False
        '
        'btn_dev_list_export
        '
        Me.btn_dev_list_export.BackColor = System.Drawing.SystemColors.InactiveCaption
        resources.ApplyResources(Me.btn_dev_list_export, "btn_dev_list_export")
        Me.btn_dev_list_export.Name = "btn_dev_list_export"
        Me.btn_dev_list_export.UseVisualStyleBackColor = False
        '
        'form_devices_list
        '
        Me.AccessibleRole = System.Windows.Forms.AccessibleRole.List
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btn_dev_list_export)
        Me.Controls.Add(Me.listview_devices)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "form_devices_list"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents listview_devices As ListView
    Friend WithEvents btn_dev_list_export As Button
End Class
