﻿'AUTHOR: Alex Dumitrascu
'DATE: 02-01-2018
'UPDATE: 28-08-2018
'
'FORM: admin.vb (access from admin_pwd.vb)


Imports System.Data.SqlClient
Imports System.IO
Imports System.Text.RegularExpressions

Public Class admin

    'PROTECTED FIEND VARS
    Protected Friend selected_user As String = Nothing

    'ON LOAD: admin.vb
    Private Sub admin_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'get users
        getUsers()

        'get master version

        Dim files = Directory.GetFiles("\\sagdxh01\GDnetworks\Program", "gdV_*.txt")
        If files.Length > 0 Then
            'GET VERSION
            For Each file In files
                Dim regex_version As Regex = New Regex("(?<=_).*?(?=.txt)", RegexOptions.IgnoreCase)
                Dim new_version = regex_version.Match(file)

                lbl_master_version_load.Text = new_version.ToString
            Next
        Else
            lbl_master_version_load.Text = "Version file missing!"
        End If


        'load user's details
        getUserDetails()

        'load counters
        getCounters()


    End Sub

    'GET USERS FROM DB
    Private Sub getUsers()

        Dim usr_connection As SqlConnection
        Dim cmd_usr As SqlCommand
        Dim userReader As SqlDataReader
        Dim sql_user_settings = "Select * from users_settings"
        usr_connection = New SqlConnection(gdx_main.connectionString)
        cmd_usr = New SqlCommand(sql_user_settings, usr_connection)

        Try
            usr_connection.Open()
            userReader = cmd_usr.ExecuteReader

            'GET USERS SETTINGS INTO LIST
            If userReader.HasRows Then
                While userReader.Read
                    Dim newitem As New ListViewItem()
                    newitem.UseItemStyleForSubItems = False
                    newitem.Text = userReader.GetValue(0).ToString                                          'ID
                    newitem.SubItems.Add(userReader.GetValue(1).ToString)                                   'Username
                    newitem.SubItems(1).Font = New Font(listview_users.Font, FontStyle.Bold)                            'SET BOLD
                    newitem.SubItems.Add(userReader.GetValue(2).ToString)                                   'Role
                    newitem.SubItems(2).Font = New Font(listview_users.Font, FontStyle.Bold)                            'SET BOLD
                    If userReader.GetValue(3).ToString = "Disabled" Then                                    'Acc Status
                        newitem.SubItems.Add(userReader.GetValue(3).ToString).ForeColor = Color.Orange                  'SET ORANGE COLOR
                    Else
                        newitem.SubItems.Add(userReader.GetValue(3).ToString).ForeColor = Color.Black                   'SET BLACK COLOR                      
                    End If
                    newitem.SubItems(3).Font = New Font(listview_users.Font, FontStyle.Bold)                            'SET BOLD
                    newitem.SubItems.Add(userReader.GetValue(4).ToString)                                   'IP Address
                    'user status (online / offline)
                    If userReader.GetValue(4).ToString = "N/A" Then
                        newitem.SubItems.Add("N/A").ForeColor = Color.Black
                    ElseIf userReader.GetValue(4).ToString = "0.0.0.0" Then
                        newitem.SubItems.Add("N/A").ForeColor = Color.Gray
                    Else
                        If userReader.GetValue(5).ToString() = "Online" Then
                            newitem.SubItems.Add("Online").ForeColor = Color.Green
                        Else
                            newitem.SubItems.Add("Offline").ForeColor = Color.Red
                        End If
                    End If
                    newitem.SubItems(5).Font = New Font(listview_users.Font, FontStyle.Bold)                            'SET BOLD
                    newitem.SubItems.Add(userReader.GetValue(6).ToString)                                   'Version
                    newitem.SubItems.Add(userReader.GetValue(7).ToString)                                   'Platform
                    newitem.SubItems.Add(userReader.GetValue(8).ToString)                                   'Created On
                    newitem.SubItems.Add(userReader.GetValue(9).ToString)                                   'Last Login
                    newitem.SubItems.Add(userReader.GetValue(10).ToString)                                  'Browser
                    newitem.SubItems.Add(userReader.GetValue(11).ToString)                                  'Remote
                    newitem.SubItems.Add(userReader.GetValue(12).ToString)                                  'Refresh Rate
                    newitem.SubItems.Add(userReader.GetValue(13).ToString)                                  'Form Location
                    newitem.SubItems.Add(userReader.GetValue(14).ToString)                                  'AutoUpdate

                    'add
                    listview_users.Items.Add(newitem).ToString()
                End While
            End If

            usr_connection.Close()

            'autosize columns & header
            listview_users.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent)
            listview_users.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize)

        Catch ex As Exception
            MsgBox("Unable to open DB! Contact your system administrator!", MsgBoxStyle.Critical, Title:="GDnetworks - Critical! [DB: users_settings - get users]")
        End Try

    End Sub

    'ON CLOSING FORM: clear list and dispose
    Private Sub admin_Closing(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        listview_users.Clear()
        Me.Dispose()

    End Sub

    'get users details count (right pane)
    Private Sub getUserDetails()

        'set counters to 0
        Dim enabledUsers As Integer = 0
        Dim disabledUsers As Integer = 0
        Dim onlineUsers As Integer = 0
        Dim offlineUsers As Integer = 0
        Dim autoUpdate As Integer = 0


        'get info from list
        For Each item As ListViewItem In listview_users.Items

            'get number of enabled and disabled users
            If item.SubItems(3).Text = "Enabled" Then
                enabledUsers = enabledUsers + 1
            ElseIf item.SubItems(3).Text = "Disabled" Then
                disabledUsers = disabledUsers + 1
            End If

            'get number of online and offline users
            If item.SubItems(5).Text = "Online" Then
                onlineUsers = onlineUsers + 1
            ElseIf item.SubItems(5).Text = "Offline" Then
                offlineUsers = offlineUsers + 1
            End If

            'get numbher of auto-update users
            If item.SubItems(14).Text = "True" Then
                autoUpdate = autoUpdate + 1
            End If

        Next

        'load user info (right pane)
        lbl_admin_date_load.Text = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        lbl_admin_user_load.Text = gdx_main.getUsername()

        'total number of users
        lbl_admin_user_nb_load.Text = listview_users.Items.Count

        'total number of enabled users
        lbl_admin_enabled_load.Text = enabledUsers.ToString

        'total number of disabled users
        lbl_admin_disabled_load.Text = disabledUsers.ToString

        'total number of online users
        lbl_admin_online_load.Text = onlineUsers.ToString

        'total number of offline users
        lbl_admin_offline_load.Text = offlineUsers.ToString

        'total number of auto-update users
        lbl_admin_auto_load.Text = autoUpdate.ToString



    End Sub

    'set counters
    Private Sub getCounters()

        'set counters to 0
        Dim count_admin As Integer = 0
        Dim count_dba As Integer = 0
        Dim count_dev As Integer = 0
        Dim count_guest As Integer = 0
        Dim count_hosupport As Integer = 0
        Dim count_orpos As Integer = 0
        Dim count_techsupport As Integer = 0
        Dim count_tech As Integer = 0
        Dim count_samsupport As Integer = 0
        Dim count_sam As Integer = 0


        For Each item As ListViewItem In listview_users.Items

            If item.SubItems(2).Text = "Admin" Then
                count_admin = count_admin + 1
            ElseIf item.SubItems(2).Text = "DBA" Then
                count_dba = count_dba + 1
            ElseIf item.SubItems(2).Text = "DEV" Then
                count_dev = count_dev + 1
            ElseIf item.SubItems(2).Text = "Guest" Then
                count_guest = count_guest + 1
            ElseIf item.SubItems(2).Text = "HO Support" Then
                count_hosupport = count_hosupport + 1
            ElseIf item.SubItems(2).Text = "ORPOS" Then
                count_orpos = count_orpos + 1
            ElseIf item.SubItems(2).Text = "Tech Support" Then
                count_techsupport = count_techsupport + 1
            ElseIf item.SubItems(2).Text = "Tech Support*" Then
                count_tech = count_tech + 1
            ElseIf item.SubItems(2).Text = "SAM" Then
                count_samsupport = count_samsupport + 1
            ElseIf item.SubItems(2).Text = "SAM*" Then
                count_sam = count_sam + 1
            End If

        Next

        'PRINT TO LABELS

        lbl_admin_admin_load.Text = count_admin.ToString()
        lbl_admin_dba_load.Text = count_dba.ToString()
        lbl_admin_dev_load.Text = count_dev.ToString()
        lbl_admin_guest_load.Text = count_guest.ToString()
        lbl_admin_hosupport_load.Text = count_hosupport.ToString()
        lbl_admin_orpos_load.Text = count_orpos.ToString()
        lbl_admin_techsupport_load.Text = count_techsupport.ToString()
        lbl_admin_techsup_load.Text = count_tech.ToString()
        lbl_admin_sam_load.Text = count_samsupport.ToString()
        lbl_admin_sm_load.Text = count_sam.ToString()

    End Sub

    'LISTVIEW: GET SELECTED USER
    Private Sub listview_users_SelectedIndexChanged(sender As Object, e As EventArgs) Handles listview_users.SelectedIndexChanged

    End Sub

    'BUTTON: REFRESH
    Private Sub btn_admin_refresh_Click(sender As Object, e As EventArgs) Handles btn_admin_refresh.Click
        listview_users.Items.Clear()
        getUsers()

        'reload users details (right pane)
        getUserDetails()

        'load counters
        getCounters()

    End Sub

    'BUTTON: ADD USER
    Private Sub btn_admin_adduser_Click(sender As Object, e As EventArgs) Handles btn_admin_adduser.Click
        AddUser.ShowDialog()
    End Sub

    'BUTTON: EDIT USER
    Private Sub btn_admin_edituser_Click(sender As Object, e As EventArgs) Handles btn_admin_edituser.Click

        If listview_users.SelectedItems.Count = Nothing Then
            MsgBox("Please select the username you want to edit!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (selection)")
        Else

            Dim msg_error As String = "no"
            Dim selected_user = listview_users.SelectedItems(0).SubItems(1).Text
            Dim selected_role As String = listview_users.SelectedItems(0).SubItems(2).Text

            If gdx_main.user_level = "limited+" And Not selected_role = "SAM" And Not selected_role = "Guest" Then
                MsgBox("You don't have enough rights to edit: " & selected_user, MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            ElseIf gdx_main.user_level = "general+" And Not selected_role = "SAM" And Not selected_role = "Tech Support" And Not selected_role = "Guest" Then
                MsgBox("You don't have enough rights to edit: " & selected_user, MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            ElseIf gdx_main.user_level = "elevated" And selected_role = "*SYS" Then
                MsgBox("You don't have enough rights to edit: " & selected_user, MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            Else

                'load selected user
                selected_user = listview_users.SelectedItems(0).SubItems(1).Text
                EditUser.ShowDialog()

            End If
        End If

    End Sub

    'BUTTON: DELETE USER
    Private Sub btn_admin_deleteuser_Click(sender As Object, e As EventArgs) Handles btn_admin_deleteuser.Click

        If listview_users.SelectedItems.Count = Nothing Then
            MsgBox("Please select the username you want to edit!", MsgBoxStyle.Information, Title:="GDnetworks - Info! (user selection)")
        Else

            Dim msg_error As String = "no"
            Dim selected_user = listview_users.SelectedItems(0).SubItems(1).Text
            Dim selected_role As String = listview_users.SelectedItems(0).SubItems(2).Text

            If gdx_main.user_level = "limited+" And Not selected_role = "SAM" And Not selected_role = "Guest" Then
                MsgBox("You don't have enough rights to delete: " & selected_user, MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            ElseIf gdx_main.user_level = "general+" And Not selected_role = "SAM" And Not selected_role = "Tech Support" And Not selected_role = "Guest" Then
                MsgBox("You don't have enough rights to delete: " & selected_user, MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            ElseIf gdx_main.user_level = "elevated" And selected_role = "*SYS" Then
                MsgBox("You don't have enough rights to delete: " & selected_user, MsgBoxStyle.Information, Title:="GDnetworks - Information! (delete user)")
            Else

                'load selected user
                Select Case MsgBox("Are you sure you want to delete: " & selected_user, MsgBoxStyle.YesNo, Title:="GDnetworks - Confirmation! (delete user)")
                'DELETE
                    Case MsgBoxResult.Yes
                        gdx_main.delete_user(selected_user)
                'KEEP USER
                    Case MsgBoxResult.No
                End Select

            End If

        End If
    End Sub

    'SORT LIST USING admin_users_compare CLASS
    Private Sub Listview_users_ColumnClick(sender As Object, e As System.Windows.Forms.ColumnClickEventArgs) Handles listview_users.ColumnClick
        Dim sortColumn As Integer = -1

        If e.Column <> sortColumn Then
            ' Set the sort column to the new column.
            sortColumn = e.Column
            ' Set the sort order to ascending by default.
            listview_users.Sorting = SortOrder.Ascending
        Else
            ' Determine what the last sort order was and change it.
            If listview_users.Sorting = SortOrder.Ascending Then
                listview_users.Sorting = SortOrder.Descending
            Else
                listview_users.Sorting = SortOrder.Ascending
            End If
        End If

        listview_users.ListViewItemSorter = New ListViewItemCompare(e.Column, listview_users.Sorting)
        listview_users.Sort()

    End Sub


End Class