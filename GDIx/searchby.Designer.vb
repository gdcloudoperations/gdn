﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class searchby
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(searchby))
        Me.listview_stores = New System.Windows.Forms.ListView()
        Me.col_store = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_banner = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_sister = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_status = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_mall = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_address = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_city = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_Province = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_pcode = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.col_country = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lbl_searchby_field = New System.Windows.Forms.Label()
        Me.btn_searchby_close = New System.Windows.Forms.Button()
        Me.lbl_searchby_nstores = New System.Windows.Forms.Label()
        Me.chkbox_searchby_combo = New System.Windows.Forms.CheckBox()
        Me.btn_searchby_refresh = New System.Windows.Forms.Button()
        Me.lbl_searchby_value = New System.Windows.Forms.Label()
        Me.lbl_searchby_field_load = New System.Windows.Forms.Label()
        Me.lbl_searchby_value_load = New System.Windows.Forms.Label()
        Me.grpbox_searchby_options = New System.Windows.Forms.GroupBox()
        Me.grpbox_searchby_only = New System.Windows.Forms.GroupBox()
        Me.chkbox_searchby_closedonly = New System.Windows.Forms.CheckBox()
        Me.chkbox_searchby_temponly = New System.Windows.Forms.CheckBox()
        Me.chkbox_searchby_remoteonly = New System.Windows.Forms.CheckBox()
        Me.chkbox_searchby_onlycombo = New System.Windows.Forms.CheckBox()
        Me.grpbox_searchby_exclude = New System.Windows.Forms.GroupBox()
        Me.chkbox_searchby_status = New System.Windows.Forms.CheckBox()
        Me.btn_searchby_save = New System.Windows.Forms.Button()
        Me.lbl_searchby_info = New System.Windows.Forms.Label()
        Me.grpbox_searchby_options.SuspendLayout()
        Me.grpbox_searchby_only.SuspendLayout()
        Me.grpbox_searchby_exclude.SuspendLayout()
        Me.SuspendLayout()
        '
        'listview_stores
        '
        Me.listview_stores.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.col_store, Me.col_banner, Me.col_sister, Me.col_status, Me.col_mall, Me.col_address, Me.col_city, Me.col_Province, Me.col_pcode, Me.col_country})
        Me.listview_stores.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.listview_stores.FullRowSelect = True
        Me.listview_stores.HideSelection = False
        Me.listview_stores.Location = New System.Drawing.Point(29, 43)
        Me.listview_stores.MultiSelect = False
        Me.listview_stores.Name = "listview_stores"
        Me.listview_stores.Size = New System.Drawing.Size(641, 250)
        Me.listview_stores.TabIndex = 3
        Me.listview_stores.UseCompatibleStateImageBehavior = False
        Me.listview_stores.View = System.Windows.Forms.View.Details
        '
        'col_store
        '
        Me.col_store.Text = "Store"
        Me.col_store.Width = 44
        '
        'col_banner
        '
        Me.col_banner.Text = "Banner"
        '
        'col_sister
        '
        Me.col_sister.Text = "Sister Store"
        Me.col_sister.Width = 75
        '
        'col_status
        '
        Me.col_status.Text = "Status"
        '
        'col_mall
        '
        Me.col_mall.Text = "Mall"
        Me.col_mall.Width = 50
        '
        'col_address
        '
        Me.col_address.Text = "Address"
        '
        'col_city
        '
        Me.col_city.Text = "City"
        '
        'col_Province
        '
        Me.col_Province.Text = "Province/State"
        Me.col_Province.Width = 94
        '
        'col_pcode
        '
        Me.col_pcode.Text = "Postal Code"
        Me.col_pcode.Width = 74
        '
        'col_country
        '
        Me.col_country.Text = "Country"
        '
        'lbl_searchby_field
        '
        Me.lbl_searchby_field.AutoSize = True
        Me.lbl_searchby_field.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_searchby_field.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_searchby_field.Location = New System.Drawing.Point(34, 15)
        Me.lbl_searchby_field.Name = "lbl_searchby_field"
        Me.lbl_searchby_field.Size = New System.Drawing.Size(67, 15)
        Me.lbl_searchby_field.TabIndex = 110
        Me.lbl_searchby_field.Text = "Search by: "
        '
        'btn_searchby_close
        '
        Me.btn_searchby_close.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_searchby_close.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_searchby_close.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_searchby_close.Location = New System.Drawing.Point(721, 311)
        Me.btn_searchby_close.Name = "btn_searchby_close"
        Me.btn_searchby_close.Size = New System.Drawing.Size(81, 21)
        Me.btn_searchby_close.TabIndex = 113
        Me.btn_searchby_close.Text = "CLOSE"
        Me.btn_searchby_close.UseVisualStyleBackColor = False
        '
        'lbl_searchby_nstores
        '
        Me.lbl_searchby_nstores.AutoSize = True
        Me.lbl_searchby_nstores.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_searchby_nstores.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_searchby_nstores.Location = New System.Drawing.Point(34, 311)
        Me.lbl_searchby_nstores.Name = "lbl_searchby_nstores"
        Me.lbl_searchby_nstores.Size = New System.Drawing.Size(90, 15)
        Me.lbl_searchby_nstores.TabIndex = 115
        Me.lbl_searchby_nstores.Text = "nb_of_stores"
        '
        'chkbox_searchby_combo
        '
        Me.chkbox_searchby_combo.AutoSize = True
        Me.chkbox_searchby_combo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkbox_searchby_combo.Location = New System.Drawing.Point(20, 30)
        Me.chkbox_searchby_combo.Name = "chkbox_searchby_combo"
        Me.chkbox_searchby_combo.Size = New System.Drawing.Size(141, 19)
        Me.chkbox_searchby_combo.TabIndex = 116
        Me.chkbox_searchby_combo.Text = "Duplicate COMBO"
        Me.chkbox_searchby_combo.UseVisualStyleBackColor = True
        '
        'btn_searchby_refresh
        '
        Me.btn_searchby_refresh.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_searchby_refresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_searchby_refresh.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_searchby_refresh.Location = New System.Drawing.Point(634, 311)
        Me.btn_searchby_refresh.Name = "btn_searchby_refresh"
        Me.btn_searchby_refresh.Size = New System.Drawing.Size(81, 21)
        Me.btn_searchby_refresh.TabIndex = 117
        Me.btn_searchby_refresh.Text = "Refresh"
        Me.btn_searchby_refresh.UseVisualStyleBackColor = False
        '
        'lbl_searchby_value
        '
        Me.lbl_searchby_value.AutoSize = True
        Me.lbl_searchby_value.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_searchby_value.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lbl_searchby_value.Location = New System.Drawing.Point(210, 15)
        Me.lbl_searchby_value.Name = "lbl_searchby_value"
        Me.lbl_searchby_value.Size = New System.Drawing.Size(74, 15)
        Me.lbl_searchby_value.TabIndex = 118
        Me.lbl_searchby_value.Text = "Looking for: "
        '
        'lbl_searchby_field_load
        '
        Me.lbl_searchby_field_load.AutoSize = True
        Me.lbl_searchby_field_load.Location = New System.Drawing.Point(95, 17)
        Me.lbl_searchby_field_load.Name = "lbl_searchby_field_load"
        Me.lbl_searchby_field_load.Size = New System.Drawing.Size(76, 13)
        Me.lbl_searchby_field_load.TabIndex = 120
        Me.lbl_searchby_field_load.Text = "searchBy_field"
        '
        'lbl_searchby_value_load
        '
        Me.lbl_searchby_value_load.AutoSize = True
        Me.lbl_searchby_value_load.Location = New System.Drawing.Point(278, 17)
        Me.lbl_searchby_value_load.Name = "lbl_searchby_value_load"
        Me.lbl_searchby_value_load.Size = New System.Drawing.Size(83, 13)
        Me.lbl_searchby_value_load.TabIndex = 121
        Me.lbl_searchby_value_load.Text = "searchBy_value"
        '
        'grpbox_searchby_options
        '
        Me.grpbox_searchby_options.Controls.Add(Me.grpbox_searchby_only)
        Me.grpbox_searchby_options.Controls.Add(Me.grpbox_searchby_exclude)
        Me.grpbox_searchby_options.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_searchby_options.Location = New System.Drawing.Point(688, 43)
        Me.grpbox_searchby_options.Name = "grpbox_searchby_options"
        Me.grpbox_searchby_options.Size = New System.Drawing.Size(205, 250)
        Me.grpbox_searchby_options.TabIndex = 123
        Me.grpbox_searchby_options.TabStop = False
        Me.grpbox_searchby_options.Text = "Search Options"
        '
        'grpbox_searchby_only
        '
        Me.grpbox_searchby_only.Controls.Add(Me.chkbox_searchby_closedonly)
        Me.grpbox_searchby_only.Controls.Add(Me.chkbox_searchby_temponly)
        Me.grpbox_searchby_only.Controls.Add(Me.chkbox_searchby_remoteonly)
        Me.grpbox_searchby_only.Controls.Add(Me.chkbox_searchby_onlycombo)
        Me.grpbox_searchby_only.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_searchby_only.Location = New System.Drawing.Point(18, 114)
        Me.grpbox_searchby_only.Name = "grpbox_searchby_only"
        Me.grpbox_searchby_only.Size = New System.Drawing.Size(167, 130)
        Me.grpbox_searchby_only.TabIndex = 125
        Me.grpbox_searchby_only.TabStop = False
        Me.grpbox_searchby_only.Text = "Select Only:"
        '
        'chkbox_searchby_closedonly
        '
        Me.chkbox_searchby_closedonly.AutoSize = True
        Me.chkbox_searchby_closedonly.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkbox_searchby_closedonly.Location = New System.Drawing.Point(20, 104)
        Me.chkbox_searchby_closedonly.Name = "chkbox_searchby_closedonly"
        Me.chkbox_searchby_closedonly.Size = New System.Drawing.Size(70, 19)
        Me.chkbox_searchby_closedonly.TabIndex = 119
        Me.chkbox_searchby_closedonly.Text = "Closed"
        Me.chkbox_searchby_closedonly.UseVisualStyleBackColor = True
        '
        'chkbox_searchby_temponly
        '
        Me.chkbox_searchby_temponly.AutoSize = True
        Me.chkbox_searchby_temponly.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkbox_searchby_temponly.Location = New System.Drawing.Point(20, 79)
        Me.chkbox_searchby_temponly.Name = "chkbox_searchby_temponly"
        Me.chkbox_searchby_temponly.Size = New System.Drawing.Size(62, 19)
        Me.chkbox_searchby_temponly.TabIndex = 118
        Me.chkbox_searchby_temponly.Text = "Temp"
        Me.chkbox_searchby_temponly.UseVisualStyleBackColor = True
        '
        'chkbox_searchby_remoteonly
        '
        Me.chkbox_searchby_remoteonly.AutoSize = True
        Me.chkbox_searchby_remoteonly.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkbox_searchby_remoteonly.Location = New System.Drawing.Point(20, 54)
        Me.chkbox_searchby_remoteonly.Name = "chkbox_searchby_remoteonly"
        Me.chkbox_searchby_remoteonly.Size = New System.Drawing.Size(76, 19)
        Me.chkbox_searchby_remoteonly.TabIndex = 117
        Me.chkbox_searchby_remoteonly.Text = "Remote"
        Me.chkbox_searchby_remoteonly.UseVisualStyleBackColor = True
        '
        'chkbox_searchby_onlycombo
        '
        Me.chkbox_searchby_onlycombo.AutoSize = True
        Me.chkbox_searchby_onlycombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkbox_searchby_onlycombo.Location = New System.Drawing.Point(20, 29)
        Me.chkbox_searchby_onlycombo.Name = "chkbox_searchby_onlycombo"
        Me.chkbox_searchby_onlycombo.Size = New System.Drawing.Size(76, 19)
        Me.chkbox_searchby_onlycombo.TabIndex = 116
        Me.chkbox_searchby_onlycombo.Text = "COMBO"
        Me.chkbox_searchby_onlycombo.UseVisualStyleBackColor = True
        '
        'grpbox_searchby_exclude
        '
        Me.grpbox_searchby_exclude.Controls.Add(Me.chkbox_searchby_status)
        Me.grpbox_searchby_exclude.Controls.Add(Me.chkbox_searchby_combo)
        Me.grpbox_searchby_exclude.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grpbox_searchby_exclude.Location = New System.Drawing.Point(18, 20)
        Me.grpbox_searchby_exclude.Name = "grpbox_searchby_exclude"
        Me.grpbox_searchby_exclude.Size = New System.Drawing.Size(167, 88)
        Me.grpbox_searchby_exclude.TabIndex = 124
        Me.grpbox_searchby_exclude.TabStop = False
        Me.grpbox_searchby_exclude.Text = "Exclude"
        '
        'chkbox_searchby_status
        '
        Me.chkbox_searchby_status.AutoSize = True
        Me.chkbox_searchby_status.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkbox_searchby_status.Location = New System.Drawing.Point(20, 55)
        Me.chkbox_searchby_status.Name = "chkbox_searchby_status"
        Me.chkbox_searchby_status.Size = New System.Drawing.Size(124, 19)
        Me.chkbox_searchby_status.TabIndex = 117
        Me.chkbox_searchby_status.Text = "CLOSED stores"
        Me.chkbox_searchby_status.UseVisualStyleBackColor = True
        '
        'btn_searchby_save
        '
        Me.btn_searchby_save.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.btn_searchby_save.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btn_searchby_save.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_searchby_save.Location = New System.Drawing.Point(808, 311)
        Me.btn_searchby_save.Name = "btn_searchby_save"
        Me.btn_searchby_save.Size = New System.Drawing.Size(81, 21)
        Me.btn_searchby_save.TabIndex = 124
        Me.btn_searchby_save.Text = "EXPORT"
        Me.btn_searchby_save.UseVisualStyleBackColor = False
        '
        'lbl_searchby_info
        '
        Me.lbl_searchby_info.AutoSize = True
        Me.lbl_searchby_info.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_searchby_info.ForeColor = System.Drawing.Color.Red
        Me.lbl_searchby_info.Location = New System.Drawing.Point(697, 17)
        Me.lbl_searchby_info.Name = "lbl_searchby_info"
        Me.lbl_searchby_info.Size = New System.Drawing.Size(192, 13)
        Me.lbl_searchby_info.TabIndex = 134
        Me.lbl_searchby_info.Text = "Double-click on any store to see details"
        '
        'searchby
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(905, 341)
        Me.Controls.Add(Me.lbl_searchby_info)
        Me.Controls.Add(Me.btn_searchby_save)
        Me.Controls.Add(Me.btn_searchby_refresh)
        Me.Controls.Add(Me.grpbox_searchby_options)
        Me.Controls.Add(Me.lbl_searchby_value_load)
        Me.Controls.Add(Me.lbl_searchby_field_load)
        Me.Controls.Add(Me.lbl_searchby_value)
        Me.Controls.Add(Me.lbl_searchby_nstores)
        Me.Controls.Add(Me.btn_searchby_close)
        Me.Controls.Add(Me.lbl_searchby_field)
        Me.Controls.Add(Me.listview_stores)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "searchby"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "GDnetworks - Search "
        Me.grpbox_searchby_options.ResumeLayout(False)
        Me.grpbox_searchby_only.ResumeLayout(False)
        Me.grpbox_searchby_only.PerformLayout()
        Me.grpbox_searchby_exclude.ResumeLayout(False)
        Me.grpbox_searchby_exclude.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents listview_stores As ListView
    Friend WithEvents col_store As ColumnHeader
    Friend WithEvents lbl_searchby_field As Label
    Friend WithEvents btn_searchby_close As Button
    Friend WithEvents lbl_searchby_nstores As Label
    Friend WithEvents col_banner As ColumnHeader
    Friend WithEvents col_sister As ColumnHeader
    Friend WithEvents col_mall As ColumnHeader
    Friend WithEvents col_address As ColumnHeader
    Friend WithEvents col_city As ColumnHeader
    Friend WithEvents col_Province As ColumnHeader
    Friend WithEvents col_pcode As ColumnHeader
    Friend WithEvents col_country As ColumnHeader
    Friend WithEvents chkbox_searchby_combo As CheckBox
    Friend WithEvents btn_searchby_refresh As Button
    Friend WithEvents lbl_searchby_value As Label
    Friend WithEvents lbl_searchby_field_load As Label
    Friend WithEvents lbl_searchby_value_load As Label
    Friend WithEvents col_status As ColumnHeader
    Friend WithEvents grpbox_searchby_options As GroupBox
    Friend WithEvents grpbox_searchby_exclude As GroupBox
    Friend WithEvents chkbox_searchby_status As CheckBox
    Friend WithEvents grpbox_searchby_only As GroupBox
    Friend WithEvents chkbox_searchby_remoteonly As CheckBox
    Friend WithEvents chkbox_searchby_onlycombo As CheckBox
    Friend WithEvents chkbox_searchby_temponly As CheckBox
    Friend WithEvents chkbox_searchby_closedonly As CheckBox
    Friend WithEvents btn_searchby_save As Button
    Friend WithEvents lbl_searchby_info As Label
End Class
