﻿'AUTHOR: Alex Dumitrascu
'DATE: 07-12-2017
'UPDATE: 25-01-2018
'
'FORM: About.vb (access from MAIN)

Imports System

Public Class form_about

    'LOAD VERSION & USER OS PLATFORM
    Private Sub form_about_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        lbl_about_version.Text = "version:  " & gdx_main.version
        lbl_about_platform.Text = "platform: " & gdx_main.osVersion

    End Sub

End Class