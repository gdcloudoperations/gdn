﻿'AUTHOR: Alex Dumitrascu
'DATE: 08-03-2018
'UPDATE: 09-03-2018
'
'FORM: support_docs.vb (access from MAIN)

Public Class support_docs

    'ON LOAD
    Private Sub support_docs_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        txtbox_docs_gdrive.Text = gdx_main.docs_gdrive
        txtbox_docs_jira.Text = gdx_main.docs_jira
        txtbox_docs_local.Text = gdx_main.docs_local

    End Sub

    'BUTTON SAVE: save support docs locations
    Private Sub btn_docs_save_Click(sender As Object, e As EventArgs) Handles btn_docs_save.Click

        'Google drive docs
        If txtbox_docs_gdrive.Text = "" Then
            gdx_main.docs_gdrive = "N/A"
        Else
            gdx_main.docs_gdrive = txtbox_docs_gdrive.Text
        End If

        'Jira docs
        If txtbox_docs_jira.Text = "" Then
            gdx_main.docs_jira = "N/A"
        Else
            gdx_main.docs_jira = txtbox_docs_jira.Text
        End If
        'local docs
        If txtbox_docs_local.Text = "" Then
            gdx_main.docs_local = "N/A"
        Else
            gdx_main.docs_local = txtbox_docs_local.Text
        End If


        'update user DB
        gdx_main.update_user(gdx_main.getUsername(), "DocsGDrive", txtbox_docs_gdrive.Text)
        gdx_main.update_user(gdx_main.getUsername(), "DocsJira", txtbox_docs_jira.Text)
        gdx_main.update_user(gdx_main.getUsername(), "DocsLocal", txtbox_docs_local.Text)
        MsgBox("User settings updated successfully!", MsgBoxStyle.Information, Title:="GDnetworks - Succes! (update user)")
        Me.Dispose()

    End Sub

    'BUTTON BROWSE: brose folder for local docs
    Private Sub btn_docs_local_Click(sender As Object, e As EventArgs) Handles btn_docs_local.Click

        Dim dialog As New FolderBrowserDialog()
        dialog.RootFolder = Environment.SpecialFolder.Desktop
        dialog.SelectedPath = "C:\"
        dialog.Description = "Select support docs local path"
        If dialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
            txtbox_docs_local.Text = dialog.SelectedPath
        End If

    End Sub

    Private Sub btn_docs_cancel_Click(sender As Object, e As EventArgs) Handles btn_docs_cancel.Click
        Me.Dispose()
    End Sub
End Class